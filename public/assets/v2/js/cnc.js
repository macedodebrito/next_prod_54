// Clear Form

// ON Open Modal
$(function() {
    console.log("ready.");
    $("#output_btn").on('click', function () {
        $("#send-output :input").prop("disabled", false);
        $("#set-obs").prop('disabled', true);
        $("#get-user-info").prop('disabled', true);    
        $("#set-user-validation").prop('disabled', true);    
        $("#btn-send-output").prop('disabled', true).show();
        $("#btn-wait-output").hide();
        $("#view-user-info-wrapper").addClass('d-none');
        $("#get-user-info").show().val('');
        $("#set-obs").val('');

        $("#get-pp").prop('disabled', true);
        $("#get-pp").empty();
        $("#get-pp").selectpicker('destroy');        
        $("#get-pp").selectpicker('render');

        $("#get-checked").prop('disabled', true);
        $("#get-checked").empty();
        $("#get-checked").selectpicker('destroy');        
        $("#get-checked").selectpicker('render');

        // get
        $.ajax({            
            type: 'GET',
            url: config.cnc.getPP,        
            success: function (data) {
                // inject to element / select
                $.each(data, function(key, value) {
                    $('#get-pp').append('<option data-rel="'+value.checked+'" data-subtext="'+value.color_name+' ['+value.checked+']" title="#'+value.pp+' - '+value.product_name+' ('+value.color_name+')" value="' + value.id + '">#' + value.pp + ' - '+value.product_name+'</option>');
                });
                $('#get-pp').selectpicker('destroy');                
                $('#get-pp').prop('disabled', false);                
                $('#get-pp').selectpicker('render');
            },
            beforeSend: function () {
                // reset
                console.log("reset.")
                // add spinner until its loaded.
                console.log("spinner.")
            },
        });
    });

    $("#get-pp").on('change', function (e) {
        $("#get-checked").prop('disabled', true);
        $("#set-obs").prop('disabled', true).val('');
        $("#get-user-info").prop('disabled', true);    
        $("#get-checked").empty();        
        $("#get-checked").selectpicker('destroy');        
        $("#get-checked").selectpicker('render');
        console.log("changed option: " + $(this).val() + " / " + $(this).find("option:selected").data('rel'));
        var i = 0;
        while(i < $(this).find("option:selected").data('rel')) {
            i++;
            $("#get-checked").append('<option value="' + i + '">' + i +'</option>');
        }
        $("#get-checked").selectpicker('destroy');        
        $("#get-checked").prop('disabled', false);
        $("#get-checked").selectpicker('render');
    });

    $("#get-checked").on('change', function (e) {   
        $("#set-obs").prop('disabled', false);    
        $("#get-user-info").prop('disabled', false);
        $("#set-obs").prop('disabled', false);    
    });    
});

$("#btn-send-output").on('click', function (e) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: config.cnc.sendOutput,
        data: new FormData(document.getElementById("send-output")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $("#send-output :input").prop("disabled", true);
            $("#get-pp").selectpicker('destroy');        
            $("#get-checked").selectpicker('destroy');        
            $("#get-pp").selectpicker('render');
            $("#get-checked").selectpicker('render');
            $("#btn-send-output").prop('disabled', true).hide();
            $("#btn-wait-output").show();
            $("#set-user-validation").prop('disabled', true);            
        }
      })

        .done((data) => {
            console.log({ data });
            //$("#cnc_stock_output").modal('hide');
            if (data.id != 0) {
                console.log("A");
            }
            else {
                console.log("B");
            }
        })
        .fail((err) => {
          console.error(err);
        })
});


$('.btnx').on('click', function() {
    var $this = $(this);
  $this.button('loading');
    setTimeout(function() {
       $this.button('reset');
   }, 8000);
});
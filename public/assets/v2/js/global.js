//// Relógio de Ponto
//
// on open, focus no código

(function ($) {
    $.fn.delayKeyup = function (callback, ms) {
        return this.each(function () {
            var timer = 0, elem = this;
            $(this).keyup(function () {
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.call(elem);
                }, ms);
            });
        });
    };
})(jQuery);

$( window ).on( "load", function() {        
    $("#output_btn").prop('disabled', false);
    $("#loader").modal("hide");
});

$(document).ready(function() { 
    $('#user-clock-confirm').click(function() {
        if (!this.checked) {
            $("#relogio-de-ponto-info-button").removeClass("d-block").addClass("d-none");
            $("#relogio-de-ponto-info-button-action").show();
            $("#relogio-de-ponto-info-button-work-absence").show();
        }
        else {
            $("#relogio-de-ponto-info-button").removeClass("d-none").addClass("d-block");
        }
    });
});

$("#relogio-de-ponto-btn").on('click', function () {
    $("#get-user-clock").val('');
    $("#get-user-clock-id").val('');
    $("#get-user-clock-login-logout").val('');
    $("#relogio-de-ponto-info").removeClass("d-block").addClass("d-none");
    $("#relogio-de-ponto-info-button").removeClass("d-block").addClass("d-none");    
    $("#relogio-de-ponto-info-button-action").show();
    $("#relogio-de-ponto-info-button-work-absence").show();
});

$('#relogio-de-ponto').on('shown.bs.modal', function (e) {
    $("#get-user-clock").trigger("focus");
});

$("#relogio-de-ponto-info-button-action").on('click', function () {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: config.routes.setUserClock,
        data: new FormData(document.getElementById("relogio-de-ponto-form")),        
        processData: false,
        contentType: false,
        
        success: function (data) {
            location.reload();
        },
        beforeSend: function () {
            $('#relogio-de-ponto').modal('toggle');
            $("#waiting-spinner-title").text("Relógio de Ponto");
            $("#waiting-spinner-text").text("A processar...");
            $("#waiting-spinner").modal({backdrop: "static"});
        },
    });
});

$('#get-user-clock').delayKeyup(function () {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: config.routes.checkUser,
        //data: $('#relogio-de-ponto-form').serialize(),        
        //contentType: 'application/json',
        data: new FormData(document.getElementById("relogio-de-ponto-form")),
        processData: false,
        contentType: false,
      })
        .done((data) => {
            //console.log({ data });
            if (data.id != 0) {
                var newdata = data.created_at.date;
                var datex = newdata.split(" ");
                var date_day = datex[0].split("-");
                var day = date_day[2];
                var month = date_day[1];
                var year = date_day[0];

                var time = datex[1].split(":");
                var hour = time[0];
                var minute = time[1];

                $("#relogio-de-ponto-info").removeClass("d-none");
                $("#relogio-de-ponto-info-name").text(data.name);
                $("#relogio-de-ponto-info-in-out").removeClass("bg-danger").removeClass("bg-success").addClass(data.class.in_out);
                $("#relogio-de-ponto-info-in-out-text").text(data.in_out_text + ' ' + day + "." + month + "." + year + ' às ' + hour + ":" + minute);
                $("#relogio-de-ponto-info-in-out-icon").addClass(data.class.in_out_icon);
                $("#relogio-de-ponto-info-button-action").removeClass("bg-danger").removeClass("bg-success").addClass(data.class.action).text(data.action_text);
                $("#user-clock-confirm").prop('checked', false);
                $("#get-user-clock-id").val(data.id);
                $("#get-user-clock-login-logout").val(data.login_logout);
            }
            $("#relogio-de-ponto-info").addClass(data.class.info);
            $("#relogio-de-ponto-info-button").addClass(data.class.info);
        })
        .fail((err) => {
          console.error(err);
        })
        //.always(() => {
        //  console.log('always called');
        //});

});
  
$('#get-user-info').delayKeyup(function () {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: config.routes.checkUserInfo,
        data: new FormData(document.getElementById("send-output")),
        processData: false,
        contentType: false,
      })
        .done((data) => {
            console.log({ data });
            if (data.id != 0) {
                $("#set-user-validation").prop('disabled', false);
                $('#get-user-info').hide();
                $('#set-user-id').val(data.id);
                $("#view-user-info-wrapper").removeClass('d-none');
                $("#view-user-info").val(data.name);
            }
            else {
                $("#set-user-validation").prop('disabled', true);
                $('#get-user-info').show();
                $("#view-user-info-wrapper").addClass('d-none');
            }
        })
        .fail((err) => {
          console.error(err);
        })
});

$("#reset-user-info").on('click', function () {
    $('#get-user-info').show().val('');
    $("#view-user-info-wrapper").addClass('d-none');
    $("#set-user-validation").prop('disabled', true);
    $("#btn-send-output").prop('disabled', true);
});

$("#set-user-validation").on('click', function () {
    $("#btn-send-output").prop('disabled', false);
});

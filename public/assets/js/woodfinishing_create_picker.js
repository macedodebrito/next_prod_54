$(document).ready(function () {
    // MUDAR O PP

    // TEM BUG
    $('#request_id').val(0);
    $("button[name='next_action']").prop('disabled', true);
    $("#picker").prop('disabled', true);
    $("#picker2").prop('disabled', true);
    $("#total").prop('disabled', true);

    $('<label>').attr({
        id: 'label_picker',
        for : 'new_picker',
        class: 'control-label',
    }).text("Contagem do PP").insertAfter(".panel-heading").css({'width': '15%', 'float': 'left', 'margin-left': '15px', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_code',
        for : 'product_name',
        class: 'control-label',
    }).text("Produto").insertAfter("#label_picker").css({'width': '15%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_request_id',
        for : 'request_id',
        class: 'control-label',
    }).text("PP").insertAfter("#label_code").css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

//    $('<label>').attr({
//        id: 'label_serial',
//        for : 'serial',
//        class: 'control-label',
//    }).text("Serial").insertAfter("#label_request_id").css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_obs',
        for : 'obs',
        class: 'control-label',
    }).text("Observações").insertAfter("#label_request_id").css({'width': '45%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_delete',
        for : 'delete',
        class: 'control-label',
    }).text("Apagar").insertAfter("#label_obs").css({'width': '5%', 'float': 'left', 'margin-bottom': '15px'});

    // CHECK PASSWORD
    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
//                    $("button[name='next_action']").prop('disabled', false);
                    $("#picker").prop('disabled', false);
                    //$("#picker_2").prop('disabled', false);
                    $("#picker").focus();
                }
                else {
//                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                    $("#picker").prop('disabled', true);
                    $("#picker2").prop('disabled', true);
                }
            }
        });

    }, 1000);

    // PICKER
    $('#picker').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_woodfinishing_picker/' + $("#picker").val(),
            success: function (data) {
//                console.log(data);
                //$("#picker").val("");
                //$("#picker").focus();
                $("button[name='next_action']").prop('disabled', true);
                if (data.id != 0) {
                    console.log($(".new_picker[rel='" + data.request_pp + "']").length + 1);
                    console.log(data.request_value);
                    if (data.product_next + $(".new_picker[rel='" + data.request_pp + "']").length + 1 <= data.request_value ) {
                        var next = data.product_next + $(".new_picker[rel='" + data.request_pp + "']").length + 1;
                        console.log(next);
                        if (!$('#picker_' + data.id + '_' + next).length) {
//                            $("#picker").prop('disabled', true);
//                            $("#picker2").prop('disabled', false);
                            //$("#picker2").focus();

                            console.log("GO1");
                            $('<input>').attr({
                                type: 'text',
                                id: 'picker_' + data.id + '_' + next,
                                name: 'new_picker[]',
                                class: 'form-control new_picker row',
                                value: next+"/"+data.request_value,
                                rel: data.request_pp,
                                disabled: 'disabled',
                            }).insertAfter("#label_delete").css({'width': '15%', 'float': 'left', 'margin-left': '15px', 'margin-right': '15px', 'margin-bottom': '15px'});

                            $('<input>').attr({
                                type: 'text',
                                id: 'product_name_' + data.id + '_' + next,
                                name: 'product_names[]',
                                class: 'form-control new_product_names',
                                value: data.product_name,
                                disabled: 'disabled',
                            }).insertAfter("#picker_" + data.id + '_' + next).css({'width': '15%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                            $('<input>').attr({
                                type: 'text',
                                id: 'request_id_' + data.id + '_' + next,
                                name: 'request_id[]',
                                class: 'form-control new_pp',
                                value: data.request_pp,
                                disabled: 'disabled',
                            }).insertAfter("#product_name_" + data.id + '_' + next).css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

//                            $('<input>').attr({
//                                type: 'text',
//                                id: 'serial_' + data.id,
//                                name: 'serial[]',
//                                class: 'form-control new_serial',
//                                value: data.serial,
//                                disabled: 'disabled',
//                            }).insertAfter("#request_id_" + data.id).css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                            $('<input>').attr({
                                type: 'text',
                                id: 'obs_' + data.id + '_' + next,
                                name: 'obs[]',
                                class: 'form-control new_obs',
                            }).insertAfter("#request_id_" + data.id + '_' + next).css({'width': '45%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                            $('<input>').attr({
                                type: 'button',
                                id: 'button_' + data.id + '_' + next,
                                rel: data.id + '_' + next,
                                class: 'form-control new_button_delete btn-danger',
                                value: "x",
                            }).insertAfter("#obs_" + data.id + '_' + next).css({'width': '5%', 'float': 'left', 'margin-bottom': '15px'});
                            $("#picker").prop('disabled', false);
                            $("#picker2").prop('disabled', true);
                            $("#picker").val("");
                            $("#picker2").val("");
                            $("#picker").focus();
                        }
                    }
                    $("#total").val($(".new_picker").length);
                }
                else {
                    $("#picker").prop('disabled', false);
                    $("#picker").val("");
                    $("#picker").focus();
                }
            }
        });

    }, 1000);


    $('#picker2').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_woodfinishing_picker/' + $("#picker2").val(),
            success: function (data) {
//                console.log(data);
                $("#picker_2").val("");
                $("#picker_2").focus();
                $("button[name='next_action']").prop('disabled', true);
                if (data.id != 0) {
//                    console.log(data.product_count + ' | ' + data.id + ' - ' + $(".new_picker[rel='"+data.request_pp+"']").length);
                    if (data.product_next + $(".new_picker[rel='" + data.request_pp + "']").length + 1 <= data.request_value ) {
                        var next = data.product_next + $(".new_picker[rel='" + data.request_pp + "']").length + 1;
//                        console.log("verificar se já existe na listagem esse PICKER");
                        if (!$('#picker_' + data.id + '_' + next).length) {
//                           console.log("CHECKAR AMBOS PICKERS");
                            if ($("#picker").val() === $("#picker2").val()) {
                                console.log("GO2");
                                $('<input>').attr({
                                    type: 'text',
                                    id: 'picker_' + data.id + '_' + next,
                                    name: 'new_picker[]',
                                    class: 'form-control new_picker row',
                                    value: next+"/"+data.request_value,
                                    rel: data.request_pp,
                                    disabled: 'disabled',
                                }).insertAfter("#label_delete").css({'width': '15%', 'float': 'left', 'margin-left': '15px', 'margin-right': '15px', 'margin-bottom': '15px'});

                                $('<input>').attr({
                                    type: 'text',
                                    id: 'product_name_' + data.id + '_' + next,
                                    name: 'product_names[]',
                                    class: 'form-control new_product_names',
                                    value: data.product_name,
                                    disabled: 'disabled',
                                }).insertAfter("#picker_" + data.id + '_' + next).css({'width': '15%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                                $('<input>').attr({
                                    type: 'text',
                                    id: 'request_id_' + data.id + '_' + next,
                                    name: 'request_id[]',
                                    class: 'form-control new_pp',
                                    value: data.request_pp,
                                    disabled: 'disabled',
                                }).insertAfter("#product_name_" + data.id + '_' + next).css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

//                                $('<input>').attr({
//                                    type: 'text',
//                                    id: 'serial_' + data.id,
//                                    name: 'serial[]',
//                                    class: 'form-control new_serial',
//                                    value: data.serial,
//                                    disabled: 'disabled',
//                                }).insertAfter("#request_id_" + data.id).css({'width': '10%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                                $('<input>').attr({
                                    type: 'text',
                                    id: 'obs_' + data.id + '_' + next,
                                    name: 'obs[]',
                                    class: 'form-control new_obs',
                                }).insertAfter("#request_id_" + data.id + '_' + next).css({'width': '45%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

                                $('<input>').attr({
                                    type: 'button',
                                    id: 'button_' + data.id + '_' + next,
                                    rel: data.id + '_' + next,
                                    class: 'form-control new_button_delete btn-danger',
                                    value: "x",
                                }).insertAfter("#obs_" + data.id + '_' + next).css({'width': '5%', 'float': 'left', 'margin-bottom': '15px'});
                                $("#picker").prop('disabled', false);
                                $("#picker2").prop('disabled', true);
                                $("#picker").val("");
                                $("#picker2").val("");
                                $("#picker").focus();
                            }
                            else {
//                                console.log("DONT GO")
                                $("#picker2").val("");
                                $("#picker2").prop('disabled', true);
                                $("#picker").focus();
                            }
                        }
                    }
                    $("#total").val($(".new_picker").length);
                }
                else {
                    $("#picker").prop('disabled', false);
                    $("#picker2").prop('disabled', true);
                    $("#picker").val("");
                    $("#picker2").val("");
                    $("#picker").focus();
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
//        $(".new_serial").prop('disabled', false);
        $(".new_pp").prop('disabled', false);
        $("form").submit();
    });

    // APAGAR LINHA
    $(document).on('click', '.new_button_delete', function () {
        picker_id = $(this).attr('rel');
        $("#picker_" + picker_id).remove();
        $("#product_name_" + picker_id).remove();
        $("#request_id_" + picker_id).remove();
//        $("#serial_" + picker_id).remove();
        $("#obs_" + picker_id).remove();
        $("#button_" + picker_id).remove();
        $("button[name='next_action']").prop('disabled', true);
        $("#total").val($(".new_picker").length);
    });

    // VALIDAR
    $(document).on('click', "input[name='validate_picker']", function () {
        if ($(".new_picker").length > 0) {
            $("button[name='next_action']").prop('disabled', false);
        }
        else {
            $("input[name='user_id']").val("");
            $("button[name='next_action']").prop('disabled', true);
        }
    });

    // 
    $(document).on('click', "input[name='manual_version']", function () {
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/woodfinishing/history/create';
    });

    // ADICIONAR "VALIDAR"
    $("<input type='button' name='validate_picker' class='btn btn-danger' style='margin-right: 10px;' value='Validar'/>").insertBefore("button[name='next_action']");

    // ADICIONAR "VERSÃO MANUAL"
    $("<input type='button' name='manual_version' class='btn btn-info' style='float: right; margin-top: -30px;' value='Alterar para Versão Manual'/>").insertAfter(".content-header > h1");
});


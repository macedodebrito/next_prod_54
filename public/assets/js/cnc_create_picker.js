$(document).ready(function () {
    // MUDAR O PP

    // TEM BUG
    $('#request_id').val(0);
    $("button[name='next_action']").prop('disabled', true);
    $("#picker").prop('disabled', true);
//    $("#picker2").prop('disabled', true);
    $("#total").prop('disabled', true);
    $("#published").focus();

    $('<label>').attr({
        id: 'label_picker',
        for : 'new_picker',
        class: 'control-label',
    }).text("Contagem do PP").insertAfter(".panel-heading").css({'width': '10%', 'float': 'left', 'margin-left': '15px', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_code',
        for : 'product_name',
        class: 'control-label',
    }).text("Produto").insertAfter("#label_picker").css({'width': '25%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_obs',
        for : 'obs',
        class: 'control-label',
    }).text("Observações").insertAfter("#label_code").css({'width': '55%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

    $('<label>').attr({
        id: 'label_delete',
        for : 'delete',
        class: 'control-label',
    }).text("Apagar").insertAfter("#label_obs").css({'width': '5%', 'float': 'left', 'margin-bottom': '15px'});

    // CHECK PASSWORD
    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
//                    $("button[name='next_action']").prop('disabled', false);
                    $("#picker").prop('disabled', false);
                    //$("#picker_2").prop('disabled', false);
                    $("#picker").focus();
                }
                else {
//                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                    $("#picker").prop('disabled', true);
//                    $("#picker2").prop('disabled', true);
                }
            }
        });

    }, 1000);

    // PICKER
    $('#picker').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_cnc_picker/' + $("#picker").val(),
            success: function (data) {
//                console.log(data);
                //$("#picker").val("");
                //$("#picker").focus();
                $("button[name='next_action']").prop('disabled', true);
                if (data.id != 0) {    
                    if ($(".new_picker[rel='" + data.id + "']").length + 1 <= data.GET_PP_MAX ) {
                        var next = $(".new_picker[rel='" + data.id + "']").length + 1;
                        if (!$('#picker_' + data.id + '_' + next).length) {
                            console.log("GO1");
                            $("#total").find('option').remove();
                            $("#total").attr('data-id',data.id);
                            $("#total").attr('data-GET_PP_MAX',data.GET_PP_MAX);
                            $("#total").attr('data-product_name',data.product_name);
                            $("#total").attr('data-product_code',data.product_name + data.custom_code.toUpperCase());
                            $("#total").attr('data-color_name',data.custom_name);
                            $("#total").attr('data-product_id',data.product_id);
                            $("#total").attr('data-custom_id',data.custom_id);
                            var i;
                            for (i = 0; i <= data.GET_PP_MAX-next+1; i++) {
                                $("#total").append($('<option>', { 
                                    value: i,
                                    text : i
                                }));
                            }
                            $("#total").val(0).prop('disabled', false);
                        }
                    }
                    //$("#total").val($(".new_picker").length);
                }
                else {
                    $("#picker").prop('disabled', false);
                    $("#picker").val("");
                    $("#picker").focus();
                    $("#total").find('option').remove();
                    $("#total").prop('disabled', true);
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
//        $(".new_serial").prop('disabled', false);
        $(".new_pp").prop('disabled', false);
        $("form").submit();
    });

    $("#total").change(function (e) {
        //var data_id = $(this).attr('data-id');
        var data_id = $(this).attr('data-product_code');
        var data_GET_PP_MAX = $(this).attr('data-GET_PP_MAX');
        var data_product_name = $(this).attr('data-product_name') + " (" + $(this).attr('data-color_name') + ")";
        var data_product_id = $(this).attr('data-product_id');
        var data_custom_id = $(this).attr('data-custom_id');
        var tstamp = Math.floor(Date.now() / 1000);
        for (i = 1; i <= this.value; i++) {         
            var next = $(".new_picker[rel='" + data_id + "']").length + 1;
            tstamp++;
            $('<input>').attr({
                type: 'text',
                id: 'picker_' + data_id + '_' + tstamp,
                name: 'new_picker[]',
                class: 'form-control new_picker row',
                value: next+"/"+data_GET_PP_MAX,
                rel: data_id,
                'data-max': data_GET_PP_MAX,
                disabled: 'disabled',
            }).insertAfter("#label_delete").css({'width': '10%', 'float': 'left', 'margin-left': '15px', 'margin-right': '15px', 'margin-bottom': '15px'});

            $('<input>').attr({
                type: 'text',
                id: 'product_name_' + data_id + '_' + tstamp,
                name: 'product_names[]',
                class: 'form-control new_product_names',
                value: data_product_name,
                disabled: 'disabled',
            }).insertAfter("#picker_" + data_id + '_' + tstamp).css({'width': '25%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

            $('<input>').attr({
                type: 'hidden',
                id: 'product_id_' + data_id + '_' + tstamp,
                name: 'product_ids[]',
                class: 'form-control new_product_ids',
                value: data_product_id,
            }).insertAfter("#product_name_" + data_id + '_' + tstamp);

            $('<input>').attr({
                type: 'hidden',
                id: 'custom_id_' + data_id + '_' + tstamp,
                name: 'custom_ids[]',
                class: 'form-control new_custom_ids',
                value: data_custom_id,
            }).insertAfter("#product_id_" + data_id + '_' + tstamp);

            $('<input>').attr({
                type: 'text',
                id: 'obs_' + data_id + '_' + tstamp,
                name: 'obs[]',
                class: 'form-control new_obs',
            }).insertAfter("#custom_id_" + data_id + '_' + tstamp).css({'width': '55%', 'float': 'left', 'margin-right': '15px', 'margin-bottom': '15px'});

            $('<input>').attr({
                type: 'button',
                id: 'button_' + data_id + '_' + tstamp,
                rel: data_id + '_' + tstamp,
                'data-rel': data_id,
                class: 'form-control new_button_delete btn-danger',
                value: "x",
            }).insertAfter("#obs_" + data_id + '_' + tstamp).css({'width': '5%', 'float': 'left', 'margin-bottom': '15px'});
            
            $("#picker").prop('disabled', false);
            $("#picker").val("");
            $("#picker").focus();
        }
        $("#total").find('option').remove();
        $("#total").prop('disabled', true);
    });
    
    // APAGAR LINHA
    $(document).on('click', '.new_button_delete', function () {
        picker_id = $(this).attr('rel');
        data_rel = $(this).attr('data-rel');
        $("#picker_" + picker_id).remove();
        $("#product_name_" + picker_id).remove();
        $("#request_id_" + picker_id).remove();
//        $("#serial_" + picker_id).remove();
        $("#obs_" + picker_id).remove();
        $("#button_" + picker_id).remove();

        $(".new_picker[rel='" + data_rel + "']").each(function( index ) {
           $(this).val($(".new_picker[rel='" + data_rel + "']").length-index+'/'+$(this).attr('data-max'));
        });
        
        $("button[name='next_action']").prop('disabled', true);
        $("#total").val($(".new_picker").length);
        
    });

    // VALIDAR
    $(document).on('click', "input[name='validate_picker']", function () {
        if ($(".new_picker").length > 0) {
            $("button[name='next_action']").prop('disabled', false);
        }
        else {
            $("input[name='user_id']").val("");
            $("button[name='next_action']").prop('disabled', true);
        }
    });

    // 
    $(document).on('click', "input[name='manual_version']", function () {
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/requests/cnc_history/create';
    });

    // ADICIONAR "VALIDAR"
    $("<input type='button' name='validate_picker' class='btn btn-danger' style='margin-right: 10px;' value='Validar'/>").insertBefore("button[name='next_action']");

    // ADICIONAR "VERSÃO MANUAL"
    $("<input type='button' name='manual_version' class='btn btn-info' style='float: right; margin-top: -30px;' value='Alterar para Versão Manual'/>").insertAfter(".content-header > h1");
});


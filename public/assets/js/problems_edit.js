$(document).ready(function () {
    
    $('button[name="next_action"]').prop('disabled', true);
    $('textarea[name="text"]').prop('disabled', true);
    $('textarea[name="obs"]').prop('disabled', true);
    $('select[name="section_type"]').prop('disabled', true);
    $('select[name="status"]').prop('disabled', true);
    
    $('#auth').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#auth").val(),
            success: function (data) {
                if (data.id != 0) {
                    $('button[name="next_action"]').prop('disabled', false);                    
                    $('textarea[name="text"]').prop('disabled', false);
                    $('input[name="resolution_id"]').val(data.id);
                    $('textarea[name="obs"]').prop('disabled', false);
                    $('select[name="section_type"]').prop('disabled', false);
                    $('select[name="status"]').prop('disabled', false);                    
                }
                else {
                    $('button[name="next_action"]').prop('disabled', true);
                    $('textarea[name="text"]').prop('disabled', true);
                    $('input[name="resolution_id"]').val('');
                    $('textarea[name="obs"]').prop('disabled', true);
                    $('select[name="section_type"]').prop('disabled', true);
                    $('select[name="status"]').prop('disabled', true);                    
                }
            }
        });

    }, 1000);
});


function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function getdate(date, offset) {
    var pickervalue = date;

    var datepicker = pickervalue.split(".");
    var joindate = new Date();
    var numberOfDaysToAdd = offset;

    joindate.setFullYear(parseInt(datepicker[2]), parseInt(datepicker[1]) - 1, parseInt(datepicker[0]) + numberOfDaysToAdd);

    var dateObj = joindate
    //var seconds = addZero(dateObj.getSeconds());
    var minute = addZero(dateObj.getMinutes());
    var hour = addZero(dateObj.getHours());
    var month = addZero(dateObj.getUTCMonth() + 1); //months from 1-12
    var day = addZero(dateObj.getUTCDate());
    var year = dateObj.getUTCFullYear();
    //console.log(dateObj);
    newdate = day + "." + month + "." + year + " " + hour + ":" + minute;
    //console.log(newdate);
    return newdate;
}

$(document).ready(function () {
    $("button[name='next_action']").click(function (e) {
//        $("#carpintaria").val($('#quantity').val());
//        $("#pintura").val($('#quantity').val());
//        $("#montagem").val($('#quantity').val());
//        console.log($("#end_at").val());
//        console.log("---")
//        $("#end_at").val($("#end_at").val());
//        console.log($("#end_at").val());
//        console.log("---")
//        console.log($("#end_paint_at").val());
//        console.log($("#end_woodfinishing_at").val());
//        console.log($("#end_wood_at").val());
        $("form").submit();

    });

    $('input#end_at').bind("change keyup input blur focus", function () {
        $("#end_paint_at").val(getdate($("#end_at").val(), -4));
        $("#end_woodfinishing_at").val(getdate($("#end_at").val(), -7));
        $("#end_wood_at").val(getdate($("#end_at").val(), -10));
    });

    var myURL2 = parseUri(window.location.href);
    if (myURL2['href'] != myURL2['href'].split("/admin")[0] + '/admin/request_weeks') {
        $('form').css('marginBottom', '80px');
    }
});
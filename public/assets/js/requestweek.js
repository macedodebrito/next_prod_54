function check_multi_line(amount) {
    console.log("CHECKING MULTILINES...: " + amount);
    var check = 1;
    // CHECK DATA
    if ($("#end_at").val().length !== 0) {
        check = 1;
    }
    else {
        check = 0;
    }

    // CHECK PRODUCTS
    var checking = 0;
    while (checking < amount) {
        checking++;
        console.log("CHECKING: " + checking + " | " + check);
        if (check == 1) {
            console.log("#quantity_" + checking + " : " + $("#quantity_" + checking).val());
            if ($("#quantity_" + checking).val().length > 0) {
                check = 1;
            }
            else {
                check = 0;
            }
        }

    }

    // FINISHED CHECK;
    if (check == 1) {
        $("button[name='next_action']").prop('disabled', false);
    }
    else {
        $("button[name='next_action']").prop('disabled', true);
    }
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function getdate(date, offset) {
    var pickervalue = date;

    var datepicker = pickervalue.split(".");
    var joindate = new Date();
    var numberOfDaysToAdd = offset;

    joindate.setFullYear(parseInt(datepicker[2]), parseInt(datepicker[1]) - 1, parseInt(datepicker[0]) + numberOfDaysToAdd);

    var dateObj = joindate
    //var seconds = addZero(dateObj.getSeconds());
    var minute = addZero(dateObj.getMinutes());
    var hour = addZero(dateObj.getHours());
    var month = addZero(dateObj.getUTCMonth() + 1); //months from 1-12
    var day = addZero(dateObj.getUTCDate());
    var year = dateObj.getUTCFullYear();
    //console.log(dateObj);
    newdate = day + "." + month + "." + year + " " + hour + ":" + minute;
    //console.log(newdate);
    return newdate;
}

$(document).ready(function () {
    $("div.panel.panel-default").css('font-size', '12px');
    // LIMPAR 
    $("button[name='next_action']").prop('disabled', true);
    $("input[name='urgency_date']").prop('disabled', true);

    $('.new_quantity').remove();
    $('.new_obs').remove();
    $('input#group_quantity').val(1);

    $("select[id='product_id']").parent().attr("id", "product_selection");

    $("input[id='custom_id']").attr('type', 'hidden');

    $("input[id='quantity']").attr('type', 'hidden');
    $("input[id='carpintaria']").attr('type', 'hidden');
    $("input[id='woodfinishing']").attr('type', 'hidden');
    $("input[id='pintura']").attr('type', 'hidden');
    $("input[id='montagem']").attr('type', 'hidden');
    $("input[id='obs']").attr('type', 'hidden');

    $("<input type='button' name='validate_multi' class='btn btn-danger' style='margin-right: 10px;' value='Validar Encomenda'/>").insertBefore("button[name='next_action']");

//    $("select[id='product_id']").parent().clone().attr({
//        id: 'product_id_' + 1,
//        name: 'new_product_id[]',
//        class: 'form-control new_product_id'
//    }).appendTo("label[for='product_id']").css('width', '100%');


    // CLONE SELECT
//    $("div[id='product_selection']").clone().attr({
//        id: 'product_div_id_' + 1,
//    }).appendTo("label[for='product_id']").css('width', '100%');
//    $("div[id='product_div_id_" + 1 + "'] > select").attr("id", 'product_id_' + 1).attr("name", 'product_id_' + 1).addClass("new_product_id");
//    $("div[id='product_div_id_" + 1 + "'] > span > span > span").attr("aria-labelledby", 'select2-product_id_' + 1 + '-container');
//    $("div[id='product_div_id_" + 1 + "'] > span > span > span > span").attr("id", 'select2-product_id_' + 1 + '-container');

    $('<select name="product_id_selector[]" id="product_div_id_' + 1 + '" class="form-control new_product_id">').insertAfter("div[id='product_selection']");
    var $options = $("#product_id > option").clone();
    $('#product_div_id_' + 1).append($('<option>', {
        value: 0,
        text: 'Escolha um Produto',
    })).append($options);
    $("label[for='product_id']").css('margin-bottom', '0px');

    // HIDE DEFAULT SELECT
    $("div[id='product_selection']").hide();

    $('<select>').attr({
        id: 'custom_id_' + 1,
        name: 'new_custom_id[]',
        class: 'form-control new_custom_id',
    }).prop('disabled', 'disabled').appendTo("label[for='custom_id']").css('width', '100%');

    $('<input>').attr({
        type: 'number',
        id: 'quantity_' + 1,
        name: 'new_quantity[]',
        class: 'form-control new_quantity',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');

    $('<input>').attr({
        type: 'number',
        id: 'carpintaria_' + 1,
        name: 'new_carpintaria[]',
        class: 'form-control new_carpintaria',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='carpintaria']").css('width', '100%');

    $('<input>').attr({
        type: 'number',
        id: 'woodfinishing_' + 1,
        name: 'new_woodfinishing[]',
        class: 'form-control new_woodfinishing',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='woodfinishing']").css('width', '100%');


    $('<input>').attr({
        type: 'number',
        id: 'pintura_' + 1,
        name: 'pintura[]',
        class: 'form-control new_pintura',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='pintura']").css('width', '100%');

    $('<input>').attr({
        type: 'number',
        id: 'montagem_' + 1,
        name: 'montagem[]',
        class: 'form-control new_montagema',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='montagem']").css('width', '100%');

    $('<input>').attr({
        type: 'text',
        id: 'obs_' + 1,
        name: 'observations[]',
        class: 'form-control new_obs',
    }).appendTo("label[for='obs']").css('width', '100%');

    $("label[for='obs']").css('width', '100%');

    // INSERIR A DROPDOWN depois do NEW ENTRY
    $('<select id="mySelect" class="btn bg-gray" style="margin-left: 20px;">').insertAfter('.panel-heading > a.btn');
    $('#mySelect')
            .append($('<option>', {
                value: 0,
                text: 'Em falta'
            }))
            .append($('<option>', {
                value: 1,
                text: 'Pendentes'
            }))
            .append($('<option>', {
                value: 2,
                text: 'Satisfeitas'
            }))
            .append($('<option>', {
                value: 'T',
                text: 'Todas as encomendas'
            }));

    if (getUrlVars()["status"]) {
        $('#mySelect').val(getUrlVars()["status"]);
    }
    else {
        $('#mySelect').val(0);
    }
    $('#mySelect').on('change', function () {
        var selectedValue = $(this).val();
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/request_weeks?status=' + selectedValue
    });

// MULTI
// DETECTAR O NUMERO DE PRODUTOS
    $('input#group_quantity').bind("keyup input click", function () {
        var numItems = $('.new_quantity').length
        var missingItems = $('input#group_quantity').val() - numItems
        var start_check = $('input#group_quantity').val() - missingItems;
        var check = 0;

        // SE NAO EXISTE (nao deixa ser 0)
        if (!$('input#group_quantity').val() || $('input#group_quantity').val() < 1) {
            $('input#group_quantity').val(1);
        }
        // SE > 1
        else {


            //console.log(numItems + " / " + $('input#group_quantity').val() + " \ " + missingItems);
            var check_delete = numItems
            if ($('input#group_quantity').val() < numItems) {
                while (check_delete > $('input#group_quantity').val()) {
                    $('select#product_id_' + check_delete).remove();
                    $('select#custom_id_' + check_delete).remove();
                    $('input#quantity_' + check_delete).remove();
                    $('input#carpintaria_' + check_delete).remove();
                    $('input#woodfinishing_' + check_delete).remove();
                    $('input#pintura_' + check_delete).remove();
                    $('input#montagem_' + check_delete).remove();
                    $('input#obs_' + check_delete).remove();
                    $("select[id='product_div_id_" + check_delete + "']").remove();
                    check_delete = check_delete - 1;
                }
            }
        }
        // LOOP PARA OS PRODUTOS        
        while (check < missingItems) {
            check = check + 1;
            //console.log("[numItems] " + numItems + "[missingItems] " + missingItems);
            var updatedItems = document.getElementsByClassName("new_quantity").length;
            var new_check = updatedItems + 1;
            //var old_check = start_check - 1;
            //console.log("missingItems > 1 [CHECK] " + check + " [OLD] " + old_check + " [START] " + start_check + " [UPDATED] " + updatedItems);
            //new_check = start_check + 1;
            //var insert_check = old_check + 1;
            var insert_where = "select[id='product_div_id_" + updatedItems + "']";
            //var numItems = $('.new_product_id').length;
            //var insert_where = "select[id='product_div_id_" + numItems;
            $('<select name="product_id_selector[]" id="product_div_id_' + new_check + '" class="form-control new_product_id">').insertAfter(insert_where);
            var $options = $("#product_id > option").clone();
            $('#product_div_id_' + new_check).append($('<option>', {
                value: 0,
                text: 'Escolha um Produto',
            })).append($options);
            $("select[id='product_div_id_" + new_check + "']").show();

            // console.log(this.value);
            $('<select>').attr({
                id: 'custom_id_' + new_check,
                name: 'new_custom_id[]',
                class: 'form-control new_custom_id',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='custom_id']").css('width', '100%');

            $('<input>').attr({
                type: 'number',
                id: 'quantity_' + new_check,
                name: 'new_quantity[]',
                class: 'form-control new_quantity',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');

            $('<input>').attr({
                type: 'number',
                id: 'carpintaria_' + new_check,
                name: 'new_carpintaria[]',
                class: 'form-control new_carpintaria',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='carpintaria']").css('width', '100%');

            $('<input>').attr({
                type: 'number',
                id: 'woodfinishing_' + new_check,
                name: 'new_woodfinishing[]',
                class: 'form-control new_woodfinishing',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='woodfinishing']").css('width', '100%');

            $('<input>').attr({
                type: 'number',
                id: 'pintura_' + new_check,
                name: 'pintura[]',
                class: 'form-control new_pintura',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='pintura']").css('width', '100%');

            $('<input>').attr({
                type: 'number',
                id: 'montagem_' + new_check,
                name: 'montagem[]',
                class: 'form-control new_montagem',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='montagem']").css('width', '100%');

            $('<input>').attr({
                type: 'text',
                id: 'obs_' + new_check,
                name: 'observations[]',
                class: 'form-control new_obs',
            }).appendTo("label[for='obs']").css('width', '100%');
        }
    });



    // MUDAR O PRODUTO
    $('#product_id').val(0);

    $('#custom_id').prop('disabled', true);
    $('#quantity').prop('disabled', true);
    $('#carpintaria').prop('readonly', true);
    $('#woodfinishing').prop('readonly', true);
    $('#pintura').prop('readonly', true);
    $('#montagem').prop('readonly', true);

    $(document).on('change', '.new_product_id', function () {
        if (this.value != 0) {
//    $('.new_product_id').delegate("select", "click", function (e) {
            var line_id = this.id.split('product_div_id_');            
            line_id = line_id[1];
            //console.log(line_id)
            $('#custom_id_' + line_id).empty();
            $('#quantity_' + line_id).val("");
            $('#carpintaria_' + line_id).val("");
            $('#woodfinishing_' + line_id).val("");
            $('#pintura_' + line_id).val("");
            $('#montagem_' + line_id).val("");
            $('#custom_id_' + line_id).prop('disabled', true);
            $.ajax({
                url: './get_product/' + this.value,
                success: function (data) {
                    $('#custom_id_' + line_id).prop('disabled', false);
                    console.log(data.custom);
                    if (jQuery.isEmptyObject(data.custom)) {
                        $('#custom_id_'+ line_id).append('<option value="1">Por Definir</option>');
                    }
                    else {
                        $.each(data.custom, function (i, value) {
  
                            $('#custom_id_'+ line_id).append('<option value=' + i + '>' + value + '</option>');
                        });    
                    }                    

                    $('#quantity_' + line_id).prop('disabled', false);
//                    $("#quantity_" + line_id).keyup(function () {
                    $("#quantity_" + line_id).bind("keyup input click", function () {
                        if ($("#quantity_" + line_id).val() <= 0) {
                            $("#quantity_" + line_id).val("");
                            $("button[name='next_action']").prop('disabled', true);
                        }                        
                        carpintaria_temp = data.carpintaria_req - data.carpintaria;
                        if (carpintaria_temp > 0) {
                            carpintaria = $('#quantity_' + line_id).val()
                        }
                        else {
                            carpintaria = $('#quantity_' + line_id).val() - data.carpintaria;
                            if (carpintaria < 0) {
                                carpintaria = 0;
                            }
                        }

                        woodfinishing_temp = data.acabamento_req - data.acabamento;
                        if (woodfinishing_temp > 0) {
                            woodfinishing = $('#quantity_' + line_id).val()
                        }
                        else {
                            woodfinishing = $('#quantity_' + line_id).val() - data.acabamento;
                            if (woodfinishing < 0) {
                                woodfinishing = 0;
                            }
                        }

                        pintura_temp = data.pintura_req - data.pintura;
                        if (pintura_temp > 0) {
                            pintura = $('#quantity_' + line_id).val()
                        }
                        else {
                            pintura = $('#quantity_' + line_id).val() - data.pintura;
                            if (pintura < 0) {
                                pintura = 0;
                            }
                        }

                        montagem_temp = data.montagem_req - data.montagem;
                        if (montagem_temp > 0) {
                            montagem = $('#quantity_' + line_id).val()
                        }
                        else {
                            montagem = $('#quantity_' + line_id).val() - data.montagem;
                            if (montagem < 0) {
                                montagem = 0;
                            }
                        }

                        montagem = $('#quantity_' + line_id).val();

                        $("#carpintaria_" + line_id).val(carpintaria);
                        $("#woodfinishing_" + line_id).val(woodfinishing);
                        $("#pintura_" + line_id).val(pintura);
                        $("#montagem_" + line_id).val(montagem);
                    });
                }
            });
        }
    });

    $("input[name='validate_multi']").click(function (e) {
        check_multi_line($('.new_quantity').length);
    });

    $("button[name='next_action']").click(function (e) {
        $("#carpintaria_" + line_id).val($('#quantity_' + line_id).val());
        $("#woodfinishing_" + line_id).val($('#quantity_' + line_id).val());
        $("#pintura_" + line_id).val($('#quantity_' + line_id).val());
        $("#montagem_" + line_id).val($('#quantity_' + line_id).val());

        $("form").submit();

    });

    // CHECKED URGENCY
    $('input[name=urgency]').click(function() {
        if($(this).is(':checked')) {
            $("input[name='urgency_date']").prop('disabled', false);
        } else {
            $("input[name='urgency_date']").prop('disabled', true);
        }
    });     
     
     
    // DATAS
    $('input#end_at').bind("change keyup input blur focus", function () {
        if ($('input#end_at').val()) {
            $("#end_paint_at").val(getdate($("#end_at").val(), -4));
            $("#end_woodfinishing_at").val(getdate($("#end_at").val(), -7));
            $("#end_wood_at").val(getdate($("#end_at").val(), -10));
        }
        else {
            $("#end_paint_at").val("");
            $("#end_woodfinishing_at").val("");
            $("#end_wood_at").val("");
            $("button[name='next_action']").prop('disabled', true);
        }
    });

    var myURL2 = parseUri(window.location.href);
    if (myURL2['href'] != myURL2['href'].split("/admin")[0] + '/admin/request_weeks') {
        $('form').css('marginBottom', '80px');
    }
    $('.form-element-required').css('vertical-align', 'baseline');
});
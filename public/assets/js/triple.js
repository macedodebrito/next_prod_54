function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function getdate(date, offset) {
    var pickervalue = date;

    var datepicker = pickervalue.split(".");
    var joindate = new Date();
    var numberOfDaysToAdd = offset;

    joindate.setFullYear(parseInt(datepicker[2]), parseInt(datepicker[1]) - 1, parseInt(datepicker[0]) + numberOfDaysToAdd);

    var dateObj = joindate
    var minute = addZero(dateObj.getMinutes());
    var hour = addZero(dateObj.getHours());
    var month = addZero(dateObj.getUTCMonth() + 1); //months from 1-12
    var day = addZero(dateObj.getUTCDate());
    var year = dateObj.getUTCFullYear();
    console.log(dateObj);
    newdate = day + "." + month + "." + year + " " + hour + ":" + minute;
    return newdate;
}

$(document).ready(function () {
    
    $("div[id='btn-refresh']").click(function (e) {
        $.ajax({
            url: './new_triple',
            success: function (data) {
                location.reload();
            },
            beforeSend: function () {
                $('#waiting_freeze').show();
            },
        });
    });

    $("div[id='btn-triple']").click(function (e) {
        $.ajax({
            url: './new_triple_week',
           success: function (data) {
                location.reload();
            },
            beforeSend: function () {
                $('#waiting_freeze').show();
            },
        });
    });
        $("div[id='btn-edit-one']").click(function (e) {
        $.ajax({
            url: './new_triple_week_up',
           success: function (data) {
                location.reload();
            },
            beforeSend: function () {
                $('#waiting_freeze').show();
            },
        });
    });
    
    
});
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function getdate(date, offset) {
    var pickervalue = date;

    var datepicker = pickervalue.split(".");
    var joindate = new Date();
    var numberOfDaysToAdd = offset;

    joindate.setFullYear(parseInt(datepicker[2]), parseInt(datepicker[1]) - 1, parseInt(datepicker[0]) + numberOfDaysToAdd);

    var dateObj = joindate
    //var seconds = addZero(dateObj.getSeconds());
    var minute = addZero(dateObj.getMinutes());
    var hour = addZero(dateObj.getHours());
    var month = addZero(dateObj.getUTCMonth() + 1); //months from 1-12
    var day = addZero(dateObj.getUTCDate());
    var year = dateObj.getUTCFullYear();
    //console.log(dateObj);
    newdate = day + "." + month + "." + year + " " + hour + ":" + minute;
    console.log(newdate);
    return newdate;
}

$(document).ready(function () {
    // INSERIR A DROPDOWN depois do NEW ENTRY
    $('<select id="mySelect" class="btn bg-gray" style="margin-left: 20px;">').insertAfter('.panel-heading > a.btn');
    $('#mySelect')
            .append($('<option>', {
                value: 0,
                text: 'Em falta'
            }))
            .append($('<option>', {
                value: 1,
                text: 'Pendentes'
            }))
            .append($('<option>', {
                value: 2,
                text: 'Satisfeitas'
            }))
            .append($('<option>', {
                value: 'T',
                text: 'Todas as encomendas'
            }));

    if (getUrlVars()["status"]) {
        $('#mySelect').val(getUrlVars()["status"]);
    }
    else {
        $('#mySelect').val(0);
    }
    $('#mySelect').on('change', function () {
        var selectedValue = $(this).val();
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/request_weeks?status=' + selectedValue
    });


    // MUDAR O PRODUTO
    $('#product_id').val(0);

    $('#quantity').prop('disabled', true);
    $('#carpintaria').prop('readonly', true);
    $('#pintura').prop('readonly', true);
    $('#montagem').prop('readonly', true);

    $('#product_id').on('change', function () {
        $('#quantity').val("");
        $('#carpintaria').val("");
        $('#pintura').val("");
        $('#montagem').val("");
        $.ajax({
            url: './get_product/' + this.value,
            success: function (data) {
                $('#quantity').prop('disabled', false);
                $("#quantity").keyup(function () {
                    carpintaria_temp = data.carpintaria_req - data.carpintaria;
                    if (carpintaria_temp > 0) {
                        carpintaria = $('#quantity').val()
                    }
                    else {
                        carpintaria = $('#quantity').val() - data.carpintaria;
                        if (carpintaria < 0) {
                            carpintaria = 0;
                        }
                    }

                    pintura_temp = data.pintura_req - data.pintura;
                    if (pintura_temp > 0) {
                        pintura = $('#quantity').val()
                    }
                    else {
                        pintura = $('#quantity').val() - data.pintura;
                        if (pintura < 0) {
                            pintura = 0;
                        }
                    }

                    montagem_temp = data.montagem_req - data.montagem;
                    if (montagem_temp > 0) {
                        montagem = $('#quantity').val()
                    }
                    else {
                        montagem = $('#quantity').val() - data.montagem;
                        if (montagem < 0) {
                            montagem = 0;
                        }
                    }

                    montagem = $('#quantity').val();

                    $("#carpintaria").val(carpintaria);
                    $("#pintura").val(pintura);
                    $("#montagem").val(montagem);
                });
            }
        });

    });


    $("button[name='next_action']").click(function (e) {
        $("#carpintaria").val($('#quantity').val());
        $("#pintura").val($('#quantity').val());
        $("#montagem").val($('#quantity').val());

        $("form").submit();

    });

    $('input#end_at').bind("change keyup input blur focus", function () {
        $("#end_paint_at").val(getdate($("#end_at").val(), -3));
        $("#end_wood_at").val(getdate($("#end_at").val(), -7));
    });

    var myURL2 = parseUri(window.location.href);
    if (myURL2['href'] != myURL2['href'].split("/admin")[0] + '/admin/request_weeks') {
        $('form').css('marginBottom', '80px');
    }
});
$(document).ready(function () {
    // TEM BUG
        $.ajax({
            url: '../get_pp_edit/' + $('#request_id').val(),
            success: function (data) {
                $('#value')
                        .find('option')
                        .remove()
                        .end();
                var check = 0;
                while (check < data.id) {
                    check = check + 1;
                    $('#value')
                            .append($('<option>', {
                                value: check,
                                text: check
                            }));
                }
            }
        });
    $("button[name='next_action']").prop('disabled', true);
    //
    
    $('#request_id').on('change', function () {
        $.ajax({
            url: '../get_pp/' + this.value,
            success: function (data) {
                $('#value')
                        .find('option')
                        .remove()
                        .end();
                var check = 0;
                while (check < data.id) {
                    check = check + 1;
                    $('#value')
                            .append($('<option>', {
                                value: check,
                                text: check
                            }));
                }
            }
        });

    });
    
    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
                    $("button[name='next_action']").prop('disabled', false); 
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                }
            }
        });

    }, 1000);

    $('.picks_code').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#picks_code").val(),
            success: function (data) {
                if (data.id != 0) {
                    $(".picks").show();
                    $(".picks_code").hide();
                    $("#picks_code").val(data.id);
                }
                else {
                    $(".picks").hide();
                    $(".picks_code").show();
                }
            }
        });

    }, 1000);
    
    $("button[name='next_action']").click(function (e) {
        $("#published").val("0");
        $("form").submit();

    });    

});


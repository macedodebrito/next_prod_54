$(document).ready(function () {
    // TEM BUG
    $('#request_id').val(0);
    $("button[name='next_action']").prop('disabled', true);
    //

    $('#request_id').on('change', function () {
        $.ajax({
            url: './get_pp/' + this.value,
            success: function (data) {
                $('#value')
                        .find('option')
                        .remove()
                        .end();
                var check = 0;
                while (check < data.id) {
                    check = check + 1;
                    $('#value')
                            .append($('<option>', {
                                value: check,
                                text: check
                            }));
                }
            }
        });

    });

    $(document).on('click', 'a.accept_product', function (e) {
        if ($(this).attr("href") == "#removing") {
            var status = -1;
        }
        else if ($(this).attr("href") == "#remove_product") {
            var status = 0;
        }
        else if ($(this).attr("href") == "#accept_product") {
            var status = 1;
        }
        else if ($(this).attr("href") == "#removing_fast") {
            var status = -2;
        }
        if (status < 0) {

            $.ajax({
                url: './cnc_status/' + this.id + '/' + status,
                success: function (data) {
                    location.reload();
                }
            });
        }
        else {
            $.ajax({
                url: './cnc_status/' + this.id + '/' + status + '/' + $("#picks_code").val(),
                success: function (data) {
                    location.reload();
                }
            });
        }

    });

    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
                    $("button[name='next_action']").prop('disabled', false);
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                }
            }
        });

    }, 1000);

    $('.picks_code').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#picks_code").val(),
            success: function (data) {
                if (data.id != 0) {
                    $(".picks_code").hide();
                    $(".picks").show();
                    $("#picks_code").val(data.id);
                }
                else {
                    $(".picks").hide();
                    $(".picks_code").show();
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
        e.preventDefault();
        $("#published").val("1");
        $("form").submit();
    });
});

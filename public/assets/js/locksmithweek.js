function check_multi_line(amount) {
    console.log("CHECKING MULTILINES...: " + amount);
    var check = 1;
    // CHECK DATA
    if ($("#end_at").val().length !== 0) {
        check = 1;
    }
    else {
        check = 0;
    }

    // CHECK PRODUCTS
    var checking = 0;
    while (checking < amount) {
        checking++;
        console.log("CHECKING: " + checking + " | " + check);
        if (check == 1) {
            console.log("#quantity_" + checking + " : " + $("#quantity_" + checking).val());
            if ($("#quantity_" + checking).val() > 0) {
                check = 1;
            }
            else {
                check = 0;
            }
        }

    }

    // FINISHED CHECK;
    if (check == 1) {
        $("button[name='next_action']").prop('disabled', false);
    }
    else {
        $("button[name='next_action']").prop('disabled', true);
    }
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

function getdate(date, offset) {
    var pickervalue = date;

    var datepicker = pickervalue.split(".");
    var joindate = new Date();
    var numberOfDaysToAdd = offset;

    joindate.setFullYear(parseInt(datepicker[2]), parseInt(datepicker[1]) - 1, parseInt(datepicker[0]) + numberOfDaysToAdd);

    var dateObj = joindate
    //var seconds = addZero(dateObj.getSeconds());
    var minute = addZero(dateObj.getMinutes());
    var hour = addZero(dateObj.getHours());
    var month = addZero(dateObj.getUTCMonth() + 1); //months from 1-12
    var day = addZero(dateObj.getUTCDate());
    var year = dateObj.getUTCFullYear();
    //console.log(dateObj);
    newdate = day + "." + month + "." + year + " " + hour + ":" + minute;
    //console.log(newdate);
    return newdate;
}

$(document).ready(function () {

    // LIMPAR 
    $("button[name='next_action']").prop('disabled', true);

    $('.new_quantity').remove();
    $('.new_obs').remove();
    $('input#group_quantity').val(1);

    $("select[id='locksmith_id']").parent().attr("id", "locksmith_selection");

    $("input[id='quantity']").attr('type', 'hidden');
    $("input[id='obs']").attr('type', 'hidden');

    $("<input type='button' name='validate_multi' class='btn btn-danger' style='margin-right: 10px;' value='Validar Encomenda'/>").insertBefore("button[name='next_action']");

    $('<select name="locksmith_id_selector[]" id="locksmith_div_id_' + 1 + '" class="form-control new_locksmith_id">').insertAfter("div[id='locksmith_selection']");

    var $options = $("#locksmith_id > option").clone();

    $('#locksmith_div_id_' + 1).append($('<option>', {
        value: 0,
        text: 'Escolha um Produto',
    })).append($options);
    $("label[for='locksmith_id']").css('margin-bottom', '0px');

    // HIDE DEFAULT SELECT
    $("div[id='locksmith_selection']").hide();

    $('<input>').attr({
        type: 'number',
        id: 'quantity_' + 1,
        name: 'new_quantity[]',
        class: 'form-control new_quantity',
        maxLength: 3
    }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');
    
//    $('#quantity_' + 1).append($('<option>', {
//        value: 0,
//        text: 'Quantidade',
//    }));

//    $('<select>').attr({
//        id: 'quantity_' + 1,
//        name: 'new_quantity[]',
//        class: 'form-control new_quantity',
//    }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');

    $('<input>').attr({
        type: 'text',
        id: 'obs_' + 1,
        name: 'observations[]',
        class: 'form-control new_obs',
    }).appendTo("label[for='obs']").css('width', '100%');

    $("label[for='obs']").css('width', '100%');

    // INSERIR A DROPDOWN depois do NEW ENTRY
    $('<select id="mySelect" class="btn bg-gray" style="margin-left: 20px;">').insertAfter('.panel-heading > a.btn');
    $('#mySelect')
            .append($('<option>', {
                value: 0,
                text: 'Em falta'
            }))
            .append($('<option>', {
                value: 1,
                text: 'Pendentes'
            }))
            .append($('<option>', {
                value: 2,
                text: 'Satisfeitas'
            }))
            .append($('<option>', {
                value: 'T',
                text: 'Todas as encomendas'
            }));

    if (getUrlVars()["status"]) {
        $('#mySelect').val(getUrlVars()["status"]);
    }
    else {
        $('#mySelect').val(0);
    }
    $('#mySelect').on('change', function () {
        var selectedValue = $(this).val();
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/locksmith_weeks?status=' + selectedValue
    });

// MULTI
// DETECTAR O NUMERO DE PRODUTOS
    $('input#group_quantity').bind("keyup input click", function () {
        var numItems = $('.new_quantity').length
        var missingItems = $('input#group_quantity').val() - numItems
        var start_check = $('input#group_quantity').val() - missingItems;
        var check = 0;
        // SE NAO EXISTE (nao deixa ser 0)
        if (!$('input#group_quantity').val() || $('input#group_quantity').val() < 1) {
            $('input#group_quantity').val(1);
        }
        // SE > 1
        else {
            console.log(numItems + " / " + $('input#group_quantity').val() + " \ " + missingItems);
            var check_delete = numItems
            if ($('input#group_quantity').val() < numItems) {
                while (check_delete > $('input#group_quantity').val()) {
                    $('select#locksmith_id_' + check_delete).remove();
                    $('input#quantity_' + check_delete).remove();
                    $('input#obs_' + check_delete).remove();
                    $("select[id='locksmith_div_id_" + check_delete + "']").remove();
                    check_delete = check_delete - 1;
                }
            }
        }
        // LOOP PARA OS PRODUTOS        
        while (check < missingItems) {
            check = check + 1;
            //console.log("[numItems] " + numItems + "[missingItems] " + missingItems);
            var updatedItems = document.getElementsByClassName("new_quantity").length;
            var new_check = updatedItems + 1;
            //var old_check = start_check - 1;
            //console.log("missingItems > 1 [CHECK] " + check + " [OLD] " + old_check + " [START] " + start_check + " [UPDATED] " + updatedItems);
            //new_check = start_check + 1;
            //var insert_check = old_check + 1;
            var insert_where = "select[id='locksmith_div_id_" + updatedItems + "']";
            //var numItems = $('.new_product_id').length;
            //var insert_where = "select[id='product_div_id_" + numItems;
            $('<select name="locksmith_id_selector[]" id="locksmith_div_id_' + new_check + '" class="form-control new_locksmith_id">').insertAfter(insert_where);
            var $options = $("#locksmith_id > option").clone();
            $('#locksmith_div_id_' + new_check).append($('<option>', {
                value: 0,
                text: 'Escolha um Produto',
            })).append($options);
            $("select[id='locksmith_div_id_" + new_check + "']").show();

            console.log(this.value);
            $('<input>').attr({
                type: 'number',
                id: 'quantity_' + new_check,
                name: 'new_quantity[]',
                class: 'form-control new_quantity',
                maxLength: 3
            }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');
//            $('<select>').attr({
//                id: 'quantity_' + new_check,
//                name: 'new_quantity[]',
//                class: 'form-control new_quantity',
//            }).prop('disabled', 'disabled').appendTo("label[for='quantity']").css('width', '100%');

            $('<input>').attr({
                type: 'text',
                id: 'obs_' + new_check,
                name: 'observations[]',
                class: 'form-control new_obs',
            }).appendTo("label[for='obs']").css('width', '100%');
        }
    });



    // MUDAR O PRODUTO
    $('#locksmith_id').val(0);

    $('#quantity').prop('disabled', true);

    $(document).on('click', '.new_locksmith_id', function () {
        if (this.value != 0) {
//    $('.new_product_id').delegate("select", "click", function (e) {
            var line_id = this.id.split('locksmith_div_id_');
            line_id = line_id[1];
            $('#quantity_' + line_id).val("");
            $.ajax({
                url: './get_locksmith/' + this.value,
                success: function (data) {
                    $('select#quantity_' + line_id)
                            .find('option')
                            .remove()
                            .end();
                    $('#quantity_' + line_id).prop('disabled', false);
//                    $("#quantity_" + line_id).keyup(function () {
//                    $("#quantity_" + line_id).bind("keyup select click", function () {
//                        locksmith_temp = data.locksmith_req - data.locksmith;
//                        if (locksmith_temp > 0) {
//                            locksmith = $('#quantity_' + line_id).val()
//                        }
//                        else {
//                            locksmith = $('#quantity_' + line_id).val() - data.locksmith;
//                            if (locksmith < 0) {
//                                locksmith = 0;
//                            }
//                        }
//                        locksmith = $('#quantity_' + line_id).val();
//                    });
//                    var tmp = 0;
//                    while (tmp <= data.locksmith) {
//                        $('#quantity_' + line_id).append($('<option>', {
//                            value: tmp,
//                            text: tmp,
//                        }));
//                        tmp = tmp + 1;
//                    }
                }
            });
        }
    });

    $("input[name='validate_multi']").click(function (e) {
        check_multi_line($('.new_quantity').length);
    });

    $("button[name='next_action']").click(function (e) {
        $("#locksmith_" + line_id).val($('#quantity_' + line_id).val());

        $("form").submit();

    });


    var myURL2 = parseUri(window.location.href);
    if (myURL2['href'] != myURL2['href'].split("/admin")[0] + '/admin/locksmith_weeks') {
        $('form').css('marginBottom', '80px');
    }
    $('.form-element-required').css('vertical-align', 'baseline');
});
$(document).ready(function () {
    // TEM BUG
    //$('#product_id').val(0);
    $("button[name='next_action']").prop('disabled', true);
    //

    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    console.log($("#product_id").val())
                    console.log($("#value").val())
                    if (($("#product_id").val() > 0) && ($("#value").val() > 0)) {
                        $("input[name='user_id']").val(data.id);
                        $("button[name='next_action']").prop('disabled', false);
                    }
                    else {
                        $("button[name='next_action']").prop('disabled', true);
                        $("input[name='user_id']").val("");
                        $("#published").val("");
                    }
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
        $("#published").val("0");
        $("form").submit();
    });

});


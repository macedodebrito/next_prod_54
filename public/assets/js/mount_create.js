$(document).ready(function () {
    // MUDAR O PP

    // TEM BUG
    $('#request_id').val(0);
    $("button[name='next_action']").prop('disabled', true);
    $("#request_id").prop('disabled', true);
    $("#quantity").prop('disabled', true);

    $('#request_id').on('change', function () {
        $.ajax({
            url: './get_pp/' + this.value,
            success: function (data) {
                $('.new_serial').remove();
                $('.new_obs').remove();
                $('#quantity')
                        .find('option')
                        .remove()
                        .end();
                var check = 0;
//                console.log(data);
//                console.log('1LAST SERIAL: ' + data.serial);
                if (data.serial == null) {
                    var last_serial = 0;
                }
                else {
                    var last_serial = data.serial;
                }
                //var last_serial = 097;

                $('input[name=last_serial]').val(last_serial);
                while (check < data.id) {
                    check = check + 1;
                    $('#quantity')
                            .append($('<option>', {
                                value: check,
                                text: check
                            }));
                }
            }
        });

    });

    // MUDAR A QUANTIDADE

    $('#quantity').on('change', function () {
        $('input[name=value]').val(this.value);
        var check = 0;
//        console.log('LAST SERIAL: ' + $('#last_serial').val());
        $("input[id='serial']").attr('type', 'hidden').val("XXXYYYZZZ69");
        $("input[id='obs']").attr('type', 'hidden');

        $('.new_serial').remove();
        $('.new_obs').remove();
        while (check < this.value) {
            check = check + 1;
            go_value = check + parseInt($('input[name=last_serial]').val());

            if (go_value < 10) {
                var go_value = "00" + go_value;
            } else if (go_value > 9 && go_value < 100) {
                var go_value = "0" + go_value;
            }
            else {
                var go_value = go_value;
            }

//            console.log(this.value);
            $('<input>').attr({
                type: 'text',
                id: 'serial_' + check,
                name: 'serials[]',
                class: 'form-control new_serial',
                value: go_value,
                maxLength: 3
            }).appendTo("label[for='serial']").css('width', '100%');

            $('<input>').attr({
                type: 'text',
                id: 'obs_' + check,
                name: 'observations[]',
                class: 'form-control new_obs'
            }).appendTo("label[for='obs']");
        }

    });

//    $("button[name='next_action']").click(function (e) {
//        $("form").submit();
//    });

    $(document).on('keyup', '.new_serial', function () {
        temp_serial = $(this).val()
        temp_id = this.id
        $(".new_serial").each(function () {
            if (temp_id != this.id) {
                if (temp_serial == $(this).val()) {
                    $(this).val("")
                }
            }
            else {
                if (!$.isNumeric($(this).val())) {
                    $(this).val("")
                    $("button[name='next_action'").prop('disabled', true);
                }
                if ($(this).val().toString().length < 3) {
                    $("button[name='next_action'").prop('disabled', true);
                }
                else {
                    $.ajax({
                        url: './check_serial/' + $("#request_id").val() + "/" + $(this).val(),
                        success: function (data) {
                            if (data > 0) {
                                $("#" + temp_id).val("")
                                $("button[name='next_action'").prop('disabled', true);
                            }
                            else {
                                $("button[name='next_action'").prop('disabled', false);
                            }
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', 'a.accept_product', function (e) {
        if ($(this).attr("href") == "#removing") {
            var status = -1;
        }
        if ($(this).attr("href") == "#removing_fast") {
            var status = -2;
        }
        $.ajax({
            url: './stock_status/' + this.id + '/' + status,
            success: function (data) {
                location.reload();
            }
        });
    });

    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
                    $("#request_id").prop('disabled', false);
                    $("#quantity").prop('disabled', false);
                    $("button[name='next_action']").prop('disabled', false);
                    $(".new_serial").prop('disabled', false);
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                    $(".new_serial").prop('disabled', true);
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
        $("#published").val("0");
        $("form").submit();

    });

    // 
    $(document).on('click', "input[name='picker_version']", function () {
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/mount/history_picker/create';
    });

// ADICIONAR "VERSÃO PICKER"
    $("<input type='button' name='picker_version' class='btn btn-info' style='float: right; margin-top: -30px;' value='Alterar para Leitura de Código de Barras'/>").insertAfter(".content-header > h1");
});


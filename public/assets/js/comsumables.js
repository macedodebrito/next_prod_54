$(document).ready(function () {
    $(".accept_comsumable").hide();
    
    $(document).on('click', 'a.comsumable_a', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_comsumable") {
            var status = "valid";
            var get_id = $("#id_comsumables_code_"+idPwd).val()
        }
        $.ajax({
            url: './valid_comsumables/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
                location.reload();
            }
        });
    });

    $('.comsumables_code').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        var idPwd = this.id;
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#"+idPwd).hide();
                    $(".validator_"+idPwd).show();
                    $("#id_"+idPwd).val(data.id);
                }
                else {
                    $(".accept_comsumable").hide();
                    $("#"+idPwd).show();
                    $("#id_"+idPwd).val("");
                }
            }
        });
    }, 1000);
});


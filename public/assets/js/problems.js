$(document).ready(function () {
    
    $('button[name="next_action"]').prop('disabled', true);
    $('textarea[name="text"]').prop('disabled', true);
    
    $('#auth').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#auth").val(),
            success: function (data) {
                if (data.id != 0) {
                    $('button[name="next_action"]').prop('disabled', false);                    
                    $('textarea[name="text"]').prop('disabled', false);
                    $('input[name="user"]').val(data.id);
                }
                else {
                    $('button[name="next_action"]').prop('disabled', true);
                    $('textarea[name="text"]').prop('disabled', true);
                    $('input[name="user"]').val('');
                }
            }
        });

    }, 1000);
});


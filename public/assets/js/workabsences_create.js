$(document).ready(function () {
    $("button[name='next_action']").prop('disabled', true);
    //

    $('#create_id').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#create_id").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("button[name='next_action']").attr('data-rel', data.id);
                    $("button[name='next_action']").prop('disabled', false);
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("button[name='next_action']").attr('data-rel', "");
                }
            }
        });

    }, 1000);

    $('#WorkAbsencesPassword').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#WorkAbsencesPassword").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#WorkAbsencesModal * button.btn-success").attr('data-valid', data.id);
                    $("#WorkAbsencesModal * button.btn-success").prop('disabled', false);
                }
                else {
                    $("#WorkAbsencesModal * button.btn-success").prop('disabled', true);
                    $("#WorkAbsencesModal * button.btn-success").attr('data-valid', "");
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
        e.preventDefault();
        $("#create_id").val($("button[name='next_action']").attr('data-rel'));
        $("form").submit();
    });

    $(".work_absences_validation").click(function (e) {
        $("#WorkAbsencesModal * button.btn-success").attr('data-rel', $(this).attr('data-rel'))
        $("#WorkAbsencesPassword").val('');
    });

    $('#WorkAbsencesModal').on('shown.bs.modal', function (e) {
        $(".modal-backdrop").remove();        
        $("#WorkAbsencesModal * button.btn-success").prop('disabled', true);        
      })

      $("#WorkAbsencesModal * button.btn-success").click(function (e) {
        $('#overlay_waiting').show();
        $('#WorkAbsencesModal').modal('toggle');
        $.ajax({
            // adicionar loader + fechar modal            
            url: './set_work_absences/' + $(this).attr('data-rel') + '/' + $(this).attr('data-valid'),
            success: function (data) {
                location.reload();
            }
        });
    });
});

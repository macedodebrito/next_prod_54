$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};

function on_ready() {
    $(".store_picks").hide();
    $(".tech_picks").hide();
    $(".serigrafia_picks").hide();
    $(".locksmith_picks").hide();
    $(".wires_picks").hide();
    $(".pvc_picks").hide();
    $(".packing_cnc_picks").hide();
    $(".auth_picks").hide();
    $(".packing_espuma_picks").hide();
    
    $("div.to-color-red").parent("td").addClass('bg-red');
    //$("div.to-color-green").parent("td").addClass('bg-green');
}


$(document).on('ready', function (e) {
    $('[data-toggle="tooltip"]').tooltip({html: true, trigger: 'click'});
});
 

$(document).on('click', '[data-toggle="tooltip"]', function(e) {
    $('[data-toggle="tooltip"]').tooltip({html: true, trigger: 'click'});
})


$(document).ready(function () {
    on_ready();
    //$(document).ready(function () {

    $(document).on('click', 'a.accept_auth', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = "auth";
            var get_id = $("#auth_id_picks_code_auth_" + idPwd).val()
        }
        $.ajax({
            url: './paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
//                $.ajax({
//                    url: './pdf/' + idPwd + '/palete_pdf_view',
//                    success: function (data) {
//                        //location.reload();
//                    }
//                });
                //window.location.replace('./pdf/' + idPwd + '/palete_pdf_view');
                window.open('./pdf/' + idPwd + '/palete_pdf_view', "_blank");
                location.reload();
            }
        });
    });
    
    var timeout = null;
    // FUNCTION TO CALL EVERY SECTION VALIDATOR
    $(document).on('keyup', '.picks_code_sections', function() {
        var clicked = this;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            var myURL = parseUri(window.location.href);
            var idPwd = clicked.id;
            var sectionID = $(clicked).attr("rel");
            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(clicked).val(),
                success: function (data) {
                    if (data.id != 0) {
                        $("#" + idPwd).hide();
                        $(".span_" + idPwd).show();
                        $("#input_" + idPwd).val(data.id);
                    }
                    else {
                        $(".span_" + idPwd).hide();
                        $("#" + idPwd).show();
                        $("#input_" + idPwd).val('');
                    }
                }
            });
        }, 1000);
    });

    //$(document).on('delaykeyup', '.picks_code_sections', function (e) {
//    $('.picks_code_sections').delayKeyup(function () {
//
//        var myURL = parseUri(window.location.href);
//        var idPwd = this.id;
//        var sectionID = $(this).attr("rel");
//        $.ajax({
//            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
//            success: function (data) {
//                if (data.id != 0) {
//                    $("#" + idPwd).hide();
//                    $(".span_" + idPwd).show();
//                    $("#input_" + idPwd).val(data.id);
//                }
//                else {
//                    $(".span_" + idPwd).hide();
//                    $("#" + idPwd).show();
//                    $("#input_" + idPwd).val('');
//                }
//            }
//        });
//    }, 1000);


    // FUNCTION TO CALL EVERY AUTH SECTION VALIDATOR
    $(document).on('keyup', '.picks_auth_sections', function() {
        var clicked = this;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            var myURL = parseUri(window.location.href);
            var idPwd = clicked.id;
            var sectionID = $(clicked).attr("rel");
            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(clicked).val(),
                success: function (data) {
                    if (data.id != 0) {
                        $(clicked).hide();
                        $(clicked).siblings('span').show();
                        $(clicked).siblings("span").attr('rel', data.id);
                    }
                    else {
                        $(".span_" + idPwd).hide();
                        $("#" + idPwd).show();
                        $("#input_" + idPwd).val('');
                    }
                }
            });
        }, 1000);
    });
    
//    $(document).on('keyup', ".picks_auth_sections", function () {
//        var myURL = parseUri(window.location.href);
//        var idPwd = this.id;
//        var sectionID = $(this).attr("rel");
//        var ELEMENT = this;
//        $.ajax({
//            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
//            success: function (data) {
//                if (data.id != 0) {
//                    $(ELEMENT).hide();
//                    $(ELEMENT).siblings('span').show();
//                    $(ELEMENT).siblings("span").attr('rel', data.id);
//                }
//                else {
//                    $(".span_" + idPwd).hide();
//                    $("#" + idPwd).show();
//                    $("#input_" + idPwd).val('');
//                }
//            }
//        });
//    });


    // FUNCTION TO VALIDATE ON SECTION
    $(document).on('click', 'a.first_validator', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = $(this).attr("rel");
            var get_id = $("#input_" + status + "_" + idPwd).val()
        }
        $.ajax({
            url: './paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
                location.reload();
            }
        });
    });

    // FUNCTION TO VALIDATE ON SECTION
    $(document).on('click', 'a.second_validator', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = $(this).attr("rel");
            var get_id = $(this).parent().attr('rel');

        }
        $.ajax({
            url: './paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
                location.reload();
            }
        });
    });

    $(document).on('keyup', '.picks_code_auth', function() {
        var clicked = this;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            var myURL = parseUri(window.location.href);
            var idPwd = clicked.id;
            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(clicked).val(),
                success: function (data) {
                    if (data.id != 0) {
                        $("#" + idPwd).hide();
                        $(".auth_" + idPwd).show();
                        $("#auth_id_" + idPwd).val(data.id);
                    }
                    else {
                        $(".auth_picks").hide();
                        $("#" + idPwd).show();
                    }
                }
            });
        }, 1000);
    });
//    $('.picks_code_auth').delayKeyup(function () {
//        var myURL = parseUri(window.location.href);
//        var idPwd = this.id;
//        $.ajax({
//            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
//            success: function (data) {
//                if (data.id != 0) {
//                    $("#" + idPwd).hide();
//                    $(".auth_" + idPwd).show();
//                    $("#auth_id_" + idPwd).val(data.id);
//                }
//                else {
//                    $(".auth_picks").hide();
//                    $("#" + idPwd).show();
//                }
//            }
//        });
//
//    }, 1000);

    $(document).on('click', 'a.delete_palete_history', function (e) {
        var idPwd = $(this).attr('rel');
        $("#delete_palete_history_span").text('#' + idPwd);
        $("button#delete_palete_history_yes").attr('rel', idPwd);
        $("#delete_palete_history").show();
    });

    $(document).on('click', 'button#delete_palete_history_yes', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './delete_palete_history/' + idPwd,
            success: function (data) {
                $("#delete_palete_history").hide();
                $("#delete_palete_history_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#delete_palete_history_no', function (e) {
        $("#delete_palete_history_span").text('');
        $("#delete_palete_history").hide();
    });
      
    $(document).on('click', 'a.first_obs, a.second_obs', function (e) {
        var idPwd = $(this).attr('rel');
        $("#obs_palete_span").text('#' + idPwd);
        $("button#obs_palete_yes").attr('rel', idPwd);
        $("button#obs_palete_delete").attr('rel', idPwd);
        $("input#palete_obs_input").val($(this).attr('data-rel'));
        $("#obs_palete").show();
    });

    $(document).on('click', 'button#obs_palete_yes', function (e) {
        var idPwd = $(this).attr("rel");
        var obs = encodeURIComponent($("#palete_obs_input").val());
        $.ajax({
            url: './palete_store_obs/' + idPwd + '/' + obs,
            success: function (data) {
                $("#obs_palete").hide();
                $("#obs_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#obs_palete_delete', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './palete_store_obs_delete/' + idPwd,
            success: function (data) {
                $("#obs_palete").hide();
                $("#obs_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#obs_palete_no', function (e) {
        $("#obs_palete_span").text('');
        $("#obs_palete").hide();
    });

    $(document).on('click', 'div.nxt_phc', function (e) {
        var idPwd = $(this).attr('data-rel');
        var idPwd2 = $(this).attr('data-rel');
        if (!idPwd) {
            idPwd = $(this).attr('data-value');
        }
        $("#nxt_phc_palete_span").text('#' + idPwd);
        $("#nxt_phc_default_palete_obs_input").val($(this).attr('data-value'));
        var idPwd_explode = idPwd2.split("/");
        $("#nxt_phc_palete_obs_input").val(idPwd_explode[0]);
        $("#nxt_phc_palete_obs_input2").val(idPwd_explode[1]);
        $("button#nxt_phc_palete_yes").attr('rel', $(this).attr('rel'));
        $("button#nxt_phc_palete_delete").attr('rel', $(this).attr('rel'));
        $("#nxt_phc_palete").show();
    });

    $(document).on('click', 'button#nxt_phc_palete_no', function (e) {
        $("#nxt_phc_palete_span").text('');
        $("#nxt_phc_palete").hide();
    });

    $(document).on('click', 'button#nxt_phc_palete_delete', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './nxt_phc_palete_delete/' + idPwd,
            success: function (data) {
                $("#nxt_phc_palete").hide();
                $("#nxt_phc_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#nxt_phc_palete_yes', function (e) {
        var idPwd = $(this).attr("rel");
        var obs = encodeURIComponent($("#nxt_phc_palete_obs_input").val() + '##' + $("#nxt_phc_palete_obs_input2").val());
        $.ajax({
            url: './nxt_phc_palete_yes/' + idPwd + '/' + obs,
            success: function (data) {
                $("#nxt_phc_palete").hide();
                $("#nxt_phc_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).ready(function () {
        $("#fixed-header").hide();
        // CLONAR
        $("<div style=\"display: inline; margin-left: 12px;\"><b>Legenda:</b> <span style=\"background-color: #000000; color: #FFFFFF; padding: 4px 10px;\">NXT</span>Nota de Encomenda NXT <span style=\"background-color: #FF0000; color: #FFFFFF; padding: 4px 10px;\">NSP</span> Nota de Saída de Produção</div>").insertAfter("input.form-control.input-sm[type='search']");
    });

    $(window).on('resize scroll', function () {
        if ($("#DataTables_Table_1").find("thead").isInViewport()) {
            $("#fixed-header").hide();
            $("#fixed-header > table").html("");
            $("#DataTables_Table_1").find("thead")
                    .clone()
                    .appendTo("#fixed-header > table");
        }
        else {
            $("#fixed-header").css({
                'width': $("#DataTables_Table_1").find("thead").width() + 'px',
                'height': $("#DataTables_Table_1").find("thead").height() + 'px'
            }).show();
        }
        // WIDTH

    });

    $(document).on('click', '.palete_edit_checks', function () {
    //$(".palete_edit_checks").on('click', function () {
        var title = $(this).parentsUntil("tr").parent().children("td").eq(1).text();
        $("#palete_edit_check_overlay_span").text(title);
        $("#palete_edit_check_overlay_id").val($(this).attr('data-rel'));
        $.ajax({
            url: './get_palete_update_state_admin/' + $(this).attr('data-rel'),
            success: function (data) {
                if (data.store_id != 0) {
                    $('input[name="store"]').prop('checked', true);
                }
                if (data.store_auth_id != 0) {
                    $('input[name="store_auth"]').prop('checked', true);
                }
                if (data.tech_id != 0) {
                    $('input[name="tech"]').prop('checked', true);
                }
                if (data.tech_auth_id != 0) {
                    $('input[name="tech_auth"]').prop('checked', true);
                }
                if (data.serigrafia_id != 0) {
                    $('input[name="serigrafia"]').prop('checked', true);
                }
                if (data.serigrafia_auth_id != 0) {
                    $('input[name="serigrafia_auth"]').prop('checked', true);
                }
                if (data.locksmith_id != 0) {
                    $('input[name="locksmith"]').prop('checked', true);
                }
                if (data.locksmith_auth_id != 0) {
                    $('input[name="locksmith_auth"]').prop('checked', true);
                }
                if (data.wires_id != 0) {
                    $('input[name="wires"]').prop('checked', true);
                }
                if (data.wires_auth_id != 0) {
                    $('input[name="wires_auth"]').prop('checked', true);
                }
                if (data.packing_cnc_id != 0) {
                    $('input[name="packing_cnc"]').prop('checked', true);
                }
                if (data.packing_cnc_auth_id != 0) {
                    $('input[name="packing_cnc_auth"]').prop('checked', true);
                }
                if (data.packing_espuma_id != 0) {
                    $('input[name="packing_espuma"]').prop('checked', true);
                }
                if (data.packing_espuma_auth_id != 0) {
                    $('input[name="packing_espuma_auth"]').prop('checked', true);
                }
                if (data.auth_id != 0) {
                    $('input[name="auth"]').prop('checked', true);
                }
                $("#palete_edit_check_overlay").show();
            }
        });

    });

    $("#palete_edit_check_overlay_ok").on('click', function () {
        $("#palete_edit_check_overlay_ok_wait").show();
        location.reload();
    });

    $("#palete_edit_check_overlay * input").on('change', function () {
        if (this.checked) {
            var checked = $("#palete_edit_check_overlay_auth").val();
        }
        else {
            var checked = 0;
        }
        $.ajax({
            url: './palete_update_state_admin/' + $(this).attr('name') + "/" + checked + "/" + $("#palete_edit_check_overlay_id").val(),
            success: function (data) {
            }
        });
    });

    // LEVANtAMENTO DE PALETES
    $(".levantamento_palete_overlay").on('click', function () {
        $('#levantamento_palete_yes').prop('disabled', true);
        $('#levantamento_palete_obs_input').val('');
        $('#levantamento_palete_yes').val($(this).attr('data-rel'));
        var title = $(this).parentsUntil("tr").parent().children("td:first").text();
        $("#levantamento_palete_check_overlay_span").text(title);
        $("#levantamento_palete").show();
        $("#levantamento_palete_quantidade").find('option').remove().end();
        var i = 0;
        while (i <= $(this).attr('data-value')) {
            $("#levantamento_palete_quantidade").append('<option value="'+i+'">'+i+'</option>')
            i++;
        }
        
    });
    
    $("#levantamento_palete_no").on('click', function () {
        $("#levantamento_palete").hide();
    });

    $(document).on('keyup', '#levantamento_palete_obs_input', function() {
        var clicked = this;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            var myURL = parseUri(window.location.href);
            var idPwd = clicked.id;
            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(clicked).val(),
                success: function (data) {
                    if ((data.id != 0) && ($("#levantamento_palete_quantidade").val() != 0)) {
                        $('#levantamento_palete_yes').prop('disabled', false);
                        $('#levantamento_palete_yes').attr('data-rel-user-id', data.id);
                        $('#levantamento_palete_yes').attr('data-rel-section', 'montagem');
                        $('#levantamento_palete_yes').attr('data-rel-value', $("#levantamento_palete_quantidade").val());
                    }
                    else {
                        $('#levantamento_palete_yes').prop('disabled', true);
                        $('#levantamento_palete_obs_input').val('');
                    }
                }
            });
        }, 1000);
    });
    
//    $('#levantamento_palete_obs_input').delayKeyup(function () {
//
//        var myURL = parseUri(window.location.href);
//        var idPwd = this.id;
//        $.ajax({
//            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
//            success: function (data) {
//                if ((data.id != 0) && ($("#levantamento_palete_quantidade").val() != 0)) {
//                    $('#levantamento_palete_yes').prop('disabled', false);
//                    $('#levantamento_palete_yes').attr('data-rel-user-id', data.id);
//                    $('#levantamento_palete_yes').attr('data-rel-section', 'montagem');
//                    $('#levantamento_palete_yes').attr('data-rel-value', $("#levantamento_palete_quantidade").val());
//                }
//                else {
//                    $('#levantamento_palete_yes').prop('disabled', true);
//                    $('#levantamento_palete_obs_input').val('');
//                }
//            }
//        });
//
//    }, 1000);

    $("#levantamento_palete_yes").on('click', function () {
        $.ajax({
            url: './levantamento_palete/' + $(this).val() + "/" + $(this).attr('data-rel-section') + "/" + $(this).attr('data-rel-user-id') + "/" + $(this).attr('data-rel-value'),
            success: function (data) {
                $("#levantamento_palete").hide();
                $("#overlay_waiting").show();
                location.reload();
            }
        });
    });
    $(document).on('click', "li.paginate_button:not('active')", function (e) {
      on_ready();
    });
});




function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = value;
            });
    return vars;
}

$(document).ready(function () {
    // INSERIR A DROPDOWN depois do NEW ENTRY
    $('<select id="mySelect" class="btn bg-gray" style="margin-left: 20px;">').insertAfter('.panel-heading > a.btn');
    $('#mySelect')
            .append($('<option>', {
                value: 0,
                text: 'PP em Aberto'
            }))
            .append($('<option>', {
                value: 1,
                text: 'PP Concluídos'
            }))
            .append($('<option>', {
                value: 'T',
                text: 'Todos os PP'
            }));

    if (getUrlVars()["status"]) {
        $('#mySelect').val(getUrlVars()["status"]);
    }
    else {
        $('#mySelect').val(0);
    }
    $('#mySelect').on('change', function () {
        var selectedValue = $(this).val();
        var myURL = parseUri(window.location.href);
        window.location.href = myURL['pathname'].split("/admin")[0] + '/admin/locksmith_requests?status=' + selectedValue
    });

});
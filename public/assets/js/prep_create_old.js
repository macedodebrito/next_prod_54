$(document).ready(function () {
    $(".picks").hide();
    
    $(document).on('click', 'a.accept_product', function (e) {
        h_id = this.id
        if ($(this).attr("href") == "#removing") {
            var status = -1;
        }
        else if ($(this).attr("href") == "#remove_product") {
            var status = 0;
        }
        else if ($(this).attr("href") == "#accept_product") {
            var status = 1;
        }
        else if ($(this).attr("href") == "#removing_fast") {
            var status = -2;
        }
        else if ($(this).attr("href") == "#prep_cnc") {
            var status = 3;
        }
        if (status < 0) {

            $.ajax({
                url: './prep_status/' + this.id + '/' + status,
                success: function (data) {
                    location.reload();
                }
            });
        }
        else {
            $.ajax({
                url: './prep_status/' + this.id + '/' + status + '/' + $("#picks_code_"+h_id).val(),
                success: function (data) {
                    location.reload();
                }
            });
        }

    });

    $('.picks_code_pass').delayKeyup(function () {
        var myURL = parseUri(window.location.href);
        var h_id = this.id
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + this.value,
            success: function (data) {
                if (data.id != 0) {
                    $("#h_"+h_id).hide();
                    $("#picks_span_"+h_id).show();
                    $("#"+h_id).val(data.id);
                }
                else {
                    $(".picks").hide();
                    $("#h_"+h_id).show();
                }
            }
        });

    }, 1000);

});


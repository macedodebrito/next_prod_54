$(document).ready(function () {
    // MUDAR O PP

    // TEM BUG
    $("#serial").prop('disabled', true);
    $("#obs").prop('disabled', true);

    $("button[name='next_action']").prop('disabled', true);

    $(document).on('keyup', '#serial', function () {
        temp_serial = $(this).val()
        temp_id = this.id
        $("#serial").each(function () {
            if (temp_id != this.id) {
                if (temp_serial == $(this).val()) {
                    $(this).val("")
                }
            }
            else {
                if (!$.isNumeric($(this).val())) {
                    $(this).val("")
                    $("button[name='next_action'").prop('disabled', true);
                }
                if ($(this).val().toString().length < 3) {
                    $("button[name='next_action'").prop('disabled', true);
                }
                else {
                    $.ajax({
                        url: '../check_serial/' + $("input[name='request_id']").val() + "/" + $(this).val(),
                        success: function (data) {
                            if (data > 0) {
                                $("#" + temp_id).val("")
                                $("button[name='next_action'").prop('disabled', true);
                            }
                            else {
                                $("button[name='next_action'").prop('disabled', false);
                            }
                        }
                    });
                }
            }
        });
    });


    $('#published').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("input[name='user_id']").val(data.id);
                    $("button[name='next_action']").prop('disabled', false);
                    $("#serial").prop('disabled', false);
                    $("#obs").prop('disabled', false);
                }
                else {
                    $("button[name='next_action']").prop('disabled', true);
                    $("input[name='user_id']").val("");
                    $("#serial").prop('disabled', true);
                    $("#obs").prop('disabled', true);
                }
            }
        });

    }, 1000);

    $("button[name='next_action']").click(function (e) {
        $("#published").val("0");
        $("form").submit();

    });

});


$(document).ready(function () {
    $(".store_picks").hide();
    $(".auth_picks").hide();
    $(".locksmith_picks").hide();

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({html: true, trigger: 'click'});
    });

    $(document).on('click', 'a.accept_auth', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = "auth";
            var get_id = $("#auth_id_picks_code_auth_" + idPwd).val()
        }
        $.ajax({
            url: './tech_paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
//                $.ajax({
//                    url: './pdf/' + idPwd + '/palete_pdf_view',
//                    success: function (data) {
//                        //location.reload();
//                    }
//                });
                //window.location.replace('./pdf/' + idPwd + '/palete_pdf_view');
                window.open('./pdf/' + idPwd + '/tech_palete_pdf_view', "_blank");
                location.reload();
            }
        });
    });



    // FUNCTION TO CALL EVERY SECTION VALIDATOR
    $('.picks_code_sections').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        var idPwd = this.id;
        var sectionID = $(this).attr("rel");
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#" + idPwd).hide();
                    $(".span_" + idPwd).show();
                    $("#input_" + idPwd).val(data.id);
                }
                else {
                    $(".span_" + idPwd).hide();
                    $("#" + idPwd).show();
                    $("#input_" + idPwd).val('');
                }
            }
        });
    }, 1000);


    // FUNCTION TO CALL EVERY AUTH SECTION VALIDATOR
    $(document).on('keyup', ".picks_auth_sections", function () {
        console.log("x");
        var myURL = parseUri(window.location.href);
        var idPwd = this.id;
        var sectionID = $(this).attr("rel");
        var ELEMENT = this;
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
            success: function (data) {
                if (data.id != 0) {
                    $(ELEMENT).hide();
                    $(ELEMENT).siblings('span').show();
                    $(ELEMENT).siblings("span").attr('rel', data.id);
                }
                else {
                    $(".span_" + idPwd).hide();
                    $("#" + idPwd).show();
                    $("#input_" + idPwd).val('');
                }
            }
        });
    });


    // FUNCTION TO VALIDATE ON SECTION
    $(document).on('click', 'a.first_validator', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = $(this).attr("rel");
            var get_id = $("#input_" + status + "_" + idPwd).val()
        }
        $.ajax({
            url: './tech_paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
                location.reload();
            }
        });
    });

    // FUNCTION TO VALIDATE ON SECTION
    $(document).on('click', 'a.second_validator', function (e) {
        var idPwd = this.id;
        if ($(this).attr("href") == "#accept_product") {
            var status = $(this).attr("rel");
            var get_id = $(this).parent().attr('rel');
            
        }
        $.ajax({
            url: './tech_paletes/' + this.id + '/' + status + '/' + get_id,
            success: function (data) {
                location.reload();
            }
        });
    });

    $('.picks_code_auth').delayKeyup(function () {

        var myURL = parseUri(window.location.href);
        var idPwd = this.id;
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $(this).val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#" + idPwd).hide();
                    $(".auth_" + idPwd).show();
                    $("#auth_id_" + idPwd).val(data.id);
                }
                else {
                    $(".auth_picks").hide();
                    $("#" + idPwd).show();
                }
            }
        });

    }, 1000);

    $(document).on('click', 'a.tech_delete_palete_history', function (e) {
        var idPwd = $(this).attr('rel');
        $("#delete_palete_history_span").text('#' + idPwd);
        $("button#delete_palete_history_yes").attr('rel', idPwd);
        $("#delete_palete_history").show();
    });

    $(document).on('click', 'button#delete_palete_history_yes', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './tech_delete_palete_history/' + idPwd,
            success: function (data) {
                $("#delete_palete_history").hide();
                $("#delete_palete_history_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#delete_palete_history_no', function (e) {
        $("#delete_palete_history_span").text('');
        $("#delete_palete_history").hide();
    });

    $(document).on('click', 'a.first_obs, a.second_obs', function (e) {
        var idPwd = $(this).attr('rel');
        $("#obs_palete_span").text('#' + idPwd);
        $("button#obs_palete_yes").attr('rel', idPwd);
        $("button#obs_palete_delete").attr('rel', idPwd);
        $("input#palete_obs_input").val($(this).attr('data-rel'));
        $("#obs_palete").show();
    });

    $(document).on('click', 'button#obs_palete_yes', function (e) {
        var idPwd = $(this).attr("rel");
        var obs = encodeURIComponent($("#palete_obs_input").val());
        $.ajax({
            url: './tech_palete_store_obs/' + idPwd + '/' + obs,
            success: function (data) {
                $("#obs_palete").hide();
                $("#obs_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#obs_palete_delete', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './tech_palete_store_obs_delete/' + idPwd,
            success: function (data) {
                $("#obs_palete").hide();
                $("#obs_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#obs_palete_no', function (e) {
        $("#obs_palete_span").text('');
        $("#obs_palete").hide();
    });

    $(document).on('click', 'div.nxt_phc', function (e) {
        var idPwd = $(this).attr('data-rel');
        var idPwd2 = $(this).attr('data-rel');
        if (!idPwd) {
            idPwd = $(this).attr('data-value');
        }
        $("#nxt_phc_palete_span").text('#' + idPwd);
        $("#nxt_phc_default_palete_obs_input").val($(this).attr('data-value'));
        var idPwd_explode = idPwd2.split("/");
        $("#nxt_phc_palete_obs_input").val(idPwd_explode[0]);
        $("#nxt_phc_palete_obs_input2").val(idPwd_explode[1]);
        $("button#nxt_phc_palete_yes").attr('rel', $(this).attr('rel'));
        $("button#nxt_phc_palete_delete").attr('rel', $(this).attr('rel'));
        $("#nxt_phc_palete").show();
    });

    $(document).on('click', 'button#nxt_phc_palete_no', function (e) {
        $("#nxt_phc_palete_span").text('');
        $("#nxt_phc_palete").hide();
    });

    $(document).on('click', 'button#nxt_phc_palete_delete', function (e) {
        var idPwd = $(this).attr("rel");
        $.ajax({
            url: './tech_nxt_phc_palete_delete/' + idPwd,
            success: function (data) {
                $("#nxt_phc_palete").hide();
                $("#nxt_phc_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).on('click', 'button#nxt_phc_palete_yes', function (e) {
        var idPwd = $(this).attr("rel");
        var obs = encodeURIComponent($("#nxt_phc_palete_obs_input").val() + '##' + $("#nxt_phc_palete_obs_input2").val());
        $.ajax({
            url: './tech_nxt_phc_palete_yes/' + idPwd + '/' + obs,
            success: function (data) {
                $("#nxt_phc_palete").hide();
                $("#nxt_phc_palete_span").text('');
                location.reload();
            }
        });
    });

    $(document).ready(function () {
        $("<div style=\"display: inline; margin-left: 12px;\"><b>Legenda:</b> <span style=\"background-color: #000000; color: #FFFFFF; padding: 4px 10px;\">NXT</span>Nota de Encomenda NXT <span style=\"background-color: #FF0000; color: #FFFFFF; padding: 4px 10px;\">NSP</span> Nota de Saída de Produção</div>").insertAfter("input.form-control.input-sm[type='search']");
    });
});


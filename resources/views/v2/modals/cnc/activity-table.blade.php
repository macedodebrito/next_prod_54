<div class="modal fade" id="cnc_activity_table" tabindex="-1" aria-hidden="true" style="display: none;">
         <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="card-title">Histórico de Lançamentos</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
               </div>
               <div class="modal-body">
                  <div class="card recent-sales overflow-auto">
                     <div class="card-body">
                        <h5 class="card-title">CNC</h5>
                        <table class="table table-borderless table-striped datatable">
                           <thead>
                              <tr>
                                 <th scope="col">Lançado</th>
                                 <th scope="col">PP</th>
                                 <th scope="col">#</th>
                                 <th scope="col">Produto</th>
                                 <th scope="col">Operador</th>
                                 <th scope="col">Observações</th>
                              </tr>
                           </thead>
                           <tbody>
                           @foreach($getAll as $all_item)
                                <tr>
                                    <th scope="row">
                                        <span class="fs-6">{{ \Carbon\Carbon::parse($all_item->created_at)->format('d.m.Y') }}<br/>{{ \Carbon\Carbon::parse($all_item->created_at)->format('H:m:s') }}</span>
                                    </th>
                                    <td>{{ $all_item->pp }}</td>
                                    <td class="align-middle"><span class="badge bg-success fs-6">{{ $all_item->value }}</span></td>
                                    <td>
                                    {{ $all_item->name }}
                                        <div>{!! colorBadge($all_item->hex_bg, $all_item->hex_cor, $all_item->name_cor) !!}</div>
                                    </td>
                                    <td class="align-middle">{{ $all_item->user }}</td>
                                    <td>{{ $all_item->obs }}</td>
                                </tr>
                            @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
               </div>
            </div>
         </div>
      </div>
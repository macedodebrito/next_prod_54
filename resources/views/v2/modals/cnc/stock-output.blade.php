<div class="modal fade" id="cnc_stock_output" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form id="send-output" method="POST" autocomplete="off">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <h5 class="card-title">Dar Saída para a Preparação</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">                                      
                    <div class="row">

                        <div class="col-12 col-md-8">                        
                            <div class="w-100 text-start">
                                <label>Pedido de Produção</label>
                            </div>                        
                            <select id="get-pp" name="get-pp" class="selectpicker w-100" data-live-search="true" title="Escolha uma opção" required>
                            </select>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="w-100 text-start">
                                <label>Quantidade</label>
                            </div>                        
                            <select id="get-checked" name="get-checked" class="selectpicker w-100" data-live-search="true" title="Escolha uma opção" disabled required>
                            </select>
                        </div>

                        <div class="col-12">
                            <div class="form-floating mt-3">
                                <textarea class="form-control" placeholder="Address" id="set-obs" name="set-obs" style="height: 100px;" disabled></textarea>
                                <label for="set-obs">Observações</label>
                            </div>
                        </div>                        

                    </div>                
                </div>
                <div class="modal-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-6 p-0 ps-md-0">
                                <input autocomplete="off" id="get-user-info" name="get-user-info" type="password" class="form-control" placeholder="Digite o seu código" disabled required>
                                <div id="view-user-info-wrapper" class="input-group d-none">
                                    <input id="view-user-info" type="text" class="form-control" placeholder="" aria-label="" disabled>
                                    <input id="set-user-id" name="set-user-id" type="hidden">
                                    <button id="reset-user-info" type="button" class="btn btn-dark input-group-text"><i class="bi bi-x-lg"></i></button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 p-0 pe-md-0 mt-3 mt-md-0">
                                <button id="set-user-validation" type="button" class="btn btn-primary" disabled>Validar</button>
                                <button id="btn-send-output" type="button" class="btn btn-success" disabled>Dar Saída</button>
                                <button id="btn-wait-output" type="button" class="btn btn-success" disabled><span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> A Processar...</button>
                                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@push('custom-styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/css/bootstrap-select.min.css">
@endpush

@push('custom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta3/dist/js/bootstrap-select.min.js"></script>

    <script>
    // global app configuration object
    config.cnc = {
        getPP: "{{ route('v2_cncGetPP') }}",
        getChecked: "{{ route('v2_cncGetChecked') }}",
        sendOutput: "{{ route('v2_cncSendOutput') }}"
    };
  </script>

    <script src="{{ asset('assets/v2/js/cnc.js') }}"></script>
@endpush
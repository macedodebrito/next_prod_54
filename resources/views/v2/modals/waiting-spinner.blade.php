<div class="modal fade" id="waiting-spinner" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h5 id="waiting-spinner-title" class="card-title"></h5>
        </div>
        <div class="modal-body">
          <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
          <span id="waiting-spinner-text"></span>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="relogio-de-ponto" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="card-title">Relógio de Ponto</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="relogio-de-ponto-form" method="POST">
            {!! csrf_field() !!}              
          <div class="form-floating">
            <input type="password" class="form-control" id="get-user-clock" name="get-user-clock" placeholder="Digite o seu código">
            <input type="hidden" class="form-control" id="get-user-clock-id" name="get-user-clock-id">
            <input type="hidden" class="form-control" id="get-user-clock-login-logout" name="get-user-clock-login-logout">
            <label for="get-user-clock">Digite o seu código</label>
          </div>

          <div id="relogio-de-ponto-info" class="mt-3 d-none">
            <div>
              <i class="bi bi-person-circle"></i> <span id="relogio-de-ponto-info-name" class="fw-bold"></span>
            </div>
            <div>
              <span id="relogio-de-ponto-info-in-out" class="badge">
                <i id="relogio-de-ponto-info-in-out-icon" class="bi me-1"></i>
                <span id="relogio-de-ponto-info-in-out-text"></span>
              </span>                
            </div>

            <div class="form-check mt-3">
              <input class="form-check-input" type="checkbox" id="user-clock-confirm">
              <label class="form-check-label" for="user-clock-confirm">
                Confirma?
              </label>
            </div>

          </div>
          </form>
        </div>
        <div id="relogio-de-ponto-info-button" class="modal-footer d-none">
          <button id="relogio-de-ponto-info-button-action" type="button" class="btn w-100" data-bs-toggle="modal" data-bs-target="#waiting-spinner"></button>
          <button id="relogio-de-ponto-info-button-work-absence" type="button" class="btn btn-dark w-100">Faltas</button>
        </div>
      </div>
    </div>
  </div>
@extends('v2.main.base')

@section('content')

    <main id="main" class="main">
  
    <div class="mx-0">
        <div class="row justify-content-between g-0 align-items-center">
            <div class="col">
                <div class="pagetitle">
                    <h1>CNC</h1>
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item active">CNC</li>
                        </ol>
                    </nav>
                </div><!-- End Page Title -->    
            </div>

            <div class="col text-end">
                <!-- Example split danger button -->
                <div class="btn-group">
                    <button disabled id="output_btn" type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#cnc_stock_output"><i class="bi bi-arrow-up-right-circle-fill me-2"></i> <span>Dar Saída</span></button>
                    <button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="visually-hidden">Toggle Dropdown</span>
                    </button>

                    @include('v2.modals.cnc.stock-output')

                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Pedidos em Aberto</a></li>                                                
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Painel de bordo</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

  
      <section class="section dashboard">
            <div class="row">
    
                <div class="col-lg-8">
                    <?php echo App\Http\Controllers\V2\CncController::getOrders() ?>
                </div>

                <div class="col-lg-4">
                    <?php echo App\Http\Controllers\V2\CncController::getActivity() ?>                    
                </div>

            
            </div>
        </section>
    </main><!-- End #main -->   

@endsection

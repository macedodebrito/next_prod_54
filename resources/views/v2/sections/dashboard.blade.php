@extends('v2.main.base')

@section('content')
  
    <main id="main" class="main">
  
      <div class="pagetitle">
        <h1>Dashboard</h1>
        <nav>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </nav>
      </div>
  
      <section class="section dashboard">
        <div class="row">
            
          <div class="col-lg-8">
          <?php echo App\Http\Controllers\V2\DashboardController::getProductivity("1") ?>
          </div>
            
          <div class="col-lg-4">
            <?php echo App\Http\Controllers\V2\DashboardController::getActivity("sections") ?>
          </div>
  
        </div>
      </section>
  
    </main>

@endsection

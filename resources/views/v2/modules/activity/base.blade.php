            <div id="module-activity" class="card">
              <div class="filter">
                <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                  <li class="dropdown-header text-start">
                    <h6>Filtro</h6>
                  </li>
  
                  <li><a class="dropdown-item fw-bold" href="#">Secções</a></li>
                  <li><a class="dropdown-item" href="#">Livro de Ponto</a></li>
                </ul>
              </div>
  
              <div class="card-body">
                <h5 class="card-title">Actividade <span>| Secções</span></h5>
  
                <div class="activity">
  
                  @foreach($resultSQL as $activity)
                    @include('v2.modules.activity.sections', ['item' => $activity])
                  @endforeach
  
                </div>
  
              </div>
            </div>
<div class="activity-item d-flex">
    <div class="activite-label">{{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}</div>
    <i class='bi bi-circle-fill activity-badge text-activity-{{ $activity->status }} align-self-start'></i>
    <div class="activity-content">
        <span class="fw-bold">{{ $activity->status_name }}</span> <small>| {{ $activity->bigname }}</small>
        <div>
            <span class="badge bg-activity-{{ $activity->status }}">{{ $activity->value }} x {{ $activity->name }}</span> 
            @if ($activity->custom_name)<div><span class="badge bg-dark">{{ $activity->custom_name }}</span></div>@endif
        </div>
    </div>
</div>
<li class="nav-item dropdown">
    <a class="d-none d-sm-block nav-link" href="#" data-bs-toggle="dropdown">
        <div class="border-1 border-end px-3 d-inline-block text-center">
        <div>
            <h6 class="p-0 m-0">DIA</h6>
            <span class="badge bg-{{ $dailyUntilSuper['daily']['color'] }}"><i class="bi {{ $dailyUntilSuper['daily']['icon'] }} me-1"></i> {{ $dailyUntilSuper['SQL'][0]->daily_points }}</span>
        </div>
        </div>

        <div class="border-1 border-end px-3 d-inline-block text-center">
        <div>
            <h6 class="p-0 m-0">SEMANA</h6>
            <span class="badge bg-{{ $dailyUntilSuper['week']['color'] }}"><i class="bi {{ $dailyUntilSuper['week']['icon'] }} me-1"></i>{{ $dailyUntilSuper['SQL'][0]->weekly_points }}</span>
        </div>
        </div>

        <div class="border-1 border-end px-3 d-inline-block text-center">
        <div>
            <h6 class="p-0 m-0">MÊS</h6>
            <span class="badge bg-{{ $dailyUntilSuper['month']['color'] }}"><i class="bi {{ $dailyUntilSuper['month']['icon'] }} me-1"></i>{{ $dailyUntilSuper['SQL'][0]->monthly_points }}</span>
        </div>
        </div>

        <div class="border-1 border-end px-3 d-inline-block text-center">
        <div>
            <h6 class="p-0 m-0">SUPER</h6>
            <span class="badge bg-{{ $dailyUntilSuper['super']['color'] }}"><i class="bi {{ $dailyUntilSuper['super']['icon'] }} me-1"></i>{{ $dailyUntilSuper['SQL'][0]->monthly_points }}</span>
        </div>
        </div>                  
    </a>              
                
    <a class="d-block d-sm-none nav-link nav-icon" href="#" data-bs-toggle="dropdown">
        <span class="badge bg-{{ $dailyUntilSuper['daily']['color'] }}"><i class="bi bi-check-circle-fill me-1"></i>{{ $dailyUntilSuper['SQL'][0]->daily_points }}</span>
    </a><!-- End Notification Icon -->

    <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">    
    <li class="notification-item py-1">
        <i class="bi {{ $dailyUntilSuper['daily']['icon'] }} text-{{ $dailyUntilSuper['daily']['color'] }}"></i>
        <div>
        <h4 class="p-0 m-0">DIA</h4>
        <span class="text-{{ $dailyUntilSuper['daily']['color'] }} small fw-bold">{{ $dailyUntilSuper['SQL'][0]->daily_points }}</span> <span class="text-muted small ps-1">/ {{ $dailyUntilSuper['SQL'][0]->l_day }}</span>
        </div>
    </li>

    <li>
        <hr class="dropdown-divider">
    </li>

    <li class="notification-item py-2">
        <i class="bi {{ $dailyUntilSuper['week']['icon'] }} text-{{ $dailyUntilSuper['week']['color'] }}"></i>
        <div>
        <h4 class="p-0 m-0">SEMANA</h4>
        <span class="text-{{ $dailyUntilSuper['week']['color'] }} small fw-bold">{{ $dailyUntilSuper['SQL'][0]->weekly_points }}</span> <span class="text-muted small ps-1">/ {{ $dailyUntilSuper['SQL'][0]->l_week }}</span>
        </div>
    </li>

    <li>
        <hr class="dropdown-divider">
    </li>

    <li class="notification-item py-2">
        <i class="bi {{ $dailyUntilSuper['month']['icon'] }} text-{{ $dailyUntilSuper['month']['color'] }}"></i>
        <div>
        <h4 class="p-0 m-0">MÊS</h4>
        <span class="text-{{ $dailyUntilSuper['month']['color'] }} small fw-bold">{{ $dailyUntilSuper['SQL'][0]->monthly_points }}</span> <span class="text-muted small ps-1">/ {{ $dailyUntilSuper['SQL'][0]->l_month }}</span>
        </div>
    </li>

    <li>
        <hr class="dropdown-divider">
    </li>

    <li class="notification-item py-2">
        <i class="bi {{ $dailyUntilSuper['super']['icon'] }} text-{{ $dailyUntilSuper['super']['color'] }}"></i>
        <div>
        <h4 class="p-0 m-0">SUPER</h4>
        <span class="text-{{ $dailyUntilSuper['super']['color'] }} small fw-bold">{{ $dailyUntilSuper['SQL'][0]->monthly_points }}</span> <span class="text-muted small ps-1">/ {{ $dailyUntilSuper['SQL'][0]->l_super }}</span>
        </div>
    </li>

    <li>
        <hr class="dropdown-divider d-none">
    </li>
    <li class="dropdown-footer d-none">
    <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">Ver produtividade</span></a>
    </li>

    </ul><!-- End Notification Dropdown Items -->
</li>
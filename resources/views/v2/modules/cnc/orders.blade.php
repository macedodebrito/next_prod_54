<div class="card recent-sales overflow-auto">  
    <div class="card-body">
        <h5 class="card-title">Encomendas</h5>

        <table class="table table-borderless table-striped datatable">
            <thead>
                <tr>
                <th scope="col">Estado</th>
                <th scope="col">Término</th>
                <th scope="col">#</th>                            
                <th scope="col">Produto</th>                            
                <th scope="col">Pontos</th>
                <th scope="col">Observações</th>
                <th scope="col">Cliente</th>
                </tr>
            </thead>
            <tbody>
                @foreach($getResults as $order_item)
                <tr>
                    <th scope="row">
                        <span class="badge bg-{{ $order_item->atraso_icon }} fs-6 w-100">{{ $order_item->atraso_text }}<br/>({{ $order_item->atraso_dias }})</span>
                    </th>
                    <td>{{ \Carbon\Carbon::parse($order_item->end_cnc_at)->format('d.m.Y') }}</td>
                    <td class="align-middle"><span class="badge bg-{{ $order_item->status }} fs-6">@if ($order_item->estoque_antes_encomenda < 0) {{ $order_item->encomenda }} @else {{ $order_item->encomenda-$order_item->estoque_antes_encomenda }} @endif<br><small>(enc: {{ $order_item->encomenda }})</small></span></td>
                    <td>
                       {{ $order_item->product_name }} <i class="bi bi-image fs-6"></i>
                        <div>{!! colorBadge($order_item->hex_bg, $order_item->hex_cor, $order_item->name_cor) !!}</div>
                    </td>
                    <td class="align-middle">100</td>
                    <td>{{ $order_item->obs }}</td>
                    <td>{{ $order_item->client }}</td>
                </tr>
                @endforeach            
            </tbody>
        </table>
    </div>
</div>
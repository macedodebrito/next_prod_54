<!-- Recent Activity -->
<div class="card">
   <div class="card-body">
      <h5 class="card-title">Actividade Recente 
         <span class="float-end">
         <button type="button" class="btn btn-outline-dark btn-sm rounded-pill" data-bs-toggle="modal" data-bs-target="#cnc_activity_table"><i class="bi bi-table"></i></button>
         </button>
         <span>
      </h5>
      @include('v2.modals.cnc.activity-table')
      <div class="activity">
         @foreach($getLatest as $activity_item)
         <div class="activity-item d-flex">
            <div class="activite-label">
               <span class="badge rounded-pill bg-secondary">PP {{ $activity_item->pp }}</span>
               <div>{{ \Carbon\Carbon::parse($activity_item->created_at)->diffForHumans() }}</div>
            </div>
            <i class='bi bi-circle-fill activity-badge text-success align-self-start'></i>
            <div class="activity-content w-100 mb-3">
               <div class="d-flex justify-content-between">
                  <div>
                     <span class="badge bg-primary text-wrap">{{ $activity_item->value }} x {{ $activity_item->name }}</span> {!! colorBadge($activity_item->hex_bg, $activity_item->hex_cor, $activity_item->name_cor) !!}
                  </div>
                  <div>
                  
                    <a class="icon ms-3" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots-vertical"></i></a>
                    
                    <ul class="dropdown-menu">
                        <li class="dropdown-header text-start">
                            <h6>PP {{ $activity_item->pp }}</h6>
                        </li>
                        <li><a class="dropdown-item" href="#"><i class="bi bi-pencil-fill"></i> Edit</a></li>
                        <li><a class="dropdown-item text-danger" href="#"><i class="bi bi-trash3-fill"></i> Delete</a></li>
                    </ul>
                
                  </div>
               </div>
               <div><small>por {{ $activity_item->user }}</small></div>
               @if ($activity_item->obs)
               <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <i>{{ $activity_item->obs }}</i>
               </div>
               @endif
            </div>
         </div>
         @endforeach    
      </div>
   </div>
</div>
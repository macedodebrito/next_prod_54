    <header id="header" class="header fixed-top d-flex align-items-center">
  
        <div class="d-flex align-items-center justify-content-between">
          <i class="bi bi-list toggle-sidebar-btn"></i>
          <a href="{{ route('v2_dashboard') }}" class="logo d-flex">
            <span class="d-none d-lg-block">NXT</span>
          </a>                        
        </div><!-- End Logo -->
      
        <nav id="section-side-menu" class="header-nav me-auto d-none d-lg-none">
  
          <div class="form-floating m-3">
            <select class="form-select border-0" id="floatingSelect" aria-label="Floating label select example">
              <option @if(Route::currentRouteName() == "v2_dashboard") selected @endif="v2_dashboard">Dashboard</option>
              <option @if(Route::currentRouteName() == "v2_cnc") selected @endif value="v2_cnc">CNC</option>
              <option value="prep">Preparação</option>
              <option value="carpintaria">Carpintaria</option>
              <option value="acabamento">Acabamento</option>
              <option value="pintura">Pintura</option>
              <option value="montagem">Montagem</option>
              <option value="serralharia">Serralharia</option>
              <option value="redes">Redes</option>
              <option value="tecnica">Técnica</option>
              <option value="materiais">Materiais</option>
            </select>
            <label for="floatingSelect">Secção</label>
          </div>
        </nav><!-- End Icons Navigation -->
  
        <nav class="header-nav ms-auto">
  
          <ul class="d-flex align-items-center">
            @if (in_array(Route::currentRouteName(), ['v2_cnc']))
              @include('v2.modules.productivity.daily-until-super', [ 'dailyUntilSuper' => dailyUntilSuper(Route::currentRouteName()) ])
            @endif
    
            <li class="nav-item dropdown">
  
              <a id="relogio-de-ponto-btn" class="nav-link nav-icon ms-2 ms-md-4" href="#" data-bs-toggle="modal" data-bs-target="#relogio-de-ponto">
                <i class="bi bi-clock"></i>
              </a><!-- End Messages Icon -->
  
            </li><!-- End Messages Nav -->
    
            <li class="nav-item dropdown pe-3">
    
              <a class="nav-link nav-profile d-flex align-items-center" href="#" data-bs-toggle="dropdown">              
                <i class="bi bi-three-dots-vertical" style="font-size: 1.25rem;"></i>
              </a><!-- End Profile Iamge Icon -->
    
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">                    
                <li>
                  <a class="dropdown-item d-flex align-items-center" href="{{ route('home') }}">
                    <i class="bi bi-gear"></i>
                    <span>Admin V1 (Old NXT)</span>
                  </a>
                </li>

                <li>
                  <hr class="dropdown-divider">
                </li> 
    
                <li>
                  <a class="dropdown-item d-flex align-items-center" href="#">
                    <i class="bi bi-gear"></i>
                    <span>Manutenção</span>
                  </a>
                </li>
                         
                <li>
                  <a class="dropdown-item d-flex align-items-center" href="#">
                    <i class="bi bi-gear"></i>
                    <span>Consumíveis</span>
                  </a>
                </li>

                <li>
                  <hr class="dropdown-divider">
                </li>

                <li>
                  <a class="dropdown-item d-flex align-items-center" href="{{ route('logout') }}">
                    <i class="bi bi-box-arrow-right"></i>
                    <span>Sair</span>
                  </a>
                </li>
    
              </ul><!-- End Profile Dropdown Items -->
            </li><!-- End Profile Nav -->
    
          </ul>
        </nav><!-- End Icons Navigation -->
    
      </header>

    <div class="modal fade" id="loader" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
          loading...
        </div>
    </div>
    <aside id="sidebar" class="sidebar">
  
        <ul class="sidebar-nav" id="sidebar-nav">
          
          <li class="nav-heading">Secções</li>
            
          <li class="nav-item">
            <a class="nav-link active" href="{{ route('v2_cnc') }}">
              <i class="bi bi-columns-gap"></i>
              <span>CNC</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-x-diamond"></i>
              <span>Preparação</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-box-seam"></i>
              <span>Carpintaria</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-gem"></i>
              <span>Acabamento</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-palette"></i>
              <span>Pintura</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-patch-check"></i>
              <span>Montagem</span>
            </a>
          </li>
  
          <li class="nav-heading">Outras</li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-gear"></i>
              <span>Serralharia</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-grid-3x3"></i>
              <span>Redes</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-cpu"></i>
              <span>Técnica</span>
            </a>
          </li>
  
          <li class="nav-item">
            <a class="nav-link collapsed" href="#">
              <i class="bi bi-clipboard-data"></i>
              <span>Materiais</span>
            </a>
          </li>
  
        </ul>
    
      </aside>
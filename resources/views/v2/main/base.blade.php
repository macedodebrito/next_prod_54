<!DOCTYPE html>
<html lang="en">

<head>
  <title>NXT</title>
  <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1" ">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Favicons -->
  <link href="{{ asset('assets/v2/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/v2/img/apple-touch-icon.png') }}" rel="apple-touch-icon">  

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->

  <link href="{{ asset('assets/v2/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/quill/quill.snow.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/v2/vendor/simple-datatables/style.css') }}" rel="stylesheet">
  
  @stack('custom-styles')

  <link href="{{ asset('assets/v2/css/style.css') }}" rel="stylesheet">


</head>

<body>
    @include('v2.main.navbar')
    @include('v2.main.sidebar')
    @yield('content')
    @include('v2.modals.waiting-spinner')
    @include('v2.modals.relogio-de-ponto')

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/v2/vendor/apexcharts/apexcharts.min.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/chart.js/chart.umd.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/echarts/echarts.min.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/quill/quill.min.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/simple-datatables/simple-datatables.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ asset('assets/v2/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script>
    // global app configuration object
    var config = {
        routes: {
          checkUser: "{{ route('v2_checkUser') }}",
          checkUserInfo: "{{ route('v2_checkUserInfo') }}",
          setUserClock: "{{ route('v2_setUserClock') }}"
        }
    };
  </script>

  @stack('custom-scripts')

  <script src="{{ asset('assets/v2/js/main.js') }}"></script>
  <script src="{{ asset('assets/v2/js/global.js') }}"></script>



</body>

</html>
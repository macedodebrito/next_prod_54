<!-- pdf.blade.php -->
<!DOCTYPE html>
<style>
    body, html, table, tr, td {
        font-family: 'Arial';
        font-size: 14px;
    }
</style>
<html>
    <head>
        <meta charset="utf-8">
        <title>NEXT-proaudio</title>
    </head>
    <body>
        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td>
                    <img src="{{ asset('assets/gfx/NEXT_b.png')}}"/>
                </td>
                <td style="text-align: right;">
                    <b>PRODUTIVIDADE</b><br/>
                    {{ $start_of_month }} a {{ $end_of_month }}
                </td>
            </tr>
        </table>
        <hr/>

        <table cellspacing="0" class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="background-color: #ddd; padding-top: 4px; padding-bottom: 4px;">
                    <b>NOME</b>
                </td>
                <td style="background-color: #ddd; text-align: right; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    <b>ÚLTIMA<br/>SEMANA</b>
                </td>                
                <td style="background-color: #ddd; text-align: right; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    <b>ESTA<br/>SEMANA</b>
                </td>
                <td style="background-color: #ddd; text-align: right; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    <b>MES<br/>ANTERIOR</b>
                </td>                    
                <td style="background-color: #ddd; text-align: right; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    <b>ESTE<br/>MES</b>
                </td>                
            </tr>
            @foreach($sql_info as $section)
            <tr>
                <td style="border-top: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px;">
                    <b>{{ $section->bigname }}</b>
                </td>
                <td style="text-align: right; border-top: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px; width: 15%;">s
                    {{ $section->nr_semanal_anterior }}
                </td>
                <td style="text-align: right; border-top: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    {{ $section->nr_semanal }}
                </td>
                <td style="text-align: right; border-top: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    {{ $section->nr_mensal_anterior }}
                </td>                
                <td style="text-align: right; border-top: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    {{ $section->nr_mensal }}
                </td>                            
            </tr>
            @endforeach
        </table>

        <hr/>

        <table cellspacing="0" class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="background-color: #ddd; padding-top: 4px; padding-bottom: 4px;">
                    <b>Secção</b>
                </td>
                <td style="background-color: #ddd; text-align: right; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    <b>ESTE<br/>MES</b>
                </td>                
            </tr>
            @foreach($sections as $key => $section)
            <tr>
                <td style="border-bottom: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px;">
                    <b>{{ $section["name"] }}</b>
                </td>
                <td style="text-align: right; border-bottom: 1px solid #CCC; padding-top: 4px; padding-bottom: 4px; width: 15%;">
                    {{ $section["value_month"] }}
                </td>                      
            </tr>
            @endforeach
        </table>

        <table class="table table-bordered" style="width: 100%; position: fixed; bottom: 0; border-top: 2px solid #000;">            
            <tr>
                <td style="width: 30%;">
                    Processado por NXT | www.next-proaudio.com
                </td>
                <td style="width: 30%; text-align: right;">
                    criado em <?php echo date('Y-m-d H:i:s') ?>
                </td>                
            </tr>
        </table>

    </body>
</html>
<!-- pdf.blade.php -->
<!DOCTYPE html>
<style>
    body, html, table, tr, td {
        font-family: 'Arial';
        font-size: 12px;
    }
</style>
<html>
    <head>
        <meta charset="utf-8">
        <title>NEXT-proaudio</title>
    </head>
    <body>
        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td>
                    <img src="{{ asset('assets/gfx/NEXT_b.png')}}"/>
                </td>
                <td style="text-align: right;">
                    <b>SECÇÃO</b> <br/><span style="text-transform: uppercase;">{{ $sql_section }}</span>
                    <br/>
                    <b>ID DO PEDIDO</b> <br/>#{{ $sql_info->id }}
                </td>
            </tr>
        </table>

        <hr/>

        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="width: 25%;">
                    REQUISITADO POR
                </td>
                <td style="width: 25%;">
                    {{ $autor }}
                </td>    
                <td style="width: 25%;">
                    DATA DO PEDIDO
                </td>
                <td style="width: 25%; text-align: right;">
                    {{ $sql_info->created_at }} 
                </td> 
            </tr>
        </table>

        <hr/>

        <table class="table table-bordered" style="width: 100%;">
            <tr style="background-color: #eee; font-weight: bold;"> 
                <td style="width: 5%; text-align: center;">
                    Devolver
                </td>                
                <td style="width: 10%;">
                    Referência
                </td>
                <td style="width: 25%;">
                    Designação
                </td>
                <td style="width: 6%; text-align: center;">
                    Qtd.
                </td>               
                <td style="width: 8%;">
                    Unid.
                </td>      
                <td style="width: 7%; text-align: center;">
                    Stock
                </td>                 
                <td style="width: 5%; text-align: center;">
                    Conf.
                </td>                                 
            </tr>

            @foreach($sql_comsumables as $comsumable)
            <tr>               
                <td style="width: 5%; text-align: center;">
                    @if ($comsumable->deliver == 0)
                    -
                    @else
                    <div style="color: #000000; font-weight: bold">SIM</div>
                    @endif
                </td>                
                <td style="width: 10%;" @if(!$comsumable->ref)style="background-color:#FFFF0E;@endif">
                    @if($comsumable->ref)
                    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($comsumable->ref, 'C39', 1,33)}}" alt="barcode" />
                    <br>
                    {{ $comsumable->ref }}
                    @else
                    - SEM REFERENCIA
                    @endif
                </td>
                <td style="width: 25%;">
                    {{ $comsumable->nome }}
                </td>
                <td style="width: 6%; text-align: center;">
                    {{ $comsumable->value }}
                </td>                 
                <td style="width: 8%;">
                    {{ $comsumable->unit }}
                </td>  
                <td style="width: 7%; text-align: center;">
                    {{ round($comsumable->stock, 1) }}
                </td>                
                <td style="width: 5%; border: 1px solid #000000;">
                    
                </td>                                 
            </tr>

            @endforeach       

            @if(count($sql_comsumables_new) > 0)           
            @foreach($sql_comsumables_new as $new)
            <tr style="background-color: #ffffe0;">               
                <td style="width: 25%;">
                    PARA CRIAR
                </td>
                <td style="width: 42%;">
                    {{ $new->name }}
                </td>
                <td style="width: 6%; text-align: center;">
                    {{ $new->value }}
                </td>         
                 <td style="width: 5%; border: 1px solid #000000;">
                    
                </td>                  
            </tr>

            @endforeach
         
            @endif
        </table>

        <hr/>

        <table class="table table-bordered" style="width: 100%; position: fixed; bottom: 0; border-top: 2px solid #000;">            
            <tr>
                <td style="width: 30%;">
                    Processado por NXT | www.next-proaudio.com
                </td>
                <td style="width: 30%; text-align: right;">
                    criado em <?php echo date('Y-m-d H:i:s') ?>
                </td>                
            </tr>
        </table>

    </body>
</html>
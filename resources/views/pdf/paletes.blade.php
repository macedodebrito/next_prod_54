<!-- pdf.blade.php -->
<!DOCTYPE html>
<style>
    body, html, table, tr, td {
        font-family: 'Arial';
        font-size: 14px;
    }
</style>
<html>
    <head>
        <meta charset="utf-8">
        <title>NEXT-proaudio</title>
    </head>
    <body>
        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td>
                    <img src="{{ asset('assets/gfx/NEXT_b.png')}}"/>
                </td>
                <td style="text-align: right;">
                    PALETE ID
                    <br/>#{{ $sql_request_week->id }}_{{ $sql_info->id }}_{{ $sql_request_week->product_id }}
                </td>
            </tr>
        </table>

        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="text-align: right;">
                    PREVISÃO DE ENTREGA
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    {{ $sql_request_week->end_at }} 
                </td>
            </tr>            
        </table>

        <hr/>

        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="width: 30%;">
                    CLIENTE
                </td>
                <td style="width: 70%;">
                    {{ $sql_request_week->client }}
                </td>
            </tr>
        </table>

        <hr/> 

        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="width: 30%;">
                    PRODUTO
                </td>
                <td style="width: 70%;">
                    {{ $sql_request_week->montagem }} x {{ $sql_product->name }}
                </td>
            </tr>
        </table>

        <hr/> 

        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="width: 30%;">
                    SEPARADO POR
                </td>
                <td style="width: 40%;">
                    {{ $separado }}
                </td>
                <td style="width: 30%; text-align: right;">
                    em {{ $sql_info->store_date }}
                </td>                
            </tr>
        </table>

        <hr/>
        
        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td style="width: 30%;">
                    AUTORIZADO POR
                </td>
                <td style="width: 40%;">
                    {{ $autorizado }}
                </td>
                <td style="width: 30%; text-align: right;">
                    em {{ $sql_info->auth_date }}
                </td>                
            </tr>
        </table>
        
        <hr/>
        
        <table class="table table-bordered" style="width: 100%; position: fixed; bottom: 0; border-top: 2px solid #000;">            
            <tr>
                <td style="width: 30%;">
                    Processado por NXT | www.next-proaudio.com
                </td>
                <td style="width: 30%; text-align: right;">
                    criado em <?php echo date('Y-m-d H:i:s') ?>
                </td>                
            </tr>
        </table>
        
    </body>
</html>
<!-- pdf.blade.php -->
<!DOCTYPE html>
<style>
    html, body, table, tr, td, p {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
    }
</style>
<html>
    <head>
        <meta charset="utf-8">
        <title>NEXT-proaudio</title>
    </head>
    <body>
        <table class="table table-bordered" style="width: 100%;">
            <tr>
                <td>
                    <img src="{{ asset('assets/gfx/NEXT_b.png')}}"/>
                </td>
                <td style="text-align: right;">
                    <b>Funcionário</b>
                    <br/>{{ $autor }}
                </td>
            </tr>
        </table>
        <hr/>

        <table class="table table-bordered" style="width: 100%;">
            <tr style="background-color: #ccc; font-weight: bold;">
                <td style="width: 26%;">
                    PRODUTIVIDADE
                </td>                
                <td colspan="2" style="text-align: center;">
                    PESSOAL (SEMANA)
                </td>
                @if($date_points > 0)
                <td colspan="2" style="text-align: center;">
                    SECÇÃO (MENSAL)
                </td>
                @endif
            </tr>
            <tr style="background-color: #eee; font-weight: bold;">
                <td style="width: 26%;">
                    (POR SECÇÃO)
                </td>                
                <td style="width: {{ ($date_points == 0) ? "37%" : "11%" }};; text-align: center;">
                    PASSADA
                </td>
                <td style="width: {{ ($date_points == 0) ? "37%" : "11%" }}; text-align: center;">
                    ACTUAL
                </td>
                @if($date_points > 0)
                <td style="width: 25%; text-align: center;">
                    {{$lastRange['rangeStart']}} a {{$lastRange['rangeEnd']}}
                </td>
                <td style="width: 25%; text-align: center;">
                    {{$thisRange['rangeStart']}} a {{$thisRange['rangeEnd']}}
                </td>
                @endif
            </tr>
            @foreach($sections as $key => $section)
            <tr>
                <td style="background-color: #F0F8FF; font-weight: bold; width: 24%;">
                    {{ $section['name'] }}
                </td>                
                <td style="width: {{ ($date_points == 0) ? "37%" : "11%" }}; text-align: center;">
                    {{ $lastWeek[$section['points']] }}
                </td>
                <td style="width: {{ ($date_points == 0) ? "37%" : "11%" }}; text-align: center;">
                    {{ $thisWeek[$section['points']] }}
                </td>
                @if($date_points > 0)
                <td style="width: 25%; text-align: center;background-color: #F0F8FF; text-align: center;">
                    {{ $lastRange[$section['points']] }}
                </td>
                <td style="width: 25%; text-align: center;background-color: #F0F8FF; text-align: center;">
                    {{ $thisRange[$section['points']] }}
                </td>
                @endif
            </tr>
            @endforeach
        </table>

        <hr/>

        <table class="table table-bordered" style="width: 100%;">
            <tr style="background-color: #eee; font-weight: bold;">
                <td>
                    (ÚLTIMOS 12 MESES)<br/>PESSOAL
                </td>
                @foreach($months as $key => $month)
                <td style="width: 5%; text-align: center;">
                    {{ $month['date'] }}
                </td>
                @endforeach
            </tr>
            @foreach($sections as $key1 => $section)
            <tr>
                <td style="background-color: #F0F8FF; font-weight: bold; width: 26%;">
                    {{ $section['name'] }}
                </td>                
               @foreach($months as $key2 => $month)
                <td style="width: 6%;text-align: center;">
                    {{ $month['prod'][$section['points']] }}
                </td>
                @endforeach
            </tr>
            @endforeach
        </table>

        (valores mensais sao compreendidos entre o dia 28 do mês anterior e o dia 27 do mês actual)
        <hr/>        

        <table class="table table-bordered" style="width: 100%; position: fixed; bottom: 0; border-top: 2px solid #000;">            
            <tr>
                <td style="width: 30%;">
                    Processado por NXT | www.next-proaudio.com
                </td>
                <td style="width: 30%; text-align: right;">
                    criado em <?php echo date('Y-m-d H:i:s') ?>
                </td>                
            </tr>
        </table>

    </body>
</html>
FROM --platform=linux/amd64 php:7.1-fpm

# Env variables
ENV ACCEPT_EULA=y

# Copy composer.lock and composer.json
COPY composer.lock composer.json /var/www/

# Set working directory
WORKDIR /var/www

#Install Global stuff
RUN apt-get update && \
    apt-get install -y --force-yes --no-install-recommends \
        libmemcached-dev \
        libz-dev \
        libpq-dev \
        libjpeg-dev \
        libpng-dev \
        libfreetype6-dev \
        libssl-dev \
        libmcrypt-dev \
        openssh-server \
        libmagickwand-dev \
        git \
        cron \
        nano \
        libxml2-dev \ 
        libicu-dev \ 
        curl \
        apt-transport-https \
        locales \
        default-mysql-client

# Microsoft SQL Server Prerequisites

RUN apt-get update && apt-get install -y gnupg

RUN apt-get update \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends --force-yes install \
        unixodbc-dev \
        msodbcsql17 \ 
        freetds-common \ 
        freetds-bin \
        freetds-dev \
        freetds-bin \
        tdsodbc \
        mssql-tools        

RUN set -x ./configure  --with-tdsver=4.2 --with-unixODBC=shared,/usr > /dev/null

# Edit odbc.ini, odbcinst.ini, and freetds.conf files
RUN echo "[PHC]\n\
host = 192.168.1.10\n\
port = 1433\n\
tds version = 4.2" >> /etc/freetds.conf

RUN echo "[FreeTDS]\n\
Description = FreeTDS unixODBC Driver\n\
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so\n\
Setup = /usr/lib/x86_64-linux-gnu/odbc/libtdsS.so\n\
UsageCount = 1" >> /etc/odbcinst.ini

RUN echo "[PHC]\n\
Description = MSSQL Server 2016\n\
Driver = FreeTDS\n\
Database = cvagroup\n\
Server = 192.168.1.10\n\
Port = 1433\n\
TDS_Version =4.2" >> /etc/odbc.ini

RUN docker-php-ext-install mbstring pdo pdo_mysql \
    && pecl install sqlsrv-5.6.1 pdo_sqlsrv-5.6.1 xdebug-2.9.0 \
    && ./configure --with-unixODBC=shared,/usr > /dev/null \
    && echo "extension=pdo_sqlsrv.so" >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/30-pdo_sqlsrv.ini \
    && echo "extension=sqlsrv.so" >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/30-sqlsrv.ini \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug

RUN set -x \
    && cd /usr/src/ && tar -xf php.tar.xz && mv php-7* php \
    && cd /usr/src/php/ext/odbc \
    && phpize \
    && sed -ri 's@^ *test +"\$PHP_.*" *= *"no" *&& *PHP_.*=yes *$@#&@g' configure \
    && docker-php-ext-configure pdo_odbc --with-pdo-odbc=unixODBC,/usr \    
    && ./configure --with-unixODBC=shared,/usr > /dev/null \
    && docker-php-ext-install pdo_odbc odbc gd > /dev/null \
    && export LD_LIBRARY_PATH=/usr/local/lib

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
COPY --chown=www:www . /var/www

# Change current user to www
USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
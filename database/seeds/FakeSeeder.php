<?php

use Faker\Factory;
use App\Model\Request;
use App\Model\RequestUser;
use App\Model\RequestWeek;
use App\Model\WoodUser;
use App\Model\PaintUser;
use App\Model\MountUser;
use App\Model\StockUser;
use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    public function run()
    {
        Request::truncate();
        factory(Request::class, 10)->create();
        
        RequestUser::truncate();
        factory(RequestUser::class, 20)->create();
        
        WoodUser::truncate();
        PaintUser::truncate();
        MountUser::truncate();
        StockUser::truncate();
        RequestWeek::truncate();
    }
}

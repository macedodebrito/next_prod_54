<?php

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Role::truncate();
        DB::table('role_user')->truncate();
        Permission::truncate();

        $adminUser = User::create([
            'name'     => 'admin',
            'bigname'    => 'André Correia',
            'password' => '4321',
        ]);

        $testUser = User::create([
            'name'     => 'manager',
            'bigname'    => 'Sr. Almeida (TESTE)',
            'password' => '1234',
        ]);

        $adminRole = Role::create([
            'name'  => 'admin',
            'label' => 'Administrador'
        ]);

        $newRole = Role::create([
            'name'  => 'cnc',
            'label' => 'CNC'
        ]);        
  
        $newRole = Role::create([
            'name'  => 'carpintaria',
            'label' => 'Carpintaria'
        ]);        
        
         $newRole = Role::create([
            'name'  => 'pintura',
            'label' => 'Pintura'
        ]);

        $newRole = Role::create([
            'name'  => 'montagem',
            'label' => 'Montagem'
        ]);

        $newRole = Role::create([
            'name'  => 'tecnica',
            'label' => 'Técnica'
        ]);
        
        $managerRole = Role::create([
            'name'  => 'manager',
            'label' => 'Manager'
        ]);

        $newRole = Role::create([
            'name'  => 'rh',
            'label' => 'Recursos Humanos'
        ]);

        $newRole = Role::create([
            'name'  => 'stock',
            'label' => 'Gestão de Stock'
        ]);

        $newRole = Role::create([
            'name'  => 'armazem',
            'label' => 'Gestão de Armazém'
        ]);        
        $britoUser = User::create([
            'name'     => 'brito',
            'bigname'    => 'Filipe Brito',
            'password' => '696970',
        ]);
        
        $adminUser->roles()->attach($adminRole);
        $britoUser->roles()->attach($adminRole);
        $testUser->roles()->attach($managerRole);
    }
}

<?php

use App\Model\Serie;
use App\Model\Product;
use App\Model\SerieProduct;
use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Serie::truncate();
        Product::truncate();
        SerieProduct::truncate();

        // LA SERIES

        $serie = Serie::create([
            'name'     => 'LA',
            'order'     => 0,
            ]);

        // LA SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'LA212x',
            'serie_id'     => 1,
            'code'     => "00212",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 1,
            ]);

        $product = Product::create([
            'name'     => 'LA122',
            'serie_id'     => 1,
            'code'     => "00122",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 2,
            ]);

        $product = Product::create([
            'name'     => 'LA122W',
            'serie_id'     => 1,
            'code'     => "00123",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 3,
            ]);

        $product = Product::create([
            'name'     => 'LA122A',
            'serie_id'     => 1,
            'code'     => "00124",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 4,
            ]);

        $product = Product::create([
            'name'     => 'LA122WA',
            'serie_id'     => 1,
            'code'     => "00125",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 5,
            ]);

        $product = Product::create([
            'name'     => 'LAs118A',
            'serie_id'     => 1,
            'code'     => "00527",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 6,
            ]);

        $product = Product::create([
            'name'     => 'LA12',
            'serie_id'     => 1,
            'code'     => "00601",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 7,
            ]);

        $product = Product::create([
            'name'     => 'LA12c',
            'serie_id'     => 1,
            'code'     => "00505",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 8,
            ]);

        $product = Product::create([
            'name'     => 'LA8',
            'serie_id'     => 1,
            'code'     => "00522",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 9,
            ]);

        $product = Product::create([
            'name'     => 'LAs118',
            'serie_id'     => 1,
            'code'     => "00507",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 10,
            ]);

        $product = Product::create([
            'name'     => 'LAs218',
            'serie_id'     => 1,
            'code'     => "00703",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 11,
            ]);

        $product = Product::create([
            'name'     => 'LAs418',
            'serie_id'     => 1,
            'code'     => "00418",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 12,
            ]);

        // LAm SERIES
        $serie = Serie::create([
            'name'     => 'LAm',
            'order'     => 1,
            ]);

        // LAm SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'LAm114xA',
            'serie_id'     => 2,
            'code'     => "00111",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 2,
            'product_id'     => 13,
            ]);

        $product = Product::create([
            'name'     => 'LAm112x',
            'serie_id'     => 2,
            'code'     => "00114",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 2,
            'product_id'     => 14,
            ]);

        $product = Product::create([
            'name'     => 'LAm115D',
            'serie_id'     => 2,
            'code'     => "00119",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 2,
            'product_id'     => 15,
            ]);

        $product = Product::create([
            'name'     => 'LAm115E',
            'serie_id'     => 2,
            'code'     => "00120",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 2,
            'product_id'     => 16,
            ]);

        // PX SERIES
        $serie = Serie::create([
            'name'     => 'PX',
            'order'     => 2,
            ]);

        // PX SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'PXH64',
            'serie_id'     => 3,
            'code'     => "00810",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 3,
            'product_id'     => 17,
            ]);

        $product = Product::create([
            'name'     => 'PXH95',
            'serie_id'     => 3,
            'code'     => "00095",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 3,
            'product_id'     => 18,
            ]);

        $product = Product::create([
            'name'     => 'PXL118_SUB1',
            'serie_id'     => 3,
            'code'     => "00811",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 3,
            'product_id'     => 19,
            ]);

        $product = Product::create([
            'name'     => 'PXL118_SUB2',
            'serie_id'     => 3,
            'code'     => "00813",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 3,
            'product_id'     => 20,
            ]);

        $product = Product::create([
            'name'     => 'PXA8000',
            'serie_id'     => 3,
            'code'     => "00812",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 3,
            'product_id'     => 21,
            ]);

        // CX SERIES
        $serie = Serie::create([
            'name'     => 'CX',
            'order'     => 3,
            ]);        

        // CX SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'CXH64',
            'serie_id'     => 4,
            'code'     => "00800",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 4,
            'product_id'     => 22,
            ]);

        $product = Product::create([
            'name'     => 'CXL151',
            'serie_id'     => 4,
            'code'     => "00801",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 4,
            'product_id'     => 23,
            ]);

        // HFA SERIES
        $serie = Serie::create([
            'name'     => 'HFA',
            'order'     => 4,
            ]);      

        // HFA SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'HFA106p',
            'serie_id'     => 5,
            'code'     => "00036",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 24,
            ]);

        $product = Product::create([
            'name'     => 'HFA206',
            'serie_id'     => 5,
            'code'     => "00006",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 25,
            ]);

        $product = Product::create([
            'name'     => 'HFA206p',
            'serie_id'     => 5,
            'code'     => "00056",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 26,
            ]);

        $product = Product::create([
            'name'     => 'HFA108',
            'serie_id'     => 5,
            'code'     => "00008",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 27,
            ]);

        $product = Product::create([
            'name'     => 'HFA110',
            'serie_id'     => 5,
            'code'     => "00010",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 28,
            ]);

        $product = Product::create([
            'name'     => 'HFA112',
            'serie_id'     => 5,
            'code'     => "00012",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 29,
            ]);

        $product = Product::create([
            'name'     => 'HFA115',
            'serie_id'     => 5,
            'code'     => "00015",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 30,
            ]);

        $product = Product::create([
            'name'     => 'HFA212',
            'serie_id'     => 5,
            'code'     => "00032",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 31,
            ]);

        $product = Product::create([
            'name'     => 'HFA212W',
            'serie_id'     => 5,
            'code'     => "00034",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 32,
            ]);

        $product = Product::create([
            'name'     => 'HFA112s',
            'serie_id'     => 5,
            'code'     => "00013",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 33,
            ]);

        $product = Product::create([
            'name'     => 'HFA115s',
            'serie_id'     => 5,
            'code'     => "00016",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 34,
            ]);

        $product = Product::create([
            'name'     => 'HFA118s',
            'serie_id'     => 5,
            'code'     => "00019",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 35,
            ]);

        $product = Product::create([
            'name'     => 'HFA118sHP',
            'serie_id'     => 5,
            'code'     => "00020",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 36,
            ]);

        // X SERIES
        $serie = Serie::create([
            'name'     => 'X',
            'order'     => 5,
            ]);

        // X SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'X8',
            'serie_id'     => 6,
            'code'     => "00751",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 37,
            ]);

        $product = Product::create([
            'name'     => 'X10',
            'serie_id'     => 6,
            'code'     => "00752",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 38,
            ]);

        $product = Product::create([
            'name'     => 'X12',
            'serie_id'     => 6,
            'code'     => "00753",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 39,
            ]);

        $product = Product::create([
            'name'     => 'X15',
            'serie_id'     => 6,
            'code'     => "00754",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 40,
            ]);

        $product = Product::create([
            'name'     => 'X212',
            'serie_id'     => 6,
            'code'     => "00755",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 41,
            ]);

        $product = Product::create([
            'name'     => 'Xs12',
            'serie_id'     => 6,
            'code'     => "00556",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 42,
            ]);

        $product = Product::create([
            'name'     => 'Xs15',
            'serie_id'     => 6,
            'code'     => "00557",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 43,
            ]);

        $product = Product::create([
            'name'     => 'Xh18D',
            'serie_id'     => 6,
            'code'     => "00662",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 44,
            ]);

        $product = Product::create([
            'name'     => 'Xh18E',
            'serie_id'     => 6,
            'code'     => "00661",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 45,
            ]);

        $product = Product::create([
            'name'     => 'Xs118',
            'serie_id'     => 6,
            'code'     => "00607",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 6,
            'product_id'     => 46,
            ]);

        // PFA SERIES
        $serie = Serie::create([
            'name'     => 'PFA',
            'order'     => 6,
            ]);

        // PFA SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'PFA8p',
            'serie_id'     => 7,
            'code'     => "00048",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 47,
            ]);

        $product = Product::create([
            'name'     => 'PFA8',
            'serie_id'     => 7,
            'code'     => "00028",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 48,
            ]);

        $product = Product::create([
            'name'     => 'PFA12p',
            'serie_id'     => 7,
            'code'     => "00023",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 49,
            ]);

        $product = Product::create([
            'name'     => 'PFA12',
            'serie_id'     => 7,
            'code'     => "00022",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 50,
            ]);

        $product = Product::create([
            'name'     => 'PFA15p',
            'serie_id'     => 7,
            'code'     => "00026",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 51,
            ]);

        $product = Product::create([
            'name'     => 'PFA15',
            'serie_id'     => 7,
            'code'     => "00025",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 52,
            ]);

        $product = Product::create([
            'name'     => 'PFA15s',
            'serie_id'     => 7,
            'code'     => "00027",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 53,
            ]);

        $product = Product::create([
            'name'     => 'PFA15sp',
            'serie_id'     => 7,
            'code'     => "00045",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 54,
            ]);

        $product = Product::create([
            'name'     => 'PFA18s',
            'serie_id'     => 7,
            'code'     => "00029",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 55,
            ]);

        $product = Product::create([
            'name'     => 'PFA18sp',
            'serie_id'     => 7,
            'code'     => "00030",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 56,
            ]);

        $product = Product::create([
            'name'     => 'PFA18sHP',
            'serie_id'     => 7,
            'code'     => "00031",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 57,
            ]);

        $product = Product::create([
            'name'     => 'PFA18spHP',
            'serie_id'     => 7,
            'code'     => "00033",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 7,
            'product_id'     => 58,
            ]);

        // KUBIX SERIES
        $serie = Serie::create([
            'name'     => 'KUBIX',
            'order'     => 7,
            ]);

        // KUBIX SERIES > PRODUCTS
        $product = Product::create([
            'name'     => 'K5',
            'serie_id'     => 8,
            'code'     => "00005",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 59,
            ]);   

        $product = Product::create([
            'name'     => 'K5+',
            'serie_id'     => 8,
            'code'     => "00035",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 60,
            ]); 

        $product = Product::create([
            'name'     => 'K8',
            'serie_id'     => 8,
            'code'     => "00018",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 61,
            ]); 

        $product = Product::create([
            'name'     => 'K8A',
            'serie_id'     => 8,
            'code'     => "00038",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 62,
            ]); 

        $product = Product::create([
            'name'     => 'K12',
            'serie_id'     => 8,
            'code'     => "00612",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 63,
            ]); 

        $product = Product::create([
            'name'     => 'K12A',
            'serie_id'     => 8,
            'code'     => "00312",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 64,
            ]); 

        $product = Product::create([
            'name'     => 'K10s',
            'serie_id'     => 8,
            'code'     => "00110",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 65,
            ]); 

        $product = Product::create([
            'name'     => 'K10sA',
            'serie_id'     => 8,
            'code'     => "00210",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 66,
            ]); 

        $product = Product::create([
            'name'     => 'K12s',
            'serie_id'     => 8,
            'code'     => "00912",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 67,
            ]); 

        $product = Product::create([
            'name'     => 'K12sA',
            'serie_id'     => 8,
            'code'     => "00412",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 8,
            'product_id'     => 68,
            ]); 

           // FIX
        $product = Product::create([
            'name'     => 'LA12c.v2',
            'serie_id'     => 1,
            'code'     => "00605",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 69,
            ]);     
        
        $product = Product::create([
            'name'     => 'LAs418 2x4Ohm',
            'serie_id'     => 1,
            'code'     => "00419",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 70,
            ]);    
        
        $product = Product::create([
            'name'     => 'LA212x.v2',
            'serie_id'     => 1,
            'code'     => "00213",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 1,
            'product_id'     => 71,
            ]);    
        
        $product = Product::create([
            'name'     => 'HFA115sHP',
            'serie_id'     => 5,
            'code'     => "00117",
            ]);

        $serieproduct = SerieProduct::create([
            'serie_id'     => 5,
            'product_id'     => 72,
            ]);        
    }
}

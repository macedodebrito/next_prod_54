<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreateLocksmith extends Migration {

    public function up() {
        Schema::create('locksmith', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('name');
            $table->integer('points');
            $table->timestamps();
        });

        Schema::create('locksmith_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locksmith_id');
            $table->string('pp');
            $table->integer('value');
            $table->timestamps();
            $table->timestamp('start_at');
            $table->timestamp('order_at');
        });
        Schema::create('locksmith_requests_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locksmith_requests_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });
        
        // Importing EXCEL FILE
        $path = public_path();
        $csv = array_map('str_getcsv', file($path . '\..\dump\locksmith_first_import.csv'));
        foreach ($csv as $line) {
            DB::table('locksmith')->insert(
                    [
                        'id' => null,
                        'code' => $line[0],
                        'name' => $line[1],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]
            );
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('locksmith');
        Schema::drop('locksmith_requests');
        Schema::drop('locksmith_requests_users');
    }

}

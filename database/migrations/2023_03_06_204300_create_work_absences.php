<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class CreateWorkAbsences extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_absences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');     // funcionario
            $table->string('title');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->string('obs');          // obs
            $table->integer('create_id');     // quem criou
            $table->integer('valid_id');     // quem validou
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_absences');
       
    }

}

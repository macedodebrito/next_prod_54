<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComsumablesAddConsumables extends Migration {

    public function up() {
        Schema::table('comsumables', function($table) {
            $table->integer('comsumables')->after('armazem')->default(0);
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables', function($table) {
            $table->dropColumn('comsumables');
        });       
    }

}
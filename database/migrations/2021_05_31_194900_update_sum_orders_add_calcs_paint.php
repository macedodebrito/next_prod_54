<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumOrdersAddCalcsPaint extends Migration {

    public function up() {
        Schema::table('sum_orders', function($table) {
            $table->integer('paint_order_amount')->default(0)->after('woodfinishing_calc_amount');
            $table->integer('paint_amount')->default(0)->after('paint_order_amount');
            $table->timestamp('paint_date')->after('paint_amount');
            $table->date('paint_date_simple')->after('paint_date');
            $table->integer('paint_temp_amount')->default(0)->after('paint_date_simple');
            $table->integer('paint_calc_amount')->default(0)->after('paint_temp_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('paint_order_amount');
            $table->dropColumn('paint_amount');
            $table->dropColumn('paint_date');
            $table->dropColumn('paint_date_simple');
            $table->dropColumn('paint_temp_amount');
            $table->dropColumn('paint_calc_amount');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreateCustomizations extends Migration {

    public function up() {
        Schema::create('customizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('value');
            $table->integer('color_wood');
            $table->integer('color_steel');
            $table->integer('custom');
            $table->integer('montagem');
            $table->integer('serralharia');
            $table->integer('tecnica');
            $table->timestamps();
        });
        DB::table('customizations')->insert(
                [
                    'id' => null,
                    'name' => 'Azul',
                    'value' => 'Azul',
                    'color_wood' => 1,
                    'color_steel' => 1,
                    'custom' => 0,
                    'montagem' => 1,
                    'serralharia' => 1,
                    'tecnica' => 0,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Branco',
            'value' => 'Branco',
            'color_wood' => 1,
            'color_steel' => 1,
            'custom' => 0,
            'montagem' => 1,
            'serralharia' => 1,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Creme',
            'value' => 'Creme',
            'color_wood' => 1,
            'color_steel' => 1,
            'custom' => 0,
            'montagem' => 1,
            'serralharia' => 1,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Dourado',
            'value' => 'Dourado',
            'color_wood' => 0,
            'color_steel' => 1,
            'custom' => 0,
            'montagem' => 1,
            'serralharia' => 1,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Preto',
            'value' => 'Preto',
            'color_wood' => 1,
            'color_steel' => 1,
            'custom' => 0,
            'montagem' => 1,
            'serralharia' => 1,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Vermelho',
            'value' => 'Vermelho',
            'color_wood' => 1,
            'color_steel' => 1,
            'custom' => 0,
            'montagem' => 1,
            'serralharia' => 1,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Dante',
            'value' => 'Dante',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => '1+1-',
            'value' => '1+1-',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => '100V',
            'value' => '100V',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'DPA2000.v2',
            'value' => 'DPA2000.v2',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Filtro V5',
            'value' => 'Filtro V5',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Flight Case',
            'value' => 'Flight Case',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]);
        DB::table('customizations')->insert([
            'id' => null,
            'name' => 'Lacado',
            'value' => 'Lacado',
            'color_wood' => 0,
            'color_steel' => 0,
            'custom' => 1,
            'montagem' => 1,
            'serralharia' => 0,
            'tecnica' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('customizations');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdatePaleteLevantamentoAddValue extends Migration {

    public function up() {

        Schema::table('paletes_levantamento', function($table) {
            $table->integer('value')->after('section')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('paletes_levantamento', function($table) {
            $table->dropColumn('value');
        });
    }

}

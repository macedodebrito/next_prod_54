<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateNxtToPhc extends Migration {

    public function up() {
        Schema::create('nxt_to_phc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref');
            $table->string('designation');
            $table->integer('quantity');
            $table->string('serial');
            $table->string('color_wood');
            $table->string('color_steel');
            $table->string('custom');
            $table->string('supplier');
            $table->integer('armazem');
        });

        Schema::table('status_requests', function($table) {
            $table->string('designation')->after('last_update');
        });
        
        Schema::table('locksmith_status_requests', function($table) {
            $table->string('designation')->after('last_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('nxt_to_phc');
        
        Schema::table('status_requests', function($table) {
            $table->dropColumn('designation');
        });      
        
        Schema::table('locksmith_status_requests', function($table) {
            $table->dropColumn('designation');
        });         
    }

}

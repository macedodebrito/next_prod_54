<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateCompaniesTable extends Migration {

    public function up() {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('users_login_2', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('login_logout');
            $table->integer('status');
            $table->timestamps();
        });
        DB::statement('INSERT INTO users_login_2 SELECT * FROM users_login');
        DB::statement("INSERT INTO companies VALUES(null, 'AFCMC')");
        DB::statement("INSERT INTO companies VALUES(null, 'CVA Electrónica')");
        DB::statement("INSERT INTO companies VALUES(null, 'EXTRA')");

        Schema::table('users', function($table) {
            $table->integer('company_id')->before('created_at');
        });

        DB::table('users')->where('id', 1)->update(['company_id' => 2]);
        DB::table('users')->where('id', 3)->update(['company_id' => 1]);
        DB::table('users')->where('id', 4)->update(['company_id' => 1]);
        DB::table('users')->where('id', 5)->update(['company_id' => 1]);
        DB::table('users')->where('id', 6)->update(['company_id' => 1]);
        DB::table('users')->where('id', 7)->update(['company_id' => 1]);
        DB::table('users')->where('id', 8)->update(['company_id' => 2]);
        DB::table('users')->where('id', 9)->update(['company_id' => 2]);
        DB::table('users')->where('id', 12)->update(['company_id' => 2]);
        DB::table('users')->where('id', 13)->update(['company_id' => 1]);
        DB::table('users')->where('id', 14)->update(['company_id' => 1]);
        DB::table('users')->where('id', 15)->update(['company_id' => 1]);
        DB::table('users')->where('id', 16)->update(['company_id' => 1]);
        DB::table('users')->where('id', 17)->update(['company_id' => 2]);
        DB::table('users')->where('id', 18)->update(['company_id' => 2]);
        DB::table('users')->where('id', 19)->update(['company_id' => 1]);
        DB::table('users')->where('id', 20)->update(['company_id' => 3]);
        DB::table('users')->where('id', 22)->update(['company_id' => 2]);
        DB::table('users')->where('id', 23)->update(['company_id' => 1]);
        DB::table('users')->where('id', 24)->update(['company_id' => 1]);
        DB::table('users')->where('id', 25)->update(['company_id' => 1]);
        DB::table('users')->where('id', 26)->update(['company_id' => 1]);
        DB::table('users')->where('id', 27)->update(['company_id' => 1]);
        DB::table('users')->where('id', 28)->update(['company_id' => 1]);
        DB::table('users')->where('id', 30)->update(['company_id' => 1]);
        DB::table('users')->where('id', 31)->update(['company_id' => 1]);
        DB::table('users')->where('id', 32)->update(['company_id' => 2]);
        DB::table('users')->where('id', 33)->update(['company_id' => 1]);
        DB::table('users')->where('id', 35)->update(['company_id' => 1]);
        DB::table('users')->where('id', 37)->update(['company_id' => 2]);
        DB::table('users')->where('id', 38)->update(['company_id' => 1]);
        DB::table('users')->where('id', 41)->update(['company_id' => 3]);
        DB::table('users')->where('id', 42)->update(['company_id' => 2]);
        DB::table('users')->where('id', 43)->update(['company_id' => 3]);
        DB::table('users')->where('id', 44)->update(['company_id' => 1]);
        DB::table('users')->where('id', 46)->update(['company_id' => 3]);
        DB::table('users')->where('id', 47)->update(['company_id' => 3]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('companies');
        Schema::drop('users_login_2');
        Schema::table('users', function($table) {
            $table->dropColumn('company_id');
        });
    }

}

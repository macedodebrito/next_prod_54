<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhcSemanasProductsAddCativo extends Migration {

    public function up() {
        Schema::table('phc_semanas_products', function($table) {
            $table->integer('cativo');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('phc_semanas_products', function($table) {
            $table->dropColumn('cativo');
        });       
    }

}
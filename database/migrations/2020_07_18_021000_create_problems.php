<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateProblems extends Migration {

    public function up() {
        Schema::create('problems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section_type', 64);
            $table->integer('user');            
            $table->longText('text');
            $table->longText('obs');
            $table->integer('status');
            $table->timestamps();
        });     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('problems');
    }

}

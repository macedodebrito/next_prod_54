<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateLocksmithAddSectionsValues extends Migration {

    public function up() {

        Schema::table('locksmith', function($table) {
            $table->integer('cnc')->after('points')->default(1);
            $table->integer('store')->after('cnc')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith', function($table) {
            $table->dropColumn('store');
            $table->dropColumn('cnc');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateCheckConfig extends Migration {

    public function up() {
        Schema::create('check_config', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('updating');
            $table->integer('pp');
            $table->integer('pp_orders');
            $table->integer('cnc');
            $table->integer('carpintaria');
            $table->integer('acabamento');
            $table->integer('pintura');
            $table->integer('montagem');
        });
        DB::table('check_config')->insert(['id' => null, 'updating' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('check_config');
    }

}

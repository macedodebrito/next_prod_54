<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration {

    public function up() {
        Schema::create('config', function (Blueprint $table) {
            $table->integer('limits_cnc');
            $table->integer('limits_wood');
            $table->integer('limits_woodfinishing');
            $table->integer('limits_paint');
            $table->integer('limits_mount');
            $table->timestamps();
        });
        
        Schema::create('versions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->text('log');
            $table->boolean('status');
            $table->timestamps();
        });        
        
        Schema::create('versions_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('message');
            $table->boolean('status');
            $table->timestamps();
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('config');
        Schema::drop('versions');
        Schema::drop('versions_logs');
    }

}

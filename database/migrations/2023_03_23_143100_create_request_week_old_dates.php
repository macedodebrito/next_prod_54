<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class CreateRequestWeekOldDates extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_week_old_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_week_id');
            $table->timestamp('end_at');
            $table->timestamp('end_paint_at');
            $table->timestamp('end_woodfinishing_at');
            $table->timestamp('end_wood_at');
        });
        
        // MIRROR INFO
        $request_week_dates = DB::table('request_week')->select('id', 'end_at', 'end_paint_at', 'end_woodfinishing_at', 'end_wood_at')->orderBy('id', 'asc')->get()->toArray();

        foreach ($request_week_dates as $old_date) {
            DB::table('request_week_old_dates')->insert([
                'id' => null, 
                'request_week_id' => $old_date->id,
                'end_at' => $old_date->end_at,
                'end_paint_at' => $old_date->end_paint_at,
                'end_woodfinishing_at' => $old_date->end_woodfinishing_at,
                'end_wood_at' => $old_date->end_wood_at
            ]);            
        }
        
        // ALTER INFO TO NEW DATES
        $change_request_week_dates = DB::table('request_week')->select('id', 'end_at', 'end_paint_at', 'end_woodfinishing_at', 'end_wood_at')->orderBy('id', 'asc')->get()->toArray();
        
        foreach ($change_request_week_dates as $new_date) {
            
            $new_end_paint_at = Carbon::createFromFormat('Y-m-d H:i:s', $new_date->end_at)->subDays(4)->format('Y-m-d H:i:s');
            $new_end_woodfinishing_at = Carbon::createFromFormat('Y-m-d H:i:s', $new_date->end_at)->subDays(7)->format('Y-m-d H:i:s');
            $new_end_wood_at = Carbon::createFromFormat('Y-m-d H:i:s', $new_date->end_at)->subDays(10)->format('Y-m-d H:i:s');

            DB::table('request_week')->where('id', $new_date->id)->update([
                'end_paint_at' => $new_end_paint_at,
                'end_woodfinishing_at' => $new_end_woodfinishing_at,
                'end_wood_at' => $new_end_wood_at
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // ROLLBACK DATES TO OLD
        $change_to_old_request_week_dates = DB::table('request_week_old_dates')->select('request_week_id', 'end_at', 'end_paint_at', 'end_woodfinishing_at', 'end_wood_at')->orderBy('id', 'asc')->get()->toArray();
        
        foreach ($change_to_old_request_week_dates as $back_to_old_date) {
            DB::table('request_week')->where('id', $back_to_old_date->request_week_id)->update([
                'end_paint_at' => $back_to_old_date->end_paint_at,
                'end_woodfinishing_at' => $back_to_old_date->end_woodfinishing_at,
                'end_wood_at' => $back_to_old_date->end_wood_at
            ]);
        }
        
        Schema::drop('request_week_old_dates');       
    }

}

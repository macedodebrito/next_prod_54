<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreateConsumablesTable extends Migration {

    public function up() {
        Schema::create('comsumables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref');
            $table->string('nome');
            $table->integer('cnc');
            $table->integer('carpintaria');
            $table->integer('acabamento');
            $table->integer('pintura');
            $table->integer('montagem');
            $table->integer('serralharia');
            $table->integer('tecnica');
            $table->integer('redes');
            $table->integer('serigrafia');
            $table->integer('filtros');
            $table->string('unit');
            $table->timestamps();
        });

        Schema::create('comsumables_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auth_id');
            $table->string('section');
            $table->timestamps();
        });
        
        Schema::create('comsumables_orders_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comsumables_order_id');
            $table->integer('comsumables_id');
            $table->integer('value');
            $table->timestamps();
        });

        // Importing EXCEL FILE
        $path = public_path();
        $csv = array_map('str_getcsv', file($path . '\..\dump\comsumables.csv'));
        foreach ($csv as $line) {
            DB::table('comsumables')->insert(
                    [
                        'id' => null,
                        'ref' => $line[0],
                        'nome' => $line[1],
                        'cnc' => $line[2],
                        'carpintaria' => $line[3],
                        'acabamento' => $line[4],
                        'pintura' => $line[5],
                        'montagem' => $line[6],
                        'serralharia' => $line[7],
                        'tecnica' => $line[8],
                        'redes' => $line[9],
                        'serigrafia' => $line[10],
                        'filtros' => $line[11],
                        'unit' => $line[12],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('comsumables');
        Schema::drop('comsumables_orders');
        Schema::drop('comsumables_orders_items');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class LevantamentoDePaletes extends Migration {

    public function up() {
        Schema::create('paletes_levantamento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('object_id');            
            $table->string('section');            
            $table->integer('levantamento_user_id');
            $table->timestamp('levantamento_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('paletes_levantamento');
    }

}

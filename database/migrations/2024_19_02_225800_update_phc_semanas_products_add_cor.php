<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class UpdatePhcSemanasProductsAddCor extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phc_semanas_products', function($table) {
            $table->string('cor');
            $table->boolean('texteis');
            $table->string('ref');
            $table->string('codigo');
            $table->integer('stock');
            $table->integer('qttrec');
            $table->integer('stock_cor');
            $table->integer('qttrec_cor');
            $table->dropColumn('cativo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phc_semanas_products', function($table) {
            $table->dropColumn('cor');
            $table->dropColumn('texteis');
            $table->dropColumn('ref');
            $table->dropColumn('codigo');
            $table->dropColumn('stock');
            $table->dropColumn('qttrec');
            $table->dropColumn('stock_cor');
            $table->dropColumn('qttrec_cor');
            $table->string('cativo');            
        });
    }

}

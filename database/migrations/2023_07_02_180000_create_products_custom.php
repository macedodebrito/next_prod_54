<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class CreateProductsCustom extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_custom', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('custom_id')->nullable();
            $table->integer('product_id');
            $table->string('custom_code')->nullable();
            $table->string('nc_code');            
            $table->string('name')->nullable();
        });

        // NXT TABLE
        Schema::create('products_custom_nxt', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_cor');
            $table->string('name');
            $table->string('hex_color');
            $table->string('color_code');
        });
        DB::table('products_custom_nxt')->insert([
            'id' => null, 
            'name_cor' => 'Unknown',
            'name' => 'Por Definir',
            'color_code' => 'UNK',
            'hex_color' => 'FF0000'
        ]);        
        DB::table('products_custom_nxt')->insert([
            'id' => null, 
            'name_cor' => 'Black Polyurea',
            'name' => 'Polyurea (Preto)',
            'color_code' => 'BPU',
            'hex_color' => '000000'
        ]);
        DB::table('products_custom_nxt')->insert([
            'id' => null, 
            'name_cor' => 'Black Textured',
            'name' => 'Texturado (Preto)',
            'color_code' => 'BTX',
            'hex_color' => '999999'
        ]);
        DB::table('products_custom_nxt')->insert([
            'id' => null, 
            'name_cor' => 'White Textured',
            'name' => 'Texturado (Branco)',
            'color_code' => 'WTX',
            'hex_color' => 'CCCCCC'
        ]);        

        Schema::table('requests', function($table) {
            $table->integer('custom_id')->after('product_id');
        });
        // default to "textured"
        DB::statement('UPDATE requests SET custom_id=3 where id!="X"');

        Schema::table('sum_mount', function($table) {
            $table->string('color_name')->after('request_id');
            $table->string('color_hex')->after('color_name');
        });
        // default to "textured"
        DB::table('sum_mount')->where('id',"!=","X")->update( ['color_name' => "Black Textured", 'color_hex' => '999999'] );

        Schema::table('request_week', function($table) {
            $table->string('custom_id')->after('product_id');
        });
        // default to "textured"
        DB::table('request_week')->where('id',"!=","X")->update( ['custom_id' => 3] );

        Schema::table('status_requests', function($table) {
            $table->string('custom_color')->after('product_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_custom');
        Schema::drop('products_custom_nxt');
        Schema::table('requests', function($table) {
            $table->dropColumn('custom_id');
        });
        Schema::table('sum_mount', function($table) {
            $table->dropColumn('color_name');
            $table->dropColumn('color_hex');
        });
        Schema::table('request_week', function($table) {
            $table->dropColumn('custom_id');
        });
        Schema::table('status_requests', function($table) {
            $table->dropColumn('custom_color');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaintTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wood_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });    
        
        Schema::create('paint_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });  
        
        Schema::create('mount_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('serial');          // serial
            $table->integer('quantity');       // quantos
            $table->integer('last_serial');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });        
        
        Schema::create('stock_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('mount_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->timestamps();
        });          
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wood_users');
        Schema::drop('paint_users');
        Schema::drop('mount_users');
        Schema::drop('stock_users');
    }

}

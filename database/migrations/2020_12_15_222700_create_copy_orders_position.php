<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateCopyOrdersPosition extends Migration {

    public function up() {
        Schema::create('copy_order_position', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section_type', 64);
            $table->integer('order_id');
            $table->integer('order_position');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('copy_order_position');
    }

}

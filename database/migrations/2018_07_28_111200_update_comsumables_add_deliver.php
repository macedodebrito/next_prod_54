<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class UpdateComsumablesAddDeliver extends Migration {

    public function up() {
        Schema::table('comsumables', function($table) {
            $table->integer('deliver')->after('unit')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables', function($table) {
            $table->dropColumn('deliver');
        });
    }

}

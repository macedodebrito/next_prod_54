<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateStatusRequestsExtra extends Migration {

    public function up() {
        Schema::table('status_requests', function($table) {
            $table->integer('extra')->after('value');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('status_requests', function($table) {
            $table->dropColumn('extra');
        });       
    }

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('bigname');
            $table->string('password', 60)->unique();
            $table->rememberToken();
            $table->timestamps();
        });
        
        Schema::create('users_login', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('login_logout');
            $table->integer('status');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('users_login');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsAddMiniCode extends Migration {

    public function up() {
        Schema::table('products', function($table) {
            $table->string('mini_code')->after('serie_id')->default("NC");
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('products', function($table) {
            $table->dropColumn('mini_code');
        });       
    }

}
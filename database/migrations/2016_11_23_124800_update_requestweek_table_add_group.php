<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestweekTableAddGroup extends Migration {

    public function up() {
        Schema::table('request_week', function($table) {
            $table->integer('group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('request_week', function($table) {
            $table->dropColumn('group');
        });
    }

}

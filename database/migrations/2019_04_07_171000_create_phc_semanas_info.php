<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreatePhcSemanasInfo extends Migration {

    public function up() {
        Schema::create('phc_semanas_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp');
            $table->timestamp('new_date')->nullable();
            $table->integer('locked');
            $table->integer('auth_id');
            $table->timestamp('auth_id_date')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('phc_semanas_info');
    }

}

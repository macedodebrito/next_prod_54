<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumOrdersAddCalcsWoodfinishing extends Migration {

    public function up() {
        Schema::table('sum_orders', function($table) {
            $table->integer('woodfinishing_order_amount')->default(0)->after('wood_calc_amount');
            $table->integer('woodfinishing_amount')->default(0)->after('woodfinishing_order_amount');
            $table->timestamp('woodfinishing_date')->after('woodfinishing_amount');
            $table->date('woodfinishing_date_simple')->after('woodfinishing_date');
            $table->integer('woodfinishing_temp_amount')->default(0)->after('woodfinishing_date_simple');
            $table->integer('woodfinishing_calc_amount')->default(0)->after('woodfinishing_temp_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('woodfinishing_order_amount');
            $table->dropColumn('woodfinishing_amount');
            $table->dropColumn('woodfinishing_date');
            $table->dropColumn('woodfinishing_date_simple');
            $table->dropColumn('woodfinishing_temp_amount');
            $table->dropColumn('woodfinishing_calc_amount');
        });
    }

}

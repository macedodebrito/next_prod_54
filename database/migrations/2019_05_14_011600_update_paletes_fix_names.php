<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdatePaletesFixNames extends Migration {

    public function up() {

        Schema::table('products', function($table) {
            $table->renameColumn('espuma', "packing_cnc");
            $table->renameColumn('packing', "packing_espuma");
        });
        
        Schema::table('paletes', function($table) {
            $table->renameColumn('espuma_id', 'packing_cnc_id');
            $table->renameColumn('espuma_date', 'packing_cnc_date');
            $table->renameColumn('espuma_auth_id', 'packing_cnc_auth_id');
            $table->renameColumn('espuma_auth_date', 'packing_cnc_auth_date');
            
            $table->renameColumn('packing_id', 'packing_espuma_id');
            $table->renameColumn('packing_date', 'packing_espuma_date');
            $table->renameColumn('packing_auth_id', 'packing_espuma_auth_id');
            $table->renameColumn('packing_auth_date', 'packing_espuma_auth_date');            
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('products', function($table) {
            $table->renameColumn('packing_cnc', 'espuma');
        });
        
        Schema::table('paletes', function($table) {
            $table->renameColumn('packing_cnc_id', 'espuma_id');
            $table->renameColumn('packing_cnc_date', 'espuma_date');
            $table->renameColumn('packing_cnc_auth_id', 'espuma_auth_id');
            $table->renameColumn('packing_cnc_auth_date', 'espuma_auth_date');
            
            $table->renameColumn('packing_espuma_id', 'packing_id');
            $table->renameColumn('packing_espuma_date', 'packing_date');
            $table->renameColumn('packing_espuma_auth_id', 'packing_auth_id');
            $table->renameColumn('packing_espuma_auth_date', 'packing_auth_date');            
        });        
    }

}

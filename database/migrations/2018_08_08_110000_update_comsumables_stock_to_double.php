<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComsumablesStockToDouble extends Migration {

    public function up() {
        Schema::table('comsumables_orders_items', function($table) {
            $table->decimal('stock', 15, 8)->change();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables_orders_items', function($table) {
            $table->integer('stock')->change();
        });       
    }

}
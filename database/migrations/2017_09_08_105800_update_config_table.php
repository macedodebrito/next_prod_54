<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateConfigTable extends Migration {

    public function up() {
        Schema::table('config', function($table) {
            $table->increments('id')->before('limits_cnc');
            $table->integer('week_0')->after('limits_mount');
            $table->integer('week_1')->after('week_0');
            $table->integer('week_2')->after('week_1');
            $table->integer('week_3')->after('week_2');
        });

        DB::table('config')->insert([
            "limits_cnc" => 12800,
            "limits_wood" => 12800,
            "limits_woodfinishing" => 12800,
            "limits_paint" => 12800,
            "limits_mount" => 12800,
            "week_0" => 3200,
            "week_1" => 3200,
            "week_2" => 3200,
            "week_3" => 3200,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('config', function($table) {
            $table->dropColumn('id');
            $table->dropColumn('week_0');
            $table->dropColumn('week_1');
            $table->dropColumn('week_2');
            $table->dropColumn('week_3');
        });
    }

}
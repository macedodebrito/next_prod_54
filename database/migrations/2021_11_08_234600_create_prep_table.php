<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class CreatePrepTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->boolean('prep');
            $table->timestamps();
        });

        DB::statement('INSERT INTO prep_users SELECT * FROM request_users WHERE prep!=9');  

        DB::statement('UPDATE request_users SET published=1 where published=0');
        DB::statement('UPDATE request_users SET prep=1 where prep=0');
        DB::statement('UPDATE prep_users SET prep=1 where prep=0');

        DB::table('request_users')->where('valid_id',0)->update( ['valid_id' => \DB::raw("`user_id`")] );

        Schema::table('request_users', function($table) {
            $table->integer('published')->default(1)->change();
            $table->integer('prep')->default(1)->change();
        });

        DB::table('sum_dates')->insert(['id' => null, 'section' => 'prep', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);

        Schema::table('check_config', function($table) {
            $table->integer('prep')->default(0)->after('cnc');
        });

        Schema::table('sum_orders', function($table) {
            $table->integer('prep_order_amount')->default(0)->after('cnc_calc_amount');
            $table->timestamp('prep_date')->after('prep_amount');
            $table->date('prep_date_simple')->after('prep_date');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prep_users');
        Schema::table('request_users', function($table) {
            $table->integer('published')->default(0)->change();
            $table->integer('prep')->default(0)->change();
        });

        DB::table('sum_dates')->where('section', 'prep')->delete();
        
        Schema::table('check_config', function($table) {
            $table->dropColumn('prep');
        });
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('prep_order_amount');
            $table->dropColumn('prep_date');
            $table->dropColumn('prep_date_simple');
        });
    }

}

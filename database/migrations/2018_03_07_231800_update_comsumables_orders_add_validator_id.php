<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComsumablesOrdersAddValidatorId extends Migration {

    public function up() {
        Schema::table('comsumables_orders', function($table) {
            $table->integer('validator_id')->after('section');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables_orders', function($table) {
            $table->dropColumn('validator_id');
        });       
    }

}
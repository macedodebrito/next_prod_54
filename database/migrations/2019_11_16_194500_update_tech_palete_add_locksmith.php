<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateTechPaleteAddLocksmith extends Migration {

    public function up() {

        Schema::table('tech_palete', function($table) {
            $table->integer('locksmith_id');
            $table->timestamp('locksmith_date')->nullable();
            $table->integer('locksmith_auth_id');
            $table->timestamp('locksmith_auth_date')->nullable();
            $table->integer('status');
        });
        DB::statement('UPDATE tech_palete SET status=2 WHERE auth_id!=0');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tech_palete', function($table) {
            $table->dropColumn('locksmith_id');
            $table->dropColumn('locksmith_date');
            $table->dropColumn('locksmith_auth_id');
            $table->dropColumn('locksmith_auth_date');
            $table->dropColumn('status');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateProductsAddEspuma extends Migration {

    public function up() {

        Schema::table('products', function($table) {
            $table->integer('espuma');
        });
        
        Schema::table('paletes', function($table) {
            $table->integer('espuma_id');
            $table->integer('espuma_date');
            $table->integer('espuma_auth_id');
            $table->integer('espuma_auth_date');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('products', function($table) {
            $table->dropColumn('espuma');
        });
        
        Schema::table('palete', function($table) {
            $table->dropColumn('espuma_id');
            $table->dropColumn('espuma_date');
            $table->dropColumn('espuma_auth_id');
            $table->dropColumn('espuma_auth_date');
        });        
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class UpdateProductsCustomNxt extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_custom_nxt', function($table) {
            $table->string('hex_color_bg')->nullable()->after('hex_color');
        });

        Schema::table('sum_mount', function($table) {
            $table->string('color_hex_bg')->nullable()->after('color_hex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_custom_nxt', function($table) {
            $table->dropColumn('hex_color_bg');
        });

        Schema::table('sum_mount', function($table) {
            $table->dropColumn('color_hex_bg');
        });        
    }

}

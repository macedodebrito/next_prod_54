<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateTripleweekTable extends Migration {

    public function up() {
        Schema::table('triple_week', function($table) {
            $table->integer('requestweek_id')->after('group');
        });
        Schema::table('freeze_week', function($table) {
            $table->integer('requestweek_id')->after('week');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('triple_week', function($table) {
            $table->dropColumn('requestweek_id');
        });
        Schema::table('freeze_week', function($table) {
            $table->dropColumn('requestweek_id');
        });        
    }

}
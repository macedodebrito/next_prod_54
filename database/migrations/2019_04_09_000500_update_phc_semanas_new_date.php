<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhcSemanasNewDate extends Migration {

    public function up() {
        Schema::table('phc_semanas', function($table) {
            $table->timestamp('new_date');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('phc_semanas', function($table) {
            $table->dropColumn('new_date');
        });       
    }

}
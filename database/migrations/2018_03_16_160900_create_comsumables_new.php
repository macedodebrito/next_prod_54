<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateComsumablesNew extends Migration {

    public function up() {
        Schema::create('comsumables_new', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comsumables_order_id');
            $table->string('name');
            $table->integer('value');
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('comsumables_new');
    }

}

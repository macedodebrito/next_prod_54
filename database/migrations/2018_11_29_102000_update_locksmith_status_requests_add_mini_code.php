<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLocksmithStatusRequestsAddMiniCode extends Migration {

    public function up() {
        Schema::table('locksmith_status_requests', function($table) {
            $table->string('mini_code')->after('locksmith_id');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith_status_requests', function($table) {
            $table->dropColumn('mini_code');
        });       
    }

}
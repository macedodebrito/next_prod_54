<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAddDatePoints extends Migration {

    public function up() {
        Schema::table('users', function($table) {
            $table->integer('date_points');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function($table) {
            $table->dropColumn('date_points');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateSumDates extends Migration {

    public function up() {
        Schema::create('sum_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section');
            $table->integer('value');
            $table->timestamps();
        });
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'pp', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'pp_orders', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'cnc', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);        
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'carpintaria', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'acabamento', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'pintura', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'montagem', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'serralharia', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'redes', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        DB::table('sum_dates')->insert(['id' => null, 'section' => 'tecnica', 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sum_dates');
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhcSemanasAddRotulo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('phc_semanas', function($table) {
            $table->integer('u_etqprint');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('phc_semanas', function($table) {
            $table->dropColumn('u_etqprint');
        });
    }

}

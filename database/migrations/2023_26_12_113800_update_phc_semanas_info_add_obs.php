<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class UpdatePhcSemanasInfoAddObs extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phc_semanas_info', function($table) {
            $table->text('obs');
            $table->integer('obs_user_id');
            $table->timestamp('obs_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phc_semanas_info', function($table) {
            $table->dropColumn('obs');
            $table->dropColumn('obs_user_id');
            $table->dropColumn('obs_date');
        });
    }

}

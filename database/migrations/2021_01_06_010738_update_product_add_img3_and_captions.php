<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductAddImg3AndCaptions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('products', function($table) {
            $table->string('img_3');
            $table->string('caption_1');
            $table->string('caption_2');
            $table->string('caption_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('products', function($table) {
            $table->dropColumn('img_3');
            $table->dropColumn('caption_1');
            $table->dropColumn('caption_2');
            $table->dropColumn('caption_3');
        });
    }

}

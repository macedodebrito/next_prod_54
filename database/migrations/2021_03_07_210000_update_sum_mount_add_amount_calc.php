<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumMountAddAmountCalc extends Migration {

    public function up() {
        Schema::table('sum_mount', function($table) {
            $table->integer('prep')->default(0)->after('cnc');
            $table->integer('cnc_amount')->default(0)->after('request_amount');
            $table->integer('prep_amount')->default(0)->after('cnc_amount');
            $table->integer('wood_amount')->default(0)->after('prep_amount');
            $table->integer('woodfinishing_amount')->default(0)->after('wood_amount');
            $table->integer('paint_amount')->default(0)->after('woodfinishing_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_mount', function($table) {
            $table->dropColumn('prep');
            $table->dropColumn('cnc_amount');
            $table->dropColumn('prep_amount');
            $table->dropColumn('wood_amount');
            $table->dropColumn('woodfinishing_amount');
            $table->dropColumn('paint_amount');
        });
    }

}

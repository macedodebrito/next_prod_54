<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhcSemanasAdd3Fields extends Migration {

    public function up() {
        Schema::table('phc_semanas', function($table) {
            $table->integer('u_matcomp');
            $table->integer('u_packing');
            $table->string('u_packing_url');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tech_status_requests', function($table) {
            $table->dropColumn('u_matcomp');
            $table->dropColumn('u_packing');
            $table->dropColumn('u_packing_url');
        });       
    }

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateV2PHCMountOutput extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phc_mount_output_bo', function (Blueprint $table) {            
            $table->uuid('bostamp');
            $table->nullableTimestamps();
            $table->integer('cron');
            $table->string('nmdos');
            $table->integer('ndos');
            $table->integer('fechada');
            $table->increments('obrano');
            $table->timestamp('dataobra');
            $table->timestamp('dataopen');
            $table->integer('no');
            $table->string('nome');
            $table->integer('boano');
            $table->string('datafinal');
            $table->integer('etotaldeb');
            $table->string('ousrinis');
            $table->string('usrinis');
            $table->integer('sqtt14');
            $table->integer('ocupacao');
            $table->integer('lifref');
            $table->timestamp('ousrdata');
            $table->time('ousrhora');
            $table->timestamp('usrdata');
            $table->time('usrhora');
            $table->string('memissao');            
        });

        Schema::create('phc_mount_output_bi', function (Blueprint $table) {
            $table->uuid('bistamp');
            $table->nullableTimestamps();
            $table->integer('cron');
            $table->string('bostamp');
            $table->string('nmdos');
            $table->integer('ndos');
            $table->integer('obrano');
            $table->timestamp('dataobra');
            $table->timestamp('dataopen');
            $table->timestamp('rdata');
            $table->string('ref');
            $table->string('design');
            $table->integer('qtt');
            $table->integer('stipo');
            $table->string('lobs');
            $table->string('lobs2');
            $table->integer('edebito');
            $table->integer('ettdeb');
            $table->integer('IVA');
            $table->integer('TABIVA');
            $table->string('unidade');
            $table->integer('armazem');
            $table->boolean('texteis');
            $table->string('cor');
            $table->string('tam');
            $table->string('ousrinis');
            $table->string('usrinis');
            $table->integer('no');
            $table->integer('resrec');
            $table->integer('tabfor');
            $table->integer('posic');
            $table->integer('lrecno');
            $table->integer('lordem');
            $table->integer('lifref');
            $table->timestamp('ousrdata');
            $table->time('ousrhora');
            $table->timestamp('usrdata');
            $table->time('usrhora');
            $table->string('nome');
            $table->string('series');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phc_mount_output_bo');
        Schema::drop('phc_mount_output_bi');
    }

}

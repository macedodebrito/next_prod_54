<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumOrdersAddCalcsWood extends Migration {

    public function up() {
        Schema::table('sum_orders', function($table) {
            $table->integer('wood_order_amount')->default(0)->after('prep_calc_amount');
            $table->integer('wood_amount')->default(0)->after('wood_order_amount');
            $table->timestamp('wood_date')->after('wood_amount');
            $table->date('wood_date_simple')->after('wood_date');
            $table->integer('wood_temp_amount')->default(0)->after('wood_date_simple');
            $table->integer('wood_calc_amount')->default(0)->after('wood_temp_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('wood_order_amount');
            $table->dropColumn('wood_amount');
            $table->dropColumn('wood_date');
            $table->dropColumn('wood_date_simple');
            $table->dropColumn('wood_temp_amount');
            $table->dropColumn('wood_calc_amount');
        });
    }

}

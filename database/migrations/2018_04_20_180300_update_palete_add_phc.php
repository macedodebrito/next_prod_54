<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaleteAddPhc extends Migration {

    public function up() {
        Schema::table('paletes', function($table) {
            $table->integer('phc_id')->after('request_week_id')->default(0);
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('paletes', function($table) {
            $table->dropColumn('phc_id');
        });       
    }

}
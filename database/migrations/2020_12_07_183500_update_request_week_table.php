<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateRequestWeekTable extends Migration {

    public function up() {
        DB::unprepared('alter table request_week CHANGE end_at end_at timestamp NOT NULL default CURRENT_TIMESTAMP;');
    }
    public function down() {        
    }
}

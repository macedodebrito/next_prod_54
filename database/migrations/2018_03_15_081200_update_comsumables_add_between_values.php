<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class UpdateComsumablesAddBetweenValues extends Migration {

    public function up() {
        Schema::table('comsumables', function($table) {
            $table->integer('between_start')->after('unit')->default(1);
            $table->integer('between_end')->after('between_start')->default(1);
        });

        // Importing EXCEL FILE
        $path = public_path();
        $csv = array_map('str_getcsv', file($path . '\..\dump\comsumables_update_1.csv'));
        foreach ($csv as $line) {
            if ($line[0]) {
                $check = \App\Model\Comsumables::where('ref', $line[0])->count();
                // EXISTE
                if ($check == 1) {
                    print "EXISTE " . $line[0];
                    if (!$line[13]) {
                        print " default_0\n";
                        $between_start = 1;
                        $between_end = 5;
                    } else {
                        print "\n";
                        $between_start = $line[13];
                        $between_end = $line[14];
                    }
                    DB::table('comsumables')->where('ref', "=", $line[0])->update(
                            [
                                'between_start' => $between_start,
                                'between_end' => $between_end,
                            ]
                    );
                }
                // NOVO
                else {
                    print "- NOVO " . $line[0];
                    if (!$line[13]) {
                        print " default_0\n";
                        $between_start = 1;
                        $between_end = 5;
                    } else {
                        print "\n";
                        $between_start = $line[13];
                        $between_end = $line[14];
                    }
                    DB::table('comsumables')->insert(
                            [
                                'id' => null,
                                'ref' => $line[0],
                                'nome' => $line[1],
                                'cnc' => $line[2],
                                'carpintaria' => $line[3],
                                'acabamento' => $line[4],
                                'pintura' => $line[5],
                                'montagem' => $line[6],
                                'serralharia' => $line[7],
                                'tecnica' => $line[8],
                                'redes' => $line[9],
                                'serigrafia' => $line[10],
                                'filtros' => $line[11],
                                'unit' => $line[12],
                                'between_start' => $between_start,
                                'between_end' => $between_end,
                                'created_at' => Carbon::now()->toDateTimeString(),
                                'updated_at' => Carbon::now()->toDateTimeString()
                            ]
                    );
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables', function($table) {
            $table->dropColumn('between_start');
            $table->dropColumn('between_end');
        });
    }

}

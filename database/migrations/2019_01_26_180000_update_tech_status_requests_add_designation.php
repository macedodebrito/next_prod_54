<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTechStatusRequestsAddDesignation extends Migration {

    public function up() {
        Schema::table('tech_status_requests', function($table) {
            $table->string('designation');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tech_status_requests', function($table) {
            $table->dropColumn('designation');
        });       
    }

}
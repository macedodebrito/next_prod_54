<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComsumablesOrdersItemsAddStock extends Migration {

    public function up() {
        Schema::table('comsumables_orders_items', function($table) {
            $table->integer('stock')->after('value');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables_orders_items', function($table) {
            $table->dropColumn('stock');
        });       
    }

}
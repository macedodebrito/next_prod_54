<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumMountAddOrdersInfo extends Migration {

    public function up() {
        Schema::table('sum_mount', function($table) {
            $table->integer('request_amount')->default(0);
            $table->integer('mount_amount')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_mount', function($table) {
            $table->dropColumn('request_amount');
            $table->dropColumn('mount_amount');
        });
    }

}

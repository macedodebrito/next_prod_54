<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateLocksmithStatusRequests extends Migration {

    public function up() {
        Schema::create('locksmith_status_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locksmith_id');
            $table->string('code');
            $table->string('name');
            $table->integer('value');
            $table->integer('extra');
            $table->integer('phc_reserved');
            $table->integer('phc_available');
            $table->integer('total');
            $table->timestamp('last_update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('locksmith_status_requests');
    }

}

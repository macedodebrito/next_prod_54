<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestWeekAddUrgencyDate extends Migration {

    public function up() {
        Schema::table('request_week', function($table) {
            $table->timestamp('urgency_date')->nullable();
            $table->integer('urgency');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('request_week', function($table) {
            $table->dropColumn('urgency_date');
            $table->dropColumn('urgency');
        });       
    }

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateStatusRequests extends Migration {

    public function up() {
        Schema::table('status_requests', function($table) {
            $table->string('code')->after('product_id');
            $table->integer('phc_reserved');
            $table->integer('phc_available');
            $table->integer('total');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('status_requests', function($table) {
            $table->dropColumn('code');
            $table->dropColumn('phc_reserved');
            $table->dropColumn('phc_available');
        });       
    }

}
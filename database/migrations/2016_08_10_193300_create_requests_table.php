<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pp');
            $table->integer('product_id');
            $table->integer('value');
            $table->timestamps();
            $table->timestamp('start_at');
            $table->timestamp('order_at');
            $table->softDeletes();
        });
        Schema::create('request_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });        
        Schema::create('request_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('quantity');       // quantos
            $table->integer('carpintaria');       // quantos
            $table->integer('pintura');       // quantos
            $table->integer('montagem');       // quantos
            $table->string('client');          // client       
            $table->string('obs');          // obs       
            $table->timestamp('end_at');            
            $table->timestamp('end_paint_at');            
            $table->timestamp('end_wood_at');            
            $table->timestamps();
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
        Schema::drop('request_users');
        Schema::drop('request_week');
    }

}

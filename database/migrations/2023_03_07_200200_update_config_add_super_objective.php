<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConfigAddSuperObjective extends Migration {

    public function up() {
        Schema::table('config', function($table) {
            $table->integer('s_limits_cnc');
            $table->integer('s_limits_prep');
            $table->integer('s_limits_wood');
            $table->integer('s_limits_woodfinishing');
            $table->integer('s_limits_woodfinishing_black');
            $table->integer('s_limits_paint');
            $table->integer('s_limits_mount');
            $table->integer('s_limits_wires');
            $table->integer('s_limits_locksmith');
            $table->integer('s_limits_tech');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('config', function($table) {
            $table->dropColumn('s_limits_cnc');
            $table->dropColumn('s_limits_prep');
            $table->dropColumn('s_limits_wood');
            $table->dropColumn('s_limits_woodfinishing');
            $table->dropColumn('s_limits_woodfinishing_black');
            $table->dropColumn('s_limits_paint');
            $table->dropColumn('s_limits_mount');
            $table->dropColumn('s_limits_wires');
            $table->dropColumn('s_limits_locksmith');
            $table->dropColumn('s_limits_tech');
        });       
    }

}
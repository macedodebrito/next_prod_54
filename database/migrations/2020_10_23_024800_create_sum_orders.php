<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateSumOrders extends Migration {

    public function up() {
        Schema::create('sum_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('points');
            $table->integer('request');
            $table->integer('order_amount');
            $table->integer('mount_amount');
            $table->integer('temp_amount');
            $table->integer('calc_amount');
            $table->string('product_name');
            $table->string('client');
            $table->timestamp('mount_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sum_orders');
    }

}

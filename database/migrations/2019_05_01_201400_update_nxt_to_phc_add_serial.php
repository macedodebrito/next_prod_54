<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNxttoPhcAddSerial extends Migration {

    public function up() {
        Schema::table('nxt_to_phc', function($table) {
            $table->string('internal_serial')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('nxt_to_phc', function($table) {
            $table->dropColumn('internal_serial');
        });       
    }

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateConfigAddSections extends Migration {

    public function up() {
        Schema::table('config', function($table) {
            $table->integer('limits_week')->after('id');            
            $table->integer('limits_monthly')->before('limits_week');            
            $table->integer('limits_prep')->after('limits_cnc');
            $table->integer('limits_woodfinishing_black')->after('limits_woodfinishing');
            $table->integer('limits_wires')->after('limits_mount');
            $table->integer('limits_locksmith')->after('limits_wires');
            $table->integer('m_limits_cnc');
            $table->integer('m_limits_prep');
            $table->integer('m_limits_wood');
            $table->integer('m_limits_woodfinishing');
            $table->integer('m_limits_woodfinishing_black');
            $table->integer('m_limits_paint');
            $table->integer('m_limits_mount');
            $table->integer('m_limits_wires');
            $table->integer('m_limits_locksmith');
            $table->integer('setup_week');
            $table->integer('setup_monthly');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('config', function($table) {
            $table->dropColumn('limits_week');
            $table->dropColumn('limits_monthly');
            $table->dropColumn('limits_prep');
            $table->dropColumn('limits_woodfinishing_black');
            $table->dropColumn('limits_wires');
            $table->dropColumn('limits_locksmith');
            $table->dropColumn('m_limits_cnc');
            $table->dropColumn('m_limits_prep');
            $table->dropColumn('m_limits_wood');
            $table->dropColumn('m_limits_woodfinishing');
            $table->dropColumn('m_limits_woodfinishing_black');
            $table->dropColumn('m_limits_paint');
            $table->dropColumn('m_limits_mount');
            $table->dropColumn('m_limits_wires');
            $table->dropColumn('m_limits_locksmith');
            $table->dropColumn('setup_week');
            $table->dropColumn('setup_monthly');
        });       
    }

}
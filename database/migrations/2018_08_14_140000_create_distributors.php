<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateDistributors extends Migration {

    public function up() {
        Schema::create('distributors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('market');
            $table->string('name');
            $table->string('type');
            $table->timestamp('first_buy')->nullable();
            $table->string('seller');
            $table->boolean('open_orders');
            $table->timestamp('last_buy')->nullable();
            $table->string('anual_billing');
            $table->string('previous_anual_billing');
            $table->timestamps();
        });

        Schema::create('distributors_prosp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('market');
            $table->string('name');
            $table->string('type');
            $table->timestamp('since')->nullable();
            $table->timestamp('official_date')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->boolean('state');
            $table->longtext('obs');
            $table->timestamps();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('distributors');
        Schema::drop('distributors_prosp');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateProductsAddSectionsValues extends Migration {

    public function up() {

        Schema::table('products', function($table) {
            $table->integer('store')->after('points');
            $table->integer('tech')->after('store');
            $table->integer('serigrafia')->after('tech');
            $table->integer('locksmith')->after('serigrafia');
            $table->integer('wires')->after('locksmith');
            $table->integer('pvc')->after('wires');
            $table->integer('packing')->after('pvc');
        });

        // Importing EXCEL FILE
        $path = public_path();
        $csv = array_map('str_getcsv', file($path . '\..\dump\matriz_products_add_sections.csv'));
        foreach ($csv as $line) {
            DB::table('products')->where('id', $line[0])->update(
                    [
                        'store' => $line[2],
                        'tech' => $line[3],
                        'serigrafia' => $line[4],
                        'locksmith' => $line[5],
                        'wires' => $line[6],
                        'pvc' => $line[7],
                        'packing' => 1,
                    ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('products', function($table) {
            $table->dropColumn('store');
            $table->dropColumn('tech');
            $table->dropColumn('serigrafia');
            $table->dropColumn('locksmith');
            $table->dropColumn('wires');
            $table->dropColumn('pvc');
            $table->dropColumn('packing');
        });
    }

}

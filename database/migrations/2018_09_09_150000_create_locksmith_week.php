<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreateLocksmithWeek extends Migration {

    public function up() {
        Schema::create('locksmith_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locksmith_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('quantity');       // quantos
            $table->string('client');          // client       
            $table->string('obs');          // obs       
            $table->integer('group');          // obs     
            $table->timestamp('end_at');
            $table->timestamps();
        });  
        
        Schema::create('locksmith_palete', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('locksmith_week_id');
            $table->integer('store_id');
            $table->string('store_obs');
            $table->timestamp('stored_date')->nullable();
            $table->integer('auth_id');
            $table->string('auth_obs');
            $table->timestamp('authed_date')->nullable();
            $table->timestamps();
            $table->string('phc_id');
        });         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('locksmith_week');
        Schema::drop('locksmith_palete');
    }

}

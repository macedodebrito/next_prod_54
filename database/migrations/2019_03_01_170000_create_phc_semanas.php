<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreatePhcSemanas extends Migration {

    public function up() {
        Schema::create('phc_semanas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pp');
            $table->string('vendedor');
            $table->string('cliente');
            $table->timestamp('data');
            $table->integer('week');
            $table->integer('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('phc_semanas');
    }

}

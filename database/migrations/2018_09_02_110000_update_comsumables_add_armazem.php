<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComsumablesAddArmazem extends Migration {

    public function up() {
        Schema::table('comsumables', function($table) {
            $table->integer('armazem')->after('filtros')->default(0);
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('comsumables', function($table) {
            $table->dropColum('armazem');
        });       
    }

}
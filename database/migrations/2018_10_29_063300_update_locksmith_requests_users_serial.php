<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateLocksmithRequestsUsersSerial extends Migration {

    public function up() {

        Schema::table('locksmith_requests_users', function($table) {
            $table->text('serial')->after('value');
            $table->integer('last_serial')->after('serial');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith_requests_users', function($table) {
            $table->dropColumn('serial');
            $table->dropColumn('last_serial');
        });
    }

}

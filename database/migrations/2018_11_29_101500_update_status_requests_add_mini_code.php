<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusRequestsAddMiniCode extends Migration {

    public function up() {
        Schema::table('status_requests', function($table) {
            $table->string('mini_code')->after('product_id');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('status_requests', function($table) {
            $table->dropColumn('mini_code');
        });       
    }

}
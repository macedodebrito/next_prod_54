<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaletesModifyPhcToString extends Migration {

    public function up() {
        Schema::table('paletes', function($table) {
            $table->string('phc_id')->change();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('paletes', function($table) {
            $table->integer('phc_id')->change();
        });       
    }

}
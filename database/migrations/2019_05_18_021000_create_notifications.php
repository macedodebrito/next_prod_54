<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateNotifications extends Migration {

    public function up() {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section', 64);
            $table->integer('user');
            
            $table->integer('type');
            $table->integer('object_id');
            $table->integer('open_user_id');
            $table->timestamp('open_at');
            $table->integer('close_user_id');
            $table->timestamp('close_at');

            $table->integer('status');
            $table->timestamps();
        });
        
        Schema::create('notifications_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->timestamps();
        });
        
        DB::table('notifications_type')->insert(
                    [
                        'id' => null,
                        'name' => "montagem_palete",
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]
            );      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('notifications');
        Schema::drop('notifications_type');
    }

}

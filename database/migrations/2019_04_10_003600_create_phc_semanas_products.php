<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreatePhcSemanasProducts extends Migration {

    public function up() {
        Schema::create('phc_semanas_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pp');
            $table->integer('quantity')->nullable();
            $table->string('product_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('phc_semanas_products');
    }

}

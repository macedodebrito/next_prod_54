<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhcSemanasAddTotal extends Migration {

    public function up() {
        Schema::table('phc_semanas', function($table) {
            $table->string('total');
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('phc_semanas', function($table) {
            $table->dropColumn('total');
        });       
    }

}
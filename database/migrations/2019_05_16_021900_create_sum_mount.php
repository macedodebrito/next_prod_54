<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreateSumMount extends Migration {

    public function up() {
        Schema::create('sum_mount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id');
            $table->integer('product_id');
            $table->string('name', 255);
            $table->string('type', 32);
            $table->integer('pp');
            $table->integer('cnc');
            $table->integer('carpintaria');
            $table->integer('acabamento');
            $table->integer('pintura');
            $table->integer('montagem');
            $table->integer('serralharia');
            $table->integer('redes');
            $table->integer('tecnica');
            $table->integer('total');
            $table->integer('encomendas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sum_mount');
    }

}

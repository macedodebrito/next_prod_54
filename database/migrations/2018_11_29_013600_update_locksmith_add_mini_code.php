<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLocksmithAddMiniCode extends Migration {

    public function up() {
        Schema::table('locksmith', function($table) {
            $table->string('mini_code')->after('id')->default("NC");
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith', function($table) {
            $table->dropColumn('mini_code');
        });       
    }

}
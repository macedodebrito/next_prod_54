<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateProductsAddImages extends Migration {

    public function up() {
        Schema::table('products', function($table) {
            $table->string('img_1');            
            $table->string('img_2');            
        });
    }
    public function down() {        
        Schema::table('products', function($table) {
            $table->dropColumn('img_1');
            $table->dropColumn('img_2');
        });        
    }
}

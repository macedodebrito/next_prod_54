<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;


class CreateTripleWeekTable extends Migration {

    public function up() {
        Schema::create('triple_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('quantity');       // quantos
            $table->integer('carpintaria');       // quantos
            $table->boolean('woodfinishing');                       
            $table->integer('pintura');       // quantos
            $table->integer('montagem');       // quantos
            $table->string('client');          // client       
            $table->string('obs');          // obs       
            $table->timestamp('end_at');            
            $table->timestamp('end_paint_at'); 
            $table->timestamp('end_woodfinishing_at'); 
            $table->timestamp('end_wood_at');            
            $table->timestamps();
            $table->integer('group');
        });        
        
        Schema::create('freeze_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('quantity');       // quantos
            $table->integer('carpintaria');       // quantos
            $table->boolean('woodfinishing');                       
            $table->integer('pintura');       // quantos
            $table->integer('montagem');       // quantos
            $table->string('client');          // client       
            $table->string('obs');          // obs       
            $table->timestamp('end_at');            
            $table->timestamp('end_paint_at'); 
            $table->timestamp('end_woodfinishing_at'); 
            $table->timestamp('end_wood_at');            
            $table->timestamps();
            $table->integer('group');
            $table->integer('week');
        }); 
         DB::statement('INSERT INTO triple_week SELECT * FROM request_week WHERE id=1');               
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('triple_week');
        Schema::drop('freeze_week');
    }
}

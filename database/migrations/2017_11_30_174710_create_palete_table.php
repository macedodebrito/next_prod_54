<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class CreatePaleteTable extends Migration {

    public function up() {
        Schema::create('paletes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_week_id');
            $table->integer('store_id');
            $table->string('store_obs');
            $table->timestamp('stored_date')->nullable();
            $table->integer('auth_id');
            $table->string('auth_obs');
            $table->timestamp('authed_date')->nullable();
            $table->timestamps();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('paletes');
    }

}

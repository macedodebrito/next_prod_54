<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateSumOrdersAddCalcs extends Migration {

    public function up() {
        Schema::table('sum_orders', function($table) {
            $table->integer('cnc_order_amount')->default(0)->after('request');
            $table->integer('cnc_amount')->default(0)->after('cnc_order_amount');
            $table->timestamp('cnc_date')->after('cnc_amount');
            $table->date('cnc_date_simple')->after('cnc_date');
            $table->integer('cnc_temp_amount')->default(0)->after('cnc_date_simple');
            $table->integer('cnc_calc_amount')->default(0)->after('cnc_temp_amount');
            $table->integer('prep_amount')->default(0)->after('cnc_calc_amount');
            $table->integer('prep_temp_amount')->default(0)->after('prep_amount');
            $table->integer('prep_calc_amount')->default(0)->after('prep_temp_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('cnc_order_amount');
            $table->dropColumn('cnc_amount');
            $table->dropColumn('cnc_date');
            $table->dropColumn('cnc_date_simple');
            $table->dropColumn('cnc_temp_amount');
            $table->dropColumn('cnc_calc_amount');
            $table->dropColumn('prep_amount');
            $table->dropColumn('prep_temp_amount');
            $table->dropColumn('prep_calc_amount');
        });
    }

}

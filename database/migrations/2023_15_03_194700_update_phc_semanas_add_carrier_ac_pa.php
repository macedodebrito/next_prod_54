<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as DB;
class UpdatePhcSemanasAddCarrierAcPa extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phc_semanas', function($table) {
            $table->string('u_typeac');
            $table->string('u_typepa');
            $table->string('carrier_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phc_semanas', function($table) {
            $table->dropColumn('u_typeac');
            $table->dropColumn('u_typepa');
            $table->dropColumn('carrier_info');
        });
    }

}

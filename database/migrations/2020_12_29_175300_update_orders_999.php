<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateOrders999 extends Migration {

    public function up() {
        DB::table('sum_orders')->where('order', -1)->update(['order' => 999]);
    }
    public function down() {        
        DB::table('sum_orders')->where('order', 999)->update(['order' => -1]);
    }
}

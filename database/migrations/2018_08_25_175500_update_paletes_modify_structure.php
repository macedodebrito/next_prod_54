<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdatePaletesModifyStructure extends Migration {

    public function up() {
        Schema::table('paletes', function($table) {
            $table->integer('store_auth_id')->after('stored_date');
            $table->timestamp('store_auth_date')->after('store_auth_id')->nullable();
            $table->renameColumn('stored_date', 'store_date');

            $table->integer('tech_id')->after('store_auth_date');
            $table->timestamp('tech_date')->after('tech_id')->nullable();
            $table->integer('tech_auth_id')->after('tech_date');
            $table->timestamp('tech_auth_date')->after('tech_auth_id')->nullable();

            $table->integer('serigrafia_id')->after('tech_auth_date');
            $table->timestamp('serigrafia_date')->after('serigrafia_id')->nullable();
            $table->integer('serigrafia_auth_id')->after('serigrafia_date');
            $table->timestamp('serigrafia_auth_date')->after('serigrafia_auth_id')->nullable();

            $table->integer('locksmith_id')->after('serigrafia_auth_date');
            $table->timestamp('locksmith_date')->after('locksmith_id')->nullable();
            $table->integer('locksmith_auth_id')->after('locksmith_date');
            $table->timestamp('locksmith_auth_date')->after('locksmith_auth_id')->nullable();

            $table->integer('wires_id')->after('locksmith_auth_date');
            $table->timestamp('wires_date')->after('wires_id')->nullable();
            $table->integer('wires_auth_id')->after('wires_date');
            $table->timestamp('wires_auth_date')->after('wires_auth_id')->nullable();

            $table->integer('pvc_id')->after('wires_auth_date');
            $table->timestamp('pvc_date')->after('pvc_id')->nullable();
            $table->integer('pvc_auth_id')->after('pvc_date');
            $table->timestamp('pvc_auth_date')->after('pvc_auth_id')->nullable();

            $table->integer('packing_id')->after('pvc_auth_date');
            $table->timestamp('packing_date')->after('packing_id')->nullable();
            $table->integer('packing_auth_id')->after('pvc_date');
            $table->timestamp('packing_auth_date')->after('packing_auth_id')->nullable();

            $table->integer('status')->after('packing_auth_date');
            
            $table->renameColumn('authed_date', 'auth_date');
        });

        DB::statement('UPDATE paletes SET status=2 WHERE status=0');

        Schema::create('paletes_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('section');
        });

        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'store']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'tech']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'serigrafia']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'locksmith']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'wires']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'pvc']);
        DB::table('paletes_sections')->insert(['id' => null, 'section' => 'packing']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('paletes', function($table) {
            $table->dropColumn('store_auth_id');
            $table->dropColumn('store_auth_date');

            $table->dropColumn('tech_id');
            $table->dropColumn('tech_date');
            $table->dropColumn('tech_auth_id');
            $table->dropColumn('tech_auth_date');

            $table->dropColumn('serigrafia_id');
            $table->dropColumn('serigrafia_date');
            $table->dropColumn('serigrafia_auth_id');
            $table->dropColumn('serigrafia_auth_date');

            $table->dropColumn('locksmith_id');
            $table->dropColumn('locksmith_date');
            $table->dropColumn('locksmith_auth_id');
            $table->dropColumn('locksmith_auth_date');

            $table->dropColumn('wires_id');
            $table->dropColumn('wires_date');
            $table->dropColumn('wires_auth_id');
            $table->dropColumn('wires_auth_date');

            $table->dropColumn('pvc_id');
            $table->dropColumn('pvc_date');
            $table->dropColumn('pvc_auth_id');
            $table->dropColumn('pvc_auth_date');

            $table->dropColumn('packing_id');
            $table->dropColumn('packing_date');
            $table->dropColumn('packing_auth_id');
            $table->dropColumn('packing_auth_date');

            $table->dropColumn('status');

            $table->renameColumn('store_date', 'stored_date');
            $table->renameColumn('auth_date', 'authed_date');

            Schema::drop('paletes_sections');
        });
    }

}

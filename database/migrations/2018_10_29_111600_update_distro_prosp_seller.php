<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateDistroProspSeller extends Migration {

    public function up() {

        Schema::table('distributors_prosp', function($table) {
            $table->text('seller')->after('type');
            $table->text('email')->after('name');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('distributors_prosp', function($table) {
            $table->dropColumn('seller');
            $table->dropColumn('email');
        });
    }

}

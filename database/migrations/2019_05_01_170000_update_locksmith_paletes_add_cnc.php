<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateLocksmithPaletesAddCnc extends Migration {

    public function up() {
        Schema::table('locksmith_palete', function($table) {
            $table->integer('cnc_id')->after('stored_date');
            $table->timestamp('cnc_date')->after('cnc_id')->nullable();
            $table->integer('cnc_auth_id')->after('cnc_date');
            $table->timestamp('cnc_auth_date')->after('cnc_auth_id')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith_palete', function($table) {        
            $table->dropColumn('cnc_id');
            $table->dropColumn('cnc_date');
            $table->dropColumn('cnc_auth_id');
            $table->dropColumn('cnc_auth_date');
        });
    }

}

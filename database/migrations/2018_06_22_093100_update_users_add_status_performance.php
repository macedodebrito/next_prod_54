<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAddStatusPerformance extends Migration {

    public function up() {
        Schema::table('users', function($table) {
            $table->integer('status')->default(1);
            $table->integer('performance')->default(1);
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function($table) {
            $table->dropColumn('status');
            $table->dropColumn('performance');
        });       
    }

}
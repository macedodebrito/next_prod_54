<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRequestUsersAddPrep extends Migration {

    public function up() {
        Schema::table('request_users', function($table) {
            $table->integer('prep')->after('published')->default(0);
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('request_users', function($table) {
            $table->dropColumn('prep');
        });       
    }

}
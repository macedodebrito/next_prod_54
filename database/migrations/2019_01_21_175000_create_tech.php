<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;
use Carbon\Carbon;

class CreateTech extends Migration {

    public function up() {
        Schema::create('tech', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mini_code')->default("NF");
            $table->string('code');
            $table->string('name');
            $table->integer('points');
            $table->timestamps();
        });

        Schema::create('tech_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tech_id');
            $table->string('pp');
            $table->integer('value');
            $table->timestamps();
            $table->timestamp('start_at');
            $table->timestamp('order_at');
        });
        Schema::create('tech_requests_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tech_requests_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('valid_id');     // quem valiou (ou recusou)
            $table->integer('value');       // quantos
            $table->text('serial');
            $table->integer('last_serial');
            $table->string('obs');          // obs
            $table->boolean('published');
            $table->timestamps();
        });

        Schema::create('tech_status_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tech_id');
            $table->string('mini_code');
            $table->string('code');
            $table->string('name');
            $table->integer('value');
            $table->integer('extra');
            $table->integer('phc_reserved');
            $table->integer('phc_available');
            $table->integer('total');
            $table->timestamp('last_update');
        });

        Schema::create('tech_week', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tech_id');  // pp 
            $table->integer('user_id');     // quem concluiu
            $table->integer('quantity');       // quantos
            $table->string('client');          // client       
            $table->string('obs');          // obs       
            $table->integer('group');          // obs     
            $table->timestamp('end_at');
            $table->timestamps();
        });

        Schema::create('tech_palete', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tech_week_id');
            $table->integer('store_id');
            $table->string('store_obs');
            $table->timestamp('stored_date')->nullable();
            $table->integer('auth_id');
            $table->string('auth_obs');
            $table->timestamp('authed_date')->nullable();
            $table->timestamps();
            $table->string('phc_id');
        });

        // Importing EXCEL FILE
        $path = public_path();
        $csv = array_map('str_getcsv', file($path . '\..\dump\tech_first_import.csv'));
        foreach ($csv as $line) {
            DB::table('tech')->insert(
                    [
                        'id' => null,
                        'mini_code' => str_split($line[0], 2)[0],
                        'code' => substr($line[0], 2),
                        'name' => $line[1],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString()
                    ]
            );
        }

        Schema::table('config', function($table) {
            $table->integer('limits_tech')->after('limits_wires');
            $table->integer('m_limits_tech')->after('m_limits_wires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tech');
        Schema::drop('tech_requests');
        Schema::drop('tech_requests_users');
        Schema::drop('tech_status_requests');
        Schema::drop('tech_week');
        Schema::drop('tech_palete');

        Schema::table('config', function($table) {
            $table->dropColumn('limits_tech');
            $table->dropColumn('m_limits_tech');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateLocksmithPaleteAddStatus extends Migration {

    public function up() {

        Schema::table('locksmith_palete', function($table) {
            $table->integer('status');
            $table->integer('store_auth_id');
            $table->timestamp('store_auth_date')->nullable();
        });

// FIX TABLE
        DB::statement('UPDATE locksmith_palete SET status=2 WHERE auth_id!=0');
        DB::statement('UPDATE locksmith_palete SET store_auth_id=store_id');
        DB::statement('UPDATE locksmith_palete SET store_auth_date=stored_date');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('locksmith_palete', function($table) {
            $table->dropColumn('status');
            $table->dropColumn('store_auth_id');
            $table->dropColumn('store_auth_date');
        });
    }

}

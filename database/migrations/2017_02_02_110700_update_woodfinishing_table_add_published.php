<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWoodfinishingTableAddPublished extends Migration {

    public function up() {
        Schema::table('stock_users', function($table) {
            $table->boolean('published')->after('value');
            $table->string('obs')->after('value');
            $table->dropColumn('mount_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('stock_users', function($table) {
            $table->dropColumn('published');
            $table->dropColumn('obs');
            $table->boolean('mount_id')->after('request_id');
        });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateRequestWeekAndSumOrdersAddOrder extends Migration {

    public function up() {
        Schema::table('request_week', function($table) {
            $table->integer('order')->default(0);            
        });
        Schema::table('sum_orders', function($table) {
            $table->integer('order')->default(0);            
            $table->date('mount_date_simple');                   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('request_week', function($table) {
            $table->dropColumn('order');
        });
        Schema::table('sum_orders', function($table) {
            $table->dropColumn('order');
            $table->dropColumn('mount_date_simple');
        });
    }

}

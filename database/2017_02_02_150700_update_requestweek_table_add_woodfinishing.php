<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB as DB;

class UpdateRequestweekTableAddWoodfinishing extends Migration {

    public function up() {
        Schema::table('request_week', function($table) {
            $table->boolean('woodfinishing')->after('carpintaria');
            $table->timestamp('end_woodfinishing_at')->after('end_paint_at');
        });

        DB::table('request_week')->update([
            "woodfinishing" => DB::raw("`pintura`"),
            "end_woodfinishing_at" => DB::raw("`end_wood_at`"),
        ]);
        $path = public_path();
        DB::unprepared(file_get_contents($path . '\..\dump\stock_users_fake.sql'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('request_week', function($table) {
            $table->dropColumn('woodfinishing');
            $table->dropColumn('end_woodfinishing_at');
        });
    }

}

<?php

namespace App\Jobs\V2;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDOException;
use Illuminate\Database\QueryException;

class SendMountOutput2PHC implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $output;

    public function __construct($output)
    {
        $this->output = $output;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //setlocale(LC_ALL, 0);
        setlocale(LC_ALL, 'Portuguese_Portugal.1252'); // Forçar o uso de UTF-8 no job
        ini_set('default_charset', 'UTF-8'); // Garantir que o charset seja UTF-8

        try {
            $decodedOutput = json_decode($this->output, true);
            \DB::connection('odbc-cvagroup')->transaction(function() use ($decodedOutput) {
                foreach($decodedOutput as $key => $info) {
                    $info = ensureUTF8ArrayDecode($info);
                    foreach($info['arrays'] as $k => $v) {
                        $info['arrays'][$k] = mb_convert_encoding($v, 'Windows-1252', 'UTF-8');
                    }
                    \DB::connection('odbc-cvagroup')->table($info['table'])->insert($info['arrays']);
                }
            });
            \DB::connection('odbc-cvagroup')->commit();
        }

        catch (QueryException $exception) {
            \DB::connection('odbc-cvagroup')->rollBack();
            throw $exception;            
        }

        catch (PDOException $exception) {
            \DB::connection('odbc-cvagroup')->rollBack();
            throw $exception;
        }

        catch (Exception $exception) {
            $errorInfo = $conn->getPdo()->errorInfo();
            \Log::error('Erro PDO: ' . $e->getMessage());
            \Log::error('Detalhes do erro ODBC: ' . json_encode($errorInfo));            
            \DB::connection('odbc-cvagroup')->rollBack();
            throw $exception;
        }
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IP extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $address = 'software.prod@next-proaudio.com';
        $name = 'NEXT-prod Software';
        $subject = 'NEXT-prod :: Novo IP';
        return $this->view('emails.ip', array('IP' => 'TESTE'))
                        ->from($address, $name)
                        ->subject($subject);
    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DebugbarMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd(Auth::check());
        /* 
        if(!Auth::check() || Auth::user()->id !== 3) {
            dd("2");
            \Debugbar::disable();
        }
        else {
            dd("1");
            \Debugbar::enable();
        } */
        
        /*  
        if (auth()->user() && in_array(auth()->id(), [1,2,3])) {
            \Debugbar::enable();
        }
        else {
            \Debugbar::disable();
        } 
        */

        return $next($request);
    }
}
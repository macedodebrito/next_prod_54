<?php

namespace App\Http\Controllers\V2;
use \App\Model\V2\RequestWeek;
use \App\Model\V2\RequestUser;
use \App\Model\V2\Request;
use \App\Model\V2\CNC;

ini_set('max_execution_time', 360);

use SleepingOwl\Admin\Http\Controllers\AdminController;

class CncController extends AdminController {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    static function index() {
        return view('v2.sections.cnc');
    }

    static function sendOutput(Request $request) {
        return $request;
    }

    static function getChecked($id) {
        return $id;
    }

    static function getPP() {
        /*
        $get_pp = \App\Model\Request::orderBy('created_at', 'asc')->groupBy('id')->get();
        $new_request_pp = [];
        foreach ($get_pp as $key => $value) {
            $get_request = \App\Model\Request::where('id', $value->id)->first();
            $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();
            $get_color = \App\Model\ProductCustomNxt::where('id', $get_request->custom_id)->first();

            $total = \App\Model\Request::where('id', $value->id)->sum('value');
            $permitido = \App\Model\RequestUser::where('request_id', $value->id)->sum('value');
            $check = $total - $permitido;
            $new_request_pp[0] = "Escolha um Pedido para Produção";
            if ($check > 0) {
                $new_request_pp[$value->id] = "#$get_request->pp - $get_product->name ($get_color->name)";
            }
        };
        */
        $getResults = Request::select(\DB::raw('requests.id, requests.pp, products.name as product_name, products_custom_nxt.name as color_name, requests.value - SUM(request_users.value) AS checked'))        
        ->join('products', 'products.id', '=', 'requests.product_id')
        ->join('products_custom_nxt', 'products_custom_nxt.id', '=', 'requests.custom_id')
        ->join('request_users', 'request_users.request_id', '=', 'requests.id')
        ->groupBy('requests.id')
        ->having('checked', '>', 0)
        ->orderBy('requests.created_at', 'asc')
        ->get();

        return $getResults;
    }

    static function getOrders() {

        \DB::statement("
        CREATE TEMPORARY TABLE temp_produzido AS
        SELECT 
            r.product_id,
            r.custom_id,
            SUM(ru.value) AS total_produzido,
            SUM(r.value) AS total_pp
        FROM 
            requests r
        JOIN 
            request_users ru ON r.id = ru.request_id
        WHERE 
            r.deleted_at IS NULL AND ru.published = 1
        GROUP BY 
            r.product_id, r.custom_id;
        ");

        \DB::statement("
        CREATE TEMPORARY TABLE temp_diferenca_producao AS
        SELECT 
            r.product_id,
            r.custom_id,
            SUM(r.value) - COALESCE(SUM(ru.value), 0) AS diferenca_producao
        FROM 
            requests r
        LEFT JOIN 
            request_users ru ON r.id = ru.request_id
        WHERE 
            r.deleted_at IS NULL AND ru.published = 1
        GROUP BY 
            r.product_id, r.custom_id;            
    ");

    $getResults = \DB::table('request_week as rw')
    ->leftJoin('temp_produzido as tp', function($join) {
        $join->on('rw.product_id', '=', 'tp.product_id')
             ->on('rw.custom_id', '=', 'tp.custom_id');
    })
    ->leftJoin('temp_diferenca_producao as tdp', function($join) {
        $join->on('rw.product_id', '=', 'tdp.product_id')
             ->on('rw.custom_id', '=', 'tdp.custom_id');
    })
    ->leftJoin('products as p', 'rw.product_id', '=', 'p.id')
    ->leftJoin('products_custom_nxt as pcn', 'rw.custom_id', '=', 'pcn.id')
    ->selectRaw('
        
        DATE_SUB(rw.end_wood_at, INTERVAL 5 DAY) AS end_cnc_at,

        rw.end_at as data_encomenda,
        rw.product_id,
        rw.obs as obs,
        rw.client as client,
        p.name as product_name,
        rw.custom_id,
        pcn.name as name_cor, 
        pcn.hex_color_bg as hex_bg,       
        pcn.hex_color as hex_cor,       
        DATEDIFF(DATE(rw.end_wood_at) - interval 5 day, CURDATE()) as atraso_dias,
        
        (SELECT
        CASE 
        WHEN atraso_dias < 0 
            THEN "Atraso" 
        WHEN atraso_dias > 7 
            THEN "Em Produção" 
        ELSE "Possível Atraso" 
        END) 
        as atraso_text,
        
        (SELECT
        CASE 
        WHEN atraso_dias < 0 
            THEN "danger" 
        WHEN atraso_dias > 7 
            THEN "success" 
        ELSE "warning" 
        END) 
        as atraso_icon,

        COALESCE(tp.total_pp, 0) as pedidos,
        COALESCE(tp.total_produzido, 0) as saidas,
        COALESCE(tdp.diferenca_producao, 0) as diferenca,
        
        (SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
            AND rw_inner.custom_id = rw.custom_id) as total_encomendas,
        rw.montagem as encomenda,

        COALESCE(tp.total_produzido, 0) - 
        (SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
            AND rw_inner.custom_id = rw.custom_id
            AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id < rw.id))
        ) as estoque_antes_encomenda,
            
        COALESCE(tp.total_produzido, 0) - 
        (SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
            AND rw_inner.custom_id = rw.custom_id
            AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id <= rw.id))
        ) as estoque_apos_encomenda,
            
        (
        COALESCE(tp.total_produzido, 0) - 
        (SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
            AND rw_inner.custom_id = rw.custom_id
            AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id <= rw.id))
        )
        ) AS saida_cumulativa,
        
    CASE
        WHEN COALESCE(tp.total_produzido, 0) - (
            SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
              AND rw_inner.custom_id = rw.custom_id
              AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id < rw.id))
        ) > 0 AND COALESCE(tp.total_produzido, 0) - (
            SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
              AND rw_inner.custom_id = rw.custom_id
              AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id <= rw.id))
        ) >= 0 THEN "success"
        WHEN COALESCE(tp.total_produzido, 0) - (
            SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
              AND rw_inner.custom_id = rw.custom_id
              AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id < rw.id))
        ) > 0 AND COALESCE(tp.total_produzido, 0) - (
            SELECT COALESCE(SUM(rw_inner.montagem), 0)
            FROM request_week rw_inner
            WHERE rw_inner.product_id = rw.product_id
              AND rw_inner.custom_id = rw.custom_id
              AND (rw_inner.end_at < rw.end_at OR (rw_inner.end_at = rw.end_at AND rw_inner.id <= rw.id))
        ) < 0 THEN "warning"
        ELSE "danger"
    END AS status
            '    
    )
    ->havingRaw('saida_cumulativa < 0')
    ->orderBy('rw.end_at', 'ASC')
    //->orderBy('rw.id', 'ASC')
    ->orderBy('saida_cumulativa', 'DESC')
    ->get();

    \DB::statement("DROP TEMPORARY TABLE IF EXISTS temp_produzido;");
    \DB::statement("DROP TEMPORARY TABLE IF EXISTS temp_diferenca_producao;");
    
    /*
        $getResultsx = RequestWeek::select(\DB::raw('
        DATEDIFF(request_week.end_wood_at - interval 5 day, CURDATE()) as atraso_dias, 
        (
    SELECT
    CASE 
    WHEN atraso_dias < 0 
    THEN "Atraso" 
    WHEN atraso_dias > 7 
    THEN "Em Produção" 
    ELSE "Possível Atraso" 
    END) as atraso_text,
(
    SELECT
    CASE 
    WHEN atraso_dias < 0 
    THEN "danger" 
    WHEN atraso_dias > 7 
    THEN "success" 
    ELSE "warning" 
    END) as atraso_icon,    
        products_custom_nxt.name_cor as name_cor, products_custom_nxt.hex_color as hex_cor, products_custom_nxt.hex_color_bg as hex_bg, request_week.*, sum_orders.points as points, sum_orders.cnc_date as cnc_date, sum_orders.order_id AS order_id, sum_orders.id AS oid, sum_orders.cnc_order_amount, sum_orders.cnc_amount, sum_orders.cnc_temp_amount, sum_orders.prep_temp_amount, sum_orders.cnc_calc_amount, sum_orders.order as sum_order, sum_orders.product_name as product_name'))
        ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
        ->join('products_custom_nxt', 'request_week.custom_id', '=', 'products_custom_nxt.id')
        ->where('sum_orders.cnc_temp_amount', '<', 0)
        ->orderBy('sum_orders.cnc_date', 'asc')        
        ->get();

    */
        return view('v2.modules.cnc.orders', compact('getResults'));
    }

    static function getActivity() {
        $getResults = CNC::select('users.bigname AS user', 'request_users.created_at AS created_at', 'requests.pp AS pp', 'products.name AS name', 'request_users.value', 'request_users.obs', 'products_custom_nxt.name as name_cor', 'products_custom_nxt.hex_color as hex_cor', 'products_custom_nxt.hex_color_bg as hex_bg')        
        ->join('requests', 'requests.id', '=', 'request_users.request_id')
        ->join('products', 'products.id', '=', 'requests.product_id')
        ->join('products_custom_nxt', 'requests.custom_id', '=', 'products_custom_nxt.id')
        ->join('users', 'users.id', '=', 'request_users.user_id')
        ->orderBy('request_users.created_at', 'desc');

        $getAll = $getResults->get();
        $getLatest = $getResults->take(10)->get();
        
        return view('v2.modules.cnc.activity', compact('getLatest', 'getAll'));
    }    
}


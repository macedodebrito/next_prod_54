<?php

namespace App\Http\Controllers\V2;
use \App\Model\V2\CNC;
use \App\Model\V2\PREP;
use \App\Model\V2\WOOD;
use \App\Model\V2\WOODFINISHING;
use \App\Model\V2\PAINT;
use \App\Model\V2\MOUNT;

ini_set('max_execution_time', 360);

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use SleepingOwl\Admin\Http\Controllers\AdminController;

function setData($getResults, $start_hour, $end_hour) {
    $setData = array();
    foreach($getResults as $result) {
        while($start_hour != $end_hour) {
            if ($result->hour != $start_hour) {
                $setData[] = 0;
            }
            else {
                $setData[] = $result->points;
            }
            $start_hour++;   
        }            
    }
    return $setData;    
}

class DashboardController extends AdminController {
    static function index() {
        return view('v2.sections.dashboard');
    }

    static function getProductivity($when) {
        if ($when === "today") {
            // time_interval
            $dt_start = \Carbon\Carbon::now();
            $dt_end = \Carbon\Carbon::now();            
        }

        else {
            // time_interval
            $dt_start = \Carbon\Carbon::now()->subDays($when);
            $dt_end = \Carbon\Carbon::now()->subDays($when);            
        }

        // fake date just for testing
        //$dt_start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2024-05-27 00:00:00');
        //$dt_end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2024-05-27 00:00:00');

        $start_hour = 7;
        $end_hour = 19;

        // Global Setting
        $time_start = $dt_start->setTime(8, 00, 00)->toDateTimeString();
        $time_9 = $dt_start->setTime(9, 00, 00)->toDateTimeString();
        $time_10 = $dt_start->setTime(10, 00, 00)->toDateTimeString();
        $time_11 = $dt_start->setTime(11, 00, 00)->toDateTimeString();
        $time_12 = $dt_start->setTime(12, 00, 00)->toDateTimeString();
        $time_13 = $dt_start->setTime(13, 00, 00)->toDateTimeString();
        $time_14 = $dt_start->setTime(14, 00, 00)->toDateTimeString();
        $time_15 = $dt_start->setTime(15, 00, 00)->toDateTimeString();
        $time_16 = $dt_start->setTime(16, 00, 00)->toDateTimeString();
        $time_17 = $dt_start->setTime(17, 00, 00)->toDateTimeString();
        $time_18 = $dt_start->setTime(18, 00, 00)->toDateTimeString();
        $time_end = $dt_end->setTime(19, 00, 00)->toDateTimeString();
        $time_interval = [$time_start, $time_9, $time_10, $time_11, $time_12, $time_13, $time_14, $time_15, $time_16, $time_17, $time_18, $time_end];
        
        // sections            

        /*
            series: [{
            name: 'CNC',
            data: [31, 40, 28, 51, 42, 82, 56],
            }, {
            name: 'Preparação',
            data: [11, 32, 45, 32, 34, 52, 41]
            }, {
            name: 'Carpintaria',
            data: [15, 11, 32, 18, 9, 24, 11]
            }],
        */   

        // CNC
        $chartName = "CNC";
        $colors[] = "#00c0ef";
        $getResults = CNC::select(\DB::raw('HOUR(request_users.created_at) as hour, request_users.value * products.points AS points'))
            ->join('requests', 'request_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('request_users.created_at', '=', $dt_start->toDateString())
            ->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);
            

        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];
        
        // Preparação
        $chartName = "Preparação";
        $colors[] = "#0000FF";
        $getResults = PREP::select(\DB::raw('HOUR(prep_users.created_at) as hour, prep_users.value * products.points AS points'))
            ->join('requests', 'prep_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('prep_users.created_at', '=', $dt_start->toDateString())
            ->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);
        
        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];                        
        
        // Carpintaria
        $chartName = "Carpintaria";
        $colors[] = "#dc3545";
        $getResults = WOOD::select(\DB::raw('HOUR(wood_users.created_at) as hour, wood_users.value * products.points AS points'))
            ->join('requests', 'wood_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('wood_users.created_at', '=', $dt_start->toDateString())
            ->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);

        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];

        // Acabamento
        $chartName = "Acabamento";
        $colors[] = "#605ca8";
        $getResults = WOODFINISHING::select(\DB::raw('HOUR(stock_users.created_at) as hour, stock_users.value * products.points AS points'))
            ->join('requests', 'stock_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('stock_users.created_at', '=', $dt_start->toDateString())
            ->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);

        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];

        // Pintura
        $chartName = "Pintura";
        $colors[] = "#00a65a";
        $getResults = PAINT::select(\DB::raw('HOUR(paint_users.created_at) as hour, paint_users.value * products.points AS points'))
            ->join('requests', 'paint_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('paint_users.created_at', '=', $dt_start->toDateString())
            //->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);

        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];

        // Montagem
        $chartName = "Montagem";
        $colors[] = "#FF851B";
        $getResults = MOUNT::select(\DB::raw('HOUR(mount_users.created_at) as hour, mount_users.value * products.points AS points'))
            ->join('requests', 'mount_users.request_id', 'requests.id')
            ->join('products', 'requests.product_id', 'products.id')
            ->whereDate('mount_users.created_at', '=', $dt_start->toDateString())
            ->groupBy('hour')
            ->get();        

        $setData = setData($getResults, $start_hour, $end_hour);

        $setChart[] = [
            'name' => $chartName,
            //'data' => json_encode($setData),
            'data' => $setData,
        ];

        $charts = json_encode($setChart);
        //dd($charts);
        
        return view('v2.modules.productivity.base', compact('time_interval', 'colors', 'charts'));
    }

    static function getActivity($type) {
        if ($type === "sections") {

        // Temporary QUERY until V2_database implementation
        $resultSQL = \DB::select("
            SELECT result.* FROM (
    
            (SELECT 'cnc' AS status, 'CNC' AS status_name, products.name, users.bigname, request_users.created_at, request_users.value, products_custom_nxt.name AS custom_name
            FROM request_users 
            JOIN requests ON request_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON request_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
                
            UNION 
                
            (SELECT 'prep' AS status, 'Preparação' AS status_name, products.name, prep_users.user_id, prep_users.created_at, prep_users.value, products_custom_nxt.name AS custom_name
            FROM prep_users 
            JOIN requests ON prep_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON prep_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
                
            (SELECT 'wood' AS status, 'Carpintaria' AS status_name, products.name, users.bigname, wood_users.created_at, wood_users.value, products_custom_nxt.name AS custom_name
            FROM wood_users 
            JOIN requests ON wood_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON wood_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
                
            (SELECT 'woodfinishing' AS status, 'Acabamento' AS status_name, products.name, users.bigname, stock_users.created_at, stock_users.value, products_custom_nxt.name AS custom_name
            FROM stock_users 
            JOIN requests ON stock_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON stock_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
            
            (SELECT 'paint' AS status, 'Pintura' AS status_name, products.name, users.bigname, paint_users.created_at, paint_users.value, products_custom_nxt.name AS custom_name
            FROM paint_users 
            JOIN requests ON paint_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON paint_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
            
            (SELECT 'mount' AS status, 'Montagem' AS status_name, products.name, users.bigname, mount_users.created_at, mount_users.quantity AS value, products_custom_nxt.name AS custom_name
            FROM mount_users 
            JOIN requests ON mount_users.request_id = requests.id
            JOIN products ON requests.product_id = products.id
            JOIN users ON mount_users.user_id = users.id
            JOIN products_custom_nxt ON requests.custom_id = products_custom_nxt.id
            ORDER BY created_at DESC LIMIT 0,3)
                
            UNION 
            
            (SELECT 'locksmith' AS status, 'Serralharia' AS status_name, locksmith.name, users.bigname, locksmith_requests_users.created_at, locksmith_requests_users.value, '' AS custom_name
            FROM locksmith_requests_users 
            JOIN locksmith_requests ON locksmith_requests_users.locksmith_requests_id = locksmith_requests.id
            JOIN locksmith ON locksmith_requests.locksmith_id = locksmith.id
            JOIN users ON locksmith_requests_users.user_id = users.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
            
            (SELECT 'tech' AS status, 'Técnica' AS status_name, tech.name, users.bigname, tech_requests_users.created_at, tech_requests_users.value, '' AS custom_name
            FROM tech_requests_users 
            JOIN tech_requests ON tech_requests_users.tech_requests_id = tech_requests.id
            JOIN tech ON tech_requests.tech_id = tech.id
            JOIN users ON tech_requests_users.user_id = users.id
            ORDER BY created_at DESC LIMIT 0,3)
            
            UNION 
            
            (SELECT 'wires' AS status, 'Redes' AS status_name, products.name, users.bigname, wires.created_at, wires.value, '' AS custom_name
            FROM wires 
            JOIN products ON wires.product_id = products.id
            JOIN users ON wires.user_id = users.id
            ORDER BY created_at DESC LIMIT 0,3)
                
            ) result 
            
            ORDER BY result.created_at DESC
        ");        
        /*
        SELECT result.* FROM (
            
        (SELECT 'cnc' AS status, requests.product_id, request_users.user_id, request_users.created_at, request_users.value
        FROM request_users JOIN requests ON request_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)
            
        UNION 
            
        (SELECT 'prep' AS status, requests.product_id, prep_users.user_id, prep_users.created_at, prep_users.value
        FROM prep_users JOIN requests ON prep_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 
            
        (SELECT 'wood' AS status, requests.product_id, wood_users.user_id, wood_users.created_at, wood_users.value
        FROM wood_users JOIN requests ON wood_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 
            
        (SELECT 'woodfinishing' AS status, requests.product_id, stock_users.user_id, stock_users.created_at, stock_users.value
        FROM stock_users JOIN requests ON stock_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 

        (SELECT 'paint' AS status, requests.product_id, paint_users.user_id, paint_users.created_at, paint_users.value
        FROM paint_users JOIN requests ON paint_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 

        (SELECT 'mount' AS status, requests.product_id, mount_users.user_id, mount_users.created_at, mount_users.quantity AS value
        FROM mount_users JOIN requests ON mount_users.request_id = requests.id
        ORDER BY created_at DESC LIMIT 0,3)
            
        UNION 

        (SELECT 'locksmith' AS status, locksmith_requests.locksmith_id, locksmith_requests_users.user_id, locksmith_requests_users.created_at, locksmith_requests_users.value
        FROM locksmith_requests_users JOIN locksmith_requests ON locksmith_requests_users.locksmith_requests_id = locksmith_requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 

        (SELECT 'tech' AS status, tech_requests.tech_id, tech_requests_users.user_id, tech_requests_users.created_at, tech_requests_users.value
        FROM tech_requests_users JOIN tech_requests ON tech_requests_users.tech_requests_id = tech_requests.id
        ORDER BY created_at DESC LIMIT 0,3)

        UNION 

        (SELECT 'wires' AS status, products.id, wires.user_id, wires.created_at, wires.value
        FROM wires JOIN products ON wires.product_id = products.id
        ORDER BY created_at DESC LIMIT 0,3)
            
        ) result 

        ORDER BY result.created_at DESC

        */            

            return view('v2.modules.activity.base', compact('resultSQL'));
        }
        
    }
}

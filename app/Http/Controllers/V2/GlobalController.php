<?php

namespace App\Http\Controllers\V2;

ini_set('max_execution_time', 360);

use Illuminate\Http\Request;
use SleepingOwl\Admin\Http\Controllers\AdminController;
use App\User;
//use Carbon\Carbon;
use App\Model\UserLogin;

class GlobalController extends AdminController {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function checkUserInfo(Request $request) {
        $id = $request->get('get-user-info');
        $dump = User::where('password', $id)->first();
        $GG = [];
        if (count($dump) > 0) {
            $dump_id = UserLogin::where('user_id', $dump->id)->orderBy('created_at', 'desc')->first();
            $GG['id'] = $dump->id;
            $GG['name'] = $dump->bigname;
            $GG['class']['info'] = "d-block";
            if (count($dump_id) > 0) {
                $GG['login_logout'] = $dump_id->login_logout;
                $GG['created_at'] = $dump_id->created_at;
                if ($dump_id->login_logout == "S") {
                    $GG['class']['in_out'] = "bg-danger";
                    $GG['class']['action'] = "btn-success";
                    $GG['class']['in_out_icon'] = "bi-box-arrow-left";
                    $GG['in_out_text'] = "Saiu:";
                    $GG['login_logout'] = "E";
                    $GG['action_text'] = "Dar Entrada";
                }
                else {
                    $GG['class']['in_out'] = "bg-success";
                    $GG['class']['action'] = "btn-danger";
                    $GG['class']['in_out-icon'] = "bi-box-arrow-right";
                    $GG['in_out_text'] = "Entrou:";
                    $GG['login_logout'] = "S";
                    $GG['action_text'] = "Dar Saída";
                }
            } else {
                $GG['login_logout'] = "E";
                $GG['created_at'] = "NUNCA";
                $GG['class']['in_out'] = "bg-danger";
                $GG['class']['action'] = "btn-success";
                $GG['class']['in_out_icon'] = "bi-box-arrow-left";
                $GG['in_out_text'] = "";
                $GG['action_text'] = "Dar Entrada";

            }
            return $GG;
        } else {
            $GG['id'] = 0;
            $GG['class']['info'] = "d-none";
            return $GG;
        }
    }

    public function checkUser(Request $request) {
        $id = $request->get('get-user-clock');
        $dump = User::where('password', $id)->first();
        $GG = [];
        if (count($dump) > 0) {
            $dump_id = UserLogin::where('user_id', $dump->id)->orderBy('created_at', 'desc')->first();
            $GG['id'] = $dump->id;
            $GG['name'] = $dump->bigname;
            $GG['class']['info'] = "d-block";
            if (count($dump_id) > 0) {
                $GG['login_logout'] = $dump_id->login_logout;
                $GG['created_at'] = $dump_id->created_at;
                if ($dump_id->login_logout == "S") {
                    $GG['class']['in_out'] = "bg-danger";
                    $GG['class']['action'] = "btn-success";
                    $GG['class']['in_out_icon'] = "bi-box-arrow-left";
                    $GG['in_out_text'] = "Saiu:";
                    $GG['login_logout'] = "E";
                    $GG['action_text'] = "Dar Entrada";
                }
                else {
                    $GG['class']['in_out'] = "bg-success";
                    $GG['class']['action'] = "btn-danger";
                    $GG['class']['in_out-icon'] = "bi-box-arrow-right";
                    $GG['in_out_text'] = "Entrou:";
                    $GG['login_logout'] = "S";
                    $GG['action_text'] = "Dar Saída";
                }
            } else {
                $GG['login_logout'] = "E";
                $GG['created_at'] = "NUNCA";
                $GG['class']['in_out'] = "bg-danger";
                $GG['class']['action'] = "btn-success";
                $GG['class']['in_out_icon'] = "bi-box-arrow-left";
                $GG['in_out_text'] = "";
                $GG['action_text'] = "Dar Entrada";

            }
            return $GG;
        } else {
            $GG['id'] = 0;
            $GG['class']['info'] = "d-none";
            return $GG;
        }
    }

    public function setUserClock(Request $request) {        
        $id = $request->get('get-user-clock-id');
        $status = $request->get('get-user-clock-login-logout');        
        UserLogin::insert(array('id' => null, 'user_id' => $id, 'login_logout' => $status, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
    }    
}


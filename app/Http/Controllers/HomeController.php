<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 360);

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\PaintUser;
use App\Model\LocksmithRequestsUser;
use App\Model\LocksmithRequests;
use App\Model\Locksmith;
use App\Model\LocksmithPalete;
use App\Model\TechRequestsUser;
use App\Model\TechPalete;
use App\Model\RequestUser;
use App\Model\MountUser;
use App\Model\PrepUser;
use App\Model\WoodUser;
use App\Model\WoodFinishingUser;
use App\Model\BLACKFINISHING_History;
use App\Model\WIRES_History;
use App\Model\WorkAbsences;
use App\Model\Stock;
use App\Model\Palete;
use App\User;
use Carbon\Carbon;
use App\Model\UserLogin;
use App\Model\RequestWeek;
use App\Model\Product;
use App\Model\ProductCustom;
use App\Model\ProductCustomNxt;
use App\Model\Request as Request2;
use SleepingOwl\Admin\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use PDF;
use Mail;

class HomeController extends AdminController {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    static function update_nxt() {
        $pp = DB::table('requests')->whereNull('deleted_at')->sum('value');
        $pp_order = DB::table('request_week')->sum('montagem');
        $cnc = \App\Model\RequestUser::sum('value');
        $prep = \App\Model\PrepUser::sum('value');
        $carpintaria = \App\Model\WoodUser::sum('value');
        $acabamento = \App\Model\StockUser::sum('value');
        $pintura = \App\Model\PaintUser::sum('value');
        $montagem = \App\Model\MountUser::sum('value');

        print_r("Verifying values:\n");
        print_r("PP: $pp \n");
        print_r("PP_ORDERS: $pp_order \n");
        print_r("CNC: $cnc \n");
        print_r("PREP: $prep \n");
        print_r("CARPINTARIA: $carpintaria \n");
        print_r("ACABAMENTO: $acabamento \n");
        print_r("PINTURA: $pintura \n");
        print_r("MONTAGEM: $montagem \n");
        print_r("---\n");

        if ($pp != DB::table('check_config')->first()->pp) {
            print_r("PP\n");
            \Artisan::call('update_products', ['section' => 'pp']);
        } else if ($pp_order != DB::table('check_config')->first()->pp_orders) {
            print_r("PP Orders\n");
            \Artisan::call('update_products', ['section' => 'pp_orders']);
        } else if ($cnc != DB::table('check_config')->first()->cnc) {
            print_r("CNC\n");
            \Artisan::call('update_products', ['section' => 'cnc']);
        } else if ($prep != DB::table('check_config')->first()->prep) {
            print_r("PREP\n");
            \Artisan::call('update_products', ['section' => 'prep']);
        } else if ($carpintaria != DB::table('check_config')->first()->carpintaria) {
            print_r("Carpintaria\n");
            \Artisan::call('update_products', ['section' => 'wood']);
        } else if ($acabamento != DB::table('check_config')->first()->acabamento) {
            print_r("Acabamento\n");
            \Artisan::call('update_products', ['section' => 'woodfinishing']);
        } else if ($pintura != DB::table('check_config')->first()->pintura) {
            print_r("Pintura\n");
            \Artisan::call('update_products', ['section' => 'paint']);
        } else if ($montagem != DB::table('check_config')->first()->montagem) {
            print_r("Montagem\n");
            \Artisan::call('update_products', ['section' => 'mount']);
        } else {
            print_r("Everything Up to Date.");
        }
    }

    public function fixTable() {
        \Artisan::call('update_products', ['section' => 'fix']);
        return \Artisan::output();;
    }

    public function rma() {

        return $this->renderContent(
                        view('admin::rma')
        );
    }
    
    public function distributors() {

        return $this->renderContent(
                        view('admin::distributors')
        );
    }

    public function distributors2() {

        return $this->renderContent(
                        view('admin::distributors_carrousel')
        );
    }

    public function distributors3() {

        return $this->renderContent(
                        view('admin::distributors_online')
        );
    }

    public function dashboard() {

        return $this->renderContent(
                        view('admin::dashboard')
        );
    }

    public function materials() {

        return $this->renderContent(
                        view('admin::materials')
        );
    }

    public function wires() {

        return $this->renderContent(
                        view('admin::wires')
        );
    }

    public function blackfinishing() {

        return $this->renderContent(
                        view('admin::blackfinishing')
        );
    }

    public function serralharia() {

        return $this->renderContent(
                        view('admin::locksmith')
        );
    }

    public function serralharia_open_mount() {

        return $this->renderContent(
                        view('admin::locksmith_open_mount')
        );
    }

    public function tecnica() {

        return $this->renderContent(
                        view('admin::tech')
        );
    }

    public function tecnica_open_mount() {

        return $this->renderContent(
                        view('admin::tech_open_mount')
        );
    }

    public function cnc() {

        return $this->renderContent(
                        view('admin::cnc')
        );
    }

    public function prep() {

        return $this->renderContent(
                        view('admin::prep')
        );
    }

    public function cnc_bordo() {

        return $this->renderContent(
                        view('admin::cnc_bordo')
        );
    }

    public function cnc_bordo_v2() {

        return $this->renderContent(
                        view('admin::cnc_bordo_v2')
        );
    }

    public function wood() {

        return $this->renderContent(
                        view('admin::wood')
        );
    }

    public function wood_bordo() {

        return $this->renderContent(
                        view('admin::wood_bordo')
        );
    }

    public function wood_open() {

        return $this->renderContent(
                        view('admin::wood_open')
        );
    }

    public function woodfinishing() {

        return $this->renderContent(
                        view('admin::woodfinishing')
        );
    }

    public function woodfinishing_bordo() {

        return $this->renderContent(
                        view('admin::woodfinishing_bordo')
        );
    }

    public function woodfinishing_open() {

        return $this->renderContent(
                        view('admin::woodfinishing_open')
        );
    }

    public function paint() {

        return $this->renderContent(
                        view('admin::paint')
        );
    }

    public function paint_bordo() {

        return $this->renderContent(
                        view('admin::paint_bordo')
        );
    }

    public function serralharia_paletes_view() {

        return $this->renderContent(
                        view('admin::serralharia_paletes')
        );
    }

    public function tecnica_paletes_view() {

        return $this->renderContent(
                        view('admin::tecnica_paletes')
        );
    }

    public function serralharia_paletes_history() {

        return $this->renderContent(
                        view('admin::serralharia_paletes_history')
        );
    }

    public function tecnica_paletes_history() {

        return $this->renderContent(
                        view('admin::tecnica_paletes_history')
        );
    }

    public function view_paletes_arquivo_serralharia() {

        return $this->renderContent(
                        view('admin::view_paletes_arquivo_serralharia')
        );
    }

    public function view_paletes_arquivo_tecnica() {

        return $this->renderContent(
                        view('admin::view_paletes_arquivo_tecnica')
        );
    }

    public function view_paletes() {

        return $this->renderContent(
                        view('admin::paletes')
        );
    }

    public function view_paletes_serralharia() {

        return $this->renderContent(
                        view('admin::paletes_serralharia')
        );
    }

    public function view_paletes_tecnica() {

        return $this->renderContent(
                        view('admin::paletes_tecnica')
        );
    }

    public function view_paletes_arquivo() {

        return $this->renderContent(
                        view('admin::view_paletes_arquivo')
        );
    }

    public function mount_paletes() {

        return $this->renderContent(
                        view('admin::mount_paletes')
        );
    }

    public function mount_paletes_history() {

        return $this->renderContent(
                        view('admin::mount_paletes_history')
        );
    }

    public function paint_open() {

        return $this->renderContent(
                        view('admin::paint_open')
        );
    }

    public function mount() {

        return $this->renderContent(
                        view('admin::mount')
        );
    }

    public function mount_bordo() {

        return $this->renderContent(
                        view('admin::mount_bordo')
        );
    }

    public function mount_open() {

        return $this->renderContent(
                        view('admin::mount_open')
        );
    }

    public function triple_week() {

        return $this->renderContent(
                        view('admin::triple')
        );
    }

    public function mount_get_pp($id) {
        $total = PaintUser::where('request_id', $id)->sum('value');
        $permitido = MountUser::where('request_id', $id)->sum('value');
        $last = MountUser::where('request_id', $id)->orderBy('serial', 'desc')->first();


        $GG = [];
        $GG['id'] = $total - $permitido;
        if ($last) {
            $GG['serial'] = $last->serial;
        } else {
            $GG['serial'] = 0;
        }
        return $GG;
    }

    public function mount_get_pp_Edit($id) {
        $total = PaintUser::where('request_id', $id)->sum('value');
        $permitido = MountUser::where('request_id', $id)->sum('value');
        $last = MountUser::where('request_id', $id)->orderBy('serial', 'desc')->first();


        $GG = [];
        $GG['id'] = $total;
        if ($last) {
            $GG['serial'] = $last->serial;
        } else {
            $GG['serial'] = 0;
        }
        return $GG;
    }

    public function locksmith_get_pp($id) {
        $total = \App\Model\LocksmithRequests::where('id', $id)->sum('value');
        $permitido = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $id)->sum('value');
        $last = LocksmithRequestsUser::where('locksmith_requests_id', $id)->orderBy('serial', 'desc')->first();


        $GG = [];

        $GG['id'] = $total;
        if ($last) {
            $GG['serial'] = $last->serial;
        } else {
            $GG['serial'] = 0;
        }
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function tech_get_pp($id) {
        $total = \App\Model\TechRequests::where('id', $id)->sum('value');
        $permitido = \App\Model\TechRequestsUser::where('tech_requests_id', $id)->sum('value');
        $last = TechRequestsUser::where('tech_requests_id', $id)->orderBy('serial', 'desc')->first();


        $GG = [];

        $GG['id'] = $total;
        if ($last) {
            $GG['serial'] = $last->serial;
        } else {
            $GG['serial'] = 0;
        }
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function locksmith_get_pp_edit($id) {
        $total = \App\Model\LocksmithRequests::where('id', $id)->sum('value');
        $permitido = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function tech_get_pp_edit($id) {
        $total = \App\Model\TechRequests::where('id', $id)->sum('value');
        $permitido = \App\Model\TechRequestsUser::where('tech_requests_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function cnc_get_pp($id) {
        $total = Request2::where('id', $id)->sum('value');
        $permitido = RequestUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function cnc_get_pp_edit($id) {
        $total = \App\Model\Request::where('id', $id)->sum('value');
        $permitido = \App\Model\RequestUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function prep_get_pp($id) {
        $total = RequestUser::where('request_id', $id)->sum('value');
        $permitido = PrepUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function prep_get_pp_edit($id) {
        $total = RequestUser::where('request_id', $id)->sum('value');
        $permitido = PrepUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function wood_get_pp($id) {
        $total = PrepUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = WoodUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total - $permitido;
        $GG['total'] = $total;
        $GG['permitido'] = $permitido;
        return $GG;
    }

    public function wood_get_pp_edit($id) {
        $total = RequestUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = WoodUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function woodfinishing_get_pp($id) {
        $total = WoodUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = WoodFinishingUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function woodfinishing_get_pp_edit($id) {
        $total = WoodUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = WoodFinishingUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function paint_get_pp($id) {
        $total = WoodFinishingUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = PaintUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total - $permitido;
        return $GG;
    }

    public function paint_get_pp_edit($id) {
        $total = WoodFinishingUser::where('request_id', $id)->where('published', 1)->sum('value');
        $permitido = PaintUser::where('request_id', $id)->sum('value');
        $GG = [];
        $GG['id'] = $total;
        return $GG;
    }

    public function mount_get_last($id) {
        $serial = MountUser::where('request_id', $id)->orderBy('serial', 'desc')->first();
        return $serial;
    }

    public function mount_check_serial($id, $serial) {
        $serial = MountUser::where('request_id', $id)->where('serial', $serial)->first();
        return count($serial);
    }

    public function week_get_product($id) {
        $GG = [];
        $GG['acabamento'] = 0;
        $GG['carpintaria'] = 0;
        $GG['pintura'] = 0;
        $GG['montagem'] = 0;
        $GG['custom'] = [];
        $GG['id'] = $id;

        $get_custom = ProductCustom::where('product_id', $id)->join('products_custom_nxt', 'products_custom.custom_id','=','products_custom_nxt.id')->get();
        foreach ($get_custom as $value) {
            $GG['custom'][$value->custom_id] = ProductCustomNxt::where('id', $value->custom_id)->first()->name;
        }

        $total_req = Request2::where('product_id', $id)->get();
        foreach ($total_req as $value) {
            $get_prod = Request2::where('product_id', $value->product_id)->get();            
                    
            $total = 0;
            $total_p = 0;
            $total_m = 0;

            $fabricados = 0;
            $na_carpintaria = 0;
            $no_acabamento = 0;
            $montados = 0;
            $acabados = 0;
            $na_pintura = 0;
            $terminados = 0;
            $na_montagem = 0;

            foreach ($get_prod as $value2) {
                // O QUE SAIU PARA A PINTURA
                $na_carpintaria = $na_carpintaria + WoodUser::where('request_id', $value2->id)->sum('value');

                // O QUE SAIU PARA A MONTAGEM
                $no_acabamento = $no_acabamento + WoodFinishingUser::where('request_id', $value2->id)->sum('value');

                // O QUE SAIU PARA A MONTAGEM
                $na_pintura = $na_pintura + PaintUser::where('request_id', $value2->id)->sum('value');

                //O QUE SAIU PARA STOCK, sem ser validado (linda)
                $na_montagem = $na_montagem + MountUser::where('request_id', $value2->id)->sum('value');


                $acabados = $acabados + PaintUser::where('request_id', $value2->id)->sum('value');
                $fabricados = $fabricados + PaintUser::where('request_id', $value2->id)->sum('value');
                $montados = $montados + MountUser::where('request_id', $value2->id)->sum('value');
                $terminados = $terminados + Stock::where('request_id', $value2->id)->sum('value');
            }
            $acabados = $no_acabamento - $acabados;
            $fabricados = $na_carpintaria - $fabricados;
            $montados = $na_pintura - $montados;
            $terminados = $na_montagem - $terminados;
            //print_r("C: $na_carpintaria / $fabricados | P: $na_pintura / $montados | M: $na_montagem / $terminados");
//            print_r("<pre>");
//            print_r($GG);
//            print_r("</pre>");
//            if ($fabricados >= 0) {
//                $GG['carpintaria'] = $fabricados;
//            } else {
//                $GG['carpintaria'] = 0;
//            }
//            if ($montados >= 0) {
//                $GG['pintura'] = $montados;
//            } else {
//                $GG['pintura'] = 0;
//            }
//            if ($terminados >= 0) {
//                $GG['montagem'] = $terminados;
//            } else {
//                $GG['montagem'] = 0;
//            }
            $GG['carpintaria'] = $fabricados;
            $GG['acabamento'] = $acabados;
            $GG['pintura'] = $montados;
            $GG['montagem'] = $terminados;
        }
        $SEMANAL = [];

        $total = RequestWeek::where('id', $id)->sum('carpintaria');

        $total_week = RequestWeek::where('product_id', $id)->sum('carpintaria');

        $total_req = Request2::where('product_id', $id)->first();

        $total_req2 = Request2::where('product_id', $id)->get();
        $total_inc = 0;
        foreach ($total_req2 as $go) {
            $dump = WoodUser::where('request_id', $go->id)->sum('value');
            $total_inc = $total_inc + $dump;
        }

        $SEMANAL[$id]['product_id'] = $id;
        $SEMANAL[$id]['name'] = Product::where('id', $id)->first()->name;
        $SEMANAL[$id]['total_semanal'] = $total_week;
        $SEMANAL[$id]['total_montado'] = $total_inc;
        $GG['carpintaria_req'] = $SEMANAL[$id]['total_semanal'] - $SEMANAL[$id]['total_montado'];

        $SEMANAL = [];
        $total = RequestWeek::where('id', $id)->sum('pintura');
        $total_week = RequestWeek::where('product_id', $id)->sum('pintura');
        $total_req = Request2::where('product_id', $id)->first();
        $total_req2 = Request2::where('product_id', $id)->get();
        $total_inc = 0;
        foreach ($total_req2 as $go) {
            $dump = PaintUser::where('request_id', $go->id)->sum('value');
            $total_inc = $total_inc + $dump;
        }

        $SEMANAL[$id]['product_id'] = $id;
        $SEMANAL[$id]['name'] = Product::where('id', $id)->first()->name;
        $SEMANAL[$id]['total_semanal'] = $total_week;
        $SEMANAL[$id]['total_montado'] = $total_inc;

        //print_r($SEMANAL);
        $GG['pintura_req'] = $SEMANAL[$id]['total_semanal'] - $SEMANAL[$id]['total_montado'];

        $SEMANAL = [];
        $total = RequestWeek::where('id', $id)->sum('montagem');
        $total_week = RequestWeek::where('product_id', $id)->sum('montagem');
        $total_req = Request2::where('product_id', $id)->first();
        $total_req2 = Request2::where('product_id', $id)->get();
        $total_inc = 0;
        foreach ($total_req2 as $go) {
            $dump = MountUser::where('request_id', $go->id)->sum('value');
            $total_inc = $total_inc + $dump;
        }

        $SEMANAL[$id]['product_id'] = $id;
        $SEMANAL[$id]['name'] = Product::where('id', $id)->first()->name;
        $SEMANAL[$id]['total_semanal'] = $total_week;
        $SEMANAL[$id]['total_montado'] = $total_inc;

        //print_r($SEMANAL);
        $GG['montagem_req'] = $SEMANAL[$id]['total_semanal'] - $SEMANAL[$id]['total_montado'];

        return $GG;
    }

    public function week_get_locksmith($id) {
        $GG = [];
        $GG['locksmith'] = 0;
        $GG['id'] = $id;

//        $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $id)->get();
//        foreach ($total_req as $value) {
//            $get_prod = \App\Model\LocksmithRequests::where('locksmith_id', $value->locksmith_id)->get();
//            $GG['locksmith'] = $value->value;
//            $na_serralharia = 0;
//
//            foreach ($get_prod as $value2) {
//                // O QUE SAIU PARA STOCK DA SERRALHARIA
//                $na_serralharia = $na_serralharia + \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $value2->id)->sum('value');
//            }
//            $GG['locksmith'] = $GG['locksmith'] - $na_serralharia;
//        }
//        $GG['montagem_req'] = $GG['locksmith'];

        $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $id)->get();

        foreach ($total_req as $value) {
            $get_prod = \App\Model\LocksmithRequests::where('locksmith_id', $value->locksmith_id)->get();
            $GG['locksmith'] = \App\Model\LocksmithRequests::where('locksmith_id', $id)->sum('value');
            $na_serralharia = 0;
            foreach ($get_prod as $value2) {
                // O QUE SAIU PARA STOCK DA SERRALHARIA
                $na_serralharia = $na_serralharia + \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $value2->id)->sum('value');
            }
            $GG['locksmith'] = $GG['locksmith'] - $na_serralharia;
        }
        $GG['montagem_req'] = $GG['locksmith'];
        return $GG;
    }

    public function week_get_tech($id) {
        $GG = [];
        $GG['tech'] = 0;
        $GG['id'] = $id;

        $total_req = \App\Model\TechRequests::where('tech_id', $id)->get();
        foreach ($total_req as $value) {
            $get_prod = \App\Model\TechRequests::where('tech_id', $value->tech_id)->get();
//            $GG['tech'] = $value->value;
            $GG['tech'] = \App\Model\TechRequests::where('tech_id', $id)->sum('value');
            $na_tecnica = 0;

            foreach ($get_prod as $value2) {
                // O QUE SAIU PARA STOCK DA SERRALHARIA
                $na_tecnica = $na_tecnica + \App\Model\TechRequestsUser::where('tech_requests_id', $value2->id)->sum('value');
            }
            $GG['tech'] = $GG['tech'] - $na_tecnica;
        }
        $GG['montagem_req'] = $GG['tech'];

        return $GG;
    }

    public function stock() {

        return $this->renderContent(
                        view('admin::stocks')
        );
    }

    public function stock_ok() {

        return $this->renderContent(
                        view('admin::stocks_ok')
        );
    }

    public function stock_ok_search() {

        return $this->renderContent(
                        view('admin::stocks_ok_search')
        );
    }

    public function reorder_orders($id, $value) {
        \App\Model\Sum_Orders::where('order_id', $id)->update(['order' => $value]);
        $RETURN = \Artisan::call('update_products', ['section' => 'pp_orders']);
        if ($RETURN) {
            return $RETURN;
        }
    }

    public function stock_status($id, $value) {

        if ($value == -1) {
            $dump = MountUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 3) {
            $dump = MountUser::where('id', $id)->where('published', 1)->update(['published' => 0, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        } else if ($value == 0) {
            $dump = MountUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => \Auth::user()->id]);
        } else if ($value == 1) {
            $dump = MountUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        } else if ($value == -2) {
            $dump = MountUser::where('id', $id)->where('published', 0)->delete();
        } else if ($value == 2) {
            $dump = MountUser::where('id', $id)->where('published', 1)->update(['published' => $value, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        }
        return [];
    }

    public function palete_status($id, $value, $user_id) {
        $col_id = $value . "_id";
        $col_date = $value . "_date";
        Palete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
        if ($value == "store") {
            $col_id = $value . "_auth_id";
            $col_date = $value . "_auth_date";
            Palete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
        }
        if ($value != "auth") {
            // AFTER UPDATE, CHECKING ALL SECTIONSs
            $results = DB::select('SELECT section from paletes_sections');

            $request_week_id = Palete::where('id', $id)->first()->request_week_id;
            $request_week_info = \App\Model\RequestWeek::where('id', $request_week_id)->first()->product_id;

            foreach ($results as $row) {

                $COLUMN = $row->section;
                $value_col = $COLUMN . "_id";
                $value_auth_col = $COLUMN . "_auth_id";
                $go_status = 0;

                if (\App\Model\Product::where('id', $request_week_info)->first()->$COLUMN == 1) {

                    $sql = Palete::where('id', $id)->where("$value_col", '!=', 0)->where("$value_auth_col", '!=', 0)->first();

                    if (count($sql) > 0) {
                        $go_status = 1;
                    } else {
                        $go_status = 0;
                        return [];
                    }
                } else {
                    $go_status = 1;
                }
            }
            if ($go_status == 1) {
                Palete::where('id', $id)->update(['status' => 1, 'store_obs' => '']);
            }
            return [];
        } else {
            Palete::where('id', $id)->update(['status' => 2]);
        }
    }

    public function locksmith_palete_status($id, $value, $user_id) {
        if ($value == "store") {
            $col_id = "store_id";
            $col_date = "stored_date";
            LocksmithPalete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
            LocksmithPalete::where('id', $id)->update(["store_auth_id" => $user_id, "store_auth_date" => \Carbon\Carbon::now()->toDateTimeString()]);
        } else {
            if ($value != "auth") {
                $col_id = $value . "_id";
                $col_date = $value . "_date";
            } else {
                $col_id = "auth_id";
                $col_date = "authed_date";
            }
            LocksmithPalete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
        }
        if ($value != "auth") {
            // AFTER UPDATE, CHECKING ALL SECTIONS
            $results = DB::select('SELECT section from paletes_sections');

            $request_week_id = LocksmithPalete::where('id', $id)->first()->locksmith_week_id;
            $request_week_info = \App\Model\LocksmithWeek::where('id', $request_week_id)->first()->locksmith_id;

            foreach ($results as $row) {

                $COLUMN = $row->section;
                $value_col = $COLUMN . "_id";
                $value_auth_col = $COLUMN . "_auth_id";
                $go_status = 0;

                if (\App\Model\Locksmith::where('id', $request_week_info)->first()->$COLUMN == 1) {

                    $sql = LocksmithPalete::where('id', $id)->where("$value_col", '!=', 0)->where("$value_auth_col", '!=', 0)->first();

                    if (count($sql) > 0) {
                        $go_status = 1;
                    } else {
                        $go_status = 0;
                        return [];
                    }
                } else {
                    $go_status = 1;
                }
            }
            if ($go_status == 1) {
                LocksmithPalete::where('id', $id)->update(['status' => 1, 'store_obs' => '']);
            }
            return [];
        } else {
            LocksmithPalete::where('id', $id)->update(['status' => 2]);
        }
    }

//    public function locksmith_palete_status($id, $value, $user_id) {
//        if ($value == "store") {
//            LocksmithPalete::where('id', $id)->update(["store_id" => $user_id, "stored_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//        } else {
//            LocksmithPalete::where('id', $id)->update(["auth_id" => $user_id, "authed_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//        }
//    }
//    public function locksmith_palete_status($id, $value, $user_id) {
//        $col_id = $value . "_id";
//        $col_date = $value . "_date";
//        //LocksmithPalete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//        if ($value == "store") {
//            LocksmithPalete::where('id', $id)->update(["store_id" => $user_id, "stored_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//        } else {
//            if ($value != "auth") {
//                LocksmithPalete::where('id', $id)->update(["$col_id" => $user_id, "$col_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//            } else {
//                LocksmithPalete::where('id', $id)->update(["auth_id" => $user_id, "authed_date" => \Carbon\Carbon::now()->toDateTimeString()]);
//            }
//        }
//    }


    public function tech_palete_status($id, $value, $user_id) {
        if ($value == "store") {
            TechPalete::where('id', $id)->update(["store_id" => $user_id, "stored_date" => \Carbon\Carbon::now()->toDateTimeString()]);
        } else if ($value == "auth") {
            TechPalete::where('id', $id)->update(["auth_id" => $user_id, "authed_date" => \Carbon\Carbon::now()->toDateTimeString(), "status" => 2]);
        } else {
            $value_id = $value . "_id";
            $value_date = $value . "_date";
            TechPalete::where('id', $id)->update([$value_id => $user_id, $value_date => \Carbon\Carbon::now()->toDateTimeString()]);
        }

        if ($value != "auth") {
            $get_status = TechPalete::where('id', $id)->first();
            if ($get_status->store_id != 0 && $get_status->locksmith_id != 0 && $get_status->locksmith_auth_id != 0) {
                TechPalete::where('id', $id)->update(['status' => 1]);
            }
        }
    }

    public function locksmith_status($id, $value, $user_id = null) {

        if ($value == -1) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 3) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', 1)->update(['published' => 0, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        } else if ($value == 0) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => \Auth::user()->id]);
        } else if ($value == 1) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        } else if ($value == -2) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', 0)->delete();
        } else if ($value == 2) {
            $dump = LocksmithRequestsUser::where('id', $id)->where('published', 1)->update(['published' => $value, 'valid_id' => \Auth::user()->id, 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);
        }
        return [];
    }

    public function cnc_status($id, $value, $user_id = null) {
        $userId=User::where('password', $user_id)->first()->id;
        $user_id=$userId;
        if ($value == -1) {
            $dump = RequestUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->delete();
        }
        return [];
    }

/*     public function prep_status($id, $value, $user_id = null) {
        if ($value == -1) {
            $dump = RequestUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'prep' => 1, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->delete();
        } else if ($value == 3) {
            $dump = RequestUser::where('id', $id)->where('published', 0)->update(['published' => 0, 'prep' => 1, 'valid_id' => $user_id]);
        }
        return [];
    } */

    public function prep_status($id, $value, $user_id = null) {
        $userId=User::where('password', $user_id)->first()->id;
        $user_id=$userId;
        if ($value == -1) {
            $dump = PrepUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = PrepUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = PrepUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = PrepUser::where('id', $id)->where('published', 0)->delete();
        }
        return [];
    }

    public function wood_status($id, $value, $user_id = null) {
        $userId=User::where('password', $user_id)->first()->id;
        $user_id=$userId;
        if ($value == -1) {
            $dump = WoodUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = WoodUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = WoodUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = WoodUser::where('id', $id)->where('published', 0)->delete();
        }
        return [];
    }

    public function woodfinishing_status($id, $value, $user_id = null) {
        $userId=User::where('password', $user_id)->first()->id;
        $user_id=$userId;
        if ($value == -1) {
            $dump = WoodFinishingUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = WoodFinishingUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = WoodFinishingUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = WoodFinishingUser::where('id', $id)->where('published', 0)->delete();
        }
        return [];
    }

    public function paint_status($id, $value, $user_id = null) {
        $userId=User::where('password', $user_id)->first()->id;
        $user_id=$userId;
        if ($value == -1) {
            $dump = PaintUser::where('id', $id)->where('published', $value)->delete();
        } else if ($value == 0) {
            $dump = PaintUser::where('id', $id)->where('published', 0)->update(['published' => -1, 'valid_id' => $user_id]);
        } else if ($value == 1) {
            $dump = PaintUser::where('id', $id)->where('published', 0)->update(['published' => $value, 'valid_id' => $user_id]);
        } else if ($value == -2) {
            $dump = PaintUser::where('id', $id)->where('published', 0)->delete();
        }
        return [];
    }

    public function check_waiting($value) {
        if ($value == "cnc") {
            $dump = RequestUser::where('prep', 1)->where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "woodfinishing") {
            $dump = WoodUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "prep") {
            $dump = PrepUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "paint") {
            $dump = WoodFinishingUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "mount") {
            $dump = PaintUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "locksmith") {
            $dump = LocksmithRequestsUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        } else if ($value == "tech") {
            $dump = TechRequestsUser::where('published', 0)->sum('value');
            if ($dump == null) {
                $dump = 0;
            }
        }
        return $dump;
    }

    public function get_code($id) {
        $dump = User::where('password', $id)->first();
        $GG = [];
        if (count($dump) > 0) {
            $dump_id = UserLogin::where('user_id', $dump->id)->orderBy('created_at', 'desc')->first();
            $GG['id'] = $dump->id;
            $GG['name'] = $dump->bigname;

            if (count($dump_id) > 0) {
                $GG['login_logout'] = $dump_id->login_logout;
                $GG['created_at'] = $dump_id->created_at;
            } else {
                $GG['login_logout'] = "E";
                $GG['created_at'] = "NUNCA";
            }
            return $GG;
        } else {
            $GG['id'] = 0;
            return $GG;
        }
    }

    public function get_picker($id) {
        // if barcode len = 12
        if (strlen($id) == 12) {
            $get_CODE = substr($id, 0, -7);
            $get_PP = substr($id, 5, -3);
            $get_SERIAL = trim(substr($id, -3));
            $GG = [];

            if (!isset($get_CODE)) {
                $GG['id'] = 0;
                $GG['error'] = "nao tem codigo de produto";
                return $GG;
            }
            $dump_CODE = \App\Model\Product::where('code', $get_CODE)->first();
            if (!isset($dump_CODE) || !isset($get_PP) || !isset($get_SERIAL)) {
                $GG['id'] = 0;
                $GG['error'] = "a serial nao é válida";
                return $GG;
            }
            $dump_PP = \App\Model\Request::where('pp', $get_PP)->where('product_id', $dump_CODE->id)->first();





            // check if serial match with week validation
            // GET 1st PRODUCT on WEEK LIST (meaning those products arent ready)
            $check = RequestWeek::orderBy('end_at', 'asc')->where('product_id', $dump_CODE->product_id)->get();
            // lista de pedidos semanais por ordem
            $SEMANAL = [];
            $week = [];
            //print_r("<pre>");
            foreach ($check as $key => $instance) {
                $total = RequestWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('montagem');

                $total_week = RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->sum('montagem');

                $total_req = Request::where('product_id', $instance->product_id)->first();

                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = MountUser::where('request_id', $go->id)->sum('value');
        //                    $dump = MountUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                foreach (RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->montagem;
                }
            }
            //print_r($SEMANAL);
            foreach ($SEMANAL as $key2 => $value2) {
                foreach ($value2['semana'] as $key3 => $value3) {
                    if ($SEMANAL[$key2]['total_montado'] > 0) {
                        if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        }
                        //print_r($SEMANAL[$key2]['semana'][$key3]);
                    }
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                        $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    }
                }
            }
            $GG['week'] = $week;
            //$query->whereIn('id', $week)->orderBy('end_at', 'asc');
            // if PP counter is valid
            if (count($dump_PP) > 0) {
                $dump_SERIAL = \App\Model\MountUser::where('request_id', $dump_PP->id)->where('serial', $get_SERIAL)->first();
                $GG['error2'] = count($dump_SERIAL);
                if (count($dump_SERIAL) == 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['custom_id'] = $dump_PP->custom_id;
                    $GG['custom_name'] = \App\Model\ProductCustomNxt::where('id', $dump_PP->custom_id)->first()->name;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    $temp_total = \App\Model\PaintUser::where('request_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $temp_check = \App\Model\MountUser::where('request_id', $dump_PP->id)->where('published', '>', -1)->sum('value');
                    $GG['product_count'] = $temp_total - $temp_check;

                    $GG['serial'] = $get_SERIAL;
                    $GG['error'] = "serial válida";
                    return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['error'] = "a serial já existe";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['error'] = "o PP nao tem contagem válida";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['error'] = "a serial nao tem 12 dígitos";
            return $GG;
        }
    }

    public function get_locksmith_picker($id) {
        if (strlen($id) == 12) {
            $get_CODE = substr($id, 0, -7);
            $get_PP = substr($id, 5, -3);
            $get_SERIAL = trim(substr($id, -3));
            $GG = [];
            $dump_CODE = \App\Model\Locksmith::where('code', $get_CODE)->first();
            $dump_PP = \App\Model\LocksmithRequests::where('pp', $get_PP)->where('locksmith_id', $dump_CODE->id)->first();
            if (count($dump_PP) > 0) {
                $dump_SERIAL = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $dump_PP->id)->where('serial', $get_SERIAL)->first();
                if (count($dump_SERIAL) == 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    $temp_total = \App\Model\LocksmithRequests::where('id', $dump_PP->id)->sum('value');
                    $temp_check = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $temp_total - $temp_check;

                    $GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            return $GG;
        }
    }

    public function get_tech_picker($id) {
        if (strlen($id) == 12) {
            $get_CODE = substr($id, 0, -7);
            $get_PP = substr($id, 5, -3);
            $get_SERIAL = trim(substr($id, -3));
            $GG = [];
            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
            $dump_PP = \App\Model\TechRequests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();
            if (count($dump_PP) > 0) {
                $dump_SERIAL = \App\Model\TechRequestsUser::where('tech_requests_id', $dump_PP->id)->where('serial', $get_SERIAL)->first();
                if (count($dump_SERIAL) == 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    $temp_total = \App\Model\TechRequests::where('id', $dump_PP->id)->sum('value');
                    $temp_check = \App\Model\TechRequestsUser::where('tech_requests_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $temp_total - $temp_check;

                    $GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            return $GG;
        }
    }

    public function send_picker($section) {
        foreach ($_REQUEST['ids'] as $key => $product) {
            $create_date = Carbon::createFromTimestamp($_REQUEST['datas'][$key])->toDateTimeString();
            $product_id = $_REQUEST['ids'][$key];
            $auth_id = $_REQUEST['auths'][$key];
            $custom_id = $_REQUEST['custom_ids'][$key];
            $obs = $_REQUEST['obs'][$key];

            $get_all_pp = \App\Model\Request::where('product_id', $product_id)->where('custom_id', $custom_id)->orderBy('pp', 'asc')->get();
            foreach ($get_all_pp as $pp) {
                if ($section === "prep") {
                    $actual = \App\Model\PrepUser::where('request_id', $pp->id)->groupBy('request_id')->sum('value');
                    $anterior = \App\Model\RequestUser::where('request_id', $pp->id)->where('published', 1)->groupBy('request_id')->sum('value');
                } else if ($section === "wood") {
                    $actual = \App\Model\WoodUser::where('request_id', $pp->id)->groupBy('request_id')->sum('value');
                    $anterior = \App\Model\PrepUser::where('request_id', $pp->id)->where('published', 1)->groupBy('request_id')->sum('value');
                } else if ($section === "woodfinishing") {
                    $actual = \App\Model\WoodFinishingUser::where('request_id', $pp->id)->groupBy('request_id')->sum('value');
                    $anterior = \App\Model\WoodUser::where('request_id', $pp->id)->where('published', 1)->groupBy('request_id')->sum('value');
                } else {
                    $actual = \App\Model\PaintUser::where('request_id', $pp->id)->groupBy('request_id')->sum('value');
                    $anterior = \App\Model\WoodFinishingUser::where('request_id', $pp->id)->where('published', 1)->groupBy('request_id')->sum('value');
                }
                $total = $anterior - $actual;
                //print_r("(# $pp->id PP $pp->pp ) CNC $anterior - WOOD $actual = $total\n");
                if ($actual < $anterior) {
                    //print_r(" <--- OK\n");
                    $request_id = $pp->id;
                    break;
                }
            }
            if ($section === "prep") {
                \App\Model\PrepUser::insert(
                        array('id' => null,
                            'request_id' => $request_id,
                            'user_id' => $auth_id,
                            'valid_id' => 0,
                            'value' => 1,
                            'obs' => $obs,
                            'published' => 0,
                            'created_at' => $create_date,
                            'updated_at' => $create_date
                        )
                );
            } else if ($section === "wood") {
                \App\Model\WoodUser::insert(
                        array('id' => null,
                            'request_id' => $request_id,
                            'user_id' => $auth_id,
                            'valid_id' => $auth_id,
                            'value' => 1,
                            'obs' => $obs,
                            'published' => 1,
                            'created_at' => $create_date,
                            'updated_at' => $create_date
                        )
                );
            } else if ($section === "woodfinishing") {
                \App\Model\WoodFinishingUser::insert(
                        array('id' => null,
                            'request_id' => $request_id,
                            'user_id' => $auth_id,
                            'valid_id' => $auth_id,
                            'value' => 1,
                            'obs' => $obs,
                            'published' => 1,
                            'created_at' => $create_date,
                            'updated_at' => $create_date
                        )
                );
            } else {
                \App\Model\PaintUser::insert(
                        array('id' => null,
                            'request_id' => $request_id,
                            'user_id' => $auth_id,
                            'valid_id' => $auth_id,
                            'value' => 1,
                            'obs' => $obs,
                            'published' => 1,
                            'created_at' => $create_date,
                            'updated_at' => $create_date
                        )
                );
            }
        }
        return [];
    }

    public function get_cnc_picker($id) {
        if (strlen($id) == 8) {
        //            $get_CODE = substr($id, 0, -7);
        //            $get_PP = substr($id, 5, -3);
        //            $get_SERIAL = trim(substr($id, -3));
        //            $GG = [];
        //            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
        //            $dump_PP = \App\Model\Requests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();

            // PARTIR O NC_CODE COM O COLOR_CODE
            $get_nc_code = substr($id, 0, 5);
            $get_color_code = substr($id, -3);

            $dump_PP = \App\Model\Product::where('code', $get_nc_code)->where('mini_code', 'NC')->first();
            if (count($dump_PP) == 1) {

                // CHECK COLOR CODE
                $dump_COLOR = \App\Model\ProductCustomNxt::where('color_code', $get_color_code)->first();
                if (count($dump_COLOR) == 1) {

                    $CHECK_PP = \App\Model\Request::where('product_id', $dump_PP->id)->where('custom_id', $dump_COLOR->id)->orderBy('id', 'asc')->get();
                    $CHECK_PP_SUM = $CHECK_PP->sum('value');
                    $GET_PP_DONE = 0;

                    foreach ($CHECK_PP as $row) {
                        $GET_PP_DONE = $GET_PP_DONE + \App\Model\RequestUser::where('request_id', $row->id)->sum('value');
                    }

                    $GG['CHECK_PP_SUM'] = $CHECK_PP_SUM;
                    $GG['GET_PP_DONE'] = $GET_PP_DONE;
                    $GG['GET_PP_MAX'] = $CHECK_PP_SUM - $GET_PP_DONE;

                    $dump_VALUE = $dump_PP->value;
                    $dump_SERIAL = \App\Model\RequestUser::where('request_id', $dump_PP->id)->sum('value');
                    if ($GG['GET_PP_MAX'] > 0) {

                        foreach ($CHECK_PP as $row) {
                            $GET_UNFINISHED_PP = \App\Model\RequestUser::where('request_id', $row->id)->sum('value');
                            if ($CHECK_PP_SUM > $GET_UNFINISHED_PP) {
                                $GG['pp_to_add'] = $row->id;
                                break;
                            }
                        }

                        $GG['id'] = $get_nc_code;
                        $GG['custom_id'] = $dump_COLOR->id;
                        $GG['custom_code'] = $get_color_code;
                        $GG['custom_name'] = $dump_COLOR->name;
                        $GG['pp'] = $row->pp;
                        $GG['product_id'] = $dump_PP->id;
                        $GG['product_code'] = $dump_PP->code;
                        $GG['product_name'] = $dump_PP->name;

                        return $GG;
                    } else {
                        $GG['id'] = 0;
                        $GG['dump'] = "product PP are all full";
                        $GG['dump_value'] = $dump_VALUE;
                        $GG['serial_count'] = $dump_SERIAL;
                        return $GG;
                    }
                }
                else {
                    $GG['id'] = 0;
                    $GG['dump'] = "color code not found";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "product code not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_paint_picagem_serial($id) {
        if (strlen($id) == 8) {

            // PARTIR O NC_CODE COM O COLOR_CODE
            $get_nc_code = substr($id, 0, 5);
            $get_color_code = substr($id, -3);

            $dump_PP = \App\Model\Product::where('code', $get_nc_code)->where('mini_code', 'NC')->first();
            if (count($dump_PP) == 1) {

                // CHECK COLOR CODE
                $dump_COLOR = \App\Model\ProductCustomNxt::where('color_code', $get_color_code)->first();
                if (count($dump_COLOR) == 1) { 

                    $CHECK_PP = \App\Model\Request::where('product_id', $dump_PP->id)->where('custom_id', $dump_COLOR->id)->orderBy('id', 'asc')->get();
                    $CHECK_PP_SUM = $CHECK_PP->sum('value');

                    $GET_PP_DONE = 0;
                    $GET_PP_NOW = 0;

                    foreach ($CHECK_PP as $row) {
                        $GET_PP_DONE = $GET_PP_DONE + \App\Model\WoodFinishingUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                        $GET_PP_NOW = $GET_PP_NOW + \App\Model\PaintUser::where('request_id', $row->id)->sum('value');
                    }

                    $GG['CHECK_PP_SUM'] = $CHECK_PP_SUM;
                    $GG['GET_PP_DONE'] = $GET_PP_DONE;
                    $GG['GET_PP_MAX'] = $GET_PP_DONE - $GET_PP_NOW;

                    $dump_SERIAL = \App\Model\PaintUser::where('request_id', $dump_PP->id)->sum('value');
                    $GG['serial_count'] = $dump_SERIAL;
                    if ($GG['GET_PP_MAX'] > 0) {

                        foreach ($CHECK_PP as $row) {
                            $GET_UNFINISHED_PP = \App\Model\PaintUser::where('request_id', $row->id)->sum('value');
                            if ($CHECK_PP_SUM > $GET_UNFINISHED_PP) {
                                $GG['pp_to_add'] = $row->id;
                                break;
                            }
                        }

                        $GG['id'] = $get_nc_code;
                        $GG['custom_id'] = $dump_COLOR->id;
                        $GG['custom_code'] = $get_color_code;
                        $GG['custom_name'] = $dump_COLOR->name;
                        $GG['pp'] = $row->pp;
                        $GG['product_id'] = $dump_PP->id;
                        $GG['product_code'] = $dump_PP->code;
                        $GG['product_name'] = $dump_PP->name;

                        return $GG;
                    } else {
                        $GG['id'] = 0;
                        $GG['dump'] = "product PP are all full";
                        $GG['dump_value'] = $GG['GET_PP_MAX'];
                        return $GG;
                    }
                }
                else {
                    $GG['id'] = 0;
                    $GG['dump'] = "color code not found";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "product code not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_woodfinishing_picagem_serial($id) {
        if (strlen($id) == 8) {

            // PARTIR O NC_CODE COM O COLOR_CODE
            $get_nc_code = substr($id, 0, 5);
            $get_color_code = substr($id, -3);

            $dump_PP = \App\Model\Product::where('code', $get_nc_code)->where('mini_code', 'NC')->first();
            if (count($dump_PP) == 1) {

                // CHECK COLOR CODE
                $dump_COLOR = \App\Model\ProductCustomNxt::where('color_code', $get_color_code)->first();
                if (count($dump_COLOR) == 1) {     

                    $CHECK_PP = \App\Model\Request::where('product_id', $dump_PP->id)->where('custom_id', $dump_COLOR->id)->orderBy('id', 'asc')->get();
                    $CHECK_PP_SUM = $CHECK_PP->sum('value');

                    $GET_PP_DONE = 0;
                    $GET_PP_NOW = 0;

                    foreach ($CHECK_PP as $row) {
                        $GET_PP_DONE = $GET_PP_DONE + \App\Model\WoodUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                        $GET_PP_NOW = $GET_PP_NOW + \App\Model\WoodFinishingUser::where('request_id', $row->id)->sum('value');
                    }

                    $GG['CHECK_PP_SUM'] = $CHECK_PP_SUM;
                    $GG['GET_PP_DONE'] = $GET_PP_DONE;
                    $GG['GET_PP_MAX'] = $GET_PP_DONE - $GET_PP_NOW;

                    $dump_SERIAL = \App\Model\WoodFinishingUser::where('request_id', $dump_PP->id)->sum('value');
                    $GG['serial_count'] = $dump_SERIAL;
                    if ($GG['GET_PP_MAX'] > 0) {

                        foreach ($CHECK_PP as $row) {
                            $GET_UNFINISHED_PP = \App\Model\WoodFinishingUser::where('request_id', $row->id)->sum('value');
                            if ($CHECK_PP_SUM > $GET_UNFINISHED_PP) {
                                $GG['pp_to_add'] = $row->id;
                                break;
                            }
                        }

                        $GG['id'] = $get_nc_code;
                        $GG['custom_id'] = $dump_COLOR->id;
                        $GG['custom_code'] = $get_color_code;
                        $GG['custom_name'] = $dump_COLOR->name;
                        $GG['pp'] = $row->pp;
                        $GG['product_id'] = $dump_PP->id;
                        $GG['product_code'] = $dump_PP->code;
                        $GG['product_name'] = $dump_PP->name;

                        return $GG;
                    } else {
                        $GG['id'] = 0;
                        $GG['dump'] = "product PP are all full";
                        $GG['dump_value'] = $GG['GET_PP_MAX'];
                        return $GG;
                    }
                }
                else {
                    $GG['id'] = 0;
                    $GG['dump'] = "color code not found";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "product code not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_prep_picagem_serial($id) {
        if (strlen($id) == 8) {
            
            // PARTIR O NC_CODE COM O COLOR_CODE
            $get_nc_code = substr($id, 0, 5);
            $get_color_code = substr($id, -3);

            $dump_PP = \App\Model\Product::where('code', $get_nc_code)->where('mini_code', 'NC')->first();
            if (count($dump_PP) == 1) {

                // CHECK COLOR CODE
                $dump_COLOR = \App\Model\ProductCustomNxt::where('color_code', $get_color_code)->first();
                if (count($dump_COLOR) == 1) {                

                    $CHECK_PP = \App\Model\Request::where('product_id', $dump_PP->id)->where('custom_id', $dump_COLOR->id)->orderBy('id', 'asc')->get();
                    $CHECK_PP_SUM = $CHECK_PP->sum('value');

                    $GET_PP_DONE = 0;
                    $GET_PP_NOW = 0;

                    foreach ($CHECK_PP as $row) {
                        $GET_PP_DONE = $GET_PP_DONE + \App\Model\RequestUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                        $GET_PP_NOW = $GET_PP_NOW + \App\Model\PrepUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                    }

                    $GG['CHECK_PP_SUM'] = $CHECK_PP_SUM;
                    $GG['GET_PP_DONE'] = $GET_PP_DONE;
                    $GG['GET_PP_MAX'] = $GET_PP_DONE - $GET_PP_NOW;

                    $dump_SERIAL = \App\Model\PrepUser::where('request_id', $dump_PP->id)->sum('value');
                    $GG['serial_count'] = $dump_SERIAL;
                    if ($GG['GET_PP_MAX'] > 0) {

                        foreach ($CHECK_PP as $row) {
                            $GET_UNFINISHED_PP = \App\Model\PrepUser::where('request_id', $row->id)->sum('value');
                            if ($CHECK_PP_SUM > $GET_UNFINISHED_PP) {
                                $GG['pp_to_add'] = $row->id;
                                break;
                            }
                        }

                        $GG['id'] = $get_nc_code;
                        $GG['custom_id'] = $dump_COLOR->id;
                        $GG['custom_code'] = $get_color_code;
                        $GG['custom_name'] = $dump_COLOR->name;
                        $GG['pp'] = $row->pp;
                        $GG['product_id'] = $dump_PP->id;
                        $GG['product_code'] = $dump_PP->code;
                        $GG['product_name'] = $dump_PP->name;

                        return $GG;
                    } else {
                        $GG['id'] = 0;
                        $GG['dump'] = "product PP are all full";
                        $GG['dump_value'] = $GG['GET_PP_MAX'];
                        return $GG;
                    }
                }
                else {
                    $GG['id'] = 0;
                    $GG['dump'] = "color code not found";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "product code not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_prep_picker($id) {
        if (strlen($id) == 4) {
//            $get_CODE = substr($id, 0, -7);
//            $get_PP = substr($id, 5, -3);
//            $get_SERIAL = trim(substr($id, -3));
//            $GG = [];
//            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
//            $dump_PP = \App\Model\Requests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();
            $dump_PP = \App\Model\Request::where('pp', $id)->first();
            if (count($dump_PP) > 0) {
                $dump_VALUE = $dump_PP->value;
                $dump_SERIAL = \App\Model\PrepUser::where('request_id', $dump_PP->id)->sum('value');
                if ($dump_VALUE - $dump_SERIAL > 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $dump_CODE = \App\Model\Product::where('id', $dump_PP->product_id)->first();
                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    //$temp_total = \App\Model\Request::where('id', $dump_PP->id)->sum('value');
                    //$temp_check = \App\Model\RequestUser::where('request_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $dump_VALUE - $dump_SERIAL; //$temp_total - $temp_check;
                    $GG['product_next'] = $dump_VALUE - $GG['product_count'];

                    //$GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['dump'] = "pp is full";
                    $GG['dump_value'] = $dump_VALUE;
                    $GG['serial_count'] = $dump_SERIAL;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "pp not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_wood_picagem_serial($id) {
        if (strlen($id) == 8) {

            // PARTIR O NC_CODE COM O COLOR_CODE
            $get_nc_code = substr($id, 0, 5);
            $get_color_code = substr($id, -3);

            $dump_PP = \App\Model\Product::where('code', $get_nc_code)->where('mini_code', 'NC')->first();
            if (count($dump_PP) == 1) {

                // CHECK COLOR CODE
                $dump_COLOR = \App\Model\ProductCustomNxt::where('color_code', $get_color_code)->first();
                if (count($dump_COLOR) == 1) {                

                    $CHECK_PP = \App\Model\Request::where('product_id', $dump_PP->id)->where('custom_id', $dump_COLOR->id)->orderBy('id', 'asc')->get();
                    $CHECK_PP_SUM = $CHECK_PP->sum('value');

                    $GET_PP_DONE = 0;
                    $GET_PP_NOW = 0;

                    foreach ($CHECK_PP as $row) {
                        $GET_PP_DONE = $GET_PP_DONE + \App\Model\PrepUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                        $GET_PP_NOW = $GET_PP_NOW + \App\Model\WoodUser::where('request_id', $row->id)->where('published', 1)->sum('value');
                    }

                    $GG['CHECK_PP_SUM'] = $CHECK_PP_SUM;
                    $GG['GET_PP_DONE'] = $GET_PP_DONE;
                    $GG['GET_PP_MAX'] = $GET_PP_DONE - $GET_PP_NOW;

                    $dump_SERIAL = \App\Model\WoodUser::where('request_id', $dump_PP->id)->sum('value');
                    $GG['serial_count'] = $dump_SERIAL;
                    if ($GG['GET_PP_MAX'] > 0) {

                        foreach ($CHECK_PP as $row) {
                            $GET_UNFINISHED_PP = \App\Model\WoodUser::where('request_id', $row->id)->sum('value');
                            if ($CHECK_PP_SUM > $GET_UNFINISHED_PP) {
                                $GG['pp_to_add'] = $row->id;
                                break;
                            }
                        }

                        $GG['id'] = $get_nc_code;
                        $GG['custom_id'] = $dump_COLOR->id;
                        $GG['custom_code'] = $get_color_code;
                        $GG['custom_name'] = $dump_COLOR->name;
                        $GG['pp'] = $row->pp;
                        $GG['product_id'] = $dump_PP->id;
                        $GG['product_code'] = $dump_PP->code;
                        $GG['product_name'] = $dump_PP->name;

                        return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['dump'] = "product PP are all full";
                    $GG['dump_value'] = $GG['GET_PP_MAX'];
                    return $GG;
                    }
                }
                else {
                    $GG['id'] = 0;
                    $GG['dump'] = "color code not found";
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "product code not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_wood_picker($id) {
        if (strlen($id) == 4) {
//            $get_CODE = substr($id, 0, -7);
//            $get_PP = substr($id, 5, -3);
//            $get_SERIAL = trim(substr($id, -3));
//            $GG = [];
//            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
//            $dump_PP = \App\Model\Requests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();
            $dump_PP = \App\Model\Request::where('pp', $id)->first();
            if (count($dump_PP) > 0) {
                $dump_VALUE = $dump_PP->value;
                $dump_SERIAL = \App\Model\WoodUser::where('request_id', $dump_PP->id)->sum('value');
                if ($dump_VALUE - $dump_SERIAL > 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $dump_CODE = \App\Model\Product::where('id', $dump_PP->product_id)->first();
                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    //$temp_total = \App\Model\Request::where('id', $dump_PP->id)->sum('value');
                    //$temp_check = \App\Model\RequestUser::where('request_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $dump_VALUE - $dump_SERIAL; //$temp_total - $temp_check;
                    $GG['product_next'] = $dump_VALUE - $GG['product_count'];

                    //$GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['dump'] = "pp is full";
                    $GG['dump_value'] = $dump_VALUE;
                    $GG['serial_count'] = $dump_SERIAL;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "pp not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_woodfinishing_picker($id) {
        if (strlen($id) == 4) {
//            $get_CODE = substr($id, 0, -7);
//            $get_PP = substr($id, 5, -3);
//            $get_SERIAL = trim(substr($id, -3));
//            $GG = [];
//            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
//            $dump_PP = \App\Model\Requests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();
            $dump_PP = \App\Model\Request::where('pp', $id)->first();
            if (count($dump_PP) > 0) {
                $dump_VALUE = $dump_PP->value;
                $dump_SERIAL = \App\Model\WoodFinishingUser::where('request_id', $dump_PP->id)->sum('value');
                if ($dump_VALUE - $dump_SERIAL > 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $dump_CODE = \App\Model\Product::where('id', $dump_PP->product_id)->first();
                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    //$temp_total = \App\Model\Request::where('id', $dump_PP->id)->sum('value');
                    //$temp_check = \App\Model\RequestUser::where('request_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $dump_VALUE - $dump_SERIAL; //$temp_total - $temp_check;
                    $GG['product_next'] = $dump_VALUE - $GG['product_count'];

                    //$GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['dump'] = "pp is full";
                    $GG['dump_value'] = $dump_VALUE;
                    $GG['serial_count'] = $dump_SERIAL;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "pp not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function get_paint_picker($id) {
        if (strlen($id) == 4) {
//            $get_CODE = substr($id, 0, -7);
//            $get_PP = substr($id, 5, -3);
//            $get_SERIAL = trim(substr($id, -3));
//            $GG = [];
//            $dump_CODE = \App\Model\Tech::where('code', $get_CODE)->first();
//            $dump_PP = \App\Model\Requests::where('pp', $get_PP)->where('tech_id', $dump_CODE->id)->first();
            $dump_PP = \App\Model\Request::where('pp', $id)->first();
            if (count($dump_PP) > 0) {
                $dump_VALUE = $dump_PP->value;
                $dump_SERIAL = \App\Model\PaintUser::where('request_id', $dump_PP->id)->sum('value');
                if ($dump_VALUE - $dump_SERIAL > 0) {
                    $GG['id'] = $id;
                    $GG['request_id'] = $dump_PP->id;
                    $GG['request_pp'] = $dump_PP->pp;
                    $GG['request_value'] = $dump_PP->value;

                    $dump_CODE = \App\Model\Product::where('id', $dump_PP->product_id)->first();
                    $GG['product_id'] = $dump_CODE->id;
                    $GG['product_code'] = $dump_CODE->code;
                    $GG['product_name'] = $dump_CODE->name;

                    //$temp_total = \App\Model\Request::where('id', $dump_PP->id)->sum('value');
                    //$temp_check = \App\Model\RequestUser::where('request_id', $dump_PP->id)->where('published', 1)->sum('value');
                    $GG['product_count'] = $dump_VALUE - $dump_SERIAL; //$temp_total - $temp_check;
                    $GG['product_next'] = $dump_VALUE - $GG['product_count'];

                    //$GG['serial'] = $get_SERIAL;

                    return $GG;
                } else {
                    $GG['id'] = 0;
                    $GG['dump'] = "pp is full";
                    $GG['dump_value'] = $dump_VALUE;
                    $GG['serial_count'] = $dump_SERIAL;
                    return $GG;
                }
            } else {
                $GG['id'] = 0;
                $GG['dump'] = "pp not found";
                return $GG;
            }
        } else {
            $GG['id'] = 0;
            $GG['dump'] = "strlen not valid";
            return $GG;
        }
    }

    public function set_work_absences($id, $valid) {
        WorkAbsences::where('id', $id)->update(['valid_id' => $valid]);
        return [];
    }

    public function set_oclock($id, $value) {
        UserLogin::insert(array('id' => null, 'user_id' => $id, 'login_logout' => $value, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
    }

    public function request_palete_to_mount($id, $user_id) {
        \App\Model\Notifications::insert(array('id' => null, 'section' => 'armazem', 'user' => 0, 'type' => 1, 'object_id' => $id, 'open_user_id' => $user_id, 'open_at' => \Carbon\Carbon::now()->toDateTimeString(), 'close_user_id' => 0, 'close_at' => \Carbon\Carbon::now()->toDateTimeString(), 'status' => 0, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
        return [];
    }

    public function request_palete_to_mount_ok($id, $user_id) {
        \App\Model\Notifications::where('object_id', $id)->where('type', 1)->where('status', 0)->update(['close_user_id' => $user_id, 'close_at' => \Carbon\Carbon::now()->toDateTimeString(), 'status' => 1]);
        return [];
    }

    public function new_triple() {
        $check = \App\Model\RequestWeek::orderBy('end_at', 'asc')->get();

        // lista de pedidos semanais por ordem
        $SEMANAL = [];
        $week = [];
        $week_missing = [];
        //print_r("<pre>");
        foreach ($check as $key => $instance) {
            $total = \App\Model\RequestWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('montagem');

            $total_week = \App\Model\RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->sum('montagem');

            $total_req = \App\Model\Request::where('product_id', $instance->product_id)->first();

            //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

            $total_req2 = \App\Model\Request::where('product_id', $instance->product_id)->get();
            $total_inc = 0;
            foreach ($total_req2 as $go) {
                $dump = \App\Model\MountUser::where('request_id', $go->id)->sum('value');
//                    $dump = MountUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                $total_inc = $total_inc + $dump;
            }
            //print_r($total_inc);

            $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
            $SEMANAL[$instance->product_id]['name'] = \App\Model\Product::where('id', $instance->product_id)->first()->name;
            $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
            $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

            foreach (\App\Model\RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->montagem;
            }
        }
        //print_r($SEMANAL);
        foreach ($SEMANAL as $key2 => $value2) {
            foreach ($value2['semana'] as $key3 => $value3) {
                if ($SEMANAL[$key2]['total_montado'] > 0) {
                    if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                    } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                    }
                    //print_r($SEMANAL[$key2]['semana'][$key3]);
                }
                if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                    $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    $week_missing[$SEMANAL[$key2]['semana'][$key3]['id']] = $SEMANAL[$key2]['semana'][$key3]['value'];
                }
            }
        }

//        DB::statement("DELETE from triple_week WHERE id!='x'");
        DB::statement("TRUNCATE TABLE triple_week");
        foreach ($week as $triple) {
            $result_encomenda = \App\Model\RequestWeek::where('id', $triple)->first();
            $r_end_at = new Carbon($result_encomenda->end_at);
            $r_product_id = $result_encomenda->product_id;
            $r_client = $result_encomenda->client;
            $r_quantity = $result_encomenda->quantity;

            //
            //CREATE TEMPORARY TABLE temporary_table AS SELECT * FROM original_table WHERE <conditions>;
            //UPDATE temporary_table SET <auto_inc_field>=NULL, <fieldx>=<valuex>, <fieldy>=<valuey>, ...;
            //INSERT INTO original_table SELECT * FROM temporary_table;
            //
            
            DB::statement("INSERT INTO triple_week SELECT *, 0 as 'requestweek_id' FROM request_week WHERE id='$triple'");
            DB::table('triple_week')->where('end_at', $result_encomenda->end_at)->where('quantity', $r_quantity)->where('client', $r_client)->where('product_id', $r_product_id)->update(['end_at' => $r_end_at, 'montagem' => $week_missing[$triple], 'requestweek_id' => $triple]);
        }
        // $GG['falta'];        
        return 1;
    }

    public function new_triple_week() {
        DB::statement("DELETE from freeze_week WHERE week!='-1'");

        $results = DB::select('SELECT * from triple_week ORDER BY end_at ASC,obs ASC');

        global $POINTS_x;
        $POINTS_x = 0;
        $config = \App\Model\Config::find(1);
        $week_0 = $config->week_0;
        $week_1 = $week_0 + $config->week_1;
        $week_2 = $week_1 + $config->week_2;
        $week_3 = $week_2 + $config->week_3;

        foreach ($results as $row) {
            // VERIFICAR SE O ID EXISTE EM ATRASO
            $requestweek_id = $row->requestweek_id;
            $check_atraso = DB::select("SELECT * from freeze_week WHERE requestweek_id='$requestweek_id' AND week='-1'");
            if (count($check_atraso) == 0) {
                $get_product_info = \App\Model\Product::where('id', $row->product_id)->first();
                $temp_var = 0;
                while ($temp_var < $row->montagem) {
//                print_r("$row->client | $row->quantity | $row->montagem | $row->obs \n");

                    $temp_var++;
                    $POINTS_x = $POINTS_x + $get_product_info->points;
                    if ($POINTS_x <= $week_0) {
                        $SET_WEEK = 0;
                    } elseif ($POINTS_x <= $week_1) {
                        $SET_WEEK = 1;
                    } elseif ($POINTS_x <= $week_2) {
                        $SET_WEEK = 2;
                    } else {
                        $SET_WEEK = 3;
                    }
                    \App\Model\FreezeWeek::insert(array('id' => null, 'product_id' => $row->product_id, 'user_id' => $row->user_id, 'quantity' => $row->quantity, 'carpintaria' => 1, 'woodfinishing' => 1, 'pintura' => 1, 'montagem' => 1, 'client' => $row->client, 'obs' => $row->obs, 'end_at' => $row->end_at, 'end_paint_at' => $row->end_paint_at, 'end_woodfinishing_at' => $row->end_woodfinishing_at, 'end_wood_at' => $row->end_wood_at, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString(), 'group' => 0, 'week' => $SET_WEEK, 'requestweek_id' => $row->requestweek_id));
                }
            }
        }
        return $POINTS_x;
    }

    public function new_triple_week_up() {
        DB::update("update freeze_week SET week='-1',end_at=end_at WHERE week='0'");
        DB::update("update freeze_week SET week='0',end_at=end_at WHERE week='1'");
        DB::update("update freeze_week SET week='1',end_at=end_at WHERE week='2'");

        $results = DB::select('SELECT * from freeze_week WHERE week=3 ORDER BY end_at ASC');

        if (count($results) > 0) {
            $POINTS = 0;
            $config = \App\Model\Config::find(1);
            $week_2 = $config->week_2;
            foreach ($results as $row) {
                $get_product_info = \App\Model\Product::where('id', $row->product_id)->first();
                $temp_var = 0;
                while ($temp_var < $row->montagem) {
                    $POINTS = $POINTS + $get_product_info->points;

                    if ($POINTS <= $week_2) {
                        DB::update("update freeze_week SET week='2',end_at=end_at WHERE id='$row->id'");
                    }
                    $temp_var++;
                }
            }
        } else {
            // quando esvaziar a ultima semana, manda email
            return 2;
        }
        //DB::update("update freeze_week SET week='2',end_at=end_at WHERE week='3'");      
        return 1;
    }

    public function getProductivityInfo($id, $type, $day = null, $like = null) {
        if (!$day && !$like) {
            $start = strtotime($type);
            $start = date('w', $start) == date('w') ? $start + 7 * 86400 : $start;
            $end = strtotime(date("Y-m-d", $start) . " +6 days");
            $this_week_sd = date("Y-m-d", $start);
            $this_week_ed = date("Y-m-d", $end);
            $totalCNC = RequestUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalPREP = PrepUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalWOOD = WoodUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();        
            $totalWOODF = WoodFinishingUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalWOODB = BLACKFINISHING_History::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalPAINT = PaintUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalMOUNT = MountUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalLOCKSMITH = LocksmithRequestsUser::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            $totalWIRES = WIRES_History::where('user_id', $id)->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        }
        else {
            if ($type == 'like') {
                
                $ano = date("Y", strtotime(date($like)));
                $mes = date("m", strtotime(date($like)));

                if ($mes < 10) {
                    $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
                }
                $like = "$ano-$mes-%";
                $this_week_ed = $like;
                $this_week_sd = $like;

                $totalCNC = RequestUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalPREP = PrepUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalWOOD = WoodUser::where('user_id', $id)->where('created_at', 'like', $like)->get();        
                $totalWOODF = WoodFinishingUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalWOODB = BLACKFINISHING_History::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalPAINT = PaintUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalMOUNT = MountUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalLOCKSMITH = LocksmithRequestsUser::where('user_id', $id)->where('created_at', 'like', $like)->get();
                $totalWIRES = WIRES_History::where('user_id', $id)->where('created_at', 'like', $like)->get();     
            }
            else {
                $start = strtotime($type);
                //$start = strtotime(date("Y-m-$day", $start) . " last month first day");
                $start = strtotime(date("Y-m-$day", $start));
                //$end = strtotime(date("Y-m-$day", $start) . " last day last month");
                $end = strtotime(date("Y-m-d", $start) . "-1 month +1 day");
                $this_week_ed = date("Y-m-d", $start);
                //$this_week_ed = date("Y-m-28", $start);
                $this_week_sd = date("Y-m-d", $end);
                //$this_week_sd = date("Y-m-1", $end);
                $totalCNC = RequestUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalPREP = PrepUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();        
                $totalWOOD = WoodUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalWOODF = WoodFinishingUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalWOODB = BLACKFINISHING_History::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalPAINT = PaintUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalMOUNT = MountUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalLOCKSMITH = LocksmithRequestsUser::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
                $totalWIRES = WIRES_History::whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
            }
        }
        
        $info = [];
        $info['rangeStart'] = $this_week_sd;
        $info['rangeEnd'] = $this_week_ed;

        // cnc
        $CNC = 0;
        foreach ($totalCNC as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CNC = $CNC + $valor;
        }
        // preparação
        $PREP = 0;
        foreach ($totalPREP as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PREP = $PREP + $valor;
        }
        // carpintaria
        $CARPINTARIA = 0;
        foreach ($totalWOOD as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA = $CARPINTARIA + $valor;
        }

        // acabamento da carpintaria
        $CARPINTARIA_ACABAMENTO = 0;
        foreach ($totalWOODF as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_ACABAMENTO = $CARPINTARIA_ACABAMENTO + $valor;
        }

        // acabamento de preto
        $CARPINTARIA_PRETO = 0;
        foreach ($totalWOODB as $value) {
            $pontos = Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_PRETO = $CARPINTARIA_PRETO + $valor;
        }

        // pintura
        $PINTURA = 0;
        foreach ($totalPAINT as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PINTURA = $PINTURA + $valor;
        }

        // montagem
        $MONTAGEM = 0;
        foreach ($totalMOUNT as $value) {
            $product_id = Request2::where('id', $value->request_id)->first()->product_id;
            $pontos = Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $MONTAGEM = $MONTAGEM + $valor;
        }

        // serralharia
        $SERRALHARIA = 0;
        foreach ($totalLOCKSMITH as $value) {
            if ($value->locksmith_requests_id) {
                $product_id = LocksmithRequests::where('id', $value->locksmith_requests_id)->first()->locksmith_id;
                $pontos = Locksmith::where('id', $product_id)->first()->points;
                $valor = $value->value * $pontos;
            } else {
                $valor = 0;
            }
            $SERRALHARIA = $SERRALHARIA + $valor;
        }

        // redes
        $REDES = 0;
        foreach ($totalWIRES as $value) {
            $pontos = Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $REDES = $REDES + $valor;
        }
        $info['CNC'] = $CNC;
        $info['PREP'] = $PREP;
        $info['WOOD'] = $CARPINTARIA;
        $info['WOODF'] = $CARPINTARIA_ACABAMENTO;
        $info['WOODB'] = $CARPINTARIA_PRETO;
        $info['PAINT'] = $PINTURA;
        $info['MOUNT'] = $MONTAGEM;
        $info['LOCKSMITH'] = $SERRALHARIA;
        $info['WIRES'] = $REDES;
        $TOTAL = $CNC + $PREP + $CARPINTARIA + $CARPINTARIA_ACABAMENTO + $CARPINTARIA_PRETO + $PINTURA + $MONTAGEM + $SERRALHARIA + $REDES;
        $info['TOTAL'] = $TOTAL;
        return $info;
    }

    public function pdf($id, $value) {

        // section performance
        $sections[0]['name'] = 'CNC';
        $sections[0]['table'] = 'request_users';
        $sections[0]['points'] = 'CNC';
        $sections[1]['name'] = 'Preparação';
        $sections[1]['table'] = 'prep_users';
        $sections[1]['points'] = 'PREP';
        $sections[2]['name'] = "Carpintaria";
        $sections[2]['table'] = "wood_users";
        $sections[2]['points'] = "WOOD";
        $sections[3]['name'] = "Acabamento de Carpintaria";
        $sections[3]['table'] = "stock_users";
        $sections[3]['points'] = "WOODF";
        $sections[4]['name'] = "Acabamento de Preto";
        $sections[4]['table'] = "blackfinishing";
        $sections[4]['points'] = "WOODB";
        $sections[5]['name'] = "Pintura";
        $sections[5]['table'] = "paint_users";
        $sections[5]['points'] = "PAINT";
        $sections[6]['name'] = "Montagem";
        $sections[6]['table'] = "mount_users";
        $sections[6]['points'] = "MOUNT";
        $sections[7]['name'] = "Serralharia";
        $sections[7]['table'] = "locksmith_requests_users";
        $sections[7]['points'] = "LOCKSMITH";
        $sections[8]['name'] = "Redes";
        $sections[8]['table'] = "wires";
        $sections[8]['points'] = "WIRES";
        
        if (($value == "palete_pdf") || ($value == "palete_pdf_view")) {
            $sql_info = \App\Model\Palete::where('id', $id)->first();
            $sql_request_week = \App\Model\RequestWeek::where('id', $sql_info->request_week_id)->first();
            $sql_product = \App\Model\Product::where('id', $sql_request_week->product_id)->first();
            $separado = \App\User::where('id', $sql_info->store_id)->first()->name;
            $autorizado = \App\User::where('id', $sql_info->auth_id)->first()->name;

            $pdf = PDF::loadView('pdf.paletes', compact('sql_info', 'sql_request_week', 'sql_product', 'separado', 'autorizado'));
            $FILE_NAME = "NEXT-palete#" . $sql_request_week->id . "_" . $id . "_" . $sql_request_week->product_id;
        } elseif (($value == "locksmith_palete_pdf") || ($value == "locksmith_palete_pdf_view")) {
            $sql_info = \App\Model\LocksmithPalete::where('id', $id)->first();
            $sql_request_week = \App\Model\LocksmithWeek::where('id', $sql_info->locksmith_week_id)->first();
            $sql_product = \App\Model\Locksmith::where('id', $sql_request_week->locksmith_id)->first();
            $separado = \App\User::where('id', $sql_info->store_id)->first()->name;
            $autorizado = \App\User::where('id', $sql_info->auth_id)->first()->name;

            $pdf = PDF::loadView('pdf.paletes', compact('sql_info', 'sql_request_week', 'sql_product', 'separado', 'autorizado'));
            $FILE_NAME = "NEXT-serralharia_palete#" . $sql_request_week->id . "_" . $id . "_" . $sql_request_week->locksmith_id;
        } elseif (($value == "tech_palete_pdf") || ($value == "tech_palete_pdf_view")) {
            $sql_info = \App\Model\TechPalete::where('id', $id)->first();
            $sql_request_week = \App\Model\TechWeek::where('id', $sql_info->tech_week_id)->first();
            $sql_product = \App\Model\Tech::where('id', $sql_request_week->tech_id)->first();
            $separado = \App\User::where('id', $sql_info->store_id)->first()->name;
            $autorizado = \App\User::where('id', $sql_info->auth_id)->first()->name;

            $pdf = PDF::loadView('pdf.paletes', compact('sql_info', 'sql_request_week', 'sql_product', 'separado', 'autorizado'));
            $FILE_NAME = "NEXT-tecnica_palete#" . $sql_request_week->id . "_" . $id . "_" . $sql_request_week->tech_id;
        } elseif ($value == "comsumable_pdf") {

            $sql_info = \App\Model\ComsumablesOrders::where('id', $id)->first();
            $sql_section = $sql_info->section;
            $sql_comsumables = DB::table('comsumables_orders_items')
                    ->join('comsumables_orders', 'comsumables_orders.id', '=', 'comsumables_orders_items.comsumables_order_id')
                    ->join('comsumables', 'comsumables.id', '=', 'comsumables_orders_items.comsumables_id')
                    ->where('comsumables_orders.id', '=', $id)
                    ->get();
            $sql_comsumables_new = \App\Model\ComsumablesNew::where('comsumables_order_id', $id)->get();
            $autor = \App\User::where('id', $sql_info->auth_id)->first()->name;
            $pdf = PDF::loadView('pdf.comsumables', compact('sql_info', 'sql_comsumables', 'sql_comsumables_new', 'autor', 'sql_section'));
            $FILE_NAME = "NEXT-comsumable#" . $id;
        } elseif ($value == "user_pdf") {

            $user_info = \App\User::where('id', $id)->first();
            $autor = $user_info->bigname;
            $date_points = $user_info->date_points;
            $sql_comsumables = DB::table('comsumables_orders_items')
                    ->join('comsumables_orders', 'comsumables_orders.id', '=', 'comsumables_orders_items.comsumables_order_id')
                    ->join('comsumables', 'comsumables.id', '=', 'comsumables_orders_items.comsumables_id')
                    ->where('comsumables_orders.id', '=', $id)
                    ->get();
            $sql_comsumables_new = \App\Model\ComsumablesNew::where('comsumables_order_id', $id)->get();

            //$sections = ['CNC', 'Preparação', 'Carpintaria', 'Acabamento de Carpintaria', 'Acabamento de Preto', 'Pintura', 'Montagem', 'Serralharia', 'Redes'];

            for ($i = 1; $i <= 12; $i++) {
                $months[$i]['date'] = date("m\nY", strtotime(date('Y-m-28') . " -$i months"));
                $months[$i]['prod'] = $this->getProductivityInfo($id, 'like', null, date("Y-m-28", strtotime(date('Y-m-28') . " -$i months")));;
            }
            $months = array_reverse($months);
            
            $thisWeek = $this->getProductivityInfo($id, 'last monday');
            $lastWeek = $this->getProductivityInfo($id, 'last monday last week');
            
            /* if ($date_points > 0)  { 
                $thisRange = $this->getProductivityInfo($id,'this month', $date_points); 
                $lastRange = $this->getProductivityInfo($id,'last month', $date_points); 
            }
            else { 
                $thisRange = ""; 
                $lastRange = ""; 
            } */

            $thisRange = $this->getProductivityInfo($id,'this month', 27); 
            $lastRange = $this->getProductivityInfo($id,'last month', 27); 


            $pdf = PDF::loadView('pdf.performance', compact('date_points', 'thisRange', 'lastRange', 'thisWeek', 'lastWeek', 'sql_comsumables', 'sql_comsumables_new', 'autor', 'sections', 'months'));
            $FILE_NAME = "USER#" . $id;
        } elseif ($value == "users_pdf") {

            // user perforance
            $sql_info = \App\User::where('status', 1)->where('performance', 1)->orderBy('bigname', 'asc')->get();

            
            //$st = new \Carbon\Carbon('first day of this month');
            //$st = \Carbon\Carbon::now()->day(28)->startOfDay();
            //$start_of_month = $st->format("Y-m-d");
            //$end_of_month = $st->endOfMonth()->format("Y-m-d");
            //$end_of_month = $st->copy()->day(27)->format("Y-m-d");

            $start_of_month = \Carbon\Carbon::now()->subMonthNoOverflow()->day(28)->startOfDay()->format("Y-m-d");
            $end_of_month = \Carbon\Carbon::now()->day(27)->endOfDay()->format("Y-m-d");

            foreach ($sections as $key => $section) {
                $SQL_m = DB::table($section["table"])->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
                $SQL_POINTS_MES = 0;
                // mes actual
                foreach ($SQL_m as $valuex) {
                    if (($section["table"] != "wires") && ($section["table"] != "locksmith_requests_users") && ($section["table"] != "blackfinishing")) {
                        $SQL_R = DB::table('requests')->where('id', $valuex->request_id)->first();
                        $SQL_P = DB::table('products')->where('id', $SQL_R->product_id)->first();
                    } else {
                        if ($section["table"] == "locksmith_requests_users") {
                            $SQL_R = DB::table('locksmith_requests')->where('id', $valuex->locksmith_requests_id)->first();
                            $SQL_P = DB::table('locksmith')->where('id', $SQL_R->locksmith_id)->first();
                        } else {
                            $SQL_P = DB::table('products')->where('id', $valuex->product_id)->first();
                        }
                    }
                    $POINTS = $SQL_P->points;
                    $SQL_POINTS_MES = $POINTS * $valuex->value + $SQL_POINTS_MES;
                }
                $sections[$key]['value_month'] = $SQL_POINTS_MES;
            }
            $pdf = PDF::loadView('pdf.performance_all', compact('sql_info', 'sections', 'start_of_month', 'end_of_month'));
            $FILE_NAME = "ALLUSERS";
        }
        if (($value != "palete_pdf_view") && ($value != "locksmith_palete_pdf_view") && ($value != "users_pdf")) {
            return $pdf->download($FILE_NAME . '.pdf');
        } else {
            return $pdf->stream($FILE_NAME . 'pdf');
        }
    }

    public function get_comsumables($id) {
        $sql = \App\Model\Comsumables::where("$id", 1)->orderby('nome', 'desc')->get();
        $GG = [];
        if (count($sql) > 0) {
            foreach ($sql as $key => $comsumable) {
                $GG[$key]['comsumable_id'] = $comsumable->id;
                $GG[$key]['ref'] = $comsumable->ref;
                $GG[$key]['nome'] = $comsumable->nome;
                $GG[$key]['unit'] = $comsumable->unit;
                $GG[$key]['min'] = $comsumable->between_start;
                $GG[$key]['max'] = $comsumable->between_end;
            }
        }
        return $GG;
    }

    public function distro_refresh() {
// CHECK PHC
        if (1 == 1) {
            DB::statement("TRUNCATE TABLE distributors");

            $server_port = '192.168.1.10';
            $port = '1433';

            $ser = $server_port; #the name of the SQL Server
            $db = "cvagroup"; #the name of the database
            $user = "cva"; #a valid username
            $pass = "cva295"; #a password for the username
            $instance = "PHC";

            $conn = odbc_connect("Driver={".env('ODBC_DRIVER')."};Server=$server_port;Instance=$instance;Database=$db;Client_CSet=UTF-8", $user, $pass);
            $sql = "SELECT * FROM cl WHERE tipo='DISTRIBUIDOR' OR tipo='DEALER'";
            $rs = odbc_exec($conn, $sql);

            $ESTE_ANO = date("Y");
            $ANO_ANTERIOR = date("Y") - 1;

            while (odbc_fetch_row($rs)) {
                $TYPE = trim(odbc_result($rs, "tipo"));
                $MARKET = trim(odbc_result($rs, "zona"));
                $CLIENT = trim(odbc_result($rs, "nome"));
                $SELLER = trim(odbc_result($rs, "vendnm"));
                $sql_first = "SELECT TOP 1 ndoc, fdata FROM ft WHERE (nmdoc='Factura' OR nmdoc='Fatura') AND nome='$CLIENT' ORDER BY fdata ASC";
                $rs_first = odbc_exec($conn, $sql_first);
                $FIRST = trim(odbc_result($rs_first, "fdata"));
                $sql_open = "SELECT TOP 1 obrano FROM bo WHERE nmdos='Encomenda de Cliente' AND fechada=0 AND nome='$CLIENT'";
                $rs_open = odbc_exec($conn, $sql_open);
                $OPEN_ = trim(odbc_result($rs_open, "obrano"));
                if ($OPEN_) {
                    $OPEN = $OPEN_;
                } else {
                    $OPEN = 0;
                }
                $sql_last = "SELECT TOP 1 obrano, dataobra FROM bo WHERE nmdos='Encomenda de Cliente' AND nome='$CLIENT' ORDER BY dataobra DESC";
                $rs_last = odbc_exec($conn, $sql_last);
                $LAST = trim(odbc_result($rs_last, "dataobra"));
                $sql_last_year_new = "SELECT * FROM (
SELECT 
NO,ESTAB,NOME

,SUM((CASE YEAR(FT.FDATA) WHEN YEAR(GETDATE())-1 THEN ETTILIQ ELSE 0 end)) AS '$ANO_ANTERIOR'
,SUM((CASE YEAR(FT.FDATA) WHEN YEAR(GETDATE()) THEN ETTILIQ ELSE 0 end)) AS '$ESTE_ANO'

FROM FT (NOLOCK)
WHERE 
YEAR(FDATA) BETWEEN YEAR(GETDATE())-1 AND YEAR(GETDATE())
AND anulado=0
AND FT.ndoc IN (1,4,12,14)
AND NOME='$CLIENT'
GROUP BY no,estab,nome
) X
ORDER BY '$ESTE_ANO' DESC";
                $rs_last_year_new = odbc_exec($conn, $sql_last_year_new);
                $YEAR = round(odbc_result($rs_last_year_new, "$ESTE_ANO"), 2);
                $LAST_YEAR = round(odbc_result($rs_last_year_new, "$ANO_ANTERIOR"), 2);

                \App\Model\Distro::insert(array('id' => null, 'market' => $MARKET, 'name' => $CLIENT, 'type' => $TYPE, 'first_buy' => $FIRST, 'seller' => $SELLER, 'open_orders' => $OPEN, 'last_buy' => $LAST, 'anual_billing' => $YEAR, 'previous_anual_billing' => $LAST_YEAR, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));
            }
            odbc_close($conn);
        }
    }

    public function send_comsumables() {
        $user_id = $_REQUEST["ocomsumables-id"];
        $section = $_REQUEST["ocomsumables-section"];
        // set comsumable order
        $new_id = \App\Model\ComsumablesOrders::insertGetId(array('auth_id' => $user_id, 'section' => $section, 'validator_id' => 0, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));

        foreach ($_REQUEST["ocn"] as $select) {
            foreach ($select as $key => $comsumable) {
                if ($comsumable > 0) {
                    $id = $key;
                    $value = $comsumable;
                    $STOCK = 0;
                    // CHECK PHC
                    if (1 == 1) {
                        $server_port = '192.168.1.10';
                        $port = '1433';

                        $ser = $server_port; #the name of the SQL Server
                        $db = "cva"; #the name of the database
                        $user = "cva"; #a valid username
                        $pass = "cva295"; #a password for the username
                        $instance = "PHC";

                        $sql_consumable = \App\Model\Comsumables::where('id', $id)->first();

                        $conn = odbc_connect("Driver={".env('ODBC_DRIVER')."};Server=$server_port;Instance=$instance;Database=$db;", $user, $pass);
                        $sql = "SELECT * FROM st WHERE ref='$sql_consumable->ref'";
                        $rs = odbc_exec($conn, $sql);
                        while (odbc_fetch_row($rs)) {
                            $STOCK = trim(odbc_result($rs, "stock"));
                        }
                        //
                    }

                    \App\Model\ComsumablesOrdersItems::insert(array('comsumables_order_id' => $new_id, 'comsumables_id' => $id, 'value' => $value, 'stock' => $STOCK, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));
                }
            }
        }
        foreach ($_REQUEST["ocn-new"] as $select) {
            foreach ($select as $key => $comsumable) {
                if ($comsumable > 0) {
                    $id = $key;
                    $name = $_REQUEST["ocomsumable-new-$id"];
                    $value = $comsumable;
                    \App\Model\ComsumablesNew::insert(array('comsumables_order_id' => $new_id, 'name' => $name, 'value' => $value, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));
                }
            }
        }
        /*
        $data_email['autor'] = \App\User::where('id', $user_id)->first()->bigname;
        $data_email['section'] = $section;
        Mail::send('emails.comsumables', $data_email, function($message) {
            $message
                    ->from('software.prod@NEXT-proaudio.com')
                    ->to('brito.nextproaudio@gmail.com')
                    ->to('armazem@cva.pt')
                    ->subject('NXT :: Nova Requisição - Consumíveis');
        });
        */
        return [];
    }

    public function view_comsumables() {

        return $this->renderContent(
                        view('admin::view_comsumables')
        );
    }

    public function accept_comsumables($id, $value, $user_id) {
        if ($value == "valid") {
            $dump = \App\Model\ComsumablesOrders::where('id', $id)->update(['validator_id' => $user_id, 'updated_at' => Carbon::now()->toDateTimeString()]);
        }
        return [];
    }

    public function view_comsumables_history() {

        return $this->renderContent(
                        view('admin::view_comsumables_history')
        );
    }

    public function delete_palete_history($id) {
        $dump = \App\Model\Palete::where('id', $id)->update(['store_id' => 0, 'auth_id' => 0, 'status' => 0, 'updated_at' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function locksmith_delete_palete_history($id) {
        $dump = \App\Model\LocksmithPalete::where('id', $id)->update(['store_id' => 0, 'auth_id' => 0, 'status' => 0, 'updated_at' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function tech_delete_palete_history($id) {
        $dump = \App\Model\TechPalete::where('id', $id)->update(['store_id' => 0, 'auth_id' => 0, 'status' => 0, 'updated_at' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function palete_store_obs($id, $obs) {
        $dump = \App\Model\Palete::where('id', $id)->update(['store_obs' => $obs, 'store_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function palete_store_obs_delete($id) {
        $dump = \App\Model\Palete::where('id', $id)->update(['store_obs' => '', 'store_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function locksmith_palete_store_obs($id, $obs) {
        $dump = \App\Model\LocksmithPalete::where('id', $id)->update(['store_obs' => $obs, 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function tech_palete_store_obs($id, $obs) {
        $dump = \App\Model\TechPalete::where('id', $id)->update(['store_obs' => $obs, 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function locksmith_palete_store_obs_delete($id) {
        $dump = \App\Model\LocksmithPalete::where('id', $id)->update(['store_obs' => '', 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function tech_palete_store_obs_delete($id) {
        $dump = \App\Model\TechPalete::where('id', $id)->update(['store_obs' => '', 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function nxt_phc_palete_delete($id) {
        $dump = \App\Model\Palete::where('id', $id)->update(['phc_id' => 0]);
        return [];
    }

    public function nxt_phc_palete_yes($id, $obs) {
        $obs_explode = explode("##", $obs);
        if ($obs_explode[1]) {
            if ($obs_explode[0]) {
                $obs = "$obs_explode[0]/$obs_explode[1]";
            } else {
                $obs = $obs_explode[1];
            }
        } else {
            $obs = $obs_explode[0];
        }
        $dump = \App\Model\Palete::where('id', $id)->update(['phc_id' => $obs, 'store_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function locksmith_nxt_phc_palete_delete($id) {
        $dump = \App\Model\LocksmithPalete::where('id', $id)->update(['phc_id' => 0]);
        return [];
    }

    public function tech_nxt_phc_palete_delete($id) {
        $dump = \App\Model\TechPalete::where('id', $id)->update(['phc_id' => 0]);
        return [];
    }

    public function locksmith_nxt_phc_palete_yes($id, $obs) {
        $obs_explode = explode("##", $obs);
        if ($obs_explode[1]) {
            if ($obs_explode[0]) {
                $obs = "$obs_explode[0]/$obs_explode[1]";
            } else {
                $obs = $obs_explode[1];
            }
        } else {
            $obs = $obs_explode[0];
        }
        $dump = \App\Model\LocksmithPalete::where('id', $id)->update(['phc_id' => $obs, 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function tech_nxt_phc_palete_yes($id, $obs) {
        $obs_explode = explode("##", $obs);
        if ($obs_explode[1]) {
            if ($obs_explode[0]) {
                $obs = "$obs_explode[0]/$obs_explode[1]";
            } else {
                $obs = $obs_explode[1];
            }
        } else {
            $obs = $obs_explode[0];
        }
        $dump = \App\Model\TechPalete::where('id', $id)->update(['phc_id' => $obs, 'stored_date' => Carbon::now()->toDateTimeString()]);
        return [];
    }

    public function unlock_pp($pp, $id, $value) {
        $SQL = \App\Model\Phc_Semanas_Info::where('pp', $pp)->first();
        if ($value == "unlock") {
            $lock = 1;
        }
        if ($value == "lock") {
            $lock = 0;
        }
        if (count($SQL) > 0) {
            \App\Model\Phc_Semanas_Info::where('pp', $pp)->update(['locked' => $lock, 'auth_id' => $id, 'auth_id_date' => Carbon::now()->toDateTimeString()]);
        } else {
            \App\Model\Phc_Semanas_Info::insert(array('locked' => $lock, 'auth_id' => $id, 'pp' => $pp, 'auth_id_date' => \Carbon\Carbon::now()->toDateTimeString(), 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
        }
        return [];
    }

    public function schedule_pp($pp, $id, $day, $month, $year) {
        $SQL = \App\Model\Phc_Semanas_Info::where('pp', $pp)->first();
        $NEW_DATE = Carbon::createFromFormat('d-m-Y H:i:s', "$day-$month-$year 00:00:00")->toDateTimeString();
        if (count($SQL) > 0) {
            \App\Model\Phc_Semanas_Info::where('pp', $pp)->update(['new_date' => $NEW_DATE, 'auth_id' => $id, 'auth_id_date' => Carbon::now()->toDateTimeString()]);
        } else {
            \App\Model\Phc_Semanas_Info::insert(array('new_date' => $NEW_DATE, 'auth_id' => $id, 'pp' => $pp, 'auth_id_date' => \Carbon\Carbon::now()->toDateTimeString(), 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
        }
        return [];
    }

    public function obs_new($pp, $id, $obs) {
        $obs = base64_decode($obs);
        $SQL = \App\Model\Phc_Semanas_Info::where('pp', $pp)->first();        
        if (count($SQL) > 0) {
            \App\Model\Phc_Semanas_Info::where('pp', $pp)->update(['obs' => $obs, 'obs_user_id' => $id, 'obs_date' => \Carbon\Carbon::now()->toDateTimeString(),]);
        } else {
            \App\Model\Phc_Semanas_Info::insert(array('obs' => $obs, 'obs_user_id' => $id, 'obs_date' => \Carbon\Carbon::now()->toDateTimeString(), 'pp' => $pp, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
        }
        return [];
    }

    public function downloads($type, $pp) {
        if ($type == "packing") {
            $SQL = \App\Model\Phc_Semanas::where('pp', $pp)->first();
            return response()->download($SQL->u_packing_url);
        }
    }

    public function get_requests_updated() {
        // Artisan::call("infyom:scaffold", ['name' => $request['name'], '--fieldsFile' => 'public/Product.json']);
        \Artisan::call("schedule:run");
    }

    public function get_palete_update_state_admin($id) {
        $sql = Palete::where('id', $id)->first();
        return $sql;
    }

    public function palete_update_state_admin($section, $status, $id) {
        $section_date = $section . "_date";
        $section_id = $section . "_id";
        $section_date_now = \Carbon\Carbon::now()->toDateTimeString();
        DB::update("update paletes SET $section_id = '$status', $section_date = '$section_date_now' where id = '$id'");
        return [];
    }

    public function get_locksmith_palete_update_state_admin($id) {
        $sql = LocksmithPalete::where('id', $id)->first();
        return $sql;
    }

    public function locksmith_palete_update_state_admin($section, $status, $id) {
        if ($section == "store") {
            $section_date = $section . "d_date";
            $section_id = $section . "_id";
        } else if ($section == "auth") {
            $section_date = $section . "ed_date";
            $section_id = $section . "_id";
        } else {
            $section_date = $section . "_date";
            $section_id = $section . "_id";
        }
        $section_date_now = \Carbon\Carbon::now()->toDateTimeString();
        DB::update("update locksmith_palete SET $section_id = '$status', $section_date = '$section_date_now' where id = '$id'");
        return [];
    }

    public function levantamento_palete($object_id, $section, $user_id, $value) {
        $date_now = \Carbon\Carbon::now()->toDateTimeString();
        \App\Model\PaleteLevantamento::insert(array('id' => null, 'object_id' => $object_id, 'section' => $section, 'value' => $value, 'levantamento_user_id' => $user_id, 'levantamento_at' => \Carbon\Carbon::now()->toDateTimeString(), 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()));
        return [];
    }

}

<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception) {

//        if ($e instanceof Exception) {
//
//            $debugSetting = \Config::get('app.debug');
//            // email err if debug off (in prod env)
//            if (!$debugSetting) {
//                \Config::set('app.debug', true);
//                if (ExceptionHandler::isHttpException($e)) {
//                    $content = ExceptionHandler::toIlluminateResponse(ExceptionHandler::renderHttpException($e), $e);
//                } else {
//                    $content = ExceptionHandler::toIlluminateResponse(ExceptionHandler::convertExceptionToResponse($e), $e);
//                }
//                \Config::set('app.debug', $debugSetting);
//
//                $lc2 = (isset($content->original)) ? $content->original : $e->getMessage();
//
//                // add request info to err msg
//                $lc1 = '';
//                try {
//                    $request = request();
//                    $lc1 = "<div>" .
//                            "-- Request --<br>" .
//                            "<br>Method: " . $request->getMethod() .
//                            "<br>Uri: " . $request->getUri() .
//                            "<br>Ip: " . $request->getClientIp() .
//                            "<br>Referer: " . $request->server('HTTP_REFERER') .
//                            "<br>Is secure: " . $request->isSecure() .
//                            "<br>Is ajax: " . $request->ajax() .
//                            "<br>User agent: " . $request->server('HTTP_USER_AGENT') .
//                            "<br>Content:<br>" . nl2br(htmlentities($request->getContent())) .
//                            "</div>";
//                } catch (Exception $e2) {
//                    
//                }
//
//                if (strpos($lc2, '</body>') !== false) {
//                    $lc2 = str_replace('</body>', $lc1 . '</body>', $lc2);
//                } else {
//                    $lc2.= $lc1;
//                }
//                $la2['content'] = $lc2;      //put into array for blade
//
//                $ln1 = Mail::send('emails.errors.debug', $la2, function($m) {
//                            $m->from('software.prod@NEXT-proaudio.com')
//                                    ->to('brito.nextproaudio@gmail.com')
//                                    ->subject('NXT :: Debug');
//                        });
//            }
//        }
        // SENTRY
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }
        // SENTRY
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) {
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception) {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }

}

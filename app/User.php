<?php

namespace App;

use App\Model\Contact;
use Illuminate\Foundation\Auth\User as Authenticatable;
use KodiComponents\Support\Upload;
use Illuminate\Http\UploadedFile;
use Validator;
use App\Model\Request;
use Carbon\Carbon;

class User extends Authenticatable {

    use HasRoles,
        Upload;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'bigname',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'avatar' => 'image',
    ];
    protected $appends = [
        'nr_semanal_anterior',
        'nr_semanal',
        'nr_mensal_anterior',
        'nr_mensal',
        'nr_total'
    ];

    public function getNrSemanalAttribute() {

        $monday = strtotime("last monday");
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;
        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");
        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);

        // cnc
        $CNC = 0;
        $total = Model\RequestUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CNC = $CNC + $valor;
        }
        // preparação
        $PREP = 0;
        $total = Model\PrepUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PREP = $PREP + $valor;
        }
        // carpintaria
        $CARPINTARIA = 0;
        $total = Model\WoodUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA = $CARPINTARIA + $valor;
        }

        // acabamento da carpintaria
        $CARPINTARIA_ACABAMENTO = 0;
        $total = Model\WoodFinishingUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_ACABAMENTO = $CARPINTARIA_ACABAMENTO + $valor;
        }

        // acabamento de preto
        $CARPINTARIA_PRETO = 0;
        $total = Model\BLACKFINISHING_History::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_PRETO = $CARPINTARIA_PRETO + $valor;
        }

        // pintura
        $PINTURA = 0;
        $total = Model\PaintUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PINTURA = $PINTURA + $valor;
        }

        // montagem
        $MONTAGEM = 0;
        $total = Model\MountUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $MONTAGEM = $MONTAGEM + $valor;
        }

        // serralharia
        $SERRALHARIA = 0;
        $total = Model\LocksmithRequestsUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();

        foreach ($total as $value) {
            if ($value->locksmith_requests_id) {
                $product_id = Model\LocksmithRequests::where('id', $value->locksmith_requests_id)->first()->locksmith_id;
                $pontos = Model\Locksmith::where('id', $product_id)->first()->points;
                $valor = $value->value * $pontos;
            } else {
                $valor = 0;
            }
            $SERRALHARIA = $SERRALHARIA + $valor;
        }

        // redes
        $REDES = 0;
        $total = Model\WIRES_History::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $REDES = $REDES + $valor;
        }
        $TOTAL = $CNC + $PREP + $CARPINTARIA + $CARPINTARIA_ACABAMENTO + $CARPINTARIA_PRETO + $PINTURA + $MONTAGEM + $SERRALHARIA + $REDES;
        return $TOTAL;
    }

    public function getNrSemanalAnteriorAttribute() {

        $monday = strtotime('last monday last week');
        $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;
        $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");
        $this_week_sd = date("Y-m-d", $monday);
        $this_week_ed = date("Y-m-d", $sunday);

        // cnc
        $CNC = 0;
        $total = Model\RequestUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CNC = $CNC + $valor;
        }
        // preparação
        $PREP = 0;
        $total = Model\PrepUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PREP = $PREP + $valor;
        }
        // carpintaria
        $CARPINTARIA = 0;
        $total = Model\WoodUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA = $CARPINTARIA + $valor;
        }

        // acabamento da carpintaria
        $CARPINTARIA_ACABAMENTO = 0;
        $total = Model\WoodFinishingUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_ACABAMENTO = $CARPINTARIA_ACABAMENTO + $valor;
        }

        // acabamento de preto
        $CARPINTARIA_PRETO = 0;
        $total = Model\BLACKFINISHING_History::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_PRETO = $CARPINTARIA_PRETO + $valor;
        }

        // pintura
        $PINTURA = 0;
        $total = Model\PaintUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PINTURA = $PINTURA + $valor;
        }

        // montagem
        $MONTAGEM = 0;
        $total = Model\MountUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $MONTAGEM = $MONTAGEM + $valor;
        }

        // serralharia
        $SERRALHARIA = 0;
        $total = Model\LocksmithRequestsUser::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            if ($value->locksmith_requests_id) {
                $product_id = Model\LocksmithRequests::where('id', $value->locksmith_requests_id)->first()->locksmith_id;
                $pontos = Model\Locksmith::where('id', $product_id)->first()->points;
                $valor = $value->value * $pontos;
            } else {
                $valor = 0;
            }
            $SERRALHARIA = $SERRALHARIA + $valor;
        }

        // redes
        $REDES = 0;
        $total = Model\WIRES_History::where('user_id', $this->attributes['id'])->whereBetween('created_at', array($this_week_sd, $this_week_ed))->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $REDES = $REDES + $valor;
        }

        $TOTAL = $CNC + $PREP + $CARPINTARIA + $CARPINTARIA_ACABAMENTO + $CARPINTARIA_PRETO + $PINTURA + $MONTAGEM + $SERRALHARIA + $REDES;
        return $TOTAL;
    }

    public function getNrMensalAttribute() {
        $ano = date('Y');
        $mes = date('m');
        $like = "$ano-$mes-%";

        // cnc
        $CNC = 0;
        $total = Model\RequestUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CNC = $CNC + $valor;
        }

        // preparação
        $PREP = 0;
        $total = Model\PrepUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PREP = $PREP + $valor;
        }

        // carpintaria
        $CARPINTARIA = 0;
        $total = Model\WoodUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA = $CARPINTARIA + $valor;
        }

        // acabamento da carpintaria
        $CARPINTARIA_ACABAMENTO = 0;
        $total = Model\WoodFinishingUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_ACABAMENTO = $CARPINTARIA_ACABAMENTO + $valor;
        }

        // acabamento de preto
        $CARPINTARIA_PRETO = 0;
        $total = Model\BLACKFINISHING_History::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_PRETO = $CARPINTARIA_PRETO + $valor;
        }

        // pintura
        $PINTURA = 0;
        $total = Model\PaintUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PINTURA = $PINTURA + $valor;
        }

        // montagem
        $MONTAGEM = 0;
        $total = Model\MountUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $MONTAGEM = $MONTAGEM + $valor;
        }

        // serralharia
        $SERRALHARIA = 0;
        $total = Model\LocksmithRequestsUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            if ($value->locksmith_requests_id) {
                $product_id = Model\LocksmithRequests::where('id', $value->locksmith_requests_id)->first()->locksmith_id;
                $pontos = Model\Locksmith::where('id', $product_id)->first()->points;
                $valor = $value->value * $pontos;
            } else {
                $valor = 0;
            }
            $SERRALHARIA = $SERRALHARIA + $valor;
        }

        // redes
        $REDES = 0;
        $total = Model\WIRES_History::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $REDES = $REDES + $valor;
        }

        $TOTAL = $CNC + $PREP + $CARPINTARIA + $CARPINTARIA_ACABAMENTO + $CARPINTARIA_PRETO + $PINTURA + $MONTAGEM + $SERRALHARIA + $REDES;
        return $TOTAL;
    }

    public function getNrMensalAnteriorAttribute() {
        $ano = date('Y');
        $mes = date('m') - 1;
        if ($mes < 10) {
            $mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
        }
        $like = "$ano-$mes-%";

        // cnc
        $CNC = 0;
        $total = Model\RequestUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CNC = $CNC + $valor;
        }

        // prep
        $PREP = 0;
        $total = Model\PrepUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PREP = $PREP + $valor;
        }

        // carpintaria
        $CARPINTARIA = 0;
        $total = Model\WoodUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA = $CARPINTARIA + $valor;
        }

        // acabamento da carpintaria
        $CARPINTARIA_ACABAMENTO = 0;
        $total = Model\WoodFinishingUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_ACABAMENTO = $CARPINTARIA_ACABAMENTO + $valor;
        }

        // acabamento de preto
        $CARPINTARIA_PRETO = 0;
        $total = Model\BLACKFINISHING_History::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $CARPINTARIA_PRETO = $CARPINTARIA_PRETO + $valor;
        }

        // pintura
        $PINTURA = 0;
        $total = Model\PaintUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $PINTURA = $PINTURA + $valor;
        }

        // montagem
        $MONTAGEM = 0;
        $total = Model\MountUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $product_id = Model\Request::where('id', $value->request_id)->first()->product_id;
            $pontos = Model\Product::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            $MONTAGEM = $MONTAGEM + $valor;
        }

        // serralharia
        $SERRALHARIA = 0;
        $total = Model\LocksmithRequestsUser::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            if ($value->locksmith_requests_id) {
            $product_id = Model\LocksmithRequests::where('id', $value->locksmith_requests_id)->first()->locksmith_id;
            $pontos = Model\Locksmith::where('id', $product_id)->first()->points;
            $valor = $value->value * $pontos;
            }
            else {
                $valor = 0;
            }
            $SERRALHARIA = $SERRALHARIA + $valor;
        }

        // redes
        $REDES = 0;
        $total = Model\WIRES_History::where('user_id', $this->attributes['id'])->where('created_at', 'like', $like)->get();
        foreach ($total as $value) {
            $pontos = Model\Product::where('id', $value->product_id)->first()->points;
            $valor = $value->value * $pontos;
            $REDES = $REDES + $valor;
        }

        $TOTAL = $CNC + $PREP + $CARPINTARIA + $CARPINTARIA_ACABAMENTO + $CARPINTARIA_PRETO + $PINTURA + $MONTAGEM + $SERRALHARIA + $REDES;
        return $TOTAL;
    }

//    static function boot() {
//        Validator::extend('check_password', function($attribute, $value, $parameters, $validator) {
//            dd(1);
//            if (!isset($_POST['password'])) {
//                return $validator;
//            } else {
//
//                $CHECKING = User::get();
//                foreach ($CHECKING as $value) {
//                    $WHAT = \Hash::check($_REQUEST['password'], $value->password);
//                    if ($WHAT == 1) {
//                        $validator->errors()->all();
//                    }
//                }
//                return;
//            }
//        });
//    }

    /**
     * @return array
     */
    public function getUploadSettings() {
        return [
            'avatar' => [
                'fit' => [300, 300, function ($constraint) {
                        $constraint->upsize();
                        $constraint->aspectRatio();
                    }],
            ],
        ];
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    protected function getUploadFilename(UploadedFile $file) {
        return md5($this->id) . '.' . $file->getClientOriginalExtension();
    }

    /**
     * @return bool
     */
    public function isSuperAdmin() {
        return $this->hasRole('admin');
    }

    /**
     * @return bool
     */
    public function isManager() {
        return $this->hasRole('manager');
    }

    public function isCNC() {
        return $this->hasRole('cnc');
    }

    public function isWood() {
        return $this->hasRole('carpintaria');
    }

    public function isPaint() {
        return $this->hasRole('pintura');
    }

    public function isMount() {
        return $this->hasRole('montagem');
    }

    public function isTech() {
        return $this->hasRole('tecnica');
    }

    public function isRH() {
        return $this->hasRole('rh');
    }

    public function isStock() {
        return $this->hasRole('stock');
    }

    public function isArmazem() {
        return $this->hasRole('armazem');
    }

    public function isSerralharia() {
        return $this->hasRole('serralharia');
    }

    public function isSerigrafia() {
        return $this->hasRole('serigrafia');
    }

    public function isFaltas() {
        return $this->hasRole('faltas');
    }    

    /**
     * @param string $password
     */
//    public function setPasswordAttribute($password) {
//        $this->attributes['password'] = $password;
//    }

    /**
     * @return string
     */
    public function getAvatarUrlOrBlankAttribute() {
        if (empty($url = $this->avatar_url)) {
            return asset('images/blank.png');
        }

        return $url;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts() {
        return $this->belongsToMany(Contact::class, 'contact_id');
    }

    public function companies() {
        return $this->hasOne(\App\Model\Companies::class, 'id', 'company_id');
    }

}

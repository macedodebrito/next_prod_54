<?php


if (!function_exists('ensureUTF8ArrayDecode')) {
    function ensureUTF8ArrayDecode($array) {
        array_walk_recursive($array, function(&$item) {
            if (is_string($item) && mb_detect_encoding($item, 'UTF-8', true) === false) {
                // Converte para UTF-8 apenas se não estiver nesse formato
                $item = utf8_decode($item); // Ou use mb_convert_encoding se necessário
            }
        });
        return $array;
    }
}

if (!function_exists('array2SQL')) {
    function array2SQL($table, $array)
    {       
        $output['table'] = $table;
        $output['arrays'] = $array;
        return $output;
    }
}

if (!function_exists('colorBadge')) {
    function colorBadge($bg, $font, $text)
    {
        return '<span class="badge rounded-pill" style="color:'.$font.';background-color:'.$bg.'">'.$text.'</span>';
    }
}

if (!function_exists('colorIcon')) {
    function colorIcon($info, $compare)
    {
                
        $colors_icons = array(
            'colors' => array(
                'green' => "success",
                'yellow' => "warning",
                'red' => "danger",
            ),
            'icons' => array(
                'done' => "bi-check-circle-fill",
                'notdone' => "bi-exclamation-circle-fill",
            )
        );

        $colorIcon = [];

        $actual = $info * 100 / $compare;
        $diff = 100 - $actual;

        if ($actual > 100) {
            $colorIcon['color'] = $colors_icons['colors']['green'];
            $colorIcon['icon'] = $colors_icons['icons']['done'];
        } else if ($diff > 0 && $diff < 10) {
            $colorIcon['color'] = $colors_icons['colors']['yellow'];
            $colorIcon['icon'] = $colors_icons['icons']['done'];
        } else {
            $colorIcon['color'] = $colors_icons['colors']['red'];
            $colorIcon['icon'] = $colors_icons['icons']['notdone'];
        }  

        return $colorIcon;
    }
}

if (!function_exists('dailyUntilSuper')) {
    function dailyUntilSuper($route)    
    {        
        $table = array(
            'v2_cnc' => array(
                'table' => "request_users",
                'week' => "limits_cnc",
                'month' => "m_limits_cnc",
                'super' => "s_limits_cnc",
                'where_clause' => "AND request_users.published >= 0 AND request_users.prep >= 0"
            )
        );

        $sqltable       = $table[$route]['table'];        
        $points_week    = $table[$route]['week'];
        $points_month   = $table[$route]['month'];
        $points_super   = $table[$route]['super'];
        $where_clause   = $table[$route]['where_clause'];
    
        $resultSQL = \DB::select("
            SELECT
            COALESCE(SUM(case when DATE($sqltable.created_at)=CURDATE() then $sqltable.value * products.points else 0 end),0) AS daily_points, 
            COALESCE(SUM(case when YEARWEEK(DATE(request_users.created_at), 1) = YEARWEEK(CURDATE(), 1) then $sqltable.value * products.points else 0 end),0) AS weekly_points,
            COALESCE(SUM($sqltable.value * products.points),0) as monthly_points,
            ROUND(config.$points_week/5, 0) AS l_day,
            config.$points_week AS l_week,
            config.$points_month AS l_month,
            config.$points_super AS l_super
            FROM $sqltable
            JOIN requests ON $sqltable.request_id = requests.id
            JOIN products ON products.id = requests.product_id
            JOIN config ON config.id = 1
            WHERE YEAR(DATE($sqltable.created_at)) = YEAR(CURRENT_DATE()) AND MONTH(DATE($sqltable.created_at)) = MONTH(CURRENT_DATE()) 
            $where_clause
        ");        
        $colorIcon['daily'] = colorIcon(round($resultSQL[0]->daily_points), $resultSQL[0]->l_week/5);
        $colorIcon['week'] = colorIcon(round($resultSQL[0]->weekly_points), $resultSQL[0]->l_week);
        $colorIcon['month'] = colorIcon(round($resultSQL[0]->monthly_points), $resultSQL[0]->l_month);
        $colorIcon['super'] = colorIcon(round($resultSQL[0]->monthly_points), $resultSQL[0]->l_super);
        $colorIcon['SQL'] = $resultSQL;

        return $colorIcon;
    }
}

if (!function_exists('connectPHC')) {
    //function connectPHC($info, $function, $db, $param = null)
    function connectPHC($info, $function, $param = null)
    {
        // 4 - cvagroup
        // 5 - cva
        //$conn = odbc_connect("Driver={".env('ODBC_DRIVER')."};Server=".env('DB_HOST_'.$db).";Instance=PHC;Database=".env('DB_DATABASE_'.$db).";Client_CSet=UTF-8;String Types=Unicode;UID=".env('DB_USERNAME_'.$db)." ;PWD=".env('DB_PASSWORD_'.$db), env('DB_USERNAME_'.$db), env('DB_PASSWORD_'.$db));
        $function($info, $param);
        //$function($conn, $info, $param);
        //odbc_close($conn);
    }
}

if (!function_exists('mount2PHC')) {
    //function mount2PHC($conn, $info, $param = null)
    function mount2PHC($info, $param = null)
    {       
        // Cada Output
        if ($param == "manual"){
            $serials = $info['serials'];
            $group = array();
            foreach ( $info['serials'] as $key => $value ) {
                $group[$info['request_id']][] = $value;
            }
            $id_pp = "id";
        }

        else {
            $serials = $info['serial'];             
            $group = array();
            foreach ( $info['serial'] as $key => $value ) {
                $group[$info['request_id'][$key]][] = $value;
            }
            $id_pp = "pp";
        }

        $now = \Carbon\Carbon::now();
        $dataobra = $now->format('Y-m-d H:i:s.v');
        $usrdata = $now->format('H:i:s');
        $boano = $now->format('Y');
        $bostamp = bin2hex(random_bytes(10));

        $insert_bo = [
            'bostamp'       => $bostamp,
            'nmdos'         => "Produção a Receber",
            'ndos'          => 60,
            'fechada'       => 0,
            'obrano'        => null,
            'dataobra'      => $dataobra,
            'dataopen'      => $dataobra,
            'no'            => 5,
            'nome'          => "CVA ELECTRÓNICA, LDA",
            'boano'         => $boano,
            'datafinal'     => "1900-01-01 00:00:00.000",
            'etotaldeb'     => 0,
            'ousrinis'      => "nxt",
            'usrinis'       => "nxt",
            'sqtt14'        => count($serials),
            'ocupacao'      => 4,
            'lifref'        => 1,
            'ousrdata'      => $dataobra,
            'ousrhora'      => $usrdata,
            'usrdata'       => $dataobra,
            'usrhora'       => $usrdata,
            'memissao'      => "EURO",
            'cron'          => 0,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];

        $obrano = \App\Model\V2\PHCMountOutputBO::insertGetId($insert_bo);
        
        $insert_bi = [];

        // Cada PP
        $posic = 0;
        foreach($group as $key => $value) {
            $posic++;
            $sqlInfo = \App\Model\Request::select('requests.id', 'requests.pp', 'requests.product_id', 'status_requests.mini_code', 'status_requests.code', 'status_requests.name', 'status_requests.custom_color', 'status_requests.texteis', 'products_custom_nxt.name_cor')
            ->join('status_requests', 'status_requests.product_id', 'requests.product_id')
            ->join('products_custom_nxt', 'requests.custom_id', 'products_custom_nxt.id')
            ->where("requests.$id_pp", '=', $key)
            ->first();
            
            if ($sqlInfo->custom_id == 1) {
                $cor = "";
                $tam = "";
            } 
            else {
                $cor = $sqlInfo->name_cor;
                $tam = "Unique";
            }            
            
            foreach ($value as $serial_key => $serial_update) {
                $value[$serial_key] = "$sqlInfo->code$sqlInfo->pp$serial_update";
            }            
            $request_serials = implode(', ', $value);
            $bistamp = bin2hex(random_bytes(10));

            $draw = [
                'bistamp'       => $bistamp,
                'cron'          => 0,
                'bostamp'       => $bostamp,
                'nmdos'         => "Produção a Receber",
                'ndos'          => 60,
                'obrano'        => $obrano,
                'dataobra'      => $dataobra,
                'dataopen'      => $dataobra,
                'rdata'         => $dataobra,
                'ref'           => "$sqlInfo->mini_code$sqlInfo->code",
                'design'        => "$sqlInfo->name",
                'qtt'           => count($value),
                'stipo'         => 4,
                'lobs'          => "",
                'lobs2'         => "",
                'edebito'       => 0,
                'ettdeb'        => 0,
                'IVA'           => 23,
                'TABIVA'        => 2,
                'unidade'       => "UN",
                'armazem'       => 1,
                'texteis'       => $sqlInfo->texteis,
                'cor'           => $cor,
                'tam'           => $tam,
                'ousrinis'      => "nxt",
                'usrinis'       => "nxt",
                'no'            => 5,
                'resrec'        => 1,
                'tabfor'        => 1,
                'posic'         => $posic,
                'lrecno'        => "",
                'lordem'        => $posic*10000,
                'lifref'        => 1,
                'ousrdata'      => $dataobra,
                'ousrhora'      => $usrdata,
                'usrdata'       => $dataobra,
                'usrhora'       => $usrdata,
                'nome'          => "CVA ELECTRÓNICA, LDA",
                'series'        => $request_serials,
                'created_at' =>  \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),    
            ];
            $insert_bi[] = $draw;
        }            
        
        \App\Model\V2\PHCMountOutputBI::insert($insert_bi);
             
        $bo = $insert_bo;        
        $bo['obrano'] = $obrano;
        $bo = array_splice($bo, 0, -3); // remover cron + timestamps
        $bo3stamp['bo3stamp'] = $bo['bostamp'];
        $bo2stamp['bo2stamp'] = $bo['bostamp'];

        $sendMount2PHC = [];
        $sendMount2PHC[] = array2SQL("cvagroup.dbo.bo3", $bo3stamp);
        $sendMount2PHC[] = array2SQL("cvagroup.dbo.bo2", $bo2stamp);
        $sendMount2PHC[] = array2SQL("cvagroup.dbo.bo", $bo);

        foreach ($insert_bi as $bi) {
            $bi_ = $bi;
            $bi_ = array_splice($bi_, 0, -2);
            unset($bi_['cron']);
            $bi2['bostamp'] = $bi['bostamp'];
            $bi2['bi2stamp'] = $bi['bistamp'];

            $sendMount2PHC[] = array2SQL("cvagroup.dbo.bi", $bi_);
            $sendMount2PHC[] = array2SQL("cvagroup.dbo.bi2", $bi2);                        
        }               
        $payload = json_encode($sendMount2PHC, JSON_UNESCAPED_UNICODE);
        dispatch((new \App\Jobs\V2\SendMountOutput2PHC($payload)));
    }
}
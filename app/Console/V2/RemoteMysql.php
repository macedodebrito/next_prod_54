<?php
 
namespace App\Console\V2;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
 
class RemoteMysql extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remotemysql:dump {--options=}';
 
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remotely get/install Dump to local DB';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ds = DIRECTORY_SEPARATOR;
        $path = database_path() . $ds . 'backups';

        //$this->line("Some text");
        //$this->info("Hey, watch this !");
        //$this->comment("Just a comment passing by");
        //$this->question("Why did you do that?");
        //$this->error("Ops, that should not happen.");

        $options = $this->choice('What do you wanna do?', ['get', 'install']);        

        if ($this->confirm('Is ['.$options.'] correct? Do you wish to continue? [y|N]')) {
        }        

        // OPTIONS
        if ($options === "get") {
            $password = $this->secret('What is the DATABASE password?');
            $file = date('Y-m-d_H:i:s') . '_mysqldump.sql';
            $command = sprintf('mysqldump -h '.env('DB_HOST_4').' -u cvanext -p\'%s\' next_prod_54 > %s', $password,  $path . $ds . $file);
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }    
            $this->comment("OK. Lets proceed with the DUMP.");
            exec($command);
            $this->info("Dumped: $file");
        }
        else {                        
            $files = File::files($path);
            $fileOptions = $this->choice('What file to install?', $files);
            $command = sprintf('mysql -h '.env('DB_HOST').' -u '.env('DB_USERNAME').' --password='.env('DB_PASSWORD').' '.env('DB_DATABASE').' < %s', $fileOptions);
            $this->comment("OK. Lets proceed with the IMPORT.");

            exec($command, $output, $error);
            $this->error($error);
            $this->info("Imported: $fileOptions");            
        }
    }
}
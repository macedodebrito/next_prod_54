<?php
 
namespace App\Console\V2;
 
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class TestMSSQL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pp';
 
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking Removed PPs';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

        
    public function handle()
    {               
        $missingRequestIds = \DB::table('request_users as ru')
            ->whereNotExists(function ($query) {
                $query->select(\DB::raw(1))
                    ->from('requests as r')
                    ->whereRaw('r.id = ru.request_id');
            })
            ->pluck('ru.request_id');
        dd($missingRequestIds);
    }
}
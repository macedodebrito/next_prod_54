<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB as DB;
use Mail;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        V2\RemoteMysql::class,
        V2\TestMSSQL::class,
    ];

    public function processJobsIfPending()
    {
        // Contar o número de jobs pendentes
        $pendingJobs = DB::table('jobs')->whereNull('reserved_at')->count();
    
        if ($pendingJobs > 0) {
            // Jobs pendentes encontrados, iniciar o queue:work
            \Artisan::call('queue:work', [
                '--stop-when-empty' => true, // Para o worker após processar todos os jobs
            ]);
        } else {
            // Não há jobs pendentes
            print_r("--- Nenhum job pendente.\n\n");
        }
    }

    protected function check_monthly_limit_reset($debug) {
        $today_day = date('j');
        print_r("A verificar dia do mês:" . $today_day . "...\n");
        if ($today_day != 28) {
            print_r("nao é dia 28. Updating CONFIG to: " . $today_day . "\n");
            DB::table('config')->where('id', 1)->update(['limits_monthly' => $today_day]);
        }
        $today_month = date('F');

        $last_date = DB::table('config')->where('id', 1)->first()->limits_monthly;
        $setup_monthly = DB::table('config')->where('id', 1)->first()->setup_monthly;

        if (($today_day == 28) && ($last_date != 0)) {
            print_r("É dia 28. Reseting CONFIG ($last_date / $setup_monthly)...\n");
            DB::table('config')->where('id', 1)
                    ->update(
                            [
                                'm_limits_cnc' => $setup_monthly,
                                'm_limits_prep' => $setup_monthly,
                                'm_limits_wood' => $setup_monthly,
                                'm_limits_woodfinishing' => $setup_monthly,
                                'm_limits_woodfinishing_black' => $setup_monthly,
                                'm_limits_paint' => $setup_monthly,
                                'm_limits_mount' => $setup_monthly,
                                'm_limits_wires' => $setup_monthly,
                                'm_limits_tech' => $setup_monthly,
                                'm_limits_locksmith' => $setup_monthly,
                                'limits_monthly' => 0,
                            ]
            );
            print_r("Reseting Monthly Goals...\n");
            $data_email['MONTH'] = $today_month;
            $data_email['POINTS'] = $setup_monthly;
            Mail::send('emails.monthly_goals', $data_email, function($message) {
                $message
                        ->from('software.prod@NEXT-proaudio.com')
                        ->to('brito.nextproaudio@gmail.com')
//                            ->to('andrecorreia@cva.pt')
//                            ->to('tiago@cva.pt')
//                            ->to('correia@cva.pt')
                        ->subject('NXT :: Reseting Monthly Goals');
            });
            //print_r("sending email...\n\n");
        }
    }

    protected function check_week_limit_reset($debug) {
        $today_date = date('d-m-Y');
        $today_week = date('W');

        print_r("A verificar Semana do Ano:" . $today_week . "...\n");

        $last_week = DB::table('config')->where('id', 1)->first()->limits_week;
        $setup_week = DB::table('config')->where('id', 1)->first()->setup_week;

        if ($today_week != $last_week) {
            DB::table('config')->where('id', 1)
                    ->update(
                            [
                                'limits_cnc' => $setup_week,
                                'limits_prep' => $setup_week,
                                'limits_wood' => $setup_week,
                                'limits_woodfinishing' => $setup_week,
                                'limits_woodfinishing_black' => $setup_week,
                                'limits_paint' => $setup_week,
                                'limits_mount' => $setup_week,
                                'limits_wires' => $setup_week,
                                'limits_tech' => $setup_week,
                                'limits_locksmith' => $setup_week,
                                'limits_week' => $today_week,
                            ]
            );
            print_r("Reseting Weekly Goals...\n");
            $data_email['WEEK'] = $today_week;
            $data_email['POINTS'] = $setup_week;
            Mail::send('emails.weekly_goals', $data_email, function($message) {
                $message
                        ->from('software.prod@NEXT-proaudio.com')
                        ->to('brito.nextproaudio@gmail.com')
//                            ->to('andrecorreia@cva.pt')
//                            ->to('tiago@cva.pt')
//                            ->to('correia@cva.pt')
                        ->subject('NXT :: Reseting Weekly Goals');
            });
            //print_r("sending email...\n\n");
        }
    }

    protected function find_new_ip($send) {
        //print_r("Checking IP...");
        $realIP = file_get_contents("http://ipecho.net/plain");
        $ip = DB::table('ips')->where('id', 1)->first()->name;

        if ($ip != $realIP) {
            DB::table('ips')->where('id', 1)->update(['name' => $realIP]);
            //print_r("$realIP VS $ip\n");
            if ($send > 0) {
                $data_email['IP'] = $realIP;
                Mail::send('emails.ip', $data_email, function($message) {
                    $message
                            ->from('software.prod@NEXT-proaudio.com')
                            ->to('brito.nextproaudio@gmail.com')
                            ->to('andrecorreia@cva.pt')
                            ->to('tiago@cva.pt')
                            ->to('correia@cva.pt')
                            ->subject('NXT :: Novo IP');
                });
                //print_r("sending email...\n\n");
            }
        }
    }

    protected function collect_NXT_orders($debug) {
        // PRODUTOS
        if ($debug > 0)
            //print_r("truncating TABLE: status_requests...\n\n");
        DB::table('status_requests')->truncate();

        if ($debug > 0)
            //print_r("FETCHING GROUP FROM PP...\n");

        //$sql_requests = \App\Model\Request::orderBy('start_at', 'asc')->groupBy('product_id')->get();
        $sql_requests = \App\Model\Request::orderBy('start_at', 'asc')
        ->join('products_custom_nxt', 'requests.custom_id', 'products_custom_nxt.id')
        ->groupBy('product_id')
        ->groupBy('custom_id')
        ->get();
        foreach ($sql_requests as $unique) {

            $total_week = \App\Model\RequestWeek::where('product_id', $unique['product_id'])->where('custom_id', $unique['custom_id'])->orderBy('end_at', 'asc')->sum('montagem');
            if (!count($total_week)) {
                $total_week = 0;
            }

            $total_extra = \App\Model\RequestWeek::where('product_id', $unique['product_id'])->where('custom_id', $unique['custom_id'])->orderBy('end_at', 'asc')->where('client', "=", "EXTRA_STOCK")->sum("montagem");
            if (!count($total_extra)) {
                $total_extra = 0;
            }

            $total_req2 = \App\Model\Request::where('product_id', $unique['product_id'])->where('custom_id', $unique['custom_id'])->get();
            $total_inc = 0;
            $total_inc2 = 0;
            foreach ($total_req2 as $go) {
                $dump = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', -1)->sum('value');
                $total_inc = $total_inc + $dump;
            }
            $CONTA = $total_week - $total_inc;

            $get_product = \App\Model\Product::where('id', $unique['product_id'])->first();
            $get_name = $get_product->name;
            $get_code = $get_product->code;
            $get_mini_code = $get_product->mini_code;
            $get_color_code = $unique['color_code'];
            $get_custom_code = "$get_mini_code$get_code$get_color_code";
            if ($debug > 0) {
                
                //print_r("$get_name TOTAL: $total_week EXTRA_STOCK: $total_extra | custom_color: $unique_custom_id\n");
                
            }
            $unique_custom_id = $unique['custom_id'];
            DB::table('status_requests')->insert(array('id' => null, 'product_id' => $unique['product_id'], 'custom_color' => $unique['custom_id'], 'name' => $get_name, 'value' => $CONTA, 'extra' => 0, 'code' => $get_code, 'mini_code' => $get_mini_code, 'custom_code' => $get_custom_code));
        }
        
        // SERRALHARIA
        if ($debug > 0)
            //print_r("truncating TABLE: locksmith_status_requests...\n\n");
        DB::table('locksmith_status_requests')->truncate();

        if ($debug > 0)
            //print_r("FETCHING GROUP FROM PP...\n");

        $sql_requests = \App\Model\LocksmithRequests::orderBy('start_at', 'asc')->groupBy('locksmith_id')->get();
        foreach ($sql_requests as $unique) {

            $total_week = \App\Model\LocksmithWeek::where('locksmith_id', $unique['locksmith_id'])->orderBy('end_at', 'asc')->sum('quantity');
            if (!count($total_week)) {
                $total_week = 0;
            }

            $total_extra = \App\Model\LocksmithWeek::where('locksmith_id', $unique['locksmith_id'])->orderBy('end_at', 'asc')->where('client', "=", "EXTRA_STOCK")->sum("quantity");
            if (!count($total_extra)) {
                $total_extra = 0;
            }

            $total_req2 = \App\Model\LocksmithRequests::where('locksmith_id', $unique['locksmith_id'])->get();
            $total_inc = 0;
            $total_inc2 = 0;
            foreach ($total_req2 as $go) {
                $dump = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->sum('value');
                $total_inc = $total_inc + $dump;
            }
            $CONTA = $total_week - $total_inc;

            $get_locksmith = \App\Model\Locksmith::where('id', $unique['locksmith_id'])->first();
            $get_name = $get_locksmith->name;
            $get_code = $get_locksmith->code;
            $get_mini_code = $get_locksmith->mini_code;

            if ($debug > 0)
                //print_r("$get_name TOTAL: $total_week EXTRA_STOCK: $total_extra\n");
            DB::table('locksmith_status_requests')->insert(array('id' => null, 'locksmith_id' => $unique['locksmith_id'], 'name' => $get_name, 'value' => $CONTA, 'extra' => 0, 'code' => $get_code, 'mini_code' => $get_mini_code));
        }
        
        // TÉCNICA
        if ($debug > 0)
            //print_r("truncating TABLE: tech_status_requests...\n\n");
        DB::table('tech_status_requests')->truncate();

        if ($debug > 0)
            //print_r("FETCHING GROUP FROM PP...\n");

        $sql_requests = \App\Model\TechRequests::orderBy('start_at', 'asc')->groupBy('tech_id')->get();
        foreach ($sql_requests as $unique) {

            $total_week = \App\Model\TechWeek::where('tech_id', $unique['tech_id'])->orderBy('end_at', 'asc')->sum('quantity');
            if (!count($total_week)) {
                $total_week = 0;
            }

            $total_extra = \App\Model\TechWeek::where('tech_id', $unique['tech_id'])->orderBy('end_at', 'asc')->where('client', "=", "EXTRA_STOCK")->sum("quantity");
            if (!count($total_extra)) {
                $total_extra = 0;
            }

            $total_req2 = \App\Model\TechRequests::where('tech_id', $unique['tech_id'])->get();
            $total_inc = 0;
            $total_inc2 = 0;
            foreach ($total_req2 as $go) {
                $dump = \App\Model\TechRequestsUser::where('tech_requests_id', $go->id)->sum('value');
                $total_inc = $total_inc + $dump;
            }
            $CONTA = $total_week - $total_inc;

            $get_tech = \App\Model\Tech::where('id', $unique['tech_id'])->first();
            $get_name = $get_tech->name;
            $get_code = $get_tech->code;
            $get_mini_code = $get_tech->mini_code;

            if ($debug > 0)
                //print_r("$get_name TOTAL: $total_week EXTRA_STOCK: $total_extra\n");
            DB::table('tech_status_requests')->insert(array('id' => null, 'tech_id' => $unique['tech_id'], 'name' => $get_name, 'value' => $CONTA, 'extra' => 0, 'code' => $get_code, 'mini_code' => $get_mini_code));
        }        
        
    }

    protected function collect_new_products_info($debug) {
        // PRODUTOS
        if ($debug > 0) {
            //print_r("GETTING INFO FROM PRODUCTS (sem PP)...\n\n");
        }

        $sql_product = DB::table('products')
        ->select(\DB::raw('products_custom.custom_id as custom_id, products_custom.custom_code as custom_code, products.id as id, products.name as name, products.code as code, products.mini_code as mini_code'))
        ->join('products_custom', 'products.id', '=', 'products_custom.product_id')
        ->get();

        foreach ($sql_product as $product) {
            $sql_pp = DB::table('status_requests')->where('product_id', $product->id)->where('custom_color', $product->custom_id)->count();
            if ($sql_pp < 1) {
                if ($debug > 0) {
                    //print_r("(NEW) $product->name | 'custom_color' => $product->custom_id | 'custom_code' => $product->custom_code\n");
                    DB::table('status_requests')->insert(array('id' => null, 'product_id' => $product->id, 'custom_color' => $product->custom_id , 'custom_code' => $product->custom_code, 'name' => $product->name, 'value' => 0, 'code' => $product->code, 'mini_code' => $product->mini_code));
                }
            }
        }
        
        // SERRALHARIA
        if ($debug > 0) {
            //print_r("SERRALHARIA - GETTING INFO FROM PRODUCTS (sem PP)...\n\n");
        }

        $sql_product = \App\Model\Locksmith::get();
        foreach ($sql_product as $product) {
            $sql_pp = DB::table('locksmith_status_requests')->where('locksmith_id', $product->id)->count();
            if ($sql_pp < 1) {
                if ($debug > 0)
                    //print_r("(NEW) $product->name\n");
                DB::table('locksmith_status_requests')->insert(array('id' => null, 'locksmith_id' => $product->id, 'name' => $product->name, 'value' => 0, 'code' => $product->code, 'mini_code' => $product->mini_code));
            }
        }    
        
        // TECNICA
        if ($debug > 0) {
            //print_r("TECNICA - GETTING INFO FROM PRODUCTS (sem PP)...\n\n");
        }

        $sql_product = \App\Model\Tech::get();
        foreach ($sql_product as $product) {
            $sql_pp = DB::table('tech_status_requests')->where('tech_id', $product->id)->count();
            if ($sql_pp < 1) {
                if ($debug > 0)
                    //print_r("(NEW) $product->name\n");
                DB::table('tech_status_requests')->insert(array('id' => null, 'tech_id' => $product->id, 'name' => $product->name, 'value' => 0, 'code' => $product->code, 'mini_code' => $product->mini_code));
            }
        }          
    }

    protected function connect_phc($debug) {
        //print_r("PHC\n\n");
        if ($debug > 0) {
            $server_port = '192.168.1.10';
            $port = '1433';

            $ser = $server_port; #the name of the SQL Server
            $db = "cvagroup"; #the name of the database
            $user = "cva"; #a valid username
            $pass = "cva295"; #a password for the username
            $instance = "PHC";

            $conn = odbc_connect("Driver={".env('ODBC_DRIVER')."};Server=$server_port;Instance=$instance;Database=$db;", $user, $pass);

            // CORES TO NXT - PART 1
            DB::table('products_custom')->truncate();

            // PRODUTOS
            $sql = "SELECT st.texteis AS texteis, st.design AS design, st.qttcli AS qttcli, st.stock AS stock, st.ref AS ref, sx.cor as cor, sx.codigo AS codigo FROM st FULL OUTER JOIN sx ON st.ref = sx.ref WHERE st.familia LIKE '01.%' AND st.ref LIKE 'NC%'";
            $rs = odbc_exec($conn, $sql);
            $get_now = \Carbon\Carbon::now()->toDateTimeString();
            
            //print_r("Contagem: ".count($rs)."\n\n");

            while (odbc_fetch_row($rs)) {
                $NOME = trim(odbc_result($rs, "design"));
                $TEXTEIS = trim(odbc_result($rs, "texteis"));
                $RESERVADO = odbc_result($rs, "qttcli");
                $STOCK = odbc_result($rs, "stock");                
                $CUSTOM_COLOR = odbc_result($rs, "cor");
                $CUSTOM_CODE = odbc_result($rs, "codigo");
                
                //print_r("COLOR: ($CUSTOM_COLOR) $CUSTOM_CODE...\n");
                $CUSTOM_COLOR = preg_replace('~\x{00a0}~siu',' ',$CUSTOM_COLOR);
                $CUSTOM_COLOR = trim($CUSTOM_COLOR);                
                
                
                $CUSTOM_CODE = preg_replace('~\x{00a0}~siu',' ',$CUSTOM_CODE);
                $CUSTOM_CODE = trim($CUSTOM_CODE);

                //print_r("COLOR trimmed: ($CUSTOM_COLOR) $CUSTOM_CODE...\n");

                $REF = str_replace("NC", "", trim(odbc_result($rs, "ref")));
                //print_r("FETCHING: ($REF) $NOME...\n");

                // CORES
                //print_r("CORES...\n");
                $product_exist = \App\Model\Product::where('mini_code', 'NC')->where('code', $REF)->get();
                if (count($product_exist) > 0) {
                    foreach($product_exist as $custom) {
                        if ($CUSTOM_COLOR) { 
                            $get_custom_id = \App\Model\ProductCustomNxt::where('name_cor', $CUSTOM_COLOR)->first(); 
                            if (count($get_custom_id) > 0) { $custom_id = $get_custom_id->id; }
                            else { $custom_id = 1; }
                        }
                        else {
                            $custom_id = 1; 
                        }
                        DB::table('products_custom')->insert(array('id' => null, 'custom_id' => $custom_id, 'product_id' => $custom->id, 'custom_code' => $CUSTOM_CODE, 'nc_code' => "NC$REF", 'name' => $CUSTOM_COLOR));
                    }
                }
                //print_r("VALIDATING...\n");
                $VALIDATE = DB::table('status_requests')->where('custom_code', $CUSTOM_CODE)->count();
                if ($VALIDATE > 0) {
                    $CONTA = DB::table('status_requests')->where('custom_code', $CUSTOM_CODE)->first()->value;
                    $CONTA_count = DB::table('status_requests')->where('custom_code', $CUSTOM_CODE)->count();
                    if (count($CONTA_count)) {
//                    $TOTAL = $STOCK - $RESERVADO + $CONTA;
                        $TOTAL = $STOCK - $RESERVADO;
                        $get_custom_SQL = \App\Model\ProductCustomNxt::where('name_cor', $CUSTOM_COLOR)->first();
                        if (count($get_custom_SQL) > 0) { $custom_id = $get_custom_SQL->id; }
                        else { $custom_id = 1; }
                        //print_r("INSERTING: $CUSTOM_COLOR | $CUSTOM_CODE ($REF) $NOME | RESERVADO: $RESERVADO | STOCK: $STOCK = TOTAL: $TOTAL | custom_color: $custom_id \n");
                        DB::table('status_requests')->where('code', $REF)->where('mini_code', 'NC')->update(['phc_reserved' => $RESERVADO, 'phc_available' => $STOCK, 'total' => $TOTAL, 'last_update' => $get_now, 'designation' => $NOME, 'custom_color' => $custom_id, 'custom_code' => $CUSTOM_CODE, 'texteis' => $TEXTEIS]);
                    }
                } else {
                    //print_r("BYPASSING: ($REF) $NOME...\n");               
                }
            }


            // CAPAS
            print_r("CAPAS...\n");
            $sql = "SELECT * FROM st WHERE familia='10' AND ref like 'TX%'";
            $rs = odbc_exec($conn, $sql);
            $get_now = \Carbon\Carbon::now()->toDateTimeString();
            while (odbc_fetch_row($rs)) {
                $NOME = trim(odbc_result($rs, "design"));
                $TEXTEIS = trim(odbc_result($rs, "texteis"));
                $RESERVADO = odbc_result($rs, "qttcli");
                $STOCK = odbc_result($rs, "stock");
                $REF = str_replace("TX", "", trim(odbc_result($rs, "ref")));
                //print_r("CAPAS FETCHING: ($REF) $NOME...\n");
                $VALIDATE = DB::table('status_requests')->where('code', $REF)->where('mini_code', 'TX')->count();
                if ($VALIDATE > 0) {
                    $CONTA = DB::table('status_requests')->where('code', $REF)->where('mini_code', 'TX')->first()->value;
                    $CONTA_count = DB::table('status_requests')->where('code', $REF)->where('mini_code', 'TX')->count();
                    if (count($CONTA_count)) {
//                    $TOTAL = $STOCK - $RESERVADO + $CONTA;
                        $TOTAL = $STOCK - $RESERVADO;
                        //print_r("INSERTING: ($REF) $NOME | RESERVADO: $RESERVADO | STOCK: $STOCK = TOTAL: $TOTAL\n");
                        DB::table('status_requests')->where('code', $REF)->where('mini_code', 'TX')->update(['phc_reserved' => $RESERVADO, 'phc_available' => $STOCK, 'total' => $TOTAL, 'last_update' => $get_now, 'designation' => $NOME, 'texteis' => $TEXTEIS]);
                    }
                } else {
                    //print_r("BYPASSING: ($REF) $NOME...\n");
                }
            }

            // SERRALHARIA 
            print_r("SERRALHARIA...\n\n");
            //$sql = "SELECT * FROM st WHERE design LIKE '%suporte%' OR design LIKE '%acessorio%' OR design LIKE '%dolly%' OR design LIKE '%painel%'";            
            //$sql = "select ref, design, qttcli, stock from st where design like '%NEXT%' and (design like '%suporte%' or design like '%acessorio%' or design like '%dolly%' or design like '%painel%' or design like '%estrutura%' or design like '%frame%') and familia=1 and ref not like '%C' and design not like 'bloqueado%' and ref not like '%P' and ref not like '%M' and ref like 'NC%'";
            $sql = "select ref, design, qttcli, stock, fornecedor from st where design like '%NEXT%' and (design like '%suporte%' or design like '%acessorio%' or design like '%dolly%' or design like '%painel%' or design like '%estrutura%' or design like '%frame%') and ref like 'NC%' and familia not like '01.%' and stns=0 and fornecedor = 'CVA ELECTRÓNICA, LDA'";
            $rs = odbc_exec($conn, $sql);
            $get_now = \Carbon\Carbon::now()->toDateTimeString();
            while (odbc_fetch_row($rs)) {
                $NOME = trim(odbc_result($rs, "design"));
                $RESERVADO = odbc_result($rs, "qttcli");
                $STOCK = odbc_result($rs, "stock");
                $REF = str_replace("NC", "", trim(odbc_result($rs, "ref")));
                //print_r("SERRALHARIA FETCHING: ($REF) $NOME...\n");
                $VALIDATE = DB::table('locksmith_status_requests')->where('code', $REF)->where('mini_code', 'NC')->count();
                if ($VALIDATE > 0) {
                    $CONTA = DB::table('locksmith_status_requests')->where('code', $REF)->where('mini_code', 'NC')->first()->value;
                    $CONTA_count = DB::table('locksmith_status_requests')->where('code', $REF)->where('mini_code', 'NC')->count();
                    if (count($CONTA_count)) {
//                    $TOTAL = $STOCK - $RESERVADO + $CONTA;
                        $TOTAL = $STOCK - $RESERVADO;
                        //print_r("INSERTING: ($REF) $NOME | RESERVADO: $RESERVADO | STOCK: $STOCK = TOTAL: $TOTAL\n");
                        DB::table('locksmith_status_requests')->where('code', $REF)->where('mini_code', 'NC')->update(['phc_reserved' => $RESERVADO, 'phc_available' => $STOCK, 'total' => $TOTAL, 'last_update' => $get_now, 'designation' => $NOME]);
                    }
                } else {
                    //print_r("BYPASSING: ($REF) $NOME...\n");
                }
            }
            
            // TÉCNICA
            print_r("TECNICA...n\n");
            $tech_sql = DB::table('tech')->orderby('name', 'desc')->get();
            $get_now = \Carbon\Carbon::now()->toDateTimeString();
            foreach ($tech_sql as $product) {
                $REF_NXT = $product->mini_code."".$product->code;
                $sql = "SELECT ref, design, qttcli, stock FROM st WHERE ref='$REF_NXT'";
                $rs = odbc_exec($conn, $sql);
                while (odbc_fetch_row($rs)) {
                    $NOME = trim(odbc_result($rs, "design"));
                    $RESERVADO = odbc_result($rs, "qttcli");
                    $STOCK = odbc_result($rs, "stock");                    
                    $REF = str_replace($product->mini_code, "", trim(odbc_result($rs, "ref")));
                    //print_r("TECNICA FETCHING: ($REF) [$REF_NXT] $NOME...\n");
                    $VALIDATE = DB::table('tech_status_requests')->where('code', $product->code)->where('mini_code', $product->mini_code)->count();
                    if ($VALIDATE > 0) {
                        $CONTA = DB::table('tech_status_requests')->where('code', $product->code)->where('mini_code', $product->mini_code)->first()->value;
                        $CONTA_count = DB::table('tech_status_requests')->where('code', $product->code)->where('mini_code', $product->mini_code)->count();
                        if (count($CONTA_count)) {
    //                    $TOTAL = $STOCK - $RESERVADO + $CONTA;
                            $TOTAL = $STOCK - $RESERVADO;
                            //print_r("INSERTING: ($REF) $NOME | RESERVADO: $RESERVADO | STOCK: $STOCK = TOTAL: $TOTAL\n");
                            DB::table('tech_status_requests')->where('code', $product->code)->where('mini_code', $product->mini_code)->update(['phc_reserved' => $RESERVADO, 'phc_available' => $STOCK, 'total' => $TOTAL, 'last_update' => $get_now, 'designation' => $NOME]);
                        }
                    } else {
                        //print_r("BYPASSING: ($REF) $NOME...\n");
                    }
                }  
            }                      
            
        }
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->call(function () {
            $this->processJobsIfPending();
        })->everyMinute();

        $schedule->call(function () {

            // SETTINGS
            $debug_print = 1;
//            $this->find_new_ip(1);
            print_r("collect_NXT_orders\n");
            $this->collect_NXT_orders(1);
            print_r("collect_new_products_info\n");
            $this->collect_new_products_info(1);
            print_r("connect_phc\n");
            $this->connect_phc(1);
            print_r("check_week_limit_reset\n");
            $this->check_week_limit_reset(1);
            print_r("check_monthly_limit_reset\n");
            $this->check_monthly_limit_reset(1);
            print_r("update_products\n");
            \Artisan::call('update_products', ['section'=>'pp_orders']);

//            if ($debug_print > 0)
//                //print_r("NEW_EXTRA...\n");
//
//
//            $sql_week = DB::table('request_week')->orderBy('end_at', 'asc')->get();
//            foreach ($sql_week as $week) {
//                $reservas[$week->product_id]['extra'] = $reservas[$week->product_id]['extra'] + $week->montagem;
//            }
//            $DUMP = DB::select("
//                    SELECT 
//                    `mount_users`.`request_id`,
//                    `mount_users`.`value`,
//                    `mount_users`.`obs`,                    
//                    `requests`.`id` as PP,
//                    `products`.`id` as product_id,
//                    `products`.`name` as NAME,
//                    `mount_users`.`created_at`
//                    FROM `mount_users`
//                    INNER JOIN `requests` ON mount_users.request_id = requests.id
//                    INNER JOIN `products` ON requests.product_id = products.id
//
//                    WHERE mount_users.published>=-1 
//                    ORDER BY mount_users.created_at ASC
//                    ");
//
//            foreach ($DUMP as $saida) {
//                if (!isset($reservas[$saida->product_id]['stock'])) {
//                    $reservas[$saida->product_id]['stock'] = 0;
//                }
//                if (!isset($reservas[$saida->product_id]['extra'])) {
//                    $reservas[$saida->product_id]['extra'] = 0;
//                }
//                $reservas[$saida->product_id]['stock'] = $reservas[$saida->product_id]['stock'] + 1;
//            }
            //var_dump($reservas[52]);
//            $get_now = \Carbon\Carbon::now()->toDateTimeString();
//            foreach ($reservas as $product) {
//                if ($product['encomendas'] == 0) {
//                    $total_extra = 0;
//                } else {
//                    $total_extra = $product['total_week'] - $product['extra'] - $product['total_extra'];
//                }
//                DB::table('status_requests')->where('product_id', $product['product_id'])->update(['extra' => $total_extra, 'last_update' => $get_now]);
//            }

            if ($debug_print > 1) {
                // SEND GET TO EXTERNAL DH
                #    $json = json_decode(file_get_contents('http://host.com/api/stuff/1'), true);
                #$sql_get = DB::table("status_requests")->select(array('product_id', 'total'))->get();
                #//print_r($sql_get);
                #$SEND_JSON = $sql->toJson();
                #//print_r($SEND_JSON);
                #$request = Request::create('http://www.next-proaudio.com/temp_injection/5345345345324', 'GET');
            }
        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}

<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        // WORK AREA
        \App\Model\Locksmith_Open::class => \Admin\Policies\Locksmith_OpenSectionModelPolicy::class,
        \App\Model\Locksmith_History::class => \Admin\Policies\Locksmith_HistorySectionModelPolicy::class,
        \App\Model\CNC_history::class => \Admin\Policies\CNC_historySectionModelPolicy::class,
        \App\Model\WOOD_open::class => \Admin\Policies\WOOD_openSectionModelPolicy::class,
        \App\Model\WOOD_history::class => \Admin\Policies\WOOD_historySectionModelPolicy::class,
        \App\Model\WOODFINISHING_history::class => \Admin\Policies\WOODFINISHING_historySectionModelPolicy::class,
        \App\Model\WOODFINISHING_open::class => \Admin\Policies\WOODFINISHING_openSectionModelPolicy::class,
        \App\Model\PAINT_history::class => \Admin\Policies\PAINT_historySectionModelPolicy::class,
        \App\Model\PAINT_open::class => \Admin\Policies\PAINT_openSectionModelPolicy::class,
        \App\Model\MOUNT_history::class => \Admin\Policies\MOUNT_historySectionModelPolicy::class,
        \App\Model\MOUNT_open::class => \Admin\Policies\MOUNT_openSectionModelPolicy::class,
        \App\Model\MOUNT_history_Stock::class => \Admin\Policies\MOUNT_history_StockSectionModelPolicy::class,
        \App\Model\MOUNT_history_Stock_OK::class => \Admin\Policies\MOUNT_history_Stock_OKSectionModelPolicy::class,
        \App\Model\MOUNT_history_Stock_OK_Search::class => \Admin\Policies\MOUNT_history_Stock_OK_SearchSectionModelPolicy::class,
        \App\Model\Palete::class => \Admin\Policies\PaleteSectionModelPolicy::class,
        \App\Model\WIRES_history::class => \Admin\Policies\WIRES_historySectionModelPolicy::class,
        \App\Model\BLACKFINISHING_history::class => \Admin\Policies\BLACKFINISHING_historySectionModelPolicy::class,
        // ADMIN
        \App\User::class => \Admin\Policies\UsersSectionModelPolicy::class,
        \App\Role::class => \Admin\Policies\RolesSectionModelPolicy::class,
        \App\Model\Request::class => \Admin\Policies\RequestSectionModelPolicy::class,
        \App\Model\Serie::class => \Admin\Policies\SerieSectionModelPolicy::class,
        \App\Model\Product::class => \Admin\Policies\ProductSectionModelPolicy::class,
        \App\Model\RequestWeek::class => \Admin\Policies\RequestWeekSectionModelPolicy::class,
        \App\Model\RequestWeekOrder::class => \Admin\Policies\RequestWeekOrderSectionModelPolicy::class,
        \App\Model\DeliveryCalc::class => \Admin\Policies\DeliveryCalcSectionModelPolicy::class,
        \App\Model\StatusRequests::class => \Admin\Policies\StatusRequestsSectionModelPolicy::class,
        \App\Model\RequestWeekGroup::class => \Admin\Policies\RequestWeekGroupSectionModelPolicy::class,
        \App\Model\Deadline::class => \Admin\Policies\DeadlineSectionModelPolicy::class,
        \App\Model\Clock::class => \Admin\Policies\ClockSectionModelPolicy::class,
        \App\Model\Clock_2::class => \Admin\Policies\Clock_2SectionModelPolicy::class,
        \App\Model\Clock_User::class => \Admin\Policies\Clock_UserSectionModelPolicy::class,
        \App\Model\Clock_User_2::class => \Admin\Policies\Clock_User_2SectionModelPolicy::class,
        \App\Model\Clock_Month::class => \Admin\Policies\Clock_MonthSectionModelPolicy::class,
        \App\Model\Clock_Month_2::class => \Admin\Policies\Clock_Month_2SectionModelPolicy::class,
        \App\Model\Clock_Day::class => \Admin\Policies\Clock_DaySectionModelPolicy::class,
        \App\Model\FreezeWeek::class => \Admin\Policies\FreezeWeekSectionModelPolicy::class,
        \App\Model\FreezeWeek_1::class => \Admin\Policies\FreezeWeek_1SectionModelPolicy::class,
        \App\Model\FreezeWeek_2::class => \Admin\Policies\FreezeWeek_2SectionModelPolicy::class,
        \App\Model\FreezeWeek_3::class => \Admin\Policies\FreezeWeek_3SectionModelPolicy::class,
        \App\Model\FreezeWeek_All::class => \Admin\Policies\FreezeWeek_AllSectionModelPolicy::class,
        \App\Model\FreezeWeek_Atraso::class => \Admin\Policies\FreezeWeek_AtrasoSectionModelPolicy::class,
        \App\Model\Comsumables::class => \Admin\Policies\ComsumablesSectionModelPolicy::class,
        \App\Model\ComsumablesOrder::class => \Admin\Policies\ComsumablesOrderSectionModelPolicy::class,
        \App\Model\ComsumablesOrder_History::class => \Admin\Policies\ComsumablesOrder_HistorySectionModelPolicy::class,
        \App\Model\LocksmithStatusRequests::class => \Admin\Policies\LocksmithStatusRequestsSectionModelPolicy::class,
        \App\Model\WorkAbsences::class => \Admin\Policies\WorkAbsencesSectionModelPolicy::class,
        \App\Model\Colors::class => \Admin\Policies\ColorsSectionModelPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    protected function getPermissions() {
        return Permission::with('roles')->get();
    }

}

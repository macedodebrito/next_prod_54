<?php

namespace App\Model\V2;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\ServiceProvider;

class PAINT extends Model {

    /**
     * @var array
     */
    protected $table = 'paint_users';
    protected $fillable = [
        'request_id',
        'user_id',
        'product_id',
        'value',
        'obs',
        'published'
    ];
    
    static function boot() {        
    }

    public function requests() {
        return $this->hasMany(Request::class);
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(Request::class, 'request_id');
    }

}

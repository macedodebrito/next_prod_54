<?php

namespace App\Model\V2;

use Baum\Extensions\Eloquent\Model;

class RequestWeek extends Model {

    /**
     * @var array
     */
    protected $table = 'request_week';
    protected $fillable = [
        'product_id',
        'user_id',
        'quantity',
        'carpintaria',
        'woodfinishing',
        'pintura',
        'montagem',
        'client',
        'obs',
        'end_at',
        'end_paint_at',
        'end_woodfinishing_at',
        'end_wood_at',
        'group',
        'urgency_date',
        'urgency',
        'order'
    ];
    protected $appends = [
        'end_cnc_at',
        'end_prep_at',
        'end_at_simple'
    ];

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function sum_orders() {
        return $this->belongsTo(Sum_Orders::class, 'id', 'order_id');
    }
    
    public function getEndCncAtAttribute() {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_wood_at'])->subDays(5)->format('Y-m-d H:i:s');
    }
    public function getEndPrepAtAttribute() {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_wood_at'])->subDays(5)->format('Y-m-d H:i:s');
    }
    public function getEndAtSimpleAttribute() {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_at'])->format('Y-m-d');
    }
    public function request_custom_nxt() {
        return $this->hasOne(\App\Model\ProductCustomNxt::class, 'id', 'custom_id');
    }
}

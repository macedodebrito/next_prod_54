<?php

namespace App\Model\V2;

use Baum\Extensions\Eloquent\Model;
use \App\Webpatser\Uuid\Uuid;

class PHCMountOutputBI extends Model {

    /**
     * @var array
     */
    protected $table = 'phc_mount_output_bi';
   
}


<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distro_Prosp extends Model
{
    protected $table = "distributors_prosp";
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'market',
        'type',
        'since',
        'deadline',
        'official_date',
        'obs',
        'state',
    ];
 
}

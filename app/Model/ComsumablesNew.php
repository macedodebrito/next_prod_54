<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ComsumablesNew extends Model {
    protected $table = 'comsumables_new';
    protected $fillable = [
        'comsumables_order_id',
        'name',
        'value',
    ];    
}

<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = "notifications";
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'section',
        'user',
        'type',
        'object_id',
        'open_user_id',
        'close_user_id',
        'status',
    ];
 
}

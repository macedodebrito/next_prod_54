<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TechRequests extends Model {
    /**
     * @var array
     */
    protected $table = 'tech_requests';
    protected $fillable = [
        'pp',
        'value',
        'start_at',
        'order_at'
    ];
    public function products() {
        return $this->belongsTo(\App\Model\Tech::class, 'tech_id');
    }

    public function request_users() {
        return $this->belongsTo(\App\Model\TechRequestsUser::class, 'tech_requests_id');
    }  
}

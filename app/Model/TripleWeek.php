<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class TripleWeek extends Model {

    /**
     * @var array
     */
    protected $table = 'triple_week';
    protected $fillable = [
        'product_id',
        'carpintaria',
        'pintura',
        'montagem',
        'obs',
        'start_at',
        'end_at',
        'end_paint_at',
        'end_wood_at',
        'client',
    ];
    protected $appends = [
        'total_points',
        'total_hours',
        'total_days'
    ];

    public function getTotalPointsAttribute() {
        $POINTS = Product::where('id', $this->attributes['product_id'])->first()->points;
        return $this->montagem * $POINTS;
    }

    public function getTotalHoursAttribute() {
        return $this->total_points * 0.0125;
    }

    public function getTotalDaysAttribute() {
        return $this->total_hours / 8;
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

}

<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Config extends Model {

    /**
     * @var array
     */
    protected $table = 'config';
    protected $fillable = [
        'limits_cnc',
        'limits_wood',
        'limits_woodfinishing',
        'limits_paint',
        'limits_mount',
        'week_0',
        'week_1',
        'week_2',
        'week_3',
    ];
}


<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\PaintUser;
use App\Model\MountUser;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\ServiceProvider;

class MountUser extends Model {

    /**
     * @var array
     */
    protected $table = 'mount_users';
    protected $fillable = [
        'request_id',
        'user_id',
        'product_id',
        'value',
        'obs',
        'serial',
        'published'
    ];

    public function __construct() {
        
    }

    static function boot() {

//        Validator::extend('serial_dump', function($attribute, $value, $parameters, $validator) {
//            $messages = ["check_max2" => "!"];
//            if (!isset($_REQUEST['request_id'])) {
//                return $validator;
//            } else {
//                $total = PaintUser::where('request_id', $_REQUEST['request_id'])->sum('value');
//                $permitido = MountUser::where('request_id', $_REQUEST['request_id'])->sum('value');
//                $permitido = $total - $permitido;
//                if ($permitido < $value) {
//                    $validator->errors()->all();
//                } else {
//                    return $value;
//                }
//            }
//        });   
//        print_r("<pre>");
//        print_r($_REQUEST);
//        print_r("</pre>");
//        if (isset($_REQUEST['next_action']) == "save_and_close") {
//            //dd($_REQUEST['serial']);
//            $dump = 0;
//            while($dump < $_REQUEST['value']) {
//                //print_r("</br> $key > $value </br>");
//                $dump++;
//                $info = "serial_info_"."$dump";
//                $teste = MountUser::where('request_id', $_REQUEST['request_id'])->where('serial', $_REQUEST[$info])->first();
//                if (count($teste) == 0) {
//                    MountUser::insert(array('id' => null, 'request_id' => $_REQUEST['request_id'], 'user_id' => $_REQUEST['user_id'], 'value' => $_REQUEST['value'], 'serial' => $_REQUEST[$info], 'obs' => $_REQUEST['obs'], 'created_at' => 'NOW()', 'updated_at' => 'NOW()'));
//                }
//            }
//        }
        //parent::boot();
//        static::creating(function(Model $model) {
//            //dd($_REQUEST);
//            
//            foreach ($_REQUEST['serial'] as $key => $value) {
//                //print_r("</br> $key > $value </br>");
//      
//                $teste = MountUser::where('request_id', $_REQUEST['request_id'])->where('serial', $value)->first();
//                if (count($teste) == 0) {
//                    MountUser::insert(array('id' => null, 'request_id' => $_REQUEST['request_id'], 'user_id' => $_REQUEST['user_id'], 'value' => $_REQUEST['value'], 'serial' => $value, 'obs' => $_REQUEST['obs'], 'created_at' => 'NOW()', 'updated_at' => 'NOW()'));
//                }
//            }
//            return true;
//        });
//        static::create(array $data = []) {
//            
//        };
    }

    public function requests() {
        return $this->hasMany(Request::class);
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(Request::class, 'request_id');
    }

}

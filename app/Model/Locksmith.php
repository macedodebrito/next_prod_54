<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Locksmith extends Model
{

    /**
     * @var array
     */
    protected $table = 'locksmith';
    protected $fillable = [
        'code',
        'name',
        'mini_code',
        'points',
        'cnc',
        'store'
    ];

    public function requests()
    {
        return $this->hasMany(\App\Model\LocksmithRequests::class);
    }
    
    public function request_users()
    {
        return $this->hasMany(\App\Model\LocksmithRequestsUser::class);
    }    
}

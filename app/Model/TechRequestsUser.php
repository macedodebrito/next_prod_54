<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Validator;
use Illuminate\Support\ServiceProvider;

class TechRequestsUser extends Model {

    /**
     * @var array
     */
    protected $table = 'tech_requests_users';
    protected $fillable = [
        'tech_request_id',
        'user_id',
        'valid_id',
        'value',
        'obs',
        'published'
    ];

    static function boot() {
        Validator::extend('check_max', function($attribute, $value, $parameters, $validator) {
            $messages = ["check_max" => "!"];
            if (!isset($_REQUEST['tech_requests_id'])) {
                return $validator;
            } else {
                $total = TechRequests::where('id', $_REQUEST['tech_requests_id'])->sum('value');
                $permitido = TechRequestsUser::where('tech_requests_id', $_REQUEST['tech_requests_id'])->sum('value');
                $permitido = $total - $permitido;
                if ($permitido < $value) {
                    $validator->errors()->all();
                } else {
                    return $value;
                }
            }
        });
    }

    public function requests() {
        return $this->hasMany(TechRequests::class);
    }

    public function products() {
        return $this->belongsTo(Tech::class, 'tech_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(TechRequests::class, 'tech_requests_id');
    }

}

<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class LocksmithWeek extends Model {

    /**
     * @var array
     */
    protected $table = 'locksmith_week';
    protected $fillable = [
        'locksmith_id',
        'user_id',
        'quantity',
        'client',
        'obs',
        'end_at',
        'group'
    ];

    public function products() {
        return $this->belongsTo(Locksmith::class, 'locksmith_id');
    }

}

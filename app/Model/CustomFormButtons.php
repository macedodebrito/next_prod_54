<?php
namespace App\Model;

class CustomFormButtons extends \SleepingOwl\Admin\Form\FormButtons {

    protected $hideSaveButton;
   

    public function isSaveButton()
    {
        return $this->showSaveButton;
    }    
    
    public function hideSaveButton()
    {
        $this->showSaveButton = false;
        return $this;
    }    
}
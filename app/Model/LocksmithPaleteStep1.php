<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class LocksmithPaleteStep1 extends Model {

    /**
     * @var array
     */
    protected $table = 'locksmith_palete';
    protected $fillable = [
        'locksmith_week_id',
        'store_id',
        'store_obs',
        'stored_date',
        'auth_id',
        'auth_obs',
        'authed_date',
    ];
    
    public function requests() {
        return $this->hasMany(LocksmithRequests::class);
    }

    public function products() {
        return $this->belongsTo(Locksmith::class, 'id');
    }

    public function users() {
        return $this->belongsTo(\App\User::class, 'id');
    }

    public function locksmith_week() {
        return $this->belongsTo(LocksmithWeek::class, 'locksmith_week_id');
    }
    
}

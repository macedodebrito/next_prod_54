<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends Model {

    use SoftDeletes;

    /**
     * @var array
     */
    protected $table = 'requests';
    protected $fillable = [
        'name'
    ];
    protected $appends = [
        'pp_total',
        'disp_cnc',
        'disp_carpintaria',
        'disp_acabamento',
        'disp_pintura',
        'disp_montagem',
        'disp_pintura',
        'pp_aberto',
        'encomendas'
    ];
    var $TOTAL;

    public function getPpTotalAttribute() {
        global $TOTAL;
        $total = \App\Model\Request::where('product_id', $this->attributes['product_id'])->get();
        $sum = 0;
        foreach ($total as $value) {
            $sum = $sum + $value->value;
        }
        $GG = [];
        $GG['total'] = $total;
        $GG['sum'] = $sum;
        $TOTAL = $total;
        return $GG;
    }

    public function getDispCncAttribute() {
        global $TOTAL;
        $fabricados = 0;
        foreach ($TOTAL as $value1) {
            $fabricados_a = \App\Model\RequestUser::where('request_id', $value1->id)->where('published', 1)->get();
            foreach ($fabricados_a as $value) {
                $fabricados = $fabricados + $value->value;
            }
        }
        return $fabricados;
    }

    public function getDispCarpintariaAttribute() {
        global $TOTAL;
        $CARPINTARIA = 0;
        foreach ($TOTAL as $value1) {
            $CARPINTARIA_a = \App\Model\WoodUser::where('request_id', $value1->id)->where('published', 1)->get();
            foreach ($CARPINTARIA_a as $value) {
                $CARPINTARIA = $CARPINTARIA + $value->value;
            }
        }
        return $CARPINTARIA;
    }

    public function getDispAcabamentoAttribute() {
        global $TOTAL;
        $ACABAMENTO = 0;
        foreach ($TOTAL as $value1) {
            $ACABAMENTO_a = \App\Model\WoodfinishingUser::where('request_id', $value1->id)->where('published', 1)->get();
            foreach ($ACABAMENTO_a as $value) {
                $ACABAMENTO = $ACABAMENTO + $value->value;
            }
        }
        return $ACABAMENTO;
    }
    
    public function getDispPinturaAttribute() {
        global $TOTAL;
        $PINTURA = 0;
        foreach ($TOTAL as $value1) {
            $PINTURA_a = \App\Model\PaintUser::where('request_id', $value1->id)->where('published', 1)->get();
            foreach ($PINTURA_a as $value) {
                $PINTURA = $PINTURA + $value->value;
            }
        }
        return $PINTURA;
    }

    public function getDispMontagemAttribute() {
        global $TOTAL;
        $MONTAGEM = 0;
        foreach ($TOTAL as $value1) {
            $MONTAGEM_a = \App\Model\MountUser::where('request_id', $value1->id)->where('published', '>', -1)->get();
            foreach ($MONTAGEM_a as $value) {
                $MONTAGEM = $MONTAGEM + $value->value;
            }
        }
        return $MONTAGEM;
    }

//    public function getDispArmazemAttribute() {
//        global $TOTAL;
//        $ARMAZEM = 0;
//        foreach ($TOTAL as $value1) {
//            $ARMAZEM_a = MountUser::where('request_id', $value1->id)->where('published', 0)->get();
//            if (!count($ARMAZEM)) {
//                $ARMAZEM = $ARMAZEM + 0;
//            } else {
//                foreach ($ARMAZEM_a as $value) {
//                    $ARMAZEM = $ARMAZEM + $value->value;
//                }
//            }
//        }
//        return $ARMAZEM;
//    }

    public function getEncomendasAttribute() {
        $total_week = \App\Model\RequestWeek::where('product_id', $this->attributes['product_id'])->orderBy('end_at', 'asc')->sum('montagem');
        if (!count($total_week)) {
            $total_week = 0;
        }
        $total_req2 = \App\Model\Request::where('product_id', $this->attributes['product_id'])->get();
        $total_inc = 0;
        foreach ($total_req2 as $go) {
            $dump = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', -1)->sum('value');
            $total_inc = $total_inc + $dump;
        }
        $CONTA = $total_week - $total_inc;
        return $CONTA;
    }

    public function products() {
        return $this->belongsTo(\App\Model\Product::class, 'product_id');
    }

    public function request_users() {
        return $this->belongsTo(\App\Model\RequestUser::class, 'request_id');
    }

}

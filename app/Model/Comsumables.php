<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comsumables extends Model
{

    /**
     * @var array
     */
    protected $table = 'comsumables';
    protected $fillable = [
        'ref',
        'nome',
        'cnc',
        'carpintaria',
        'acabamento',
        'pintura',
        'montagem',
        'serralharia',
        'tecnica',
        'redes',
        'serigrafia',
        'filtros',
        'unit',
        'between_start',
        'between_end',
    ];


}

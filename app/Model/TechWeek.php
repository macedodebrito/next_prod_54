<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class TechWeek extends Model {

    /**
     * @var array
     */
    protected $table = 'tech_week';
    protected $fillable = [
        'tech_id',
        'user_id',
        'quantity',
        'client',
        'obs',
        'end_at',
        'group'
    ];

    public function products() {
        return $this->belongsTo(Tech::class, 'tech_id');
    }

}

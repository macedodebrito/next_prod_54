<?php namespace App\Model;

use Baum\Extensions\Eloquent\Model;
use App\Model\Serie;
use App\Model\Product;

class SerieProduct extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'serie_id',
        'product_id'
    ];

}

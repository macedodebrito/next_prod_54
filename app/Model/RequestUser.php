<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Validator;
use Illuminate\Support\ServiceProvider;

class RequestUser extends Model {

    /**
     * @var array
     */
    protected $table = 'request_users';
    protected $fillable = [
        'request_id',
        'user_id',
        'product_id',
        'value_id',
        'obs'
    ];

    
    static function boot() {
        Validator::extend('check_max', function($attribute, $value, $parameters, $validator) {
            $messages = ["check_max" => "!"];
            if (!isset($_REQUEST['request_id'])) {
                return $validator;
            } else {
                $total = Request::where('id', $_REQUEST['request_id'])->sum('value');
                $permitido = RequestUser::where('request_id', $_REQUEST['request_id'])->sum('value');
                $permitido = $total - $permitido;
                if ($permitido < $value) {
                    $validator->errors()->all();
                } else {
                    return $value;
                }
            }
        });
    }

    public function requests() {
        return $this->hasMany(Request::class);
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(Request::class, 'request_id');
    }

}

<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Companies extends Model {

    /**
     * @var array
     */
    protected $table = 'companies';
    protected $fillable = [
        'name',
    ];

}

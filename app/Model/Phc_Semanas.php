<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Phc_Semanas extends Model {

    /**
     * @var array
     */
    protected $table = 'phc_semanas';
    protected $fillable = [
        'pp',
        'vendedor',
        'client',
        'data',
        'week',
        'year',
    ];
}


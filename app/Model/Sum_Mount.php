<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sum_Mount extends Model
{
    protected $table = 'sum_mount';
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'pp',
        'product_id',
        'type',
        'cnc',
    ];
}

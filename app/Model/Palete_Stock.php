<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Palete_Stock extends Model {

    /**
     * @var array
     */
    protected $table = 'paletes';
    protected $fillable = [
        'request_week_id',
        'store_id',
        'store_obs',
        'stored_date',
        'auth_id',
        'auth_obs',
        'authed_date',
    ];
    
    public function requests() {
        return $this->hasMany(Request::class);
    }

    public function products() {
        return $this->belongsTo(Product::class, 'id');
    }

    public function users() {
        return $this->belongsTo(\App\User::class, 'id');
    }

    public function request_week() {
        return $this->belongsTo(RequestWeek::class, 'request_week_id');
    }
    
}

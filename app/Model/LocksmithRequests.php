<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LocksmithRequests extends Model {
    /**
     * @var array
     */
    protected $table = 'locksmith_requests';
    protected $fillable = [
        'pp',
        'value',
        'start_at',
        'order_at'
    ];
    public function products() {
        return $this->belongsTo(\App\Model\Locksmith::class, 'locksmith_id');
    }

    public function request_users() {
        return $this->belongsTo(\App\Model\LocksmithRequestsUser::class, 'locksmith_requests_id');
    }  
}

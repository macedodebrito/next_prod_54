<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\WIRES_History;
use Validator;
use Illuminate\Support\ServiceProvider;

class WIRES_History extends Model {

    /**
     * @var array
     */
    protected $table = 'wires';
    protected $fillable = [
        'product_id',
        'user_id',
        'valid_id',
        'value',
        'obs'
    ];

    public function requests() {
        return $this->hasMany(Request::class);
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(Request::class, 'request_id');
    }

}

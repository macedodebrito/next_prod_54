<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLogin_2 extends Model {

    protected $table = "users_login";

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'login_logout',
        'status'
    ];

}

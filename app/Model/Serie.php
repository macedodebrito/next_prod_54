<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SleepingOwl\Admin\Traits\OrderableModel;

class Serie extends Model
{
	use SoftDeletes;
	use OrderableModel;
    /**
     * @var array
     */
    protected $fillable = [
    'name'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getOrderField()
    {
    	return 'order';
    }  
}

<?php

namespace App\Model;

class DeliveryCalc extends Sum_Orders {

    public function getMountDateSimpleAttribute() {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['mount_date'])->format('Y-m-d');
    }

}

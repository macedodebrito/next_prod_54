<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distro extends Model
{
    protected $table = "distributors";
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at'
    ];
 
}

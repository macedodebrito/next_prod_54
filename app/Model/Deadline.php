<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Deadline extends Model {

    /**
     * @var array
     */
    protected $table = 'request_week';
    protected $fillable = [
        'product_id',
        'carpintaria',
        'pintura',
        'montagem',
        'obs',
        'start_at',
        'end_at',
        'end_paint_at',
        'end_wood_at',
        'client',
    ];
    protected $appends = [
        'actual_status',
        'total_points',
        'total_hours',
        'total_days'
    ];

    public function getActualStatusAttribute() {
        $total = Deadline::where('id', $this->attributes['id'])->orderBy('end_at', 'asc')->sum('montagem');
        $total_week = Deadline::where('product_id', $this->attributes['product_id'])->orderBy('end_at', 'asc')->sum('montagem');
        $total_req = Request::where('product_id', $this->attributes['product_id'])->first();
        $total_req2 = Request::where('product_id', $this->attributes['product_id'])->get();
        $total_inc = 0;
        $total_inc2 = 0;
        foreach ($total_req2 as $go) {
            $dump = MountUser::where('request_id', $go->id)->where('published', '>', 0)->sum('value');
            $dump2 = MountUser::where('request_id', $go->id)->where('published', '>', 1)->sum('value');
            $total_inc = $total_inc + $dump;
            $total_inc2 = $total_inc2 + $dump2;
        }
        $SEMANAL[$this->attributes['product_id']]['product_id'] = $this->attributes['product_id'];
        $SEMANAL[$this->attributes['product_id']]['name'] = Product::where('id', $this->attributes['product_id'])->first()->name;
        $SEMANAL[$this->attributes['product_id']]['total_semanal'] = $total_week;
        $SEMANAL[$this->attributes['product_id']]['total_montado'] = $total_inc;
        $SEMANAL[$this->attributes['product_id']]['total_montado2'] = $total_inc2;
        foreach (Deadline::where('product_id', $this->attributes['product_id'])->orderBy('end_at', 'asc')->get() as $key => $value) {
            $SEMANAL[$this->attributes['product_id']]['semana'][$key]['id'] = $value->id;
            $SEMANAL[$this->attributes['product_id']]['semana'][$key]['value'] = $value->montagem;
        }
        foreach ($SEMANAL as $key2 => $value2) {
            foreach ($value2['semana'] as $key3 => $value3) {
                if ($SEMANAL[$key2]['total_montado'] > 0) {
                    if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                    } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                    }
                }
                if ($SEMANAL[$this->attributes['product_id']]['semana'][$key3]['id'] == $this->attributes['id']) {
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                        if ($SEMANAL[$key2]['total_montado2'] < 0) {
                            return "PENDING";
                        } else {
                            return "OK";
                        }
                    } else {
                        $KO = $SEMANAL[$key2]['semana'][$key3]['value'];
                        return "$KO";
                    }
                }
            }
        }

        return "ok";
    }

    public function getTotalPointsAttribute() {
        $POINTS = Product::where('id', $this->attributes['product_id'])->first()->points;
        return $this->actual_status * $POINTS;
    }

    public function getTotalHoursAttribute() {
        return $this->total_points * 0.0125;
    }

    public function getTotalDaysAttribute() {
        return $this->total_hours / 8;
    }

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

}

<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class RequestWeekGroup extends Model {

    /**
     * @var array
     */
    protected $table = 'request_week';
    protected $fillable = [
        'product_id',
        'user_id',
        'quantity',
        'carpintaria',
        'woodfinishing',
        'pintura',
        'montagem',
        'client',
        'obs',
        'end_at',
        'end_paint_at',
        'end_woodfinishing_at',
        'end_wood_at',
        'group'
    ];

    public function products() {
        return $this->belongsTo(Product::class, 'product_id');
    }

}

<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class ProductCustomNxt extends Model {

    /**
     * @var array
     */
    protected $table = 'products_custom_nxt';

    public function custom_items(): HasMany
    {
        return $this->hasMany(\App\Model\ProductCustom::class, 'id', 'custom_id');
    }
}


<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Validator;
use Illuminate\Support\ServiceProvider;

class LocksmithRequestsUser extends Model {

    /**
     * @var array
     */
    protected $table = 'locksmith_requests_users';
    protected $fillable = [
        'locksmith_request_id',
        'user_id',
        'valid_id',
        'value',
        'obs',
        'published'
    ];

    static function boot() {
        Validator::extend('check_max', function($attribute, $value, $parameters, $validator) {
            $messages = ["check_max" => "!"];
            if (!isset($_REQUEST['locksmith_requests_id'])) {
                return $validator;
            } else {
                $total = LocksmithRequests::where('id', $_REQUEST['locksmith_requests_id'])->sum('value');
                $permitido = LocksmithRequestsUser::where('locksmith_requests_id', $_REQUEST['locksmith_requests_id'])->sum('value');
                $permitido = $total - $permitido;
                if ($permitido < $value) {
                    $validator->errors()->all();
                } else {
                    return $value;
                }
            }
        });
    }

    public function requests() {
        return $this->hasMany(LocksmithRequests::class);
    }

    public function products() {
        return $this->belongsTo(Locksmith::class, 'locksmith_id');
    }

    public function users() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function request_pp() {
        return $this->belongsTo(LocksmithRequests::class, 'locksmith_requests_id');
    }

}

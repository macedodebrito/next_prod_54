<?php namespace App\Model;

use Baum\Extensions\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class TechStatusRequests extends Model
{
    
    protected $table = 'tech_status_requests';
    
    protected $fillable = [
        'tech_id',
        'code',
        'name',
        'value',
        'phc_available',
        'total',
        'last_update',
    ];       
}

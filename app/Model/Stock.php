<?php namespace App\Model;

use Baum\Extensions\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
//    use SoftDeletes;

    /**
     * @var array
     */
    
    protected $table = 'stock_users';
    
    protected $fillable = [
        'request_id',
        'user_id',
        'value',
        'obs'
    ];

    public function products()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function request_users()
    {
        return $this->belongsTo(RequestUser::class, 'request_id');
    } 
        
}

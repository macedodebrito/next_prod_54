<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WorkAbsences extends Model
{

    /**
     * @var array
     */
    protected $table = 'work_absences';
    protected $fillable = [
        'user_id',
        'title',
        'start_at',
        'end_at',
        'obs'
    ];   

    public function users()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }
}

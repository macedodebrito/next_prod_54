<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaleteLevantamento extends Model
{
    /**
     * @var array
     */
    protected $table = 'paletes_levantamento';  
}

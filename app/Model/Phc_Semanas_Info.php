<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Phc_Semanas_Info extends Model {

    /**
     * @var array
     */
    protected $table = 'phc_semanas_info';
    protected $fillable = [
        'pp',
        'new_date',
        'locked',
        'auth_id',
        'auth_id_date',
    ];
}


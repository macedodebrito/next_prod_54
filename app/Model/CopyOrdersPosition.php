<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CopyOrdersPosition extends Model {

   
    /**
     * @var array
     */
    protected $table = 'copy_order_position';
   
}

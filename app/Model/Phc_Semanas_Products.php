<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Phc_Semanas_Products extends Model {

    /**
     * @var array
     */
    protected $table = 'phc_semanas_products';
    protected $fillable = [
        'pp',
        'product_name',
        'quantity',
    ];
}


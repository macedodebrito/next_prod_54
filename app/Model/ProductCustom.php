<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class ProductCustom extends Model {

    /**
     * @var array
     */
    protected $table = 'products_custom';

    public function custom_nxt() {
        return $this->belongsTo(\App\Model\ProductCustomNxt::class, 'custom_id', 'id');
    }
}
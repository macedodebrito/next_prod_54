<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Colors extends Model {
    public $timestamps = false;
    /**
     * @var array
     */
    protected $table = 'products_custom_nxt';

    protected $fillable = [
        'name_cor',
        'name',
        'hex_color',
        'color_code'
    ];

    public function products()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}


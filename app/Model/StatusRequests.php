<?php namespace App\Model;

use Baum\Extensions\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class StatusRequests extends Model
{
    
    protected $table = 'status_requests';
    
    protected $fillable = [
        'product_id',
        'code',
        'name',
        'value',
        'phc_available',
        'total',
        'last_update',
    ];       
}

<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ComsumablesOrdersItems extends Model
{

    /**
     * @var array
     */
    protected $table = 'comsumables_orders_items';
    protected $fillable = [
        'comsumables_order_id',
        'comsumables_id',
        'value',
    ];


}

<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'mini_code'
    ];

    public function series()
    {
        return $this->belongsTo(\App\Model\Serie::class, 'serie_id');
    }

    public function requests()
    {
        return $this->hasMany(\App\Model\Request::class);
    }
    
    public function request_users()
    {
        return $this->hasMany(\App\Model\RequestUser::class);
    }    

    public function request_custom()
    {
        return $this->hasMany(\App\Model\ProductCustom::class);
    }        
}

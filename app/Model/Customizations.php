<?php

namespace App\Model;

use Baum\Extensions\Eloquent\Model;

class Customizations extends Model {

    /**
     * @var array
     */
    protected $table = 'customizations';
    protected $fillable = [
        'name',
        'value',
        'color_wood',
        'color_steel',
        'custom',
        'montagem',
        'serralharia',
        'tecnica',
    ];
}


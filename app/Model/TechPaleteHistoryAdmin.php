<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TechPaleteHistoryAdmin extends Model {

    /**
     * @var array
     */
    protected $table = 'tech_palete';
    protected $fillable = [
        'tech_week_id',
        'store_id',
        'store_obs',
        'stored_date',
        'auth_id',
        'auth_obs',
        'authed_date',
    ];
    
    public function requests() {
        return $this->hasMany(TechRequests::class);
    }

    public function products() {
        return $this->belongsTo(Tech::class, 'id');
    }

    public function users() {
        return $this->belongsTo(\App\User::class, 'id');
    }

    public function tech_week() {
        return $this->belongsTo(TechWeek::class, 'tech_week_id');
    }
    
}

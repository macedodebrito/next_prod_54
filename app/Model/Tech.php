<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tech extends Model
{

    /**
     * @var array
     */
    protected $table = 'tech';
    protected $fillable = [
        'code',
        'name',
        'mini_code'
    ];

    public function requests()
    {
        return $this->hasMany(\App\Model\TechRequests::class);
    }
    
    public function request_users()
    {
        return $this->hasMany(\App\Model\TechRequestsUser::class);
    }    
}

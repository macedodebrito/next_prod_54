<?php namespace App\Model;

use Baum\Extensions\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class LocksmithStatusRequests extends Model
{
    
    protected $table = 'locksmith_status_requests';
    
    protected $fillable = [
        'locksmith_id',
        'code',
        'name',
        'value',
        'phc_available',
        'total',
        'last_update',
    ];       
}

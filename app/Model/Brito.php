<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brito extends Model
{
    use SoftDeletes;
    protected $table = "products";
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at'
    ];

    public function series()
    {
        return $this->belongsTo(Serie::class, 'serie_id');
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }
    
    public function request_users()
    {
        return $this->hasMany(RequestUser::class);
    }    
}

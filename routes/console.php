<?php

use Illuminate\Foundation\Inspiring;

/*
  |--------------------------------------------------------------------------
  | Console Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of your Closure based console
  | commands. Each Closure is bound to a command instance allowing a
  | simple approach to interacting with each command's IO methods.
  |
 */

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('check_updates', function () {
    print_r(App\Http\Controllers\HomeController::update_nxt());
})->describe('check NXT');

Artisan::command('update_products {section}', function ($section) {
    if (strtolower($section) === "fix") {
        DB::table('check_config')->where('id', 1)->update(['updating' => 0]);
    }
    $CHECK = DB::table('check_config')->first();
    if ($CHECK->updating == 0) {
        print_r("(UPDATING) Accepted...\n");
        DB::table('check_config')->where('id', 1)->update(['updating' => 1]);

        $start_date = Carbon\Carbon::now();
        $start_date_1 = Carbon\Carbon::now();
        $start_date_2 = Carbon\Carbon::now();
        $start_date_3 = Carbon\Carbon::now();
        $start_date_4 = Carbon\Carbon::now();
        $start_date_5 = Carbon\Carbon::now();
        $start_date_6 = Carbon\Carbon::now();

        print_r("copy Orders position...\n\n");
        DB::statement("TRUNCATE copy_order_position");

        $ITEM_ORDER = 0;
        if ($section != "fix") {
            $order_items = \App\Model\Sum_Orders::get();
            foreach ($order_items as $item) {
                if ($section != "order") {
                    //$ITEM_ORDER = $item->order;
                    $ITEM_ORDER = 0;
                }
                \App\Model\CopyOrdersPosition::insert(['id' => null, 'order_id' => $item->order_id, 'section_type' => 'montagem', 'order_position' => $ITEM_ORDER]);
            }
        } else {
            $order_items = \App\Model\RequestWeek::get();
            foreach ($order_items as $item) {
                \App\Model\CopyOrdersPosition::insert(['id' => null, 'order_id' => $item->id, 'section_type' => 'montagem', 'order_position' => $ITEM_ORDER]);
            }
        }

        print_r("updating all products...\n\n");

        print_r("(STEP 1 - " . Carbon\Carbon::now()->diff($start_date_1)->format('%I:%S') . ") Truncating...\n");
        DB::statement("TRUNCATE sum_mount");
//    DB::statement("DELETE FROM sum_mount where type='mount'");
        DB::statement("TRUNCATE sum_orders");

        print_r("(STEP 2 - " . Carbon\Carbon::now()->diff($start_date_2)->format('%I:%S') . ") inserting PP...\n");
        // PP
        $SQL = DB::table('requests')
                ->select(\DB::raw('requests.id as id, products.id as product_id, products.name as name, requests.value as pp, requests.custom_id as custom_id'))
                ->join('products', 'products.id', '=', 'requests.product_id')
                ->whereNull('requests.deleted_at')
                ->get();

        foreach ($SQL as $product) {
            $cnc = 0;
            $prep = 0;
            $carpintaria = 0;
            $acabamento = 0;
            $pintura = 0;
            $montagem = 0;
            $encomendas = 0;

            // PP
            $JOIN = DB::table('requests')
                    ->where('product_id', $product->product_id)
                    ->where('custom_id', $product->custom_id)
                    ->whereNull('deleted_at')
                    ->sum('value');
            $sum_pp = $JOIN;

            // CNC
            $JOIN = DB::table('request_users')
                    ->select(\DB::raw('SUM(request_users.value) as cnc'))
                    ->groupBy('request_users.request_id')
                    ->where("request_users.request_id", $product->id)
                    ->where("request_users.published", 1)
                    ->orderBy('request_users.created_at', 'desc')
                    ->first();
            if (count((array) $JOIN) > 0) {
                $cnc = $JOIN->cnc;
            }

            // PREPARAÇÃO
            /* $JOIN = DB::table('prep_users')
                    ->select(\DB::raw('SUM(request_users.value) as prep'))
                    ->groupBy('request_users.request_id')
                    ->where("request_users.request_id", $product->id)
                    ->where("request_users.prep", 0)
                    ->orderBy('request_users.created_at', 'desc')
                    ->first(); */
            $JOIN = DB::table('prep_users')
                ->select(\DB::raw('SUM(prep_users.value) as prep'))
                ->groupBy('prep_users.request_id')
                ->where("prep_users.request_id", $product->id)
                ->where("prep_users.published", 1)
                ->orderBy('prep_users.request_id', 'asc')
                ->first();

            if (count((array) $JOIN) > 0) {
                $prep = $JOIN->prep;
            }


            // CARPINTARIA
            $JOIN = DB::table('wood_users')
                    ->select(\DB::raw('SUM(wood_users.value) as carpintaria'))
                    ->groupBy('wood_users.request_id')
                    ->where("wood_users.request_id", $product->id)
                    ->where("wood_users.published", 1)
                    ->orderBy('wood_users.request_id', 'asc')
                    ->first();

            if (count((array) $JOIN) > 0) {
                $carpintaria = $JOIN->carpintaria;
            }

            // ACABAMENTO
            $JOIN = DB::table('stock_users')
                    ->select(\DB::raw('SUM(stock_users.value) as acabamento'))
                    ->groupBy('stock_users.request_id')
                    ->where("stock_users.request_id", $product->id)
                    ->where("stock_users.published", 1)
                    ->orderBy('stock_users.request_id', 'asc')
                    ->first();

            if (count((array) $JOIN) > 0) {
                $acabamento = $JOIN->acabamento;
            }

            // PINTURA
            $JOIN = DB::table('paint_users')
                    ->select(\DB::raw('SUM(paint_users.value) as pintura'))
                    ->groupBy('paint_users.request_id')
                    ->where("paint_users.request_id", $product->id)
                    ->where("paint_users.published", 1)
                    ->orderBy('paint_users.request_id', 'asc')
                    ->first();

            if (count((array) $JOIN) > 0) {
                $pintura = $JOIN->pintura;
            }

            // MONTAGEM
            $JOIN = DB::table('mount_users')
                    ->select(\DB::raw('SUM(mount_users.value) as montagem'))
                    ->groupBy('mount_users.request_id')
                    ->where("mount_users.request_id", $product->id)
                    ->where("mount_users.published", ">", -1)
                    ->orderBy('mount_users.request_id', 'asc')
                    ->first();

            if (count((array) $JOIN) > 0) {
                $montagem = $JOIN->montagem;
            }

            // INSERT
            $custom_sql = \App\Model\ProductCustomNxt::where('id', $product->custom_id)->first();
            \App\Model\Sum_Mount::insert([
                'id' => null,
                'request_id' => $product->id,
                'product_id' => $product->product_id,
                'color_name' => $custom_sql->name_cor,
                'color_hex' => $custom_sql->hex_color,
                'color_hex_bg' => $custom_sql->hex_color_bg,
                'name' => $product->name,
                'type' => "mount",
                'pp' => $product->pp,
                'cnc' => $cnc,
                'prep' => $prep,
                'carpintaria' => $carpintaria,
                'acabamento' => $acabamento,
                'pintura' => $pintura,
                'montagem' => $montagem,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'request_amount' => $sum_pp,
            ]);
        }

        print_r("(STEP 3 - " . Carbon\Carbon::now()->diff($start_date_3)->format('%I:%S') . ") Inserting orders...\n");
        // ENCOMENDAS (CONTAS CORES OK)
        $SQL1 = DB::table('products')
                ->select(\DB::raw('products.id as product_id, products.name as name, SUM(request_week.montagem) as encomendas, DATE_FORMAT(request_week.end_at, "%Y-%m-%d") as mount_date_simple, copy_order_position.order_position as copy_position, request_week.custom_id as custom_id'))
                ->join('request_week', 'request_week.product_id', '=', 'products.id')
                ->join('copy_order_position', 'copy_order_position.order_id', '=', 'request_week.id')
                //->groupBy('request_week.id')
                ->groupBy('products.id')
                ->groupBy('request_week.custom_id')
                //->orderBy('copy_position', 'desc')
                ->orderBy('mount_date_simple', 'asc')
                ->get();

        foreach ($SQL1 as $product) {
            // INSERT
            $custom_sql = \App\Model\ProductCustomNxt::where('id', $product->custom_id)->first();
            \App\Model\Sum_Mount::insert([
                'id' => null,
                'product_id' => $product->product_id,
                'color_name' => $custom_sql->name_cor,
                'color_hex' => $custom_sql->hex_color,
                'color_hex_bg' => $custom_sql->hex_color_bg,
                'name' => $product->name,
                'type' => "mount",
                'encomendas' => $product->encomendas,
            ]);
        }

        print_r("(STEP 4 - " . Carbon\Carbon::now()->diff($start_date_4)->format('%I:%S') . ") Calculating 1/2...\n");
        // AMOUNT (CONTAS CORES OK)
        $SQL2 = DB::table('sum_mount')
                ->select(\DB::raw(''
                        . 'sum_mount.product_id as product_id, '
                        . 'sum_mount.color_name as color_name, '
                        . 'SUM(sum_mount.cnc) as cnc_amount, '
                        . 'SUM(sum_mount.prep) as prep_amount, '
                        . 'SUM(sum_mount.carpintaria) as wood_amount, '
                        . 'SUM(sum_mount.acabamento) as woodfinishing_amount, '
                        . 'SUM(sum_mount.pintura) as paint_amount, '
                        . 'SUM(sum_mount.montagem) as mount_amount'
                        . ''))
                ->groupBy('sum_mount.product_id')
                ->groupBy('sum_mount.color_name')
                ->where('type', 'mount')
                ->get();
        foreach ($SQL2 as $amount) {
            //print_r("('product_id', $amount->product_id)->where('color_name', $amount->color_name)->update(['cnc_amount' => $amount->cnc_amount, 'prep_amount' => $amount->prep_amount, 'wood_amount' => $amount->wood_amount, 'woodfinishing_amount' => $amount->woodfinishing_amount, 'paint_amount' => $amount->paint_amount, 'mount_amount' => $amount->mount_amount])\n");
            \App\Model\Sum_Mount::where('product_id', $amount->product_id)->where('color_name', $amount->color_name)->update(['cnc_amount' => $amount->cnc_amount, 'prep_amount' => $amount->prep_amount, 'wood_amount' => $amount->wood_amount, 'woodfinishing_amount' => $amount->woodfinishing_amount, 'paint_amount' => $amount->paint_amount, 'mount_amount' => $amount->mount_amount]);
        }
        // ENCOMENDAS - MORE
        $TEMP = [];
        $CNC_TEMP = [];
        $PREP_TEMP = [];
        $WOOD_TEMP = [];
        $WOODFINISHING_TEMP = [];
        $PAINT_TEMP = [];
        global $TEMP;
        global $CNC_TEMP;
        global $PREP_TEMP;
        global $WOOD_TEMP;
        global $WOODFINISHING_TEMP;
        global $PAINT_TEMP;

        print_r("(STEP 5 - " . Carbon\Carbon::now()->diff($start_date_5)->format('%I:%S') . ") Calculating 2/2...\n");
        //DB::enableQueryLog();
        $SQL2 = DB::table('request_week')
                ->select(\DB::raw(''
                        . 'request_week.id as id, '
                        . 'request_week.product_id as product_id, '
                        . 'request_week.client as client, '
                        . 'request_week.custom_id as custom_id, '
                        . 'products_custom_nxt.name_cor as cor, '
                        
                        . 'request_week.end_wood_at as cnc_date, '
                        . 'DATE_FORMAT(request_week.end_wood_at, "%Y-%m-%d") as cnc_date_simple, '
                        . 'request_week.end_wood_at as wood_date, '
                        . 'DATE_FORMAT(request_week.end_wood_at, "%Y-%m-%d") as wood_date_simple, '
                        . 'request_week.end_woodfinishing_at as woodfinishing_date, '
                        . 'DATE_FORMAT(request_week.end_woodfinishing_at, "%Y-%m-%d") as woodfinishing_date_simple, '
                        . 'request_week.end_paint_at as paint_date, '
                        . 'DATE_FORMAT(request_week.end_paint_at, "%Y-%m-%d") as paint_date_simple, '
                        . 'request_week.end_at as mount_date, '
                        . 'DATE_FORMAT(request_week.end_at, "%Y-%m-%d") as mount_date_simple, '
                        
                        . 'request_week.carpintaria as cnc_quantity, '
                        . 'request_week.carpintaria as prep_quantity, '
                        . 'request_week.carpintaria as wood_quantity, '
                        . 'request_week.carpintaria as woodfinishing_quantity, '
                        . 'request_week.pintura as paint_quantity, '
                        . 'request_week.montagem as quantity, '
                        
                        . 'sum_mount.request_amount as request, '
                        . 'sum_mount.cnc_amount as cnc_amount, '
                        . 'sum_mount.prep_amount as prep_amount, '
                        . 'sum_mount.wood_amount as wood_amount, '
                        . 'sum_mount.woodfinishing_amount as woodfinishing_amount, '
                        . 'sum_mount.paint_amount as paint_amount, '                        
                        . 'sum_mount.mount_amount as mount_amount, '
                        
                        . 'products.name as name, '
                        . 'products.points as points, '
                        . 'request_week.order as order_position, '
                        . 'copy_order_position.order_position as copy_position'
                        ))
                ->join('products', 'request_week.product_id', '=', 'products.id')
                ->join('sum_mount', 'sum_mount.product_id', '=', 'products.id')
                ->join('copy_order_position', 'copy_order_position.order_id', '=', 'request_week.id')
                ->join('products_custom_nxt', 'request_week.custom_id', '=', 'products_custom_nxt.id')

                ->where('sum_mount.type', 'mount')
                //->where('sum_mount.color_name', 'products_custom_nxt.name_cor')
                ->whereColumn('sum_mount.color_name', 'products_custom_nxt.name_cor')

                ->groupBy('request_week.id')

                ->orderBy('mount_date_simple', 'asc')                

                ->get();
        //print_r(DB::getQueryLog());
        foreach ($SQL2 as $order) {
            $CALC = 0;
            $CNC_CALC = 0;
            $PREP_CALC = 0;
            $WOOD_CALC = 0;
            $WOODFINISHING_CALC = 0;
            $PAINT_CALC = 0;

            if (!isset($CNC_TEMP["$order->product_id"]["$order->custom_id"])) {                
                $CNC_TEMP["$order->product_id"]["$order->custom_id"] =$order->cnc_amount - $order->cnc_quantity;
            } else {
                $CNC_TEMP["$order->product_id"]["$order->custom_id"] = $CNC_TEMP["$order->product_id"]["$order->custom_id"] - $order->cnc_quantity;
            }
            if ($CNC_TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $CNC_CALC = $CNC_TEMP["$order->product_id"]["$order->custom_id"] + $order->cnc_quantity;
            } else {
                $CNC_CALC = $CNC_TEMP["$order->product_id"]["$order->custom_id"] - $order->cnc_quantity;
            }

            if (!isset($PREP_TEMP["$order->product_id"]["$order->custom_id"])) {
                $PREP_TEMP["$order->product_id"]["$order->custom_id"] = $order->prep_amount - $order->prep_quantity;
            } else {
                $PREP_TEMP["$order->product_id"]["$order->custom_id"] = $PREP_TEMP["$order->product_id"]["$order->custom_id"] - $order->prep_quantity;
            }
            if ($PREP_TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $PREP_CALC = $PREP_TEMP["$order->product_id"]["$order->custom_id"] + $order->prep_quantity;
            } else {
                $PREP_CALC = $PREP_TEMP["$order->product_id"]["$order->custom_id"] - $order->prep_quantity;
            }

            if (!isset($WOOD_TEMP["$order->product_id"]["$order->custom_id"])) {
                $WOOD_TEMP["$order->product_id"]["$order->custom_id"] = $order->wood_amount - $order->wood_quantity;
            } else {
                $WOOD_TEMP["$order->product_id"]["$order->custom_id"] = $WOOD_TEMP["$order->product_id"]["$order->custom_id"] - $order->wood_quantity;
            }
            if ($WOOD_TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $WOOD_CALC = $WOOD_TEMP["$order->product_id"]["$order->custom_id"] + $order->wood_quantity;
            } else {
                $WOOD_CALC = $WOOD_TEMP["$order->product_id"]["$order->custom_id"] - $order->wood_quantity;
            }

            if (!isset($WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"])) {
                $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] = $order->woodfinishing_amount - $order->woodfinishing_quantity;
            } else {
                $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] = $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] - $order->woodfinishing_quantity;
            }
            if ($WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $WOODFINISHING_CALC = $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] + $order->woodfinishing_quantity;
            } else {
                $WOODFINISHING_CALC = $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"] - $order->woodfinishing_quantity;
            }

            if (!isset($PAINT_TEMP["$order->product_id"]["$order->custom_id"])) {
                $PAINT_TEMP["$order->product_id"]["$order->custom_id"] = $order->paint_amount - $order->paint_quantity;
            } else {
                $PAINT_TEMP["$order->product_id"]["$order->custom_id"] = $PAINT_TEMP["$order->product_id"]["$order->custom_id"] - $order->paint_quantity;
            }
            if ($PAINT_TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $PAINT_CALC = $PAINT_TEMP["$order->product_id"]["$order->custom_id"] + $order->paint_quantity;
            } else {
                $PAINT_CALC = $PAINT_TEMP["$order->product_id"]["$order->custom_id"] - $order->paint_quantity;
            }
            
            if (!isset($TEMP["$order->product_id"]["$order->custom_id"])) {
                $TEMP["$order->product_id"]["$order->custom_id"] = $order->mount_amount - $order->quantity;
            } else {
                $TEMP["$order->product_id"]["$order->custom_id"] = $TEMP["$order->product_id"]["$order->custom_id"] - $order->quantity;
            }
            if ($TEMP["$order->product_id"]["$order->custom_id"] < 0) {
                $CALC = $TEMP["$order->product_id"]["$order->custom_id"] + $order->quantity;
            } else {
                $CALC = $TEMP["$order->product_id"]["$order->custom_id"] - $order->quantity;
            }

            if ($TEMP["$order->product_id"]["$order->custom_id"] >= 0) {
                //$ORDER = 999;
                $ORDER = 0;
            } else {
                $ORDER = $order->copy_position;
            }


            /* if (!isset($CNC_TEMP["$order->product_id"])) {
                //$CNC_TEMP["$order->product_id"] = $order->prep_amount + $order->cnc_amount - $order->cnc_quantity;
                $CNC_TEMP["$order->product_id"] =$order->cnc_amount - $order->cnc_quantity;
            } else {
                $CNC_TEMP["$order->product_id"] = $CNC_TEMP["$order->product_id"] - $order->cnc_quantity;
            }
            if ($CNC_TEMP["$order->product_id"] < 0) {
                $CNC_CALC = $CNC_TEMP["$order->product_id"] + $order->cnc_quantity;
            } else {
                $CNC_CALC = $CNC_TEMP["$order->product_id"] - $order->cnc_quantity;
            }

            if (!isset($PREP_TEMP["$order->product_id"])) {
                $PREP_TEMP["$order->product_id"] = $order->prep_amount - $order->prep_quantity;
            } else {
                $PREP_TEMP["$order->product_id"] = $PREP_TEMP["$order->product_id"] - $order->prep_quantity;
            }
            if ($PREP_TEMP["$order->product_id"] < 0) {
                $PREP_CALC = $PREP_TEMP["$order->product_id"] + $order->prep_quantity;
            } else {
                $PREP_CALC = $PREP_TEMP["$order->product_id"] - $order->prep_quantity;
            }

            if (!isset($WOOD_TEMP["$order->product_id"])) {
                $WOOD_TEMP["$order->product_id"] = $order->wood_amount - $order->wood_quantity;
            } else {
                $WOOD_TEMP["$order->product_id"] = $WOOD_TEMP["$order->product_id"] - $order->wood_quantity;
            }
            if ($WOOD_TEMP["$order->product_id"] < 0) {
                $WOOD_CALC = $WOOD_TEMP["$order->product_id"] + $order->wood_quantity;
            } else {
                $WOOD_CALC = $WOOD_TEMP["$order->product_id"] - $order->wood_quantity;
            }

            if (!isset($WOODFINISHING_TEMP["$order->product_id"])) {
                $WOODFINISHING_TEMP["$order->product_id"] = $order->woodfinishing_amount - $order->woodfinishing_quantity;
            } else {
                $WOODFINISHING_TEMP["$order->product_id"] = $WOODFINISHING_TEMP["$order->product_id"] - $order->woodfinishing_quantity;
            }
            if ($WOODFINISHING_TEMP["$order->product_id"] < 0) {
                $WOODFINISHING_CALC = $WOODFINISHING_TEMP["$order->product_id"] + $order->woodfinishing_quantity;
            } else {
                $WOODFINISHING_CALC = $WOODFINISHING_TEMP["$order->product_id"] - $order->woodfinishing_quantity;
            }

            if (!isset($PAINT_TEMP["$order->product_id"])) {
                $PAINT_TEMP["$order->product_id"] = $order->paint_amount - $order->paint_quantity;
            } else {
                $PAINT_TEMP["$order->product_id"] = $PAINT_TEMP["$order->product_id"] - $order->paint_quantity;
            }
            if ($PAINT_TEMP["$order->product_id"] < 0) {
                $PAINT_CALC = $PAINT_TEMP["$order->product_id"] + $order->paint_quantity;
            } else {
                $PAINT_CALC = $PAINT_TEMP["$order->product_id"] - $order->paint_quantity;
            }
            
            if (!isset($TEMP["$order->product_id"])) {
                $TEMP["$order->product_id"] = $order->mount_amount - $order->quantity;
            } else {
                $TEMP["$order->product_id"] = $TEMP["$order->product_id"] - $order->quantity;
            }
            if ($TEMP["$order->product_id"] < 0) {
                $CALC = $TEMP["$order->product_id"] + $order->quantity;
            } else {
                $CALC = $TEMP["$order->product_id"] - $order->quantity;
            }

            if ($TEMP["$order->product_id"] >= 0) {
                $ORDER = 0;
            } else {
                $ORDER = $order->copy_position;
            } */

            \App\Model\Sum_Orders::insert([
                'id' => null,
                'order_id' => $order->id,
                'product_id' => $order->product_id,
                'client' => $order->client,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'request' => $order->request,
                //CNC
                'cnc_order_amount' => $order->cnc_quantity,
                'cnc_amount' => $order->cnc_amount,
                'cnc_date' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->cnc_date)->subDays(3)->format('Y-m-d H:i:s'),
                'cnc_date_simple' => \Carbon\Carbon::createFromFormat('Y-m-d', $order->cnc_date_simple)->subDays(3)->format('Y-m-d'),
                'cnc_temp_amount' => $CNC_TEMP["$order->product_id"]["$order->custom_id"],
                //'cnc_temp_amount' => $CNC_TEMP["$order->product_id"],
                'cnc_calc_amount' => $CNC_CALC,
                //PREP
                'prep_order_amount' => $order->prep_quantity,
                'prep_amount' => $order->prep_amount,
                'prep_date' => \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $order->cnc_date)->subDays(3)->format('Y-m-d H:i:s'),
                'prep_date_simple' => \Carbon\Carbon::createFromFormat('Y-m-d', $order->cnc_date_simple)->subDays(3)->format('Y-m-d'),
                'prep_temp_amount' => $PREP_TEMP["$order->product_id"]["$order->custom_id"],
                //'prep_temp_amount' => $PREP_TEMP["$order->product_id"],
                'prep_calc_amount' => $PREP_CALC,
                //WOOD
                'wood_order_amount' => $order->wood_quantity,
                'wood_amount' => $order->wood_amount,
                'wood_date' => $order->wood_date,
                'wood_date_simple' => $order->wood_date_simple,
                'wood_temp_amount' => $WOOD_TEMP["$order->product_id"]["$order->custom_id"],
                //'wood_temp_amount' => $WOOD_TEMP["$order->product_id"],
                'wood_calc_amount' => $WOOD_CALC,
                //WOODFINISHING
                'woodfinishing_order_amount' => $order->woodfinishing_quantity,
                'woodfinishing_amount' => $order->woodfinishing_amount,
                'woodfinishing_date' => $order->woodfinishing_date,
                'woodfinishing_date_simple' => $order->woodfinishing_date_simple,
                'woodfinishing_temp_amount' => $WOODFINISHING_TEMP["$order->product_id"]["$order->custom_id"],
                //'woodfinishing_temp_amount' => $WOODFINISHING_TEMP["$order->product_id"],
                'woodfinishing_calc_amount' => $WOODFINISHING_CALC,
                ////PAINT
                'paint_order_amount' => $order->paint_quantity,
                'paint_amount' => $order->paint_amount,
                'paint_date' => $order->paint_date,
                'paint_date_simple' => $order->paint_date_simple,
                'paint_temp_amount' => $PAINT_TEMP["$order->product_id"]["$order->custom_id"],
                //'paint_temp_amount' => $PAINT_TEMP["$order->product_id"],
                'paint_calc_amount' => $PAINT_CALC,
                //MOUNT
                'order_amount' => $order->quantity,
                'mount_amount' => $order->mount_amount,
                'mount_date' => $order->mount_date,
                'mount_date_simple' => $order->mount_date_simple,
                'temp_amount' => $TEMP["$order->product_id"]["$order->custom_id"],
                //'temp_amount' => $TEMP["$order->product_id"],
                'calc_amount' => $CALC,
                'product_name' => $order->name,
                'points' => $order->points,
                'order' => $ORDER
            ]);
        }
        print_r("(STEP 6 - " . Carbon\Carbon::now()->diff($start_date_6)->format('%I:%S') . ") Updating Dates and Values...\n");

        \App\Model\Sum_Dates::where('section', 'pp')->update(['value' => DB::table('requests')->whereNull('deleted_at')->sum('value')]);
        \App\Model\Sum_Dates::where('section', 'pp_orders')->update(['value' => DB::table('request_week')->sum('montagem')]);
        \App\Model\Sum_Dates::where('section', 'cnc')->update(['updated_at' => \App\Model\RequestUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'prep')->update(['updated_at' => \App\Model\PrepUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'carpintaria')->update(['updated_at' => \App\Model\WoodUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'acabamento')->update(['updated_at' => \App\Model\StockUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'pintura')->update(['updated_at' => \App\Model\PaintUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'montagem')->update(['updated_at' => \App\Model\MountUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'serralharia')->update(['updated_at' => \App\Model\LocksmithRequestsUser::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'redes')->update(['updated_at' => \App\Model\WIRES_History::orderBy('created_at', 'DESC')->first()->created_at]);
        \App\Model\Sum_Dates::where('section', 'tecnica')->update(['updated_at' => \App\Model\TechRequestsUser::orderBy('created_at', 'DESC')->first()->created_at]);

        print_r("(UPDATING) Reseting...\n");
        $end_date = Carbon\Carbon::now();
        DB::table('check_config')
                ->where('id', 1)
                ->update([
                    'updating' => 0,
                    'pp' => DB::table('requests')->whereNull('deleted_at')->sum('value'),
                    'pp_orders' => DB::table('request_week')->sum('montagem'),
                    'cnc' => \App\Model\RequestUser::sum('value'),
                    'prep' => \App\Model\PrepUser::sum('value'),
                    'carpintaria' => \App\Model\WoodUser::sum('value'),
                    'acabamento' => \App\Model\StockUser::sum('value'),
                    'pintura' => \App\Model\PaintUser::sum('value'),
                    'montagem' => \App\Model\MountUser::sum('value'),
                    'queue_time' => $end_date->diff($start_date)->format('%I:%S')
        ]);
        
        
        print_r("...DONE!!! " . $end_date->diff($start_date)->format('%I:%S') . "\n");
        /*
        print_r("[»] Sending email...\n");
        $data_email['time'] = $end_date->diff($start_date)->format('%I:%S');
        $data_email['section'] = $section;
        Mail::send('emails.product_updates', $data_email, function($message) {
            $message
                    ->from('software.prod@NEXT-proaudio.com')
                    ->to('brito.nextproaudio@gmail.com')
                    ->subject('NXT :: update_products');
        });
        */
    } else {
        print_r("(UPDATING) Already being updated (no duplicates)...\n");
    }
})->describe('Actualizar: Montagem > Produtos');

<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

//Route::group(['middleware' => 'web'], function () {
//    // Authentication Routes...
//    Route::get('login', 'Auth\AuthController@showLoginForm');
//    Route::post('login', 'Auth\AuthController@login');
//    Route::get('logout', 'Auth\AuthController@logout');
//
//    // Route::get('/home', 'HomeController@index');
//});


Route::get('/', function () {
    return view('welcome');
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    //// V2
    //
    // Global
    Route::post('v2/checkUser', 'V2\GlobalController@checkUser')->name('v2_checkUser')->middleware('auth');
    Route::post('v2/checkUserInfo', 'V2\GlobalController@checkUserInfo')->name('v2_checkUserInfo')->middleware('auth');
    Route::post('v2/setUserClock', 'V2\GlobalController@setUserClock')->name('v2_setUserClock')->middleware('auth');
    // Dashboard
    Route::get('v2', 'V2\DashboardController@index')->name('v2_dashboard')->middleware('auth');
    // CNC
    Route::get('v2/cnc', 'V2\CncController@index')->name('v2_cnc')->middleware('auth');
    Route::get('v2/cnc/getPP', 'V2\CncController@getPP')->name('v2_cncGetPP')->middleware('auth');
    Route::post('v2/cnc/getChecked', 'V2\CncController@getChecked')->name('v2_cncGetChecked')->middleware('auth');
    Route::post('v2/cnc/sendOutput', 'V2\CncController@sendOutput')->name('v2_cncSendOutput')->middleware('auth');
    
    //
    //// V2


    //// V1
    //
    Route::get('login', 'Auth\AuthController@showLoginForm');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');


//    Route::get('/home', 'HomeController@index');
    Route::get('/home', ['as' => 'home', function () {
            return Redirect::to('/admin');
        }]);
    Route::get('/', ['as' => 'home', function () {
            return Redirect::to('/admin');
        }]);

//    Route::get('/admin/requests/cnc_history', ['as' => 'admin.requests.cnc_history', function () {
//            return Redirect::to('/admin/cnc');
//        }]);

//    Route::get('/admin/wood/history', ['as' => 'admin.requests.cnc_history', function () {
//            return Redirect::to('/admin/wood');
//        }]);

//    Route::get('/admin/woodfinishing/history', ['as' => 'admin.requests.woodfinishing_history', function () {
//            return Redirect::to('/admin/woodfinishing');
//        }]);
        
//    Route::get('/admin/paint/history', ['as' => 'admin.requests.paint_history', function () {
//            return Redirect::to('/admin/paint');
//        }]);

    Route::get('/admin/mount/history_picker', ['as' => 'admin.requests.mount_history_picker', function () {
            return Redirect::to('/admin/mount/history');
        }]);

//    Route::get('/admin/clock_days', ['as' => 'admin.clock.days', function () {
//            
//            return Redirect::to('/admin/clocks');
//        }]);

//    Route::post('/admin/request_weeks/create', ['as' => 'admin.requests_weeks.create', function () {
//            return Redirect::to('/admin/request_weeks/create');
//        }]);
});

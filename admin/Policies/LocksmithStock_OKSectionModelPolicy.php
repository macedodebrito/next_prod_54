<?php

namespace Admin\Policies;

use App\User;
use App\Model\MOUNT_history_Stock_OK;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocksmithStock_OKSectionModelPolicy {

    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->isSuperAdmin()) {
            return true;
        }
        if ($user->isStock()) {
            return true;
        }        
    }

    public function display(User $user) {
        return true;
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user) {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
//        else if ($user->isManager()) {
//             return true;
//        }
//        else if ($user->isStock()) {
//             return true;
//        } 
        return false;
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user) {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user) {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user) {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
//        else if ($user->isManager()) {
//             return true;
//        }
        return false;
    }

}

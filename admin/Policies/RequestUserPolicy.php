<?php

namespace App\Policies;

use App\User;
use App\Model\RequestUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequestPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function display(User $user, RequestUser $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user, RequestUser $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user, RequestUser $item)
    {
        return $user->isSuperAdmin();
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user, RequestUser $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user, RequestUser $item)
    {
        return $user->isSuperAdmin();
    }
}

<?php

namespace Admin\Policies;

use App\User;
//use App\Model\Clock;
use Illuminate\Auth\Access\HandlesAuthorization;

class Clock_UserSectionModelPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }       
        else if ($user->isRH()) {
            return true;
        }        
    }

    public function display(User $user)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user)
    {
        return $user->isSuperAdmin();
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->isSuperAdmin();
    }
}

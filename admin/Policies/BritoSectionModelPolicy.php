<?php

namespace Admin\Policies;

use Admin\Http\Sections\Brito;
use App\User;
use App\Model\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class BritoSectionModelPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function display(User $user)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user, Product $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user, Product $item)
    {
        return $user->isSuperAdmin();
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user, Product $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user, Product $item)
    {
        return $user->isSuperAdmin();
    }
}

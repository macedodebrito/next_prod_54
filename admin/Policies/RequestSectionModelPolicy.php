<?php

namespace Admin\Policies;

//
use Admin\Http\Sections\Request;
//
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequestSectionModelPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    public function display(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }
}

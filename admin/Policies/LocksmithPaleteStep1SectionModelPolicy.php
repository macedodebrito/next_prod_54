<?php

namespace Admin\Policies;

use App\User;
use App\Model\Palete;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocksmithPaleteStep1SectionModelPolicy {

    use HandlesAuthorization;

    public function before(User $user, $ability) {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
            return true;
        }          
        else if ($user->isStock()) {
            return true;
        }
        else if ($user->isArmazem()) {
            return true;
        }       
        else if ($user->isTech()) {
            return true;
        } 
        else if ($user->isSerralharia()) {
            return true;
        }   
        else if ($user->isSerigrafia()) {
            return true;
        }         
        else {
            return false;
        }
    }

    public function display(User $user) {
        return true;
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user) {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
//        else if ($user->isManager()) {
//             return true;
//        }
//        else if ($user->isStock()) {
//             return true;
//        } 
        return false;
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user) {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user) {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user) {
//        if ($user->isSuperAdmin()) {
//            return true;
//        }
//        else if ($user->isManager()) {
//             return true;
//        }
        return false;
    }

}

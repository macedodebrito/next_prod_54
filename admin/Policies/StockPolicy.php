<?php

namespace App\Policies;

use App\User;
use App\Model\Stock;
use Illuminate\Auth\Access\HandlesAuthorization;

class StockPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        if ($user->isManager()) {
            return true;
        }
        if ($user->isArmazem()) {
            return true;
        }         
        if ($user->isStock()) {
            return true;
        }        
        if ($user->isMount()) {
            return true;
        }        
    }

    public function display(User $user, Stock $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user, Stock $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user, Stock $item)
    {
        return $user->isSuperAdmin();
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user, Stock $item)
    {
        return $user->isSuperAdmin();
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user, Stock $item)
    {
        return $user->isSuperAdmin();
    }
}

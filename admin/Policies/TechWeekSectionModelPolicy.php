<?php

namespace Admin\Policies;

use Admin\Http\Sections\RequestWeek;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TechWeekSectionModelPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
//        return true;
    }

    public function display(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function create(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function edit(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }
    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function restore(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }

    /**
     * @param User    $user
     * @param Contact $contact
     *
     * @return bool
     */
    public function delete(User $user)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
        else if ($user->isManager()) {
             return true;
        }
    }
}

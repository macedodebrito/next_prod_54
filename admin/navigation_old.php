<?php

//CNC BADGE
//$total = DB::table('requests')->sum('value');
//if ($total > 0) {
//    $maquinados = DB::table('request_users')->sum('value');
//    $por_maquinar = $total - $maquinados;
//    $percent_1 = round(100 * $por_maquinar / $total, 2);
//    $badge_cnc = 100-$percent_1;
//} else {
//    $badge_cnc = 0;
//}
//$badge_cnc = "$badge_cnc%";
//// 
// $badge_cnc->setHtmlAttributes('class', 'bg-red')

use SleepingOwl\Admin\Navigation\Page;

$navigation->setFromArray([
//    [
//        'title' => 'CNC2',
//        'icon' => 'fa fa-cogs',
//        'url' => '/admin/cnc2',
//        'priority' => 1,
////        'badge' => $badge_cnc,
//    ],    
    [
        'title' => 'CNC',
        'icon' => 'fa fa-gamepad',
        'priority' => 10,
        'htmlattributes' => ['style' => 'background-color: #00c0ef;'],
        'pages' => [
                    (new Page(\App\Model\WOODFINISHING_history::class))
                    ->setTitle('CNC')
                    ->setIcon('fa fa-gamepad')
                    ->setUrl('/admin/cnc')
                    ->setHtmlAttributes(['style' => 'background-color: #00c0ef;'])
                    ->setPriority(10),
                    (new Page(\App\Model\BLACKFINISHING_History::class))
                    ->setTitle('PREPARAÇÃO')
                    ->setIcon('fa fa-diamond')
                    ->setUrl('/admin/prep')
                    ->setHtmlAttributes(['style' => 'background-color: #000000;'])
                    ->setPriority(20)
        ]
    ],
//    [
//        'title' => 'CNC',
//        'icon' => 'fa fa-cogs',
//        'url' => '/admin/cnc',
//        'priority' => 10,
////        'badge' => $badge_cnc,
//    ],
    (new Page(\App\Model\WOOD_history::class))
            ->setTitle('CARPINTARIA')
            ->setIcon('fa fa-cubes')
            ->setUrl('/admin/wood')
            ->setPriority(20)
            ->setHtmlAttributes(['style' => 'background-color: #dd4b39;']),
//    [
//        'title' => 'CARPINTARIA',
//        'icon' => 'fa fa-cubes',
//        'url' => '/admin/wood',
//        'priority' => 20,
//    ],
    [
        'title' => 'ACABAMENTO',
        'icon' => 'fa fa-diamond',
        'priority' => 30,
        'htmlattributes' => ['style' => 'background-color: #605ca8;'],
        'pages' => [
                    (new Page(\App\Model\WOODFINISHING_history::class))
                    ->setTitle('DA CARPINTARIA')
                    ->setIcon('fa fa-diamond')
                    ->setUrl('/admin/woodfinishing')
                    ->setHtmlAttributes(['style' => 'background-color: #dd4b39;'])
                    ->setPriority(10),
//                    (new Page(\App\Model\BLACKFINISHING_History::class))
//                    ->setTitle('DE PRETO')
//                    ->setIcon('fa fa-diamond')
//                    ->setUrl('/admin/blackfinishing')
//                    ->setHtmlAttributes(['style' => 'background-color: #000000;'])
//                    ->setPriority(20)
        ]
    ],
//    [
//        'title' => 'ACABAMENTO',
//        'icon' => 'fa fa-diamond',
//        'url' => '/admin/woodfinishing',
//        'priority' => 30,
//    ],
    (new Page(\App\Model\PAINT_history::class))
            ->setTitle('PINTURA')
            ->setIcon('fa fa-paint-brush')
            ->setUrl('/admin/paint')
            ->setPriority(40)
            ->setHtmlAttributes(['style' => 'background-color: #00a65a;']),
//    [
//        'title' => 'PINTURA',
//        'icon' => 'fa fa-paint-brush',
//        'url' => '/admin/paint',
//        'priority' => 40,
//    ],
    (new Page(\App\Model\MOUNT_history::class))
            ->setTitle('MONTAGEM')
            ->setIcon('fa fa-sign-language')
            ->setUrl('/admin/mount')
            ->setPriority(50)
            ->setHtmlAttributes(['style' => 'background-color: #FF851B;']),
//    [
//        'title' => 'MONTAGEM',
//        'icon' => 'fa fa-sign-language',
//        'url' => '/admin/mount',
//        'priority' => 50,
//    ],
    (new Page(\App\Model\Locksmith_History::class))
            ->setTitle('SERRALHARIA')
            ->setIcon('fa fa-cogs')
            ->setUrl('/admin/serralharia')
            ->setPriority(51)
            ->setHtmlAttributes(['style' => 'background-color: #AAAAAA;']),
            (new Page(\App\Model\WIRES_History::class))
            ->setTitle('REDES')
            ->setIcon('fa fa-hashtag')
            ->setUrl('/admin/wires')
            ->setPriority(52)
            ->setHtmlAttributes(['style' => 'background-color: #01FF70;']),
            (new Page(\App\Model\Tech::class))
            ->setTitle('Técnica')
            ->setIcon('fa fa-tachometer')
            ->setUrl('/admin/tecnica')
            ->setPriority(53)
            ->setHtmlAttributes(['style' => 'background-color: #D81B60;']),    
            (new Page(\App\Model\Materials::class))
            ->setTitle('Materiais')
            ->setIcon('fa fa-hashtag')
            ->setUrl('/admin/materials')
            ->setPriority(54)
            ->setHtmlAttributes(['style' => 'background-color: #000000;']),
    [
        'title' => 'GESTÃO',
        'icon' => 'fa fa-group',
        'pages' => [
                    (new Page(\App\User::class))
                    ->setIcon('fa fa-user')
                    ->setPriority(0),
                    (new Page(\App\Role::class))
                    ->setIcon('fa fa-group')
                    ->setPriority(10),
                    (new Page(\App\Model\Serie::class))
                    ->setIcon('fa fa-tags')
                    ->setPriority(20),
                    (new Page(\App\Model\Product::class))
                    ->setIcon('fa fa-cube')
                    ->setPriority(30),
                    (new Page(\App\Model\Request::class))
                    ->setIcon('fa fa-check-square-o')
                    ->setPriority(40),
                    (new Page(\App\Model\Distro::class))
                    ->setTitle('Distribuição')
                    ->setIcon('fa fa-list')
                    ->setUrl('/admin/distributors')
                    ->setPriority(50),
            [
                'title' => 'Serralharia',
                'priority' => 41,
                'icon' => 'fa fa-calendar',
                'pages' => [
                            (new Page(\App\Model\Locksmith::class))
                            ->setTitle('Peças')
                            ->setIcon('fa fa-check-square-o')
                            ->setPriority(41),
                            (new Page(\App\Model\LocksmithRequests::class))
                            ->setTitle('Pedidos')
                            ->setIcon('fa fa-check-square-o')
                            ->setPriority(42),
                            (new Page(\App\Model\LocksmithWeek::class))
                            ->setIcon('fa fa-list')
                            ->setPriority(10),
                ],
            ],
            [
                'title' => 'Técnica',
                'priority' => 41,
                'icon' => 'fa fa-calendar',
                'pages' => [
                            (new Page(\App\Model\Tech::class))
                            ->setTitle('Peças')
                            ->setIcon('fa fa-check-square-o')
                            ->setPriority(41),
                            (new Page(\App\Model\TechRequests::class))
                            ->setTitle('Pedidos')
                            ->setIcon('fa fa-check-square-o')
                            ->setPriority(42),
                            (new Page(\App\Model\TechWeek::class))
                            ->setIcon('fa fa-list')
                            ->setPriority(10),
                ],
            ],            
            [
                'title' => 'Enc de Clientes',
                'priority' => 50,
                'icon' => 'fa fa-calendar',
                'pages' => [
                            (new Page(\App\Model\RequestWeek::class))
                            ->setIcon('fa fa-list')
                            ->setPriority(10),
                            (new Page(\App\Model\RequestWeekGroup::class))
                            ->setIcon('fa fa-link')
                            ->setPriority(20),
                            (new Page(\App\Model\TripleWeek::class))
                            ->setIcon('fa fa-link')
                            ->setPriority(30),
                    [
                        'title' => 'Reservas',
                        'icon' => 'fa fa-database',
                        'priority' => 80,
                        'pages' => [
                            (new Page(\App\Model\StatusRequests::class))
                            ->setTitle('Produtos')
                            ->setIcon('fa fa-list')
                            ->setPriority(10),
                            (new Page(\App\Model\LocksmithStatusRequests::class))
                            ->setTitle('Serralharia')
                            ->setIcon('fa fa-list')
                            ->setPriority(20),     
                            (new Page(\App\Model\TechStatusRequests::class))
                            ->setTitle('Técnica')
                            ->setIcon('fa fa-list')
                            ->setPriority(30),                              
                        ]
                    ],
                ],
            ],
//                    (new Page(\App\Model\Deadline::class))
//                    ->setIcon('fa fa-check-square-o')
//                    ->setPriority(60),
//                    (new Page(\App\Model\Stock::class))
//                    ->setIcon('fa fa-list')
//                    ->setHtmlAttributes(['style' => 'background-color: #fff'])
            (new Page(\App\Model\MOUNT_history_Stock::class))
                    ->setTitle('Stock')
                    ->setIcon('fa fa-list')
                    ->setUrl('/admin/stocks_ok')
                    ->setPriority(70),
            [
                'title' => 'Gestão de Paletes',
                'icon' => 'fa fa-database',
                'priority' => 80,
                'pages' => [
                            (new Page(\App\Model\Palete::class))
                            ->setTitle('Armazém')
                            ->setIcon('fa fa-sign-language')
                            ->setUrl('/admin/view_paletes')
                            ->setPriority(10),
                            (new Page(\App\Model\LocksmithPalete::class))
                            ->setTitle('Serralharia')
                            ->setIcon('fa fa-cogs')
                            ->setUrl('/admin/view_paletes_serralharia')
                            ->setPriority(20),
                            (new Page(\App\Model\TechPalete::class))
                            ->setTitle('Técnica')
                            ->setIcon('fa fa-cogs')
                            ->setUrl('/admin/view_paletes_tecnica')
                            ->setPriority(30),                    
                ]
            ],
                    (new Page(\App\Model\Comsumables::class))
                    ->setIcon('fa fa-database')
                    ->setPriority(90),
            [
                'title' => 'Relógio de Ponto',
                'priority' => 100,
                'icon' => 'fa fa-tachometer',
                'pages' => [
//                    [
//                        'title' => 'Lista total',
//                        'icon' => 'fa fa-database',
//                        'url' => '/admin/clock_2s',
//                        'priority' => 10,
//                    ],

                            (new Page(\App\Model\Clock_2::class))
                            ->setTitle('AFCMC')
                            ->setIcon('fa fa-database')
                            ->setUrl('/admin/clock_2s?id=1')
                            ->setPriority(20),
//                    [
//                        'title' => 'AFCMC',
//                        'icon' => 'fa fa-database',
//                        'url' => '/admin/clock_2s?id=1',
//                        'priority' => 20,
//                    ],
                    (new Page(\App\Model\Clock_2::class))
                            ->setTitle('CVA Electrónica')
                            ->setIcon('fa fa-database')
                            ->setUrl('/admin/clock_2s?id=2')
                            ->setPriority(30),
                    [
                        'title' => 'AFCMC',
                        'icon' => 'fa fa-database',
                        'url' => '/admin/clock_2s?id=1',
                        'priority' => 30,
                    ],
//                    [
//                        'title' => 'EXTRA',
//                        'icon' => 'fa fa-database',
//                        'url' => '/admin/clock_2s?id=3',
//                        'priority' => 40,
//                    ],
                ],
            ],
        ]
    ],]
);

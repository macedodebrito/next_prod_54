<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;

global $more_css;

class FreezeWeek extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    /**
     * @var string
     */
    protected $title = 'Semana 1';

    /**
     * @var string
     */
    protected $alias;

    public function initialize() {
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->old_end_at);
        });

        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $get_week = 0;
            $config2 = \App\Model\FreezeWeek::where('product_id', $model->product_id)->where('client', $model->client)->where('end_at', $model->end_at)->where('week', $get_week)->groupBy('client')->groupBy('product_id')->groupBy('end_at')->groupBy('quantity')->get();
            $week = $_REQUEST['week'];
            $end_at = new Carbon($_REQUEST['end_at']);
            $old_end_at = new Carbon($_REQUEST['old_end_at']);

            DB::table('freeze_week')->where('product_id', $model->product_id)->where('client', $model->client)->where('week', $get_week)->where('end_at', $old_end_at)->update(['end_at' => $end_at, 'week' => $week]);

            DB::table('freeze_week')->where('id', $model->id)->update(['end_at' => $end_at, 'week' => $week]);
        });
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

        if (\Request::path() == "admin/triple_weeks") {
            $more_css = "hidden";
        } else {
            $more_css = "";
        }

        $display = AdminDisplay::datatables()
                ->paginate(50)
                ->setOrder([[0, 'asc']])
                ->withPackage('jquery')
                ->addScript('triple.js', asset('assets/js/triple.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('end_at', 'asc')->where('week', 0)->groupBy('client')->groupBy('product_id')->groupBy('end_at')->groupBy('quantity');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
            $header_data = AdminColumn::datetime('end_at')
            ->setLabel('Data de Expedição')
            ->setWidth('150px')
            ->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i'),
            $header_product = AdminColumn::text('products.name')->setLabel('Produto')->setWidth('200px'),
//            AdminColumn::text('montagem')->setLabel('Quantidade'),
            $header_quantity = AdminColumn::custom()
            ->setLabel('Quantidade')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                global $VALOR_0;
                $config = \App\Model\FreezeWeek::where('product_id', $instance->product_id)->where('client', $instance->client)->where('end_at', $instance->end_at)->orderBy('end_at', 'asc')->where('week', 0)->groupBy('client')->groupBy('product_id')->groupBy('end_at')->groupBy('quantity')->count();
                $VALOR_0 = $config;
                return $config;
            }),
            $header_client = AdminColumn::text('client')->setLabel('Cliente'),
            $header_obs = AdminColumn::text('obs')->setLabel('Observações'),
            $header_points = AdminColumn::custom()
            ->setLabel('Pontos')
            ->setWidth('100px')
            ->setCallback(function ($instance) {
                global $VALOR_0;
                $config = \App\Model\Product::where('id', $instance->product_id)->first();
                $POINTS = $VALOR_0 * $config->points;
                return $POINTS;
            }),
            $header_palete_store = AdminColumn::custom()
            ->setLabel('Palete<br/>Separada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                $palete = \App\Model\Palete::where('request_week_id', $instance->requestweek_id)->first();
                if (count($palete) > 0) {
                    if ($palete->status != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    }
                    else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_auth = AdminColumn::custom()
            ->setLabel('Palete<br/>Autorizada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                $palete = \App\Model\Palete::where('request_week_id', $instance->requestweek_id)->first();
                if (count($palete) > 0) {
                    if ($palete->auth_id != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
        ]);

        $header_data->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_product->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_quantity->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_client->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_obs->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_points->getHeader()->setHtmlAttribute('class', 'bg-yellow');
        $header_palete_store->getHeader()->setHtmlAttribute('class', "bg-red $more_css");
        $header_palete_auth->getHeader()->setHtmlAttribute('class', "bg-black $more_css");
        return $display;
    }

    public function onEdit($id) {
        $get_week[-1] = "Tabela em Atraso";
        $get_week[0] = "Semana 1";
        $get_week[1] = "Semana 2";
        $get_week[2] = "Semana 3";
        $get_week[3] = "Semana 4";
        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::datetime('end_at')->setLabel('Data de Término na Montagem'),
                        AdminFormElement::hidden('old_end_at')->setDefaultValue(\App\Model\FreezeWeek::where('id', $id)->first()->end_at)
                            ], 3)
                    ->addColumn([
                        AdminFormElement::select('week', 'Semana')
                        ->setOptions($get_week)
                        ->required()
                            ], 3)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Produto'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

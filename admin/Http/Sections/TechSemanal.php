<?php

use App\Model\TechSemanal;
use App\Model\TechWeek;
use App\Model\TechRequestsUser;
use App\Model\TechRequests;
use App\Model\Tech;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(TechSemanal::class, function (ModelConfiguration $model) {
    $model->setTitle('Lista de Encomendas | Técnica')->setAlias('tecnica/week')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatablesAsync()
                ->setOrder([[1, 'asc']])->paginate(100);

        $display->setApply(function($query) {
            $query->orderBy('end_at', 'asc');

            $check = TechWeek::orderBy('end_at', 'asc')->get();

            // lista de pedidos semanais por ordem
            $SEMANAL = [];
            $week = [];
            //print_r("<pre>");
            foreach ($check as $key => $instance) {
                $total = TechWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('quantity');

                $total_week = TechWeek::where('tech_id', $instance->tech_id)->orderBy('end_at', 'asc')->sum('quantity');

                $total_req = TechRequests::where('tech_id', $instance->tech_id)->first();

                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                $total_req2 = TechRequests::where('tech_id', $instance->tech_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = TechRequestsUser::where('tech_requests_id', $go->id)->sum('value');
//                    $dump = TechRequestsUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->tech_id]['tech_id'] = $instance->tech_id;
                $SEMANAL[$instance->tech_id]['name'] = Tech::where('id', $instance->tech_id)->first()->name;
                $SEMANAL[$instance->tech_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->tech_id]['total_montado'] = $total_inc;

                foreach (TechWeek::where('tech_id', $instance->tech_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$instance->tech_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->tech_id]['semana'][$key]['value'] = $value->quantity;
                }
            }
            //print_r($SEMANAL);
            foreach ($SEMANAL as $key2 => $value2) {
                foreach ($value2['semana'] as $key3 => $value3) {
                    if ($SEMANAL[$key2]['total_montado'] > 0) {
                        if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        }
                        //print_r($SEMANAL[$key2]['semana'][$key3]);
                    }
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                        $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    }
                }
            }

//            print_r($value2);
//            print_r($SEMANAL);
//            print_r("</pre>");
            $query->whereIn('id', $week)->orderBy('end_at', 'asc');
            //$query->orderBy('end_at', 'asc');
        });
        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_at)));

$CSS = "";
                        $datework = \Carbon\Carbon::parse($instance->end_at);
                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }
                        

                        if ($curdate > $mydate) {
                            $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                        } else if ($curdate <= $mydate) {
                            if ($curdate == $mydate) {
                                $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                            } else {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            }
                        }
                        return $status_date;
                    }),             
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m'),
//                    AdminColumn::custom()
//                    ->setLabel('Nº Pedido de Produção')
//                    ->setWidth('20%')->setHtmlAttribute('class', 'bg-gray text-center')
//                    ->setCallback(function ($instance) {
//                                $pp = TechRequests::where('id', $instance->request_pp['id'])->first();
//                                return $pp->pp;
//                            }),
            AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {

                        $product_name = Tech::where('id', $instance->tech_id)->first();
                        return $product_name->name;
                    }),
                    AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $total = TechWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('quantity');

                                $total_week = TechWeek::where('tech_id', $instance->tech_id)->orderBy('end_at', 'asc')->sum('quantity');

                                $total_req = TechRequests::where('tech_id', $instance->tech_id)->first();

                                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                                $total_req2 = TechRequests::where('tech_id', $instance->tech_id)->get();
                                $total_inc = 0;
                                foreach ($total_req2 as $go) {
                                    $dump = TechRequestsUser::where('tech_requests_id', $go->id)->sum('value');
//                                    $dump = TechRequestsUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                                    $total_inc = $total_inc + $dump;
                                }
                                //print_r($total_inc);

                                $SEMANAL[$instance->tech_id]['tech_id'] = $instance->tech_id;
                                $SEMANAL[$instance->tech_id]['name'] = Tech::where('id', $instance->tech_id)->first()->name;
                                $SEMANAL[$instance->tech_id]['total_semanal'] = $total_week;
                                $SEMANAL[$instance->tech_id]['total_montado'] = $total_inc;

                                foreach (TechWeek::where('tech_id', $instance->tech_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                                    $SEMANAL[$instance->tech_id]['semana'][$key]['id'] = $value->id;
                                    $SEMANAL[$instance->tech_id]['semana'][$key]['value'] = $value->quantity;
                                }

                                foreach ($SEMANAL as $key2 => $value2) {
                                    foreach ($value2['semana'] as $key3 => $value3) {
                                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            }
                                            //print_r($SEMANAL[$key2]['semana'][$key3]);
                                        }
                                        //print_r($SEMANAL[$instance->product_id]['semana'][$key3]['id']);
                                        if ($SEMANAL[$instance->tech_id]['semana'][$key3]['id'] == $instance->id) {
                                            return $SEMANAL[$key2]['semana'][$key3]['value'];
                                        }
                                    }
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente')
        ]);

        return $display;
    });
    //    
});

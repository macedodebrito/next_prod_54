<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;

global $more_css;

class LocksmithFreeze extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;
    protected $model;

    /**
     * @var string
     */
    protected $title = 'Semanas da Serralharia';

    /**
     * @var string
     */
    protected $alias = "serralharia/week_updated";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

        $display = AdminDisplay::datatables()
                ->paginate(50)
                ->setOrder([[1, 'asc']]);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $total = \App\Model\LocksmithWeek::orderBy('end_at', 'asc')->get();
            $GG = [];
            foreach ($total as $encomenda) {
                $total_week = \App\Model\LocksmithWeek::where('locksmith_id', $encomenda->locksmith_id)->orderBy('end_at', 'asc')->sum('quantity');
                $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $encomenda->locksmith_id)->first();
                $total_req2 = \App\Model\LocksmithRequests::where('locksmith_id', $encomenda->locksmith_id)->get();
                $total_inc = 0;
                $total_inc2 = 0;
                foreach ($total_req2 as $go) {
                    $dump = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', -1)->sum('value');
                    $dump2 = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                    $total_inc2 = $total_inc2 + $dump2;
                }
                $SEMANAL[$encomenda->locksmith_id]['locksmith_id'] = $encomenda->locksmith_id;
                $SEMANAL[$encomenda->locksmith_id]['name'] = \App\Model\Locksmith::where('id', $encomenda->locksmith_id)->first()->name;
                $SEMANAL[$encomenda->locksmith_id]['total_semanal'] = $total_week;
                $SEMANAL[$encomenda->locksmith_id]['total_montado'] = $total_inc;
                $SEMANAL[$encomenda->locksmith_id]['total_montado2'] = $total_inc2;

                //$SEMANAL[$encomenda->product_id]['total_montado2'] = $total_inc2;
                foreach (\App\Model\LocksmithWeek::where('locksmith_id', $encomenda->locksmith_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$encomenda->locksmith_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$encomenda->locksmith_id]['semana'][$key]['value'] = $value->quantity;
                }
                foreach ($SEMANAL as $key2 => $value2) {
                    foreach ($value2['semana'] as $key3 => $value3) {
                        if ($SEMANAL[$key2]['total_montado'] > 0) {
//                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
//                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
//                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                            }
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                            $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                        }
                        //dd($SEMANAL);
                        if ($SEMANAL[$key2]['semana'][$key3]['id'] == $encomenda->id) {

                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                if ($SEMANAL[$key2]['total_montado2'] < 0) {
                                    $GG['pendentes'][] = $encomenda->id;
                                } else {
                                    $GG['feitas'][] = $encomenda->id;
                                }
                            } else {
                                $GG['falta'][] = $encomenda->id;
                            }
                        }
                    }
                }
            }
            if (!isset($GG['pendentes'])) {
                $GG['pendentes'] = "";
            }
            if (!isset($GG['feitas'])) {
                $GG['feitas'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }
            //dd($SEMANAL);
            //dd($GG);
            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 2) {
                    $query->whereIn('id', $GG['feitas']);
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['pendentes']);
                } else if ($_GET['status'] == 0) {

                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                $query->whereIn('id', $GG['falta']);
                //$query->orderBy('end_at', 'asc');
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('locksmith_id')->setModel(\App\Model\Locksmith::class)
        ]);

        $display->setColumns([
            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_at)));

                        $CSS = "";
                        $datework = \Carbon\Carbon::parse($instance->end_at);
                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }


                        if ($curdate > $mydate) {
                            $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                        } else if ($curdate <= $mydate) {
                            if ($curdate == $mydate) {
                                $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                            } else {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            }
                        }
                        return $status_date;
                    }),
            $header_data = AdminColumn::datetime('end_at')
            ->setLabel('Data de Expedição')
            ->setWidth('150px')
            ->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y'),
//            $header_product = AdminColumn::text('products.name')->setLabel('Produto')->setWidth('200px'),
            $header_product = AdminColumn::custom()
            ->setLabel('Produto')
            ->setWidth('200px')->setHtmlAttribute('class', 'text-center')
            ->setCallback(function ($instance) {
                $product_name = \App\Model\Locksmith::where('id', $instance->locksmith_id)->first();
                $IMG = "";
                $IMG_1 = "";
                $IMG_2 = "";
                $IMG_3 = "";
                $CAPTION_1 = "";
                $CAPTION_2 = "";
                $CAPTION_3 = "";

                if ($product_name->img_1) {
                    if ($product_name->caption_1) {
                        $CAPTION_1 = $product_name->caption_1;
                    }
                    $IMG_1 = "<div><span><img src=\"../$product_name->img_1\"></span><i>$CAPTION_1</i></div>";
                }
                if ($product_name->img_2) {
                    if ($product_name->caption_2) {
                        $CAPTION_2 = $product_name->caption_2;
                    }
                    $IMG_2 = "<div><span><img src=\"../$product_name->img_2\"></span><i>$CAPTION_2</i></div>";
                }
                if ($product_name->img_3) {
                    if ($product_name->caption_3) {
                        $CAPTION_3 = $product_name->caption_3;
                    }
                    $IMG_3 = "<div><span><img src=\"../$product_name->img_3\"></span><i>$CAPTION_3</i></div>";
                }

                if (($product_name->img_1) || ($product_name->img_2) || ($product_name->img_3)) {
                    $IMG = " <i class=\"watch_product_img fa fa-picture-o fa-2x\"></i><div>$IMG_1 $IMG_2 $IMG_3</div>";
                }
                return $product_name->name . $IMG;
            }),
//            AdminColumn::text('montagem')->setLabel('Quantidade'),
            $header_quantity = AdminColumn::custom()
            ->setLabel('#')
            ->setWidth('10%')->setHtmlAttribute('class', 'text-center')
            ->setCallback(function ($instance) {
                $total = \App\Model\LocksmithWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('quantity');

                $total_week = \App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->orderBy('end_at', 'asc')->sum('quantity');

                $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $instance->locksmith_id)->first();

                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                $total_req2 = \App\Model\LocksmithRequests::where('locksmith_id', $instance->locksmith_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->sum('value');
//                                    $dump = LocksmithRequestsUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->locksmith_id]['locksmith_id'] = $instance->locksmith_id;
                $SEMANAL[$instance->locksmith_id]['name'] = \App\Model\Locksmith::where('id', $instance->locksmith_id)->first()->name;
                $SEMANAL[$instance->locksmith_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->locksmith_id]['total_montado'] = $total_inc;

                foreach (\App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$instance->locksmith_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->locksmith_id]['semana'][$key]['value'] = $value->quantity;
                }

                foreach ($SEMANAL as $key2 => $value2) {
                    foreach ($value2['semana'] as $key3 => $value3) {
                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                            }
                            //print_r($SEMANAL[$key2]['semana'][$key3]);
                        }
                        //print_r($SEMANAL[$instance->product_id]['semana'][$key3]['id']);
                        if ($SEMANAL[$instance->locksmith_id]['semana'][$key3]['id'] == $instance->id) {
                            return $SEMANAL[$key2]['semana'][$key3]['value'];
                        }
                    }
                }
            }),
            $header_client = AdminColumn::text('client')->setLabel('Cliente'),
            $header_obs = AdminColumn::text('obs')->setLabel('Observações'),
            $header_points = AdminColumn::custom()
            ->setLabel('Pontos')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                global $VALOR_atraso;
                $config = \App\Model\Locksmith::where('id', $instance->locksmith_id)->first();
                $POINTS = $VALOR_atraso * $config->points;
                return $POINTS;
            }),
            $header_palete_store = AdminColumn::custom()
            ->setLabel('Palete<br/>Separada')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                $palete = \App\Model\LocksmithPalete::where('locksmith_week_id', $instance->id)->where('store_id', '!=', 0)->first();
                if (count($palete) > 0) {
                    return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_auth = AdminColumn::custom()
            ->setLabel('Palete<br/>Autorizada')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                $palete = \App\Model\LocksmithPalete::where('locksmith_week_id', $instance->id)->where('auth_id', '!=', 0)->first();
                if (count($palete) > 0) {
                    if ($palete->auth_id != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
        ]);
        $header_data->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_product->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_quantity->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_client->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_obs->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_points->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_palete_store->getHeader()->setHtmlAttribute('class', "bg-red");
        $header_palete_auth->getHeader()->setHtmlAttribute('class', "bg-black");
        return $display;
    }

}

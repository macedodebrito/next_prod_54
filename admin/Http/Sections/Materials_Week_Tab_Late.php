<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminColumnFilter;
use SleepingOwl\Admin\Form\FormElements;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Materials_Week_Tab_Late extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Materiais em Falta';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
//    public function onDisplay() {
//        // Display
//        $display = $this->fireEdit(1);
//        return $display;
//    }

    public function onDisplay() {
        $table = AdminDisplay::datatables()->setModelClass(\App\Model\Materials_Week_Tab_Late::class)->paginate(100)->setOrder([[1, 'asc']])
                ->setColumns([
            AdminColumn::text('modelo', 'Coluna'),
            AdminColumn::text('NSP', 'NSP')->setHtmlAttribute('class', 'reorder')->setWidth('80px'),
            AdminColumn::text('ref', 'Referência')->setWidth('100px'),
            AdminColumn::text('design', 'Design'),
            AdminColumn::text('qtt', 'Quantidade')->setWidth('80px'),
            AdminColumn::custom()
            ->setLabel('Encomenda')
            ->setCallback(function ($instance) {
                        if ($instance->nr_encomenda != 0) {
                            return $instance->nr_encomenda;
                        } else {
                            return "-";
                        }
                    }),
            AdminColumn::text('obs', 'Observações'),
            AdminColumn::custom()
            ->setLabel('Data')
            ->setCallback(function ($instance) {
                        if (($instance->dia) && ($instance->mes) && ($instance->ano)) {
                            if (($instance->dia != null) && ($instance->mes != null) && ($instance->ano != null)) {
                                $now = \Carbon\Carbon::now()->format("d-m-Y");
                                $now = \Carbon\Carbon::parse($now);
                                $nxt_date = \Carbon\Carbon::parse("$instance->dia-$instance->mes-$instance->ano");
                                $diff = $now->diffInDays($nxt_date);
                                if ($diff > 3) {
                                    $color = "#33CC33"; // VERDE
                                    $obs = "ON TIME";
                                } else if (($diff <= 3) && ($diff > 0)) {
                                    $color = "#FFFF00"; // AMARELO
                                    $obs = "ON TIME";
                                } else if ($diff == 0) {
                                    $color = "#4DA6FF"; // AZUL
                                    $obs = "ON TIME";
                                } else if ($diff < 0) {
                                    $color = "#FF0000"; // VERMELHO
                                    $obs = "ATRASADO";
                                } else {
                                    $color = ""; // BRANCO
                                }
                                return "<div class=\"text-center\" style=\"background-color: $color;\">$instance->dia-$instance->mes-$instance->ano <br>($diff dias - $obs)</div>";
                            }
                        } else {
                            return "Sem data Prevista";
                        }
                    }),
        ]);
        $table->getColumnFilters()->setPlacement('table.header');
        $table->getColumns()->getControlColumn()->setDeletable(false);

        $table_1 = AdminDisplay::datatables()->setModelClass(\App\Model\Materials_Week_Tab_1::class)->paginate(100)->setOrder([[1, 'asc']])
                ->setColumns([
            AdminColumn::text('modelo', 'Coluna'),
            AdminColumn::text('NSP', 'NSP')->setHtmlAttribute('class', 'reorder')->setWidth('80px'),
            AdminColumn::text('ref', 'Referência')->setWidth('100px'),
            AdminColumn::text('design', 'Design'),
            AdminColumn::text('qtt', 'Quantidade')->setWidth('80px'),
            AdminColumn::custom()
            ->setLabel('Encomenda')
            ->setCallback(function ($instance) {
                        if ($instance->nr_encomenda != 0) {
                            return $instance->nr_encomenda;
                        } else {
                            return "-";
                        }
                    }),
            AdminColumn::text('obs', 'Observações'),
            AdminColumn::custom()
            ->setLabel('Data')
            ->setCallback(function ($instance) {
                       if (($instance->dia) && ($instance->mes) && ($instance->ano)) {
                            if (($instance->dia != null) && ($instance->mes != null) && ($instance->ano != null)) {
                                $now = \Carbon\Carbon::now()->format("d-m-Y");
                                $now = \Carbon\Carbon::parse($now);
                                $nxt_date = \Carbon\Carbon::parse("$instance->dia-$instance->mes-$instance->ano");
                                $diff = $now->diffInDays($nxt_date);
                                if ($diff > 3) {
                                    $color = "#33CC33"; // VERDE
                                    $obs = "ON TIME";
                                } else if (($diff <= 3) && ($diff > 0)) {
                                    $color = "#FFFF00"; // AMARELO
                                    $obs = "ON TIME";
                                } else if ($diff == 0) {
                                    $color = "#4DA6FF"; // AZUL
                                    $obs = "ON TIME";
                                } else if ($diff < 0) {
                                    $color = "#FF0000"; // VERMELHO
                                    $obs = "ATRASADO";
                                } else {
                                    $color = ""; // BRANCO
                                }
                                return "<div class=\"text-center\" style=\"background-color: $color;\">$instance->dia-$instance->mes-$instance->ano <br>($diff dias - $obs)</div>";
                            }
                        } else {
                            return "Sem data Prevista";
                        }
                    }),
        ]);
        $table_1->getColumnFilters()->setPlacement('table.header');
        $table_1->getColumns()->getControlColumn()->setDeletable(false);

        $table_2 = AdminDisplay::datatables()->setModelClass(\App\Model\Materials_Week_Tab_2::class)->paginate(100)->setOrder([[1, 'asc']])
                ->setColumns([
            AdminColumn::text('modelo', 'Coluna'),
            AdminColumn::text('NSP', 'NSP')->setHtmlAttribute('class', 'reorder')->setWidth('80px'),
            AdminColumn::text('ref', 'Referência')->setWidth('100px'),
            AdminColumn::text('design', 'Design'),
            AdminColumn::text('qtt', 'Quantidade')->setWidth('80px'),
            AdminColumn::custom()
            ->setLabel('Encomenda')
            ->setCallback(function ($instance) {
                        if ($instance->nr_encomenda != 0) {
                            return $instance->nr_encomenda;
                        } else {
                            return "-";
                        }
                    }),
            AdminColumn::text('obs', 'Observações'),
            AdminColumn::custom()
            ->setLabel('Data')
            ->setCallback(function ($instance) {
                       if (($instance->dia) && ($instance->mes) && ($instance->ano)) {
                            if (($instance->dia != null) && ($instance->mes != null) && ($instance->ano != null)) {
                                $now = \Carbon\Carbon::now()->format("d-m-Y");
                                $now = \Carbon\Carbon::parse($now);
                                $nxt_date = \Carbon\Carbon::parse("$instance->dia-$instance->mes-$instance->ano");
                                $diff = $now->diffInDays($nxt_date);
                                if ($diff > 3) {
                                    $color = "#33CC33"; // VERDE
                                    $obs = "ON TIME";
                                } else if (($diff <= 3) && ($diff > 0)) {
                                    $color = "#FFFF00"; // AMARELO
                                    $obs = "ON TIME";
                                } else if ($diff == 0) {
                                    $color = "#4DA6FF"; // AZUL
                                    $obs = "ON TIME";
                                } else if ($diff < 0) {
                                    $color = "#FF0000"; // VERMELHO
                                    $obs = "ATRASADO";
                                } else {
                                    $color = ""; // BRANCO
                                }
                                return "<div class=\"text-center\" style=\"background-color: $color;\">$instance->dia-$instance->mes-$instance->ano <br>($diff dias - $obs)</div>";
                            }
                        } else {
                            return "Sem data Prevista";
                        }
                    }),
        ]);
        $table_2->getColumnFilters()->setPlacement('table.header');
        $table_2->getColumns()->getControlColumn()->setDeletable(false);

        $table_3 = AdminDisplay::datatables()->setModelClass(\App\Model\Materials_Week_Tab_3::class)->paginate(100)->setOrder([[1, 'asc']])
                ->setColumns([
            AdminColumn::text('modelo', 'Coluna'),
            AdminColumn::text('NSP', 'NSP')->setHtmlAttribute('class', 'reorder')->setWidth('80px'),
            AdminColumn::text('ref', 'Referência')->setWidth('100px'),
            AdminColumn::text('design', 'Design'),
            AdminColumn::text('qtt', 'Quantidade')->setWidth('80px'),
            AdminColumn::custom()
            ->setLabel('Encomenda')
            ->setCallback(function ($instance) {
                        if ($instance->nr_encomenda != 0) {
                            return $instance->nr_encomenda;
                        } else {
                            return "-";
                        }
                    }),
            AdminColumn::text('obs', 'Observações'),
            AdminColumn::custom()
            ->setLabel('Data')
            ->setCallback(function ($instance) {
                        if (($instance->dia) && ($instance->mes) && ($instance->ano)) {
                            if (($instance->dia != null) && ($instance->mes != null) && ($instance->ano != null)) {
                                $now = \Carbon\Carbon::now()->format("d-m-Y");
                                $now = \Carbon\Carbon::parse($now);
                                $nxt_date = \Carbon\Carbon::parse("$instance->dia-$instance->mes-$instance->ano");
                                $diff = $now->diffInDays($nxt_date);
                                if ($diff > 3) {
                                    $color = "#33CC33"; // VERDE
                                    $obs = "ON TIME";
                                } else if (($diff <= 3) && ($diff > 0)) {
                                    $color = "#FFFF00"; // AMARELO
                                    $obs = "ON TIME";
                                } else if ($diff == 0) {
                                    $color = "#4DA6FF"; // AZUL
                                    $obs = "ON TIME";
                                } else if ($diff < 0) {
                                    $color = "#FF0000"; // VERMELHO
                                    $obs = "ATRASADO";
                                } else {
                                    $color = ""; // BRANCO
                                }
                                return "<div class=\"text-center\" style=\"background-color: $color;\">$instance->dia-$instance->mes-$instance->ano <br>($diff dias - $obs)</div>";
                            }
                        } else {
                            return "Sem data Prevista";
                        }
                    }),
        ]);
        $table_3->getColumnFilters()->setPlacement('table.header');
        $table_3->getColumns()->getControlColumn()->setDeletable(false);

        $table_consumables = AdminDisplay::datatables()->setModelClass(\App\Model\Materials_Week_Tab_Consumables::class)->paginate(100)->setOrder([[1, 'asc']])
                ->setColumns([
            AdminColumn::text('ref', 'Referência')->setWidth('100px'),
            AdminColumn::text('design', 'Designação'),
            AdminColumn::text('qtt', 'Quantidade')->setWidth('80px'),
            AdminColumn::text('NSC', 'NSC')->setHtmlAttribute('class', 'reorder')->setWidth('80px'),
            AdminColumn::text('seccao', 'Secção'),
            AdminColumn::custom()
            ->setLabel('Encomenda')
            ->setCallback(function ($instance) {
                        if ($instance->nr_encomenda != 0) {
                            return $instance->nr_encomenda;
                        } else {
                            return "-";
                        }
                    }),
            AdminColumn::text('obs', 'Observações'),
            AdminColumn::custom()
            ->setLabel('Data')
            ->setCallback(function ($instance) {
                       if (($instance->dia) && ($instance->mes) && ($instance->ano)) {
                            if (($instance->dia != null) && ($instance->mes != null) && ($instance->ano != null)) {
                                $now = \Carbon\Carbon::now()->format("d-m-Y");
                                $now = \Carbon\Carbon::parse($now);
                                $nxt_date = \Carbon\Carbon::parse("$instance->dia-$instance->mes-$instance->ano");
                                $diff = $now->diffInDays($nxt_date);
                                if ($diff > 3) {
                                    $color = "#33CC33"; // VERDE
                                    $obs = "ON TIME";
                                } else if (($diff <= 3) && ($diff > 0)) {
                                    $color = "#FFFF00"; // AMARELO
                                    $obs = "ON TIME";
                                } else if ($diff == 0) {
                                    $color = "#4DA6FF"; // AZUL
                                    $obs = "ON TIME";
                                } else if ($diff < 0) {
                                    $color = "#FF0000"; // VERMELHO
                                    $obs = "ATRASADO";
                                } else {
                                    $color = ""; // BRANCO
                                }
                                return "<div class=\"text-center\" style=\"background-color: $color;\">$instance->dia-$instance->mes-$instance->ano <br>($diff dias - $obs)</div>";
                            }
                        } else {
                            return "Sem data Prevista";
                        }
                    }),
        ]);
        $table_consumables->getColumnFilters()->setPlacement('table.header');
        $table_consumables->getColumns()->getControlColumn()->setDeletable(false);

        $tabs = AdminDisplay::tabbed();
        $tabs->appendTab($table, 'Atraso');
        $tabs->appendTab($table_1, 'Semana 1');
        $tabs->appendTab($table_2, 'Semana 2');
        $tabs->appendTab($table_3, 'Semana 3');
        $tabs->appendTab($table_consumables, 'Consumiveis');
        return $tabs;
    }

}

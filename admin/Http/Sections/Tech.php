<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Tech extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Gestão da Técnica';
    
    public function getCreateTitle() {
        return 'Criar Nova Peça';
    }    
    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Nova Peça')
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('name', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')->setWidth('30px'),
            AdminColumn::link('name')->setLabel('Nome da Peça'),
            AdminColumn::link('mini_code')->setLabel('Designação'),
            AdminColumn::link('code')->setLabel('Código'),
            AdminColumn::link('points')->setLabel('Pontuação'),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $score = 0;
        while ($score <= 100) {
            $GG[] = $score;
            $score++;
        }
        return AdminForm::form()->setItems([
                    AdminFormElement::text('name', 'Nome da Peça')->required(),
                    AdminFormElement::text('mini_code', 'Designação')->required(),
                    AdminFormElement::text('code', 'Código')->required(),
                    AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required(),
        ]);
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}


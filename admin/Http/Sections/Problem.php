<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use Carbon\Carbon;
use SleepingOwl\Admin\Section;

class Problem extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->auth);
        });
        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->auth);
        });        
    }

    /**
     * @var string
     */
    protected $title = 'Gestão de Manutenção';

    public function getCreateTitle() {
        return 'Reportar Novo Problema';
    }

    /**
     * @var string
     */
    protected $alias = "problems";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Reportar Novo Problema')
                ->paginate(25)->setOrder([[7, 'asc'],[1, 'desc']]);
        $display->setApply(function($query) {
            $query->orderBy('status', 'asc')->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');


        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')->setWidth('30px'),
            AdminColumn::datetime('created_at')->setLabel('Data de Criação')->setWidth('120px'),
            AdminColumn::text('section_type')->setLabel('Secção'),
                    AdminColumn::custom()
                    ->setLabel('Operador')
                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                if ($instance->user != 0) {
                                    $valid_id = \App\User::where('id', $instance->user)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->user)->first()->bigname;
                                        return "$nome";
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
            AdminColumn::text('text')->setLabel('Relatório'),
            AdminColumn::text('obs')->setLabel('Observações'),
            //AdminColumn::datetime('updated_at')->setLabel('Última Actualização')->setWidth('120px'),
            AdminColumn::custom()
                    ->setLabel('Última Actualização')->setWidth('150px')
                    ->setCallback(function ($instance) {
                        $user_name = "";
                        if ($instance->resolution_id != 0) {
                            $valid_id = \App\User::where('id', $instance->resolution_id)->first();
                            if (count($valid_id) > 0) {
                                $user_name = \App\User::where('id', $instance->resolution_id)->first()->bigname;
                            } else {
                                $user_name = "<div class=\"bg-black\">DESCONHECIDO</div>";
                            }
                            return "$instance->updated_at <br/>por <strong>$user_name</strong>";
                        }
                        else {
                            return "$instance->updated_at <br/>";
                        }
                    }),  
                    AdminColumn::custom()
                    ->setLabel('Status')->setWidth('120px')
                    ->setCallback(function ($instance) {
                        switch ($instance->status) {
                            case 1: return "<div class=\"bg-red text-center\"> <i class=\"fa fa-exclamation\"></i> Por Resolver</div>";
                                break;
                            case 2: return "<div class=\"bg-yellow text-center\"> <i class=\"fa fa-clock-o\"></i> Em Resolução</div>";
                                break;
                            case 3: return "<div class=\"bg-green text-center\"> <i class=\"fa fa-check\"></i> Resolvido</div>";
                                break;
                            default: return "<div class=\"bg-black text-center\"> <i class=\"fa fa-exclamation\"></i> Reportado</div>";
                                break;
                        }
                    }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $GG['CNC'] = "CNC";
        $GG['Preparação'] = "Preparação da CNC";
        $GG['Carpintaria'] = "Carpintaria";
        $GG['Acabamento'] = "Acabamento da Carpintaria";
        $GG['Pintura'] = "Pintura";
        $GG['Montagem'] = "Montagem";
        $GG['Redes'] = "Redes";
        $GG['Serralharia'] = "Serralharia";
        $GG['Serigrafia'] = "Serigrafia";
        $GG['Técnica'] = "Técnica";
        $GG['Armazém'] = "Armazém";
        $GG['Geral'] = "Geral";

        $XX = [];
        $XX[0] = "Reportado";
        $XX[1] = "Por Resolver";
        $XX[2] = "Em Resolução";
        $XX[3] = "Resolvido";

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('problems_edit.js', asset('assets/js/problems_edit.js'), ['admin-default']);
        $form->addHeader([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::password('auth', 'Código de Operador')->required()
                    ], 2)                                       
]);        
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('section_type', 'Secção')->setOptions($GG)
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('status', 'Status')->setOptions($XX)
                            ], 2)                            
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::textarea('text')->setLabel('Descrição do Problema (Apenas Leitura)')->setReadonly(1),
                            ], 6)
                    ->addColumn([
                        AdminFormElement::textarea('obs')->setLabel('Observações'),
                            ], 6)
        ]);
        $form->addBody([
            AdminFormElement::hidden('resolution_id')->required(),
        ]);
        $form->getButtons()->replaceButtons([
            'delete' => (new Delete())->setText('Apagar'),
            'save' => (new SaveAndClose())->setText('Gravar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        $GG = [];
        $GG['CNC'] = "CNC";
        $GG['Preparação'] = "Preparação da CNC";
        $GG['Carpintaria'] = "Carpintaria";
        $GG['Acabamento'] = "Acabamento da Carpintaria";
        $GG['Pintura'] = "Pintura";
        $GG['Montagem'] = "Montagem";
        $GG['Redes'] = "Redes";
        $GG['Serralharia'] = "Serralharia";
        $GG['Serigrafia'] = "Serigrafia";
        $GG['Técnica'] = "Técnica";
        $GG['Armazém'] = "Armazém";
        $GG['Geral'] = "Geral";
//        return AdminForm::form()->setItems([
//                    AdminFormElement::text('name', 'Nome do Produto')->required(),
//                    AdminFormElement::text('code', 'Código')->required(),
//                    AdminFormElement::select('serie_id', 'Série')->setModelForOptions(new \App\Model\Serie)->setDisplay('name')->required(),
//                    AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required(),
//        ]);

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('problems.js', asset('assets/js/problems.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('section_type', 'Secção')->setOptions($GG)->required()
                            ], 3)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('auth', 'Código de Operador')->required()
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::textarea('text')->setLabel('Descrição do Problema')->required(),
                            ], 12)
        ]);
        $form->addBody([
            AdminFormElement::hidden('user')->required(),
        ]);
        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Reportar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

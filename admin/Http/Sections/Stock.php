<?php

use App\Model\Stock;
use App\Model\StockUser;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Stock::class, function (ModelConfiguration $model) {
    $model->setTitle('Stock')->enableAccessCheck();


    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('request_id', 'asc');
        });
        
        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::text('request_id')->setLabel('#')->setWidth('30px'),
        ]);

        return $display;
    });

    // Create And Edit
    $model->onCreateAndEdit(function() {
        return AdminForm::form()->setItems([
                    AdminFormElement::text('request_id', 'Nome do Produto')->required(),
        ]);
    });
});

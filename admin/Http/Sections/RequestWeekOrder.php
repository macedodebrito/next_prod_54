<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\Auth;

class RequestWeekOrder extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->group_quantity);
            unset($model->product_id_selector);
            unset($model->new_quantity);
            unset($model->observations);
            $model->product_id = -9889;
            $model->quantity = -9889;
            $model->custom_id = -9889;
//            dd($_REQUEST);          
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\RequestWeek::orderBy('id', 'desc')->first()->id;
            \App\Model\RequestWeek::where('id', $id)->delete();

            $end_at = new Carbon($_REQUEST['end_at']);
            $end_paint_at = new Carbon($_REQUEST['end_paint_at']);
            $end_woodfinishing_at = new Carbon($_REQUEST['end_woodfinishing_at']);
            $end_wood_at = new Carbon($_REQUEST['end_wood_at']);
            if (isset($_REQUEST['urgency_date'])) {
                $urgency_date = new Carbon($_REQUEST['urgency_date']);
            } else {
                $urgency_date = null;
            }
            if (isset($_REQUEST['urgency'])) {
                $urgency = 1;
            } else {
                $urgency = 0;
            }

            foreach ($_REQUEST['product_id_selector'] as $key => $value) {
                $last_id = \App\Model\RequestWeek::insertGetId(array('id' => null, 'custom_id' => $_REQUEST['new_custom_id'][$key], 'product_id' => $_REQUEST['product_id_selector'][$key], 'user_id' => $_REQUEST['user_id'], 'quantity' => $_REQUEST['new_quantity'][$key], 'carpintaria' => $_REQUEST['new_quantity'][$key], 'woodfinishing' => $_REQUEST['new_quantity'][$key], 'pintura' => $_REQUEST['new_quantity'][$key], 'montagem' => $_REQUEST['new_quantity'][$key], 'client' => $_REQUEST['client'], 'obs' => $_REQUEST['observations'][$key], 'group' => $_REQUEST['group'], 'end_at' => $end_at, 'end_paint_at' => $end_paint_at, 'end_woodfinishing_at' => $end_woodfinishing_at, 'end_wood_at' => $end_wood_at, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString(), 'urgency_date' => $urgency_date, 'urgency' => $urgency));
                \App\Model\Palete::insert(array('id' => null, 'request_week_id' => $last_id));
                \App\Model\CopyOrdersPosition::insert(['id' => null, 'order_id' => $last_id, 'order_position' => 0, 'section_type' => 'montagem']);
                \App\Model\Sum_Orders::insert([
                'id' => null,
                'order_id' => $last_id,
                'product_id' => 0,
                'client' => "TEMP",
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'mount_date' => 0,
                'order_amount' => 0,
                'request' => 0,
                'mount_amount' => 0,
                'mount_date_simple' => 0,
                'temp_amount' => 0,
                'calc_amount' => 0,
                'product_name' => "TEMP",
                'points' => 0,
                'order' => 0
            ]);
            }
            // SYNC
            \Artisan::call('schedule:run');
        });
        $this->deleted(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \App\Model\Palete::where('request_week_id', $model->getKey())->delete();
            \App\Model\CopyOrdersPosition::where('order_id', $model->getKey())->delete();
            \App\Model\Sum_Orders::where('order_id', $model->getKey())->delete();
            // SYNC
            \Artisan::call('schedule:run');
        });
        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // SYNC
            \DB::table('check_config')->where('id', 1)->update(['pp_orders' => 0]);
//                        dd($_REQUEST);
            \Artisan::call('schedule:run');
        });
    }

    /**
     * @var string
     */
    protected $title = 'Encomendas';

    public function getCreateTitle() {
        return 'Criar uma nova Encomenda';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $order_arrow = "desc";
        if (isset($_GET['status'])) {
            if ($_GET['status'] == "T") {
                $order_arrow = "desc";
            } else {
                $order_arrow = "desc";
            }
        }
        
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[13, "$order_arrow"], [3, "asc"]])
                ->withPackage('jquery')
                ->setNewEntryButtonText('Nova Encomenda de Cliente')
                ->addScript('requestweekorders.js', asset('assets/js/requestweekorders.js'), ['admin-default'])
        ;

        $display->setApply(function($query) {

            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query
                            //->orderBy('sum_orders.order', 'asc')
                            ->orderBy('end_at', 'asc')
                            ->select('request_week.*', 'sum_orders.order_id AS order_id', 'sum_orders.id AS oid', 'sum_orders.order_amount', 'sum_orders.mount_amount', 'sum_orders.temp_amount', 'sum_orders.calc_amount', 'sum_orders.order as sum_order', 'sum_orders.request as request')
                            ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
                    ;
                } else if ($_GET['status'] == 0) {
                    $query
                            //->orderBy('sum_orders.order', 'desc')
                            ->orderBy('end_at', 'asc')
                            ->select('request_week.*', 'sum_orders.order_id AS order_id', 'sum_orders.id AS oid', 'sum_orders.order_amount', 'sum_orders.mount_amount', 'sum_orders.temp_amount', 'sum_orders.calc_amount', 'sum_orders.order as sum_order', 'sum_orders.request as request')
                            ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
                            ->where('sum_orders.temp_amount', '<', 0)
                    ;
                }
            } else {
                $query
                        //->orderBy('sum_orders.order', 'desc')
                        ->orderBy('end_at', 'asc')
                        ->select('request_week.*', 'sum_orders.order_id AS order_id', 'sum_orders.id AS oid', 'sum_orders.order_amount', 'sum_orders.mount_amount', 'sum_orders.temp_amount', 'sum_orders.calc_amount', 'sum_orders.order as sum_order', 'sum_orders.request as request')
                        ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
                        ->where('sum_orders.temp_amount', '<', 0)

                ;
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

//        $display->with('orders');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_wood_at')
                    ->setLabel('Data de Término (Carpintaria)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_woodfinishing_at')
                    ->setLabel('Data de Término (Acabamento)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_paint_at')
                    ->setLabel('Data de Término (Pintura)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_at_simple')
                    ->setLabel('Data de Término (Montagem)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::custom()
                    ->setLabel('Urgente')
                    ->setCallback(function ($instance) {
                                switch ($instance->urgency) {
                                    case 1: return "<i class=\"bg-red fa fa-bell\"></i>";
                                        break;
                                    default: return "-";
                                        break;
                                }
                            }),
                    AdminColumn::datetime('urgency_date')
                    ->setLabel('Data Urgente (Montagem)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'), 
                    AdminColumn::custom()
                ->setLabel('Produto')
                ->setCallback(function ($instance) {
                    return "<div>{$instance->products->name}</div><span class=\"badge\" style=\"color:{$instance->request_custom_nxt->hex_color};background-color:{$instance->request_custom_nxt->hex_color_bg}\">{$instance->request_custom_nxt->name}</span>";
                }),
            AdminColumn::text('products.points')->setLabel('Pontos')->setWidth('50px'),
            AdminColumn::text('montagem')->setLabel('#')->setWidth('50px'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('50px')
                    ->setCallback(function ($instance) {//                      
//                    dd($instance);         
//                                $CALC = 0;
//                                if ($instance->calc_amount == 0 && $instance->temp_amount < 0) {
//                                    $CALC = $instance->order_amount;
//                                } else if ($instance->calc_amount < 0 && $instance->temp_amount < 0) {
//                                    $CALC = $instance->order_amount;
//                                } else if ($instance->calc_amount > 0 && $instance->temp_amount < 0) {
//                                    $CALC = $instance->order_amount - $instance->calc_amount;
//                                } else {
//                                    if ($instance->temp_amount > $instance->calc_amount) {
//                                        $CALC = $instance->order_amount;
//                                    } else {
//                                        $CALC = $instance->calc_amount;
//                                    }
//                                }
                                $CSS = "bg-black";
                                $CALC = "OK";
                                if ($instance->temp_amount <= 0) {
                                    // Se abriu e fechou o total de PP
                                    if (($instance->temp_amount == 0) && ($instance->mount_amount <= $instance->request)) {
                                        $CSS = "bg-green";
                                        $CALC = "OK2";
                                    } else if (($instance->mount_amount <= $instance->request) && ($instance->calc_amount > 0)) {
                                        $CSS = "bg-yellow";
                                        $CALC = $instance->order_amount - $instance->calc_amount;
                                    } else if (($instance->mount_amount <= $instance->request) && ($instance->calc_amount == 0)) {
                                        $CSS = "bg-red";
                                        $CALC = $instance->order_amount;
                                    } else if ($instance->calc_amount < 0) {
                                        $CSS = "bg-red";
                                        $CALC = $instance->order_amount;
                                    } else if ($instance->calc_amount >= 0) {
                                        $CSS = "bg-black";
                                        $CALC = "??";
                                    }
                                }

//                                else {
//
//                                    if ($instance->temp_amount <= 0) {
//                                        $CSS = "bg-red";
//                                        if ($instance->calc_amount < 0 && $instance->temp_amount < 0) {
//                                            $CALC = $instance->order_amount;
//                                        } else if ($instance->calc_amount > 0 && $instance->temp_amount < 0) {
//                                            $CALC = $instance->order_amount - $instance->calc_amount;
//                                        } else {
//                                            $CALC = $instance->order_amount - $instance->calc_amount;
//                                        }
//                                    } else if ($instance->temp_amount > 0) {
//                                        $CSS = "bg-green";
//                                        $CALC = "OK2";
//                                    } else {
//                                        $CSS = "bg-black";
//                                        $CALC = "??";
//                                    }
//                                }
//                                return "<div class=\"$CSS text-center\">$CALC | C $instance->calc_amount / T $instance->temp_amount \ O $instance->order_amount = M $instance->mount_amount ~ R $instance->request</div>";
                                return "<div class=\"$CSS text-center\">$CALC</div>";
                            }),
            AdminColumn::text('client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Nr. Encomenda')
                    ->setWidth('80px')
                    ->setCallback(function ($instance) {
                                if ($instance->group != 0) {
                                    !($instance->group % 2) ? $color = "red" : $color = "aqua";
                                    return "<div class=\"bg-$color text-center\">$instance->group</div>";
                                } else {
                                    return "-";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            //AdminColumn::text('sum_orders.order')->setLabel('Ordem <br/>(de priodidade)'),
        ]);

        return $display;
    }

    public function onEdit($id) {

        $selectsInitJs = <<<HEREDOC
<script>
    window.onload = function() {               
        $('#product_id').trigger('select2:select');    
        $(document).ajaxSuccess(function(event, jqXHR, ajaxOptions, data) {
            if (data.selected) {
                $('#custom_id').val(data.selected).trigger('change');
            }
        });
        
    };
</script>
HEREDOC;

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('requestweek_edit.js', asset('assets/js/requestweek_edit.js'), ['admin-default']);
        $form->addHeader([
                    AdminFormElement::columns()
//                    ->addColumn([
//                        AdminFormElement::number('sum_orders.order', 'Ordem (de priodidade)')->setDefaultValue(0)->required()
//                            ], 2)
                    ->addColumn([

                        AdminFormElement::date('end_at', 'Data de Término (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_paint_at', 'Data de Término (Pintura)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_woodfinishing_at', 'Data de Término (Acabamento)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_wood_at', 'Data de Término (Carpintaria)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 3)
        ]);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('product_id', 'Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::dependentselect('custom_id', 'Tipo de Pintura')
                    ->setModelForOptions(new \App\Model\ProductCustomNxt)
                    ->setDataDepends(['product_id'])
                    ->setDisplay('name')
                    ->required()
                    ->setLoadOptionsQueryPreparer(function($item, $query) {                           
                            $checkColors = $query->selectRaw('products_custom_nxt.id as id, products_custom_nxt.name as name, products_custom.product_id as product_id')->join('products_custom', 'products_custom.custom_id', '=', 'products_custom_nxt.id')->where('product_id', $item->getDependValue('product_id'));
                            //dd($checkColors->count());
                            if ($checkColors->count() != 0) {
                                return $checkColors;
                            } 
                            else {  
                                return \App\Model\ProductCustomNxt::where('products_custom_nxt.id', 1);
                            }
                            
                        //}                         
                    }),AdminFormElement::html($selectsInitJs)], 2)
                    ->addColumn([
                        AdminFormElement::text('carpintaria', 'Carpintaria')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('woodfinishing', 'Acabamento')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('pintura', 'Pintura')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('montagem', 'Montagem')->required()
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('client', 'Cliente')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('obs', 'Observações')
                            ], 9)
                    ->addColumn([
                        AdminFormElement::number('group', 'Nr. Encomenda')->required()
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::checkbox('urgency', 'É Urgente?')
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::date('urgency_date', 'Data Urgente (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
                            ], 2)
        ]);

        $form->getButtons()
                ->hideSaveAndCreateButton()
//                ->hideSaveButton();
        ;
        return $form;
    }

    public function onCreate() {
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('requestweek.js', asset('assets/js/requestweek.js'), ['admin-default']);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::date('end_at', 'Data de Término (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_paint_at', 'Data de Término (Pintura)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_woodfinishing_at', 'Data de Término (Acabamento)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 3)
                    ->addColumn([
                        AdminFormElement::date('end_wood_at', 'Data de Término (Carpintaria)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 3)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group_quantity', '# de Produtos')->setDefaultValue(1)->required()
                            ], 1)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('product_id', 'Referência de Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::number('custom_id', 'Tipo de Pintura')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::number('quantity', 'Quantidade')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('carpintaria', 'Carpintaria')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('woodfinishing', 'Acabamento')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('pintura', 'Pintura')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('montagem', 'Montagem')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('obs', 'Observações')
                            ], 3)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group', 'Nr. Encomenda')->setDefaultValue(0)->required(),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('client', 'Cliente'),
                            ], 10)
                    ->addColumn([
                        AdminFormElement::checkbox('urgency', 'É urgente?'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::date('urgency_date', 'Data Urgente (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                            ], 2)
        ]);
        $form->addHeader([
            AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
//            AdminFormElement::number('sum_orders.order', 'Ordem')->setDefaultValue(0)->required(),
        ]);
//        $form->setButtons(new CustomFormButtons);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Criar Encomenda'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

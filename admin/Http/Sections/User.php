<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Role;
//use App\User;
//use SleepingOwl\Admin\Model\ModelConfiguration;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;






class User extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Funcionários';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        return AdminDisplay::datatables()
        ->with('roles')
        ->setHtmlAttribute('class', 'table-primary')
        ->setColumns([
            AdminColumn::link('name')->setLabel('Username')->setWidth('200px'),
            AdminColumn::text('bigname')->setLabel('Nome Completo')->setWidth('300px'),
            AdminColumn::text('password')->setLabel('Código')->setWidth('150px'),
            AdminColumn::lists('roles.label')->setLabel('Roles')->setWidth('200px'),
            ])->paginate(20);

        return $display;
    }

    public function onEdit($id) {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Username')->required()->unique(),
            AdminFormElement::text('bigname', 'Nome Completo')->required(),
            AdminFormElement::text('password', 'Password')->required()->unique(),
            AdminFormElement::multiselect('roles', 'Roles')->setModelForOptions(new Role())->setDisplay('name'),
            ]);
    }

    public function onCreate() {
        return $this->onEdit(null);
    }
}
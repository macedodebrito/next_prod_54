<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class PaletesTab extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'PaletesTab';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
//    public function onDisplay() {
//        // Display
//        $display = $this->fireEdit(1);
//        return $display;
//    }

    public function onEdit($id) {
        $formWeekSection = AdminForm::panel();
        $formWeekSection->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('limits_cnc')->setLabel('CNC')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_prep')->setLabel('Preparação')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_wood')->setLabel('Carpintaria')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_woodfinishing')->setLabel('Acabamento')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_woodfinishing_black')->setLabel('Acab. de Preto')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_paint')->setLabel('Pintura')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_mount')->setLabel('Montagem')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_wires')->setLabel('Redes')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('limits_locksmith')->setLabel('Serralharia')
                            ], 1)
        ]);
        $formWeekSection->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos Semanais das Secções'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);

        $formMonthlySection = AdminForm::panel();
        $formMonthlySection->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('m_limits_cnc')->setLabel('CNC')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_prep')->setLabel('Preparação')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_wood')->setLabel('Carpintaria')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_woodfinishing')->setLabel('Acabamento')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_woodfinishing_black')->setLabel('Acab. de Preto')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_paint')->setLabel('Pintura')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_mount')->setLabel('Montagem')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_wires')->setLabel('Redes')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('m_limits_locksmith')->setLabel('Serralharia')
                            ], 1)
        ]);
        $formMonthlySection->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos Mensais das Secções'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);

        $formWeekMount = AdminForm::panel();

        $formWeekMount->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('week_0')->setLabel('Semana 0')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('week_1')->setLabel('Semana 1')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('week_2')->setLabel('Semana 2')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('week_3')->setLabel('Semana 3')
                            ], 1)
        ]);

        $formWeekMount->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos Semanais da Montagem'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);
        
        $setupAll = AdminForm::panel();

        $setupAll->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('setup_week')->setLabel('Reset Semanal')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('setup_monthly')->setLabel('Reset Mensal')
                            ], 1)
        ]);        
        
        $setupAll->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);

        $tabs = AdminDisplay::tabbed();
        $tabs->appendTab($setupAll, 'Gerais');
        $tabs->appendTab($formWeekSection, 'Semanais das Secções');
        $tabs->appendTab($formMonthlySection, 'Mensais das Secções');
        $tabs->appendTab($formWeekMount, 'Semanais da Montagem');
        return $tabs;
    }

}

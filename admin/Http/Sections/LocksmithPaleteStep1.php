<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LocksmithPaleteStep1 extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Gestão de Paletes';

    /**
     * @var string
     */
    protected $alias = "paletes_serralharia";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(100)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->addScript('locksmith_paletes.js', asset('assets/js/locksmith_paletes.js'), ['admin-default'])
                ->addStyle('paletes.css', asset('assets/css/paletes.css'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('phc_id', 'desc')->orderBy('locksmith_week_id', 'desc')->where('status', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('locksmith_week');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->locksmith_week_id\" data-rel=\"$instance->phc_id\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF; cursor: pointer; font-size: 12px;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->locksmith_week_id\" data-rel=\"\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF; cursor: pointer; font-size: 12px;\">NXT $instance->locksmith_week_id</div>";
                                }
                                return $GG;
                            }),
                    AdminColumn::datetime('locksmith_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('90px')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('70px')
                    ->setCallback(function ($instance) {
                                $request_week_info = \App\Model\LocksmithWeek::where('id', $instance->locksmith_week_id)->first()->locksmith_id;
                                return "<span style=\"font-size: 12px;\">" . \App\Model\Locksmith::where('id', $request_week_info)->first()->name . "</span>";
                            }),
            AdminColumn::text('locksmith_week.quantity')->setLabel('#')->setWidth('1%'),
            AdminColumn::text('locksmith_week.client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Obs Encomenda')
                    ->setCallback(function ($instance) {
                                $request_obs = \App\Model\LocksmithWeek::where('id', $instance->locksmith_week_id)->first()->obs;
                                return $request_obs;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Obs Palete')
                    ->setCallback(function ($instance) {
                                if ($instance->store_obs) {
                                    return "[ <a rel=\"$instance->id\" class=\"first_obs\" href=\"#$instance->id\" data-rel=\"$instance->store_obs\"><i class=\"fa fa-pencil\"></i></a> ] $instance->store_obs";
                                } else {
                                    return "<a rel=\"$instance->id\" class=\"first_obs\" href=\"#$instance->id\"><i class=\"fa fa-plus-square-o fa-2x\"></i></a>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('CNC')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-aqua')
                    ->setCallback(function ($instance) {
                                $selected_class = "cnc";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\LocksmithWeek::where('id', $instance->locksmith_week_id)->first()->locksmith_id;

                                if (\App\Model\Locksmith::where('id', $request_week_info)->first()->$selected_class == 1) {

                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Armazém')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-orange')
                    ->setCallback(function ($instance) {
                                $selected_class = "store";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";


                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $DATA = $instance->$col_date;
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Validado por: <b>$nome</b><br/>($DATA)\"></i>";
                                    } else {
                                        return "<div class=\"bg-black\">DESC.</div>";
                                    }
                                } else {
                                    $selected_class_picks = $selected_class . "_picks";
                                    $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                    $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                    $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                    return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('')
                    ->setWidth('10px')
                    ->setCallback(function ($instance) {
                                if (Auth::user()->hasRole("admin")) {
                                    return "<i id=\"palete_edit_check_$instance->id\" data-rel=\"$instance->id\" class=\"fa fa-edit fa-2x palete_edit_checks\"></i>";
                                } else {
                                    return "<i class=\"fa fa-remove fa-2x\"></i>";
                                }
                            })
        ]);

        return $display;
    }

}

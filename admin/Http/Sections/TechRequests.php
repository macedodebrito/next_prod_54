<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
//
use App\Role;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class TechRequests extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Pedidos para a Técnica';

    public function getCreateTitle() {
        return 'Criar Novo Pedido para a Técnica';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->setNewEntryButtonText('Novo Pedido para a Técnica')
//                ->withPackage('jquery')
//                ->addScript('locksmithrequest.js', asset('assets/js/locksmithrequest.js'), ['admin-default'])
                ;

        $display->setApply(function($query) {
            $total = \App\Model\TechRequests::orderBy('order_at', 'asc')->get();
            $GG = [];
            foreach ($total as $value) {
                $efectuados = \App\Model\TechRequestsUser::where('tech_requests_id', $value->id)->where('published', '>', 0)->sum('value');
                if (!count($efectuados)) {
                    $efectuados = 0;
                }
                $missing = $value->value - $efectuados;
                if ($missing < 1) {
                    $GG['feitos'][] = $value->id;
                } else if ($missing > 0) {
                    $GG['falta'][] = $value->id;
                }
            }

            if (!isset($GG['feitos'])) {
//                $GG['feitos'] = "";
                unset($GG['feitos']);
            }
            if (!isset($GG['falta'])) {
//                $GG['falta'] = "";
                unset($GG['falta']);
            }

            if (isset($_GET['status'])) {
                
                if ($_GET['status'] == "T") {
                    $query->orderBy('order_at', 'asc');
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['feitos']);
                } else if ($_GET['status'] == 0) {
                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                if (isset($GG['falta'])) {
                    $query->whereIn('id', $GG['falta']);
                } else {
                    $query->orderBy('order_at', 'asc');
                }
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Tech::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('start_at')
                    ->setLabel('Data de Início Autorizado')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::datetime('order_at')
                    ->setLabel('Ordenação por Data')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('pp')->setLabel('Nº do Pedido'),
            AdminColumn::text('value')->setLabel('Quantidade')->setWidth('100px'),
                    AdminColumn::custom()->setLabel('Em Falta')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\TechRequests::where('id', $instance->id)->first();
                                $value = $total['value'];
                                $efectuados = \App\Model\TechRequestsUser::where('tech_requests_id', $instance->id)->where('published', '>', 0)->sum('value');
                                if (!count($efectuados)) {
                                    $efectuados = 0;
                                }
                                $missing = $value - $efectuados;

                                if ($missing > 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            }),
            AdminColumn::text('products.name')->setLabel('Peça')->append(AdminColumn::filter('product_id')),
                    AdminColumn::datetime('created_at')
                    ->setLabel('Criado a')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
        ]);

        return $display;
    }

    public function onEdit($id) {
        return AdminForm::form()->setItems([
                    AdminFormElement::date('start_at', 'Data para Início')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::date('order_at', 'Ordenação por Data')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::text('pp', 'Nº do Pedido')->required()->unique(),
                    AdminFormElement::text('value', 'Quantidade')->required(),
                    AdminFormElement::select('tech_id', 'Peça')->setModelForOptions(new \App\Model\Tech)->setDisplay('name')->required(),
        ]);
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ComsumablesOrders_History extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Histórico de Consumíveis';

    /**
     * @var string
     */
    protected $alias = "view_comsumables_history";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
        ;

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('created_at', 'desc')->where("validator_id", "!=", 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');


        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Data do Pedido')
                    ->setHtmlAttribute('class', 'bg-info text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Pedido Por')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->auth_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->auth_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->auth_id)->first()->bigname;
                                        return "$nome";
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Secção')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->section == "cnc") {
                                    return "CNC";
                                } elseif ($instance->section == "carpintaria") {
                                    return "Carpintaria";
                                } elseif ($instance->section == "acabamento") {
                                    return "Acabamento";
                                } elseif ($instance->section == "pintura") {
                                    return "Pintura";
                                } elseif ($instance->section == "montagem") {
                                    return "Montagem";
                                } elseif ($instance->section == "serralharia") {
                                    return "Serralharia";
                                } elseif ($instance->section == "tecnica") {
                                    return "Técnica";
                                } elseif ($instance->section == "redes") {
                                    return "Redes";
                                } elseif ($instance->section == "serigrafia") {
                                    return "Serigrafia";
                                } elseif ($instance->section == "filtros") {
                                    return "Filtros";
                                } elseif ($instance->section == "armazem") {
                                    return "Armazém de Prod. Acabado";
                                } elseif ($instance->section == "comsumables") {
                                    return "Armazém de Consumíveis";
                                } else {
                                    return "Outros";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Ficheiro')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                return "<a class=\"consumable_pdf\" id=\"$instance->id\" href=\"./pdf/$instance->id/comsumable_pdf\"><i class=\"fa fa-file-pdf-o fa-2x\"></i></a>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Validado por')
                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                if ($instance->validator_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->validator_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->validator_id)->first()->bigname;
                                        return "$nome<br/>$instance->updated_at";
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            })
        ]);

        return $display;
    }

}

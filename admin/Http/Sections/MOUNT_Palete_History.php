<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MOUNT_Palete_History extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Paletes - Montagem / Histórico';

    /**
     * @var string
     */
    protected $alias = "mount/paletes_history";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->addScript('paletes.js', asset('assets/js/paletes.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('request_week_id', 'desc')->where('store_id', '!=', 0)->where('auth_id', "!=", 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('request_week');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"text-center\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"text-center\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF;\">NXT $instance->request_week_id</div>";
                                }
                                return $GG;
                            }),            
                    AdminColumn::datetime('request_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('160px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                        $request_week_info_sql = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first();
                        $request_week_info = $request_week_info_sql->product_id;
                        return \App\Model\Product::where('id', $request_week_info)->first()->name."<span class=\"badge\" style=\"background-color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color_bg.";color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color."\">".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->name."</span>";
                            }),
            AdminColumn::text('request_week.montagem')->setLabel('Quantidade')->setWidth('100px'),
            AdminColumn::text('request_week.client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Autorizado Por')
                    ->setWidth('160px')->setHtmlAttribute('class', 'bg-info text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->store_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->store_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->store_id)->first()->bigname;
                                        return "$nome<br/>$instance->stored_date";
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">Por separar</div>";
                                }
                            }),
        ]);

        return $display;
    }

}

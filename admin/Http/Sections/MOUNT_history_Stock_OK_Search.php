<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

ini_set('max_execution_time', 360);

class MOUNT_history_Stock_OK_Search extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Listagem de Stock';

    /**
     * @var string
     */
    protected $alias = "stocks/mount_history_ok";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

        $display = AdminDisplay::datatables()
                ->setOrder([[0, 'desc']])
                ->paginate(25)
                ->withPackage('jquery')
                ->addScript('stock.js', asset('assets/js/stock.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('updated_at', 'desc')->where('published', 1)->orWhere('published', 2);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('updated_at')
                    ->setLabel('Efectuado em')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Referência')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                        // devia ser PaintUser para confirmar que existe na secção anterior, mas a pickagem passa isso à frente
                                $request_user = \App\Model\MountUser::where('request_id', $instance->request_id)->first();
                                $request_name = \App\Model\Request::where('id', $request_user->request_id)->first();
                                $product_name = \App\Model\Product::where('id', $request_name->product_id)->first();
                                return "NC$product_name->code";
                            }),                 
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $request_user = \App\Model\MountUser::where('request_id', $instance->request_id)->first();
                                $request_name = \App\Model\Request::where('id', $request_user->request_id)->first();
                                $product_name = \App\Model\Product::where('id', $request_name->product_id)->first();
                                return $product_name->name;
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('Quantidade')
//                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
//                    ->setCallback(function ($instance) {
//                                $entrance = MountUser::where('id', $instance->id)->first();
//                                $total_cnc = PaintUser::where('request_id', $instance->request_id)->sum('value');
//                                $total_request = Request::where('id', $instance->request_id)->first();
//                                $total_wood = MountUser::where('request_id', $instance->request_id)->sum('value');
//
//                                if ($total_wood > $total_cnc) {
//                                    $new_color = "bg-red";
//                                } else {
//                                    $new_color = "bg-success";
//                                }
//                                return "<div class=\"$new_color text-center\">$entrance->value</div>";
//                            }),
            AdminColumn::custom()
                    ->setLabel('Numero de Série')
                    ->setWidth('130px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {

                                $get_request = \App\Model\Request::where('id', $instance->request_id)->first();
                                $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();

                                $entrance = \App\Model\MountUser::where('id', $instance->id)->first();

                                return "<div class=\"text-center\">$get_product->code$get_request->pp$instance->serial</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Validado por')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('5%')
                    ->setCallback(function ($instance) {

                                $role_check = 0;
                                $role_id = \App\Role::where('name', 'stock')->first();
                                $manager_id = \App\Role::where('name', 'manager')->first();
                                $admin_id = \App\Role::where('name', 'admin')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $manager_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $admin_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                switch ($instance->published) {
                                    case 1:
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"stocks\" href=\"#back\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" class=\"stocks\" href=\"#sell\"><i class=\"fa fa-check fa-2x\"></i></a>";
                                        } else {
                                            return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"></i>";
                                        }
                                        break;
                                    default:
                                        return "<i class=\"fa fa-check fa-2x\"></i>";
                                        break;
                                }
                            })
        ]);

        return $display;
    }

}

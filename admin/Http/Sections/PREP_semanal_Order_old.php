<?php

use App\Model\PREP_semanal_Order;
use App\Model\RequestWeek;
use App\Model\WoodUser;
use App\Model\PaintUser;
use App\Model\MountUser;
use App\Model\RequestUser;
use App\Model\PrepUser;
use App\Model\Stock;
use App\Model\Request;
use App\Model\Product;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(PREP_semanal_Order::class, function (ModelConfiguration $model) {
    $model->setTitle('PREP - Semanal')->setAlias('prep/week_orders')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->setOrder([[1, 'asc']])->paginate(100);

        $display->setApply(function($query) {
            $query
                    ->orderBy('sum_orders.order', 'desc')
                    ->orderBy('sum_orders.cnc_date', 'asc')
                    ->select('prep_week.*', 'sum_orders.points as points', 'sum_orders.prep_date as prep_date', 'sum_orders.order_id AS order_id', 'sum_orders.id AS oid', 'sum_orders.prep_order_amount', 'sum_orders.prep_amount', 'sum_orders.prep_temp_amount', 'sum_orders.prep_calc_amount', 'sum_orders.order as sum_order')
                    ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
                    ->where('sum_orders.prep_temp_amount', '<', 0)
            ;
        });
        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_cnc_at)));

                        $CSS = "";
                        $datework = \Carbon\Carbon::parse($instance->end_cnc_at);
                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }


                        if ($curdate > $mydate) {
                            $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                        } else if ($curdate <= $mydate) {
                            if ($curdate == $mydate) {
                                $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                            } else {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            }
                        }
                        return $status_date;
                    }),
                    AdminColumn::datetime('end_cnc_at')
                    ->setLabel('Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
//                    AdminColumn::custom()
//                    ->setLabel('Data de Término')
//                    ->setWidth('100px')
//                    ->setHtmlAttribute('class', 'text-center')
//                    ->setCallback(function ($instance) {
//                                //return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->end_at)->subDays(7)->format('d.m.Y');
//                                return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->end_wood_at)->format('d.m.Y');
//                            }),
            AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setCallback(function ($instance) {
                               global $CALC;

                                if ($instance->prep_temp_amount <= 0) {
                                    if ($instance->prep_calc_amount < 0 && $instance->prep_temp_amount < 0) {
                                        $CALC = $instance->prep_order_amount;
                                    } else if ($instance->prep_calc_amount > 0 && $instance->prep_temp_amount < 0) {
                                        $CALC = $instance->prep_order_amount - $instance->prep_calc_amount;
                                    } else {
                                        $CALC = $instance->prep_order_amount - $instance->prep_calc_amount;
                                    }
                                } else if ($instance->temp_amount > 0) {
                                    $CALC = "OK";
                                } else {
                                    $CALC = "??";
                                }
                                $x_dump = \App\Model\Sum_Mount::select(\DB::raw('SUM(cnc) as cnc, SUM(prep) as prep, sum_mount.name, img_1, img_2, caption_1, caption_2, caption_3'))
                                        ->where('product_id', $instance->product_id)
                                        ->join('products', 'products.id', '=', 'sum_mount.product_id')
                                        ->groupBy('product_id')
                                        ->first();
                                $x_calc = $x_dump->cnc - $x_dump->prep;

                                if (!isset($GLOBALS[$instance->product_id]['f'])) {
                                    $GLOBALS[$instance->product_id]['f'] = $x_calc;
                                }

                                $GLOBALS[$instance->product_id]['f2'] = $GLOBALS[$instance->product_id]['f'] - $CALC;

                                $F = $GLOBALS[$instance->product_id]['f'];
                                $F2 = $GLOBALS[$instance->product_id]['f2'];

                                $GLOBALS[$instance->product_id]['f'] = $GLOBALS[$instance->product_id]['f2'];

                                if ($F > 0 && $F2 < 0) {
                                    $CSS = "<i style=\"color: orange;\" class=\"fa fa-circle fa-2x\"></i>";
                                } else if ($F <= 0 && $F2 < 0) {
                                    $CSS = "<i style=\"color: red;\" class=\"fa fa-circle fa-2x\"></i>";
                                } else {
                                    $CSS = "<i style=\"color: green;\" class=\"fa fa-circle fa-2x\"></i>";
                                }

                                $IMG = "";
                                $IMG_1 = "";
                                $IMG_2 = "";
                                $IMG_3 = "";
                                $CAPTION_1 = "";
                                $CAPTION_2 = "";
                                $CAPTION_3 = "";

                                if ($x_dump->img_1) {
                                    if ($x_dump->caption_1) {
                                        $CAPTION_1 = $x_dump->caption_1;
                                    }
                                    $IMG_1 = "<div><span><img src=\"../$x_dump->img_1\"></span><i>$CAPTION_1</i></div>";
                                }
                                if ($x_dump->img_2) {
                                    if ($x_dump->caption_2) {
                                        $CAPTION_2 = $x_dump->caption_2;
                                    }
                                    $IMG_2 = "<div><span><img src=\"../$x_dump->img_2\"></span><i>$CAPTION_2</i></div>";
                                }
                                if ($x_dump->img_3) {
                                    if ($x_dump->caption_3) {
                                        $CAPTION_3 = $x_dump->caption_3;
                                    }
                                    $IMG_3 = "<div><span><img src=\"../$x_dump->img_3\"></span><i>$CAPTION_3</i></div>";
                                }

                                if (($x_dump->img_1) || ($x_dump->img_2) || ($x_dump->img_3)) {
                                    $IMG = " <i class=\"watch_product_img fa fa-picture-o fa-2x\"></i><div>$IMG_1 $IMG_2 $IMG_3</div>";
                                }

                                global $POINTS;
                                $POINTS = $instance->points;
                                return "$CSS " . $x_dump->name . $IMG;
                            }),
//                           
            AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('5%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                return $CALC;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                               global $CALC;
                                global $POINTS;
                                $POINTS = $CALC * $POINTS;
                                return $POINTS;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente')
        ]);

        return $display;
    });
    //    
});

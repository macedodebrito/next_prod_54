<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Distro extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Distribuição';

    /**
     * @var string
     */
    protected $alias = "admin/distributors";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatables()
                ->setOrder([[5, 'desc']])
                ->paginate(50)
                ->withPackage('jquery')
                ->addScript('distro.js', asset('assets/js/distro.js'), ['admin-default']);

        $display->setHtmlAttribute('class', 'table-info table-hover');


        $display->setColumns([
            AdminColumn::text('market')->setLabel('Mercado'),
            AdminColumn::text('name')->setLabel('Cliente'),
            AdminColumn::text('type')->setLabel('Tipo de Cliente'),
                    AdminColumn::datetime('first_buy', '1ª Compra')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
            AdminColumn::text('seller')->setLabel('Vendedor'),
                    AdminColumn::custom()
                    ->setLabel('Encomendas em Aberto')
                    ->setCallback(function ($instance) {
                                switch ($instance->open_orders) {
                                    case 0: return "<div class=\"bg-danger\">Não</div>";
                                        break;
                                    default: return "<div class=\"bg-success\">Sim</div>";
                                        break;
                                }
                            }),
                    AdminColumn::datetime('last_buy', 'Última Encomenda')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
            AdminColumn::text('anual_billing')->setLabel("Facturação " . date("Y")),
            AdminColumn::text('previous_anual_billing')->setLabel("Facturação " . (date("Y") - 1)),
        ]);

        return $display;
    }

}

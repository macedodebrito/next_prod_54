<?php

use App\Model\CNC_total_v2;
use SleepingOwl\Admin\Model\ModelConfiguration;

global $TOTAL_PP;
global $TOTAL_CNC;
global $TOTAL_PREP;
global $TOTAL_CARPINTARIA;
global $TOTAL_ACABAMENTO;
global $TOTAL_PINTURA;
global $TOTAL_MONTAGEM;
global $TOTAL_PP_ABERTO;
global $TOTAL_PRODUCAO; 

AdminSection::registerModel(CNC_total_v2::class, function (ModelConfiguration $model) {
    $model->setTitle('Painel de Bordo')->setAlias('requests/cnc_total_v2')->disableDeleting()->disableRestoring();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables();

        $display->setApply(function($query) {
            $query->select(DB::raw('encomendas, name, color_name, color_hex, color_hex_bg, SUM(pp) as pp, SUM(cnc) as cnc, SUM(prep) as prep, SUM(carpintaria) as carpintaria, SUM(acabamento) as acabamento, SUM(pintura) as pintura, SUM(montagem) as montagem, SUM(encomendas) as encomendas'))
                    ->orderBy('name', 'asc')
                    ->groupBy('product_id')
                    ->groupBy('color_name')
                    ->where('type', 'mount');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                AdminColumn::custom()
                ->setLabel('Produto')
                ->setCallback(function ($instance) {
                    return "<div>$instance->name</div><span class=\"badge\" style=\"color:$instance->color_hex;background-color:$instance->color_hex_bg\">$instance->color_name</span>";
                }),
                    AdminColumn::custom()
                    ->setLabel('Total dos PP')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                $total = $instance->pp;
                                global $TOTAL_PP;
                                $TOTAL_PP = $total;
                                if (!count($total)) {
                                    $total = 0;
                                }
                                if ($total < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($total == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-warning";
                                }
                                return "<div class=\"$new_color text-center\">$total</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('PP em aberto (CNC)')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $abertos = $instance->pp - $instance->cnc;
                                global $TOTAL_CNC;
                                $TOTAL_CNC = $abertos;
                                if ($abertos < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($abertos == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-danger";
                                }
                                return "<div class=\"$new_color text-center\">$abertos</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível PREPARAÇÃO')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $PREP = $instance->cnc - $instance->prep;
                                global $TOTAL_PREP;
                                $TOTAL_PREP = $PREP;
                                if ($PREP < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($PREP == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$PREP</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível CARPINTARIA')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $CARPINTARIA = $instance->prep - $instance->carpintaria;
                                global $TOTAL_CARPINTARIA;
                                $TOTAL_CARPINTARIA = $CARPINTARIA;
                                if ($CARPINTARIA < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($CARPINTARIA == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$CARPINTARIA</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível ACABAMENTO')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $ACABAMENTO = $instance->carpintaria - $instance->acabamento;
                                global $TOTAL_ACABAMENTO;
                                $TOTAL_ACABAMENTO = $ACABAMENTO;
                                if ($ACABAMENTO < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($ACABAMENTO == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$ACABAMENTO</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível PINTURA')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $PINTURA = $instance->acabamento - $instance->pintura;
                                global $TOTAL_PINTURA;
                                $TOTAL_PINTURA = $PINTURA;
                                if ($PINTURA < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($PINTURA == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$PINTURA</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível MONTAGEM')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $MONTAGEM = $instance->pintura - $instance->montagem;
                                global $TOTAL_MONTAGEM;
                                $TOTAL_MONTAGEM = $MONTAGEM;
                                if ($MONTAGEM < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($MONTAGEM == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$MONTAGEM</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Total PP em aberto')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                global $TOTAL_CNC;
                                global $TOTAL_PREP;
                                global $TOTAL_CARPINTARIA;
                                global $TOTAL_ACABAMENTO;
                                global $TOTAL_PINTURA;
                                global $TOTAL_MONTAGEM;

                                $TOTAL_PP = $TOTAL_CNC + $TOTAL_PREP + $TOTAL_CARPINTARIA + $TOTAL_ACABAMENTO + $TOTAL_PINTURA + $TOTAL_MONTAGEM;
                                
                                global $TOTAL_PP_ABERTO;
                                global $TOTAL_PRODUCAO;
                                $TOTAL_PP_ABERTO = $TOTAL_PP;
                                $TOTAL_PRODUCAO = $TOTAL_PREP + $TOTAL_CARPINTARIA + $TOTAL_ACABAMENTO + $TOTAL_PINTURA + $TOTAL_MONTAGEM;
                                
                                
                                if ($TOTAL_PP < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($TOTAL_PP == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-gray";
                                }
                                return "<div class=\"$new_color text-center\">$TOTAL_PP</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Encomendas de Clientes')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $ENCOMENDAS;
                                $CONTA = $instance->encomendas - $instance->montagem;
                                $ENCOMENDAS = $CONTA;
                                if ($CONTA > 0) {
                                    return "<div class=\"bg-red text-center\">$CONTA</div>";
                                } else {
                                    return "<div class=\"bg-aqua text-center\">$CONTA</div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Sobras de PP')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                global $TOTAL_PP_ABERTO;
                                global $ENCOMENDAS;
                                $SOBRAS = $TOTAL_PP_ABERTO - $ENCOMENDAS;
                                if ($SOBRAS < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($SOBRAS == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-warning";
                                }
                                return "<div class=\"$new_color text-center\">$SOBRAS</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Total em Produção')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
//                                $total_a = Request::where('product_id', $instance->product_id)->get();
//                                $ARMAZEM = 0;
//                                foreach ($total_a as $value1) {
//                                    $ARMAZEM_a = MountUser::where('request_id', $value1->id)->where('published', 0)->get();
//                                    if (!count($ARMAZEM)) {
//                                        $ARMAZEM = $ARMAZEM + 0;
//                                    } else {
//                                        foreach ($ARMAZEM_a as $value) {
//                                            $ARMAZEM = $ARMAZEM + $value->value;
//                                        }
//                                    }
//                                }                                
                                global $TOTAL_PRODUCAO;
                                $TOTAL_PROD = $TOTAL_PRODUCAO;
                                if ($TOTAL_PROD < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($TOTAL_PROD == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-danger";
                                }
//                                return "<div class=\"$new_color text-center\">$TOTAL_PROD (+ $ARMAZEM)</div>";
                                return "<div class=\"$new_color text-center\">$TOTAL_PROD</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponivel em Produção')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $TOTAL_PRODUCAO;
                                global $ENCOMENDAS;
                                $TOTAL_PROD = $TOTAL_PRODUCAO;
                                $DISP_PROD = $TOTAL_PRODUCAO - $ENCOMENDAS;
                                if ($DISP_PROD < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($DISP_PROD == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$DISP_PROD</div>";
                            }),
        ]);

        return $display;
    });
    //
});

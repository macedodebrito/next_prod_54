<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//use AdminColumnFilter;
//
use App\Model\UserLogin;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

class Clock_Day extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;
    protected $model;
    
    public function initialize() {
    }
    
    /**
     * @var string
     */
    protected $title = 'Relógio de Ponto';

    /**
     * @var string
     */
    protected $alias = "clock_days";

    /**
     * @return DisplayInterface
     */

    public function onDisplay() {
        if (isset($_GET['id']) && isset($_GET['m']) && isset($_GET['y']) && isset($_GET['d'])) {
            if ($_GET['m'] == "1") {
                $MES = "Janeiro";
            } else if ($_GET['m'] == "2") {
                $MES = "Fevereiro";
            } else if ($_GET['m'] == "3") {
                $MES = "Março";
            } else if ($_GET['m'] == "4") {
                $MES = "Abril";
            } else if ($_GET['m'] == "5") {
                $MES = "Maio";
            } else if ($_GET['m'] == "6") {
                $MES = "Junho";
            } else if ($_GET['m'] == "7") {
                $MES = "Julho";
            } else if ($_GET['m'] == "8") {
                $MES = "Agosto";
            } else if ($_GET['m'] == "9") {
                $MES = "Setembro";
            } else if ($_GET['m'] == "10") {
                $MES = "Outubro";
            } else if ($_GET['m'] == "11") {
                $MES = "Novembro";
            } else {
                $MES = "Dezembro";
            }
            $YEAR = $_GET['y'];
            $DAY = $_GET['d'];
            $user_name = \App\Model\Clock::where('id', $_GET['id'])->first()->bigname;
        }

        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Novo Registo')
                ->paginate(10);        
        
        $display->setTitle("Funcionário: <b>$user_name</b> (Registos para o dia $DAY de $MES de $YEAR)");        
        $display->setApply(function($query) {
            $dump = \App\Model\Clock_month::select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day, created_at, login_logout, id"))
                    ->orderBy('year', 'desc')
                    ->orderBy('month', 'desc')
                    ->orderBy('day', 'asc')
                    ->where('user_id', $_GET['id'])
                    ->get();
            $GG = [];
            foreach ($dump as $value) {
                if (($value->year == $_GET['y']) && ($value->month == $_GET['m']) && ($value->day == $_GET['d'])) {
                    $GG[] = $value->id;
                }
            }
            $query->whereIn('id', $GG)->get();
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Hora')
                    ->setCallback(function ($instance) {
                                return "<div> <a href=\"./clock_days/$instance->id/edit\">$instance->created_at</a></div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('200px')
                    ->setCallback(function ($instance) {

                                if ($instance->login_logout == "E") {
                                    $new_color = "bg-green";
                                    $status = "ENTRADA";
                                } else if ($instance->login_logout == "S") {
                                    $new_color = "bg-red";
                                    $status = "SAÍDA";
                                }

                                return "<div class=\"$new_color text-center\"> $status </div>";
                            }),
        ]);
        $display->setDatatableAttributes(['searching' => false]);
        return $display;
    }
    public function onEdit($id) {
        if (isset($_GET['id'])) {
            $ID = $_GET['id'];
        } else {
            $ID = "";
        }
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('clock.js', asset('assets/js/clock.js'), ['admin-default']);

        $form->setItems([
            AdminFormElement::date('created_at', 'Hora de Registo')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::select('login_logout', 'Tipo de Registo')->setOptions(['E' => 'Entrada', 'S' => 'Saída'])->required(),
            AdminFormElement::hidden('user_id')->setDefaultValue($ID)->required(),
        ]);
        
        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar Registo'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);  
        
        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

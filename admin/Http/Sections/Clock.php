<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\UserLogin;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Clock extends Section {  
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Relógio de Ponto';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()->paginate(25);
        $display->getColumns()->getControlColumn()->setDeletable(false)->setEditable(true);
        $display->setApply(function($query) {
            $query->orderBy('bigname', 'asc')->get();
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Funcionário')
                    
                    ->setCallback(function ($instance) {
                                return "<a href=\"clock_users?id=$instance->id\">$instance->bigname</a>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('200px')
                    ->setCallback(function ($instance) {
                                $dump = \App\Model\UserLogin::where('user_id', $instance->id)->orderBy('created_at', 'desc')->first();
                                if (count($dump) > 0) {
                                    if ($dump->login_logout == "E") {
                                        $new_color = "bg-green";
                                        $new_info = "ACTIVO";
                                    } else if ($dump->login_logout == "S") {
                                        $new_color = "bg-red";
                                        $new_info = "INACTIVO";
                                    }
                                } else {
                                    $new_color = "bg-black";
                                    $new_info = "DESCONHECIDO";
                                }
                                return "<div class=\"$new_color text-center\">$new_info</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Hora de Registo')
                    ->setWidth('200px')                                    
                    ->setCallback(function ($instance) {
                                $dump = \App\Model\UserLogin::where('user_id', $instance->id)->orderBy('created_at', 'desc')->first();
                                if (count($dump) > 0) {                                                                  
                                        $new_info = Carbon::createFromFormat('Y-m-d H:i:s', $dump->created_at)->format('d-m-Y H:i');
                                } else {                                    
                                    $new_info = "SEM REGISTOS";
                                }
                                return "<div class=\"text-center\">$new_info</div>";
                            })        ]);

        return $display;
    }   
}
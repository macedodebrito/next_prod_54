<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WOOD_open extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'WOOD - Entradas';

    /**
     * @var string
     */
    protected $alias = "wood/open";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
                ->withPackage('jquery')
                ->addScript('prep_create.js', asset('assets/js/prep_create.js'), ['admin-default'])
                ->setApply(function($query) {             
                        $query->orderBy('published', 'asc')->orderBy('created_at', 'desc');
                });
      

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Data de Entrada')
                    ->setWidth('160px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Nº Pedido de Produção')
                    ->setWidth('20%')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                return $pp->pp;
                            }),
                            AdminColumn::custom()
                            ->setLabel('Produto')
                            ->setWidth('100px')
                            ->setCallback(function ($instance) {
                                        $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();                               
                                        $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->request_pp['custom_id'])->first();
                                        $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                                        return "<div text-center\">$product_name->name</div>$COLOR";
                                    }),
                     AdminColumn::custom()
                    ->setLabel('Maquinado')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {

                                $total_request = \App\Model\PrepUser::where('id', $instance->id)->first();

                                if ($total_request->value > 0) {
                                    $new_color = "bg-success";
                                } else {
                                    $new_color = "bg-aqua";
                                }
                                global $VALOR;
                                $VALOR = $total_request->value;
                                return "<div class=\"$new_color text-center\">$total_request->value</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Operador Preparação')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                                $POINTS = $VALOR * $product_name->points;
                                return $POINTS;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Validado por')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-aqua text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->valid_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->valid_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->valid_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">Por validar</div>";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('180px')
                    ->setCallback(function ($instance) {
                                // warning 
                                // check = 1
                                // remove
                                // user-times = -1
                                $default_icon = "fa-circle-o-notch fa-spin fa-2x";

                                $role_check = 0;
                                $role_id = \App\Role::where('name', 'carpintaria')->first();
                                $manager_id = \App\Role::where('name', 'manager')->first();
                                $admin_id = \App\Role::where('name', 'admin')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $manager_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $admin_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                
                                switch ($instance->published) {
                                    case 1:
                                        $default_icon = "fa-check fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    case -1:
                                        $default_icon = "fa-user-times fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    default:
                                        if ($role_check > 0) {
                                            //return "<a id=\"$instance->id\" class=\"accept_product\" href=\"#remove_product\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" class=\"accept_product\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a>";
                                            //return "<span class=\"picks\"><a id=\"$instance->id\" rel=\"\" class=\"accept_product\" href=\"#remove_product\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" rel=\"\" class=\"accept_product\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span><span class=\"picks_code\"><i class=\"fa $default_icon\"></i> <input id=\"picks_code\" type=\"password\" size=\"10\" class=\"form-control\"></span>";
                                            return "<span id=\"picks_span_picks_code_$instance->id\" class=\"picks\"><a id=\"$instance->id\" rel=\"\" class=\"accept_product\" href=\"#remove_product\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" rel=\"\" class=\"accept_product\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span><span id=\"h_picks_code_$instance->id\" class=\"picks_code\"><i class=\"fa $default_icon\"></i> <input id=\"picks_code_$instance->id\" type=\"password\" size=\"10\" class=\"form-control picks_code_pass\"></span>";
                                        } else {
                                            return "<i class=\"fa $default_icon\"></i>";
                                        }
                                        break;
                                }
                                
                            })
        ]);

        return $display;
    }

}

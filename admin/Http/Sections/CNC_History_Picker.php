<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CNC_History_Picker extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //dd($_REQUEST);
            unset($model->picker);
            unset($model->picker2);
            unset($model->total);
            unset($model->published);

            // INTEGRAÇÃO NXT2PHC
            //unset($model->n2p_custom);
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //dd($_REQUEST);
            $id = \App\Model\RequestUser::orderBy('id', 'desc')->first()->id;
            \App\Model\RequestUser::where('id', $id)->delete();

            $create_date = Carbon::now()->toDateTimeString();
            foreach ($_REQUEST['obs'] as $key => $value) {
                $get_all_pp = \App\Model\Request::where('product_id', $_REQUEST['product_ids'][$key])->where('custom_id', $_REQUEST['custom_ids'][$key])->orderBy('pp', 'asc')->get();
                foreach ($get_all_pp as $pp) {
                    if (\App\Model\RequestUser::where('request_id', $pp->id)->sum('value') < \App\Model\Request::where('id', $pp->id)->sum('value')) {
                        $request_id = $pp->id;
                        break;
                    }
                }

                //$result = \App\Model\RequestUser::where('request_id', \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id)->count();

                \App\Model\RequestUser::insert(
                        array('id' => null,
                            'request_id' => $request_id,
                            'user_id' => $_REQUEST['user_id'],
                            'valid_id' => $_REQUEST['user_id'],
                            'value' => 1,
                            'obs' => $_REQUEST['obs'][$key],
                            'published' => 1,
                            'prep' => 1,
                            'created_at' => $create_date,
                            'updated_at' => $create_date
                        )
                );

                //$object_request = \App\Model\Request::where('pp', $request_id)->first();
            }

            // check and group: DATE + PP + OBS
            $get_grouped_products = \App\Model\RequestUser::where('value', 1)->where('created_at', $create_date)->groupBy('request_id')->get();

            foreach ($get_grouped_products as $pp2) {
                $sum = \App\Model\RequestUser::where('request_id', $pp2->request_id)->where('value', 1)->where('created_at', $create_date)->sum('value');
                if ($sum > 1) {
                    \App\Model\RequestUser::insert(
                            array('id' => null,
                                'request_id' => $pp2->request_id,
                                'user_id' => $_REQUEST['user_id'],
                                'valid_id' => $_REQUEST['user_id'],
                                'value' => $sum,
                                'published' => 1,
                                'prep' => 1,
                                'created_at' => $create_date,
                                'updated_at' => $create_date
                            )
                    );

                    \App\Model\RequestUser::where('request_id', $pp2->request_id)->where('value', 1)->where('created_at', $create_date)->delete();
                }
            }

            //$get_grouped_products = \App\Model\RequestUser::where('end_at', $result_encomenda->end_at)->where('quantity', $r_quantity)->where('client', $r_client)->where('product_id', $r_product_id)->update(['end_at' => $r_end_at, 'montagem' => $week_missing[$triple], 'requestweek_id' => $triple]);
            \Artisan::call('check_updates');
        });
    }

    /**
     * @var string
     */
    protected $title = 'CNC - Histórico Picagem';

    public function getCreateTitle() {
        return 'A enviar para a Preparação';
    }

    /**
     * @var string
     */
    protected $alias = "requests/cnc_history_picker";

    /**
     * @return DisplayInterface
     */
    public function onCreate() {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('cnc_create_picker.js', asset('assets/js/cnc_create_picker.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('picker')
                        ->setLabel('Código de Barras (Coluna)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::hidden('picker2')
                        ->setLabel('Código de Barras (Caixa)')
                            ], 5)
                    ->addColumn([
                        AdminFormElement::select('total', 'Quantidade')->setDefaultValue("0")
                            ], 2)
        ]);
//        $SELECT = [];
//        $SELECT[''] = '-';
//        $SQL = \App\Model\Customizations::where("tecnica", 1)->orderBy('name', 'asc')->get();
//        foreach ($SQL as $item) {
//            $SELECT["$item->value"] = $item->name;
//        }
//        $form->addHeader([
//                    AdminFormElement::columns()
//                    ->addColumn([
//                        AdminFormElement::select('n2p_custom', 'Customização')->setOptions($SELECT)->setDefaultValue("")
//                            ], 2)
//                    ->addColumn([
//                        AdminFormElement::text('internal_serial')->setLabel('Serial Incorporado')
//                            ], 2)
//        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('valid_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('value')->setDefaultValue(1)
                    ->required(),
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para a Preparação'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

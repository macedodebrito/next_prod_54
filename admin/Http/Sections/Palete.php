<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Palete extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Gestão de Paletes';

    /**
     * @var string
     */
    protected $alias = "paletes";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(200)
                ->setOrder([[2, 'asc']])
                ->withPackage('jquery')
                ->addScript('paletes.js', asset('assets/js/paletes.js'), ['admin-default'])
                ->addStyle('paletes.css', asset('assets/css/paletes.css'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('phc_id', 'desc')->orderBy('request_week_id', 'desc')->where('status', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('request_week');
        $display->setColumns([
                    AdminColumn::custom()
                    ->setWidth('6px')
                    ->setCallback(function ($instance) {
                                $to_color = \Carbon\Carbon::parse('now')->diffInDays($instance->request_week['end_at'], false) > 5 ? 'to-color-green' : 'to-color-red';
                                return "<div class=\"$to_color\"></div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->request_week_id\" data-rel=\"$instance->phc_id\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF; cursor: pointer; font-size: 12px;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->request_week_id\" data-rel=\"\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF; cursor: pointer; font-size: 12px;\">NXT $instance->request_week_id</div>";
                                }
                                return $GG;
                            }),
                    AdminColumn::datetime('request_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('90px')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('70px')
                    ->setCallback(function ($instance) {
                                $request_week_info_sql = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first();
                                $request_week_info = $request_week_info_sql->product_id;
                                return "
                                <span style=\"font-size: 12px;\">" . \App\Model\Product::where('id', $request_week_info)->first()->name . "</span>
                                <span class=\"badge\" style=\"background-color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color_bg.";color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color."\">".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->name."</span>
                                ";
                            }),
            AdminColumn::text('request_week.montagem')->setLabel('#')->setWidth('50px'),
            AdminColumn::text('request_week.client')->setLabel('Cliente')->setWidth('80px'),
                    AdminColumn::custom()
                    ->setLabel('Obs Encomenda')
//                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                $request_obs = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->obs;
                                return $request_obs;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Obs Palete')
//                    ->setWidth('200px')
                    ->setCallback(function ($instance) {
                                if ($instance->store_obs) {
                                    return "[ <a rel=\"$instance->id\" class=\"first_obs\" href=\"#$instance->id\" data-rel=\"$instance->store_obs\"><i class=\"fa fa-pencil\"></i></a> ] $instance->store_obs";
                                } else {
                                    return "<a rel=\"$instance->id\" class=\"first_obs\" href=\"#$instance->id\"><i class=\"fa fa-plus-square-o fa-2x\"></i></a>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Levantamento Palete')
//                    ->setWidth('200px')
//                    ->setCallback(function ($instance) {
//                                $get_product_value= $instance->request_week['montagem'];
//                                $levantamento = \App\Model\PaleteLevantamento::where('object_id', $instance->id)->where('section', 'montagem')->first();
//                                if ($levantamento) {
//                                    $DATA = $levantamento->levantamento_at;
//                                    if ($levantamento->value == 0) {
//                                        $levantado = "(?)";
//                                    }
//                                    else {
//                                        $levantado = "($levantamento->value)";
//                                    }
//                                    $nome = \App\User::where('id', $levantamento->levantamento_user_id)->first()->bigname;
//                                    return "<div style=\"text-align: center;\"><i class=\"fa fa-2x fa-check-square-o\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Levantado por: <b>$nome</b><br/>($DATA)\"></i> $levantado</div>";
//                                }
//                                else {
//                                    return "<div style='text-align: center; cursor: pointer;'><i data-value=\"$get_product_value\" data-rel=\"$instance->id\" class=\"fa fa-2x fa-square-o levantamento_palete_overlay\"></i></div>";
//                                }
//                                
//                            }),
                    ->setCallback(function ($instance) {
                                $get_product_value = $instance->request_week['montagem'];
                                $levantamento = \App\Model\PaleteLevantamento::where('object_id', $instance->id)->where('section', 'montagem')->get();
                                $levantamento_listagem = "";
                                $levantamento_count = 0;
                                if (count($levantamento) > 0) {
                                    foreach ($levantamento as $value) {
                                        $DATA = $value->levantamento_at;
                                        if ($value->value == 0) {
                                            $levantado = "<span style='color: yellow; font-weigth: bold;'>(?)</span>";
                                        } else {
                                            $levantado = "<span style='color: limegreen; font-weigth: bold;'>($value->value)</span>";
                                        }
                                        $levantamento_count = $levantamento_count + $value->value;
                                        $nome = \App\User::where('id', $value->levantamento_user_id)->first()->bigname;
                                        $levantamento_listagem .= "<div style='text-align: left;'>$levantado Levantado por: <b>$nome</b><br/>($DATA)</div>";
                                    }
                                    $max_value = $get_product_value - $levantamento_count;
                                    return "<div style='text-align: center; cursor: pointer;'><i class=\"fa fa-2x fa-list\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"$levantamento_listagem\"></i> $levantamento_count/$get_product_value <i data-value=\"$max_value\" data-rel=\"$instance->id\" class=\"fa fa-2x fa-plus-square-o levantamento_palete_overlay\"></i></div>";
                                } else {
                                    return "<div style='text-align: center; cursor: pointer;'><i data-value=\"$get_product_value\" data-rel=\"$instance->id\" class=\"fa fa-2x fa-square-o levantamento_palete_overlay\"></i></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Armazém')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-orange')
                    ->setCallback(function ($instance) {
                                $selected_class = "store";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {
                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $DATA = $instance->$col_date;
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Validado por: <b>$nome</b><br/>($DATA)\"></i>";
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Técnica')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-maroon')
                    ->setCallback(function ($instance) {
                                $selected_class = "tech";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {

                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serigrafia')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-aqua')
                    ->setCallback(function ($instance) {
                                $selected_class = "serigrafia";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {
                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serralharia')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-info')
                    ->setCallback(function ($instance) {
                                $selected_class = "locksmith";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {

                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Redes')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-purple')
                    ->setCallback(function ($instance) {
                                $selected_class = "wires";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {
                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('PVC')
//                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
//                    ->setCallback(function ($instance) {
//                                $selected_class = "pvc";
//                                $selected_class_auth = $selected_class . "_auth";
//                                $ID = $instance->id;
//                                $col_id = $selected_class . "_id";
//                                $col_date = $selected_class . "_date";
//                                $col_auth = $selected_class . "_auth_id";
//                                $col_auth_date = $selected_class . "_auth_date";
//
//                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;
//
//                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {
//                                    if ($instance->$col_id != 0) {
//                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
//                                        if (count($valid_id) > 0) {
//                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
//                                            if ($instance->$col_auth == 0) {
//                                                $DATA = $instance->$col_date;
//                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
//                                            } else {
//                                                $DATA = $instance->$col_date;
//                                                $AUTH_DATA = $instance->$col_auth_date;
//                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
//                                                return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
//                                            }
//                                        } else {
//                                            return "<div class=\"bg-black\">DESC.</div>";
//                                        }
//                                    } else {
//                                        $selected_class_picks = $selected_class . "_picks";
//                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
//                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
//                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
//                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
//                                    }
//                                } else {
//                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
//                                }
//                            }),
            AdminColumn::custom()
                    ->setLabel('Embalagem CNC')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-green')
                    ->setCallback(function ($instance) {
                                $selected_class = "packing_cnc";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {

                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Embalagem Espuma')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center bg-lime')
                    ->setCallback(function ($instance) {
                                $selected_class = "packing_espuma";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $request_week_info = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first()->product_id;

                                if (\App\Model\Product::where('id', $request_week_info)->first()->$selected_class == 1) {

                                    if ($instance->$col_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                            if ($instance->$col_auth == 0) {
                                                $DATA = $instance->$col_date;
                                                return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"  data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/><span style='padding: 4px;' class='bg-red'>Por Validar</span> <span class='$selected_class span_second' rel='' style='display: none;'><a id='$ID' rel='$selected_class_auth' class='second_validator' href='#accept_product'><i class='fa fa-check fa-2x'></i></a></span><input class='picks_auth_sections' size='5' style='color: #000000;' type='password'/>\"></i>";
                                            } else {
                                                $DATA = $instance->$col_date;
                                                $AUTH_DATA = $instance->$col_auth_date;
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                                return "<i class=\"fa fa-check fa-2x\" data-html=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: <b>$nome</b><br/>($DATA)<br/>Validado por: <b>$nome_auth</b><br/>($AUTH_DATA)\"></i>";
                                            }
                                        } else {
                                            return "<div class=\"bg-black\">DESC.</div>";
                                        }
                                    } else {
                                        $selected_class_picks = $selected_class . "_picks";
                                        $selected_class_instance = "span_" . $selected_class . "_" . $instance->id;
                                        $selected_class_picks_instance = $selected_class . "_" . $instance->id;
                                        $selected_class_code = "input_" . $selected_class . "_" . $instance->id;
                                        return "<div class=\"bg-red\">Por separar</div><br/><span class=\"$selected_class $selected_class_picks $selected_class_instance\"><a id=\"$instance->id\" rel=\"$selected_class\" class=\"first_validator\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"$selected_class_picks_instance\" rel=\"$selected_class\" type=\"password\" size=\"5\" class=\"form-control picks_code_sections\"><input type=\"hidden\" id=\"$selected_class_code\">";
                                    }
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('')
                    ->setWidth('10px')
                    ->setCallback(function ($instance) {
                                if (Auth::user()->hasRole("admin")) {
                                    return "<i id=\"palete_edit_check_$instance->id\" data-rel=\"$instance->id\" class=\"fa fa-edit fa-2x palete_edit_checks\"></i>";
                                } else {
                                    return "<i class=\"fa fa-remove fa-2x\"></i>";
                                }
                            })
        ]);

        return $display;
    }

}

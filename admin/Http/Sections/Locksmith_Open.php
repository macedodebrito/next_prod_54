<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Locksmith_Open extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Serralharia - Abertos';

    /**
     * @var string
     */
    protected $alias = 'serralharia/open';

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display
        static $new_color = null;
        $display = AdminDisplay::datatables();

        $display->getColumns()->getControlColumn()->setDeletable(false);
        
        $display->setApply(function($query) {
            $total = \App\Model\LocksmithRequests::get();
            $get_ready = [];
            foreach ($total as $key => $value) {
                $efectuados = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $value->id)->where('published', 1)->sum('value');
                if ($value->value - $efectuados != 0) {
                    $get_ready[] = $value->id;
                }
            }
            $query->whereIn('id', $get_ready)->orderBy('order_at', 'asc');
            //$query->orderBy('start_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'request_users');

        $display->setFilters([
            AdminDisplayFilter::related('locksmith_id')->setModel(\App\Model\Locksmith::class)
        ]);

        $display->setColumns([
            AdminColumn::custom()->setLabel('#')->setCallback(function ($instance) {
                        $get_order = \App\Model\LocksmithRequests::orderBy('order_at', 'asc')->get();
                        $c = 0;
                        if (count($get_order) > 0) {
                            foreach ($get_order as $value) {
                                $c++;
                                if ($value->id == $instance->id) {
                                    $efectuados = $c;
                                }
                            }
                            return $efectuados;
                        }
                    })->setWidth('50px')->setHtmlAttribute('class', 'bg-warning text-center'),
                    AdminColumn::datetime('start_at')
                    ->setLabel('Data de Início Autorizada')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::text('pp')
                    ->setLabel('Nº do Pedido')
                    ->setWidth('20%')
                    ->setHtmlAttribute('class', 'bg-gray text-center'),
            AdminColumn::text('products.name')->setLabel('Peça'),
                    AdminColumn::text('value')->setLabel('Total PP')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center'),
            AdminColumn::custom()->setLabel('Produzidos')->setCallback(function ($instance) {
                        $efectuados = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $instance->id)->where('published', 1)->sum('value');
                        if ($efectuados == null) {
                            $efectuados = 0;
                        }
                        return $efectuados;
                    })->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center'),
                    AdminColumn::custom()->setLabel('Por Produzir')
                    ->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\LocksmithRequests::where('id', $instance->id)->first();
                                $efectuados = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $instance->id)->where('published', 1)->sum('value');
                                $missing = $total->value - $efectuados;

                                if ($missing < 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-danger";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            })
                    ->setWidth('10%')
        ]);

        return $display;
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Config extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Config';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
//    public function onDisplay() {
//        // Display
//        $display = $this->fireEdit(1);
//        return $display;
//    }

    public function onEdit($id) {
        $form = AdminForm::panel();

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('limits_cnc')->setLabel('CNC')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_prep')->setLabel('Preparação')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_wood')->setLabel('Carpintaria')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_woodfinishing')->setLabel('Acabamento')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_woodfinishing_black')->setLabel('Acab. de Preto')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_paint')->setLabel('Pintura')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_mount')->setLabel('Montagem')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_wires')->setLabel('Redes')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('limits_locksmith')->setLabel('Serralharia')
                            ], 2)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos Semanais das Secções'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);

        return $form;
    }

}

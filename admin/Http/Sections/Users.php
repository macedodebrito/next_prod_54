<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Role;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

global $PROD;
/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Funcionários';

    public function getCreateTitle() {
        return 'Criar acesso para Novo Funcionário';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        return AdminDisplay::datatables()
                        ->setNewEntryButtonText('Adicionar Novo Funcionário')
                        ->with('roles')
                        ->setHtmlAttribute('class', 'table-primary')
                        ->setColumns([
                            AdminColumn::link('name')->setLabel('Utilizador')->setWidth('150px'),
                            AdminColumn::text('bigname')->setLabel('Nome Completo')->setWidth('200px'),
                            AdminColumn::text('password')->setLabel('Código')->setWidth('80px'),
                            AdminColumn::lists('roles.label')->setLabel('Permissões'),
                            AdminColumn::text('companies.name')->setLabel('Empresa')->setWidth('150px'),
                            AdminColumn::custom()
                            ->setLabel('Trabalhador')
                            ->setWidth('80px')
                            ->setCallback(function ($instance) {
                                        $dump = \App\User::where('id', $instance->id)->first();
                                        if (count($dump) > 0) {
                                            if ($dump->status == 1) {
                                                return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                                            } else if ($dump->status == 0) {
                                                return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                            }
                                        } else {
                                            return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                        }
                                    }),
                            AdminColumn::custom()
                            ->setLabel('Produtividade')
                            ->setWidth('80px')
                            ->setCallback(function ($instance) {
                                        $dump = \App\User::where('id', $instance->id)->first();
                                        global $PROD;
                                        $PROD = $dump;                                        
                                        if (count($dump) > 0) {
                                            if ($dump->performance == 1) {
                                                return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                                            } else if ($dump->performance == 0) {
                                                return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                            }
                                        } else {
                                            return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                        }
                                    }),
                            AdminColumn::custom()
                            ->setWidth('100px')
                            ->setLabel('Semana Anterior')
                            ->setCallback(function ($instance) {
                                global $PROD;
                                if (count($PROD) > 0) {
                                    if ($PROD->performance == 1) return $instance->nr_semanal_anterior;
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>";
                                }
                                else {
                                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                }
                            }),
                            AdminColumn::custom()
                            ->setWidth('100px')
                            ->setLabel('Semana Actual (' . date('W') . ')')
                            ->setCallback(function ($instance) {
                                global $PROD;
                                if (count($PROD) > 0) {
                                    if ($PROD->performance == 1) return $instance->nr_semanal;
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>";
                                }
                                else {
                                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                }
                            }),
                            AdminColumn::custom()
                            ->setWidth('100px')
                            ->setLabel('Mes Anterior')
                            ->setCallback(function ($instance) {
                                global $PROD;
                                if (count($PROD) > 0) {
                                    if ($PROD->performance == 1) return $instance->nr_mensal_anterior;
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>";
                                }
                                else {
                                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                }
                            }),
                            AdminColumn::custom()
                            ->setWidth('100px')
                            ->setLabel('Mes Actual (' . date('m') . ')')
                            ->setCallback(function ($instance) {
                                global $PROD;
                                if (count($PROD) > 0) {
                                    if ($PROD->performance == 1) return $instance->nr_mensal;
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>";
                                }
                                else {
                                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                }
                            }),
                            AdminColumn::custom()
                            ->setWidth('100px')
                            ->setLabel('Dia de Reset')
                            ->setCallback(function ($instance) {
                                    if ($instance->date_points > 0 ) return "<b class=\"badge bg-green\"> $instance->date_points </b>";
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>"; 
                            }),
                            AdminColumn::custom()
                            ->setWidth('80px')
                            ->setLabel('PDF')
                            ->setCallback(function ($instance) {
                                global $PROD;
                                if (count($PROD) > 0) {
                                    if ($PROD->performance == 1) return "<a class=\"download_pdf\" id=\"$instance->id\" href=\"./pdf/$instance->id/user_pdf\"><i class=\"fa fa-file-pdf-o fa-2x\"></i></a>";
                                    else return "<i style=\"color: #999;\" class=\"fa fa-minus fa-2x\"></i>";
                                }
                                else {
                                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                                }
                            }),
                        ])->paginate(20);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id) {
        return AdminForm::panel()->addBody([
                    AdminFormElement::text('name', 'Utilizador')->required()->unique(),
                    AdminFormElement::text('bigname', 'Nome Completo')->required(),
                    AdminFormElement::text('password', 'Password')->required()->unique(),
                    AdminFormElement::multiselect('roles', 'Permissões')->setModelForOptions(new Role())->setDisplay('name'),
                    AdminFormElement::select('company_id', 'Empresa')->setModelForOptions(new \App\Model\Companies())->setDisplay('name'),
                    AdminFormElement::select('status', 'É trabalhador?')->setOptions(['1' => 'Sim', '0' => 'Não'])->required(),
                    AdminFormElement::select('performance', 'Listar Produtividade?')->setOptions(['1' => 'Sim', '0' => 'Não'])->required(),
                    AdminFormElement::text('date_points', 'Dia de Reset')->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate() {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id) {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id) {
        // todo: remove if unused
    }

}

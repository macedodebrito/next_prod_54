<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MOUNT_history extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // INTEGRAÇÃO NXT2PHC
            unset($model->n2p_color_wood);
            unset($model->n2p_color_steel);
            unset($model->n2p_custom);
        });

        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $id = \App\Model\MountUser::orderBy('id', 'desc')->first()->id;
            \App\Model\MountUser::where('id', $id)->delete();
            foreach ($_REQUEST['serials'] as $key => $value) {
                //$ldate = date('Y-m-d H:i:s');
                \App\Model\MountUser::insert(array('id' => null, 'request_id' => $_REQUEST['request_id'], 'user_id' => $_REQUEST['user_id'], 'valid_id' => $_REQUEST['user_id'], 'value' => 1, 'serial' => $value, 'obs' => $_REQUEST['observations'][$key], 'quantity' => $_REQUEST['quantity'], 'last_serial' => $_REQUEST['last_serial'], 'published' => 1, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));

//                print_r("<pre>");
//                print_r($_REQUEST);
//                print_r($_REQUEST['request_id']);

                $object_request = \App\Model\Request::where('id', $_REQUEST['request_id'])->first();

                $object_freeze = \App\Model\FreezeWeek::where('product_id', $object_request->product_id)->orderBy('week', 'asc')->orderBy('end_at', 'asc')->first();

                if (count($object_freeze) == 1) {
                    \App\Model\FreezeWeek::where('id', $object_freeze->id)->delete();
                }

                $object_triple = \App\Model\TripleWeek::where('product_id', $object_request->product_id)->orderBy('end_at', 'asc')->first();
                if (count($object_triple) > 0) {
                    $nova_montagem = $object_triple->montagem - 1;
                    DB::table('triple_week')->where('id', $object_triple->id)->update(['montagem' => $nova_montagem]);
                }
                // INTEGRAÇÃO NXT2PHC
                
                $N2P_ref = \App\Model\Product::where('id', $object_request->product_id)->first()->mini_code . "" . \App\Model\Product::where('id', $object_request->product_id)->first()->code;
                $N2P_designation = \App\Model\StatusRequests::where('mini_code', \App\Model\Product::where('id', $object_request->product_id)->first()->mini_code)->where('code', \App\Model\Product::where('id', $object_request->product_id)->first()->code)->first()->designation;
                $N2P_quantity = 1;
                $N2P_serial = \App\Model\Product::where('id', $object_request->product_id)->first()->code . "" . \App\Model\Request::where('id', $_REQUEST['request_id'])->first()->pp . "" . $value;
                $N2P_wood = $_REQUEST['n2p_color_wood'];
                $N2P_steel = $_REQUEST['n2p_color_steel'];
                $N2P_custom = $_REQUEST['n2p_custom'];
                $N2P_supplier = "ANTONIO FERNANDO C. MARTINS CORREIA";
                $N2P_armazem = 1;
                DB::table('nxt_to_phc')->insert(array('id' => null, 'ref' => $N2P_ref, 'designation' => $N2P_designation, 'quantity' => $N2P_quantity, 'serial' => $N2P_serial, 'color_wood' => $N2P_wood, 'color_steel' => $N2P_steel, 'custom' => $N2P_custom, 'supplier' => $N2P_supplier, 'armazem' => $N2P_armazem));
                
            }
            //connectPHC($_REQUEST, 'mount2PHC', 4, 'manual');
            mount2PHC($_REQUEST, 'manual');
            \Artisan::call('check_updates');
        });
    }

    /**
     * @var string
     */
    protected $title = 'MOUNT - Histórico';

    public function getCreateTitle() {
        return 'A enviar para o Armazém';
    }

    /**
     * @var string
     */
    protected $alias = "mount/history";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        //$display = AdminDisplay::datatablesAsync()
        $display = AdminDisplay::datatablesAsync()
                ->paginate(25)
//                ->setOrder([[0, 'desc']])
                ->setNewEntryButtonText('Enviar para o Armazém')
                ->withPackage('jquery')
                ->addScript('mount_create.js', asset('assets/js/mount_create.js'), ['admin-default']);

        $display->setApply(function($query) {
            $query->orderBy('published', 'asc')->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
            $header_DATA = AdminColumn::datetime('created_at')
            ->setLabel('Efectuado em')
            ->setWidth('100px')
            ->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i'),
            $header_PRODUCAO = AdminColumn::custom()
            ->setLabel('Nº Pedido de Produção')
            ->setWidth('100px')->setHtmlAttribute('class', 'bg-gray text-center')
            ->setCallback(function ($instance) {
                $new_color = "";
                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                $dump = \App\Model\MountUser::where('id', $instance->id)->first();
                if ($dump->published < 0) {
                    $new_color = "bg-red";
                }
                return "<div class=\"$new_color text-center\">$pp->pp</div>";
            }),
            $header_PRODUTO = AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {
                $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                $new_color = "";
                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                $dump = \App\Model\MountUser::where('id', $instance->id)->first();
                if ($dump->published < 0) {
                    $new_color = "bg-red";
                }
                $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $pp->custom_id)->first();
                $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                return "<div class=\"$new_color \">$product_name->name</div>$COLOR";
            }),
            $header_SERIAL = AdminColumn::custom()
            ->setLabel('Numero de Série')
            ->setWidth('30%')->setHtmlAttribute('class', 'bg-warning text-center')
            ->setCallback(function ($instance) {
                $new_color = "";
                $get_request = \App\Model\Request::where('id', $instance->request_id)->first();
                $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();

                $entrance = \App\Model\MountUser::where('id', $instance->id)->first();
                if ($entrance->published < 0) {
                    $new_color = "bg-red";
                }
                global $SERIAL;
                $SERIAL = $get_product->code . "" . $get_request->pp . "" . $instance->serial;
                return "<div class=\"$new_color text-center\">$get_product->code$get_request->pp$instance->serial</div>";
            }),
                    AdminColumn::custom()
                    ->setLabel('Operador Montagem')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
            $header_PONTOS = AdminColumn::custom()->setLabel('Pontos')->setCallback(function ($instance) {
                $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                $new_color = "";
                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                $dump = \App\Model\MountUser::where('id', $instance->id)->first();
                if ($dump->published < 0) {
                    $new_color = "bg-red";
                }
                return "<div class=\"$new_color text-center\">$product_name->points</div>";
            }),            
            $header_OBS = AdminColumn::custom()
            ->setLabel('Observações')
            ->setCallback(function ($instance) {
                $new_color = "";
                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                $dump = \App\Model\MountUser::where('id', $instance->id)->first();
                if ($dump->published < 0) {
                    $new_color = "bg-red";
                }

                $CUSTOM = "";

                global $SERIAL;
                $SQL = DB::table('nxt_to_phc')->where('serial', $SERIAL)->first();
                if (count($SQL) > 0) {
                    $cor_madeira = $SQL->color_wood;
                    $cor_metal = $SQL->color_steel;
                    $custom = $SQL->custom;

                    if ($cor_madeira) {
                        $CUSTOM = "(<b>Madeira:</b> $cor_madeira) ";
                    }
                    if ($cor_metal) {
                        $CUSTOM .= "(<b>Metal:</b> $cor_metal) ";
                    }
                    if ($custom) {
                        $CUSTOM .= "(<b>Customização:</b> $custom) ";
                    }
                }
                else {
                    $CUSTOM = "";
                }
                return "<div class=\"$new_color text-center\">$instance->obs<br>$CUSTOM</div>";
            }),
            $header_STATUS = AdminColumn::custom()
            ->setLabel('Status')
            ->setWidth('5%')
            ->setCallback(function ($instance) {
                // warning 
                // check = 1
                // remove
                // user-times = -1
                $default_html_tag = "#pendente";
                $default_icon = "fa-circle-o-notch fa-spin fa-2x";

                $role_check = 0;
                $role_id = \App\Role::where('name', 'montagem')->first();
                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                    $role_check = 1;
                }

                switch ($instance->published) {
                    case 1:
                        $default_icon = "fa-check fa-2x";
                        return "<i class=\"fa $default_icon\"></i>";
                        break;
                    case 2:
                        $default_icon = "fa-check fa-2x";
                        return "<i class=\"fa $default_icon\"></i>";
                        break;
                    case -1:
                        $default_icon = "fa-user-times fa-2x";
                        $default_html_tag = "#removing";
                        if ($role_check > 0) {
                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"$default_html_tag\"><i class=\"fa $default_icon\"></i></a>";
                        } else {
                            return "<i class=\"fa fa-user-times fa-2x\"></i>";
                        }
                        break;
                    default:
                        if ($role_check > 0) {
                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"#removing_fast\"><i class=\"fa fa-user-times fa-2x\"></i></a> <i class=\"fa $default_icon\"></i>";
                        } else {
                            return "<i class=\"fa $default_icon\"></i>";
                        }
                        break;
                }
            })
        ]);

        // ALTERAR AS CORES PELOS VALORES
//                $instance = MOUNT_history::find($id);
//                if ( $instance->country_id == 'search_value') {
//                $fullNameColumn-> getHeader()->setHtmlAttribute('class', 'Ahtung');
//        }
//        $display->setColumnFilters([
//
//            AdminColumnFilter::date()->setPlaceholder('por Data')->setFormat('d.m.Y'),
//            AdminColumnFilter::text()->setPlaceholder('por PP'),
//            AdminColumnFilter::text()->setPlaceholder('por Nº de Série'),
//            AdminColumnFilter::text()->setPlaceholder('por Produto'),
//            null,
//            AdminColumnFilter::text()->setPlaceholder('por Obs'),
//            null
//        ]);
//        $display->getColumnFilters()->setPlacement('panel.heading');
        $display->setDatatableAttributes(['searching' => false]);

        return $display;
    }

    public function onEdit($id) {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('mount_edit.js', asset('assets/js/mount_edit.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('serial')
                        ->setLabel('Serial Number')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('obs')->setLabel('Observação')
                    ]),
            AdminFormElement::hidden('request_id')
        ]);

        $form->getButtons()
                ->setSaveButtonText('Editar Saída dos produtos para o Armazem')
                ->hideSaveAndCloseButton()
                ->setCancelButtonText('Cancelar')
                ->hideSaveAndCreateButton();

        return $form;
    }

    public function onCreate() {        
        $get_pp = \App\Model\PaintUser::orderBy('created_at', 'asc')->groupBy('id')->get();
        $new_request_pp = [];
        foreach ($get_pp as $key => $value) {
            $get_request = \App\Model\Request::where('id', $value->request_id)->first();
            $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();
            $get_color = \App\Model\ProductCustomNxt::where('id', $get_request->custom_id)->first();

            $total = \App\Model\PaintUser::where('request_id', $value->request_id)->where('published', 1)->sum('value');
            $permitido = \App\Model\MountUser::where('request_id', $value->request_id)->sum('value');
            $check = $total - $permitido;
            $new_request_pp[0] = "Escolha um Pedido para Produção";
            if ($check > 0) {
                $new_request_pp[$value->request_id] = "#$get_request->pp - $get_product->name ($get_color->name) | SERIAL: $get_product->code $get_request->pp XXX";
            }
        };

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('mount_create.js', asset('assets/js/mount_create.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('request_id', 'Pedido de Produção')
                        ->setOptions($new_request_pp)
                        ->setDefaultValue(0)
                        ->required()
                            ], 4)
                    ->addColumn([
                        AdminFormElement::select('quantity', 'Quantidade')
                        ->required()
                            ], 2)
        ]);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('serial')
                        ->setLabel('Serial Number')
                        ->setDefaultValue("XXX")
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('obs')->setLabel('Observação')
                    ])
        ]);
        $SELECT_WOOD = [];
        $SELECT_WOOD[''] = '-';
        $SQL = \App\Model\Customizations::where("color_wood", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_WOOD["$item->value"] = $item->name;
        }

        $SELECT_STEEL = [];
        $SELECT_STEEL[''] = '-';
        $SQL = \App\Model\Customizations::where("color_steel", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_STEEL["$item->value"] = $item->name;
        }

        $SELECT_CUSTOM = [];
        $SELECT_CUSTOM[''] = '-';
        $SQL = \App\Model\Customizations::where("custom", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_CUSTOM["$item->value"] = $item->name;
        }
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('n2p_color_wood', 'Cor Madeira')->setOptions($SELECT_WOOD)->setDefaultValue("")
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('n2p_color_steel', 'Cor Metal')->setOptions($SELECT_STEEL)->setDefaultValue("")
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('n2p_custom', 'Customização')->setOptions($SELECT_CUSTOM)->setDefaultValue("")
                            ], 8)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('value')->setDefaultValue(1)
                    ->required(),
                    AdminFormElement::hidden('last_serial')->setDefaultValue(0)
                    ->required()
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para o Armazém'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class RequestWeekGroup extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Grupo (PP)';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
        //->setOrder([[2, 'asc']])
        //->withPackage('jquery')
        //->setNewEntryButtonText('Nova Encomenda de Cliente')
        //->addScript('requestweek.js', asset('assets/js/requestweek.js'), ['admin-default'])
        ;

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->GroupBy('group')->orderBy('created_at', 'asc')->where('group', '!=', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_wood_at')
                    ->setLabel('Data de Término (Carpintaria)')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_woodfinishing_at')
                    ->setLabel('Data de Término (Acabamento)')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_paint_at')
                    ->setLabel('Data de Término (Pintura)')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término (Montagem)')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('# de Encomendas')
                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                $group_sum = \App\Model\RequestWeekGroup::where('group', $instance->group)->get();
                                return count($group_sum);
                            }),
                    AdminColumn::custom()
                    ->setLabel('Grupo')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                if ($instance->group != 0) {
                                    !($instance->group % 2) ? $color = "red" : $color = "aqua";
                                    return "<div class=\"bg-$color text-center\">$instance->group</div>";
                                } else {
                                    return "-";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::form()
                ->setItems([
            AdminFormElement::date('end_at', 'Data de Término (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_paint_at', 'Data de Término (Pintura)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_woodfinishing_at', 'Data de Término (Acabamento)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_wood_at', 'Data de Término (Carpintaria)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::number('group')->required(),
            AdminFormElement::hidden('id')->required(),
        ]);
        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Datas'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]); 

        return $form;
    }

//    public function onCreate() {
//        return $this->onEdit(null);
//    }

    public function updating() {
        //dd($_REQUEST);
        $end_at = new Carbon($_REQUEST['end_at']);
        $end_paint_at = new Carbon($_REQUEST['end_paint_at']);
        $end_woodfinishing_at = new Carbon($_REQUEST['end_woodfinishing_at']);
        $end_wood_at = new Carbon($_REQUEST['end_wood_at']);

        $SQL = RequestWeekGroup::where('id', $_REQUEST['id'])->first();
        //dd($SQL);
        RequestWeekGroup::where('group', $SQL->group)->update(['end_at' => $end_at, 'end_paint_at' => $end_paint_at, 'end_woodfinishing_at' => $end_woodfinishing_at, 'end_wood_at' => $end_wood_at, 'group' => $_REQUEST['group']]);
    }

}

//            ->disableDeleting()

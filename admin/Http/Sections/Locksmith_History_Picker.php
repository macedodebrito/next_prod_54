<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Locksmith_History_Picker extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->picker);
            unset($model->picker2);
            unset($model->total);
            unset($model->published);

            // INTEGRAÇÃO NXT2PHC
            unset($model->n2p_color_steel);
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\LocksmithRequestsUser::orderBy('id', 'desc')->first()->id;
            \App\Model\LocksmithRequestsUser::where('id', $id)->delete();

            foreach ($_REQUEST['obs'] as $key => $value) {
                $result = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', \App\Model\LocksmithRequests::where('pp', $_REQUEST['locksmith_requests_id'][$key])->first()->id)->count();
                if ($result > 0) {
                    $last_serial = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', \App\Model\LocksmithRequests::where('pp', $_REQUEST['locksmith_requests_id'][$key])->first()->id)->orderby('serial', 'desc')->first()->serial;
                } else {
                    $last_serial = 0;
                }
                \App\Model\LocksmithRequestsUser::insert(
                        array('id' => null,
                            'locksmith_requests_id' => \App\Model\LocksmithRequests::where('pp', $_REQUEST['locksmith_requests_id'][$key])->first()->id,
                            'user_id' => $_REQUEST['user_id'],
                            'valid_id' => 0,
                            'value' => 1,
                            'serial' => $_REQUEST['serial'][$key],
                            'last_serial' => $last_serial,
                            'obs' => $_REQUEST['obs'][$key],
                            'published' => 1,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        )
                );

                $object_request = \App\Model\LocksmithRequests::where('pp', $_REQUEST['locksmith_requests_id'][$key])->first();

                // INTEGRAÇÃO NXT2PHC
                $N2P_ref = \App\Model\Locksmith::where('id', $object_request->locksmith_id)->first()->mini_code . "" . \App\Model\Locksmith::where('id', $object_request->locksmith_id)->first()->code;
                $N2P_designation = \App\Model\LocksmithStatusRequests::where('mini_code', \App\Model\Locksmith::where('id', $object_request->locksmith_id)->first()->mini_code)->where('code', \App\Model\Locksmith::where('id', $object_request->locksmith_id)->first()->code)->first()->designation;
                $N2P_quantity = 1;
                $N2P_serial = \App\Model\Locksmith::where('id', $object_request->locksmith_id)->first()->code . "" . $_REQUEST['locksmith_requests_id'][$key] . "" . $_REQUEST['serial'][$key];
                $N2P_wood = "";
                $N2P_steel = $_REQUEST['n2p_color_steel'];
                $N2P_custom = "";
                $N2P_supplier = "ANTONIO FERNANDO C. MARTINS CORREIA";
                $N2P_armazem = 1;
                DB::table('nxt_to_phc')->insert(array('id' => null, 'ref' => $N2P_ref, 'designation' => $N2P_designation, 'quantity' => $N2P_quantity, 'serial' => $N2P_serial, 'color_wood' => $N2P_wood, 'color_steel' => $N2P_steel, 'custom' => $N2P_custom, 'supplier' => $N2P_supplier, 'armazem' => $N2P_armazem));
            }
        });
    }

    /**
     * @var string
     */
    protected $title = 'Serraharia - Histórico Picagem';

    public function getCreateTitle() {
        return 'A enviar para a Montagem';
    }

    /**
     * @var string
     */
    protected $alias = "serralharia/history_picker";

    /**
     * @return DisplayInterface
     */
    public function onCreate() {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('locksmith_create_picker.js', asset('assets/js/locksmith_create_picker.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('picker')
                        ->setLabel('Código de Barras (Peça)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('picker2')
                        ->setLabel('Código de Barras (Caixa)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('total')->setDefaultValue(0)
                        ->setLabel('Total de Produtos')
                            ], 2)
        ]);
        $SELECT = [];
        $SELECT[''] = '-';
        $SQL = \App\Model\Customizations::where("serralharia", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT["$item->value"] = $item->name;
        }
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('n2p_color_steel', 'Cor Metal')->setOptions($SELECT)->setDefaultValue("")
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('value')->setDefaultValue(1)
                    ->required(),
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para o Armazém'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

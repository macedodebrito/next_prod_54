<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Comsumables extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Consumíveis';

    public function getCreateTitle() {
        return 'Criar Novo Consumível';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Novo Consumível')
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('nome', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Referência')
                    ->setCallback(function ($instance) {
                                switch ($instance->ref) {
                                    case null: return "<div class=\"bg-yellow text-center\">- SEM REF.</div>";
                                        break;
                                    default: return $instance->ref;
                                        break;
                                }
                            }),
            AdminColumn::text('nome')->setLabel('Nome')->setWidth('150px'),
                    AdminColumn::custom()
                    ->setLabel('CNC')
                    ->setCallback(function ($instance) {
                                switch ($instance->cnc) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Carpintaria')
                    ->setCallback(function ($instance) {
                                switch ($instance->carpintaria) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Acabamento')
                    ->setCallback(function ($instance) {
                                switch ($instance->acabamento) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pintura')
                    ->setCallback(function ($instance) {
                                switch ($instance->pintura) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Montagem')
                    ->setCallback(function ($instance) {
                                switch ($instance->montagem) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serralharia')
                    ->setCallback(function ($instance) {
                                switch ($instance->serralharia) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Técnica')
                    ->setCallback(function ($instance) {
                                switch ($instance->tecnica) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Redes')
                    ->setCallback(function ($instance) {
                                switch ($instance->redes) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serigrafia')
                    ->setCallback(function ($instance) {
                                switch ($instance->serigrafia) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Filtros')
                    ->setCallback(function ($instance) {
                                switch ($instance->filtros) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Armazém<br/>Prod. Acabado')
                    ->setCallback(function ($instance) {
                                switch ($instance->armazem) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Armazém<br/>Consumiveis')
                    ->setCallback(function ($instance) {
                                switch ($instance->comsumables) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),                                    
            AdminColumn::text('unit')->setLabel('Tipo de Unidade')->setWidth('120px'),
            AdminColumn::text('between_start')->setLabel('Min.'),
            AdminColumn::text('between_end')->setLabel('Max.'),
                    AdminColumn::custom()
                    ->setLabel('Entregar Antigo')
                    ->setCallback(function ($instance) {
                                switch ($instance->deliver) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('ref')->setLabel('Referência'),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('nome')->setLabel('Nome')->required(),
                            ], 4),
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::checkbox('cnc')->setLabel('CNC'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('carpintaria')->setLabel('Carpintaria'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('acabamento')->setLabel('Acabamento'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('pintura')->setLabel('Pintura'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('montagem')->setLabel('Montagem'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('serralharia')->setLabel('Serralharia'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('tecnica')->setLabel('Técnica'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('redes')->setLabel('Redes'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('serigrafia')->setLabel('Serigrafia'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('filtros')->setLabel('Filtros'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('armazem')->setLabel('Armazém de Produto Acabado'),
                            ], 12)  
                    ->addColumn([
                        AdminFormElement::checkbox('comsumables')->setLabel('Armazém de Consumíveis'),
                            ], 12),             
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('unit')->setLabel('Tipo de Unidade'),
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('between_start')->setLabel('Quantidade Mínima'),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('between_end')->setLabel('Quantidade Máxima'),
                            ], 2),
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::checkbox('deliver')->setLabel('Entregar Antigo?'),
                            ], 12)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar Consumível'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

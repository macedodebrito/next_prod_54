<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class StatusRequests extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Reservas - Produtos';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatables()
                ->setOrder([[6, 'asc']])
                ->paginate(100);

                $display->setApply(function($query) {
                    $query                            
                            ->select('status_requests.*', 'products_custom_nxt.name as color_name', 'products_custom_nxt.hex_color', 'products_custom_nxt.hex_color_bg')
                            ->join('products_custom_nxt', 'status_requests.custom_color', '=', 'products_custom_nxt.id')
                    ;
                });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setColumns([
                    AdminColumn::datetime('last_update', 'Última<br/>Actualização')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setWidth('200px')
                    ->setFormat('d.m.Y H:i:s'),

/*                     AdminColumn::text('name')->setLabel('Nome<br/>Produto')
                    ->setHtmlAttribute('class', 'text-center bg-black'),
 */
                     AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setCallback(function ($instance) {
                        return "<div>$instance->name <br/>($instance->custom_code)</div><span class=\"badge\" style=\"color:$instance->hex_color;background-color:$instance->hex_color_bg\">$instance->color_name</span>";
                    }),

            AdminColumn::text('phc_available')->setLabel('Stock<br/>PHC')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-warning'),
            AdminColumn::text('phc_reserved')->setLabel('Encomendado<br/>PHC')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-success'),
                    AdminColumn::custom()
                    ->setLabel('Disponível<br/>de Imediato')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-info text-center')->setWidth('150px')
                    ->setCallback(function ($instance) {
//                                $dump = \App\Model\RequestWeek::where('product_id', $instance->product_id)->where('client', "=", "EXTRA_STOCK")->sum("quantity");
//                                return $instance->total - $dump;
                                return $instance->total;
                            }),
                    AdminColumn::text('value')->setLabel('Encomendas<br/>NXT')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center bg-warning'),
//            AdminColumn::text('extra')->setLabel('Reforço<br/>de Stock')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-danger'),
            AdminColumn::custom()->setLabel('Previsão de<br/>Disponibilidade')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center bg-danger')
                    ->setCallback(function ($instance) {
                                return $instance->phc_available - $instance->phc_reserved + $instance->value;
                            }),
        ]);

        return $display;
    }

//    public function onEdit($id) {
//        $GG = [];
//        $score = 0;
//        while ($score <= 100) {
//            $GG[] = $score;
//            $score++;
//        }
//        return AdminForm::form()->setItems([
//                    AdminFormElement::text('name', 'Nome do Produto')->required(),
//                    AdminFormElement::text('code', 'Código')->required(),
//                    AdminFormElement::select('serie_id', 'Série')->setModelForOptions(new Serie)->setDisplay('name')->required(),
//                    AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required(),
//        ]);
//    }
//    public function onCreate() {
//        return $this->onEdit(null);
//    }
}

<?php

use App\Model\CNC_total;
use App\Model\Request;
use App\Model\RequestWeek;
use App\Model\RequestUser;
use App\Model\WoodUser;
use App\Model\PaintUser;
use App\Model\MountUser;
use App\Model\StockUser;
use App\Model\Product;
use SleepingOwl\Admin\Model\ModelConfiguration;

global $TOTAL_PP_ABERTO;
global $TOTAL_PRODUCAO;

AdminSection::registerModel(CNC_total::class, function (ModelConfiguration $model) {
    $model->setTitle('Painel de Bordo')->setAlias('requests/cnc_total')->disableDeleting()->disableRestoring();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables();

        $display->setApply(function($query) {
            $query->orderBy('start_at', 'asc')->groupBy('product_id');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'request_users');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(Product::class)
        ]);

        $display->setColumns([
            AdminColumn::text('products.name')->setLabel('Produto'),
                    AdminColumn::custom()
                    ->setLabel('Total dos PP')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                $total = $instance->pp_total['sum'];
                                if (!count($total)) {
                                    $total = 0;
                                }
                                if ($total < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($total == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-warning";
                                }
                                return "<div class=\"$new_color text-center\">$total</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('PP em aberto (CNC)')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $abertos = $instance->pp_total['sum'] - $instance->disp_cnc;
                                if ($abertos < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($abertos == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-danger";
                                }
                                return "<div class=\"$new_color text-center\">$abertos</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível CARPINTARIA')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $CARPINTARIA = $instance->disp_cnc - $instance->disp_carpintaria;
                                if ($CARPINTARIA < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($CARPINTARIA == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$CARPINTARIA</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível ACABAMENTO')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $ACABAMENTO = $instance->disp_carpintaria - $instance->disp_acabamento;
                                if ($ACABAMENTO < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($ACABAMENTO == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$ACABAMENTO</div>";
                            }),                                    
                    AdminColumn::custom()
                    ->setLabel('Disponível PINTURA')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $PINTURA = $instance->disp_acabamento - $instance->disp_pintura;
                                if ($PINTURA < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($PINTURA == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$PINTURA</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponível MONTAGEM')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $MONTAGEM = $instance->disp_pintura - $instance->disp_montagem;
                                if ($MONTAGEM < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($MONTAGEM == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$MONTAGEM</div>";
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('Entrada em Armazem')
//                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
//                    ->setCallback(function ($instance) {
//                                $ARMAZEM = $instance->disp_armazem;
//                                if ($ARMAZEM < 0) {
//                                    $new_color = "bg-red";
//                                } else
//                                if ($ARMAZEM == 0) {
//                                    $new_color = "bg-aqua";
//                                } else {
//                                    $new_color = "bg-warning";
//                                }
//                                return "<div class=\"$new_color text-center\">$ARMAZEM</div>";
//                            }),
            AdminColumn::custom()
                    ->setLabel('Total PP em aberto')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                $pp_total = $instance->pp_total['sum'];
                                $cnc = $pp_total - $instance->disp_cnc;
                                $carpintaria = $instance->disp_cnc - $instance->disp_carpintaria;
                                $pintura = $instance->disp_carpintaria - $instance->disp_pintura;
                                $montagem = $instance->disp_pintura - $instance->disp_montagem;
                                $TOTAL_PP = $cnc + $carpintaria + $pintura + $montagem;
                                global $TOTAL_PP_ABERTO;
                                global $TOTAL_PRODUCAO;
                                $TOTAL_PP_ABERTO = $TOTAL_PP;
                                $TOTAL_PRODUCAO = $carpintaria + $pintura + $montagem;
                                if ($TOTAL_PP < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($TOTAL_PP == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-gray";
                                }
                                return "<div class=\"$new_color text-center\">$TOTAL_PP</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Encomendas de Clientes')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $CONTA = $instance->encomendas;
                                if ($CONTA > 0) {
                                    return "<div class=\"bg-red text-center\">$CONTA</div>";
                                } else {
                                    return "<div class=\"bg-aqua text-center\">$CONTA</div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Sobras de PP')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                global $TOTAL_PP_ABERTO;
                                $SOBRAS = $TOTAL_PP_ABERTO - $instance->encomendas;
                                if ($SOBRAS < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($SOBRAS == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-warning";
                                }
                                return "<div class=\"$new_color text-center\">$SOBRAS</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Total em Produção')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
//                                $total_a = Request::where('product_id', $instance->product_id)->get();
//                                $ARMAZEM = 0;
//                                foreach ($total_a as $value1) {
//                                    $ARMAZEM_a = MountUser::where('request_id', $value1->id)->where('published', 0)->get();
//                                    if (!count($ARMAZEM)) {
//                                        $ARMAZEM = $ARMAZEM + 0;
//                                    } else {
//                                        foreach ($ARMAZEM_a as $value) {
//                                            $ARMAZEM = $ARMAZEM + $value->value;
//                                        }
//                                    }
//                                }                                
                                global $TOTAL_PRODUCAO;
                                $TOTAL_PROD = $TOTAL_PRODUCAO;
                                if ($TOTAL_PROD < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($TOTAL_PROD == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-danger";
                                }
//                                return "<div class=\"$new_color text-center\">$TOTAL_PROD (+ $ARMAZEM)</div>";
                                return "<div class=\"$new_color text-center\">$TOTAL_PROD</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Disponivel em Produção')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $TOTAL_PRODUCAO;
                                $TOTAL_PROD = $TOTAL_PRODUCAO;
                                $DISP_PROD = $TOTAL_PRODUCAO - $instance->encomendas;
                                if ($DISP_PROD < 0) {
                                    $new_color = "bg-red";
                                } else
                                if ($DISP_PROD == 0) {
                                    $new_color = "bg-aqua";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\">$DISP_PROD</div>";
                            }),
        ]);

        return $display;
    });
    //
});

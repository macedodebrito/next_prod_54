<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class TechStatusRequests extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Reservas - Técnica';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatables()
                ->setOrder([[6, 'asc']])
                ->paginate(100);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setColumns([
                    AdminColumn::datetime('last_update', 'Última<br/>Actualização')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setWidth('200px')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::text('name')->setLabel('Nome<br/>Produto')
                    ->setHtmlAttribute('class', 'text-center bg-black'),
            AdminColumn::text('phc_available')->setLabel('Stock<br/>PHC')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-warning'),
            AdminColumn::text('phc_reserved')->setLabel('Encomendado<br/>PHC')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-success'),
                    AdminColumn::custom()
                    ->setLabel('Disponível<br/>de Imediato')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-info text-center')->setWidth('150px')
                    ->setCallback(function ($instance) {
//                                $dump = \App\Model\RequestWeek::where('product_id', $instance->product_id)->where('client', "=", "EXTRA_STOCK")->sum("quantity");
//                                return $instance->total - $dump;
                                return $instance->total;
                            }),
                    AdminColumn::text('value')->setLabel('Encomendas<br/>NXT')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center bg-warning'),
//            AdminColumn::text('extra')->setLabel('Reforço<br/>de Stock')->setWidth('150px')->setHtmlAttribute('class', 'text-center bg-danger'),
            AdminColumn::custom()->setLabel('Previsão de<br/>Disponibilidade')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center bg-danger')
                    ->setCallback(function ($instance) {
                                return $instance->phc_available - $instance->phc_reserved + $instance->value;
                            }),
        ]);

        return $display;
    }
}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\UserLogin;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Clock_User extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = "Relógio de Ponto";

    /**
     * @var string
     */
    protected $alias = "clock_users";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        if (isset($_GET['id'])) {
            $user_name = \App\Model\Clock::where('id', $_GET['id'])->first()->bigname;
        }

        $display = AdminDisplay::datatables()->paginate(10);
        $display->getColumns()->getControlColumn()->setDeletable(false)->setEditable(true);
        $display->setTitle("Informação sobre o Funcionário: <b>$user_name</b> (Listagem por mês)");

        $display->setApply(function($query) {
            $query->select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, COUNT(*) m_count"))
                    ->groupBy('year')
                    ->groupBy('month')
                    ->orderBy('year', 'desc')
                    ->orderBy('month', 'desc')
                    ->where('user_id', $_GET['id'])
                    ->get();
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Ano.Mês')
                    ->setCallback(function ($instance) {
                                if ($instance->month == "1") {
                                    $MES = "Janeiro";
                                } else if ($instance->month == "2") {
                                    $MES = "Fevereiro";
                                } else if ($instance->month == "3") {
                                    $MES = "Março";
                                } else if ($instance->month == "4") {
                                    $MES = "Abril";
                                } else if ($instance->month == "5") {
                                    $MES = "Maio";
                                } else if ($instance->month == "6") {
                                    $MES = "Junho";
                                } else if ($instance->month == "7") {
                                    $MES = "Julho";
                                } else if ($instance->month == "8") {
                                    $MES = "Agosto";
                                } else if ($instance->month == "9") {
                                    $MES = "Setembro";
                                } else if ($instance->month == "10") {
                                    $MES = "Outubro";
                                } else if ($instance->month == "11") {
                                    $MES = "Novembro";
                                } else {
                                    $MES = "Dezembro";
                                }
                                if ($instance->month < 10) {
                                    $FIX_MONTH = "0$instance->month";
                                } else {
                                    $FIX_MONTH = $instance->month;
                                }
                                $MONTH = $instance->month;
                                $YEAR = $instance->year;
                                $ID = $_GET['id'];
                                return "<a href=\"clock_months?id=$ID&y=$YEAR&m=$MONTH\"><div>$instance->year.$FIX_MONTH ($MES)</div></a>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Registos')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {

                                return "<div class=\"text-center\">$instance->m_count</div>";
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        
    }

}

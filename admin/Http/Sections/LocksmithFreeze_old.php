<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;

global $more_css;

class LocksmithFreeze extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;
    protected $model;

    /**
     * @var string
     */
    protected $title = 'Semanas da Serralharia';

    /**
     * @var string
     */
    protected $alias = "serralharia/week_updated";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

        $display = AdminDisplay::datatables()
                ->paginate(50)
                ->setOrder([[0, 'asc']]);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('end_at', 'asc')->groupBy('client')->groupBy('locksmith_id')->groupBy('end_at')->groupBy('quantity');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('locksmith_id')->setModel(\App\Model\Locksmith::class)
        ]);

        $display->setColumns([
            $header_data = AdminColumn::datetime('end_at')
            ->setLabel('Data de Expedição')
            ->setWidth('150px')
            ->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i'),
            $header_product = AdminColumn::text('products.name')->setLabel('Produto')->setWidth('200px'),
//            AdminColumn::text('montagem')->setLabel('Quantidade'),
            $header_quantity = AdminColumn::custom()
            ->setLabel('Quantidade')
            ->setWidth('100px')
            ->setCallback(function ($instance) {
                global $VALOR_atraso;
//                $config = \App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->orderBy('end_at', 'asc')->get()->quantity;
                $config = \App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->where('client', $instance->client)->where('end_at', $instance->end_at)->orderBy('end_at', 'asc')->groupBy('client')->groupBy('locksmith_id')->groupBy('end_at')->groupBy('quantity')->count();
//                $config = $instance->quantity;
                $VALOR_atraso = $config;
                return $VALOR_atraso;
            }),
            $header_client = AdminColumn::text('client')->setLabel('Cliente'),
            $header_obs = AdminColumn::text('obs')->setLabel('Observações'),
            $header_points = AdminColumn::custom()
            ->setLabel('Pontos')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                global $VALOR_atraso;
                $config = \App\Model\Locksmith::where('id', $instance->locksmith_id)->first();
                $POINTS = $VALOR_atraso * $config->points;
                return $POINTS;
            }),
            $header_palete_store = AdminColumn::custom()
            ->setLabel('Palete<br/>Separada')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                $palete = \App\Model\LocksmithPalete::where('locksmith_week_id', $instance->id)->where('store_id', '!=', 0)->first();
                if (count($palete) > 0) {                
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";                
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_auth = AdminColumn::custom()
            ->setLabel('Palete<br/>Autorizada')
            ->setWidth('80px')
            ->setCallback(function ($instance) {
                $palete = \App\Model\LocksmithPalete::where('locksmith_week_id', $instance->id)->where('auth_id', '!=', 0)->first();
                if (count($palete) > 0) {
                    if ($palete->auth_id != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
        ]);
        $header_data->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_product->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_quantity->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_client->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_obs->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_points->getHeader()->setHtmlAttribute('class', 'bg-red');
        $header_palete_store->getHeader()->setHtmlAttribute('class', "bg-red");
        $header_palete_auth->getHeader()->setHtmlAttribute('class', "bg-black");
        return $display;
    }
}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;

class FreezeWeek_All extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    /**
     * @var string
     */
    protected $title = 'Editar Todas as Semanas';

    /**
     * @var string
     */
    protected $alias = "objectivos_all";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(100)
                ->setOrder([[0, 'asc']])
                ->withPackage('jquery')
                ->addScript('triple.js', asset('assets/js/triple.js'), ['admin-default']);

        $display->getColumns()->getControlColumn();

        $display->setApply(function($query) {
            $query->orderBy('week', 'asc')->orderBy('end_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Expedição')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('products.name')->setLabel('Produto'),
            AdminColumn::text('montagem')->setLabel('Quantidade'),
            AdminColumn::text('client')->setLabel('Cliente'),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('products.points')->setLabel('Pontos'),
                    AdminColumn::custom()
                    ->setLabel('Semana')
                    ->setCallback(function ($instance) {
                                if ($instance->week == "-1") {
                                    $semana = "ATRASO";
                                } else if ($instance->week == "0") {
                                    $semana = "1 (ACTUAL)";
                                } else if ($instance->week == "1") {
                                    $semana = "2 (PROXIMA)";
                                } else if ($instance->week == "2") {
                                    $semana = "3 (15 DIAS)";
                                } else {
                                    $semana = "ESCONDIDO";
                                }
                                return $semana;
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $get_week[-1] = "Tabela em Atraso";
        $get_week[0] = "Semana 1";
        $get_week[1] = "Semana 2";
        $get_week[2] = "Semana 3";
        $get_week[3] = "Semana 4";
        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::datetime('end_at')->setLabel('Data de Término na Montagem')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::select('week', 'Semana')
                        ->setOptions($get_week)
                        ->required()
                            ], 3)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Objectivos Semanais'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Palete_History extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Histórico de Paletes do Armazém';

    /**
     * @var string
     */
    protected $alias = "view_paletes_history";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->addScript('paletes.js', asset('assets/js/paletes.js'), ['admin-default'])
                ->addStyle('paletes.css', asset('assets/css/paletes.css'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('auth_date', 'desc')->where('status', 2);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('request_week');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->request_week_id\" data-rel=\"$instance->phc_id\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF; cursor: pointer;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->request_week_id\" data-rel=\"\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF; cursor: pointer;\">NXT $instance->request_week_id</div>";
                                }
                                return $GG;
                            }),
                    AdminColumn::datetime('request_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('90px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('70px')
                    ->setCallback(function ($instance) {
                        $request_week_info_sql = \App\Model\RequestWeek::where('id', $instance->request_week_id)->first();
                        $request_week_info = $request_week_info_sql->product_id;
                        return \App\Model\Product::where('id', $request_week_info)->first()->name."<span class=\"badge\" style=\"background-color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color_bg.";color:".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->hex_color."\">".\App\Model\ProductCustomNxt::find($request_week_info_sql->custom_id)->name."</span>";
                            }),
            AdminColumn::text('request_week.montagem')->setLabel('#')->setWidth('1%'),
            AdminColumn::text('request_week.client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Armazém')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "store";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Técnica')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "tech";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serigrafia')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "serigrafia";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serralharia')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "locksmith";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Redes')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "wires";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first();
                                            if (count($nome_auth) > 0) {
                                                $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                            }
                                            else {
                                                $nome_auth = "Desconhecido";
                                            }
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('PVC')
//                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
//                    ->setCallback(function ($instance) {
//                                $selected_class = "pvc";
//                                $selected_class_auth = $selected_class . "_auth";
//                                $ID = $instance->id;
//                                $col_id = $selected_class . "_id";
//                                $col_date = $selected_class . "_date";
//                                $col_auth = $selected_class . "_auth_id";
//                                $col_auth_date = $selected_class . "_auth_date";
//
//                                $nome_auth = "--";
//                                $AUTH_DATA = "-/-/-";
//                                $nome = "Desconhecido";
//                                $DATA = $instance->$col_date;
//                                if ($instance->$col_id != 0) {
//                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
//                                    if (count($valid_id) > 0) {
//                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
//                                        if ($instance->$col_auth == 0) {
//                                            $DATA = $instance->$col_date;
//                                        } else {
//                                            $DATA = $instance->$col_date;
//                                            $AUTH_DATA = $instance->$col_auth_date;
//                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
//                                        }
//                                    }                                    
//                                }
//                                if ($instance->$col_id > 0) {
//                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
//                                } else {
//                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
//                                }}),
            AdminColumn::custom()
                    ->setLabel('Embalagem')
                    ->setWidth('60px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $selected_class = "packing";
                                $selected_class_auth = $selected_class . "_auth";
                                $ID = $instance->id;
                                $col_id = $selected_class . "_id";
                                $col_date = $selected_class . "_date";
                                $col_auth = $selected_class . "_auth_id";
                                $col_auth_date = $selected_class . "_auth_date";

                                $nome_auth = "--";
                                $AUTH_DATA = "-/-/-";
                                $nome = "Desconhecido";
                                $DATA = $instance->$col_date;
                                if ($instance->$col_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->$col_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->$col_id)->first()->bigname;
                                        if ($instance->$col_auth == 0) {
                                            $DATA = $instance->$col_date;
                                        } else {
                                            $DATA = $instance->$col_date;
                                            $AUTH_DATA = $instance->$col_auth_date;
                                            $nome_auth = \App\User::where('id', $instance->$col_auth)->first()->bigname;
                                        }
                                    }
                                }
                                if ($instance->$col_id > 0) {
                                    return "<i class=\"fa fa-check fa-2x\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Separado por: $nome ($DATA) e Validado por: $nome_auth ($AUTH_DATA)\"></i>";
                                } else {
                                    return "<div><i class=\"fa fa-minus fa-2x\"></i></a></span></div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Autorizado Por')
                    ->setWidth('60px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->store_id != 0) {
                                    if ($instance->auth_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->auth_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->auth_id)->first()->bigname;
                                            return "$nome<br/>$instance->authed_date";
                                        } else {
                                            return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                        }
                                    } else {
                                        return "<div class=\"bg-red\">Por autorizar</div><br/><span class=\"auth_picks auth_picks_code_auth_$instance->id\"><a id=\"$instance->id\" rel=\"\" class=\"accept_auth\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"picks_code_auth_$instance->id\" type=\"password\" size=\"10\" class=\"form-control picks_code_auth\"><input type=\"hidden\" id=\"auth_id_picks_code_auth_$instance->id\">";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">À espera do Armazém</div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                if (($instance->store_id > 0) && ($instance->auth_id > 0)) {
                                    return "<a class=\"download_pdf\" id=\"$instance->id\" href=\"./pdf/$instance->id/palete_pdf\"><i class=\"fa fa-file-pdf-o fa-2x\"></i></a>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Apagar')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                if (($instance->store_id > 0) && ($instance->auth_id > 0)) {
                                    return "<a class=\"delete_palete_history\" rel=\"$instance->id\" href=\"#$instance->id\"><i class=\"fa fa-times fa-2x\"></i></a>";
                                }
                            })
        ]);

        return $display;
    }

}

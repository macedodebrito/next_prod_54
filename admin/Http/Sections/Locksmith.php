<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Locksmith extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Gestão da Serralharia';

    public function getCreateTitle() {
        return 'Criar Nova Peça';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Nova Peça')
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('name', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')->setWidth('30px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_1) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_1')->setLabel('Imagem 1')->setWidth('120px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_2) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_2')->setLabel('Imagem 2')->setWidth('120px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_3) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_3')->setLabel('Imagem 3')->setWidth('120px'),
            AdminColumn::link('name')->setLabel('Nome da Peça'),
            AdminColumn::link('mini_code')->setLabel('Designação'),
            AdminColumn::link('code')->setLabel('Código'),
            AdminColumn::link('points')->setLabel('Pontuação'),
                    AdminColumn::custom()
                    ->setLabel('CNC')
                    ->setCallback(function ($instance) {
                                switch ($instance->cnc) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Armazem')
                    ->setCallback(function ($instance) {
                                switch ($instance->store) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $score = 0;
        while ($score <= 100) {
            $GG[] = $score;
            $score++;
        }

        $form = AdminForm::panel();

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::image('img_1', 'Imagem 1')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::image('img_2', 'Imagem 2')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::image('img_3', 'Imagem 3')
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('caption_1', 'Legenda 1')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('caption_2', 'Legenda 2')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('caption_3', 'Legenda 3')
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('name', 'Nome do Produto')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('mini_code', 'Designação')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('code', 'Código')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::checkbox('cnc')->setLabel('CNC')
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('store')->setLabel('Armazém')
                            ], 12)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);
        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

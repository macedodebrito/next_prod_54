<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\UserLogin;
use Carbon\Carbon;
use DateTime;
use DateInterval;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class WorkAbsences extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Marcação de Faltas';

    public function getCreateTitle() {
        return 'Nova Falta';
    }
    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

        $display = AdminDisplay::datatables()
        ->paginate(25)
        ->setOrder([[0, 'desc']])
        ->setNewEntryButtonText('Nova Falta')
        ->withPackage('jquery')
        ->addScript('workabsences_create.js', asset('assets/js/workabsences_create.js'), ['admin-default']);

        //$display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::datetime('created_at', 'Criado em')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i:s'),
            AdminColumn::text('users.bigname')->setLabel('Funcionário'),
            AdminColumn::text('title')->setLabel('Motivo'),
            AdminColumn::datetime('start_at', 'Início')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i:s'),
            AdminColumn::datetime('end_at', 'Término')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
            ->setFormat('d.m.Y H:i:s'),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::custom()
            ->setLabel('Cálculo')
            ->setCallback(function ($instance) {
                        $create_name_SQL = \App\User::where('id', $instance->create_id)->first();
                        if (count($create_name_SQL) > 0) {
                            $create_name = $create_name_SQL->bigname;
                        }
                        else {
                            $create_name = "DESCONHECIDO";
                        }
                        $create_color = "bg-yellow";

                        $valid_name_SQL = \App\User::where('id', $instance->valid_id)->first();

                        if (count($valid_name_SQL) > 0) {
                            $valid_name = $valid_name_SQL->bigname;
                        }
                        else {
                            $valid_name = "DESCONHECIDO";
                        }

                        if (count($valid_name_SQL) > 0) {
                            $valid_color = "bg-green";
                            $create_color = "bg-green";
                            $valid_name_div = "<div class=\"$create_color text-center\" title=\"$instance->updated_at\">Validado ($valid_name)</div>";
                        } else {
                            $valid_color = "bg-red";
                            $valid_name_div = "
                                <button style=\"width:100%\" data-rel=\"$instance->id\" type=\"button\" class=\"work_absences_validation btn $valid_color\" data-toggle=\"modal\" data-target=\"#WorkAbsencesModal\">
                                    Por Validar
                                </button>
                            ";
                        }
                        
                        return "
                        <div class=\"$create_color text-center\" title=\"$instance->created_at\">Aberto ($create_name)</div>
                        $valid_name_div
                        ";
                    }),

        ]);

        return $display;
    }
    public function onEdit($id) {
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('workabsences_create.js', asset('assets/js/workabsences_create.js'), ['admin-default']);

        $form->addHeader([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::select('user_id', 'Funcionário')->setModelForOptions(new \App\User)->setDisplay('bigname')->required()
            ], 3)
            ->addColumn([
                AdminFormElement::text('title', 'Motivo')->required()
            ], 7)
            ->addColumn([
                AdminFormElement::password('create_id', 'Código de Criação')
            ], 2)
        ]);
        $form->addHeader([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::date('start_at', 'Data de Início')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
            ], 3)
            ->addColumn([
                AdminFormElement::date('end_at', 'Data de Término')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
            ], 3)
            ->addColumn([
                AdminFormElement::text('obs', 'Observações')
            ], 6)
        ]);
        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }
}

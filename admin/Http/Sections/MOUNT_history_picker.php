<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MOUNT_history_picker extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->picker);
            unset($model->picker2);
            unset($model->total);
            unset($model->published);

            // INTEGRAÇÃO NXT2PHC
            unset($model->n2p_color_wood);
            unset($model->n2p_color_steel);
            unset($model->n2p_custom);
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\MountUser::orderBy('id', 'desc')->first()->id;
            \App\Model\MountUser::where('id', $id)->delete();

            foreach ($_REQUEST['obs'] as $key => $value) {
                $result = \App\Model\MountUser::where('request_id', \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id)->count();
                if ($result > 0) {
                    $last_serial = \App\Model\MountUser::where('request_id', \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id)->orderby('serial', 'desc')->first()->serial;
                } else {
                    $last_serial = 0;
                }
                \App\Model\MountUser::insert(
                        array('id' => null,
                            'request_id' => \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id,
                            'user_id' => $_REQUEST['user_id'],
                            'valid_id' => $_REQUEST['user_id'],
                            'value' => 1,
                            'serial' => $_REQUEST['serial'][$key],
                            'quantity' => 1,
                            'last_serial' => $last_serial,
                            'obs' => $_REQUEST['obs'][$key],
                            'published' => 1,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()));

                $object_request = \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first();

                $object_freeze = \App\Model\FreezeWeek::where('product_id', $object_request->product_id)->orderBy('week', 'asc')->orderBy('end_at', 'asc')->first();

                if (count($object_freeze) == 1) {
                    \App\Model\FreezeWeek::where('id', $object_freeze->id)->delete();
                }

                $object_triple = \App\Model\TripleWeek::where('product_id', $object_request->product_id)->orderBy('end_at', 'asc')->first();
                if (count($object_triple) > 0) {
                    $nova_montagem = $object_triple->montagem - 1;
                    DB::table('triple_week')->where('id', $object_triple->id)->update(['montagem' => $nova_montagem]);
                }
                
                // INTEGRAÇÃO NXT2PHC
                
                $N2P_ref = \App\Model\Product::where('id', $object_request->product_id)->first()->mini_code . "" . \App\Model\Product::where('id', $object_request->product_id)->first()->code;
                $N2P_designation = \App\Model\StatusRequests::where('mini_code', \App\Model\Product::where('id', $object_request->product_id)->first()->mini_code)->where('code', \App\Model\Product::where('id', $object_request->product_id)->first()->code)->first()->designation;
                $N2P_quantity = 1;
                $N2P_serial = \App\Model\Product::where('id', $object_request->product_id)->first()->code . "" . $_REQUEST['request_id'][$key] . "" . $_REQUEST['serial'][$key];
                $N2P_wood = $_REQUEST['n2p_color_wood'];
                $N2P_steel = $_REQUEST['n2p_color_steel'];
                $N2P_custom = $_REQUEST['n2p_custom'];
                $N2P_supplier = "ANTONIO FERNANDO C. MARTINS CORREIA";
                $N2P_armazem = 1;
                DB::table('nxt_to_phc')->insert(array('id' => null, 'ref' => $N2P_ref, 'designation' => $N2P_designation, 'quantity' => $N2P_quantity, 'serial' => $N2P_serial, 'color_wood' => $N2P_wood, 'color_steel' => $N2P_steel, 'custom' => $N2P_custom, 'supplier' => $N2P_supplier, 'armazem' => $N2P_armazem));
                
            }

//            print_r($object_freeze);    
            //connectPHC($_REQUEST, 'mount2PHC', 'picker');
            mount2PHC($_REQUEST, 'picker');
            \Artisan::call('check_updates');
        });
    }

//    public function initialize() {
//        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            $id = \App\Model\MountUser::orderBy('id', 'desc')->first()->id;
//            \App\Model\MountUser::where('id', $id)->delete();            
//            foreach ($_REQUEST['picker'] as $key => $value) {
//                //$ldate = date('Y-m-d H:i:s');
//                \App\Model\MountUser::insert(array('id' => null, 'request_id' => $_REQUEST['request_id'], 'user_id' => $_REQUEST['user_id'], 'value' => 1, 'serial' => $value, 'obs' => $_REQUEST['observations'][$key], 'quantity' => $_REQUEST['quantity'], 'last_serial' => $_REQUEST['last_serial'], 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));
//            }
//
//        });
//    }

    /**
     * @var string
     */
    protected $title = 'MOUNT - Histórico';

    public function getCreateTitle() {
        return 'A enviar para o Armazém';
    }

    /**
     * @var string
     */
    protected $alias = "mount/history_picker";

    /**
     * @return DisplayInterface
     */
    public function onCreate() {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('mount_create_picker.js', asset('assets/js/mount_create_picker.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('picker')
                        ->setLabel('Código de Barras (Coluna)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('picker2')
                        ->setLabel('Código de Barras (Caixa)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('total')->setDefaultValue(0)
                        ->setLabel('Total de Produtos')
                            ], 2)
        ]);
        $SELECT_WOOD = [];
        $SELECT_WOOD[''] = '-';
        $SQL = \App\Model\Customizations::where("color_wood", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_WOOD["$item->value"] = $item->name;
        }

        $SELECT_STEEL = [];
        $SELECT_STEEL[''] = '-';
        $SQL = \App\Model\Customizations::where("color_steel", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_STEEL["$item->value"] = $item->name;
        }

        $SELECT_CUSTOM = [];
        $SELECT_CUSTOM[''] = '-';
        $SQL = \App\Model\Customizations::where("custom", 1)->where("montagem", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT_CUSTOM["$item->value"] = $item->name;
        }
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('n2p_color_wood', 'Cor Madeira')->setOptions($SELECT_WOOD)->setDefaultValue("")
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('n2p_color_steel', 'Cor Metal')->setOptions($SELECT_STEEL)->setDefaultValue("")
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('n2p_custom', 'Customização')->setOptions($SELECT_CUSTOM)->setDefaultValue("")
                            ], 8)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('value')->setDefaultValue(1)
                    ->required(),
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para o Armazém'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

<?php

use App\Model\CNC_semanal;
use App\Model\RequestWeek;
use App\Model\RequestUser;
use App\Model\Request;
use App\Model\Product;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(CNC_semanal::class, function (ModelConfiguration $model) {
    $model->setTitle('CNC - Semanal')->setAlias('request/week')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->setOrder([[1, 'asc']])->paginate(100);

        $display->setApply(function($query) {
            //$query->orderBy('end_wood_at', 'asc');
            //$check = RequestWeek::orderBy('end_wood_at', 'asc')->get();
//            
//            $check = RequestWeek::get()->sortBy('end_cnc_at');
//            
//            $get_semanal = RequestWeek::all();
//            $get_semanal = $get_semanal->sortBy(function($get_semanal) {
//                return $get_semanal->end_cnc_at;
//            });

            $check = RequestWeek::get()->sortBy('end_cnc_at');

            // lista de pedidos semanais por ordem
            $SEMANAL = [];
            $week = [];
//            print_r("<pre>");
            foreach ($check as $key => $instance) {
                //$total = RequestWeek::where('id', $instance->id)->get()->sum('quantity')->sortBy('end_cnc_at');

                $total_week = RequestWeek::where('product_id', $instance->product_id)->sum('carpintaria');

//                $total_req = Request::where('product_id', $instance->product_id)->first();

                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = RequestUser::where('request_id', $go->id)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                foreach (RequestWeek::where('product_id', $instance->product_id)->get()->sortBy('end_cnc_at') as $key => $value) {
                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
                }
            }
//            print_r("<pre>");
//            print_r($SEMANAL);
//            print_r("</pre>");
            foreach ($SEMANAL as $key2 => $value2) {
                foreach ($value2['semana'] as $key3 => $value3) {
                    if ($SEMANAL[$key2]['total_montado'] > 0) {
                        if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        }
                        //print_r($SEMANAL[$key2]['semana'][$key3]);
                    }
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                        $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    }
                }
            }
//            print_r("<pre>");
//            print_r($SEMANAL);
//            print_r("</pre>");
            $query->whereIn('id', $week);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');


        $display->setColumns([

            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_cnc_at)));

                        $CSS = "";
                        $datework = \Carbon\Carbon::parse($instance->end_cnc_at);
                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }
                        

                        if ($curdate > $mydate) {
                            $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                        } else if ($curdate <= $mydate) {
                            if ($curdate == $mydate) {
                                $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                            } else {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            }
                        }
                        return $status_date;
                    }),
                    AdminColumn::datetime('end_cnc_at')
                    ->setLabel('Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
            AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {

                        $product_name = Product::where('id', $instance->product_id)->first();
                        return $product_name->name;
                    }),
                    AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('1%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {

                                //$total = RequestWeek::where('id', $instance->id)->sum('quantity')->sortBy('end_cnc_at');

                                $total_week = RequestWeek::where('product_id', $instance->product_id)->sum('carpintaria');

//                                $total_req = Request::where('product_id', $instance->product_id)->first();

                                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                                $total_inc = 0;
                                foreach ($total_req2 as $go) {
                                    $dump = RequestUser::where('request_id', $go->id)->sum('value');
                                    $total_inc = $total_inc + $dump;
                                }
                                //print_r($total_inc);

                                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                                foreach (RequestWeek::where('product_id', $instance->product_id)->get()->sortBy('end_cnc_at') as $key => $value) {
                                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
                                }

                                foreach ($SEMANAL as $key2 => $value2) {
                                    foreach ($value2['semana'] as $key3 => $value3) {
                                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            }
                                            //print_r($SEMANAL[$key2]['semana'][$key3]);
                                        }
                                        //print_r($SEMANAL[$instance->product_id]['semana'][$key3]['id']);
                                        if ($SEMANAL[$instance->product_id]['semana'][$key3]['id'] == $instance->id) {
                                            global $VALOR;
                                            $VALOR = $SEMANAL[$key2]['semana'][$key3]['value'];
                                            return $VALOR;
                                        }
                                    }
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('1%')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                $product_name = \App\Model\Product::where('id', $instance->product_id)->first();
                                $POINTS = $VALOR * $product_name->points;
                                return $POINTS;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente')
        ]);

        return $display;
    });
    //    
});

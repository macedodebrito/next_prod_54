<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class TripleWeek extends Section {

    public function convertTime($dec) {
        // start by converting to seconds
        $seconds = ($dec * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($dec);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS
        return $this->lz($hours) . "H" . $this->lz($minutes);
    }

    // lz = leading zero
    public function lz($num) {
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }

    public function incDate($CALC, $total_hours) {
        $START_DATE = date('d.m.Y');
        $INC_DAYS = $CALC;
        $d = new DateTime($START_DATE);
        $t = $d->getTimestamp();
        // loop for X days
        for ($i = 0; $i < $INC_DAYS; $i++) {
            // add 1 day to timestamp
            $addDay = 86400;
            // get what day it is next day
            $nextDay = date('w', ($t + $addDay));
            // if it's Saturday or Sunday get $i-1
            if ($nextDay == 0 || $nextDay == 6) {
                $i--;
            }
            // modify timestamp, add 1 day
            $t = $t + $addDay;
        }
        $d->setTimestamp($t);
        return $d->format('l, d.m.Y');
    }

    public function incDate3($CALC, $total_hours) {
        $start = new DateTime(date('d.m.Y H:i'));
        $end = new DateTime("02.11.2016 19:00");
// otherwise the  end date is excluded (bug?)
        //$end->modify('+1 day');

        $interval = $start->diff($end);

// total days
        $days = $interval->days;
        $days_inMin = ($interval->d * 24 * 60) + ($interval->h * 60) + $interval->i;

// create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
        $holidays = array('2014-03-07');

        foreach ($period as $dt) {
            $curr = $dt->format('D');

            // for the updated question
            if (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
                $days_inMin -= (24 * 60);
            }

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
                $days_inMin -= (24 * 60);
            }
        }
        $office_hrs_min = $days_inMin - ($days * (14.5 * 60));

        //echo 'Days: ' . $days;
        //echo '<br>Days in Minutes: ' . $days_inMin . ' min = ' . $days_inMin / (24 * 60) . ' days';
        return $office_hrs_min;
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Objectivos';

    /**
     * @var string
     */
//    protected $alias ="new_triple_weeks";
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(50)
                ->setOrder([[0, 'asc']])
                ->withPackage('jquery')
                ->addScript('triple.js', asset('assets/js/triple.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('end_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Expedição')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('products.name')->setLabel('Produto'),
            AdminColumn::text('montagem')->setLabel('Quantidade'),
            AdminColumn::text('client')->setLabel('Cliente'),
            AdminColumn::text('obs')->setLabel('Observações'),
//                    AdminColumn::custom()
//                    ->setLabel('Total (Horas)')
//                    ->setWidth('100px')
//                    ->setCallback(function ($instance) {
//                                return "<b>" . $this->convertTime($instance->total_hours) . "</b>";
//                            }),
            AdminColumn::custom()
                    ->setLabel('Total (Dias)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                $CALC = $CALC + $instance->total_days;
                                return $instance->total_days;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Prazo de Entrega (Dias)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                return $CALC;
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('Data de Entrega')
//                    ->setWidth('100px')
//                    ->setCallback(function ($instance) {
//                                global $CALC;
//                                if ($CALC != 0) {
//                                    $INC_DATE = $this->incDate($CALC, $instance->total_hours);
//                                    return "<div class=\"bg-gray text-center\">$INC_DATE</div>";
//                                }
//                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos (Acumulado)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $POINTS;
                                global $POINTS_interno;
                                global $POINTS_color;
                                $config = \App\Model\Config::find(1);
                                $week_0 = $config->week_0;
                                $week_1 = $week_0 + $config->week_1;
                                $week_2 = $week_1 + $config->week_2;
                                $week_3 = $week_2 + $config->week_3;
                                $POINTS = $POINTS + $instance->total_points;
                                $POINTS_interno = $POINTS_interno + $instance->total_points;
                                if ($POINTS <= $week_0) {
                                    $COR = "bg-yellow";
                                    $POINTS_color = "bg-yellow";                                    
                                }                                
                                elseif ($POINTS <= $week_1) {
                                    $COR = "bg-green";                                    
                                    if (($POINTS_interno > 0) && ($POINTS_color == "bg-yellow")) {
                                        $POINTS_interno = $instance->total_points;
                                        $POINTS_color = "bg-green";
                                    }
                                    
                                }
                                 elseif ($POINTS <= $week_2) {
                                    $COR = "bg-aqua";
                                    if (($POINTS_interno > 0) && ($POINTS_color == "bg-green")) {
                                        $POINTS_interno = $instance->total_points;
                                        $POINTS_color = "bg-aqua";
                                    }
                                }
                                else {
                                    $COR = "bg-gray";
                                    if (($POINTS_interno > 0) && ($POINTS_color == "bg-aqua")) {
                                        $POINTS_interno = $instance->total_points;
                                        $POINTS_color = "bg-gray";
                                    }
                                }
                                return "<div class=\"$COR\">$POINTS ($POINTS_interno)</div>";
                            }),
        ]);

        return $display;
    }
    public function onEdit($id) {

        $form = AdminForm::panel();   

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::datetime('end_at')->setLabel('Data de Término para a Montagem')
                            ], 3)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Data'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;      
    }
}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Serie extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Séries';
    
    public function getCreateTitle() {
        return 'Criar uma Nova Série';
    } 
    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::table()->setNewEntryButtonText('Adicionar Nova Série');

        $display->setHtmlAttribute('class', 'table-bordered table-success table-hover');

        $display->setApply(function ($query) {
            $query->orderBy('order', 'asc');
        });       

        $display->setColumns([
            AdminColumn::text('id')->setLabel('#')->setWidth('30px'),
            AdminColumn::link('name')->setLabel('Nome da Série'),
            AdminColumn::count('products')
            ->setLabel('Produtos<br/><small>(total)</small>')
            ->setHtmlAttribute('class', 'text-center hidden-sm hidden-xs')
            ->setWidth('50px'),            
            AdminColumn::order()
            ->setLabel('Ordem')
            ->setHtmlAttribute('class', 'text-center')
            ->setWidth('100px'),            
            ]);
        
        return $display;
    }

    public function onEdit($id) {
        return AdminForm::form()->setItems([
            AdminFormElement::text('name', 'Nome da Série')->required(),
            ]);
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}
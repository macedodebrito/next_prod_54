<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PREP_open extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Preparação - Entradas da CNC';

    /**
     * @var string
     */
    protected $alias = "prep/open";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
                ->withPackage('jquery')
                ->addScript('prep_create.js', asset('assets/js/prep_create.js'), ['admin-default']);

        $display->setApply(function($query) {
            $query->orderBy('prep', 'asc')->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Data de Entrada')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::text('request_pp.pp')
                    ->setLabel('Nº PP')
                    ->setWidth('1%'),
                    /* 
                    AdminColumn::custom()
                    ->setLabel('Nº PP')
                    ->setWidth('40px')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                return $pp->pp;
                            }), 
                    */

            AdminColumn::custom()->setLabel('Imagens')->setWidth('1%')->setCallback(function ($instance) {

                        $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                        $IMG = "";
                        $IMG_1 = "";
                        $IMG_2 = "";
                        $IMG_3 = "";
                        $CAPTION_1 = "";
                        $CAPTION_2 = "";
                        $CAPTION_3 = "";

                        if ($product_name->img_1) {
                            if ($product_name->caption_1) {
                                $CAPTION_1 = $product_name->caption_1;
                            }
                            $IMG_1 = "<div><span><img src=\"../../$product_name->img_1\"></span><i>$CAPTION_1</i></div>";
                        }
                        if ($product_name->img_2) {
                            if ($product_name->caption_2) {
                                $CAPTION_2 = $product_name->caption_2;
                            }
                            $IMG_2 = "<div><span><img src=\"../../$product_name->img_2\"></span><i>$CAPTION_2</i></div>";
                        }
                        if ($product_name->img_3) {
                            if ($product_name->caption_3) {
                                $CAPTION_3 = $product_name->caption_3;
                            }
                            $IMG_3 = "<div><span><img src=\"../../$product_name->img_3\"></span><i>$CAPTION_3</i></div>";
                        }

                        if (($product_name->img_1) || ($product_name->img_2) || ($product_name->img_3)) {
                            $IMG = " <i class=\"watch_product_img fa fa-picture-o fa-2x\"></i><div>$IMG_1 $IMG_2 $IMG_3</div>";
                        }
                        return $IMG;
                    }),            
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();                               
                                $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->request_pp['custom_id'])->first();
                                $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                                return "<div text-center\">$product_name->name</div>$COLOR";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Maquinado')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {

                                $total_request = \App\Model\RequestUser::where('id', $instance->id)->first();

                                if ($total_request->value > 0) {
                                    $new_color = "bg-success";
                                } else {
                                    $new_color = "bg-aqua";
                                }
                                global $VALOR;
                                $VALOR = $total_request->value;
                                return "<div class=\"$new_color text-center\">$total_request->value</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Operador CNC')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Validado por')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-aqua text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->valid_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->valid_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->valid_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">Por validar</div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                                $POINTS = $VALOR * $product_name->points;
                                return $POINTS;
                            }),            
            AdminColumn::text('obs')->setLabel('Observações'),
        ]);
        $display->setDatatableAttributes(['searching' => false]);
        return $display;
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class WOODFINISHING_History_Picker extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->picker);
            unset($model->picker2);
            unset($model->total);
            unset($model->published);

            // INTEGRAÇÃO NXT2PHC
            //unset($model->n2p_custom);
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\WoodFinishingUser::orderBy('id', 'desc')->first()->id;
            \App\Model\WoodFinishingUser::where('id', $id)->delete();

            foreach ($_REQUEST['obs'] as $key => $value) {
                $result = \App\Model\WoodFinishingUser::where('request_id', \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id)->count();
                \App\Model\WoodFinishingUser::insert(
                        array('id' => null,
                            'request_id' => \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first()->id,
                            'user_id' => $_REQUEST['user_id'],
                            'valid_id' => 0,
                            'value' => 1,
                            'obs' => $_REQUEST['obs'][$key],
                            'published' => 0,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        )
                );

                $object_request = \App\Model\Request::where('pp', $_REQUEST['request_id'][$key])->first();
            }
            \Artisan::call('check_updates');
        });
        $this->deleted(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Artisan::call('check_updates');
        });
        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Artisan::call('check_updates');
        });
    }

    /**
     * @var string
     */
    protected $title = 'Acabamento - Histórico Picagem';

    public function getCreateTitle() {
        return 'A enviar para a Pintura';
    }

    /**
     * @var string
     */
    protected $alias = "woodfinishing/history_picker";

    /**
     * @return DisplayInterface
     */
    public function onCreate() {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('woodfinishing_create_picker.js', asset('assets/js/woodfinishing_create_picker.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('picker')
                        ->setLabel('Código de Barras (PP)')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::hidden('picker2')
                        ->setLabel('Código de Barras (Caixa)')
                            ], 5)
                    ->addColumn([
                        AdminFormElement::text('total')->setDefaultValue(0)
                        ->setLabel('Total de Produtos')
                            ], 2)
        ]);
//        $SELECT = [];
//        $SELECT[''] = '-';
//        $SQL = \App\Model\Customizations::where("tecnica", 1)->orderBy('name', 'asc')->get();
//        foreach ($SQL as $item) {
//            $SELECT["$item->value"] = $item->name;
//        }
//        $form->addHeader([
//                    AdminFormElement::columns()
//                    ->addColumn([
//                        AdminFormElement::select('n2p_custom', 'Customização')->setOptions($SELECT)->setDefaultValue("")
//                            ], 2)
//                    ->addColumn([
//                        AdminFormElement::text('internal_serial')->setLabel('Serial Incorporado')
//                            ], 2)
//        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('value')->setDefaultValue(1)
                    ->required(),
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para a Pintura'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

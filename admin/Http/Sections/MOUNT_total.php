<?php

use App\Model\MOUNT_total;
use App\Model\Product;
use App\Model\Request;
use App\Model\PaintUser;
use App\Model\MountUser;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(MOUNT_total::class, function (ModelConfiguration $model) {
    $model->setTitle('MOUNT - Total')->setAlias('mount/total')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->paginate(25);

        $display->setApply(function($query) {

            $total_req = Request::groupBy('product_id')->groupBy('custom_id')->get();
            $get_ready = [];

            foreach ($total_req as $value) {
                $get_prod = Request::where('product_id', $value->product_id)->where('custom_id', $value->custom_id)->get();
                $total = 0;
                $total_check = 0;
                foreach ($get_prod as $value2) {

                    $temp_total = PaintUser::where('request_id', $value2->id)->where('published', 1)->sum('value');
                    $temp_check = MountUser::where('request_id', $value2->id)->sum('value');
                    $total = $total + $temp_total;
                    $total_check = $total_check + $temp_check;                                        
                }
                $fabricados = $total - $total_check;
                if ($fabricados > 0) {
                    $get_ready[] = $value->id;
                }
            }            
            $query->whereIn('id', $get_ready)->orderBy('created_at', 'asc');
            //$query->orderBy('created_at', 'asc')->groupBy('product_id');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setColumns([
            AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {
                        $product_name = Product::where('id', $instance->product_id)->first();
                        $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->custom_id)->first();
                        $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                        return "<div text-center\">$product_name->name</div>$COLOR";
                    }),
                    AdminColumn::custom()
                    ->setLabel('Quantidade')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $get_prod = Request::where('product_id', $instance->product_id)->where('custom_id', $instance->custom_id)->get();
                                $total = 0;
                                $total_check = 0;
                                foreach ($get_prod as $value) {
                                    $temp_total = PaintUser::where('request_id', $value->id)->where('published', 1)->sum('value');
                                    $temp_check = MountUser::where('request_id', $value->id)->sum('value');
                                    $total = $total + $temp_total;
                                    $total_check = $total_check + $temp_check;
                                }
                                $fabricados = $total - $total_check;

//                                if ($wood > $fabricados) {
//                                    $new_color = "bg-black";
//                                } else {
//                                    $new_color = "bg-success";
//                                }
                                $new_color = "bg-success";
                                return "<div class=\"$new_color text-center\">$fabricados</div>";
                            }),
        ]);

        return $display;
    });
});

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Distro_Prosp extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Nova Distribuição';

    /**
     * @var string
     */
    protected $alias = "admin/distributors_prosp";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatables()
                ->setOrder([[9, 'desc']])
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('state', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');


        $display->setColumns([
            AdminColumn::text('market')->setLabel('Mercado'),
            AdminColumn::text('name')->setLabel('Nome'),
            AdminColumn::text('email')->setLabel('Email'),
            AdminColumn::text('type')->setLabel('Tipo'),
            AdminColumn::text('seller')->setLabel('Vendedor'),
                    AdminColumn::datetime('since', 'Iniciado')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::datetime('deadline', 'Prazo')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::datetime('official_date', 'Concluido')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
            AdminColumn::text('obs')->setLabel('Observações'),
                    AdminColumn::custom()
                    ->setLabel('state')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->state == 0) {
                                    return "<div style=\"background-color: #FFFF00;\">Em curso</div>";
                                } else {
                                    return "<div style=\"background-color: #FF0000;\">Anulado</div>";
                                }
                            }),
        ]);

//        $display->setColumnFilters([
//            null,
//            AdminColumnFilter::text()->setPlaceholder('por Produto')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Quantidade')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por points')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Obs')->setOperator('contains'),
//        ]);
        //dd($instance);        
//        if ($instance->id == 3) {
//            $header_ID->getHeader()->setHtmlAttribute('class', 'bg-red');
//        }
        //$display->getColumnFilters()->setPlacement('panel.heading');
        //$display->setDatatableAttributes(['searching' => false]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $GG[] = "Em curso";
        $GG[] = "Anulado";

        $TT = [];
        $TT["Dealer"] = "Dealer";
        $TT["End User"] = "End User";
        $TT["Exclusive Dist."] = "Exclusive Distributor";
        $TT["Rental Company"] = "Rental Company";

        $SS = [];
        $SS["André Correia"] = "André Correia";
        $SS["Rui Marques"] = "Rui Marques";
        $SS["Sérgio Pinto"] = "Sérgio Pinto";

        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('market', 'Mercado')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('name', 'Nome')->required()
                            ], 6)
                    ->addColumn([
                        AdminFormElement::select('type', 'Tipo')->setOptions($TT)->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('email', 'Email')
                            ], 2)
        ]);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('seller', 'Vendedor')->setOptions($SS)->required()
                            ], 2)                
                    ->addColumn([
                        AdminFormElement::date('since', 'Iniciado')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('deadline', 'Prazo')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('official_date', 'Concluído')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                            ], 2)
        ]);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('state', 'Estado')->setOptions($GG)->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('obs', 'Observações')
                            ], 10)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

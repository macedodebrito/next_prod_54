<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\Serie;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Brito extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'BRITO/TESTE';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatablesAsync()
                ->setOrder([[0, 'desc']])
                ->paginate(25);
//        $display->setApply(function($query) {
//            $query->orderBy('created_at', 'desc');
//        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('series');

        $display->setFilters([
            AdminDisplayFilter::related('serie_id')->setModel(Serie::class)
        ]);



        $display->setColumns([
                    AdminColumn::datetime('created_at', 'Efectuado em')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
            AdminColumn::text('id')->setLabel('#')->setWidth('30px'),
            AdminColumn::link('name')->setLabel('Nome do Produto'),
            AdminColumn::link('code')->setLabel('Código'),
//            AdminColumn::link('points')->setLabel('Pontuação'),
            AdminColumn::custom()
                    ->setLabel('points')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                return $instance->points;
                            }),
            AdminColumn::text('series.name')->setLabel('Série')->append(AdminColumn::filter('serie_id'))
        ]);

//        $display->setColumnFilters([
//            null,
//            AdminColumnFilter::text()->setPlaceholder('por Produto')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Quantidade')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por points')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Obs')->setOperator('contains'),
//        ]);
        //dd($instance);        
//        if ($instance->id == 3) {
//            $header_ID->getHeader()->setHtmlAttribute('class', 'bg-red');
//        }
        //$display->getColumnFilters()->setPlacement('panel.heading');
        $display->setDatatableAttributes(['searching' => false]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $score = 0;
        while ($score <= 100) {
            $GG[] = $score;
            $score++;
        }
        return AdminForm::form()->setItems([
                    AdminFormElement::text('name', 'Nome do Produto')->required(),
                    AdminFormElement::text('code', 'Código')->required(),
                    AdminFormElement::select('serie_id', 'Série')->setModelForOptions(new Serie)->setDisplay('name')->required(),
                    AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required(),
        ]);
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

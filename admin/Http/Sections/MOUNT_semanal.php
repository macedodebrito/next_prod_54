<?php

use App\Model\MOUNT_semanal;
use App\Model\RequestWeek;
use App\Model\MountUser;
use App\Model\PaintUser;
use App\Model\Stock;
use App\Model\Request;
use App\Model\Product;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(MOUNT_semanal::class, function (ModelConfiguration $model) {
    $model->setTitle('Lista de Encomendas | Montagem')->setAlias('mount/week')->disableDeleting();

    // Display
    $model->onDisplay(function () {

        if (\Request::path() == "admin/triple_weeks") {
            $more_css = "hidden";
        } else {
            $more_css = "";
        }

//        $display = AdminDisplay::datatablesAsync()->paginate(25);
        $display = AdminDisplay::datatables()->setOrder([[1, 'asc']])->paginate(100);

        $display->setApply(function($query) {
            $query->orderBy('end_at', 'asc');

            $check = RequestWeek::orderBy('end_at', 'asc')->get();

            // lista de pedidos semanais por ordem
            $SEMANAL = [];
            $week = [];
            //print_r("<pre>");
            foreach ($check as $key => $instance) {
                $total = RequestWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('montagem');

                $total_week = RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->sum('montagem');

                $total_req = Request::where('product_id', $instance->product_id)->first();

                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = MountUser::where('request_id', $go->id)->sum('value');
//                    $dump = MountUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                foreach (RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->montagem;
                }
            }
            //print_r($SEMANAL);
            foreach ($SEMANAL as $key2 => $value2) {
                foreach ($value2['semana'] as $key3 => $value3) {
                    if ($SEMANAL[$key2]['total_montado'] > 0) {
                        if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        }
                        //print_r($SEMANAL[$key2]['semana'][$key3]);
                    }
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                        $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    }
                }
            }

//            print_r($value2);
//            print_r($SEMANAL);
//            print_r("</pre>");
            $query->whereIn('id', $week)->orderBy('end_at', 'asc');
//            $query->whereIn('id', $week)->orderBy('urgency', 'desc')->orderBy('urgency_date', "asc")->orderBy('end_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_at)));

                        $CSS = "";
                        if ($instance->urgency == 0) {
                            $urgency_date = "";
                            $datework = \Carbon\Carbon::parse($instance->end_at);
                        } else {
                            $urgency_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->urgency_date)->format('d.m');
                            $datework = \Carbon\Carbon::parse($instance->urgency_date);
                        }

                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }

                        if ($instance->urgency == 1) {
                            $status_date = "<div class='bg-red text-center'><i class='fa fa-bell'></i> URGENTE<br>$urgency_date $DAYS_DATE</div>";
                        } else {
                            if ($curdate > $mydate) {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            } else if ($curdate <= $mydate) {
                                if ($curdate == $mydate) {
                                    $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                                } else {
                                    $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                                }
                            }
                        }

                        return $status_date;
                    }),
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setCallback(function ($instance) {
                                $total = RequestWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('montagem');

                                $total_week = RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->sum('montagem');

                                $total_req = Request::where('product_id', $instance->product_id)->first();

                                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                                $total_inc = 0;
                                $total2 = 0;
                                $total_check = 0;
                                foreach ($total_req2 as $go) {
                                    $dump = MountUser::where('request_id', $go->id)->sum('value');
//                                    $dump = MountUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                                    $total_inc = $total_inc + $dump;

                                    $temp_total = PaintUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                                    $total2 = $total2 + $temp_total;
                                    $total_check = $total_check + $dump;
                                }
                                $fabricados = $total2 - $total_check;

                                //print_r($total_inc);

                                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                                foreach (RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->montagem;
                                }

                                foreach ($SEMANAL as $key2 => $value2) {
                                    foreach ($value2['semana'] as $key3 => $value3) {
                                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            }
                                            //print_r($SEMANAL[$key2]['semana'][$key3]);
                                        }
                                        //print_r($SEMANAL[$instance->product_id]['semana'][$key3]['id']);
                                        if ($SEMANAL[$instance->product_id]['semana'][$key3]['id'] == $instance->id) {
                                            global $VALOR;
                                            $VALOR = $SEMANAL[$key2]['semana'][$key3]['value'];
                                        }
                                    }
                                }
                                if (!isset($GLOBALS[$instance->product_id]['fabricados'])) {
                                    $GLOBALS[$instance->product_id]['fabricados'] = $fabricados;
                                }

                                $GLOBALS[$instance->product_id]['fabricados2'] = $GLOBALS[$instance->product_id]['fabricados'] - $VALOR;

                                $FABRICADOS = $GLOBALS[$instance->product_id]['fabricados'];
                                $FABRICADOS2 = $GLOBALS[$instance->product_id]['fabricados2'];
                                
                                $GLOBALS[$instance->product_id]['fabricados'] = $GLOBALS[$instance->product_id]['fabricados2'];
                                
                                $product_name = Product::where('id', $instance->product_id)->first();
                                
                                if ($FABRICADOS > 0 && $FABRICADOS2 < 0) {
                                    $css = "<i style=\"color: orange;\" class=\"fa fa-circle fa-2x\"></i>";
                                }
                                else if ($FABRICADOS <= 0 && $FABRICADOS2 < 0) {
                                    $css = "<i style=\"color: red;\" class=\"fa fa-circle fa-2x\"></i>";
                                }
                                else {
                                    $css = "<i style=\"color: green;\" class=\"fa fa-circle fa-2x\"></i>";    
                                }
//                                return "( $FABRICADOS2/$FABRICADOS ) $css - " . $product_name->name;
                                return "$css " . $product_name->name;
                            }),
                    AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('5%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                return $VALOR;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                $product_name = \App\Model\Product::where('id', $instance->product_id)->first();
                                $POINTS = $VALOR * $product_name->points;
                                return $POINTS;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente'),
            $header_palete_store = AdminColumn::custom()
            ->setLabel('Palete<br/>Separada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                GLOBAL $palete;
                $palete = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                GLOBAL $check;
                $check = 1;
                if (count($palete) > 0) {
                    if ($palete->status != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        // verificar se falta embalagem e redes
                        $product = Product::where('id', $instance->product_id)->first();
                        if (($product->store == 1) && ($palete->store_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->tech == 1) && ($palete->tech_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->serigrafia == 1) && ($palete->serigrafia_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->locksmith == 1) && ($palete->locksmith_auth_id == 0)) {
                            $check = 0;
                        }
                        if ($check == 1) {
                            return "<i style=\"color: orange;\" class=\"fa fa-check fa-2x\"></i>";
                        } else {
                            // se status = 0, está em falta
                            return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                        }
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_auth = AdminColumn::custom()
            ->setLabel('Palete<br/>Autorizada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                $palete = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                if (count($palete) > 0) {
                    if ($palete->auth_id != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_ask = AdminColumn::custom()
            ->setLabel('Pedir Palete')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                GLOBAL $check;
                GLOBAL $palete;
                $PALETE = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                if($PALETE) {
                if ($PALETE->phc_id) {
                    $PALETE_ID = "NSP " . $PALETE->phc_id;
                } else {
                    $PALETE_ID = "NXT " . $PALETE->request_week_id;
                }
                $ID = $PALETE->id;
                }
                else {
                    $PALETE = "LOL";
                }
                $notification = \App\Model\Notifications::where('object_id', $ID)->where('type', 1)->first();
                //if ($check == 0 && count($palete) > 0 && $palete->auth_id == 0 && count($notification) == 0) {
//                if ((count($palete) > 0) && ($palete->status != 0) && ($check == 0) && ($palete->auth_id == 0) && (count($notification) == 0)) {
                if ((count($palete) > 0) && ($palete->status != 0) && ($palete->auth_id == 0) && (count($notification) == 0)) {
                    //if ($check == 0 && count($palete) > 0 && $palete->auth_id == 0 && count($notification) == 0) {
                    //if ($check == 0 && $palete->auth_id == 0 && count($notification) == 0) {
                    return "<i id=\"request_palete_bell_$ID\" data-palete=\"$PALETE_ID\" data-rel=\"$ID\" style=\"color: #000000;\" class=\"fa fa-bell-o fa-2x request_palete_bell\"></i>";
                } elseif (count($notification) > 0 && $notification->status == 0) {
                    $nome = \App\User::where('id', $notification->open_user_id)->first()->bigname;
                    $DATA = $notification->open_at;
                    return "<i id=\"request_palete_wait_$ID\" data-obs=\"( Pedido de palete efectuado por: <b>$nome</b> <i>[ $DATA ]</i> )\" data-palete=\"$PALETE_ID\" data-rel=\"$ID\" style=\"color: orange;\" class=\"fa fa-clock-o fa-2x request_palete_wait\"></i>";
                } else {
                    return "<i style=\"color: #CCCCCC;\" class=\"fa fa-minus fa-2x\"></i>";
                }
            }),
//            AdminColumn::text('id')->setLabel('id'),
        ]);
        $header_palete_store->getHeader()->setHtmlAttribute('class', "bg-red $more_css");
        $header_palete_auth->getHeader()->setHtmlAttribute('class', "bg-black $more_css");
        $header_palete_ask->getHeader()->setHtmlAttribute('class', "bg-green $more_css");
        return $display;
    });
    //    
});

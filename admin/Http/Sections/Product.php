<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use App\Model\ProductCustom;
use App\Model\ProductCustomNxt;

class Product extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Produtos';

    public function getCreateTitle() {
        return 'Criar Novo Produto';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Novo Produto')
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('name', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('series');

        $display->setFilters([
            AdminDisplayFilter::related('serie_id')->setModel(\App\Model\Serie::class)
        ]);

        $display->setColumns([
            AdminColumn::link('id')->setLabel('#')->setWidth('30px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_1) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_1')->setLabel('Imagem 1')->setWidth('100px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_2) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_2')->setLabel('Imagem 2')->setWidth('100px'),
                    AdminColumn::custom()
                    ->setCallback(function ($instance) {
                                if ($instance->caption_3) {
                                    return "<i class=\"fa bg-red fa-check\"></i>";
                                } else {
                                    return "";
                                }
                            }),
            AdminColumn::image('img_3')->setLabel('Imagem 3')->setWidth('100px'),
            //AdminColumn::link('name')->setLabel('Nome do Produto'),
            AdminColumn::custom()
                    ->setLabel('Nome do Produto')
                    ->setCallback(function ($instance) {
                        return "$instance->name<br><small>($instance->mini_code$instance->code)<small>";
                    }),

            AdminColumn::custom()
            ->setLabel('Pintura')
            ->setCallback(function ($instance) {
                $custom="";
                $get_custom = ProductCustom::where('product_id', $instance->id)->get();
                foreach ($get_custom as $item) {
                    $custom.="<span class=\"badge\" style=\"background-color:#{$item->custom_nxt->hex_color}\">{$item->custom_nxt->name}</span>";
                }
                if (!$custom) $custom="<span class=\"badge\" style=\"background-color:#".ProductCustomNxt::find(1)->hex_color."\">".ProductCustomNxt::find(1)->name."</span>";
                return $custom;
            }),
            //AdminColumn::link('mini_code')->setLabel('Designação'),
            //AdminColumn::link('code')->setLabel('Código'),
            AdminColumn::link('points')->setLabel('Pontuação'),
            AdminColumn::text('series.name')->setLabel('Série')->append(AdminColumn::filter('serie_id')),
                    AdminColumn::custom()
                    ->setLabel('Armazem')
                    ->setCallback(function ($instance) {
                                switch ($instance->store) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Técnica')
                    ->setCallback(function ($instance) {
                                switch ($instance->tech) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serigrafia')
                    ->setCallback(function ($instance) {
                                switch ($instance->serigrafia) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serralharia')
                    ->setCallback(function ($instance) {
                                switch ($instance->locksmith) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Redes')
                    ->setCallback(function ($instance) {
                                switch ($instance->wires) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('PVC')
                    ->setCallback(function ($instance) {
                                switch ($instance->pvc) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Embalagem CNC')
                    ->setCallback(function ($instance) {
                                switch ($instance->packing_cnc) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Embalagem Espuma')
                    ->setCallback(function ($instance) {
                                switch ($instance->packing_espuma) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $GG = [];
        $score = 0;
        while ($score <= 150) {
            $GG[] = $score;
            $score++;
        }
//        return AdminForm::form()->setItems([
//                    AdminFormElement::text('name', 'Nome do Produto')->required(),
//                    AdminFormElement::text('code', 'Código')->required(),
//                    AdminFormElement::select('serie_id', 'Série')->setModelForOptions(new \App\Model\Serie)->setDisplay('name')->required(),
//                    AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required(),
//        ]);

        $form = AdminForm::panel();

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::image('img_1', 'Imagem 1')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::image('img_2', 'Imagem 2')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::image('img_3', 'Imagem 3')
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('caption_1', 'Legenda 1')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('caption_2', 'Legenda 2')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('caption_3', 'Legenda 3')
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('name', 'Nome do Produto')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('mini_code', 'Designação')->required()
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('code', 'Código')->required()
                            ], 1)
                    ->addColumn([
                        AdminFormElement::select('serie_id', 'Série')->setModelForOptions(new \App\Model\Serie)->setDisplay('name')->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::select('points', 'Pontuação')->setOptions($GG)->required()
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::checkbox('store')->setLabel('Armazém'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('tech')->setLabel('Técnica'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('serigrafia')->setLabel('Serigrafia'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('locksmith')->setLabel('Serralharia'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('wires')->setLabel('Redes'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('pvc')->setLabel('PVC'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('packing_cnc')->setLabel('Embalagem CNC'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('packing_espuma')->setLabel('Embalagem Espuma'),
                            ], 12)
        ]);
        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

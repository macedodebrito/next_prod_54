<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Colors extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Cores';

    public function getCreateTitle() {
        return 'Adicionar Tipo de Pintura';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Tipo de Pintura')
                ->paginate(25);
        
        // $display->getColumns()->getControlColumn()->setDeletable(false)->setEditable(($instance->id == 1) ? false : true);
        $display->getColumns()->getControlColumn()->setDeletable(false)->setEditable(false);
        
        $display->setApply(function($query) {            
            $query->select('products_custom_nxt.*', 'products_custom.name AS phc_name', 'products_custom.product_id AS product_id', 'products_custom.name AS name_cor', 'products_custom.custom_code')
            ->join('products_custom', 'products_custom.custom_id', 'products_custom_nxt.id')
            ->groupBy('products_custom.name')
            ->orderBy('name', 'asc');            
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::text('id')->setLabel('ID')->setWidth('1%'),
            AdminColumn::text('name')->setLabel('Nome<br/>(NXT)')->setWidth('20%'),
            AdminColumn::custom()
                    ->setLabel('Cor<br/>(HEX Code)')
                    ->setWidth('20%')
                    ->setCallback(function ($instance) {                                
                                return "<span class=\"badge\" style=\"color:$instance->hex_color;background-color:$instance->hex_color_bg\">$instance->hex_color</span>";
                            }),                
            AdminColumn::text('name_cor')->setLabel('Nome (PHC)<br/>Não editável')->setWidth('20%'),
            //AdminColumn::text('color_code')->setLabel('Cor (PHC)<br/>Não editável')->setWidth('20%'),
            AdminColumn::custom()
            ->setLabel('Cor (PHC)<br/>Não editável')
                    ->setWidth('20%')
                    ->setCallback(function ($instance) {                                
                                if ($instance->id == 1) {
                                    $custom_code = preg_replace('~\x{00a0}~siu',' ',$instance->custom_code);
                                    $custom_code = trim($custom_code);

                                    /* 
                                    $custom_code = trim(urlencode($instance->custom_code));
                                    $custom_code = str_replace("%C2", "", $custom_code);
                                    $custom_code = str_replace("+", "", $custom_code);
                                    $custom_code = str_replace("%A0", "", $custom_code);
                                    $custom_code = substr($custom_code, -3); 
                                    */
                                    return $custom_code;
                                }
                                else {
                                    return $instance->color_code;
                                }
                            }),              
            AdminColumn::custom()
            ->setLabel('Produtos<br/>(em conflito)')
            ->setWidth('20%')
            ->setCallback(function ($instance) {                        
                            if ($instance->id == 1) {
                                $products_SQL = \App\Model\ProductCustom::select('products_custom.custom_id', 'products_custom.name as phc_name', 'products.name')
                                ->join('products', 'products.id', 'products_custom.product_id')
                                ->where('products_custom.name', $instance->phc_name)
                                ->get();
                                $fix_colors = "";
                                foreach($products_SQL as $product) {
                                    $fix_colors.="$product->name, ";
                                }
                                $fix_colors = substr($fix_colors, 0, -2);
                                return "<strong style=\"color:red\">$fix_colors</strong>";
                            }
                            else {
                                return "<strong style=\"color:green\">Sincronizado</strong>";
                            }                        
                    }),  
            AdminColumn::custom()
                ->setLabel('')
                ->setWidth('20%')
                ->setCallback(function ($instance) {                        
                    if ($instance->id != 1) {
                        return "<a href=\"./colors/$instance->id/edit\" class=\"btn btn-xs btn-primary\" data-toggle=\"tooltip\" data-original-title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>";                
                    }
            }),  
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('name')->setLabel('Nome (NXT)'),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('name_cor')->setLabel('Nome (PHC)')->setReadonly(1),
                            ], 4)
                    ->addColumn([
                        AdminFormElement::text('color_code')->setLabel('Cor (PHC)')->setReadonly(1),
                            ], 2)    
                    ->addColumn([
                        AdminFormElement::text('hex_color_bg')->setLabel('Cor do fundo (HEX Code)'),
                            ], 2) 
                    ->addColumn([
                        AdminFormElement::text('hex_color')->setLabel('Cor da letra (HEX Code)'),
                            ], 2)                             
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Tipo de Pintura'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        $form = AdminForm::panel();
        $new_name_cor = [];
        $new_custom_code = [];
        $new_name_cor_SQL = \App\Model\ProductCustom::where('custom_id', 1)->where('name', '!=', null)->where('custom_code', '!=', null)->get();
        foreach ($new_name_cor_SQL as $key => $value) {
            $custom_name = preg_replace('~\x{00a0}~siu',' ',$value->name);
            $custom_name = trim($custom_name);

            $custom_code = preg_replace('~\x{00a0}~siu',' ',$value->custom_code);
            $custom_code_trim = trim($custom_code);
            $custom_code = substr($custom_code_trim, -3);
            
            $new_name_cor[$custom_name] = "$custom_name ($custom_code_trim)";
            $new_custom_code[$custom_code] = "$custom_code";
        }
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('name')->setLabel('Nome (NXT)')->required(),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('name_cor', 'Nome (PHC)')
                        ->setOptions($new_name_cor)
                        ->required()
                            ], 4)
                    ->addColumn([
                        AdminFormElement::select('name_cor', 'Cor (PHC)')
                        ->setOptions($new_custom_code)
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('hex_color_bg')->setLabel('Cor do fundo (HEX Code)')->required(),
                            ], 2) 
                    ->addColumn([
                        AdminFormElement::text('hex_color')->setLabel('Cor da letra (HEX Code)')->required(),
                            ], 2) 
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Criar Novo Tipo de Pintura'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

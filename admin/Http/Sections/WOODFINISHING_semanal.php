<?php

use App\Model\WOODFINISHING_semanal;
use App\Model\RequestWeek;
use App\Model\WoodUser;
use App\Model\WoodFinishingUser;
use App\Model\PaintUser;
use App\Model\MountUser;
use App\Model\Stock;
use App\Model\Request;
use App\Model\Product;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(WOODFINISHING_semanal::class, function (ModelConfiguration $model) {
    $model->setTitle('WOODFINISHING - Semanal')->setAlias('woodfinishing/week')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->setOrder([[1, 'asc']])->paginate(100);

        $display->setApply(function($query) {
            $query->orderBy('end_woodfinishing_at', 'asc');

            $check = RequestWeek::orderBy('end_woodfinishing_at', 'asc')->get();

            // lista de pedidos semanais por ordem
            $SEMANAL = [];
            $week = [];
//            print_r("<pre>");
            foreach ($check as $key => $instance) {
                $total = RequestWeek::where('id', $instance->id)->orderBy('end_wood_at', 'asc')->sum('carpintaria');

                $total_week = RequestWeek::where('product_id', $instance->product_id)->orderBy('end_wood_at', 'asc')->sum('carpintaria');

                $total_req = Request::where('product_id', $instance->product_id)->first();

                //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                $total_req2 = Request::where('product_id', $instance->product_id)->get();
                $total_inc = 0;
                foreach ($total_req2 as $go) {
                    $dump = WoodFinishingUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                }
                //print_r($total_inc);

                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                foreach (RequestWeek::where('product_id', $instance->product_id)->orderBy('end_wood_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
                }
            }
//            print_r($SEMANAL);
            foreach ($SEMANAL as $key2 => $value2) {
                foreach ($value2['semana'] as $key3 => $value3) {
                    if ($SEMANAL[$key2]['total_montado'] > 0) {
                        if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                        }
                        //print_r($SEMANAL[$key2]['semana'][$key3]);
                    }
                    if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
                        $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
                    }
                }
            }

//            print_r($SEMANAL);
//            print_r("</pre>");
            $query->whereIn('id', $week)->orderBy('end_woodfinishing_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([

            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_woodfinishing_at)));

                        $CSS = "";
                        $datework = \Carbon\Carbon::parse($instance->end_woodfinishing_at);
                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }


                        if ($curdate > $mydate) {
                            $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                        } else if ($curdate <= $mydate) {
                            if ($curdate == $mydate) {
                                $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                            } else {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            }
                        }
                        return $status_date;
                    }),
                    AdminColumn::datetime('end_woodfinishing_at')
                    ->setLabel('Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m'),
//                    AdminColumn::custom()
//                    ->setLabel('Data de Término')
//                    ->setWidth('100px')
//                    ->setHtmlAttribute('class', 'text-center')
//                    ->setCallback(function ($instance) {
//                                //return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->end_at)->subDays(7)->format('d.m.Y');
//                                return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->end_wood_at)->format('d.m.Y');
//                            }),
            AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {

                        $total = RequestWeek::where('id', $instance->id)->orderBy('end_wood_at', 'asc')->sum('carpintaria');

                        $total_week = RequestWeek::where('product_id', $instance->product_id)->orderBy('end_wood_at', 'asc')->sum('carpintaria');

                        $total_req = Request::where('product_id', $instance->product_id)->first();

                        //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

                        $total_req2 = Request::where('product_id', $instance->product_id)->get();
                        $total_inc = 0;
                        $total2 = 0;
                        $total_check = 0;
                        foreach ($total_req2 as $go) {
                            $dump = WoodFinishingUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                            $total_inc = $total_inc + $dump;

                            $temp_total = WoodUser::where('request_id', $go->id)->where('published', 1)->sum('value');
                            $total2 = $total2 + $temp_total;
                            $total_check = $total_check + $dump;
                        }
                        $fabricados = $total2 - $total_check;                                //print_r($total_inc);

                        $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                        $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
                        $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                        $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

                        foreach (RequestWeek::where('product_id', $instance->product_id)->orderBy('end_wood_at', 'asc')->get() as $key => $value) {
                            $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                            $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
                        }

                        foreach ($SEMANAL as $key2 => $value2) {
                            foreach ($value2['semana'] as $key3 => $value3) {
                                if ($SEMANAL[$key2]['total_montado'] > 0) {
                                    if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                    } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                        $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                        $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                    }
                                    //print_r($SEMANAL[$key2]['semana'][$key3]);
                                }
                                //print_r($SEMANAL[$instance->product_id]['semana'][$key3]['id']);
                                if ($SEMANAL[$instance->product_id]['semana'][$key3]['id'] == $instance->id) {
                                    global $VALOR;
                                    $VALOR = $SEMANAL[$key2]['semana'][$key3]['value'];
                                }
                            }
                        }

                        if (!isset($GLOBALS[$instance->product_id]['fabricados'])) {
                            $GLOBALS[$instance->product_id]['fabricados'] = $fabricados;
                        }

                        $GLOBALS[$instance->product_id]['fabricados2'] = $GLOBALS[$instance->product_id]['fabricados'] - $VALOR;

                        $FABRICADOS = $GLOBALS[$instance->product_id]['fabricados'];
                        $FABRICADOS2 = $GLOBALS[$instance->product_id]['fabricados2'];

                        $GLOBALS[$instance->product_id]['fabricados'] = $GLOBALS[$instance->product_id]['fabricados2'];

//                        $product_name = Product::where('id', $instance->product_id)->first();
                        $product_name = \App\Model\Product::where('id', $instance->product_id)->first();
                        $IMG = "";
                        $IMG_1 = "";
                        $IMG_2 = "";
                        $IMG_3 = "";
                        $CAPTION_1 = "";
                        $CAPTION_2 = "";
                        $CAPTION_3 = "";

                        if ($product_name->img_1) {
                            if ($product_name->caption_1) {
                                $CAPTION_1 = $product_name->caption_1;
                            }
                            $IMG_1 = "<div><span><img src=\"../$product_name->img_1\"></span><i>$CAPTION_1</i></div>";
                        }
                        if ($product_name->img_2) {
                            if ($product_name->caption_2) {
                                $CAPTION_2 = $product_name->caption_2;
                            }
                            $IMG_2 = "<div><span><img src=\"../$product_name->img_2\"></span><i>$CAPTION_2</i></div>";
                        }
                        if ($product_name->img_3) {
                            if ($product_name->caption_3) {
                                $CAPTION_3 = $product_name->caption_3;
                            }
                            $IMG_3 = "<div><span><img src=\"../$product_name->img_3\"></span><i>$CAPTION_3</i></div>";
                        }

                        if (($product_name->img_1) || ($product_name->img_2) || ($product_name->img_3)) {
                            $IMG = " <i class=\"watch_product_img fa fa-picture-o fa-2x\"></i><div>$IMG_1 $IMG_2 $IMG_3</div>";
                        }
                        
                        if ($FABRICADOS > 0 && $FABRICADOS2 < 0) {
                            $css = "<i style=\"color: orange;\" class=\"fa fa-circle fa-2x\"></i>";
                        } else if ($FABRICADOS <= 0 && $FABRICADOS2 < 0) {
                            $css = "<i style=\"color: red;\" class=\"fa fa-circle fa-2x\"></i>";
                        } else {
                            $css = "<i style=\"color: green;\" class=\"fa fa-circle fa-2x\"></i>";
                        }
//                                return "( $FABRICADOS2/$FABRICADOS ) $css - " . $product_name->name;
                        return "$css " . $product_name->name. $IMG;
                    }),
                    AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                return $VALOR;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $VALOR;
                                $product_name = \App\Model\Product::where('id', $instance->product_id)->first();
                                $POINTS = $VALOR * $product_name->points;
                                return $POINTS;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente')
        ]);

        return $display;
    });
    //    
});

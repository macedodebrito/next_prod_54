<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Config2 extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Config2';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
//    public function onDisplay() {
//        // Display
//        $display = $this->fireEdit(1);
//        return $display;
//    }

    public function onEdit($id) {
        $form = AdminForm::panel();   

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('week_0')->setLabel('Semana 0')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('week_1')->setLabel('Semana 1')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('week_2')->setLabel('Semana 2')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('week_3')->setLabel('Semana 3')
                            ], 3)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new Save())->setText('Editar Objectivos Semanais'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => null,
        ]);

        return $form;
    }

}

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Model\ModelConfiguration;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Deadline extends Section {

    public function convertTime($dec) {
        // start by converting to seconds
        $seconds = ($dec * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($dec);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS
        return $this->lz($hours) . "H" . $this->lz($minutes);
    }

    // lz = leading zero
    public function lz($num) {
        return (strlen($num) < 2) ? "0{$num}" : $num;
    }

    public function incDate($CALC, $total_hours) {
        $START_DATE = date('d.m.Y');
        $INC_DAYS = $CALC;
        $d = new DateTime($START_DATE);
        $t = $d->getTimestamp();
        // loop for X days
        for ($i = 0; $i < $INC_DAYS; $i++) {
            // add 1 day to timestamp
            $addDay = 86400;
            // get what day it is next day
            $nextDay = date('w', ($t + $addDay));
            // if it's Saturday or Sunday get $i-1
            if ($nextDay == 0 || $nextDay == 6) {
                $i--;
            }
            // modify timestamp, add 1 day
            $t = $t + $addDay;
        }
        $d->setTimestamp($t);
        return $d->format('l, d.m.Y');
    }

    public function incDate3($CALC, $total_hours) {
        $start = new DateTime(date('d.m.Y H:i'));
        $end = new DateTime("02.11.2016 19:00");
// otherwise the  end date is excluded (bug?)
        //$end->modify('+1 day');

        $interval = $start->diff($end);

// total days
        $days = $interval->days;
        $days_inMin = ($interval->d * 24 * 60) + ($interval->h * 60) + $interval->i;

// create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
        $holidays = array('2014-03-07');

        foreach ($period as $dt) {
            $curr = $dt->format('D');

            // for the updated question
            if (in_array($dt->format('Y-m-d'), $holidays)) {
                $days--;
                $days_inMin -= (24 * 60);
            }

            // substract if Saturday or Sunday
            if ($curr == 'Sat' || $curr == 'Sun') {
                $days--;
                $days_inMin -= (24 * 60);
            }
        }
        $office_hrs_min = $days_inMin - ($days * (14.5*60));
        
        //echo 'Days: ' . $days;
        //echo '<br>Days in Minutes: ' . $days_inMin . ' min = ' . $days_inMin / (24 * 60) . ' days';
        return $office_hrs_min;
    }
    
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Prazos de Entrega';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'asc']])
                ->withPackage('jquery')
                ->addScript('deadline.js', asset('assets/js/deadline.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);
        
        $display->setApply(function($query) {

            $total = \App\Model\Deadline::orderBy('end_at', 'asc')->get();
            $GG = [];
            foreach ($total as $encomenda) {
                $total_week = \App\Model\Deadline::where('product_id', $encomenda->product_id)->orderBy('end_at', 'asc')->sum('montagem');
                $total_req = \App\Model\Request::where('product_id', $encomenda->product_id)->first();
                $total_req2 = \App\Model\Request::where('product_id', $encomenda->product_id)->get();
                $total_inc = 0;
                $total_inc2 = 0;
                foreach ($total_req2 as $go) {
                    $dump = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', 0)->sum('value');
                    $dump2 = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                    $total_inc2 = $total_inc2 + $dump2;
                }
                $SEMANAL[$encomenda->product_id]['product_id'] = $encomenda->product_id;
                $SEMANAL[$encomenda->product_id]['name'] = \App\Model\Product::where('id', $encomenda->product_id)->first()->name;
                $SEMANAL[$encomenda->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$encomenda->product_id]['total_montado'] = $total_inc;
                $SEMANAL[$encomenda->product_id]['total_montado2'] = $total_inc2;
                foreach (\App\Model\Deadline::where('product_id', $encomenda->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$encomenda->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$encomenda->product_id]['semana'][$key]['value'] = $value->montagem;
                }
                foreach ($SEMANAL as $key2 => $value2) {
                    foreach ($value2['semana'] as $key3 => $value3) {
                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                            }
                        }
                        //dd($SEMANAL);
                        if ($SEMANAL[$key2]['semana'][$key3]['id'] == $encomenda->id) {
                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                if ($SEMANAL[$key2]['total_montado'] < 0) {
                                    $GG['pendentes'][] = $encomenda->id;
                                } else {
                                    $GG['feitas'][] = $encomenda->id;
                                }
                            } else {
                                $GG['falta'][] = $encomenda->id;
                            }
                        }
                    }
                }
            }

            if (!isset($GG['pendentes'])) {
                $GG['pendentes'] = "";
            }
            if (!isset($GG['feitas'])) {
                $GG['feitas'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }

            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 2) {
                    $query->whereIn('id', $GG['feitas'])->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['pendentes'])->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 0) {
                    $query->whereIn('id', $GG['falta'])->orderBy('end_at', 'asc');
                }
            } else {
                $query->whereIn('id', $GG['falta'])->orderBy('end_at', 'asc');
                //
                // select brand, name, "10" as age from cars...
                //$query->whereIn('id', $GG['falta']);
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Expedição')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('products.name')->setLabel('Produto'),
            AdminColumn::text('products.points')->setLabel('Pontuação'),
                    AdminColumn::custom()
                    ->setLabel('Status Actual')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                if ($instance->actual_status == "PENDING") {
                                    return "<div class=\"bg-yellow text-center\">PENDING</div>";
                                } else if ($instance->actual_status == "OK") {
                                    return "<div class=\"bg-green text-center\">OK</div>";
                                } else {
                                    return "<div class=\"bg-red text-center\">$instance->actual_status</div>";
                                }
                            }),
            AdminColumn::text('client')->setLabel('Cliente'),
            AdminColumn::text('obs')->setLabel('Observações'),
                    AdminColumn::custom()
                    ->setLabel('Total (Pontos)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                return $instance->total_points;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Total (Horas)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                return "<b>" . $this->convertTime($instance->total_hours) . "</b>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Total (Dias)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                $CALC = $CALC + $instance->total_days;
                                return $instance->total_days;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Prazo de Entrega (Dias)')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                return $CALC;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Data de Entrega')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                if ($CALC != 0) {
                                    $INC_DATE = $this->incDate($CALC, $instance->total_hours);
                                    return "<div class=\"bg-green text-center\">$INC_DATE</div>";
                                }
                            }),
        ]);

        return $display;
    }

}

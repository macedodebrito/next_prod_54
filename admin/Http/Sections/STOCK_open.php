<?php

use App\Model\STOCK_open;
use App\Model\Product;
use App\Model\PaintUser;
use App\Model\Request;
use App\Model\MountUser;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(STOCK_open::class, function (ModelConfiguration $model) {
    $model->setTitle('STOCK - Entradas')->setAlias('stock/open')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->paginate(25);

        $display->setApply(function($query) {
            $query->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Data de Entrada')
                    ->setWidth('160px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Nº Pedido de Produção')
                    ->setWidth('20%')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                $pp = Request::where('id', $instance->request_pp['id'])->first();
                                return $pp->pp;
                            }),
            AdminColumn::custom()->setLabel('Produto')->setCallback(function ($instance) {

                        $product_name = Product::where('id', $instance->request_pp['product_id'])->first();
                        return $product_name->name;
                    }),
                    AdminColumn::custom()
                    ->setLabel('Pintado')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {

                                $total_request = PaintUser::where('id', $instance->id)->first();

                                if ($total_request->value > 0) {
                                    $new_color = "bg-success";
                                } else {
                                    $new_color = "bg-aqua";
                                }
                                return "<div class=\"$new_color text-center\">$total_request->value</div>";
                            })
        ]);

        return $display;
    });
    //    
});

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\UserLogin;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Clock_Month_2 extends Section {

    function sumarHoras($acumuladoTime, $nuevoTime) {

        //Se esperan parametros así:
        //$acumuladoTime="02:45";
        //$nuevoTime="04:36";
        //echo "Hora acumulada: $acumuladoTime"."<br>";
        //echo "Nuevo tiempo acumulado: $nuevoTime"."<br>";

        /* Tiempo acumulado */
        $myArrayAcumuladoTime = explode(":", $acumuladoTime);

        $hrsAcumuladoTime = $myArrayAcumuladoTime[0];
        $minsAcumuladoTime = $myArrayAcumuladoTime[1];

        /* Nuevo Time */
        $myArrayNewTime = explode(":", $nuevoTime);

        $hraNewTime = $myArrayNewTime[0];
        $minNewTime = $myArrayNewTime[1];

        /* Calculo */
        $sumHrs = $hrsAcumuladoTime + $hraNewTime;
        $sumMins = $minsAcumuladoTime + $minNewTime;

        /* Si se pasan los MINUTOS */
        if ($sumMins > 59) {
            /* Quitamos hora para dejarlo en minutos y se la sumamos a la de horas */
            $sumMins-=60;
            $sumHrs+=1;
        }

        // echo "Total hrs agregadas: $sumHrs:$sumMins"."<br>";
        return "$sumHrs:$sumMins";
    }

    function AddPlayTime($times) {

        $minutes = "0";
        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute) = explode(':', $time);
            $minutes += $hour * 60;
            $minutes += $minute;
        }

        $hours = floor($minutes / 60);
        $minutes -= $hours * 60;

        // returns the time already formatted
        return sprintf('%02d:%02d', $hours, $minutes);
    }

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Relógio de Ponto';

    /**
     * @var string
     */
    protected $alias = "clock_months_2";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        global $ACUMULADO;
        $ACUMULADO = "00:00";
        if (isset($_GET['id']) && isset($_GET['m']) && isset($_GET['y'])) {
            if ($_GET['m'] == "1") {
                $MES = "Janeiro";
            } else if ($_GET['m'] == "2") {
                $MES = "Fevereiro";
            } else if ($_GET['m'] == "3") {
                $MES = "Março";
            } else if ($_GET['m'] == "4") {
                $MES = "Abril";
            } else if ($_GET['m'] == "5") {
                $MES = "Maio";
            } else if ($_GET['m'] == "6") {
                $MES = "Junho";
            } else if ($_GET['m'] == "7") {
                $MES = "Julho";
            } else if ($_GET['m'] == "8") {
                $MES = "Agosto";
            } else if ($_GET['m'] == "9") {
                $MES = "Setembro";
            } else if ($_GET['m'] == "10") {
                $MES = "Outubro";
            } else if ($_GET['m'] == "11") {
                $MES = "Novembro";
            } else {
                $MES = "Dezembro";
            }
            $YEAR = $_GET['y'];
            $user_name = \App\Model\Clock_2::where('id', $_GET['id'])->first()->bigname;
            ;
        }

        $display = AdminDisplay::datatables()->paginate(50);
        $display->getColumns()->getControlColumn()->setDeletable(false)->setEditable(true);
        $display->setTitle("Funcionário: <b>$user_name</b> (Registos para $MES de $YEAR)");

        $display->setApply(function($query) {
            $dump = \App\Model\Clock_month_2::select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day, created_at, login_logout, id"))
                    ->orderBy('year', 'desc')
                    ->orderBy('month', 'desc')
                    ->orderBy('day', 'asc')
                    ->where('user_id', $_GET['id'])
                    ->get();
            $GG = [];
            $DD = [];
            foreach ($dump as $value) {
                if (($value->year == $_GET['y']) && ($value->month == $_GET['m'])) {
                    if (!in_array($value->day, $DD)) {
                        // dia do mês, nao repetido
                        $DD[] = $value->day;
                        // primeiro ID do mês
                        $GG[] = $value->id;
                    }
                }
            }
            $query->whereIn('id', $GG)->get();
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Dia')
                    ->setCallback(function ($instance) {
                                $dump = \App\Model\Clock_month_2::select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day, created_at, login_logout, id"))
                                        ->orderBy('year', 'desc')
                                        ->orderBy('month', 'desc')
                                        ->orderBy('day', 'asc')
                                        ->where('id', $instance->id)
                                        ->first();
                                $MONTH = $dump->month;
                                $YEAR = $dump->year;
                                $ID = $_GET['id'];
                                $DAY = $dump->day;

                                return "<a href=\"clock_days?id=$ID&y=$YEAR&m=$MONTH&d=$DAY\"><div>$DAY</div></a>";
//                                return "<div>$DAY</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('---')
                    ->setCallback(function ($instance) {
                                $dump = \App\Model\Clock_month_2::select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day, DAYNAME(created_at) nome_dia, created_at, login_logout, id"))
                                        ->orderBy('year', 'desc')
                                        ->orderBy('month', 'desc')
                                        ->orderBy('day', 'asc')
                                        ->where('id', $instance->id)
                                        ->first();
                                $MONTH = $dump->month;
                                $YEAR = $dump->year;
                                $ID = $_GET['id'];
                                $DAY = $dump->day;
                                $NOME = $dump->nome_dia;
                                $BG = "";
                                if ($NOME == "Sunday") {
                                    $NOME_SEMANA = "Domingo";
                                    $BG = "bg-purple";
                                } else if ($NOME == "Monday") {
                                    $NOME_SEMANA = "Segunda";
                                } else if ($NOME == "Tuesday") {
                                    $NOME_SEMANA = "Terça";
                                } else if ($NOME == "Wednesday") {
                                    $NOME_SEMANA = "Quarta";
                                } else if ($NOME == "Thursday") {
                                    $NOME_SEMANA = "Quinta";
                                } else if ($NOME == "Friday") {
                                    $NOME_SEMANA = "Sexta";
                                } else if ($NOME == "Saturday") {
                                    $NOME_SEMANA = "Sábado";
                                    $BG = "bg-purple";
                                }

                                return "<span class=\"$BG\" style=\"margin-right: 10px; padding: 4px;\">$NOME_SEMANA</span>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('(Acumulado) Entradas e Saidas')
                    ->setCallback(function ($instance) {
                                $dump1 = \App\Model\Clock_month_2::select(DB::raw("YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day, created_at, login_logout, id"))
                                        ->orderBy('year', 'desc')
                                        ->orderBy('month', 'desc')
                                        ->orderBy('day', 'asc')
                                        ->where('id', $instance->id)
                                        ->first();
                                $DAY = $dump1->day;

                                $dump = \App\Model\Clock_month_2::orderBy('created_at', 'asc')
                                        ->where('user_id', $_GET['id'])
                                        ->whereBetween('created_at', array(Carbon::createFromFormat('Y-m-d H:i:s', $instance->created_at)->format('Y-m-d'), Carbon::createFromFormat('Y-m-d H:i:s', $instance->created_at)->addDays(1)->format('Y-m-d')))
                                        ->get();



                                $GG = "";
//                                $DD = 0;
                                $times = array();

                                foreach ($dump as $value) {
                                    $hora = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('H:i:s');
                                    $last_hora = $value->created_at;
                                    $last_one = "";

//                                    $data = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('Y-m-d');
//                                    $hora_08_30 = Carbon::createFromFormat('Y-m-d H:i:s', "$data 08:30:00")->format('Y-m-d H:i:s');
//                                    $data_fix = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('Y-m-d H:i:s');
//                                    $teste = $data_fix->gte($hora_08_30);
//                                    print_r($teste."<br/>");
                                    // ENTRADA MANHA | 08:30:00 (ANTIGO)
                                    $manha_entrada_antiga_1 = str_pad("081500", 2, '0', STR_PAD_LEFT);
                                    $manha_entrada_antiga_2 = str_pad("084500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA MANHA | 12:30:00 (ANTIGO)
                                    $manha_saida_antiga_1 = str_pad("121500", 2, '0', STR_PAD_LEFT);
                                    $manha_saida_antiga_2 = str_pad("124500", 2, '0', STR_PAD_LEFT);

                                    // ENTRADA TARDE | 14:00:00
                                    $tarde_entrada_antiga_1 = str_pad("134500", 2, '0', STR_PAD_LEFT);
                                    $tarde_entrada_antiga_2 = str_pad("141500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA TARDE | 18:30:00 (ANTIGO)
                                    $tarde_saida_antiga_1 = str_pad("181500", 2, '0', STR_PAD_LEFT);
                                    $tarde_saida_antiga_2 = str_pad("184500", 2, '0', STR_PAD_LEFT);

                                    $data_before = "20170331";

                                    // ENTRADA MANHA | 08:30:00 (NOVO)
                                    $manha_entrada_nova_1 = str_pad("081500", 2, '0', STR_PAD_LEFT);
                                    $manha_entrada_nova_2 = str_pad("084500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA MANHA | 13:00:00 (NOVO)
                                    $manha_saida_nova_1 = str_pad("124500", 2, '0', STR_PAD_LEFT);
                                    $manha_saida_nova_2 = str_pad("131500", 2, '0', STR_PAD_LEFT);

                                    // ENTRADA TARDE | 14:00:00
                                    $tarde_entrada_nova_1 = str_pad("134500", 2, '0', STR_PAD_LEFT);
                                    $tarde_entrada_nova_2 = str_pad("141500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA TARDE | 18:00:00 (ANTIGO)
                                    $tarde_saida_nova_1 = str_pad("174500", 2, '0', STR_PAD_LEFT);
                                    $tarde_saida_nova_2 = str_pad("181500", 2, '0', STR_PAD_LEFT);

                                    $data_after = "20170930";

                                    // ENTRADA MANHA | 08:30:00 (AFTER)
                                    $manha_entrada_after_1 = str_pad("081500", 2, '0', STR_PAD_LEFT);
                                    $manha_entrada_after_2 = str_pad("084500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA MANHA | 13:00:00 (NOVO)
                                    $manha_saida_after_1 = str_pad("124500", 2, '0', STR_PAD_LEFT);
                                    $manha_saida_after_2 = str_pad("131500", 2, '0', STR_PAD_LEFT);

                                    // ENTRADA TARDE | 14:00:00
                                    $tarde_entrada_after_1 = str_pad("134500", 2, '0', STR_PAD_LEFT);
                                    $tarde_entrada_after_2 = str_pad("141500", 2, '0', STR_PAD_LEFT);

                                    // SAIDA TARDE | 17:30:00 (ANTIGO)
                                    $tarde_saida_after_1 = str_pad("171500", 2, '0', STR_PAD_LEFT);
                                    $tarde_saida_after_2 = str_pad("174500", 2, '0', STR_PAD_LEFT);

                                    // HORARIOS CNC


                                    $hora_fix = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('His');
                                    $data_fix = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('Ymd');
                                    $get_date = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('Y-m-d');

                                    $current_time = (int) $hora_fix;

                                    // VERIFICAR HORA DE ENTRADA DE MANHA (ANTIGA)
                                    if (($current_time >= $manha_entrada_antiga_1) && ($current_time <= $manha_entrada_antiga_2) && ($value->login_logout == "E") && ($data_fix <= $data_before)) {
                                        $hora = "08:30:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE ENTRADA DE MANHA (NOVO)
                                    else if (($current_time >= $manha_entrada_nova_1) && ($current_time <= $manha_entrada_nova_2) && ($value->login_logout == "E") && ($data_fix <= $data_after)) {
                                        $hora = "08:30:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE ENTRADA DE MANHA (AFTER)
                                    else if (($current_time >= $manha_entrada_after_1) && ($current_time <= $manha_entrada_after_2) && ($value->login_logout == "E") && ($data_fix > $data_after)) {
                                        $hora = "08:30:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE SAIDA DE MANHA (ANTIGO)
                                    else if (($current_time >= $manha_saida_antiga_1) && ($current_time <= $manha_saida_antiga_2) && ($value->login_logout == "S") && ($data_fix <= $data_before)) {
                                        $hora = "12:30:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    }
                                    // VERIFICAR HORA DE SAIDA DE MANHA (NOVO)
                                    else if (($current_time >= $manha_saida_nova_1) && ($current_time <= $manha_saida_nova_2) && ($value->login_logout == "S") && ($data_fix <= $data_after)) {
                                        $hora = "13:00:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    }
                                    // VERIFICAR HORA DE SAIDA DE MANHA (AFTER)
                                    else if (($current_time >= $manha_saida_after_1) && ($current_time <= $manha_saida_after_2) && ($value->login_logout == "S") && ($data_fix > $data_after)) {
                                        $hora = "13:00:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    }
                                    // VERIFICAR HORA DE ENTRADA DE TARDE (ANTIGO)
                                    elseif (($current_time >= $tarde_entrada_antiga_1) && ($current_time <= $tarde_entrada_antiga_2) && ($value->login_logout == "E") && ($data_fix <= $data_before)) {
                                        $hora = "14:00:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE ENTRADA DE TARDE (NOVO)
                                    else if (($current_time >= $tarde_entrada_nova_1) && ($current_time <= $tarde_entrada_nova_2) && ($value->login_logout == "E") && ($data_fix <= $data_after)) {
                                        $hora = "14:00:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE ENTRADA DE TARDE (AFTER)
                                    else if (($current_time >= $tarde_entrada_after_1) && ($current_time <= $tarde_entrada_after_2) && ($value->login_logout == "E") && ($data_fix > $data_after)) {
                                        $hora = "14:00:00";
                                        $entrada = Carbon::parse("$get_date $hora");
                                        $last_hora = $entrada;
                                    }
                                    // VERIFICAR HORA DE SAIDA DE TARDE (ANTIGO)
                                    else if (($current_time >= $tarde_saida_antiga_1) && ($current_time <= $tarde_saida_antiga_2) && ($value->login_logout == "S") && ($data_fix <= $data_before)) {
                                        $hora = "18:30:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    }
                                    // VERIFICAR HORA DE SAIDA DE TARDE (NOVO)
                                    else if (($current_time >= $tarde_saida_nova_1) && ($current_time <= $tarde_saida_nova_2) && ($value->login_logout == "S") && ($data_fix <= $data_after)) {
                                        $hora = "18:00:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    }
                                    // VERIFICAR HORA DE SAIDA DE TARDE (NOVO)
                                    else if (($current_time >= $tarde_saida_after_1) && ($current_time <= $tarde_saida_after_2) && ($value->login_logout == "S") && ($data_fix > $data_after)) {
                                        $hora = "17:30:00";
                                        $saida = Carbon::parse("$get_date $hora");
                                    } else {
                                        if ($value->login_logout == "E") {
                                            $entrada = $value->created_at;
//                                            print_r("?+? $hora<br/>");
                                        } else if ($value->login_logout == "S") {
                                            $saida = $value->created_at;
//                                            print_r("?-? $hora<br/>");
                                        }
                                    }

                                    if ($value->login_logout == "E") {
                                        $new_color = "bg-green";
                                        $last_one = $value->login_logout;
//                                        $entrada = $value->created_at;                                        
                                    } else if ($value->login_logout == "S") {
                                        $new_color = "bg-red";
                                        $last_one = $value->login_logout;
//                                        $saida = $value->created_at;

                                        if (isset($entrada)) {
//                                            $totalDuration = $saida->diffInMinutes($entrada);
                                            $totalDuration = $saida->diff($entrada)->format('%H:%I');
//                                            $DD += $DD + $totalDuration;
//                                            $teste = $saida->diff($entrada)->format('%H:%I:%S');
                                            $times[] = $totalDuration;
                                        }
                                    }
                                    $GG .= " <span class=\"$new_color\" style=\"margin-right: 10px; padding: 4px;\"> $hora </span> ";
                                }
                                //dd($hora);
                                // verificar se o último está no dia seguinte
                                if ($last_one = "E") {
                                    $temp = \App\Model\Clock_month_2::orderBy('created_at', 'asc')
                                            ->where('user_id', $_GET['id'])
                                            ->where('created_at', '>', $last_hora)
                                            ->first();

                                    if (count($temp) > 0 && ($temp->login_logout == "S")) {
                                        $ULTIMAHORA = Carbon::createFromFormat('Y-m-d H:i:s', $temp->created_at)->format('H:i:s');
                                        $GG .= " <span class=\"bg-gray\" style=\"margin-right: 10px; padding: 4px;\"> $ULTIMAHORA </span> ";
//                                        $totalDuration = $temp->created_at->diffInMinutes($last_hora);
                                        $totalDuration = $temp->created_at->diff($last_hora)->format('%H:%I');
//                                        $DD = $DD + $totalDuration;
                                        $times[] = $totalDuration;
                                    }
                                }
//                                $startT = gmdate("H:i", ($DD * 60));
                                $startT = $this->AddPlayTime($times);
                                global $ACUMULADO;
                                $get_acumulado = $this->sumarHoras($ACUMULADO, $startT);
                                $ACUMULADO = $get_acumulado;
                                return "<span class=\"bg-black\" style=\"margin-right: 10px; padding: 4px;\"> $startT </span> $GG";
                            }),
                            AdminColumn::custom()
                            ->setLabel('Acumulado Mensal')
                            ->setCallback(function ($instance) {
                                global $ACUMULADO;
                                return $ACUMULADO;
                            }),
                ]);

                return $display;
            }

            public function onEdit($id) {
                
            }

        }
        
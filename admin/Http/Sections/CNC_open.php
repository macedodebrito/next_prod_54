<?php

use App\Model\CNC_open;
use App\Model\RequestUser;
use App\Model\Request;
use App\Model\Product;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(CNC_open::class, function (ModelConfiguration $model) {
    static $new_color = null;
    $model->setTitle('CNC - Abertos')->setAlias('requests/cnc_open')->disableDeleting();
    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables();

        $display->setApply(function($query) {
            $total = Request::get();
            $get_ready = [];
            foreach ($total as $key => $value) {
                $efectuados = RequestUser::where('request_id', $value->id)->where('published', 1)->sum('value');
                if ($value->value - $efectuados != 0) {
                    $get_ready[] = $value->id;
                }
            }
            $query->whereIn('id', $get_ready)->orderBy('order_at', 'asc');
            //$query->orderBy('start_at', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'request_users');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(Product::class)
        ]);

        $display->setColumns([
            AdminColumn::custom()->setLabel('#')->setCallback(function ($instance) {
                        $get_order = Request::orderBy('order_at', 'asc')->get();
                        $c = 0;
                        if (count($get_order) > 0) {
                            foreach ($get_order as $value) {
                                $c++;
                                if ($value->id == $instance->id) {
                                    $efectuados = $c;
                                }
                            }
                            return $efectuados;
                        }
                    })->setWidth('50px')->setHtmlAttribute('class', 'bg-warning text-center'),
                    AdminColumn::datetime('start_at')
                    ->setLabel('Data de Início Autorizada')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::text('pp')
                    ->setLabel('Nº do Pedido de Produção')
                    ->setWidth('20%')
                    ->setHtmlAttribute('class', 'bg-gray text-center'),
                    AdminColumn::text('products.name')->setLabel('Produto'),
                    
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $product_name = \App\Model\Product::where('id', $instance->product_id)->first();                               
                                $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->custom_id)->first();
                                $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                                return "<div text-center\">$product_name->name</div>$COLOR";
                            }),

                    AdminColumn::text('value')->setLabel('Total PP')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center'),
                    AdminColumn::custom()->setLabel('Maquinados')
                    ->setCallback(function ($instance) {
                                $efectuados = RequestUser::where('request_id', $instance->id)->where('published', 1)->sum('value');
                                if ($efectuados == null) {
                                    $efectuados = 0;
                                }
                                return $efectuados;
                            })->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center'),
                    AdminColumn::custom()->setLabel('Por Maquinar')
                    ->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $total = Request::where('id', $instance->id)->first();
                                $efectuados = RequestUser::where('request_id', $instance->id)->where('published', 1)->sum('value');
                                $missing = $total->value - $efectuados;

                                if ($missing < 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-danger";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            })
                    ->setWidth('10%')
        ]);

        return $display;
    });
    //
});

<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
//
use App\Role;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use SleepingOwl\Admin\Contracts\Initializable;

class PREP_history extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    
    /**
     * @var string
     */
    protected $title = 'Preparação - Histórico';

    public function initialize() {
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Artisan::call('check_updates');
        });
        $this->deleted(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Artisan::call('check_updates');
        });
        $this->updated(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Artisan::call('check_updates');
        });
    }
     
    /**
     * @var string
     */
    public function getCreateTitle() {
        return 'A enviar para a Carpintaria';
    }

    protected $alias = 'prep/history';

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatablesAsync()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
                ->setNewEntryButtonText('Enviar para a Carpintaria')
                ->withPackage('jquery')
                ->addScript('prep_create.js', asset('assets/js/prep_create.js'), ['admin-default']);

        $display->setApply(function($query) {
            $query->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Efectuado em')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::custom()
                    ->setLabel('Nº Pedido de Produção')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-gray text-center')
                    ->setCallback(function ($instance) {
                                $new_color = "";
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\PrepUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$pp->pp</div>";
                            }),
            AdminColumn::custom()
                ->setLabel('Produto')
                ->setWidth('100px')
                ->setCallback(function ($instance) {
                        $product_name = \App\Model\Product::where('id', $instance->request_pp['product_id'])->first();
                        $new_color = "";
                        $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                        $dump = \App\Model\PrepUser::where('id', $instance->id)->first();
                        if ($dump->published < 0) {
                            $new_color = "bg-red";
                        }
                        $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $pp->custom_id)->first();
                        $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                        return "<div class=\"$new_color \">$product_name->name</div>$COLOR";
                    }),
                    AdminColumn::custom()
                    ->setLabel('Quantidade')
                    ->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setCallback(function ($instance) {
                                $entrance = \App\Model\PrepUser::where('id', $instance->id)->first();
                                $total_cnc = \App\Model\RequestUser::where('request_id', $instance->request_id)->sum('value');
                                $total_request = \App\Model\Request::where('id', $instance->request_id)->first();
                                $total_wood = \App\Model\PrepUser::where('request_id', $instance->request_id)->sum('value');

                                if ($total_wood > $total_cnc) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }

                                $new_color = "";
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\PrepUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$entrance->value</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Operador Preparação')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $entrance = \App\Model\PrepUser::where('id', $instance->id)->first();
                                $total_cnc = \App\Model\RequestUser::where('request_id', $instance->request_id)->sum('value');
                                $total_request = \App\Model\Request::where('id', $instance->request_id)->first();
                                $total_wood = \App\Model\PrepUser::where('request_id', $instance->request_id)->sum('value');

                                if ($total_wood > $total_cnc) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }

                                $new_color = "";
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\PrepUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                $sql_points = \App\Model\Product::where('id', $total_request->product_id)->first();
                                $PONTOS = $entrance->value * $sql_points->points;
                                return "<div class=\"$new_color text-center\">$PONTOS</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Observações')
                    ->setCallback(function ($instance) {
                                $new_color = "";
                                $pp = \App\Model\Request::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\PrepUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$instance->obs</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('5%')
                    ->setCallback(function ($instance) {
                                // warning 
                                // check = 1
                                // remove
                                // user-times = -1
                                $default_html_tag = "#pendente";
                                $default_icon = "fa-circle-o-notch fa-spin fa-2x";

                                $role_check = 0;
                                $role_id = \App\Role::where('name', 'carpintaria')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }

                                switch ($instance->published) {
                                    case 1:
                                        $default_icon = "fa-check fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    case -1:
                                        $default_icon = "fa-user-times fa-2x";
                                        $default_html_tag = "#removing";
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"$default_html_tag\"><i class=\"fa $default_icon\"></i></a>";
                                        } else {
                                            return "<i class=\"fa fa-user-times fa-2x\"></i>";
                                        }
                                        break;
                                    default:
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"#removing_fast\"><i class=\"fa fa-user-times fa-2x\"></i></a> <i class=\"fa $default_icon\"></i>";
                                        } else {
                                            return "<a href=" . url('/admin/wood/open') . "><i class=\"fa $default_icon\"></i></a>";
                                        }
                                        break;
                                }
                            })
        ]);
//        $display->setDatatableAttributes(['searching' => false]);
        return $display;
    }

    public function onEdit($id) {
        $get_pp = \App\Model\RequestUser::orderBy('created_at', 'asc')->groupBy('id')->get();
        $new_request_pp = [];
        foreach ($get_pp as $key => $value) {
            $get_request = \App\Model\Request::where('id', $value->request_id)->first();
            $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();
            $get_color = \App\Model\ProductCustomNxt::where('id', $get_request->custom_id)->first();

            $total = \App\Model\RequestUser::where('request_id', $value->request_id)->sum('value');
            $permitido = \App\Model\PrepUser::where('request_id', $value->request_id)->sum('value');
            $check = $total - $permitido;
            $new_request_pp[0] = "Escolha um Pedido para Produção";
            if ($check > 0) {
                $new_request_pp[$value->request_id] = "#$get_request->pp - $get_product->name ($get_color->name)";
            }
        };

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('prep_edit.js', asset('assets/js/prep_edit.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('request_id', 'Pedido de Produção')
                        ->setOptions($new_request_pp)
                        ->required()
                            ], 4)
                    ->addColumn([
                        AdminFormElement::select('value', 'Quantidade')
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
            AdminFormElement::text('obs')->setLabel('Observação')
        ]);

        $form->getButtons()
                ->setSaveButtonText('Gravar')
                ->hideSaveAndCloseButton()
                ->setCancelButtonText('Cancelar')
                ->hideSaveAndCreateButton();

        return $form;
    }

    public function onCreate($id = null) {
        $get_pp = \App\Model\RequestUser::orderBy('created_at', 'asc')->groupBy('id')->get();
        $new_request_pp = [];
        foreach ($get_pp as $key => $value) {
            $get_request = \App\Model\Request::where('id', $value->request_id)->first();
            $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();
            $get_color = \App\Model\ProductCustomNxt::where('id', $get_request->custom_id)->first();

            $total = \App\Model\RequestUser::where('request_id', $value->request_id)->sum('value');
            $permitido = \App\Model\PrepUser::where('request_id', $value->request_id)->sum('value');
            $check = $total - $permitido;
            $new_request_pp[0] = "Escolha um Pedido para Produção";
            if ($check > 0) {
                $new_request_pp[$value->request_id] = "#$get_request->pp - $get_product->name ($get_color->name)";
            }
        };

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('prep_create.js', asset('assets/js/prep_create.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('request_id', 'Pedido de Produção')
                        ->setOptions($new_request_pp)
                        ->required()
                            ], 4)
                    ->addColumn([
                        AdminFormElement::select('value', 'Quantidade')
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('published')->setDefaultValue(0)
                    ->required(),
            AdminFormElement::text('obs')->setLabel('Observação')
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para a Carpintaria'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

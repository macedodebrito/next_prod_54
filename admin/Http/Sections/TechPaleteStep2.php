<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Role;

class TechPaleteStep2 extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Gestão de Paletes - Stock';

    /**
     * @var string
     */
    protected $alias = "tech/1";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->addScript('tech_paletes.js', asset('assets/js/tech_paletes.js'), ['admin-default'])
                ->addStyle('paletes.css', asset('assets/css/paletes.css'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('phc_id', 'desc')->orderBy('tech_week_id', 'desc')->where('store_id', '!=', 0)->where('auth_id', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('tech_week');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->tech_week_id\" data-rel=\"$instance->phc_id\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF; cursor: pointer;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"nxt_phc text-center\" rel=\"$instance->id\" data-value=\"$instance->tech_week_id\" data-rel=\"\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF; cursor: pointer;\">NXT $instance->tech_week_id</div>";
                                }
                                return $GG;
                            }),
                    AdminColumn::datetime('tech_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('90px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('70px')
                    ->setCallback(function ($instance) {
                                $request_week_info = \App\Model\TechWeek::where('id', $instance->tech_week_id)->first()->tech_id;
                                return \App\Model\Tech::where('id', $request_week_info)->first()->name;
                            }),
            AdminColumn::text('tech_week.quantity')->setLabel('Quantidade')->setWidth('100px'),
            AdminColumn::text('tech_week.client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Obs Palete')
                    ->setCallback(function ($instance) {
                                $role_check = 0;
                                $role_id = Role::where('name', 'admin')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                if ($role_check == 1) {
                                    if ($instance->store_obs) {
                                        return "[ <a rel=\"$instance->id\" class=\"second_obs\" href=\"#$instance->id\" data-rel=\"$instance->store_obs\"><i class=\"fa fa-pencil\"></i></a> ] $instance->store_obs";
                                    } else {
                                        return "<a rel=\"$instance->id\" class=\"second_obs\" href=\"#$instance->id\"><i class=\"fa fa-plus-square-o fa-2x\"></i></a>";
                                    }
                                } else {
                                    return $instance->store_obs;
                                }
                            }),                                 
                    AdminColumn::custom()
                    ->setLabel('Autorizado Por')
                    ->setWidth('60px')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->store_id != 0) {
                                    if ($instance->auth_id != 0) {
                                        $valid_id = \App\User::where('id', $instance->auth_id)->first();
                                        if (count($valid_id) > 0) {
                                            $nome = \App\User::where('id', $instance->auth_id)->first()->bigname;
                                            return "$nome<br/>$instance->auth_date";
                                        } else {
                                            return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                        }
                                    } else {
                                        return "<div class=\"bg-red\">Por autorizar</div><br/><span class=\"auth_picks auth_picks_code_auth_$instance->id\"><a id=\"$instance->id\" rel=\"\" class=\"accept_auth\" href=\"#accept_product\"><i class=\"fa fa-check fa-2x\"></i></a></span> <input id=\"picks_code_auth_$instance->id\" type=\"password\" size=\"10\" class=\"form-control picks_code_auth\"><input type=\"hidden\" id=\"auth_id_picks_code_auth_$instance->id\">";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">À espera do Armazém</div>";
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('10px')
                    ->setCallback(function ($instance) {
                                return "<i class=\"fa fa-remove fa-2x\"></i>";
                            })
        ]);

        return $display;
    }

}

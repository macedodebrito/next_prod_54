<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LocksmithPaleteHistory extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Paletes - Serralharia / Histórico';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'desc']]);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('locksmith_week_id', 'desc')->where('status', "=", 2);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('locksmith_week');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('ID')
                    ->setWidth('1%')
                    ->setCallback(function ($instance) {
                                $GG = "";
                                if ($instance->phc_id != 0) {
                                    $GG = "<div class=\"text-center\" style=\"background: #FF0000; padding: 4px 10px; color: #FFFFFF;\">NSP $instance->phc_id</div>";
                                } else {
                                    $GG = "<div class=\"text-center\" style=\"background: #000000; padding: 4px 10px; color: #FFFFFF;\">NXT $instance->locksmith_week_id</div>";
                                }
                                return $GG;
                            }),            
                    AdminColumn::datetime('locksmith_week.end_at')
                    ->setLabel('Data de Entrega')
                    ->setWidth('160px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $request_week_info = \App\Model\LocksmithWeek::where('id', $instance->locksmith_week_id)->first()->locksmith_id;
                                return \App\Model\Locksmith::where('id', $request_week_info)->first()->name;
                            }),
            AdminColumn::text('locksmith_week.quantity')->setLabel('Quantidade')->setWidth('100px'),
            AdminColumn::text('locksmith_week.client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Autorizado Por')
                    ->setWidth('160px')->setHtmlAttribute('class', 'bg-info text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->store_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->store_id)->first();
                                    if (count($valid_id) > 0) {
                                        $nome = \App\User::where('id', $instance->store_id)->first()->bigname;
                                        return "$nome<br/>$instance->stored_date";
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                } else {
                                    return "<div class=\"bg-red\">Por separar</div>";
                                }
                            }),
        ]);

        return $display;
    }

}

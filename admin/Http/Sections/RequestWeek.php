<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\Auth;

class RequestWeek extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->group_quantity);
            unset($model->product_id_selector);
            unset($model->new_quantity);
            unset($model->observations);
            $model->product_id = -9889;
            $model->quantity = -9889;
//            dd($_REQUEST);          
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\RequestWeek::orderBy('id', 'desc')->first()->id;
            \App\Model\RequestWeek::where('id', $id)->delete();

            $end_at = new Carbon($_REQUEST['end_at']);
            $end_paint_at = new Carbon($_REQUEST['end_paint_at']);
            $end_woodfinishing_at = new Carbon($_REQUEST['end_woodfinishing_at']);
            $end_wood_at = new Carbon($_REQUEST['end_wood_at']);
            if (isset($_REQUEST['urgency_date'])) {
                $urgency_date = new Carbon($_REQUEST['urgency_date']);
            } else {
                $urgency_date = null;
            }
            if (isset($_REQUEST['urgency'])) {
                $urgency = 1;
            } else {
                $urgency = 0;
            }

            foreach ($_REQUEST['product_id_selector'] as $key => $value) {
                $last_id = \App\Model\RequestWeek::insertGetId(array('id' => null, 'product_id' => $_REQUEST['product_id_selector'][$key], 'user_id' => $_REQUEST['user_id'], 'quantity' => $_REQUEST['new_quantity'][$key], 'carpintaria' => $_REQUEST['new_quantity'][$key], 'woodfinishing' => $_REQUEST['new_quantity'][$key], 'pintura' => $_REQUEST['new_quantity'][$key], 'montagem' => $_REQUEST['new_quantity'][$key], 'client' => $_REQUEST['client'], 'obs' => $_REQUEST['observations'][$key], 'group' => $_REQUEST['group'], 'end_at' => $end_at, 'end_paint_at' => $end_paint_at, 'end_woodfinishing_at' => $end_woodfinishing_at, 'end_wood_at' => $end_wood_at, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString(), 'urgency_date' => $urgency_date, 'urgency' => $urgency));
                \App\Model\Palete::insert(array('id' => null, 'request_week_id' => $last_id));
            }
            // SYNC
            \Artisan::call('schedule:run');
        });
        $this->deleted(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \App\Model\Palete::where('request_week_id', $model->getKey())->delete();
            // SYNC
            \Artisan::call('schedule:run');
        });
    }

    /**
     * @var string
     */
    protected $title = 'Encomendas';

    public function getCreateTitle() {
        return 'Criar uma nova Encomenda';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[3, 'asc']])
                ->withPackage('jquery')
                ->setNewEntryButtonText('Nova Encomenda de Cliente')
                ->addScript('requestweek.js', asset('assets/js/requestweek.js'), ['admin-default'])
        ;

        $display->setApply(function($query) {

            $total = \App\Model\RequestWeek::orderBy('end_at', 'asc')->get();
            $GG = [];
            foreach ($total as $encomenda) {
                $total_week = \App\Model\RequestWeek::where('product_id', $encomenda->product_id)->orderBy('end_at', 'asc')->sum('montagem');
                $total_req = \App\Model\Request::where('product_id', $encomenda->product_id)->first();
                $total_req2 = \App\Model\Request::where('product_id', $encomenda->product_id)->get();
                $total_inc = 0;
                $total_inc2 = 0;
                foreach ($total_req2 as $go) {
                    $dump = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', -1)->sum('value');
                    $dump2 = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                    $total_inc2 = $total_inc2 + $dump2;
                }
                $SEMANAL[$encomenda->product_id]['product_id'] = $encomenda->product_id;
                $SEMANAL[$encomenda->product_id]['name'] = \App\Model\Product::where('id', $encomenda->product_id)->first()->name;
                $SEMANAL[$encomenda->product_id]['total_semanal'] = $total_week;
                $SEMANAL[$encomenda->product_id]['total_montado'] = $total_inc;
                $SEMANAL[$encomenda->product_id]['total_montado2'] = $total_inc2;

                foreach (\App\Model\RequestWeek::where('product_id', $encomenda->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$encomenda->product_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$encomenda->product_id]['semana'][$key]['value'] = $value->montagem;
                }
                foreach ($SEMANAL as $key2 => $value2) {
                    foreach ($value2['semana'] as $key3 => $value3) {
                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                            $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                        }
                        //dd($SEMANAL);
                        if ($SEMANAL[$key2]['semana'][$key3]['id'] == $encomenda->id) {
                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                if ($SEMANAL[$key2]['total_montado2'] < 0) {
                                    $GG['pendentes'][] = $encomenda->id;
                                } else {
                                    $GG['feitas'][] = $encomenda->id;
                                }
                            } else {
                                $GG['falta'][] = $encomenda->id;
                            }
                        }
                    }
                }
            }

            if (!isset($GG['pendentes'])) {
                $GG['pendentes'] = "";
            }
            if (!isset($GG['feitas'])) {
                $GG['feitas'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }
            //dd($SEMANAL);
            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 2) {

                    $query->whereIn('id', $GG['feitas']);
                } else if ($_GET['status'] == 1) {

                    $query->whereIn('id', $GG['pendentes']);
                } else if ($_GET['status'] == 0) {

                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                $query->whereIn('id', $GG['falta']);
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_wood_at')
                    ->setLabel('Data de Término (Carpintaria)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_woodfinishing_at')
                    ->setLabel('Data de Término (Acabamento)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_paint_at')
                    ->setLabel('Data de Término (Pintura)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término (Montagem)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
                    AdminColumn::custom()
                    ->setLabel('Urgente')
                    ->setCallback(function ($instance) {
                                switch ($instance->urgency) {
                                    case 1: return "<i class=\"bg-red fa fa-bell\"></i>";
                                        break;
                                    default: return "-";
                                        break;
                                }
                            }),
                    AdminColumn::datetime('urgency_date')
                    ->setLabel('Data Urgente (Montagem)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'), AdminColumn::text('products.name')->setLabel('Produto'),
            AdminColumn::text('products.points')->setLabel('Pontos')->setWidth('50px'),
            AdminColumn::text('montagem')->setLabel('#')->setWidth('50px'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('50px')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\RequestWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('montagem');
                                $total_week = \App\Model\RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->sum('montagem');
                                $total_req = \App\Model\Request::where('product_id', $instance->product_id)->first();
                                $total_req2 = \App\Model\Request::where('product_id', $instance->product_id)->get();
                                $total_inc = 0;
                                $total_inc2 = 0;
                                foreach ($total_req2 as $go) {
                                    $dump = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', -1)->sum('value');
                                    $dump2 = \App\Model\MountUser::where('request_id', $go->id)->where('published', '>', 1)->sum('value');
                                    $total_inc = $total_inc + $dump;
                                    $total_inc2 = $total_inc2 + $dump2;
                                }
                                $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
                                $SEMANAL[$instance->product_id]['name'] = \App\Model\Product::where('id', $instance->product_id)->first()->name;
                                $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
                                $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;
                                $SEMANAL[$instance->product_id]['total_montado2'] = $total_inc2;
                                foreach (\App\Model\RequestWeek::where('product_id', $instance->product_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                                    $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
                                    $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->montagem;
                                }
                                foreach ($SEMANAL as $key2 => $value2) {
                                    foreach ($value2['semana'] as $key3 => $value3) {
                                        if ($SEMANAL[$key2]['total_montado'] > 0) {
                                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                                        }
                                        if ($SEMANAL[$instance->product_id]['semana'][$key3]['id'] == $instance->id) {
                                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                                if ($SEMANAL[$key2]['total_montado2'] < 0) {
                                                    return "<div class=\"bg-yellow text-center\">OK</div>";
                                                } else {
                                                    return "<div class=\"bg-green text-center\">OK</div>";
                                                }
                                            } else {
                                                $KO = $SEMANAL[$key2]['semana'][$key3]['value'];
                                                return "<div class=\"bg-red text-center\">$KO</div>";
                                            }
                                        }
                                    }
                                }

                                return "ok";
                            }),
            AdminColumn::text('client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Nr. Encomenda')
                    ->setWidth('80px')
                    ->setCallback(function ($instance) {
                                if ($instance->group != 0) {
                                    !($instance->group % 2) ? $color = "red" : $color = "aqua";
                                    return "<div class=\"bg-$color text-center\">$instance->group</div>";
                                } else {
                                    return "-";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::form()
                ->withPackage('jquery')
                ->addScript('requestweek_edit.js', asset('assets/js/requestweek_edit.js'), ['admin-default'])
                ->setItems([
            AdminFormElement::date('end_at', 'Data de Término (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_paint_at', 'Data de Término (Pintura)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_woodfinishing_at', 'Data de Término (Acabamento)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::date('end_wood_at', 'Data de Término (Carpintaria)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::select('product_id', 'Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')->required(),
            AdminFormElement::text('carpintaria', 'Carpintaria')->required(),
            AdminFormElement::text('woodfinishing', 'Acabamento')->required(),
            AdminFormElement::text('pintura', 'Pintura')->required(),
            AdminFormElement::text('montagem', 'Montagem')->required(),
            AdminFormElement::text('client', 'Cliente'),
            AdminFormElement::text('obs', 'Observações'),
            AdminFormElement::number('group', 'Nr. Encomenda')->required(),
            AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
            AdminFormElement::checkbox('urgency', 'É Urgente?'),
            AdminFormElement::date('urgency_date', 'Data Urgente (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
        ]);
        $form->getButtons()
                ->hideSaveAndCreateButton()
//                ->hideSaveButton();
        ;
        return $form;
    }

    public function onCreate() {
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('requestweek.js', asset('assets/js/requestweek.js'), ['admin-default']);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::date('end_at', 'Data de Término (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('end_paint_at', 'Data de Término (Pintura)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('end_woodfinishing_at', 'Data de Término (Acabamento)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('end_wood_at', 'Data de Término (Carpintaria)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->setreadOnly(1)
                            ], 2)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group_quantity', '# de Produtos')->setDefaultValue(1)->required()
                            ], 1)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('product_id', 'Referência de Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::number('quantity', 'Quantidade')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('carpintaria', 'Carpintaria')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('woodfinishing', 'Acabamento')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('pintura', 'Pintura')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('montagem', 'Montagem')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('obs', 'Observações')
                            ], 5)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group', 'Nr. Encomenda')->setDefaultValue(0)->required(),
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('client', 'Cliente'),
                            ], 10)
                    ->addColumn([
                        AdminFormElement::checkbox('urgency', 'É urgente?'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::date('urgency_date', 'Data Urgente (Montagem)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                            ], 2)
        ]);
        $form->addHeader([
            AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
        ]);
//        $form->setButtons(new CustomFormButtons);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Criar Encomenda'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

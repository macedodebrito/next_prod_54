<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
//
use App\Role;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Request extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Pedidos (PP)';

    public function getCreateTitle() {
        return 'Criar Novo Pedido de Produção';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->setNewEntryButtonText('Novo Pedido de Produção')
                ->addScript('request.js', asset('assets/js/request.js'), ['admin-default']);

        $display->setApply(function($query) {
            $total = \App\Model\Request::orderBy('order_at', 'asc')->get();
            $GG = [];
            foreach ($total as $value) {
                $efectuados = \App\Model\RequestUser::where('request_id', $value->id)->where('published', 1)->sum('value');
                if (!count($efectuados)) {
                    $efectuados = 0;
                }
                $missing = $value->value - $efectuados;
                if ($missing < 1) {
                    $GG['feitos'][] = $value->id;
                } else if ($missing > 0) {
                    $GG['falta'][] = $value->id;
                }
            }

            if (!isset($GG['feitos'])) {
                $GG['feitos'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }

            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('order_at', 'asc');
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['feitos']);
                } else if ($_GET['status'] == 0) {
                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                $query->whereIn('id', $GG['falta']);
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('start_at')
                    ->setLabel('Data de Início Autorizado')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::datetime('order_at')
                    ->setLabel('Ordenação por Data')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('pp')->setLabel('Nº do Pedido de Produção'),
            AdminColumn::text('value')->setLabel('Quantidade')->setWidth('100px'),
                    AdminColumn::custom()->setLabel('Por Maquinar')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\Request::where('id', $instance->id)->first();
                                $value = $total['value'];
                                $efectuados = \App\Model\RequestUser::where('request_id', $instance->id)->where('published', 1)->sum('value');
                                if (!count($efectuados)) {
                                    $efectuados = 0;
                                }
                                $missing = $value - $efectuados;

                                if ($missing > 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            }),
                    AdminColumn::custom()->setLabel('Por Montar')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $pintura = \App\Model\PaintUser::where('request_id', $instance->id)->where('published', 1)->sum('value');
                                if (!count($pintura)) {
                                    $pintura = 0;
                                }
                                $efectuados = \App\Model\MountUser::where('request_id', $instance->id)->where('published', '>', -1)->sum('value');
                                if (!count($efectuados)) {
                                    $efectuados = 0;
                                }
                                $missing = $pintura - $efectuados;
                                if ($missing > 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            }),
            AdminColumn::text('products.name')->setLabel('Produto')->append(AdminColumn::filter('product_id')),
            AdminColumn::custom()
            ->setLabel('Pintura')
            ->setCallback(function ($instance) {                          
                return "<span class=\"badge\" style=\"background-color:".\App\Model\ProductCustomNxt::find($instance->custom_id)->hex_color.";color:".\App\Model\ProductCustomNxt::find($instance->custom_id)->hex_color."\">".\App\Model\ProductCustomNxt::find($instance->custom_id)->name."</span>";
            }),
                    AdminColumn::datetime('created_at')
                    ->setLabel('Criado a')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
        ]);

        return $display;
    }

    public function onEdit($id) {

        $selectsInitJs = <<<HEREDOC
<script>
    window.onload = function() {       
        $('#product_id').trigger('select2:select');    
        $(document).ajaxSuccess(function(event, jqXHR, ajaxOptions, data) {
            if (data.selected) {
                $('#custom_id').val(data.selected).trigger('change');
            }
        });
        
    };
</script>
HEREDOC;

        /* 
        return AdminForm::form()->setItems([
                    AdminFormElement::date('start_at', 'Data para Início')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::date('order_at', 'Ordenação por Data')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::text('pp', 'Nº do Pedido de Produção')->required()->unique(),
                    AdminFormElement::text('value', 'Quantidade')->required(),
                    AdminFormElement::select('product_id', 'Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')->required(),
                    AdminFormElement::select('color_id', 'Pintura')->setModelForOptions(new \App\Model\ProductCustomNxt)->setDisplay('name')->required(),
        ]); 
        */
        $form = AdminForm::panel()->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('pp', 'Nº do Pedido de Produção')->required()->unique()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::date('start_at', 'Data para Início')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                        ->required()
                            ], 5)
                    ->addColumn([
                        AdminFormElement::date('order_at', 'Ordenação por Data')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')
                            ], 5)
                    
        ]);

        $form->addBody([
            AdminFormElement::columns()
            ->addColumn([
                AdminFormElement::text('value', 'Quantidade')->required()
            ], 2)
            ->addColumn([
                AdminFormElement::select('product_id', 'Produto')->setModelForOptions(new \App\Model\Product)->setDisplay('name')->required()
            ], 5)
            
            ->addColumn([                    
                AdminFormElement::dependentselect('custom_id', 'Tipo de Pintura')
                    ->setModelForOptions(new \App\Model\ProductCustomNxt)
                    ->setDataDepends(['product_id'])
                    ->setDisplay('name')
                    ->required()
                    ->setLoadOptionsQueryPreparer(function($item, $query) {
                        $checkColors = $query->selectRaw('products_custom_nxt.id as id, products_custom_nxt.name as name, products_custom.product_id as product_id')->join('products_custom', 'products_custom.custom_id', '=', 'products_custom_nxt.id')->where('product_id', $item->getDependValue('product_id'));
                        if ($checkColors->count() != 0) {
                            return $checkColors;
                        } 
                        else {  
                            return \App\Model\ProductCustomNxt::where('products_custom_nxt.id', 1);
                        }
                        
                    //}                         
                }),AdminFormElement::html($selectsInitJs)], 2)
            
        ]);
        $form->getButtons()
                ->setSaveButtonText('Criar Pedido')
                ->hideSaveAndCloseButton()
                ->setCancelButtonText('Cancelar')
                ->hideSaveAndCreateButton();

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

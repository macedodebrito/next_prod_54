<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LocksmithStock extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Entradas da Serralharia em espera no Armazém';

    /**
     * @var string
     */
    protected $alias = "serralharia/stocks";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatablesAsync()
                ->setOrder([[0, 'desc']])
                ->paginate(25)
                ->withPackage('jquery')
                ->addScript('locksmith_stock.js', asset('assets/js/locksmith_stock.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('updated_at', 'desc')->where('published', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('updated_at')
                    ->setLabel('Efectuado em')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Referência')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                        // devia ser PaintUser para confirmar que existe na secção anterior, mas a pickagem passa isso à frente
                                 $request_user = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $instance->locksmith_requests_id)->first();
                                $request_name = \App\Model\LocksmithRequests::where('id', $request_user->locksmith_requests_id)->first();
                                $product_name = \App\Model\Locksmith::where('id', $request_name->locksmith_id)->first();
                                return $product_name->mini_code."".$product_name->code;
                            }),            
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                        // devia ser PaintUser para confirmar que existe na secção anterior, mas a pickagem passa isso à frente
                                $request_user = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $instance->locksmith_requests_id)->first();
                                $request_name = \App\Model\LocksmithRequests::where('id', $request_user->locksmith_requests_id)->first();
                                $product_name = \App\Model\Locksmith::where('id', $request_name->locksmith_id)->first();
                                return $product_name->name;
                            }),
            AdminColumn::custom()
                    ->setLabel('Numero de Série')
                    ->setWidth('130px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {

                                $get_request = \App\Model\LocksmithRequests::where('id', $instance->locksmith_requests_id)->first();
                                $get_product = \App\Model\Locksmith::where('id', $get_request->locksmith_id)->first();

                                $entrance = \App\Model\LocksmithRequestsUser::where('id', $instance->id)->first();

                                return "<div class=\"text-center\">$get_product->code$get_request->pp$instance->serial</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Operador')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                return \App\User::where('id', $instance->user_id)->first()->bigname;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('5%')
                    ->setCallback(function ($instance) {

                                $role_check = 0;
                                $role_id = \App\Role::where('name', 'armazem')->first();
                                $manager_id = \App\Role::where('name', 'manager')->first();
                                $admin_id = \App\Role::where('name', 'admin')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $manager_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $admin_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                switch ($instance->published) {
                                    case 1:
                                        $default_icon = "fa-check fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    case -1:
                                        $default_icon = "fa-user-times fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    default:
                                        $default_icon = "fa-check fa-2x";
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"locksmith_stocks\" href=\"#remove_product\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" class=\"locksmith_stocks\" href=\"#pendente\"><i class=\"fa fa-check fa-2x\"></i></a>";
                                        } else {
                                            return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"></i>";
                                        }
                                        break;
                                }
                            })
        ]);

        return $display;
    }
}
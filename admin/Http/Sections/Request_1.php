<?php

use App\Model\Request;
use App\Model\RequestUser;
use App\Model\Product;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Request::class, function (ModelConfiguration $model) {
    $model->setTitle('Pedidos')->enableAccessCheck();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[1, 'asc']])
                ->withPackage('jquery')
                ->setNewEntryButtonText('Novo Pedido de Produção')
                ->addScript('request.js', asset('assets/js/request.js'), ['admin-default']);

        $display->setApply(function($query) {
            $total = Request::orderBy('order_at', 'asc')->get();
            $GG = [];
            foreach ($total as $value) {
                $efectuados = RequestUser::where('request_id', $value->id)->where('published', 1)->sum('value');
                if (!count($efectuados)) {
                    $efectuados = 0;
                }
                $missing = $value->value - $efectuados;
                if ($missing < 1) {
                    $GG['feitos'][] = $value->id;
                } else if ($missing > 0) {
                    $GG['falta'][] = $value->id;
                }
            }

            if (!isset($GG['feitos'])) {
                $GG['feitos'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }

            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('order_at', 'asc');
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['feitos']);
                } else if ($_GET['status'] == 0) {
                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                $query->whereIn('id', $GG['falta']);
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('start_at')
                    ->setLabel('Data de Início Autorizado')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::datetime('order_at')
                    ->setLabel('Ordenação por Data')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('pp')->setLabel('Nº do Pedido de Produção'),
            AdminColumn::text('value')->setLabel('Quantidade')->setWidth('100px'),
                    AdminColumn::custom()->setLabel('Por Maquinar')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $total = Request::where('id', $instance->id)->first();
                                $value = $total['value'];
                                $efectuados = RequestUser::where('request_id', $instance->id)->where('published', 1)->sum('value');
                                if (!count($efectuados)) {
                                    $efectuados = 0;
                                }
                                $missing = $value - $efectuados;

                                if ($missing > 0) {
                                    $new_color = "bg-red";
                                } else {
                                    $new_color = "bg-success";
                                }
                                return "<div class=\"$new_color text-center\"><b>$missing</b></div>";
                            }),
            AdminColumn::text('products.name')->setLabel('Produto')->append(AdminColumn::filter('product_id')),
                    AdminColumn::datetime('created_at')
                    ->setLabel('Criado a')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
        ]);

        return $display;
    });

    // Create And Edit
    $model->onCreateAndEdit(function() {
        return AdminForm::form()->setItems([
                    AdminFormElement::date('start_at', 'Data para Início')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::date('order_at', 'Ordenação por Data')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s'),
                    AdminFormElement::text('pp', 'Nº do Pedido de Produção')->required()->unique(),
                    AdminFormElement::text('value', 'Quantidade')->required(),
                    AdminFormElement::select('product_id', 'Produto')->setModelForOptions(new Product)->setDisplay('name')->required(),
        ]);
    });
});

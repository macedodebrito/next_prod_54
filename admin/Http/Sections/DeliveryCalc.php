<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\Auth;

// calculator
global $this_week;
$this_week = date('W');

global $this_week_sum;
$this_week_sum = $this_week;

global $total_per_week;
$total_per_week = 2000;

global $points_per_day;
$points_per_day = $total_per_week / 5;

$daynum = date("N", strtotime("wednesday"));

global $max_this_week;
$max_this_week = $total_per_week / $daynum;

global $PONTOS;
$PONTOS = 0;

global $PONTOS_ACUMULADOS;
$PONTOS_ACUMULADOS = 0;

class DeliveryCalc extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    /**
     * @var string
     */
    protected $title = 'Cálculo de Entregas';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(100)
                ->setOrder([[8, 'desc'],[0, 'asc']])
//                ->withPackage('jquery')
//                ->addScript('requestweek.js', asset('assets/js/requestweek.js'), ['admin-default'])
        ;

        $display->setApply(function($query) {
            $query->orderBy('order', 'desc')->orderBy('mount_date', 'asc')->where('temp_amount', '<', 0);
            //$query->orderBy('mount_date', 'asc')->where('temp_amount', '<', 0)->where('calc_amount', '<', 0);
            //$query->orderBy('mount_date', 'asc')->where('temp_amount', '<', 0)->where('calc_amount', '!=', 0);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::datetime('mount_date_simple')
                    ->setLabel('Data de Término (Montagem)')
                    ->setWidth('120px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
            AdminColumn::text('product_name')->setLabel('Produto'),
            AdminColumn::text('order_amount')->setLabel('Encomenda (Total)')->setWidth('50px'),
                    AdminColumn::custom()
                    ->setLabel('Em Falta')
                    ->setWidth('50px')
                    ->setCallback(function ($instance) {
                                global $CALC;

                                if ($instance->calc_amount == 0 && $instance->temp_amount < 0) {
                                    $CALC = $instance->order_amount;
                                    $CSS = "bg-red";
                                } else if ($instance->calc_amount < 0 && $instance->temp_amount < 0) {
                                    $CALC = $instance->order_amount;
                                    $CSS = "bg-red";
                                } else if ($instance->calc_amount > 0 && $instance->temp_amount < 0) {
                                    $CALC = $instance->order_amount - $instance->calc_amount;
                                    $CSS = "bg-yellow";
                                } else {
                                    $CALC = $instance->calc_amount;
                                    $CSS = "bg-green";
                                }
                                return "<div class=\"$CSS text-center\">$CALC</div>";
//                                return "<div class=\"$CSS text-center\">$CALC | $instance->calc_amount ($instance->temp_amount) [$instance->order_amount]</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('50px')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                global $PONTOS;
                                $PONTOS = $CALC * $instance->points;

                                return "<div class=\"text-center\">$PONTOS</div>";
                            }),
            AdminColumn::text('client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Nr. Encomenda')
                    ->setWidth('80px')
                    ->setCallback(function ($instance) {
                                if ($instance->group != 0) {
                                    !($instance->group % 2) ? $color = "red" : $color = "aqua";
                                    return "<div class=\"bg-$color text-center\">$instance->group</div>";
                                } else {
                                    return "-";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('order')->setLabel('Ordem (de )'),
                    AdminColumn::custom()
                    ->setLabel('Entrega Estimada')
                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                global $PONTOS;
                                global $PONTOS_ACUMULADOS;
                                $PONTOS_ACUMULADOS = $PONTOS_ACUMULADOS + $PONTOS;
                                global $points_per_day;
                                global $max_this_week;
                                global $this_week_sum;
                                global $total_per_week;
                                $CSS = "";
                                if ($PONTOS_ACUMULADOS <= $max_this_week) {
                                    $DUMP = "Esta Semana";
                                    $CSS = "bg-green";
                                } else {
                                    $DUMP = $PONTOS_ACUMULADOS / $total_per_week;
                                    $DUMP = $DUMP + $this_week_sum;
                                    $DUMP = round($DUMP);
                                    if ($DUMP > 52) {
                                        $DUMP = $DUMP - 52;
                                        $CSS = "bg-gray";
                                        $DUMP = "Semana $DUMP";
                                    } else {
                                        if ($DUMP - $this_week_sum == 0) {
                                            $CSS = "bg-orange";
                                            $DUMP = "Esta Semana";
                                        } else {
                                            if ($DUMP - $this_week_sum == 1)
                                                $CSS = "bg-yellow";
                                            if ($DUMP - $this_week_sum == 2)
                                                $CSS = "bg-blue";
                                            if ($DUMP - $this_week_sum == 3)
                                                $CSS = "bg-red";
                                            if ($DUMP - $this_week_sum > 3)
                                                $CSS = "bg-black";
                                            $DUMP = "Semana $DUMP";
                                        }
                                    }
                                }
                                return "<div class=\"$CSS text-center\">$DUMP</div>";
                            })
        ]);
        $display->getColumns()->disableControls();
        return $display;
    }

}

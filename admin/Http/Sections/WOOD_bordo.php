<?php

use App\Model\WOOD_bordo;
use App\Model\Product;
use App\Model\Request;
use App\Model\RequestUser;
use App\Model\WoodUser;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(WOOD_bordo::class, function (ModelConfiguration $model) {
    $model->setTitle('WOOD - Bordo')->setAlias('wood/bordo')->disableDeleting();

    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::datatables()->paginate(25);

        $display->setApply(function($query) {
            $total_req = RequestUser::groupBy('request_id')->get();
            $get_ready = [];

            foreach ($total_req as $value) {
                $get_prod = RequestUser::where('request_id', $value->request_id)->get();
                foreach ($get_prod as $value2) {
                    $get_ready[] = $value->request_id;
                }
            }
            $query->whereIn('id', $get_ready)->orderBy('pp', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setColumns([
            AdminColumn::text('pp')->setLabel('PP'),
            AdminColumn::custom()
            ->setLabel('Produto')
            ->setCallback(function ($instance) {
                        $product_name = \App\Model\Product::where('id', $instance->product_id)->first();                               
                        $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->custom_id)->first();
                        $COLOR = "<span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                        return "<div text-center\">$product_name->name</div>$COLOR";
                    }),
            AdminColumn::custom()
                            ->setLabel('Total PP')
                            ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                            ->setCallback(function ($instance) {
                        $product_name = Request::where('id', $instance->id)->first();
                        return $product_name->value;
                    }),                           
                    AdminColumn::custom()
                    ->setLabel('Montados')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $pintura = RequestUser::where('request_id', $instance->id)->sum('value');
                                $montados = WoodUser::where('request_id', $instance->id)->sum('value');

                                //$montados = $montados - $fabricados;
                                return $montados;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Falta PP')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                $total = Request::where('id', $instance->id)->first();
                                $montados = WoodUser::where('request_id', $instance->id)->sum('value');
                                $em_falta = $total->value - $montados;
                                return "<b>$em_falta</b>";
                            })                                    
        ]);

        return $display;
    });
});

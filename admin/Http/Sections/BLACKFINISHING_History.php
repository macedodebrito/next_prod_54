<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
//
use App\Role;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BLACKFINISHING_History extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Acabamento de Preto - Histórico';

    /**
     * @var string
     */
    public function getCreateTitle() {
        return 'Efectuar registo';
    }

    protected $alias = 'blackfinishing/history';

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
                ->setNewEntryButtonText('Efectuar registo')
                ->withPackage('jquery')
                ->addScript('blackfinishing_create.js', asset('assets/js/blackfinishing_create.js'), ['admin-default']);

        $display->setApply(function($query) {
            $query->orderBy('created_at', 'desc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users');

        $display->setFilters([
            AdminDisplayFilter::related('product_id')->setModel(\App\Model\Product::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('created_at')
                    ->setLabel('Efectuado em')
                    ->setWidth('200px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('products.name')->setLabel('Produto')->setWidth('120px'),
            AdminColumn::text('value')->setLabel('Quantidade')->setWidth('120px')->setHtmlAttribute('class', 'bg-info'),
                    AdminColumn::custom()
                    ->setLabel('Operador Carpintaria')
                    ->setWidth('10%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                if ($instance->user_id != 0) {
                                    $valid_id = \App\User::where('id', $instance->user_id)->first();
                                    if (count($valid_id) > 0) {
                                        return \App\User::where('id', $instance->user_id)->first()->bigname;
                                    } else {
                                        return "<div class=\"bg-black\">DESCONHECIDO</div>";
                                    }
                                }
                            }),            
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $product_name = \App\Model\Product::where('id', $instance->product_id)->first();
                                $POINTS = $instance->value * $product_name->points;
                                return $POINTS;
                            }),            
            AdminColumn::text('obs')->setLabel('Observações'),
        ]);
//        $display->setDatatableAttributes(['searching' => false]);
        return $display;
    }

    public function onEdit($id) {
        $num = [];
        while (count($num) <= 100) {
            $num[count($num)] = count($num);
        }
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('blackfinishing_create.js', asset('assets/js/blackfinishing_create.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('product_id', 'Produto')
                        ->setModelForOptions(new \App\Model\Product)
                        ->setDisplay('name')
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('value', 'Produto')
                        ->setOptions($num)
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
            AdminFormElement::text('obs')->setLabel('Observação')
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate($id = null) {
        $num = [];
        while (count($num) <= 100) {
            $num[count($num)] = count($num);
        }
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('blackfinishing_create.js', asset('assets/js/blackfinishing_create.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('product_id', 'Produto')
                        ->setModelForOptions(new \App\Model\Product)
                        ->setDisplay('name')
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('value', 'Quantidade')
                        ->setOptions($num)
                        ->setDefaultValue(0)
                        ->required()
                            ], 2)
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('valid_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
            AdminFormElement::text('obs')->setLabel('Observação')
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

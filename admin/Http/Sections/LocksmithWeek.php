<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use Carbon\Carbon;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\Auth;

class LocksmithWeek extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;
    protected $model;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            unset($model->group_quantity);
            unset($model->locksmith_id_selector);
            unset($model->new_quantity);
            unset($model->observations);
            $model->locksmith_id = -9889;
            $model->quantity = -9889;
//            dd($_REQUEST);          
        });
        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
//            dd($_REQUEST);
            $id = \App\Model\LocksmithWeek::orderBy('id', 'desc')->first()->id;
            \App\Model\LocksmithWeek::where('id', $id)->delete();

            $end_at = new Carbon($_REQUEST['end_at']);

            foreach ($_REQUEST['locksmith_id_selector'] as $key => $value) {
                $last_id = \App\Model\LocksmithWeek::insertGetId(array('id' => null, 'locksmith_id' => $_REQUEST['locksmith_id_selector'][$key], 'user_id' => $_REQUEST['user_id'], 'quantity' => $_REQUEST['new_quantity'][$key], 'client' => $_REQUEST['client'], 'obs' => $_REQUEST['observations'][$key], 'group' => $_REQUEST['group'], 'end_at' => $end_at, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));
                \App\Model\LocksmithPalete::insert(array('id' => null, 'locksmith_week_id' => $last_id));
            }
            // SYNC
            //\Artisan::call('schedule:run');
        });
        $this->deleted(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \App\Model\LocksmithPalete::where('locksmith_week_id', $model->getKey())->delete();
            // SYNC
            //\Artisan::call('schedule:run');
        });
    }

    /**
     * @var string
     */
    protected $title = 'Encomendas';

    public function getCreateTitle() {
        return 'Criar uma nova Encomenda';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->paginate(25)
                ->setOrder([[0, 'asc']])
                ->withPackage('jquery')
                ->setNewEntryButtonText('Nova Encomenda da Serralharia')
                ->addScript('locksmithweek.js', asset('assets/js/locksmithweek.js'), ['admin-default'])
        ;

        $display->setApply(function($query) {

            $total = \App\Model\LocksmithWeek::orderBy('end_at', 'asc')->get();
            $GG = [];
            foreach ($total as $encomenda) {
                $total_week = \App\Model\LocksmithWeek::where('locksmith_id', $encomenda->locksmith_id)->orderBy('end_at', 'asc')->sum('quantity');
                $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $encomenda->locksmith_id)->first();
                $total_req2 = \App\Model\LocksmithRequests::where('locksmith_id', $encomenda->locksmith_id)->get();
                $total_inc = 0;
                $total_inc2 = 0;
                foreach ($total_req2 as $go) {
                    $dump = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', -1)->sum('value');
                    $dump2 = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', 1)->sum('value');
                    $total_inc = $total_inc + $dump;
                    $total_inc2 = $total_inc2 + $dump2;
                }
                $SEMANAL[$encomenda->locksmith_id]['locksmith_id'] = $encomenda->locksmith_id;
                $SEMANAL[$encomenda->locksmith_id]['name'] = \App\Model\Locksmith::where('id', $encomenda->locksmith_id)->first()->name;
                $SEMANAL[$encomenda->locksmith_id]['total_semanal'] = $total_week;
                $SEMANAL[$encomenda->locksmith_id]['total_montado'] = $total_inc;
                $SEMANAL[$encomenda->locksmith_id]['total_montado2'] = $total_inc2;

                //$SEMANAL[$encomenda->product_id]['total_montado2'] = $total_inc2;
                foreach (\App\Model\LocksmithWeek::where('locksmith_id', $encomenda->locksmith_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                    $SEMANAL[$encomenda->locksmith_id]['semana'][$key]['id'] = $value->id;
                    $SEMANAL[$encomenda->locksmith_id]['semana'][$key]['value'] = $value->quantity;
                }
                foreach ($SEMANAL as $key2 => $value2) {
                    foreach ($value2['semana'] as $key3 => $value3) {
                        if ($SEMANAL[$key2]['total_montado'] > 0) {
//                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
//                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
//                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                            }
                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                            $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                        }
                        //dd($SEMANAL);
                        if ($SEMANAL[$key2]['semana'][$key3]['id'] == $encomenda->id) {

                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                if ($SEMANAL[$key2]['total_montado2'] < 0) {
                                    $GG['pendentes'][] = $encomenda->id;
                                } else {
                                    $GG['feitas'][] = $encomenda->id;
                                }
                            } else {
                                $GG['falta'][] = $encomenda->id;
                            }
                        }
                    }
                }
            }
            if (!isset($GG['pendentes'])) {
                $GG['pendentes'] = "";
            }
            if (!isset($GG['feitas'])) {
                $GG['feitas'] = "";
            }
            if (!isset($GG['falta'])) {
                $GG['falta'] = "";
            }
            //dd($SEMANAL);
            //dd($GG);
            if (isset($_GET['status'])) {
                if ($_GET['status'] == "T") {
                    $query->orderBy('end_at', 'asc');
                } else if ($_GET['status'] == 2) {
                    $query->whereIn('id', $GG['feitas']);
                } else if ($_GET['status'] == 1) {
                    $query->whereIn('id', $GG['pendentes']);
                } else if ($_GET['status'] == 0) {

                    $query->whereIn('id', $GG['falta']);
                }
            } else {
                $query->whereIn('id', $GG['falta']);
                //$query->orderBy('end_at', 'asc');
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products');

        $display->setFilters([
            AdminDisplayFilter::related('locksmith_id')->setModel(\App\Model\Locksmith::class)
        ]);

        $display->setColumns([
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término (Serralharia)')
                    ->setWidth('150px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i'),
            AdminColumn::text('products.name')->setLabel('Produto'),
            AdminColumn::text('products.points')->setLabel('Pontuação'),
            AdminColumn::text('quantity')->setLabel('Quantidade'),
                    AdminColumn::custom()
                    ->setLabel('Status Actual')
                    ->setWidth('150px')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\LocksmithWeek::where('id', $instance->id)->orderBy('end_at', 'asc')->sum('quantity');
                                $total_week = \App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->orderBy('end_at', 'asc')->sum('quantity');
                                $total_req = \App\Model\LocksmithRequests::where('locksmith_id', $instance->locksmith_id)->first();
                                $total_req2 = \App\Model\LocksmithRequests::where('locksmith_id', $instance->locksmith_id)->get();
                                $total_inc = 0;
                                $total_inc2 = 0;
                                foreach ($total_req2 as $go) {
                                    $dump = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', -1)->sum('value');
                                    $dump2 = \App\Model\LocksmithRequestsUser::where('locksmith_requests_id', $go->id)->where('published', '>', 1)->sum('value');
                                    $total_inc = $total_inc + $dump;
                                    $total_inc2 = $total_inc2 + $dump2;
                                }
                                $SEMANAL[$instance->locksmith_id]['product_id'] = $instance->locksmith_id;
                                $SEMANAL[$instance->locksmith_id]['name'] = \App\Model\Locksmith::where('id', $instance->locksmith_id)->first()->name;
                                $SEMANAL[$instance->locksmith_id]['total_semanal'] = $total_week;
                                $SEMANAL[$instance->locksmith_id]['total_montado'] = $total_inc;
                                $SEMANAL[$instance->locksmith_id]['total_montado2'] = $total_inc2;
                                foreach (\App\Model\LocksmithWeek::where('locksmith_id', $instance->locksmith_id)->orderBy('end_at', 'asc')->get() as $key => $value) {
                                    $SEMANAL[$instance->locksmith_id]['semana'][$key]['id'] = $value->id;
                                    $SEMANAL[$instance->locksmith_id]['semana'][$key]['value'] = $value->quantity;
                                }
                                foreach ($SEMANAL as $key2 => $value2) {
                                    foreach ($value2['semana'] as $key3 => $value3) {
                                        if ($SEMANAL[$key2]['total_montado'] > 0) {
//                                            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
//                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                                            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
//                                                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                                                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//                                                $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
//                                            }
                                            $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                                            $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
                                            $SEMANAL[$key2]['total_montado2'] = $SEMANAL[$key2]['total_montado2'] - $value3['value'];
                                        }
                                        if ($SEMANAL[$instance->locksmith_id]['semana'][$key3]['id'] == $instance->id) {
                                            if ($SEMANAL[$key2]['semana'][$key3]['value'] <= 0) {
                                                if ($SEMANAL[$key2]['total_montado2'] < 0) {
                                                    return "<div class=\"bg-yellow text-center\">OK</div>";
                                                } else {
                                                    return "<div class=\"bg-green text-center\">OK</div>";
                                                }
                                            } else {
                                                $KO = $SEMANAL[$key2]['semana'][$key3]['value'];
                                                return "<div class=\"bg-red text-center\">$KO</div>";
                                            }
                                        }
                                    }
                                }

                                return "ok";
                            }),
            AdminColumn::text('client')->setLabel('Cliente'),
                    AdminColumn::custom()
                    ->setLabel('Nr. Encomenda')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                if ($instance->group != 0) {
                                    !($instance->group % 2) ? $color = "red" : $color = "aqua";
                                    return "<div class=\"bg-$color text-center\">$instance->group</div>";
                                } else {
                                    return "-";
                                }
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::form()
                ->withPackage('jquery')
                ->addScript('locksmithweek_edit.js', asset('assets/js/locksmithweek_edit.js'), ['admin-default'])
                ->setItems([
            AdminFormElement::date('end_at', 'Data de Término (Serralharia)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required(),
            AdminFormElement::select('locksmith_id', 'Produto')->setModelForOptions(new \App\Model\Locksmith)->setDisplay('name')->required(),
            AdminFormElement::number('quantity', 'Quantidade')->required(),
            AdminFormElement::text('client', 'Cliente'),
            AdminFormElement::text('obs', 'Observações'),
            AdminFormElement::number('group', 'Nr. Encomenda')->required(),
            AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
        ]);
        $form->getButtons()
                ->hideSaveAndCreateButton()
//                ->hideSaveButton();
        ;
        return $form;
    }

    public function onCreate() {
        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('locksmithweek.js', asset('assets/js/locksmithweek.js'), ['admin-default']);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::date('end_at', 'Data de Término (Serralharia)')->setPickerFormat('d.m.Y H:i')->setFormat('Y-m-d H:i:s')->required()
                            ], 2)]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group_quantity', '# de Produtos')->setDefaultValue(1)->required()
                            ], 1)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('locksmith_id', 'Referência de Produto')->setModelForOptions(new \App\Model\Locksmith)->setDisplay('name')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::number('quantity', 'Quantidade')
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('obs', 'Observações')
                            ], 9)
        ]);
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::number('group', 'Nr. Encomenda')->setDefaultValue(0)->required(),
                            ], 1)
                    ->addColumn([
                        AdminFormElement::text('client', 'Cliente'),
                            ], 11)
        ]);
        $form->addHeader([
            AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)->required(),
        ]);
//        $form->setButtons(new CustomFormButtons);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Criar Encomenda'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

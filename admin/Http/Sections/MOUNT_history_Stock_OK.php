<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MOUNT_history_Stock_OK extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Listagem de Stock';

    /**
     * @var string
     */
    protected $alias = "stocks/ok";

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatablesAsync()
                ->setOrder([[0, 'desc']])
                ->paginate(25)
                ->withPackage('jquery')
                ->addScript('stock.js', asset('assets/js/stock.js'), ['admin-default']);

        $display->getColumns()->getControlColumn()->setDeletable(false);

        $display->setApply(function($query) {
            $query->orderBy('published', 'asc', 'updated_at', 'desc')->where('published', 1)->orWhere('published', 2);
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setColumns([
                    AdminColumn::datetime('updated_at')
                    ->setLabel('Efectuado em')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()
                    ->setLabel('Referência')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                // devia ser PaintUser para confirmar que existe na secção anterior, mas a pickagem passa isso à frente
                                $request_user = \App\Model\MountUser::where('request_id', $instance->request_id)->first();
                                $request_name = \App\Model\Request::where('id', $request_user->request_id)->first();
                                $product_name = \App\Model\Product::where('id', $request_name->product_id)->first();
                                return $product_name->mini_code . "" . $product_name->code;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $request_user = \App\Model\MountUser::where('request_id', $instance->request_id)->first();
                                $request_name = \App\Model\Request::where('id', $request_user->request_id)->first();
                                $product_name = \App\Model\Product::where('id', $request_name->product_id)->first();
                                return $product_name->name;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Numero de Série')
                    ->setWidth('130px')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {

                                $get_request = \App\Model\Request::where('id', $instance->request_id)->first();
                                $get_product = \App\Model\Product::where('id', $get_request->product_id)->first();

                                $entrance = \App\Model\MountUser::where('id', $instance->id)->first();
                                global $SERIAL;
                                $SERIAL = $get_product->code . "" . $get_request->pp . "" . $instance->serial;
                                return "<div class=\"text-center\">$get_product->code$get_request->pp$instance->serial</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Validado por')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                return \App\User::where('id', $instance->valid_id)->first()->bigname;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Observações')
                    ->setWidth('100px')->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $CUSTOM = "";

                                global $SERIAL;
                                $SQL = DB::table('nxt_to_phc')->where('serial', $SERIAL)->first();

                                $cor_madeira = $SQL->color_wood;
                                $cor_metal = $SQL->color_steel;
                                $custom = $SQL->custom;

                                if ($cor_madeira) {
                                    $CUSTOM = "(<b>Madeira:</b> $cor_madeira) ";
                                }
                                if ($cor_metal) {
                                    $CUSTOM .= "(<b>Metal:</b> $cor_metal) ";
                                }
                                if ($custom) {
                                    $CUSTOM .= "(<b>Customização:</b> $custom) ";
                                }
                                return "<div class=\"text-center\">$instance->obs<br>$CUSTOM</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('5%')
                    ->setCallback(function ($instance) {

                                $role_check = 0;
                                $role_id = \App\Role::where('name', 'stock')->first();
                                $manager_id = \App\Role::where('name', 'manager')->first();
                                $admin_id = \App\Role::where('name', 'admin')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $manager_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                } else if (count(DB::table('role_user')->where('role_id', $admin_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                switch ($instance->published) {
                                    case 1:
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"stocks\" href=\"#back\"><i class=\"fa fa-remove fa-2x\"></i></a> <a id=\"$instance->id\" class=\"stocks\" href=\"#sell\"><i class=\"fa fa-check fa-2x\"></i></a>";
                                        } else {
                                            return "<i class=\"fa fa-circle-o-notch fa-spin fa-2x\"></i>";
                                        }
                                        break;
                                    default:
                                        return "<i class=\"fa fa-check fa-2x\"></i>";
                                        break;
                                }
                            })
        ]);

        return $display;
    }

    public function onEdit($id) {

        $form = AdminForm::panel();

        $form->addHeader([
            AdminFormElement::text('obs')->setLabel('Observação')
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Editar Observação'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

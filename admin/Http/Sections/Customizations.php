<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Model\CustomFormButtons;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Customizations extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Customizações';

    public function getCreateTitle() {
        return 'Criar Nova Customização';
    }

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        $display = AdminDisplay::datatables()
                ->setNewEntryButtonText('Adicionar Nova Customização')
                ->paginate(25);
        $display->setApply(function($query) {
            $query->orderBy('name', 'asc');
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::text('name')->setLabel('Nome')->setWidth('150px'),
            AdminColumn::text('value')->setLabel('Valor')->setWidth('150px'),
                    AdminColumn::custom()
                    ->setLabel('Montagem')
                    ->setCallback(function ($instance) {
                                switch ($instance->montagem) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Serralharia')
                    ->setCallback(function ($instance) {
                                switch ($instance->serralharia) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Técnica')
                    ->setCallback(function ($instance) {
                                switch ($instance->tecnica) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Cor Madeira')
                    ->setCallback(function ($instance) {
                                switch ($instance->color_wood) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Cor Metal')
                    ->setCallback(function ($instance) {
                                switch ($instance->color_steel) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
                    AdminColumn::custom()
                    ->setLabel('Customização')
                    ->setCallback(function ($instance) {
                                switch ($instance->custom) {
                                    case 1: return "<i class=\"fa bg-green fa-check\"></i>";
                                        break;
                                    default: return "<i class=\"fa fa-times\"></i>";
                                        break;
                                }
                            }),
        ]);

        return $display;
    }

    public function onEdit($id) {
        $form = AdminForm::panel();
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('name')->setLabel('Nome'),
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('value')->setLabel('Valor')->required(),
                            ], 3)
                    ->addColumn([
                        AdminFormElement::checkbox('montagem')->setLabel('Montagem'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('serralharia')->setLabel('Serralharia'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('tecnica')->setLabel('Técnica'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('color_wood')->setLabel('Cor Madeira'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('color_steel')->setLabel('Cor Metal'),
                            ], 12)
                    ->addColumn([
                        AdminFormElement::checkbox('custom')->setLabel('Customização'),
                            ], 12)
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Gravar Customização'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

    public function onCreate() {
        return $this->onEdit(null);
    }

}

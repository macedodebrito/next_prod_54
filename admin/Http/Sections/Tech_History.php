<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Delete;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
//
use App\Role;
//
use Carbon\Carbon;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Tech_History extends Section implements Initializable {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    public function initialize() {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            // INTEGRAÇÃO NXT2PHC
            unset($model->n2p_custom);
            unset($model->internal_serial);
        });

        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            $id = \App\Model\TechRequestsUser::orderBy('id', 'desc')->first()->id;
            \App\Model\TechRequestsUser::where('id', $id)->delete();
            foreach ($_REQUEST['serials'] as $key => $value) {
                //$ldate = date('Y-m-d H:i:s');
                \App\Model\TechRequestsUser::insert(array('id' => null, 'tech_requests_id' => $_REQUEST['tech_requests_id'], 'user_id' => $_REQUEST['user_id'], 'valid_id' => 0, 'value' => 1, 'serial' => $value, 'obs' => $_REQUEST['observations'][$key], 'last_serial' => $_REQUEST['last_serial'], 'published' => 1, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString()));

                $object_request = \App\Model\TechRequests::where('id', $_REQUEST['tech_requests_id'])->first();

                // INTEGRAÇÃO NXT2PHC
                $N2P_ref = \App\Model\Tech::where('id', $object_request->tech_id)->first()->mini_code . "" . \App\Model\Tech::where('id', $object_request->tech_id)->first()->code;
                $N2P_designation = \App\Model\TechStatusRequests::where('mini_code', \App\Model\Tech::where('id', $object_request->tech_id)->first()->mini_code)->where('code', \App\Model\Tech::where('id', $object_request->tech_id)->first()->code)->first()->designation;
                $N2P_quantity = 1;
                $N2P_serial = \App\Model\Tech::where('id', $object_request->tech_id)->first()->code . "" . \App\Model\TechRequests::where('id', $_REQUEST['tech_requests_id'])->first()->pp . "" . $value;
                $N2P_wood = "";
                $N2P_steel = "";
                $N2P_custom = $_REQUEST['n2p_custom'];
                if (isset($_REQUEST['internal_serial'])) {
                    $N2P_internal_serial = $_REQUEST['internal_serial'];
                }
                else {
                    $N2P_internal_serial = "";
                }                
                $N2P_supplier = "ANTONIO FERNANDO C. MARTINS CORREIA";
                $N2P_armazem = 1;
                DB::table('nxt_to_phc')->insert(array('id' => null, 'ref' => $N2P_ref, 'designation' => $N2P_designation, 'quantity' => $N2P_quantity, 'serial' => $N2P_serial, 'color_wood' => $N2P_wood, 'color_steel' => $N2P_steel, 'custom' => $N2P_custom, 'supplier' => $N2P_supplier, 'armazem' => $N2P_armazem, 'internal_serial' => $N2P_internal_serial));
            }
        });
    }

    /**
     * @var string
     */
    protected $title = 'Técnica - Histórico';

    /**
     * @var string
     */
    public function getCreateTitle() {
        return 'A enviar para o Armazém';
    }

    protected $setcreatetitle = 'Histórico da Técnica';
//    public function setCreateTitle('A enviar para a Carpintaria')
//    {
//        return 'A enviar para a Carpintaria';
//    }

    protected $alias = 'tecnica/history';

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {
        // Display

        $display = AdminDisplay::datatablesAsync()
                ->paginate(25)
                ->setOrder([[0, 'desc']])
                ->setNewEntryButtonText('Enviar para o Armazém')
                ->withPackage('jquery')
                ->addScript('tech_create.js', asset('assets/js/tech_create.js'), ['admin-default'])
        ;

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->with('products', 'users', 'request_pp');

        $display->setApply(function($query) {
            $query->orderBy('created_at', 'desc');
        });

        $display->setColumns([
                    AdminColumn::datetime('created_at', 'Efectuado em')->setWidth('100px')->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y H:i:s'),
                    AdminColumn::custom()->setLabel('Nº Pedido')->setCallback(function ($instance) {
                                $new_color = "";
                                $pp = \App\Model\TechRequests::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$pp->pp</div>";
                            })
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'bg-gray text-center'),
                    AdminColumn::custom()
                    ->setLabel('Peça')
                    ->setWidth('100px')
                    ->setCallback(function ($instance) {
                                $product_name = \App\Model\Tech::where('id', $instance->request_pp['tech_id'])->first();
                                $new_color = "";
                                $pp = \App\Model\TechRequests::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$product_name->name</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Quantidade')
                    ->setWidth('100px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                $total = \App\Model\TechRequests::where('id', $instance->tech_requests_id)->first();
                                $efectuados = \App\Model\TechRequestsUser::where('tech_requests_id', $instance->tech_request_id)->sum('value');

                                $missing = $total->value - $efectuados;

                                if ($missing < 0) {
                                    $new_color = "bg-red";
                                } else if ($missing == 0) {
                                    $new_color = "bg-success";
                                } else {
                                    $new_color = "bg-success";
                                }

                                $new_color = "";
                                $pp = \App\Model\TechRequests::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }

                                return "<div class=\"$new_color text-center\">$instance->value</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Numero de Série')
                    ->setWidth('30%')->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                $new_color = "";
                                $get_request = \App\Model\TechRequests::where('id', $instance->tech_requests_id)->first();
                                $get_product = \App\Model\Tech::where('id', $get_request->tech_id)->first();

                                $entrance = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($entrance->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$get_product->code$get_request->pp$instance->serial</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Operador')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'bg-warning text-center')
                    ->setCallback(function ($instance) {
                                $new_color = "";
                                $pp = \App\Model\TechRequests::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                $go = \App\User::where('id', $instance->user_id)->first()->bigname;
                                return "<div class=\"$new_color text-center\">$go</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Observações')
                    ->setCallback(function ($instance) {
                                $new_color = "";
                                $pp = \App\Model\TechRequests::where('id', $instance->request_pp['id'])->first();
                                $dump = \App\Model\TechRequestsUser::where('id', $instance->id)->first();
                                if ($dump->published < 0) {
                                    $new_color = "bg-red";
                                }
                                return "<div class=\"$new_color text-center\">$instance->obs</div>";
                            }),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('5%')
                    ->setCallback(function ($instance) {
                                // warning 
                                // check = 1
                                // remove
                                // user-times = -1
                                $default_html_tag = "#pendente";
                                $default_icon = "fa-circle-o-notch fa-spin fa-2x";
                                $role_check = 0;
                                $role_id = Role::where('name', 'serralharia')->first();
                                if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
                                    $role_check = 1;
                                }
                                switch ($instance->published) {
                                    case 1:
                                        $default_icon = "fa-check fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    case 2:
                                        $default_icon = "fa-check fa-2x";
                                        return "<i class=\"fa $default_icon\"></i>";
                                        break;
                                    case -1:
                                        $default_icon = "fa-user-times fa-2x";
                                        $default_html_tag = "#removing";
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"$default_html_tag\"><i class=\"fa $default_icon\"></i></a>";
                                        } else {
                                            return "<i class=\"fa fa-user-times fa-2x\"></i>";
                                        }
                                        break;
                                    default:
                                        if ($role_check > 0) {
                                            return "<a id=\"$instance->id\" class=\"accept_product\" href=\"#removing_fast\"><i class=\"fa fa-user-times fa-2x\"></i></a> <i class=\"fa $default_icon\"></i>";
                                        } else {
                                            return "<a href=" . url('/admin/serralharia/open_mount') . "><i class=\"fa $default_icon\"></i></a>";
                                        }
                                        break;
                                }
                            })
        ]);

//        $display->setColumnFilters([
////            AdminColumnFilter::date()->setPlaceholder('por Data')->setFormat('d.m.Y')->setOperator('contains'),
//            null,
//            AdminColumnFilter::text()->setPlaceholder('por PP')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Produto')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Quantidade')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Operador')->setOperator('contains'),
//            AdminColumnFilter::text()->setPlaceholder('por Obs')->setOperator('contains'),
//            null
//        ]);
//        $display->getColumnFilters()->setPlacement('before.panel');
//        $display->setDatatableAttributes(['searching' => true]);

        return $display;
    }

    public function onEdit($id) {

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('tech_edit.js', asset('assets/js/tech_edit.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('serial')
                        ->setLabel('Serial Number')
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('obs')->setLabel('Observação')
                    ]),
            AdminFormElement::hidden('tech_requests_id')
        ]);

        $form->getButtons()
                ->setSaveButtonText('Editar')
                ->hideSaveAndCloseButton()
                ->setCancelButtonText('Cancelar')
                ->hideSaveAndCreateButton();

        return $form;
    }

    public function onCreate() {
        $get_pp = \App\Model\TechRequests::orderBy('created_at', 'asc')->groupBy('id')->get();
        $new_request_pp = [];
        foreach ($get_pp as $key => $value) {
            $get_request = \App\Model\TechRequests::where('id', $value->id)->first();
            $get_product = \App\Model\Tech::where('id', $get_request->tech_id)->first();

            $total = \App\Model\TechRequests::where('id', $value->id)->sum('value');
            $permitido = \App\Model\TechRequestsUser::where('tech_requests_id', $value->id)->sum('value');
            $check = $total - $permitido;
            $new_request_pp[0] = "Escolha um Pedido para Produção";
            if ($check > 0) {
                $new_request_pp[$value->id] = "PP Nº: $get_request->pp » ($get_product->name) | SERIAL: $get_product->code $get_request->pp XXX";
            }
        };

        $form = AdminForm::panel()
                ->withPackage('jquery')
                ->addScript('tech_create.js', asset('assets/js/tech_create.js'), ['admin-default']);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::password('published')->setLabel('Insira o seu Código')
                            ], 2)
                    ->addColumn([
                        AdminFormElement::select('tech_requests_id', 'Pedido de Produção')
                        ->setOptions($new_request_pp)
                        ->setDefaultValue(0)
                        ->required()
                            ], 4)
                    ->addColumn([
                        AdminFormElement::select('value', 'Quantidade')
                        ->required()
                            ], 2)
        ]);

        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('serial')
                        ->setLabel('Serial Number')
                        ->setDefaultValue("XXX")
                        ->required()
                            ], 3)
                    ->addColumn([
                        AdminFormElement::text('obs')->setLabel('Observação')
                    ])
        ]);
        $SELECT = [];
        $SELECT[''] = '-';
        $SQL = \App\Model\Customizations::where("tecnica", 1)->orderBy('name', 'asc')->get();
        foreach ($SQL as $item) {
            $SELECT["$item->value"] = $item->name;
        }
        $form->addHeader([
                    AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('n2p_custom', 'Customização')->setOptions($SELECT)->setDefaultValue("")
                            ], 2)
                    ->addColumn([
                        AdminFormElement::text('internal_serial')->setLabel('Serial Incorporado')
                            ], 2)
        ]);

        $form->addBody([
                    AdminFormElement::hidden('user_id')->setDefaultValue(Auth::user()->id)
                    ->required(),
                    AdminFormElement::hidden('last_serial')->setDefaultValue(0)
                    ->required()
        ]);

        $form->getButtons()->replaceButtons([
            'delete' => null,
            'save' => (new SaveAndClose())->setText('Dar saída dos produtos para o Armazém'),
            'save_and_close' => null,
            'save_and_create' => null,
            'cancel' => (new Cancel())->setText('Cancelar'),
        ]);

        return $form;
    }

}

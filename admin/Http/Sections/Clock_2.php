<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
//
use App\Model\UserLogin;
use Carbon\Carbon;
use DateTime;
//
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

class Clock_2 extends Section {

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Relógio de Ponto';

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay() {

//        if (isset($_GET['refresh'])) {
//            $refresh = \App\Model\UserLogin_2::where('user_id', $_GET['refresh'])->orderBy('created_at', 'asc')->get();
//
//            foreach ($refresh as $clock) {
//                $on_off = $clock->login_logout;
//                $hora = Carbon::createFromFormat('Y-m-d H:i:s', $clock->created_at)->format('H:i:s');
////                $check = DateTime::createFromFormat('Y-m-d H:i:s', "2017-01-01 08:30:00")->format('H:i:s');
////                $totalDuration = $hora->diff($check)->format('%H:%I:%S');
////                print_r($totalDuration."<br/>");
//                if ((date("H") <= "08") && (date("i") <="00") && (date("s") <="00")) {
//                    print_r("SIM $hora</br>");
//                }
//                else {
//                    print_r("NAO $hora</br>");
//                }
//            }
//        }

        $display = AdminDisplay::datatables()->paginate(25);
        $display->getColumns()->getControlColumn()->setDeletable(false);
        $display->setApply(function($query) {

            if (isset($_GET['id'])) {
                $query->orderBy('bigname', 'asc')->where('company_id', $_GET['id'])->get();
            } else {
                $query->orderBy('bigname', 'asc')->get();
            }
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
                    AdminColumn::custom()
                    ->setLabel('Funcionário')
                    ->setCallback(function ($instance) {
                                return "<a href=\"clock_users_2?id=$instance->id\">$instance->bigname</a>";
                            }),
            AdminColumn::text('companies.name')->setLabel('Empresa'),
                    AdminColumn::custom()
                    ->setLabel('Status')
                    ->setWidth('200px')
                    ->setCallback(function ($instance) {
                                $dump = \App\Model\UserLogin_2::where('user_id', $instance->id)->orderBy('created_at', 'desc')->first();
                                if (count($dump) > 0) {
                                    if ($dump->login_logout == "E") {
                                        $new_color = "bg-green";
                                        $new_info = "ACTIVO";
                                    } else if ($dump->login_logout == "S") {
                                        $new_color = "bg-red";
                                        $new_info = "INACTIVO";
                                    }
                                } else {
                                    $new_color = "bg-black";
                                    $new_info = "DESCONHECIDO";
                                }
                                return "<div class=\"$new_color text-center\">$new_info</div>";
                            }),
//                    AdminColumn::custom()
//                    ->setLabel('Hora de Registo')
//                    ->setWidth('200px')
//                    ->setCallback(function ($instance) {
//                                $dump = \App\Model\UserLogin_2::where('user_id', $instance->id)->orderBy('created_at', 'desc')->first();
//                                if (count($dump) > 0) {
//                                    $new_info = Carbon::createFromFormat('Y-m-d H:i:s', $dump->created_at)->format('d-m-Y H:i');
//                                } else {
//                                    $new_info = "SEM REGISTOS";
//                                }
//                                return "<div class=\"text-center\">$new_info</div>";
//                            })
        ]);

        return $display;
    }

}

<?php

use App\Model\MOUNT_semanal_Order;
use App\Model\RequestWeek;
use App\Model\MountUser;
use App\Model\PaintUser;
use App\Model\Stock;
use App\Model\Request;
use App\Model\Product;
use App\User;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(MOUNT_semanal_Order::class, function (ModelConfiguration $model) {
    $model->setTitle('Lista de Encomendas | Montagem')->setAlias('mount/weekorder')->disableDeleting();

    // Display
    $model->onDisplay(function () {

        if (\Request::path() == "admin/triple_weeks") {
            $more_css = "hidden";
        } else {
            $more_css = "";
        }
        $display = AdminDisplay::datatables()->setOrder([[10, 'desc'], [1, 'asc']])->paginate(100);
        $display->setApply(function($query) {
            $query
                    //->orderBy('sum_orders.order', 'desc')
                    ->orderBy('sum_orders.mount_date', 'asc')
                    ->select('products_custom_nxt.name_cor as color_name', 'request_week.*', 'sum_orders.points as points', 'sum_orders.mount_date as mount_date', 'sum_orders.order_id AS order_id', 'sum_orders.id AS oid', 'sum_orders.order_amount', 'sum_orders.mount_amount', 'sum_orders.temp_amount', 'sum_orders.calc_amount', 'sum_orders.order as sum_order')
                    ->join('sum_orders', 'request_week.id', '=', 'sum_orders.order_id')
                    ->join('products_custom_nxt', 'request_week.custom_id', '=', 'products_custom_nxt.id')
                    ->where('sum_orders.temp_amount', '<', 0)
            ;
        });

        $display->setHtmlAttribute('class', 'table-info table-hover');

        $display->setColumns([
            AdminColumn::custom()->setLabel('ESTADO')->setCallback(function ($instance) {

                        $curdate = strtotime(date('Y-m-d'));
                        $mydate = strtotime(date("Y-m-d", strtotime($instance->end_at)));

                        $CSS = "";
                        if ($instance->urgency == 0) {
                            $urgency_date = "";
                            $datework = \Carbon\Carbon::parse($instance->end_at);
                        } else {
                            $urgency_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $instance->urgency_date)->format('d.m');
                            $datework = \Carbon\Carbon::parse($instance->urgency_date);
                        }

                        $now = \Carbon\Carbon::parse('now');
                        $delay_days = $now->diffInDays($datework, false);
                        $DAYS_DATE = " <span>($delay_days)</span>";
                        if ($delay_days >= 0) {
                            if ($delay_days <= 7) {
                                if ($delay_days != 0) {
                                    $CSS = "bg-yellow";
                                    $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                } else {
                                    if ($curdate > $mydate) {
                                        $CSS = "bg-danger";
                                        $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                                        $DAYS_DATE = "(-1)";
                                    } else {
                                        $CSS = "bg-yellow";
                                        $TYPE = "<i class='fa fa-clock-o'></i> POSSIVEL ATRASO";
                                    }
                                }
                            } else {
                                $CSS = "bg-green";
                                $TYPE = "<i class='fa fa-check-circle'></i> EM PRODUÇÃO";
                            }
                        } else {
                            $CSS = "bg-danger";
                            $TYPE = "<i class='fa fa-exclamation-circle'></i> ATRASO";
                        }

                        if ($instance->urgency == 1) {
                            $status_date = "<div class='bg-red text-center'><i class='fa fa-bell'></i> URGENTE<br>$urgency_date $DAYS_DATE</div>";
                        } else {
                            if ($curdate > $mydate) {
                                $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                            } else if ($curdate <= $mydate) {
                                if ($curdate == $mydate) {
                                    $status_date = "<div class='$CSS text-center'>POSSIVEL ATRASO</div>";
                                } else {
                                    $status_date = "<div class='$CSS text-center'>$TYPE $DAYS_DATE</div>";
                                }
                            }
                        }

                        return $status_date;
                    }),
                    AdminColumn::datetime('end_at')
                    ->setLabel('Data de Término')
                    ->setWidth('10%')
                    ->setHtmlAttribute('class', 'text-center')
                    ->setFormat('d.m.Y'),
                    AdminColumn::custom()
                    ->setLabel('Produto')
                    ->setCallback(function ($instance) {
                                global $CALC;

                                if ($instance->temp_amount <= 0) {
                                    if ($instance->calc_amount < 0 && $instance->temp_amount < 0) {
                                        $CALC = $instance->order_amount;
                                    } else if ($instance->calc_amount > 0 && $instance->temp_amount < 0) {
                                        $CALC = $instance->order_amount - $instance->calc_amount;
                                    } else {
                                        $CALC = $instance->order_amount - $instance->calc_amount;
                                    }
                                } else if ($instance->temp_amount > 0) {
                                    $CALC = "OK";
                                } else {
                                    $CALC = "??";
                                }
                                $x_dump = \App\Model\Sum_Mount::select(\DB::raw('SUM(pintura) as pintura, SUM(montagem) as montagem, sum_mount.name, img_1, img_2, caption_1, caption_2, caption_3'))
                                        ->where('product_id', $instance->product_id)
                                        ->where('color_name', $instance->color_name)
                                        ->join('products', 'products.id', '=', 'sum_mount.product_id')
                                        ->groupBy('product_id')
                                        ->groupBy('color_name')
                                        ->first();
                                $x_calc = $x_dump->pintura - $x_dump->montagem;

                                if (!isset($GLOBALS[$instance->product_id][$instance->custom_id]['f'])) {
                                    $GLOBALS[$instance->product_id][$instance->custom_id]['f'] = $x_calc;
                                }

                                $GLOBALS[$instance->product_id][$instance->custom_id]['f2'] = $GLOBALS[$instance->product_id][$instance->custom_id]['f'] - $CALC;

                                $F = $GLOBALS[$instance->product_id][$instance->custom_id]['f'];
                                $F2 = $GLOBALS[$instance->product_id][$instance->custom_id]['f2'];

                                $GLOBALS[$instance->product_id][$instance->custom_id]['f'] = $GLOBALS[$instance->product_id][$instance->custom_id]['f2'];

                                if ($F > 0 && $F2 < 0) {
                                    $CSS = "<i style=\"color: orange;\" class=\"fa fa-circle fa-2x\" title=\"$F / $F2\"></i>";
                                } else if ($F <= 0 && $F2 < 0) {
                                    $CSS = "<i style=\"color: red;\" class=\"fa fa-circle fa-2x\" title=\"$F / $F2\"></i>";
                                } else {
                                    $CSS = "<i style=\"color: green;\" class=\"fa fa-circle fa-2x\" title=\"$F / $F2\"></i>";
                                }

                                $IMG = "";
                                $IMG_1 = "";
                                $IMG_2 = "";
                                $IMG_3 = "";
                                $CAPTION_1 = "";
                                $CAPTION_2 = "";
                                $CAPTION_3 = "";

                                if ($x_dump->img_1) {
                                    if ($x_dump->caption_1) {
                                        $CAPTION_1 = $x_dump->caption_1;
                                    }
                                    $IMG_1 = "<div><span><img src=\"../$x_dump->img_1\"></span><i>$CAPTION_1</i></div>";
                                }
                                if ($x_dump->img_2) {
                                    if ($x_dump->caption_2) {
                                        $CAPTION_2 = $x_dump->caption_2;
                                    }
                                    $IMG_2 = "<div><span><img src=\"../$x_dump->img_2\"></span><i>$CAPTION_2</i></div>";
                                }
                                if ($x_dump->img_3) {
                                    if ($x_dump->caption_3) {
                                        $CAPTION_3 = $x_dump->caption_3;
                                    }
                                    $IMG_3 = "<div><span><img src=\"../$x_dump->img_3\"></span><i>$CAPTION_3</i></div>";
                                }

                                if (($x_dump->img_1) || ($x_dump->img_2) || ($x_dump->img_3)) {
                                    $IMG = " <i class=\"watch_product_img fa fa-picture-o fa-2x\"></i><div>$IMG_1 $IMG_2 $IMG_3</div>";
                                }

                                global $POINTS;
                                $POINTS = $instance->points;
                                $SQL_COLOR = \App\Model\ProductCustomNxt::where('id', $instance->custom_id)->first();
                                $COLOR = "<br/><span class=\"badge\" style=\"color:".$SQL_COLOR->hex_color.";background-color:".$SQL_COLOR->hex_color_bg."\">".$SQL_COLOR->name."</span>";
                                return "$CSS " . $x_dump->name . $IMG . $COLOR;
                            }),
                    AdminColumn::custom()
                    ->setLabel('#')
                    ->setWidth('5%')->setHtmlAttribute('class', 'bg-danger text-center')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                return $CALC;
                            }),
                    AdminColumn::custom()
                    ->setLabel('Pontos')
                    ->setWidth('80px')
                    ->setHtmlAttribute('class', 'bg-success text-center')
                    ->setCallback(function ($instance) {
                                global $CALC;
                                global $POINTS;
                                $POINTS = $CALC * $POINTS;
                                return $POINTS;
                            }),
            AdminColumn::text('obs')->setLabel('Observações'),
            AdminColumn::text('client')->setLabel('Cliente'),
            $header_palete_store = AdminColumn::custom()
            ->setLabel('Palete<br/>Separada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                GLOBAL $palete;
                $palete = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                GLOBAL $check;
                $check = 1;
                if (count($palete) > 0) {
                    if ($palete->status != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        // verificar se falta embalagem e redes
                        $product = Product::where('id', $instance->product_id)->first();
                        if (($product->store == 1) && ($palete->store_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->tech == 1) && ($palete->tech_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->serigrafia == 1) && ($palete->serigrafia_auth_id == 0)) {
                            $check = 0;
                        }
                        if (($product->locksmith == 1) && ($palete->locksmith_auth_id == 0)) {
                            $check = 0;
                        }
                        if ($check == 1) {
                            return "<i style=\"color: orange;\" class=\"fa fa-check fa-2x\"></i>";
                        } else {
                            // se status = 0, está em falta
                            return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                        }
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_auth = AdminColumn::custom()
            ->setLabel('Palete<br/>Autorizada')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                $palete = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                if (count($palete) > 0) {
                    if ($palete->auth_id != 0) {
                        return "<i style=\"color: #00a65a;\" class=\"fa fa-check fa-2x\"></i>";
                    } else {
                        return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                    }
                } else {
                    return "<i style=\"color: #FF0000;\" class=\"fa fa-times fa-2x\"></i>";
                }
            }),
            $header_palete_ask = AdminColumn::custom()
            ->setLabel('Pedir Palete')
            ->setWidth('80px')
            ->setHtmlAttribute('class', "$more_css")
            ->setCallback(function ($instance) {
                GLOBAL $check;
                GLOBAL $palete;
                $PALETE = \App\Model\Palete::where('request_week_id', $instance->id)->first();
                if ($PALETE) {
                    if ($PALETE->phc_id) {
                        $PALETE_ID = "NSP " . $PALETE->phc_id;
                    } else {
                        $PALETE_ID = "NXT " . $PALETE->request_week_id;
                    }
                    $ID = $PALETE->id;
                } else {
                    $PALETE = "LOL";
                }
                $notification = \App\Model\Notifications::where('object_id', $ID)->where('type', 1)->first();
                //if ($check == 0 && count($palete) > 0 && $palete->auth_id == 0 && count($notification) == 0) {
//                if ((count($palete) > 0) && ($palete->status != 0) && ($check == 0) && ($palete->auth_id == 0) && (count($notification) == 0)) {
                if ((count($palete) > 0) && ($palete->status != 0) && ($palete->auth_id == 0) && (count($notification) == 0)) {
                    //if ($check == 0 && count($palete) > 0 && $palete->auth_id == 0 && count($notification) == 0) {
                    //if ($check == 0 && $palete->auth_id == 0 && count($notification) == 0) {
                    return "<i id=\"request_palete_bell_$ID\" data-palete=\"$PALETE_ID\" data-rel=\"$ID\" style=\"color: #000000;\" class=\"fa fa-bell-o fa-2x request_palete_bell\"></i>";
                } elseif (count($notification) > 0 && $notification->status == 0) {
                    $nome = \App\User::where('id', $notification->open_user_id)->first()->bigname;
                    $DATA = $notification->open_at;
                    return "<i id=\"request_palete_wait_$ID\" data-obs=\"( Pedido de palete efectuado por: <b>$nome</b> <i>[ $DATA ]</i> )\" data-palete=\"$PALETE_ID\" data-rel=\"$ID\" style=\"color: orange;\" class=\"fa fa-clock-o fa-2x request_palete_wait\"></i>";
                } else {
                    return "<i style=\"color: #CCCCCC;\" class=\"fa fa-minus fa-2x\"></i>";
                }
            }),
            // $header_palete_order = AdminColumn::custom()
            // ->setLabel('Ordem')
            // ->setWidth('60px')
            // ->setHtmlAttribute('class', "$more_css")
            // ->setCallback(function ($instance) {
            //     $options = "";
            //     $o = 0;
            //     while ($o < 10) {
            //         $selected = "";
            //         if ($instance->sum_order == $o) {
            //             $selected = "selected";
            //         }
            //         $options.="<option $selected value=\"$o\">$o</option>";
            //         $o++;
            //     }
            //     return "<select class=\"reorder form-control input-sm\" rel=\"$instance->id\" id=\"reorder_$instance->id\">$options</select>";
            // }),
        ]);
        $header_palete_store->getHeader()->setHtmlAttribute('class', "bg-red $more_css");
        $header_palete_auth->getHeader()->setHtmlAttribute('class', "bg-black $more_css");
        $header_palete_ask->getHeader()->setHtmlAttribute('class', "bg-green $more_css");
        //$header_palete_order->getHeader()->setHtmlAttribute('class', "bg-black $more_css");
        return $display;
    });
    //    
});

<?php 

Route::get('', ['as' => 'admin.dashboard', 'uses' => '\App\Http\Controllers\HomeController@dashboard']);

Route::get('/rma', ['as' => 'admin.rma', 'uses' => '\App\Http\Controllers\HomeController@rma']);

Route::get('/distributors', ['as' => 'admin.serralharia', 'uses' => '\App\Http\Controllers\HomeController@distributors']);

Route::get('/distributors2', ['as' => 'admin.dist2', 'uses' => '\App\Http\Controllers\HomeController@distributors2']);

Route::get('/distributors3', ['as' => 'admin.dist3', 'uses' => '\App\Http\Controllers\HomeController@distributors3']);

Route::get('/downloads/{type}/{pp}', ['as' => 'admin_downloads', 'uses' => '\App\Http\Controllers\HomeController@downloads']);

Route::get('/serralharia', ['as' => 'admin.serralharia', 'uses' => '\App\Http\Controllers\HomeController@serralharia']);
Route::get('/serralharia/open_mount', ['as' => 'admin.serralharia_open_mount', 'uses' => '\App\Http\Controllers\HomeController@serralharia_open_mount']);

Route::get('/tecnica', ['as' => 'admin.tecnica', 'uses' => '\App\Http\Controllers\HomeController@tecnica']);
Route::get('/tecnica/open_mount', ['as' => 'admin.tecnica_open_mount', 'uses' => '\App\Http\Controllers\HomeController@tecnica_open_mount']);

Route::get('/cnc', ['as' => 'admin.cnc', 'uses' => '\App\Http\Controllers\HomeController@cnc']);
Route::get('/cnc_bordo', ['as' => 'admin.cnc_bordo', 'uses' => '\App\Http\Controllers\HomeController@cnc_bordo']);
Route::get('/cnc_bordo_v2', ['as' => 'admin.cnc_bordo_v2', 'uses' => '\App\Http\Controllers\HomeController@cnc_bordo_v2']);

Route::get('/prep', ['as' => 'admin.prep', 'uses' => '\App\Http\Controllers\HomeController@prep']);
//Route::get('update_nxt', ['as' => 'admin.update_nxt', 'uses' => '\App\Http\Controllers\HomeController@update_nxt']);

Route::get('/distro_refresh', ['as' => 'admin.distro_refresh', 'uses' => '\App\Http\Controllers\HomeController@distro_refresh']);

Route::get('/wood', ['as' => 'admin.wood', 'uses' => '\App\Http\Controllers\HomeController@wood']);
Route::get('/wood_bordo', ['as' => 'admin.cnc_wood', 'uses' => '\App\Http\Controllers\HomeController@wood_bordo']);
Route::get('/wood/open', ['as' => 'admin.wood_open', 'uses' => '\App\Http\Controllers\HomeController@wood_open']);

Route::get('/blackfinishing', ['as' => 'admin.blackfinishing', 'uses' => '\App\Http\Controllers\HomeController@blackfinishing']);
Route::get('/wires', ['as' => 'admin.wires', 'uses' => '\App\Http\Controllers\HomeController@wires']);

Route::get('/woodfinishing', ['as' => 'admin.woodfinishing', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing']);
Route::get('/woodfinishing_bordo', ['as' => 'admin.cnc_woodfinishing', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_bordo']);
Route::get('/woodfinishing/open', ['as' => 'admin.woodfinishing_open', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_open']);

Route::get('/paint', ['as' => 'admin.paint', 'uses' => '\App\Http\Controllers\HomeController@paint']);
Route::get('/paint_bordo', ['as' => 'admin.paint_bordo', 'uses' => '\App\Http\Controllers\HomeController@paint_bordo']);
Route::get('/paint/open', ['as' => 'admin.paint_open', 'uses' => '\App\Http\Controllers\HomeController@paint_open']);

Route::get('/mount', ['as' => 'admin.mount', 'uses' => '\App\Http\Controllers\HomeController@mount']);
Route::get('/mount_bordo', ['as' => 'admin.mount_bordo', 'uses' => '\App\Http\Controllers\HomeController@mount_bordo']);
Route::get('/mount/open', ['as' => 'admin.mount_open', 'uses' => '\App\Http\Controllers\HomeController@mount_open']);

Route::get('/materials', ['as' => 'admin.materials', 'uses' => '\App\Http\Controllers\HomeController@materials']);

Route::post('/send_picker/{section}', ['as' => 'admin.send_picker', 'uses' => '\App\Http\Controllers\HomeController@send_picker']);

Route::get('/serralharia/history/get_locksmith_pp/{id}', ['as' => 'admin.cnc_get_pp', 'uses' => '\App\Http\Controllers\HomeController@locksmith_get_pp']);
Route::get('/tecnica/history/get_tech_pp/{id}', ['as' => 'admin.tech_get_pp', 'uses' => '\App\Http\Controllers\HomeController@tech_get_pp']);
Route::get('/requests/cnc_history/get_pp/{id}', ['as' => 'admin.cnc_get_pp', 'uses' => '\App\Http\Controllers\HomeController@cnc_get_pp']);
Route::get('/prep/history/get_pp/{id}', ['as' => 'admin.prep_get_pp', 'uses' => '\App\Http\Controllers\HomeController@prep_get_pp']);
Route::get('/wood/history/get_pp/{id}', ['as' => 'admin.wood_get_pp', 'uses' => '\App\Http\Controllers\HomeController@wood_get_pp']);
Route::get('/woodfinishing/history/get_pp/{id}', ['as' => 'admin.woodfinishing_get_pp', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_get_pp']);
Route::get('/paint/history/get_pp/{id}', ['as' => 'admin.paint_get_pp', 'uses' => '\App\Http\Controllers\HomeController@paint_get_pp']);
Route::get('/mount/history/get_pp/{id}', ['as' => 'admin.mount_get_pp', 'uses' => '\App\Http\Controllers\HomeController@mount_get_pp']);

Route::get('/request_weeks/get_product/{id}', ['as' => 'admin.week_get_product', 'uses' => '\App\Http\Controllers\HomeController@week_get_product']);
Route::get('/request_week_orders/get_product/{id}', ['as' => 'admin.week_orders_get_product', 'uses' => '\App\Http\Controllers\HomeController@week_get_product']);

Route::get('reorder_orders/{id}/{value}', ['as' => 'admin.reorder_orders', 'uses' => '\App\Http\Controllers\HomeController@reorder_orders']);

Route::get('/locksmith_weeks/get_locksmith/{id}', ['as' => 'admin.week_get_locksmith', 'uses' => '\App\Http\Controllers\HomeController@week_get_locksmith']);
Route::get('/deadline/get_product/{id}', ['as' => 'admin.deadline_get_product', 'uses' => '\App\Http\Controllers\HomeController@week_get_product']);

Route::get('/tech_weeks/get_tech/{id}', ['as' => 'admin.week_get_tech', 'uses' => '\App\Http\Controllers\HomeController@week_get_tech']);

Route::get('/stocks', ['as' => 'admin.week_get_product', 'uses' => '\App\Http\Controllers\HomeController@stock']);
Route::get('/stocks_ok', ['as' => 'admin.week_get_product_ok', 'uses' => '\App\Http\Controllers\HomeController@stock_ok']);
Route::get('/stocks_search', ['as' => 'admin.week_get_product_ok_search', 'uses' => '\App\Http\Controllers\HomeController@stock_ok_search']);

Route::get('/triple_weeks', ['as' => 'admin.triple_week', 'uses' => '\App\Http\Controllers\HomeController@triple_week']);

Route::get('/serralharia/history/get_locksmith_pp_edit/{id}', ['as' => 'admin.locksmith_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@locksmith_get_pp_edit']);
Route::get('/tecnica/history/get_tech_pp_edit/{id}', ['as' => 'admin.tech_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@tech_get_pp_edit']);
Route::get('/requests/cnc_history/get_pp_edit/{id}', ['as' => 'admin.cnc_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@cnc_get_pp_edit']);
Route::get('/prep/history/get_pp_edit/{id}', ['as' => 'admin.prep_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@prep_get_pp_edit']);
Route::get('/wood/history/get_pp_edit/{id}', ['as' => 'admin.wood_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@wood_get_pp_edit']);
Route::get('/woodfinishing/history/get_pp_edit/{id}', ['as' => 'admin.woodfinishing_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_get_pp_edit']);
Route::get('/paint/history/get_pp_edit/{id}', ['as' => 'admin.paint_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@paint_get_pp_edit']);
Route::get('/mount/history/get_pp_edit/{id}', ['as' => 'admin.mount_get_pp_edit', 'uses' => '\App\Http\Controllers\HomeController@mount_get_pp_edit']);

Route::get('/locksmith_status/{id}/{status}', ['as' => 'admin.locksmith_status', 'uses' => '\App\Http\Controllers\HomeController@locksmith_status']);
Route::get('/cnc_status/{id}/{status}', ['as' => 'admin.cnc_status', 'uses' => '\App\Http\Controllers\HomeController@cnc_status']);
Route::get('prep/prep_status/{id}/{status}', ['as' => 'admin.prep_status', 'uses' => '\App\Http\Controllers\HomeController@prep_status']);
Route::get('/wood_status/{id}/{status}', ['as' => 'admin.wood_status', 'uses' => '\App\Http\Controllers\HomeController@wood_status']);
Route::get('/woodfinishing_status/{id}/{status}', ['as' => 'admin.woodfinishing_status', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_status']);
Route::get('/paint_status/{id}/{status}', ['as' => 'admin.wood_status', 'uses' => '\App\Http\Controllers\HomeController@paint_status']);
Route::get('/stock_status/{id}/{status}', ['as' => 'admin.stock_status', 'uses' => '\App\Http\Controllers\HomeController@stock_status']);
Route::get('wood/prep_status/{id}/{status}/{user_id}', ['as' => 'admin.wood_prep_status', 'uses' => '\App\Http\Controllers\HomeController@prep_status']);
Route::get('/prep_status/{id}/{status}/{user_id}', ['as' => 'admin.prep_status', 'uses' => '\App\Http\Controllers\HomeController@prep_status']);
Route::get('woodfinishing/wood_status/{id}/{status}/{user_id}', ['as' => 'admin.wood_status', 'uses' => '\App\Http\Controllers\HomeController@wood_status']);
Route::get('paint/woodfinishing_status/{id}/{status}/{user_id}', ['as' => 'admin.wood_status', 'uses' => '\App\Http\Controllers\HomeController@woodfinishing_status']);
Route::get('mount/paint_status/{id}/{status}/{user_id}', ['as' => 'admin.paint_status', 'uses' => '\App\Http\Controllers\HomeController@paint_status']);
Route::get('serralharia/locksmith_status/{id}/{status}/{user_id}', ['as' => 'admin.locksmith_status', 'uses' => '\App\Http\Controllers\HomeController@locksmith_status']);
Route::get('serralharia/locksmith_status/{id}/{status}', ['as' => 'admin.locksmith_status', 'uses' => '\App\Http\Controllers\HomeController@locksmith_status']);


Route::get('check_waiting/{value}', ['as' => 'admin.check_waiting', 'uses' => '\App\Http\Controllers\HomeController@check_waiting']);
Route::get('request_palete_to_mount/{id}/{user_id}', ['as' => 'admin.request_palete_to_mount', 'uses' => '\App\Http\Controllers\HomeController@request_palete_to_mount']);
Route::get('request_palete_to_mount_ok/{id}/{user_id}', ['as' => 'admin.request_palete_to_mount_ok', 'uses' => '\App\Http\Controllers\HomeController@request_palete_to_mount_ok']);

Route::get('/mount/history/check_serial/{id}/{serial}', ['as' => 'admin.mount_check_serial', 'uses' => '\App\Http\Controllers\HomeController@mount_check_serial']);

Route::get('get_code/{id}', ['as' => 'admin.get_code', 'uses' => '\App\Http\Controllers\HomeController@get_code']);
Route::get('get_picker/{id}', ['as' => 'admin.get_picker', 'uses' => '\App\Http\Controllers\HomeController@get_picker']);
Route::get('get_locksmith_picker/{id}', ['as' => 'admin.get_locksmith_picker', 'uses' => '\App\Http\Controllers\HomeController@get_locksmith_picker']);
Route::get('get_tech_picker/{id}', ['as' => 'admin.get_tech_picker', 'uses' => '\App\Http\Controllers\HomeController@get_tech_picker']);
Route::get('get_cnc_picker/{id}', ['as' => 'admin.get_cnc_picker', 'uses' => '\App\Http\Controllers\HomeController@get_cnc_picker']);
Route::get('get_wood_picker/{id}', ['as' => 'admin.get_wood_picker', 'uses' => '\App\Http\Controllers\HomeController@get_wood_picker']);
Route::get('get_prep_picagem_serial/{id}', ['as' => 'admin.get_prep_picagem_serial', 'uses' => '\App\Http\Controllers\HomeController@get_prep_picagem_serial']);
Route::get('get_wood_picagem_serial/{id}', ['as' => 'admin.get_wood_picagem_serial', 'uses' => '\App\Http\Controllers\HomeController@get_wood_picagem_serial']);
Route::get('get_woodfinishing_picagem_serial/{id}', ['as' => 'admin.get_woodfinishing_picagem_serial', 'uses' => '\App\Http\Controllers\HomeController@get_woodfinishing_picagem_serial']);
Route::get('get_paint_picagem_serial/{id}', ['as' => 'admin.get_paint_picagem_serial', 'uses' => '\App\Http\Controllers\HomeController@get_paint_picagem_serial']);
Route::get('get_woodfinishing_picker/{id}', ['as' => 'admin.get_woodfinishing_picker', 'uses' => '\App\Http\Controllers\HomeController@get_woodfinishing_picker']);
Route::get('get_paint_picker/{id}', ['as' => 'admin.get_paint_picker', 'uses' => '\App\Http\Controllers\HomeController@get_paint_picker']);
Route::get('set_oclock/{id}/{value}', ['as' => 'admin.set_oclock', 'uses' => '\App\Http\Controllers\HomeController@set_oclock']);

Route::get('new_triple', ['as' => 'admin.new_triple', 'uses' => '\App\Http\Controllers\HomeController@new_triple']);
Route::get('new_triple_week', ['as' => 'admin.new_triple_week', 'uses' => '\App\Http\Controllers\HomeController@new_triple_week']);
Route::get('new_triple_week_up', ['as' => 'admin.new_triple_week_up', 'uses' => '\App\Http\Controllers\HomeController@new_triple_week_up']);

Route::get('view_paletes', ['as' => 'admin.view_paletes', 'uses' => '\App\Http\Controllers\HomeController@view_paletes']);
Route::get('view_paletes_serralharia', ['as' => 'admin.view_paletes_serralharia', 'uses' => '\App\Http\Controllers\HomeController@view_paletes_serralharia']);
Route::get('view_paletes_tecnica', ['as' => 'admin.view_paletes_tecnica', 'uses' => '\App\Http\Controllers\HomeController@view_paletes_tecnica']);
Route::get('view_paletes_arquivo_serralharia', ['as' => 'admin.view_paletes_serralharia', 'uses' => '\App\Http\Controllers\HomeController@view_paletes_arquivo_serralharia']);
Route::get('view_paletes_arquivo_tecnica', ['as' => 'admin.view_paletes_tecnica', 'uses' => '\App\Http\Controllers\HomeController@view_paletes_arquivo_tecnica']);
Route::get('serralharia/paletes_list', ['as' => 'admin.serralharia_paletes', 'uses' => '\App\Http\Controllers\HomeController@serralharia_paletes_view']);
Route::get('tecnica/paletes_list', ['as' => 'admin.tecnica_paletes', 'uses' => '\App\Http\Controllers\HomeController@tecnica_paletes_view']);
Route::get('serralharia/paletes_history', ['as' => 'admin.serralharia_paletes_history', 'uses' => '\App\Http\Controllers\HomeController@serralharia_paletes_history']);
Route::get('tecnica/paletes_history', ['as' => 'admin.tecnica_paletes_history', 'uses' => '\App\Http\Controllers\HomeController@tecnica_paletes_history']);
Route::get('view_paletes_arquivo', ['as' => 'admin.view_paletes_arquivo', 'uses' => '\App\Http\Controllers\HomeController@view_paletes_arquivo']);
Route::get('/paletes/{id}/{value}/{user}', ['as' => 'admin.palete_status', 'uses' => '\App\Http\Controllers\HomeController@palete_status']);

Route::get('/locksmith_paletes/{id}/{value}/{user}', ['as' => 'admin.locksmith_paletes_status', 'uses' => '\App\Http\Controllers\HomeController@locksmith_palete_status']);
Route::get('/tech_paletes/{id}/{value}/{user}', ['as' => 'admin.tech_paletes_status', 'uses' => '\App\Http\Controllers\HomeController@tech_palete_status']);

Route::get('mount/paletes', ['as' => 'admin.mount_paletes', 'uses' => '\App\Http\Controllers\HomeController@mount_paletes']);
Route::get('mount/paletes_history', ['as' => 'admin.mount_paletes_history', 'uses' => '\App\Http\Controllers\HomeController@mount_paletes_history']);

Route::get('/pdf/{id}/{value}', ['as' => 'admin.pdf', 'uses' => '\App\Http\Controllers\HomeController@pdf']);

Route::get('/get_comsumables/{id}', ['as' => 'admin.get_comsumables', 'uses' => '\App\Http\Controllers\HomeController@get_comsumables']);

Route::get('/delete_palete_history/{id}', ['as' => 'admin.delete_palete_history', 'uses' => '\App\Http\Controllers\HomeController@delete_palete_history']);
Route::get('/locksmith_delete_palete_history/{id}', ['as' => 'admin.locksmith_delete_palete_history', 'uses' => '\App\Http\Controllers\HomeController@locksmith_delete_palete_history']);
Route::get('/tech_delete_palete_history/{id}', ['as' => 'admin.tech_delete_palete_history', 'uses' => '\App\Http\Controllers\HomeController@tech_delete_palete_history']);

Route::get('/palete_store_obs/{id}/{obs}', ['as' => 'admin.palete_store_obs', 'uses' => '\App\Http\Controllers\HomeController@palete_store_obs']);

Route::get('/palete_store_obs_delete/{id}', ['as' => 'admin.palete_store_obs', 'uses' => '\App\Http\Controllers\HomeController@palete_store_obs_delete']);

Route::get('/locksmith_palete_store_obs/{id}/{obs}', ['as' => 'admin.locksmith_palete_store_obs', 'uses' => '\App\Http\Controllers\HomeController@locksmith_palete_store_obs']);
Route::get('/tech_palete_store_obs/{id}/{obs}', ['as' => 'admin.tech_palete_store_obs', 'uses' => '\App\Http\Controllers\HomeController@tech_palete_store_obs']);

Route::get('/locksmith_palete_store_obs_delete/{id}', ['as' => 'admin.locksmith_palete_store_obs_delete', 'uses' => '\App\Http\Controllers\HomeController@locksmith_palete_store_obs_delete']);
Route::get('/tech_palete_store_obs_delete/{id}', ['as' => 'admin.tech_palete_store_obs_delete', 'uses' => '\App\Http\Controllers\HomeController@tech_palete_store_obs_delete']);

Route::get('/nxt_phc_palete_delete/{id}', ['as' => 'admin.nxt_phc_palete_delete', 'uses' => '\App\Http\Controllers\HomeController@nxt_phc_palete_delete']);

Route::get('/nxt_phc_palete_yes/{id}/{obs}', ['as' => 'admin.nxt_phc_palete_yes', 'uses' => '\App\Http\Controllers\HomeController@nxt_phc_palete_yes']);

Route::get('/locksmith_nxt_phc_palete_delete/{id}', ['as' => 'admin.locksmith_nxt_phc_palete_delete', 'uses' => '\App\Http\Controllers\HomeController@locksmith_nxt_phc_palete_delete']);

Route::get('/locksmith_nxt_phc_palete_yes/{id}/{obs}', ['as' => 'admin.locksmith_nxt_phc_palete_yes', 'uses' => '\App\Http\Controllers\HomeController@locksmith_nxt_phc_palete_yes']);

Route::get('/tech_nxt_phc_palete_delete/{id}', ['as' => 'admin.tech_nxt_phc_palete_delete', 'uses' => '\App\Http\Controllers\HomeController@tech_nxt_phc_palete_delete']);

Route::get('/tech_nxt_phc_palete_yes/{id}/{obs}', ['as' => 'admin.tech_nxt_phc_palete_yes', 'uses' => '\App\Http\Controllers\HomeController@tech_nxt_phc_palete_yes']);

Route::post('/send_comsumables', ['as' => 'admin.send_comsumables', 'uses' => '\App\Http\Controllers\HomeController@send_comsumables']);

Route::get('/view_comsumables', ['as' => 'admin.view_comsumables', 'uses' => '\App\Http\Controllers\HomeController@view_comsumables']);

Route::get('/valid_comsumables/{id}/{value}/{user_id}', ['as' => 'admin.accept_comsumables', 'uses' => '\App\Http\Controllers\HomeController@accept_comsumables']);

Route::get('/view_comsumables_history', ['as' => 'admin.view_comsumables_history', 'uses' => '\App\Http\Controllers\HomeController@view_comsumables_history']);

Route::get('unlock_pp/{pp}/{id}/{value}', ['as' => 'admin.unlock_pp', 'uses' => '\App\Http\Controllers\HomeController@unlock_pp']);
Route::get('schedule_new_date/{pp}/{id}/{day}/{month}/{year}', ['as' => 'admin.schedule_new', 'uses' => '\App\Http\Controllers\HomeController@schedule_pp']);
Route::get('obs_new/{pp}/{id}/{obs}', ['as' => 'admin.obs_new', 'uses' => '\App\Http\Controllers\HomeController@obs_new']);

Route::get('get_requests_updated', ['as' => 'admin.get_requests_updated', 'uses' => '\App\Http\Controllers\HomeController@get_requests_updated']);
Route::get('palete_update_state_admin/{section}/{status}/{id}', ['as' => 'admin.palete_update_state_admin', 'uses' => '\App\Http\Controllers\HomeController@palete_update_state_admin']);
Route::get('get_palete_update_state_admin/{id}', ['as' => 'admin.get_palete_update_state_admin', 'uses' => '\App\Http\Controllers\HomeController@get_palete_update_state_admin']);

Route::get('locksmith_palete_update_state_admin/{section}/{status}/{id}', ['as' => 'admin.locksmith_palete_update_state_admin', 'uses' => '\App\Http\Controllers\HomeController@locksmith_palete_update_state_admin']);
Route::get('get_locksmith_palete_update_state_admin/{id}', ['as' => 'admin.get_locksmith_palete_update_state_admin', 'uses' => '\App\Http\Controllers\HomeController@get_locksmith_palete_update_state_admin']);

Route::get('levantamento_palete/{user_id}/{section}/{object_id}/{value}', ['as' => 'admin.levantamento_palete', 'uses' => '\App\Http\Controllers\HomeController@levantamento_palete']);

Route::get('set_work_absences/{id}/{valid}', ['as' => 'admin.set_work_absences', 'uses' => '\App\Http\Controllers\HomeController@set_work_absences']);

Route::get('fixTable', ['as' => 'admin.fixTable', 'uses' => '\App\Http\Controllers\HomeController@fixTable']);


//Route::get('/cnc', ['as' => 'admin.cnc', function () {
//	$content = 'Define your information here.';
//	return AdminSection::view($content, 'Information');
//}]);


//Route::get('/information', ['as' => 'admin.information', function () {
//	$content = 'Define your information here.';
//	return AdminSection::view($content, 'Information');
//}]);
//
//Route::post('/news/export.json', ['as' => 'admin.news.export', function () {
//	$response = new \Illuminate\Http\JsonResponse([
//		'title' => 'Congratulation! You exported news.',
//		'news' => App\Model\News5::whereIn('id', Request::get('id', []))->get()
//	]);
//
//	$response->setJsonOptions(JSON_PRETTY_PRINT);
//
//	return $response;
//}]);
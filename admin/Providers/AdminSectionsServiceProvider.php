<?php

namespace Admin\Providers;

use Illuminate\Routing\Router;
use SleepingOwl\Admin\Contracts\Navigation\NavigationInterface;
use SleepingOwl\Admin\Contracts\Template\MetaInterface;
use SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider {

    /**
     * @var array
     */
    protected $widgets = [
        \Admin\Widgets\DashboardMap::class,
        \Admin\Widgets\NavigationUserBlock::class
    ];

    /**
     * @var array
     */
    protected $sections = [
//        'App\Model\Form' => 'Admin\Http\Sections\Form',
        // GESTAO
        'App\User' => 'Admin\Http\Sections\Users',
        'App\Role' => 'Admin\Http\Sections\Roles',
        'App\Model\Serie' => 'Admin\Http\Sections\Serie',
        'App\Model\Product' => 'Admin\Http\Sections\Product',
        'App\Model\Problem' => 'Admin\Http\Sections\Problem',
        'App\Model\RequestWeek' => 'Admin\Http\Sections\RequestWeek',
        'App\Model\RequestWeekOrder' => 'Admin\Http\Sections\RequestWeekOrder',
        'App\Model\DeliveryCalc' => 'Admin\Http\Sections\DeliveryCalc',
        'App\Model\StatusRequests' => 'Admin\Http\Sections\StatusRequests',
        'App\Model\RequestWeekGroup' => 'Admin\Http\Sections\RequestWeekGroup',
        'App\Model\Deadline' => 'Admin\Http\Sections\Deadline',
        'App\Model\TripleWeek' => 'Admin\Http\Sections\TripleWeek',
        'App\Model\FreezeWeek_Atraso' => 'Admin\Http\Sections\FreezeWeek_Atraso',
        'App\Model\FreezeWeek' => 'Admin\Http\Sections\FreezeWeek',
        'App\Model\FreezeWeek_1' => 'Admin\Http\Sections\FreezeWeek_1',
        'App\Model\FreezeWeek_2' => 'Admin\Http\Sections\FreezeWeek_2',
        'App\Model\FreezeWeek_3' => 'Admin\Http\Sections\FreezeWeek_3',
        'App\Model\FreezeWeek_All' => 'Admin\Http\Sections\FreezeWeek_All',
        'App\Model\Config' => 'Admin\Http\Sections\Config',
        'App\Model\Config2' => 'Admin\Http\Sections\Config2',
        'App\Model\ConfigTab' => 'Admin\Http\Sections\ConfigTab',
        'App\Model\Customizations' => 'Admin\Http\Sections\Customizations',
        'App\Model\WorkAbsences' => 'Admin\Http\Sections\WorkAbsences',
//        'App\Model\PaletesTab' => 'Admin\Http\Sections\PaletesTab',
        //RELÓGIO DE PONTO
        'App\Model\Clock' => 'Admin\Http\Sections\Clock',
        'App\Model\Clock_2' => 'Admin\Http\Sections\Clock_2',
        'App\Model\Clock_User' => 'Admin\Http\Sections\Clock_User',
        'App\Model\Clock_User_2' => 'Admin\Http\Sections\Clock_User_2',
        'App\Model\Clock_Month' => 'Admin\Http\Sections\Clock_Month',
        'App\Model\Clock_Month_2' => 'Admin\Http\Sections\Clock_Month_2',
        'App\Model\Clock_Day' => 'Admin\Http\Sections\Clock_Day',
        'App\Model\Distro' => 'Admin\Http\Sections\Distro',
        'App\Model\Distro2' => 'Admin\Http\Sections\Distro2',
        'App\Model\Distro_Prosp' => 'Admin\Http\Sections\Distro_Prosp',
        //'App\Model\Brito' => 'Admin\Http\Sections\Brito',
        // PAINEL DE BORDO
        'App\Model\CNC_total' => 'Admin\Http\Sections\CNC_total',
        'App\Model\CNC_total_v2' => 'Admin\Http\Sections\CNC_total_v2',
        // CNC
        'App\Model\Request' => 'Admin\Http\Sections\Request',
        'App\Model\CNC_open' => 'Admin\Http\Sections\CNC_open',
        'App\Model\CNC_history' => 'Admin\Http\Sections\CNC_history',
        'App\Model\CNC_History_Picker' => 'Admin\Http\Sections\CNC_History_Picker',
        'App\Model\CNC_semanal' => 'Admin\Http\Sections\CNC_semanal',
        'App\Model\CNC_semanal_Order' => 'Admin\Http\Sections\CNC_semanal_Order',
        // PREPARAÇÃO
        'App\Model\PREP_open' => 'Admin\Http\Sections\PREP_open',
        'App\Model\PREP_history' => 'Admin\Http\Sections\PREP_history',
        'App\Model\PREP_History_Picker' => 'Admin\Http\Sections\PREP_History_Picker',
        'App\Model\PREP_total' => 'Admin\Http\Sections\PREP_total',
        'App\Model\PREP_bordo' => 'Admin\Http\Sections\PREP_bordo',
        'App\Model\PREP_semanal_Order' => 'Admin\Http\Sections\PREP_semanal_Order',
        // CARPINTARIA
        'App\Model\WOOD_open' => 'Admin\Http\Sections\WOOD_open',
        'App\Model\WOOD_total' => 'Admin\Http\Sections\WOOD_total',
        'App\Model\WOOD_semanal' => 'Admin\Http\Sections\WOOD_semanal',
        'App\Model\WOOD_semanal_Order' => 'Admin\Http\Sections\WOOD_semanal_Order',
        'App\Model\WOOD_history' => 'Admin\Http\Sections\WOOD_history',
        'App\Model\WOOD_History_Picker' => 'Admin\Http\Sections\WOOD_History_Picker',
        'App\Model\WOOD_bordo' => 'Admin\Http\Sections\WOOD_bordo',
        // ACABAMENTO
        'App\Model\WOODFINISHING_open' => 'Admin\Http\Sections\WOODFINISHING_open',
        'App\Model\WOODFINISHING_total' => 'Admin\Http\Sections\WOODFINISHING_total',
        'App\Model\WOODFINISHING_semanal' => 'Admin\Http\Sections\WOODFINISHING_semanal',
        'App\Model\WOODFINISHING_semanal_Order' => 'Admin\Http\Sections\WOODFINISHING_semanal_Order',
        'App\Model\WOODFINISHING_history' => 'Admin\Http\Sections\WOODFINISHING_history',
        'App\Model\WOODFINISHING_History_Picker' => 'Admin\Http\Sections\WOODFINISHING_History_Picker',
        'App\Model\WOODFINISHING_bordo' => 'Admin\Http\Sections\WOODFINISHING_bordo',
        // PINTURA
        'App\Model\PAINT_open' => 'Admin\Http\Sections\PAINT_open',
        'App\Model\PAINT_total' => 'Admin\Http\Sections\PAINT_total',
        'App\Model\PAINT_semanal' => 'Admin\Http\Sections\PAINT_semanal',
        'App\Model\PAINT_semanal_Order' => 'Admin\Http\Sections\PAINT_semanal_Order',
        'App\Model\PAINT_history' => 'Admin\Http\Sections\PAINT_history',
        'App\Model\PAINT_History_Picker' => 'Admin\Http\Sections\PAINT_History_Picker',
        'App\Model\PAINT_bordo' => 'Admin\Http\Sections\PAINT_bordo',
        // MONTAGEM
        'App\Model\MOUNT_open' => 'Admin\Http\Sections\MOUNT_open',
        'App\Model\MOUNT_total' => 'Admin\Http\Sections\MOUNT_total',
        'App\Model\MOUNT_semanal' => 'Admin\Http\Sections\MOUNT_semanal',
        'App\Model\MOUNT_semanal_Order' => 'Admin\Http\Sections\MOUNT_semanal_Order',
        'App\Model\MOUNT_history' => 'Admin\Http\Sections\MOUNT_history',
        'App\Model\MOUNT_history_picker' => 'Admin\Http\Sections\MOUNT_history_picker',
        'App\Model\MOUNT_bordo' => 'Admin\Http\Sections\MOUNT_bordo',
        // STOCKS
        'App\Model\MOUNT_history_Stock' => 'Admin\Http\Sections\MOUNT_history_Stock',
        'App\Model\MOUNT_history_Stock_OK' => 'Admin\Http\Sections\MOUNT_history_Stock_OK',
        'App\Model\MOUNT_history_Stock_OK_Search' => 'Admin\Http\Sections\MOUNT_history_Stock_OK_Search',
        // PALETE
        'App\Model\Palete' => 'Admin\Http\Sections\Palete',
        'App\Model\Palete_Stock' => 'Admin\Http\Sections\Palete_Stock',
        'App\Model\Palete_History' => 'Admin\Http\Sections\Palete_History',
        'App\Model\MOUNT_Palete' => 'Admin\Http\Sections\MOUNT_Palete',
        'App\Model\MOUNT_Palete_History' => 'Admin\Http\Sections\MOUNT_Palete_History',
        // COMSUMABLES
        'App\Model\Comsumables' => 'Admin\Http\Sections\Comsumables',
        'App\Model\ComsumablesOrders' => 'Admin\Http\Sections\ComsumablesOrders',
        'App\Model\ComsumablesOrders_History' => 'Admin\Http\Sections\ComsumablesOrders_History',
        // LOCKSMITH
        'App\Model\Locksmith' => 'Admin\Http\Sections\Locksmith',
        'App\Model\LocksmithWeek' => 'Admin\Http\Sections\LocksmithWeek',
        'App\Model\LocksmithRequests' => 'Admin\Http\Sections\LocksmithRequests',
        'App\Model\LocksmithRequestsUser' => 'Admin\Http\Sections\LocksmithRequestsUser',
        'App\Model\Locksmith_Open' => 'Admin\Http\Sections\Locksmith_Open',
        'App\Model\Locksmith_History' => 'Admin\Http\Sections\Locksmith_History',
        'App\Model\Locksmith_Open_Mount' => 'Admin\Http\Sections\Locksmith_Open_Mount',
        'App\Model\LocksmithSemanal' => 'Admin\Http\Sections\LocksmithSemanal',
        'App\Model\LocksmithFreeze' => 'Admin\Http\Sections\LocksmithFreeze',
        'App\Model\LocksmithPaleteList' => 'Admin\Http\Sections\LocksmithPaleteList',
        'App\Model\LocksmithPaleteHistory' => 'Admin\Http\Sections\LocksmithPaleteHistory',
        'App\Model\LocksmithPaleteStep1' => 'Admin\Http\Sections\LocksmithPaleteStep1',
        'App\Model\LocksmithPaleteStep2' => 'Admin\Http\Sections\LocksmithPaleteStep2',
        'App\Model\LocksmithPaleteHistoryAdmin' => 'Admin\Http\Sections\LocksmithPaleteHistoryAdmin',
        'App\Model\Locksmith_History_Picker' => 'Admin\Http\Sections\Locksmith_History_Picker',
        'App\Model\LocksmithStatusRequests' => 'Admin\Http\Sections\LocksmithStatusRequests',
        'App\Model\LocksmithStock_OK' => 'Admin\Http\Sections\LocksmithStock_OK',
        'App\Model\LocksmithStock' => 'Admin\Http\Sections\LocksmithStock',
        // TECNICA
        'App\Model\Tech' => 'Admin\Http\Sections\Tech',
        'App\Model\TechRequests' => 'Admin\Http\Sections\TechRequests',
        'App\Model\TechWeek' => 'Admin\Http\Sections\TechWeek',
        'App\Model\TechPaleteStep1' => 'Admin\Http\Sections\TechPaleteStep1',
        'App\Model\TechPaleteStep2' => 'Admin\Http\Sections\TechPaleteStep2',
        'App\Model\TechPaleteHistoryAdmin' => 'Admin\Http\Sections\TechPaleteHistoryAdmin',
        'App\Model\TechStatusRequests' => 'Admin\Http\Sections\TechStatusRequests',
        'App\Model\TechFreeze' => 'Admin\Http\Sections\TechFreeze',
        'App\Model\TechSemanal' => 'Admin\Http\Sections\TechSemanal',
        'App\Model\TechPaleteList' => 'Admin\Http\Sections\TechPaleteList',
        'App\Model\TechPaleteHistory' => 'Admin\Http\Sections\TechPaleteHistory',
        'App\Model\Tech_History' => 'Admin\Http\Sections\Tech_History',
        'App\Model\Tech_History_Picker' => 'Admin\Http\Sections\Tech_History_Picker',
        // REDES
        'App\Model\WIRES_History' => 'Admin\Http\Sections\WIRES_History',
        // BLACKFINISHING
        'App\Model\BLACKFINISHING_History' => 'Admin\Http\Sections\BLACKFINISHING_History',
        // MATERIALS
        'App\Model\Materials_Week_Tab_Late' => 'Admin\Http\Sections\Materials_Week_Tab_Late',
        'App\Model\Materials_Week_Tab_1' => 'Admin\Http\Sections\Materials_Week_Tab_1',
        'App\Model\Materials_Consumables' => 'Admin\Http\Sections\Materials_Comsumables',
        // COLORS
        'App\Model\Colors' => 'Admin\Http\Sections\Colors',
    ];

    /**
     * @param \SleepingOwl\Admin\Admin $admin
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin) {
        parent::boot($admin);

        $this->loadViewsFrom(base_path("admin/resources/views"), 'admin');
        $this->registerPolicies('Admin\\Policies\\');

        $this->app->call([$this, 'registerRoutes']);
        $this->app->call([$this, 'registerNavigation']);
        $this->app->call([$this, 'registerViews']);
        $this->app->call([$this, 'registerMediaPackages']);
    }

    /**
     * @param NavigationInterface $navigation
     */
    public function registerNavigation(NavigationInterface $navigation) {
        require base_path('admin/navigation.php');
    }

    /**
     * @param WidgetsRegistryInterface $widgetsRegistry
     */
    public function registerViews(WidgetsRegistryInterface $widgetsRegistry) {
        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }
    }

    /**
     * @param Router $router
     */
    public function registerRoutes(Router $router) {
        $router->group(['prefix' => config('sleeping_owl.url_prefix'), 'middleware' => config('sleeping_owl.middleware')], function ($router) {
            require base_path('admin/Http/routes.php');
        });
    }

    /**
     * @param MetaInterface $meta
     */
    public function registerMediaPackages(MetaInterface $meta) {
        $packages = $meta->assets()->packageManager();

        require base_path('admin/assets.php');
    }

}

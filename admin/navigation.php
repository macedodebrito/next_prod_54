<?php

//CNC BADGE
//$total = DB::table('requests')->sum('value');
//if ($total > 0) {
//    $maquinados = DB::table('request_users')->sum('value');
//    $por_maquinar = $total - $maquinados;
//    $percent_1 = round(100 * $por_maquinar / $total, 2);
//    $badge_cnc = 100-$percent_1;
//} else {
//    $badge_cnc = 0;
//}
//$badge_cnc = "$badge_cnc%";
//// 
// $badge_cnc->setHtmlAttributes('class', 'bg-red')

use SleepingOwl\Admin\Navigation\Page;

$navigation->setFromArray([
//    [
//        'title' => 'CNC2',
//        'icon' => 'fa fa-cogs',
//        'url' => '/admin/cnc2',
//        'priority' => 1,
////        'badge' => $badge_cnc,
//    ],    
    [
        'title' => 'CNC',
        'icon' => 'fa fa-gamepad',
        'priority' => 10,
        'htmlattributes' => ['style' => 'background-color: #00c0ef;'],
        'pages' => [
                    (new Page(\App\Model\WOODFINISHING_history::class))
                    ->setTitle('CNC')
                    ->setIcon('fa fa-gamepad')
                    ->setUrl('/admin/cnc')
                    ->setHtmlAttributes(['style' => 'background-color: #00c0ef;'])
                    ->setPriority(10),
                    (new Page(\App\Model\BLACKFINISHING_History::class))
                    ->setTitle('PREPARAÇÃO')
                    ->setIcon('fa fa-diamond')
                    ->setUrl('/admin/prep')
                    ->setHtmlAttributes(['style' => 'background-color: #000000;'])
                    ->setPriority(20)
        ]
    ],
//    [
//        'title' => 'CNC',
//        'icon' => 'fa fa-cogs',
//        'url' => '/admin/cnc',
//        'priority' => 10,
////        'badge' => $badge_cnc,
//    ],
//    [
//        'title' => 'CARPINTARIA',
//        'icon' => 'fa fa-cubes',
//        'url' => '/admin/wood',
//        'priority' => 20,
//    ],
    [
        'title' => 'CARPINTARIA',
        'icon' => 'fa fa-cubes',
        'priority' => 30,
        'htmlattributes' => ['style' => 'background-color: #dd4b39;'],
        'pages' => [
                    (new Page(\App\Model\WOOD_history::class))
                    ->setTitle('CARPINTARIA')
                    ->setIcon('fa fa-cubes')
                    ->setUrl('/admin/wood')
                    ->setPriority(10)
                    ->setHtmlAttributes(['style' => 'background-color: #dd4b39;']),
                    (new Page(\App\Model\WOODFINISHING_history::class))
                    ->setTitle('ACABAMENTO')
                    ->setIcon('fa fa-diamond')
                    ->setUrl('/admin/woodfinishing')
                    ->setHtmlAttributes(['style' => 'background-color: #605ca8;'])
                    ->setPriority(20),
//                    (new Page(\App\Model\BLACKFINISHING_History::class))
//                    ->setTitle('DE PRETO')
//                    ->setIcon('fa fa-diamond')
//                    ->setUrl('/admin/blackfinishing')
//                    ->setHtmlAttributes(['style' => 'background-color: #000000;'])
//                    ->setPriority(20)
        ]
    ],
//    [
//        'title' => 'ACABAMENTO',
//        'icon' => 'fa fa-diamond',
//        'url' => '/admin/woodfinishing',
//        'priority' => 30,
//    ],
    (new Page(\App\Model\PAINT_history::class))
            ->setTitle('PINTURA')
            ->setIcon('fa fa-paint-brush')
            ->setUrl('/admin/paint')
            ->setPriority(40)
            ->setHtmlAttributes(['style' => 'background-color: #00a65a;']),
//    [
//        'title' => 'PINTURA',
//        'icon' => 'fa fa-paint-brush',
//        'url' => '/admin/paint',
//        'priority' => 40,
//    ],
    (new Page(\App\Model\MOUNT_history::class))
            ->setTitle('MONTAGEM')
            ->setIcon('fa fa-sign-language')
            ->setUrl('/admin/mount')
            ->setPriority(50)
            ->setHtmlAttributes(['style' => 'background-color: #FF851B;']),
//    [
//        'title' => 'MONTAGEM',
//        'icon' => 'fa fa-sign-language',
//        'url' => '/admin/mount',
//        'priority' => 50,
//    ],
    (new Page(\App\Model\Locksmith_History::class))
            ->setTitle('SERRALHARIA')
            ->setIcon('fa fa-cogs')
            ->setUrl('/admin/serralharia')
            ->setPriority(51)
            ->setHtmlAttributes(['style' => 'background-color: #AAAAAA;']),
            (new Page(\App\Model\WIRES_History::class))
            ->setTitle('REDES')
            ->setIcon('fa fa-hashtag')
            ->setUrl('/admin/wires')
            ->setPriority(52)
            ->setHtmlAttributes(['style' => 'background-color: #01FF70;']),
            (new Page(\App\Model\Tech::class))
            ->setTitle('Técnica')
            ->setIcon('fa fa-tachometer')
            ->setUrl('/admin/tecnica')
            ->setPriority(53)
            ->setHtmlAttributes(['style' => 'background-color: #D81B60;']),
            (new Page(\App\Model\Materials::class))
            ->setTitle('Materiais')
            ->setIcon('fa fa-hashtag')
            ->setUrl('/admin/materials')
            ->setPriority(54)
            ->setHtmlAttributes(['style' => 'background-color: #000000;']),
    [
        'title' => 'GESTÃO',
        'icon' => 'fa fa-gear',
        'pages' => [
                    (new Page(\App\Model\TripleWeek::class))
                    ->setIcon('fa fa-link')
                    ->setPriority(1),
                    (new Page(\App\Model\DeliveryCalc::class))
                    ->setIcon('fa fa-calc')
                    ->setPriority(2),
            [
                'title' => 'Funcionários',
                'priority' => 3,
                'icon' => 'fa fa-user',
                'pages' => [
                            (new Page(\App\User::class))
                            ->setTitle('Pessoas')
                            ->setIcon('fa fa-user')
                            ->setPriority(0),
                            (new Page(\App\Role::class))
                            ->setIcon('fa fa-group')
                            ->setPriority(10),
                            (new Page(\App\Model\WorkAbsences::class))
                            ->setIcon('fa fa-group')
                            ->setPriority(20),
                    [
                        'title' => 'Relógio de Ponto',
                        'priority' => 100,
                        'icon' => 'fa fa-tachometer',
                        'pages' => [
                                    (new Page(\App\Model\Clock_2::class))
                                    ->setTitle('AFCMC')
                                    ->setIcon('fa fa-database')
                                    ->setUrl('/admin/clock_2s?id=1')
                                    ->setPriority(20),
                                    (new Page(\App\Model\Clock_2::class))
                                    ->setTitle('CVA Electrónica')
                                    ->setIcon('fa fa-database')
                                    ->setUrl('/admin/clock_2s?id=2')
                                    ->setPriority(30),
                            [
                                'title' => 'AFCMC',
                                'icon' => 'fa fa-database',
                                'url' => '/admin/clock_2s?id=1',
                                'priority' => 30,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'title' => 'Secções',
                'priority' => 4,
                'icon' => 'fa fa-map-signs',
                'pages' => [
                    [
                        'title' => 'Montagem',
                        'priority' => 2,
                        'icon' => 'fa fa-sign-language',
                        'pages' => [
                                    (new Page(\App\Model\Serie::class))
                                    ->setIcon('fa fa-tags')
                                    ->setPriority(10),
                                    (new Page(\App\Model\Product::class))
                                    ->setIcon('fa fa-cube')
                                    ->setPriority(20),
                                    (new Page(\App\Model\Request::class))
                                    ->setIcon('fa fa-check-square-o')
                                    ->setPriority(30),
                                    (new Page(\App\Model\RequestWeekGroup::class))
                                    ->setIcon('fa fa-link')
                                    ->setPriority(40),
                                    (new Page(\App\Model\RequestWeekOrder::class))
                                    ->setIcon('fa fa-list')
                                    ->setPriority(50),
                        ],
                    ],
                    [
                        'title' => 'Serralharia',
                        'priority' => 3,
                        'icon' => 'fa fa-cogs',
                        'pages' => [
                                    (new Page(\App\Model\Locksmith::class))
                                    ->setTitle('Peças')
                                    ->setIcon('fa fa-cube')
                                    ->setPriority(41),
                                    (new Page(\App\Model\LocksmithRequests::class))
                                    ->setTitle('Pedidos')
                                    ->setIcon('fa fa-check-square-o')
                                    ->setPriority(42),
                                    (new Page(\App\Model\LocksmithWeek::class))
                                    ->setIcon('fa fa-list')
                                    ->setPriority(43),
                        ],
                    ],
                    [
                        'title' => 'Técnica',
                        'priority' => 4,
                        'icon' => 'fa fa-tachometer',
                        'pages' => [
                                    (new Page(\App\Model\Tech::class))
                                    ->setTitle('Peças')
                                    ->setIcon('fa fa-cube')
                                    ->setPriority(41),
                                    (new Page(\App\Model\TechRequests::class))
                                    ->setTitle('Pedidos')
                                    ->setIcon('fa fa-check-square-o')
                                    ->setPriority(42),
                                    (new Page(\App\Model\TechWeek::class))
                                    ->setIcon('fa fa-list')
                                    ->setPriority(43),
                        ],
                    ],
                            (new Page(\App\Model\Customizations::class))
                            ->setTitle('Customizações')
                            ->setIcon('fa fa-cogs')
                            ->setPriority(70),
                            (new Page(\App\Model\Colors::class))
                            ->setTitle('Cores')
                            ->setIcon('fa fa-paint-brush')
                            ->setPriority(71),
                ],
            ],
            [
                'title' => 'Reservas',
                'icon' => 'fa fa-tasks',
                'priority' => 5,
                'pages' => [
                            (new Page(\App\Model\StatusRequests::class))
                            ->setTitle('Montagem')
                            ->setIcon('fa fa-sign-language')
                            ->setPriority(10),
                            (new Page(\App\Model\LocksmithStatusRequests::class))
                            ->setTitle('Serralharia')
                            ->setIcon('fa fa-cogs')
                            ->setPriority(20),
                            (new Page(\App\Model\TechStatusRequests::class))
                            ->setTitle('Técnica')
                            ->setIcon('fa fa-tachometer')
                            ->setPriority(30),
                ]
            ],
            [
                'title' => 'Gestão de Paletes',
                'icon' => 'fa fa-database',
                'priority' => 48,
                'pages' => [
                            (new Page(\App\Model\Palete::class))
                            ->setTitle('Armazém')
                            ->setIcon('fa fa-sign-language')
                            ->setUrl('/admin/view_paletes')
                            ->setPriority(10),
                            (new Page(\App\Model\LocksmithPalete::class))
                            ->setTitle('Serralharia')
                            ->setIcon('fa fa-cogs')
                            ->setUrl('/admin/view_paletes_serralharia')
                            ->setPriority(20),
                            (new Page(\App\Model\TechPalete::class))
                            ->setTitle('Técnica')
                            ->setIcon('fa fa-tachometer')
                            ->setUrl('/admin/view_paletes_tecnica')
                            ->setPriority(30),
                ]
            ],
                    (new Page(\App\Model\Comsumables::class))
                    ->setIcon('fa fa-shopping-basket')
                    ->setPriority(49),
                    (new Page(\App\Model\Distro::class))
                    ->setTitle('Distribuição')
                    ->setIcon('fa fa-plane')
                    ->setUrl('/admin/distributors')
                    ->setPriority(50),
                    (new Page(\App\Model\Distro2::class))
                    ->setTitle('Logística')
                    ->setIcon('fa fa-plane')
                    ->setUrl('/admin/distributors3')
                    ->setPriority(60),
                    (new Page(\App\Model\MOUNT_history_Stock::class))
                    ->setTitle('Stock')
                    ->setIcon('fa fa-list')
                    ->setUrl('/admin/stocks_ok')
                    ->setPriority(70),
        ]
    ],]
);

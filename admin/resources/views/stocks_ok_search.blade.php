<?php
$montagem_espera = \App\Model\MountUser::where('published', '!=', 0)->sum('value');
?>
<!-- Info boxes -->
<div class="row">

    <div class="col-md-4 col-sm-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-lime"><i class="fa fa-database"></i></span>

                <div class="info-box-content">
                    <h1>PESQUISAR STOCK</h1>
                </div>
                <!-- /.info-box-content -->
            </div>
    </div>

    <div class="col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/stocks') }}">
            <div class="info-box">
                <span class="info-box-icon bg-lime"><i class="fa fa-search"></i></span>

                <div class="info-box-content">
                    <h3>VER ARMAZÉM</h3>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div>
    
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/stocks_ok') }}">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <h3>VER STOCK</h3>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div>
    
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>Material em Stock</b></span>
                    <hr/>
                    <span class="info-box-text">Listado: <?=$montagem_espera?></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\MOUNT_history_Stock_OK_Search')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>
</div>

<!-- Info boxes -->
<div class="row">


    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-diamond"></i></span>

                <div class="info-box-content">
                    <h1>PREPARAÇÃO</h1>
                </div>
                <!-- /.info-box-content -->
            </div>
    </div>
    <!-- /.col -->
    <div class="col-xs-6 col-sm-offset-5 col-sm-3">
        <a href="{{ url('/admin/wood/open') }}">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">SAÍDAS<br/>(Carpintaria)</span>
            </div>
            <!--            {!! 
                        AdminSection::getmodel('App\Model\WOOD_history')->fireDisplay() 
                        !!}  -->
            <!-- /.info-box-content -->
        </div>
        </a>
    </div>
    <!-- /.col -->
<!--    <div class="col-md-offset-8 col-md-2 col-sm-2 col-xs-12">
        <a href="{{ URL::to('admin') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-chevron-left"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">VOLTAR ÀS SECÇÕES</span>
                </div>
                 /.info-box-content 
            </div>
             /.info-box 
        </a>
    </div>    -->
    <!-- /.col -->
</div>


<?php
$por_validar = DB::table('request_users')->where('published', 0)->where('prep', 0)->sum('value');
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">               
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>Histórico</b></span>                
            </div>
            {!! 
            AdminSection::getmodel('App\Model\PREP_open')->fireDisplay() 
            !!}             
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

</div>
<!--<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>-->
<!-- /.row -->


<!-- /.row -->

<?php

use App\Role;
use App\User;

$waiting = DB::table('paint_users')->where('published', 0)->sum('value');

$role_check = 0;
$role_id = Role::where('name', 'admin')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}
$role_id = Role::where('name', 'manager')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}

$get_week_atraso = \App\Model\FreezeWeek::where('week', -1)->get();
$week_atraso_points = 0;
foreach ($get_week_atraso as $week) {
    $week_atraso_points = $week_atraso_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_0 = \App\Model\FreezeWeek::where('week', 0)->get();
$week_0_points = 0;
foreach ($get_week_0 as $week) {
    $week_0_points = $week_0_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_1 = \App\Model\FreezeWeek::where('week', 1)->get();
$week_1_points = 0;
foreach ($get_week_1 as $week) {
    $week_1_points = $week_1_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_2 = \App\Model\FreezeWeek::where('week', 2)->get();
$week_2_points = 0;
foreach ($get_week_2 as $week) {
    $week_2_points = $week_2_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_3 = \App\Model\FreezeWeek::where('week', 3)->get();
$week_3_points = 0;
foreach ($get_week_3 as $week) {
    $week_3_points = $week_3_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$view = "list"; // weeks / list
?>

<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-sign-language"></i></span>

            <div class="info-box-content">
                <h1>MONTAGEM</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/mount/open') }}">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-sign-in"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ENTRADAS<br/>(da PINTURA)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->    

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ url('/admin/mount/total') }}">
            <div class="info-box">
                <span class="info-box-icon bg-grey"><i class="fa fa-cubes"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DISPONÍVEL<br>(NA MONTAGEM)</span>
                </div>
            </div>    
        </a>
    </div>

    <!-- /.col -->
    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/mount_bordo') }}">
            <div class="info-box">
                <span class="info-box-icon bg-maroon"><i class="fa fa-desktop"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PP<br/>(EM ABERTO)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col --> 

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ url('/admin/mount/open') }}">
            <div class="info-box ajax_waiting">
                <span class="info-box-icon bg-red"><i class="fa fa-circle-o-notch fa-spin"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">EM VALIDAÇÃO<br>(PELA MONTAGEM)</span>
                    <span class="info-box-text">EM ESPERA: <span id="waiting_span">{{ $waiting }}</span></span>
                </div>         
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
        <a href="{{ url('/admin/mount/history') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">SAÍDAS<br/>(Armazém)</span>
                </div>
                <!--            {!! 
                            AdminSection::getmodel('App\Model\WOOD_history')->fireDisplay() 
                            !!}  -->
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>

    <div class="col-md-1 col-sm-1 col-xs-6">
        <a href="{{ url('/admin/mount/history_picker/create') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div> 

</div>

<div class="row">
    @if ($view == "weeks")
    <div class="col-xs-offset-3 col-md-4 col-sm-4 col-xs-6">
        <a href="{{ URL::to('admin/mount/week') }}">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span id="txt_btn_hidden_week_mount" class="info-box-text"><b>VER LISTA GERAL DE ENCOMENDAS</b><br/>(MONTAGEM)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    @endif

    <div class="<?php if ($view == 'list') echo 'col-md-offset-5 col-sm-offset-5'; ?> col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/mount/paletes') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-cubes"></i></span>
                <div class="info-box-content">
                    <span id="txt_btn_hidden_week_mount" class="info-box-text"><b>VER PALETES</b><br/>(ARMAZÉM)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

</div>

@if ($view === "weeks")
<div id="tbl_week_atraso" class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <h class="info-box-text"><b>EM ATRASO</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_atraso_points }} Pontos por finalizar</span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_Atraso')->fireDisplay()
            !!}            
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->
</div>

<div id="tbl_week_1" class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <h1 class="info-box-text"><b>SEMANA 1</b></h1>
                <hr/>
                <span class="info-box-text">{{ $week_0_points }} Pontos por finalizar</span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek')->fireDisplay()
            !!}            
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->
</div>

<div id="tbl_week_2" class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 2</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_1_points }} Pontos por finalizar</span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_1')->fireDisplay()
            !!} 
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->
</div>

<div id="tbl_week_3" class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 3</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_2_points }} Pontos por finalizar</span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_2')->fireDisplay()
            !!} 
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->
</div>
@endif

@if ($view === "list")
<div id="tbl_list" class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><b>ENCOMENDAS DE CLIENTES</b></span>
                <hr/>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\MOUNT_semanal_Order')->fireDisplay()
            !!} 
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->
</div>
@endif

<div id="request_palete_bell" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Pretende que o Armazém traga para a Montagem a Palete com o ID <span id="request_palete_id_span" style="font-weight: bold;"></span>?<br/>
        </p>
        <p>
            Coloque o Seu código <input id="request_palete_bell_input" type="password" style="width: 100px;"/>
        </p>
        <br/>
        <p>
            <button disabled class="btn btn-success" id="request_palete_bell_yes" rel="">PEDIR PALETE AO ARMAZÉM</button>
            <button class="btn btn-warning" id="request_palete_bell_no">CANCELAR</button>
        </p>
        <p id="request_palete_bell_loading" class="bold">
        </p>
    </div>
</div>

<div id="request_palete_wait" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Autorizar Palete com o ID <span id="request_palete_id_wait_span" style="font-weight: bold;"></span>?<br/>
        <div id="request_palete_id_wait_div"></div>
        </p>
        <p>
            Coloque o Seu código <input id="request_palete_wait_input" type="password" style="width: 100px;"/>
        </p>
        <br/>
        <p>
            <button disabled class="btn btn-success" id="request_palete_wait_yes" rel="">AUTORIZAR PALETE</button>
            <button class="btn btn-warning" id="request_palete_wait_no">CANCELAR</button>
        </p>
        <p id="request_palete_wait_loading" class="bold">
        </p>
    </div>
</div>

<?php
if ($role_check < 1) {
    ?>
    <script>
        (function worker() {
            $.ajax({
                url: './check_waiting/mount',
                success: function (data) {
                    if (data > 0) {
                        $(".ajax_waiting").fadeIn();
                        $(".ajax_lock").fadeOut();
                    }
                    else {
                        $(".ajax_lock").fadeIn();
                        $(".ajax_waiting").fadeOut();
                    }
                },
                complete: function () {
                    // Schedule the next request when the current one's complete
                    setTimeout(worker, 25000);
                }
            });
        })();

    </script>
    <?php
} else {
    ?>
    <script>
        (function worker() {
            $.ajax({
                url: './check_waiting/mount',
                success: function (data) {
                    if (data > 0) {
                        $(".ajax_waiting").fadeIn();
                        $(".ajax_lock").fadeOut();
                    }
                    else {
                        $(".ajax_lock").fadeIn();
                        $(".ajax_waiting").fadeOut();
                    }
                },
                complete: function () {
                    // Schedule the next request when the current one's complete
                    setTimeout(worker, 25000);
                }
            });
        })();
    </script>
    <?php
}
?>

<script>
    $("select.reorder").bind("change", function () {
        var selectedValue = $(this).val();
        var selectedRel = $(this).attr('rel');
        var myURL = parseUri(window.location.href);
        $("#overlay_waiting").show();
        $.ajax({
            url: './reorder_orders/' + selectedRel + '/' + selectedValue,
//            success: function (data) {
//                
//            },
            complete: function () {
                document.location.reload(true);
            }
        });
    });
</script>
<!-- /.row -->


<!-- /.row -->

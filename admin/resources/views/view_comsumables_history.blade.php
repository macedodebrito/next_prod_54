<?php
//$total = DB::table('requests')->sum('value');
//$maquinados = DB::table('request_users')->sum('value');
//$por_maquinar = $total-$maquinados;
//$percent_1 = round(100*$por_maquinar/$total, 2);
//$percent_2 = round(100*$maquinados/$total, 2);
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-sign-in"></i></span>

            <div class="info-box-content">
                <h1>CONSUMÍVEIS (ARQUIVO)</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- /.col -->
    
    <!-- /.col -->
    <div class="col-xs-offset-3 col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/view_comsumables') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-chevron-left"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">VOLTAR AOS CONSUMÍVEIS</span>
                    <br/>(POR VALIDAR)
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>VALIDADOS</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\ComsumablesOrders_History')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>
<!-- /.row -->

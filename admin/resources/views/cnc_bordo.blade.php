<?php
$total = DB::table('requests')->sum('value');
if ($total > 0) {
    $maquinados = DB::table('request_users')->sum('value');
    $por_maquinar = $total - $maquinados;
    $percent_1 = round(100 * $por_maquinar / $total, 2);
    $percent_2 = round(100 * $maquinados / $total, 2);
} else {
    $maquinados = 0;
    $por_maquinar = 0;
    $percent_1 = 0;
    $percent_2 = 0;
}
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-6">
        <h1>PAINEL DE BORDO</h1>
    </div>
    <!-- /.col -->

    <!-- /.col -->
    <div class="col-md-offset-6 col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/cnc') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-chevron-left"></i></span>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->

</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>CNC</b></span>
                <hr/>  
                <span class="info-box-text"><b>TOTAL PP:</b> {{ $total }} | <b>TOTAL POR MAQUINAR:</b> {{ $por_maquinar }} <b class="text-red">({{ $percent_1 }}%)</b> | <b>TOTAL EXECUTADOS:</b> {{ $maquinados }} <b class="text-green">({{ $percent_2 }}%)</b></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\CNC_total')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->

</div>
<!-- /.row -->

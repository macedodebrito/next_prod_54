<?php
//$total = DB::table('requests')->sum('value');
//$maquinados = DB::table('request_users')->sum('value');
//$por_maquinar = $total-$maquinados;
//$percent_1 = round(100*$por_maquinar/$total, 2)
//$percent_2 = round(100*$maquinados/$total, 2);
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-barcode"></i></span>

            <div class="info-box-content">
                <h1>PALETES (ARMAZÉM)</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- /.col -->

    <!-- /.col -->
    <div class="col-xs-offset-5 col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/view_paletes_arquivo') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-archive"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ARQUIVADO</span>
                    <br/>(TERMINADAS)
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <!-- /.col -->

</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>JÁ SEPARADO</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\Palete_Stock')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>POR SEPARAR</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\Palete')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>

<div id="obs_palete" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Adicione ou Edite Observações da Palete <span id="obs_palete_span" style="font-weight: bold;"></span> <br/>
        </p>
        <p>
            <input name="palete_obs_input" id="palete_obs_input" type="text" style="width: 80%;"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="obs_palete_yes" rel="">GRAVAR</button>
            <button class="btn btn-red" id="obs_palete_delete" rel="">APAGAR</button>
            <button class="btn btn-warning" id="obs_palete_no">CANCELAR</button>
        </p>
    </div>
</div>

<div id="nxt_phc_palete" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Edite os Códigos para o ID <span id="nxt_phc_palete_span" style="font-weight: bold;"></span> <br/>
        </p>
        <p>
            NXT (Por defeito): <input id="nxt_phc_default_palete_obs_input" type="text" disabled style="width: 100px;"/> NSP: <input name="nxt_phc_palete_obs_input" id="nxt_phc_palete_obs_input" type="text" style="width: 100px;"/> / <input name="nxt_phc_palete_obs_input2" id="nxt_phc_palete_obs_input2" type="text" style="width: 100px;"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="nxt_phc_palete_yes" rel="">ACTUALIZAR</button>
            <button class="btn btn-red" id="nxt_phc_palete_delete" rel="">REMOVER NSP</button>
            <button class="btn btn-warning" id="nxt_phc_palete_no">CANCELAR</button>
        </p>
    </div>
</div>

<div id="fixed-header" style="position:fixed; top: 0; background-color: #4bb1d0; margin-left: 1px;">
    <table class="table-info table-hover table table-striped datatables dataTable" style="font-size: 12px;">
    </table>
</div>

<div id="palete_edit_check_overlay" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; top: 30%; font-size: 20px; text-align: center; background: #FFFFFF;" class="row col-xs-10 col-xs-offset-1">
        <div class="row col-xs-8 col-xs-offset-2" style="border-bottom: 1px solid #000000; padding-top: 24px;">
            <p style="padding-bottom: 12px;">
                Editar Vistos para a Palete ( <span id="palete_edit_check_overlay_span" style="font-weight: bold;"></span> ) <br/>
                <input id="palete_edit_check_overlay_id" type="hidden" value=""/>
                <input id="palete_edit_check_overlay_auth" type="hidden" value="<?= Auth::user()->id; ?>"/>
            </p>
            <div class="col-xs-8 bold">Secção</div>
            <div class="col-xs-2 bold">Validado</div>
            <div class="col-xs-2 bold">Autorizado</div>   
        </div>

        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Armazém</div>
            <div class="col-xs-2"><input name="store" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="store_auth" type="checkbox" /></div>      
        </div>

        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Técnica</div>
            <div class="col-xs-2"><input name="tech" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="tech_auth" type="checkbox" /> </div>      
        </div>

        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Serigrafia</div>
            <div class="col-xs-2"><input name="serigrafia" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="serigrafia_auth" type="checkbox" /> </div>      
        </div>


        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Serralharia</div>
            <div class="col-xs-2"><input name="locksmith" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="locksmith_auth" type="checkbox" /> </div>      
        </div>


        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Redes</div>
            <div class="col-xs-2"><input name="wires" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="wires_auth" type="checkbox" /> </div>      
        </div>


        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Embalagem CNC</div>
            <div class="col-xs-2"><input name="packing_cnc" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="packing_cnc_auth" type="checkbox" /> </div>      
        </div>


        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Embalagem Espuma</div>
            <div class="col-xs-2"><input name="packing_espuma" type="checkbox" /> </div>
            <div class="col-xs-2"><input name="packing_espuma_auth" type="checkbox" /> </div>      
        </div>


        <div class="row col-xs-8 col-xs-offset-2" style="border-top: 1px solid #000000;">
            <div class="col-xs-8">Autorização</div>
            <div class="col-xs-2">-</div>      
            <div class="col-xs-2"><input name="auth" type="checkbox" /></div>

            <div>
                <button class="btn btn-success" id="palete_edit_check_overlay_ok" style="margin-top: 32px; margin-bottom: 24px;">FECHAR e ACTUALIZAR</button>

                <div id="palete_edit_check_overlay_ok_wait" class="bold" style="display: none;">
                    A actualizar Listagem. Aguarde.
                </div>

            </div> 

        </div>
    </div>
</div>

<div id="levantamento_palete" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Quer registar o Levantamento desta Palete? ( <span id="levantamento_palete_check_overlay_span" style="font-weight: bold;"></span> ) <br/>
        </p>
        <p>
            Código de Funcionário: <input name="levantamento_palete_obs_input" id="levantamento_palete_obs_input" type="password"/> Quantidade: <select id="levantamento_palete_quantidade"></select>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="levantamento_palete_yes" rel="">SIM</button>
            <button class="btn btn-warning" id="levantamento_palete_no">NÃO</button>
        </p>
    </div>
</div>

<!-- /.row -->

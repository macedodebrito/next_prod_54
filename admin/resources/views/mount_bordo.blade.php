<?php
//$total = DB::table('requests')->sum('value');
//$maquinados = DB::table('request_users')->sum('value');
//$por_maquinar = $total-$maquinados;
//$percent_1 = round(100*$por_maquinar/$total, 2);
//$percent_2 = round(100*$maquinados/$total, 2);
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-maroon"><i class="fa fa-desktop"></i></span>

            <div class="info-box-content">
                <h1>PAINEL DE BORDO</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- /.col -->
    <div class="col-md-5 col-sm-5 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-sign-language"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><b>MONTAGEM</b></span>
                <hr/>  
                <span class="info-box-text"><b>TOTAL:</b> {{ DB::table('paint_users')->sum('value') }}</span>
            </div>
        </div>        
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/mount') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-chevron-left"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">VOLTAR À MONTAGEM</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->

</div>

<div class="row"> 
    {!! 
    AdminSection::getmodel('App\Model\MOUNT_bordo')->fireDisplay() 
    !!}
</div>

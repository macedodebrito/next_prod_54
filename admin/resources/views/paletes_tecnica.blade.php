<?php
//$total = DB::table('requests')->sum('value');
//$maquinados = DB::table('request_users')->sum('value');
//$por_maquinar = $total-$maquinados;
//$percent_1 = round(100*$por_maquinar/$total, 2)
//$percent_2 = round(100*$maquinados/$total, 2);
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-barcode"></i></span>

            <div class="info-box-content">
                <h1>PALETES (TÉCNICA)</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- /.col -->

    <!-- /.col -->
    <div class="col-xs-offset-5 col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/view_paletes_arquivo_tecnica') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-archive"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ARQUIVADO</span>
                    <br/>(TERMINADAS)
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <!-- /.col -->

</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>JÁ SEPARADO</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\TechPaleteStep2')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>POR SEPARAR</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\TechPaleteStep1')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>

<div id="obs_palete" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Adicione ou Edite Observações da Palete <span id="obs_palete_span" style="font-weight: bold;"></span> <br/>
        </p>
        <p>
            <input name="palete_obs_input" id="palete_obs_input" type="text" style="width: 80%;"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="obs_palete_yes" rel="">GRAVAR</button>
            <button class="btn btn-red" id="obs_palete_delete" rel="">APAGAR</button>
            <button class="btn btn-warning" id="obs_palete_no">CANCELAR</button>
        </p>
    </div>
</div>

<div id="nxt_phc_palete" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Edite os Códigos para o ID <span id="nxt_phc_palete_span" style="font-weight: bold;"></span> <br/>
        </p>
        <p>
           NXT (Por defeito): <input id="nxt_phc_default_palete_obs_input" type="text" disabled style="width: 100px;"/> NSP: <input name="nxt_phc_palete_obs_input" id="nxt_phc_palete_obs_input" type="text" style="width: 100px;"/> / <input name="nxt_phc_palete_obs_input2" id="nxt_phc_palete_obs_input2" type="text" style="width: 100px;"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="nxt_phc_palete_yes" rel="">ACTUALIZAR</button>
            <button class="btn btn-red" id="nxt_phc_palete_delete" rel="">REMOVER NSP</button>
            <button class="btn btn-warning" id="nxt_phc_palete_no">CANCELAR</button>
        </p>
    </div>
</div>
<!-- /.row -->

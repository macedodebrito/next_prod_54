<?php

use Carbon\Carbon;

DB::statement("TRUNCATE TABLE phc_semanas");
DB::statement("TRUNCATE TABLE phc_semanas_products");

$server_port = "192.168.1.10";
$port = "1433";

$ser = $server_port; #the name of the SQL Server
$db = "cvagroup"; #the name of the database
$user = "cva"; #a valid username
$pass = "cva295"; #a password for the username
$instance = "PHC";

/* 
$connectionInfo = array(
    "Database"=>"cvagroup",
    "UID" => "cva",
    "PWD" => "cva295",
);
$conn = sqlsrv_connect("192.168.1.10", $connectionInfo);
if ($conn === false) {
    echo "Could not connect.\n";
    die(var_dump(sqlsrv_errors(), true));
}
$stmt = sqlsrv_query($conn, "(SELECT 'open' AS status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano NOT IN (SELECT u_orobrano FROM bo WHERE ndos=48) AND ndos=1 AND fechada=0) UNION (SELECT 'done' AS status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano IN (SELECT u_orobrano FROM bo WHERE ndos=48 AND fechada=0)AND ndos=1 AND fechada=1) UNION (SELECT 'picking' as status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano IN (SELECT u_orobrano FROM bo WHERE ndos=48 and fechada=0) and ndos=1 and fechada=0)");
if($stmt === false) {
    die( var_dump( sqlsrv_errors(), true) );
}

while( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {
    var_dump($row);
}
 */


//echo phpinfo(); exit;
//$conn = new PDO("odbc:sqlsrv") ;
$conn = odbc_connect("Driver={".env('ODBC_DRIVER')."};Server=$server_port;Instance=$instance;Database=$db;Client_CSet=UTF-8;String Types=Unicode;UID=$user ;PWD=$pass", $user, $pass);
//exit;

//$sql = "SELECT obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE nmdos='Encomenda de Cliente' AND fechada=0";
$sql = "(SELECT 'open' AS status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano NOT IN (SELECT u_orobrano FROM bo WHERE ndos=48) AND ndos=1 AND fechada=0) UNION (SELECT 'done' AS status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano IN (SELECT u_orobrano FROM bo WHERE ndos=48 AND fechada=0)AND ndos=1 AND fechada=1) UNION (SELECT 'picking' as status, u_etqprint, obrano, nome, vendnm, datafinal, u_matcomp, u_packing, u_packurl, etotaldeb FROM bo WHERE obrano IN (SELECT u_orobrano FROM bo WHERE ndos=48 and fechada=0) and ndos=1 and fechada=0)";
$rs = odbc_exec($conn, $sql);

$ESTE_ANO = date("Y");
$ANO_ANTERIOR = date("Y") - 1;

while (odbc_fetch_row($rs)) {
    $PP = trim(odbc_result($rs, "obrano"));
    $CLIENT = trim(odbc_result($rs, "nome"));
    $VENDEDOR = trim(odbc_result($rs, "vendnm"));
    $MATERIALS = trim(odbc_result($rs, "u_matcomp"));
    $PACKING = trim(odbc_result($rs, "u_packing"));
    $ROTULO = trim(odbc_result($rs, "u_etqprint"));
    $URL = trim(odbc_result($rs, "u_packurl"));
    $DATA1 = trim(odbc_result($rs, "datafinal"));
    $TOTAL = trim(odbc_result($rs, "etotaldeb"));
    $STATUS = trim(odbc_result($rs, "status"));
    $DATA = new Carbon($DATA1);
    $WEEK = $DATA->weekOfYear;
    $YEAR = $DATA->year;

    $CHECK_NEW_DATE = DB::table("phc_semanas_info")->where("pp", $PP)->first();
    if ((count($CHECK_NEW_DATE) > 0 && ($CHECK_NEW_DATE->new_date))) {
        $new_date = $CHECK_NEW_DATE->new_date;
        $DATA2 = new Carbon($new_date);
        $WEEK = $DATA2->weekOfYear;
        $YEAR = $DATA2->year;
    } else {
        $new_date = $DATA;
    }
    \App\Model\Phc_Semanas::insert(array('id' => null, 'pp' => $PP, 'u_etqprint' => $ROTULO, 'vendedor' => $VENDEDOR, 'cliente' => $CLIENT, 'data' => $DATA, 'week' => $WEEK, 'year' => $YEAR, 'created_at' => Carbon::now()->toDateTimeString(), 'updated_at' => Carbon::now()->toDateTimeString(), 'u_matcomp' => $MATERIALS, 'u_packing' => $PACKING, 'u_packing_url' => $URL, 'new_date' => $new_date, 'total' => $TOTAL, 'status' => $STATUS));
}
$SQL3 = DB::table("phc_semanas")->get();
foreach ($SQL3 as $value) {
    $sql2 = "SELECT bi.qtt, bi.design, bi.cativo FROM bi, bo WHERE bi.bostamp=bo.bostamp AND bo.obrano=$value->pp AND bi.qtt>0 AND bo.ndos=1";
    $rs2 = odbc_exec($conn, $sql2);
    while (odbc_fetch_row($rs2)) {
        $PRODUCT = trim(odbc_result($rs2, "design"));
        $QUANTITY = trim(odbc_result($rs2, "qtt"));
        $CATIVO = trim(odbc_result($rs2, "cativo"));
        \App\Model\Phc_Semanas_Products::insert(array('id' => null, 'pp' => $value->pp, 'product_name' => $PRODUCT, 'quantity' => $QUANTITY, 'cativo' => $CATIVO));
    }
}

odbc_close($conn);

function getStartAndEndDate($week, $year) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result['start_date'] = $dateTime->format('d-M-Y');
    $dateTime->modify('+6 days');
    $result['end_date'] = $dateTime->format('d-M-Y');
    return $result;
}

function getStartAndEndDateRange($week, $year) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result['start_date'] = $dateTime->format('d-M');
    $dateTime->modify('+4 days');
    $result['end_date'] = $dateTime->format('d-M');
    return $result['start_date'] . " - " . $result['end_date'];
}

function getProperYearAndWeek($week, $year) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result['start_date'] = $dateTime->format('d-M-Y');
    $dateTime->modify('+4 days');
    $result['end_date'] = $dateTime->format('d-M-Y');

    $year = $dateTime->format('Y');
    return $year;
}

function get_products_list($pp) {
    $SQL = DB::table("phc_semanas_products")->orderBy("product_name", "asc")->where("pp", $pp)->get();
    $DIV = "";
    foreach ($SQL as $value) {
        if ($value->cativo == 1) {
            $RESERVED = "<i class='fa fa-check-circle' style='margin-right: 4px; color: #008800'></i>";
        } else {
            $RESERVED = "<i class='fa fa-times-circle' style='margin-right: 4px; color: #FF0000'></i>";
        }
        $DIV .= "<div class='product_list_class'><span style='display: inline-block; width: 50px; padding: 2px 2px 2px 2px; background-color: #666; text-align: center; margin-right: 4px;'>$value->quantity</span> $RESERVED " . htmlspecialchars($value->product_name) . "</div>";
    }
    return $DIV;
}

function get_sum_week($loop) {
    $WEEK = date("W") + $loop;
    $SQL = DB::table("phc_semanas")->where("year", date("Y"))->where("week", $WEEK)->get();
    $total = 0;
    foreach ($SQL as $value) {
        $total = $total + $value->total;
    }
    return number_format($total, 2, ",", ".");
}

function get_sum_day($loop, $W, $D) {
    if ($loop == 0) {
        $MIN = "<";
    } else {
        $MIN = "=";
    }

    $SQL = DB::table("phc_semanas")
            ->where("new_date", "$MIN", $D)
            ->get();
    $total = 0;
    foreach ($SQL as $value) {
        $total = $total + $value->total;
    }
    return number_format($total, 2, ",", ".");
}

function check_products_list($pp) {
    $SQL = DB::table("phc_semanas_products")->orderBy("product_name", "asc")->where("pp", $pp)->get();
    $CHECK = 0;
    foreach ($SQL as $value) {
        if ($value->cativo == 1) {
            $CHECK = $CHECK + 0;
        } else {
            $CHECK = $CHECK + 1;
        }
    }
    return $CHECK;
}

function semanas($loop) {
    $WEEK = date("W") + $loop;

    $proper = getProperYearAndWeek($WEEK, date("Y"));

    if ($proper != date("Y")) {
        if ($WEEK >= 53) {
            $WEEK = $loop;
        }
    }
//    $SQL = DB::table("phc_semanas")->orderBy("new_date", "asc")->where("year", date("Y"))->where("week", $WEEK)->get();
    $SQL = DB::table("phc_semanas")->orderBy("new_date", "asc")->where("year", $proper)->where("week", $WEEK)->get();
    foreach ($SQL as $value) {
        $LOCKED = DB::table("phc_semanas_info")->where("pp", $value->pp)->where('locked', 1)->first();
        if (count($LOCKED) > 0) {
            $LOCKED_ICON = '<i class="lock_again fa fa-unlock" style="float: right; margin-top: 3px;"></i>';
            $CSS = "#33cc33";
        } else {
            $LOCKED_ICON = '<i class="lock_unlock fa fa-lock" style="float: right; margin-top: 3px;"></i>';
            $CSS = "#CCCCCC";
        }

        $DATA = explode(" ", $value->new_date);
        $DATA = explode("-", $DATA[0]);
        if (($value->u_matcomp == "1") || (check_products_list($value->pp) == "0")) {
            $M_ICON = '<span style="font-size: 12px; color: #008800;">Material <i class="fa fa-check-square-o" aria-hidden="true"></i></span>';
        } else {
            $M_ICON = '<span style="font-size: 12px; color: #FF0000;">Material <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        switch ($value->status) {
            case 'picking': $C_ICON = '<span class="badge" style="font-size: 12px; background-color: orange; color: white;">Picking em Curso <i class="fa fa-clock-o" aria-hidden="true"></i></span>';
                break;
            case 'done': $C_ICON = '<span class="badge" style="font-size: 12px; background-color: #008800; color: white;">Picking Efectuado <i class="fa fa-check-circle" aria-hidden="true"></i></span>';
                break;
            default: $C_ICON = '<span class="badge" style="font-size: 12px; background-color: red; color: white;">Encomenda em Aberto <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        switch ($value->u_etqprint) {
            case '1': $R_ICON = '<span style="font-size: 12px; color: #008800;">Rótulos <i class="fa fa-check-square-o" aria-hidden="true"></i></span> ';
                break;
            default: $R_ICON = '<span style="font-size: 12px; color: #FF0000;">Rótulos <i class="fa fa-square-o" aria-hidden="true"></i></span> ';
        }

        if ($value->u_packing_url) {
            $U_ICON = ' | <a href="./downloads/packing/' . $value->pp . '" target="_blank"><span style="font-size: 12px; color: #008800;">Packing List <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>';
        } else {
            $U_ICON = '<span style="font-size: 12px; color: #FF0000;">Packing List <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        $OLD_DATA = explode(" ", $value->data);
        $OLD_DATA = explode("-", $OLD_DATA[0]);
        if ($value->data != $value->new_date) {
            $datework = Carbon::parse($value->data);
            $now = Carbon::parse($value->new_date);
            $testdate = $now->diffInDays($datework, false);

            if ($testdate < 0) {
                $testdate = "Atraso de " . substr($testdate, 1) . " dias";
                $testdate_css = "orange";
            } else if ($testdate >= 0) {
                $testdate = "Antecipada $testdate dias";
                $testdate_css = "#338833";
            }
            $PHC_DATE = "($OLD_DATA[2]/$OLD_DATA[1]/$OLD_DATA[0])<div style=\"color: $testdate_css;\">($testdate)</div>";
        } else {
            $PHC_DATE = "";
        }

        if (($value->u_matcomp == "1") && (check_products_list($value->pp) != "0")) {
            $plus_css = "orange";
        } else {
            $plus_css = "#000000";
        }
        $PRODUCT_LIST = "<i data-toggle=\"tooltip\" data-placement=\"auto\" title=\"" . get_products_list($value->pp) . "\" style=\"color: $plus_css; float: left; margin-left: 6px; margin-top: 3px\" class=\"open_product_list fa fa-plus\"></i>";

        $NO_MONEY = "";
        if ($value->total == "0") {
            $NO_MONEY = "(<span style=\"color: #FF0000; font-size: 12px;\" class=\"fa fa-eur\"></span>)";
        }

        $NEW_DATE = "<div><span style=\"font-size: 12px; color: #666;\">$PHC_DATE</span></div>";
        echo "<div class=\"logistic-pp\" style=\"font-size: 16px;\"><div style=\"padding: 6px; margin-bottom: 6px; background-color: $CSS;\">$PRODUCT_LIST $NO_MONEY <span class=\"pp_info_nr\">$value->pp</span> $LOCKED_ICON</div><div><span>$DATA[2]/$DATA[1]/$DATA[0]</span> <i class=\"schedule_phc fa fa-calendar\" style=\"float: right; margin-right: 6px; margin-top: 3px;\"></i></div>$NEW_DATE";
        echo "<div style=\"padding: 12px;\">$value->cliente</div>";
        echo "<div style=\"font-size: 12px; color: #666; margin-bottom: 6px;\">($value->vendedor)</div>";
        echo "$M_ICON $U_ICON | $R_ICON";
        echo "<br/>$C_ICON ";
        echo "<hr/></div>";
    }
}

function dias($loop, $W, $D, $ICON) {
    if ($loop == 0) {
        $MIN = "<";
        $DAY = "<span style=\"color: #FF0000;\">Atraso</span> $ICON";
    } else {

        $MIN = "=";
        if ($loop == 1)
            $WD = "Segunda";
        if ($loop == 2)
            $WD = "Terça";
        if ($loop == 3)
            $WD = "Quarta";
        if ($loop == 4)
            $WD = "Quinta";
        if ($loop == 5)
            $WD = "Sexta";
        $DAY = "<span style=\"color: #FF0000;\">$W</span> | <span style=\"color: #666;\">$WD</span> $ICON";
    }

    $SQL = DB::table("phc_semanas")
            ->orderBy("new_date", "asc")
            ->where("new_date", "$MIN", $D)
            ->get();

    echo "<div>$DAY </div>";
    echo "<div style=\"padding-top: 5px; color: #666666; font-size: 12px;\">Estimado: €" . get_sum_day($loop, $W, $D) . "</div>";
    echo "<hr/>";

    foreach ($SQL as $value) {
        $LOCKED = DB::table("phc_semanas_info")->where("pp", $value->pp)->where('locked', 1)->first();

        if (count($LOCKED) > 0) {
            $LOCKED_ICON = '<i class="lock_again fa fa-unlock" style="float: right; margin-top: 3px;"></i>';
            $CSS = "#33cc33";
        } else {
            $LOCKED_ICON = '<i class="lock_unlock fa fa-lock" style="float: right; margin-top: 3px;"></i>';
            $CSS = "#CCCCCC";
        }

        $DATA = explode(" ", $value->new_date);
        $DATA = explode("-", $DATA[0]);
        if (($value->u_matcomp == "1") || (check_products_list($value->pp) == "0")) {
            $M_ICON = '<span style="font-size: 12px; color: #008800;">Material <i class="fa fa-check-square-o" aria-hidden="true"></i></span>';
        } else {
            $M_ICON = '<span style="font-size: 12px; color: #FF0000;">Material <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        switch ($value->status) {
            case 'picking': $C_ICON = '<span class="badge" style="font-size: 12px; background-color: orange; color: white;">Picking em Curso <i class="fa fa-clock-o" aria-hidden="true"></i></span>';
                break;
            case 'done': $C_ICON = '<span class="badge" style="font-size: 12px; background-color: #008800; color: white;">Picking Efectuado <i class="fa fa-check-circle" aria-hidden="true"></i></span>';
                break;
            default: $C_ICON = '<span class="badge" style="font-size: 12px; background-color: red; color: white;">Encomenda em Aberto <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        if ($value->u_packing == "1") {
            $P_ICON = '<span style="font-size: 12px; color: #008800;">Packing List <i class="fa fa-check-square-o" aria-hidden="true"></i></span>';
        } else {
            $P_ICON = '<span style="font-size: 12px; color: #FF0000;">Packing List <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        if ($value->u_packing_url) {
            $U_ICON = ' | <a href="./downloads/packing/' . $value->pp . '" target="_blank"><span style="font-size: 12px; color: #008800;">Packing List <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>';
        } else {
            $U_ICON = '<span style="font-size: 12px; color: #FF0000;">Packing List <i class="fa fa-square-o" aria-hidden="true"></i></span>';
        }

        switch ($value->u_etqprint) {
            case '1': $R_ICON = '<span style="font-size: 12px; color: #008800;">Rótulos <i class="fa fa-check-square-o" aria-hidden="true"></i></span> ';
                break;
            default: $R_ICON = '<span style="font-size: 12px; color: #FF0000;">Rótulos <i class="fa fa-square-o" aria-hidden="true"></i></span> ';
        }
        $OLD_DATA = explode(" ", $value->data);
        $OLD_DATA = explode("-", $OLD_DATA[0]);
        if ($value->data != $value->new_date) {
            $datework = Carbon::parse($value->data);
            $now = Carbon::parse($value->new_date);
            $testdate = $now->diffInDays($datework, false);
            if ($testdate < 0) {
                $testdate = "Atraso de " . substr($testdate, 1) . " dias";
                $testdate_css = "orange";
            } else if ($testdate >= 0) {
                $testdate = "Antecipada $testdate dias";
                $testdate_css = "#338833";
            }
            $PHC_DATE = "($OLD_DATA[2]/$OLD_DATA[1]/$OLD_DATA[0])<div style=\"color: $testdate_css;\">($testdate)</div>";
        } else {
            $PHC_DATE = "";
        }

        $NO_MONEY = "";
        if ($value->total == "0") {
            $NO_MONEY = "(<i style=\"color: #FF0000; font-size: 12px;\" class=\"fa fa-eur\"></i>)";
        }

        if (($value->u_matcomp == "1") && (check_products_list($value->pp) != "0")) {
            $plus_css = "orange";
        } else {
            $plus_css = "#000000";
        }
        $PRODUCT_LIST = "<i data-toggle=\"tooltip\" data-placement=\"auto\" title=\"" . get_products_list($value->pp) . "\" style=\"color: $plus_css; float: left; margin-left: 6px; margin-top: 3px\" class=\"open_product_list fa fa-plus\"></i>";

        $NEW_DATE = "<div><span style=\"font-size: 12px; color: #666;\">$PHC_DATE</span></div>";
        echo "<div class=\"logistic-pp\" style=\"font-size: 16px;\"><div style=\"padding: 6px; margin-bottom: 6px; background-color: $CSS;\">$PRODUCT_LIST $NO_MONEY <span class=\"pp_info_nr\">$value->pp</span> $LOCKED_ICON</div><div><span>$DATA[2]/$DATA[1]/$DATA[0]</span> <i class=\"schedule_phc fa fa-calendar\" style=\"float: right; margin-right: 6px; margin-top: 3px;\"></i></div>$NEW_DATE";
        echo "<div style=\"padding: 12px;\">$value->cliente</div>";
        echo "<div style=\"font-size: 12px; color: #666;\">($value->vendedor)</div>";
        echo "$M_ICON $U_ICON | $R_ICON";
        echo "<br/>$C_ICON";
        echo "<hr/></div>";
    }
//    }
}
?>
<div class="">
    <div id="distributor_carrousel" class="carousel slide" data-ride="carousel"  data-interval="0">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#distributor_carrousel" data-slide-to="0" class="active"></li>
            <li data-target="#distributor_carrousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <!-- Info boxes -->
                <div class="row">
                    <!-- /.col -->

                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="info-box">
                            <span class="info-box-icon bg-lime"><i class="fa fa-hashtag"></i></span>

                            <div class="info-box-content">
                                <h1>ORGANIZAÇÃO DIÁRIA</h1>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>

                    <div class="col-sm-offset-4 col-md-4 col-xs-6">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-search"></i></span>
                            <div class="info-box-content">
                                <div style="padding-top: 7px; padding-bottom: 10px;"><strong>Pesquisar</strong></div>
                                <input type="text" class="form-control input-group search-logistic"> 
                            </div>
                        </div>
                    </div> 

                </div>

                <div class="row">
                    @for ($i = 0; $i < 6; $i++)
                    <?php
                    $ICON = '<i class="p-l-30 fa fa-chevron-right"></i>';
                    if ($i == 0) {
                        $W = 0;
                        $D = date('Y-m-d H:i:s', strtotime("monday this week"));
                    } else {
                        $x = $i - 1;
                        $W = date('d', strtotime("monday this week + $x days"));
                        $D = date('Y-m-d H:i:s', strtotime("monday this week + $x days"));
                    }
                    ?>
                    <div class="col-md-2">
                        <div class="text-center info-box bold p-t-12">
                            <?php echo dias($i, $W, $D, $ICON); ?>
                        </div>        
                    </div>
                    @endfor
                </div>                
            </div>

            <div class="item">
                <!-- Info boxes -->
                <div class="row">
                    <!-- /.col -->

                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="info-box">
                            <span class="info-box-icon bg-lime"><i class="fa fa-hashtag"></i></span>

                            <div class="info-box-content">
                                <h1>PROXIMAS SEMANAS</h1>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>

                    <div class="col-sm-offset-4 col-md-4 col-xs-6">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-search"></i></span>
                            <div class="info-box-content">
                                <div style="padding-top: 7px; padding-bottom: 10px;"><strong>Pesquisar</strong></div>
                                <input type="text" class="form-control input-group search-logistic"> 
                            </div>
                        </div>
                    </div> 

                </div>

                <div class="row">
                    @for ($i = 1; $i < 7; $i++)
                    <?php
                    $D = date("W");
                    $Y = date("Y");
                    $S = $D + $i;

                    if ($D == 52 & $S > $D) {
                        $S = $i;
                    } else {
                        $S = $D;
                    }
                    ?>
                    <div class="col-md-2">
                        <div class="text-center info-box bold p-t-12">
                            <?php echo "<span style=\"color: #FF0000;\">$S</span> | <span style=\"color: #666;\">" . getStartAndEndDateRange($D + $i, $Y) . "</span>"; ?>                            
                            <i class="p-l-30 fa fa-chevron-right"></i>
                            <div style="padding-top: 5px; color: #666666; font-size: 12px;">Estimado: €<?= get_sum_week($i) ?></div>
                            <hr/>
                            <?php semanas($i); ?>
                        </div>        
                    </div>
                    @endfor
                </div>  
            </div>             

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#distributor_carrousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#distributor_carrousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div id="overlay_locked" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Pretende <span id="text_lock_unlock"></span> a Encomenda  <span id="locked_pp" style="font-weight: bold;"></span>? <br/>
        </p>
        <p>
            Código: <input name="published" id="published" type="password" style="width: 150px"/> <input id="auth_id_code" type="hidden"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="overlay_locked_yes" rel=""><span id="text_lock_unlock_caps"></span></button>
            <button class="btn btn-warning" id="overlay_locked_no">CANCELAR</button>
        </p>
    </div>
</div>

<div id="overlay_schedule" style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .85 ); z-index: 9999;">
    <div style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        <p>
            Pretende Alterar a data: <span id="schedule_pp" style="font-weight: bold;"></span>? <br/>
        </p>
        <p>
            <select id="schedule_date_day"><?php
                $x = 1;
                while ($x <= 31) {
                    if ($x < 10) {
                        $y = "0$x";
                    } else {
                        $y = $x;
                    } echo "<option value=\"$y\">$y</option>";
                    $x++;
                }
                ?></select> / <select id="schedule_date_month"><?php
                $x = 1;
                while ($x <= 12) {
                    if ($x < 10) {
                        $y = "0$x";
                    } else {
                        $y = $x;
                    } echo "<option value=\"$y\">$y</option>";
                    $x++;
                }
                ?></select> / <select id="schedule_date_year"><?php
                    $x = date("Y");
                    while ($x <= date("Y") + 2) {
                        echo "<option value=\"$x\">$x</option>";
                        $x++;
                    }
                    ?></select>
        </p>
        <p>
            Código: <input name="schedule_published" id="schedule_published" type="password" style="width: 150px"/> <input id="schedule_auth_id_code" type="hidden"/> <input id="schedule_pp_hidden" type="hidden"/>
        </p>
        <br/>
        <p>
            <button class="btn btn-success" id="overlay_schedule_yes" rel="">ALTERAR DATA</button>
            <button class="btn btn-warning" id="overlay_schedule_no">CANCELAR</button>
        </p>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.lock_unlock').on('click', function (e) {
            $("#overlay_locked").show();
            $("#overlay_locked_yes").prop('disabled', true);
            $("#overlay_locked_yes").attr('rel', 'unlock');
            $("#locked_pp").text($(this).prev("span").text());
            $("#text_lock_unlock").text("Desbloquear");
            $("#text_lock_unlock_caps").text("DESBLOQUEAR");
            $("#published").val("");
        });
        $('.lock_again').on('click', function (e) {
            $("#overlay_locked").show();
            $("#overlay_locked_yes").prop('disabled', true);
            $("#overlay_locked_yes").attr('rel', 'lock');
            $("#locked_pp").text($(this).prev("span").text());
            $("#text_lock_unlock").text("Voltar a Bloquear");
            $("#text_lock_unlock_caps").text("BLOQUEAR");
            $("#published").val("");
        });
        $(document).on('click', 'button#overlay_locked_no', function (e) {
            $("#locked_pp").text('');
            $("#overlay_locked").hide();
            $("#overlay_locked_yes").prop('disabled', true);
        });

        $("#overlay_locked_yes").on('click', function (e) {
            $("#overlay_locked_yes").prop('disabled', true);
            var myURL = parseUri(window.location.href);
            $.ajax({
                type: "GET",
                url: myURL['pathname'].split("/admin")[0] + '/admin/unlock_pp/' + $("#locked_pp").text() + '/' + $("#auth_id_code").val() + '/' + $("button#overlay_locked_yes").attr('rel'),
                success: function (data) {
                    location.reload();
                },
                beforeSend: function () {
                    $("#overlay_locked_no").click();
                    $('#overlay_waiting').show();
                }
            });
            e.preventDefault();
        });

        $('#published').delayKeyup(function () {

            var myURL = parseUri(window.location.href);

            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#published").val(),
                success: function (data) {
                    if (data.id != 0) {
                        $("#overlay_locked_yes").prop('disabled', false);
                        $("#auth_id_code").val(data.id);
                    }
                    else {
                        $("#overlay_locked_yes").prop('disabled', true);
                        $("#auth_id_code").val('');
                    }
                }
            });

        }, 1000);

        $('.schedule_phc').on('click', function (e) {
            $("#overlay_schedule").show();
            $("#overlay_schedule_yes").prop('disabled', true);
            $("#schedule_pp").text($(this).prev("span").text());
            $("#schedule_pp_hidden").val($(this).parent("div").prev("div").children(".pp_info_nr").text());
            $("#schedule_published").val("");
            var actual_date = $("#schedule_pp").text().split("/");
            $("#schedule_date_day").val(actual_date[0]);
            $("#schedule_date_month").val(actual_date[1]);
            $("#schedule_date_year").val(actual_date[2]);
        });

        $(document).on('click', 'button#overlay_schedule_no', function (e) {
            $("#schedule_pp").text('');
            $("#schedule_pp_hidden").val('');
            $("#schedule_published").val('');
            $("#overlay_schedule").hide();
            $("#overlay_schedule_yes").prop('disabled', true);
        });

        $('#schedule_published').delayKeyup(function () {

            var myURL = parseUri(window.location.href);

            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#schedule_published").val(),
                success: function (data) {
                    if (data.id != 0) {
                        $("#overlay_schedule_yes").prop('disabled', false);
                        $("#schedule_auth_id_code").val(data.id);
                    }
                    else {
                        $("#overlay_schedule_yes").prop('disabled', true);
                        $("#schedule_auth_id_code").val('');
                    }
                }
            });

        }, 1000);

        $("#overlay_schedule_yes").on('click', function (e) {
            $("#overlay_schedule_yes").prop('disabled', true);
            var myURL = parseUri(window.location.href);
            $.ajax({
                type: "GET",
                url: myURL['pathname'].split("/admin")[0] + '/admin/schedule_new_date/' + $("#schedule_pp_hidden").val() + '/' + $("#schedule_auth_id_code").val() + '/' + $("#schedule_date_day option:selected").val() + '/' + $("#schedule_date_month option:selected").val() + '/' + $("#schedule_date_year option:selected").val(),
                success: function (data) {
                    location.reload();
                },
                beforeSend: function () {
                    $("#overlay_locked_no").click();
                    $('#overlay_waiting').show();
                }
            });
            e.preventDefault();
        });

    });
    $(document).ready(function () {

        $('input.search-logistic').delayKeyup(function () {
            var searched = $(this).val();
            if (searched) {
                $("input.search-logistic").val(searched)
                $('.logistic-pp').show();
                $('.logistic-pp:not(:icontains(' + searched + '))').hide();
            }
            else {
                $('.logistic-pp').show();
            }
        });

        $('[data-toggle="tooltip"]').tooltip({html: true, trigger: 'click', container: 'body'});
    });
</script>
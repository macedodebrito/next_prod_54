
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-lime"><i class="fa fa-hashtag"></i></span>

            <div class="info-box-content">
                <h1>REDES</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    
    <div class="col-sm-offset-7 col-md-1 col-sm-1 col-xs-6">
        <a href="{{ url('/admin/wires/history/create') }}">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>
            <!-- /.info-box-content -->
        </div>
        </a>
    </div> 
    
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
            <br/>
            <div class="info-box-content">
                <span class="info-box-text"><b>HISTÓRICO</b></span>
            </div>
            
            <hr>
            {!! 
            AdminSection::getmodel('App\Model\WIRES_History')->fireDisplay() 
            !!}
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->

</div>

<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>
<!-- /.row -->


<!-- /.row -->

<?php
//$total = DB::table('requests')->sum('value');
//$maquinados = DB::table('request_users')->sum('value');
//$por_maquinar = $total-$maquinados;
//$percent_1 = round(100*$por_maquinar/$total, 2);
//$percent_2 = round(100*$maquinados/$total, 2);
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->
    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-barcode"></i></span>

            <div class="info-box-content">
                <h1>PALETES (TÉCNICA)</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <!-- /.col -->

    <!-- /.col -->
    <div class="col-xs-offset-5 col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/tecnica/paletes_history') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-archive"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ARQUIVADO</span>
                    <br/>(TERMINADAS)
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <!-- /.col -->

</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-list-ol"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEPARADAS</b></span>
                    <hr/>
                    <span class="info-box-text">-</span></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\TechPaleteList')->fireDisplay() 
            !!}   
        </div>        
    </div>
</div>
<!-- /.row -->

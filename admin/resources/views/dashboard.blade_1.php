<?php

use App\Model\Request;
use App\Model\RequestUser;
use App\Model\WoodUser;
use App\Model\PaintUser;
use App\Model\Product;

$check = DB::table('request_week')->orderBy('end_wood_at', 'asc')->get();

// lista de pedidos semanais por ordem
$SEMANAL = [];
$week = [];
//$total = 0;
//$total_req = 0;
//$total_week = 0;
//$total_req2 = 0;
//$dump = 0;
//            print_r("<pre>");
foreach ($check as $key => $instance) {
    $total = DB::table('request_week')->where('id', $instance->id)->sum('carpintaria');

    $total_week = DB::table('request_week')->where('product_id', $instance->product_id)->sum('carpintaria');

    $total_req = Request::where('product_id', $instance->product_id)->first();

    //$total_wood = WoodUser::where('request_id', $total_req->id)->get();

    $total_req2 = Request::where('product_id', $instance->product_id)->get();
    $total_inc = 0;
    foreach ($total_req2 as $go) {
        $dump = WoodUser::where('request_id', $go->id)->sum('value') - PaintUser::where('request_id', $go->id)->sum('value');
        $total_inc = $total_inc + $dump;
    }
    //print_r($total_inc);

    $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
    $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
    $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
    $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;

    foreach (DB::table('request_week')->where('product_id', $instance->product_id)->get() as $key => $value) {
        $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
        $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
    }
}
//            print_r($SEMANAL);
foreach ($SEMANAL as $key2 => $value2) {
    foreach ($value2['semana'] as $key3 => $value3) {
        if ($SEMANAL[$key2]['total_montado'] > 0) {
            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
            }
            //print_r($SEMANAL[$key2]['semana'][$key3]);
        }
        if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
            $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
        }
    }
}
$carpintaria_semanal = DB::table('request_week')->whereIn('id', $week)->orderBy('end_at', 'asc')->sum('carpintaria');
$carpintaria_disponivel = RequestUser::sum('value') - WoodUser::sum('value');
$carpintaria_saida = WoodUser::sum('value');

if ($carpintaria_disponivel > 0) {
$carpintaria_percent_1 = round(100 * $carpintaria_semanal / ($carpintaria_disponivel+$carpintaria_semanal), 2);
$carpintaria_percent_1 = 100 - $carpintaria_percent_1;
$carpintaria_percent_2 = round(100 * $carpintaria_saida / ($carpintaria_disponivel+$carpintaria_saida), 2);
}
else {
    $carpintaria_percent_1 = 100;
    $carpintaria_percent_2 = 0;
}
$total = DB::table('requests')->sum('value');
if ($total > 0) {
    $maquinados = DB::table('request_users')->sum('value');
    $por_maquinar = $total - $maquinados;
    $percent_1 = round(100 * $por_maquinar / $total, 2);
    $percent_2 = round(100 * $maquinados / $total, 2);
} else {
    $maquinados = 0;
    $por_maquinar = 0;
    $percent_1 = 100;
    $percent_2 = 0;
}
?>
<!-- Info boxes -->
<div class="row">
    
        <div class="col-md-offset-10 col-md-2 col-sm-2 col-xs-12">
        <a href="{{ url('/logout') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-sign-out"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">SAIR</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div> 
    <div class="col-md-6 col-sm-6 col-xs-12">

        <a href="{{ URL::to('admin/cnc') }}">

            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-cogs"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">CNC</span>
                    <hr>
                    <span class="info-box-text">PEDIDOS PARA PRODUÇÃO (PP): <b>{{ $total }}</b></span>
                    <hr>
                    <span class="info-box-text">POR MAQUINAR: <b>{{ $por_maquinar }} <span class="text-red">({{ $percent_1 }}%)</span></b></span>
                    <hr>
                    <span class="info-box-text">EXECUTADOS: <b>{{ $maquinados }} <span class="text-green">({{ $percent_2 }}%)</span></b></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>

    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-xs-12">

        <a href="{{ URL::to('admin/wood') }}">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>

                <div class="info-box-content">
                    <span class="info-box-number">CARPINTARIA</span>
                    <hr/>
                    <span class="info-box-text">DISPONIVEIS: <b>{{ $carpintaria_disponivel }}</b></span>
                    <hr/>
                    <span class="info-box-text">POR MONTAR: <b>{{ $carpintaria_semanal }} <span class="text-red">({{ $carpintaria_percent_1 }}%)</span></b></span>
                    <hr/>
                    <span class="info-box-text">MONTADOS: <b> {{ $carpintaria_saida }} <span class="text-green">({{ $carpintaria_percent_2 }}%)</span></b></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-6 col-sm-6 col-xs-12">

        <a href="{{ URL::to('admin/paint') }}">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-paint-brush"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PINTURA</span>
                    <span class="info-box-number">760</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-xs-12">

        <a href="{{ URL::to('admin/mount') }}">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-sign-language"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">MONTAGEM</span>
                    <span class="info-box-number">2,000</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->
</div>
<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>
<!-- /.row -->


<!-- /.row -->

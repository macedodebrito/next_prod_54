<?php

use App\Role;
use App\User;

$waiting = DB::table('prep_users')->where('published', 0)->sum('value');

$role_check = 0;
$role_id = Role::where('name', 'admin')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}
$role_id = Role::where('name', 'manager')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}
?>
<!-- Info boxes -->
<div class="row">
    <!-- /.col -->

    <div class="col-md-3 col-sm-3 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>

            <div class="info-box-content">
                <h1>CARPINTARIA</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>   

    <!-- /.col -->
    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/wood/open') }}">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-sign-in"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ENTRADAS<br/>(da PREPARAÇÃO)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col --> 

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ url('/admin/wood/total') }}">
            <div class="info-box">
                <span class="info-box-icon bg-grey"><i class="fa fa-cubes"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">DISPONÍVEL<br>(NA CARPINTARIA)</span>
                </div>
            </div>    
        </a>
    </div> 

    <!-- /.col -->
    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/wood_bordo') }}">
            <div class="info-box">
                <span class="info-box-icon bg-maroon"><i class="fa fa-desktop"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PP<br/>(EM ABERTO)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>
    <!-- /.col -->    

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ url('/admin/wood/history') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">SAÍDAS<br/>(Acabamento)</span>
                </div>
                <!--            {!! 
                            AdminSection::getmodel('App\Model\WOOD_history')->fireDisplay() 
                            !!}  -->
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <div class="col-md-1 col-sm-1 col-xs-6">
        <a href="{{ url('/admin/wood/history/create') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div> 

</div>

<div class="row ajax_waiting">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-circle-o-notch fa-spin"></i></span>
            <br/>
            <div class="info-box-content">
                <div class="col-xs-12 col-sm-7">
                    <span class="info-box-text"><b>EM VALIDAÇÃO (PELA CARPINTARIA) | EM ESPERA:</b> <span id="waiting_span">{{ $waiting }}</span></span>
                </div>
            </div>
            <hr>
            {!! 
            AdminSection::getmodel('App\Model\WOOD_open')
            ->fireDisplay()
            ->setApply(function($query) {
                $query->orderBy('published', 'asc')->where('published', 0)->orderBy('created_at', 'desc');
            })
            !!}
            <!-- /.info-box-content -->
        </div>        
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
            <br/>
            <div class="info-box-content">
                <div class="col-xs-12 col-sm-7">
                    <span class="info-box-text"><b>ENCOMENDAS DE CLIENTES</b></span>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <input id="picagem_serial" class="form-control col-xs-12" placeholder="Nrº de Série do Produto"/>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <input type="password" id="picagem_operador" class="form-control col-xs-12" disabled placeholder="Código Operador"/>
                </div>
            </div>
            <form id="picker_form">
                <div id="picagem_header" class="col-xs-12" style="border-bottom: 1px solid #ccc; padding-top:12px; padding-bottom: 12px;">
                    <div class="col-xs-2 bold">Data de Picagem</div>
                    <div class="col-xs-1 bold">Contador</div>
                    <div class="col-xs-2 bold">Produto</div>
                    <div class="col-xs-4 bold">Observação</div>
                    <div class="col-xs-2 bold">Operador</div>
                    <div class="col-xs-1 bold">Apagar</div>
                </div>

                <div class="col-xs-12" style="padding-top:12px; padding-bottom: 12px; border-bottom: 1px solid #ccc;">
                    <div class="col-xs-2">
                        <input name="validate_picker" class="btn btn-danger col-xs-12" value="Validar"/>
                    </div>
                    <div class="col-xs-5">
                        <input id="validate_picker_button" type="button" class="btn btn-success col-xs-12" disabled value="Dar saída dos produtos para o Acabamento"/>
                    </div>              
                </div>
            </form>

            <div class="col-xs-12" style="height: 20px; border-bottom: 1px solid #ccc; background-color: #ecf0f5;">
            </div>
            <div class="col-xs-12" style="height: 20px;">
            </div>
            <hr>
            {!! 
            AdminSection::getmodel('App\Model\WOOD_semanal_Order')->fireDisplay() 
            !!}
            <!-- /.info-box-content -->
        </div>        
    </div>
</div>
<div style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; z-index: 9999;" id="waiting_overlay_picker">
    <div id="waiting_overlay_picker_span" style = "position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        A processar.<br/>Por favor aguarde...
    </div>
</div>
<script>
    $(function () {
        var myDATA;
        $("#picagem_serial").focus();

        $('#picagem_serial').delayKeyup(function () {

            var myURL = parseUri(window.location.href);

            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_wood_picagem_serial/' + $("#picagem_serial").val(),
                success: function (data) {
                    if (data.id != 0) {
                        if ($(".div-header[rel='" + data.product_name + "" + data.custom_code.toUpperCase() + "']").length + 1 <= data.GET_PP_MAX) {
                            $("#picagem_operador").prop('disabled', false).focus();
                            myDATA = data;
                        }
                        else {
                            $("#picagem_operador").prop('disabled', true);
                            $("#picagem_operador").val("");
                            $("#picagem_serial").val("");
                            $("#picagem_serial").focus();
                        }
                    }
                    else {
                        $("#picagem_operador").prop('disabled', true);
                        $("#picagem_operador").val("");
                        $("#picagem_serial").val("");
                        $("#picagem_serial").focus();
                    }
                }
            });

        }, 1000);

        // CHECK PASSWORD
        $('#picagem_operador').delayKeyup(function () {

            var myURL = parseUri(window.location.href);

            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#picagem_operador").val(),
                success: function (data) {
                    if (data.id != 0) {
                        var player_data = data;
                        var data = myDATA;
                        var data_id = data.product_name + "" + data.custom_code.toUpperCase();
                        var next_nr = $(".div-header[rel='" + data_id + "']").length + 1;
                        var next = Date.now();
                        var d = new Date();
                        var picker_date = d.toISOString().slice(0, 10);
                        var picker_YYYY = picker_date.slice(0, 4);
                        var picker_MM = picker_date.slice(6, 7);
                        var picker_DD = picker_date.slice(9, 10);
                        var picker_time = d.toISOString().slice(11, 19);
                        var picker_date_time = picker_DD + '.' + picker_MM + '.' + picker_YYYY + ' ' + picker_time;
                        var timestamp_date = Date.now()/1000|0;
                        $('<div>').attr({
                            id: 'div_header_' + data_id + '_' + next,
                            class: 'div-header col-xs-12',
                            rel: data_id,
                            'data-rel': data_id + '_' + next,
                        }).insertAfter("#picagem_header").css({'padding-top': '12px', 'padding-bottom': '12px', 'background-color': '#faebcc'});
                        // tree
                        $('<div class="col-xs-2" id="label_pickerdate_' + data_id + '_' + next + '"></div><div class="col-xs-1" id="label_picker_' + data_id + '_' + next + '"></div><div class="col-xs-2" id="label_product_name_' + data_id + '_' + next + '"></div><div class="col-xs-4" id="label_obs_' + data_id + '_' + next + '"></div><div class="col-xs-2" id="label_player_' + data_id + '_' + next + '"></div><div class="col-xs-1" id="label_button_' + data_id + '_' + next + '"></div>').appendTo("#div_header_" + data_id + "_" + next);
                        // insert            

                        $('<input>').attr({
                            type: 'text',
                            id: 'pickerdate_' + data_id + '_' + next,
                            name: 'new_date[]',
                            class: 'form-control col-xs-2',
                            value: picker_date_time,
                            rel: data_id,
                            'data-timestamp': Date.now(),
                            disabled: 'disabled',
                        }).appendTo("#label_pickerdate_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'hidden',
                            id: 'data_' + data_id + '_' + next,
                            name: 'datas[]',
                            value: timestamp_date,
                        }).insertAfter("#pickerdate_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'hidden',
                            id: 'custom_id_' + data_id + '_' + next,
                            name: 'custom_ids[]',
                            value: data.custom_id,
                        }).insertAfter("#data_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'text',
                            id: 'picker_' + data_id + '_' + next,
                            name: 'new_picker[]',
                            class: 'form-control col-xs-1',
                            value: next_nr + "/" + data.GET_PP_MAX,
                            rel: data_id,
                            disabled: 'disabled',
                        }).appendTo("#label_picker_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'text',
                            id: 'product_name_' + data_id + '_' + next,
                            name: 'product_names[]',
                            class: 'form-control new_product_names',
                            value: data.product_name + " (" + data.custom_name + ")",
                            disabled: 'disabled',
                        }).appendTo("#label_product_name_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'hidden',
                            id: 'id_' + data_id + '_' + next,
                            name: 'ids[]',
                            value: data.product_id,
                        }).insertAfter("#product_name_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'text',
                            id: 'obs_' + data_id + '_' + next,
                            name: 'obs[]',
                            class: 'form-control new_obs',
                        }).appendTo("#label_obs_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'text',
                            id: 'player_' + data_id + '_' + next,
                            name: 'players[]',
                            class: 'form-control new_players',
                            value: player_data.name,
                            disabled: 'disabled',
                        }).appendTo("#label_player_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'hidden',
                            id: 'auth_' + data_id + '_' + next,
                            name: 'auths[]',
                            value: player_data.id,
                        }).insertAfter("#player_" + data_id + '_' + next);

                        $('<input>').attr({
                            type: 'button',
                            id: 'button_' + data_id + '_' + next,
                            rel: data_id + '_' + next,
                            'data-rel': data_id,
                            'data-max': data.GET_PP_MAX,
                            class: 'form-control new_button_delete btn-danger',
                            value: "x",
                        }).appendTo("#label_button_" + data_id + '_' + next);
                    }
                    $("#picagem_operador").prop('disabled', true);
                    $("#picagem_operador").val('');
                    $("#picagem_serial").val('');
                    $("#picagem_serial").focus();

                }
            });

        }, 1000);
    });

    // APAGAR LINHA
    $(document).on('click', '.new_button_delete', function () {
        picker_id = $(this).attr('rel');
        data_rel = $(this).attr('data-rel');
        data_max = $(this).attr('data-max');
        $("#div_header_" + picker_id).remove();

        $(".div-header[rel='" + data_rel + "']").each(function (index) {
            $("#label_picker_" + $(this).attr('data-rel')).children("input").val($(".div-header[rel='" + data_rel + "']").length - index + '/' + data_max);
        });

        $("button[name='next_action']").prop('disabled', true);
    });

    // VALIDAR
    $(document).on('click', "input[name='validate_picker']", function () {
        if ($(".div-header").length > 0) {
            $("#validate_picker_button").prop('disabled', false);
        }
        else {
            $("#validate_picker_button").prop('disabled', true);
        }
    });

    //DAR SAIDA
    $(document).on('click', '#validate_picker_button', function (e) {
        var datastring = $("#picker_form").serialize();
        var myURL = parseUri(window.location.href);
        $.ajax({
            type: "POST",
            url: myURL['pathname'].split("/admin")[0] + '/admin/send_picker/wood',
            data: datastring,
            dataType: "json",
            success: function (data) {
                $("#waiting_overlay_picker_span").html("<b>Lista de picagem guardada com sucesso.</b><br/>A actualizar a página.<br/>Por favor aguarde...");
                location.reload();
            },
            beforeSend: function () {
                $("#waiting_overlay_picker_span").html("<b>A processar.</b><br/>Por favor aguarde...");
                $('#waiting_overlay_picker').show();
            }
        });
        e.preventDefault();
    });

    $(".ajax_waiting").hide();
    (function worker() {
        $.ajax({
            url: './check_waiting/prep',
            success: function (data) {
                if (data > 0) {
                    $(".ajax_waiting").fadeIn();
                    //                        $(".ajax_lock").fadeOut();
                    $("#waiting_span").html(data);
                }
                else {
                    //                        $(".ajax_lock").fadeIn();
                    $(".ajax_waiting").fadeOut();
                }
            },
            complete: function () {
                // Schedule the next request when the current one's complete
                setTimeout(worker, 25000);
            }
        });
    })();
</script>

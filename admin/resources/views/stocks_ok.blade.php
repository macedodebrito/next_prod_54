<?php
$montagem_espera = \App\Model\MountUser::where('published', '!=', 0)->sum('value');
$serralharia_espera = \App\Model\LocksmithRequestsUser::where('published', '!=', 0)->sum('value');
?>
<!-- Info boxes -->
<div class="row">

    <div class="col-md-4 col-sm-4 col-xs-6">
            <div class="info-box">
                <span class="info-box-icon bg-lime"><i class="fa fa-database"></i></span>

                <div class="info-box-content">
                    <h1>LISTAGEM EM STOCK</h1>
                </div>
                <!-- /.info-box-content -->
            </div>
    </div>

    <div class="col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/stocks_search') }}">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-search"></i></span>

                <div class="info-box-content">
                    <h3>PESQUISAR</h3>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div>
    
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a href="{{ URL::to('admin/stocks') }}">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <h3>VER RECUSADOS</h3>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div>
    
</div>

<div class="row">
    <div class="col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>Produtos em Stock</b></span>
                    <hr/>
                    <span class="info-box-text">Listado: <?=$montagem_espera?></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\MOUNT_history_Stock_OK')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>
    
    <div class="col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>Seralharia em Stock</b></span>
                    <hr/>
                    <span class="info-box-text">Listado: <?=$serralharia_espera?></span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\LocksmithStock_OK')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>    
</div>

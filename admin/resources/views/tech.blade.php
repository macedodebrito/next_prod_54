<?php

use App\Role;
use App\User;

$waiting = DB::table('tech_requests_users')->where('published', 0)->sum('value');

$role_check = 0;
$role_id = Role::where('name', 'admin')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}
$role_id = Role::where('name', 'manager')->first();
if (count(DB::table('role_user')->where('role_id', $role_id->id)->where('user_id', Auth::user()->id)->first()) > 0) {
    $role_check = 1;
}
?>

<!-- Info boxes -->
<div class="row">


    <!-- /.col -->
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-maroon"><i class="fa fa-tachometer"></i></span>

            <div class="info-box-content">
                <h1>TÉCNICA</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        <a href="{{ URL::to('admin/tecnica/week') }}">
            <div class="info-box">
                <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <span id="txt_btn_hidden_week_mount" class="info-box-text"><b>VER LISTA GERAL DE ENCOMENDAS</b><br/>(TÉCNICA)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/tecnica/paletes_list') }}">
            <div class="info-box">
                <span class="info-box-icon bg-black"><i class="fa fa-cubes"></i></span>
                <div class="info-box-content">
                    <span id="txt_btn_hidden_week_mount" class="info-box-text"><b>VER PALETES</b><br/>(ARMAZÉM)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div>

    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ url('/admin/tecnica/history') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">SAÍDAS<br/>(Técnica)</span>
                </div>
                <!--            {!! 
                            AdminSection::getmodel('App\Model\WOOD_history')->fireDisplay() 
                            !!}  -->
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>

    <!--    <div class="col-md-2 col-sm-2 col-xs-6">
            <a href="{{ url('/admin/tecnica/open_mount') }}">
                <div class="info-box ajax_lock">
                    <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
    
                    <div class="info-box-content">
                        <span class="info-box-text">SAÍDAS<br/>(Serralharia)</span>
                    </div>
                                {!! 
                                AdminSection::getmodel('App\Model\WOOD_history')->fireDisplay() 
                                !!}  
                     /.info-box-content 
                </div>
            </a>
        </div>-->

    <div class="col-md-1 col-sm-1 col-xs-6">
        <a href="{{ url('/admin/tecnica/history_picker/create') }}">
            <div class="info-box ajax_lock">
                <span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>
                <!-- /.info-box-content -->
            </div>
        </a>
    </div>    
</div>



<div class="row">
    <div id="tbl_week_tech" class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-calendar"></i></span>
                <div class="info-box-content">
                    <h class="info-box-text"><b>ENCOMENDAS</b></span>
                        <hr/>
                        <span class="info-box-text">-</span>
                </div>
                {!! 
                AdminSection::getmodel('App\Model\TechFreeze')->fireDisplay()
                !!}       
                <!-- /.info-box-content -->
            </div>        
        </div>
        <!-- /.col -->
    </div>
    <!-- /.col -->
</div>

<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>
<!-- /.row -->


<!-- /.row -->

<?php
$get_week_atraso = \App\Model\FreezeWeek::where('week', -1)->get();
$week_atraso_points = 0;
foreach($get_week_atraso as $week) {
    $week_atraso_points = $week_atraso_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_0 = \App\Model\FreezeWeek::where('week', 0)->get();
$week_0_points = 0;
foreach($get_week_0 as $week) {
    $week_0_points = $week_0_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_1 = \App\Model\FreezeWeek::where('week', 1)->get();
$week_1_points = 0;
foreach($get_week_1 as $week) {
    $week_1_points = $week_1_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_2 = \App\Model\FreezeWeek::where('week', 2)->get();
$week_2_points = 0;
foreach($get_week_2 as $week) {
    $week_2_points = $week_2_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}

$get_week_3 = \App\Model\FreezeWeek::where('week', 3)->get();
$week_3_points = 0;
foreach($get_week_3 as $week) {
    $week_3_points = $week_3_points + \App\Model\Product::where('id', $week->product_id)->first()->points;
}
?>
<!-- Info boxes -->
<div class="row">

    <div class="col-md-3 col-sm-3 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-lime"><i class="fa fa-database"></i></span>

            <div class="info-box-content">
                <h1>Objectivos</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>Configurações</b></span>
                    <hr/>
                    <span class="info-box-text">Objectivos</span>
            </div>
           {!! 
            AdminSection::getmodel('App\Model\ConfigTab')->fireEdit(1) 
            !!}  
            <!-- /.info-box-content -->
        </div>
    </div>    
    
</div>

<div id="btn-refresh" class="col-xs-offset-5 col-md-2 col-sm-2 col-xs-6">
    <a href="#">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-yellow"><i class="fa fa-refresh"></i></span>
            <div class="info-box-content">
                <h1>Replicar</h1>
            </div>            
            <!-- /.info-box-content -->
        </div>
    </a>
</div>

<div id="btn-triple" class="col-md-2 col-sm-2 col-xs-6">
    <a href="#">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-green"><i class="fa fa-step-forward"></i></span>
            <div class="info-box-content">
                <h1>Aplicar</h1>
            </div>            
            <!-- /.info-box-content -->
        </div>
    </a>
</div>   

<div id="btn-edit-one" class="col-md-1 col-sm-1 col-xs-3">
    <a href="#">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-black"><i class="fa fa-long-arrow-up"></i></span>
            <div class="info-box-content">
            </div>            
            <!-- /.info-box-content -->
        </div>
    </a>
</div> 

<div id="btn-edit-date" class="col-md-2 col-sm-2 col-xs-6">
    <a href="./objectivos_all">
        <div class="info-box ajax_lock">
            <span class="info-box-icon bg-aqua"><i class="fa fa-database"></i></span>
            <div class="info-box-content">
                <h1>Editar</h1>
            </div>            
            <!-- /.info-box-content -->
        </div>
    </a>
</div>  

<div class="row">  
    <div class="col-xs-7">           

        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>RÉPLICA</b></span>
                    <hr/>
                    <span class="info-box-text">Encomendas</span>
            </div>
            {!! 
            AdminSection::getmodel('App\Model\TripleWeek')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>
    <div class="col-xs-5">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA EM ATRASO</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_atraso_points }} Pontos por finalizar</span>
            </div>                  {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_Atraso')->fireDisplay() 
            !!}  
            
            <!-- /.info-box-content -->
        </div>  

        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 1 (Actual)</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_0_points }} Pontos por finalizar</span>
            </div>                     
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek')->fireDisplay() 
            !!}             
            <!-- /.info-box-content -->
        </div> 

        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 2 (Próxima)</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_1_points }} Pontos por finalizar</span>
            </div>   
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_1')->fireDisplay() 
            !!}             
            <!-- /.info-box-content -->
        </div> 

        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 3 (15 dias)</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_2_points }} Pontos por finalizar</span>
            </div>  
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_2')->fireDisplay() 
            !!}            
            <!-- /.info-box-content -->
        </div> 

        <div class="info-box">
            <span class="info-box-icon bg-white"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
                <h class="info-box-text"><b>SEMANA 4 (Escondida, o restante)</b></span>
                    <hr/>
                    <span class="info-box-text">{{ $week_3_points }} Pontos por finalizar | {{ round($week_3_points/3200,2) }} semanas</span>
            </div>       
            {!! 
            AdminSection::getmodel('App\Model\FreezeWeek_3')->fireDisplay() 
            !!}            
            <!-- /.info-box-content -->
        </div>         
    </div>    
</div>
<div style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; z-index: 9999;" id="waiting_freeze">
    <div id="waiting_freeze_span" style="position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        A processar...<br/>Por favor aguarde...
    </div>
</div>
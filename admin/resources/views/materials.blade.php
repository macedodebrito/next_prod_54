<!-- Info boxes -->
<div class="row">

    <div class="col-md-3 col-sm-3 col-xs-6">
        <div class="info-box">
            <span class="info-box-icon bg-black"><i class="fa fa-database"></i></span>

            <div class="info-box-content">
                <h1>Gestão de Materiais</h1>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="info-box">           
           {!! 
            AdminSection::getmodel('App\Model\Materials_Week_Tab_Late')->fireDisplay() 
            !!}  
            <!-- /.info-box-content -->
        </div>
    </div>    
    
</div>

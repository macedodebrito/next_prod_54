<?php

use App\Model\Request;
use App\Model\RequestUser;
use App\Model\WoodUser;
use App\Model\PaintUser;
use App\Model\Product;

$reportados = DB::table('problems')->where('status', 0)->count();
$unsolved = DB::table('problems')->where('status', 1)->count();
$solving = DB::table('problems')->where('status', 2)->count();
//
//$check = DB::table('request_week')->orderBy('end_wood_at', 'asc')->get();
//
//// lista de pedidos semanais por ordem
//$SEMANAL = [];
//$week = [];
////$total = 0;
////$total_req = 0;
////$total_week = 0;
////$total_req2 = 0;
////$dump = 0;
////            print_r("<pre>");
//foreach ($check as $key => $instance) {
//    $total = DB::table('request_week')->where('id', $instance->id)->sum('carpintaria');
//
//    $total_week = DB::table('request_week')->where('product_id', $instance->product_id)->sum('carpintaria');
//
//    $total_req = Request::where('product_id', $instance->product_id)->first();
//
//    //$total_wood = WoodUser::where('request_id', $total_req->id)->get();
//
//    $total_req2 = Request::where('product_id', $instance->product_id)->get();
//    $total_inc = 0;
//    foreach ($total_req2 as $go) {
//        $dump = WoodUser::where('request_id', $go->id)->sum('value') - PaintUser::where('request_id', $go->id)->sum('value');
//        $total_inc = $total_inc + $dump;
//    }
//    //print_r($total_inc);
//
//    $SEMANAL[$instance->product_id]['product_id'] = $instance->product_id;
//    $SEMANAL[$instance->product_id]['name'] = Product::where('id', $instance->product_id)->first()->name;
//    $SEMANAL[$instance->product_id]['total_semanal'] = $total_week;
//    $SEMANAL[$instance->product_id]['total_montado'] = $total_inc;
//
//    foreach (DB::table('request_week')->where('product_id', $instance->product_id)->get() as $key => $value) {
//        $SEMANAL[$instance->product_id]['semana'][$key]['id'] = $value->id;
//        $SEMANAL[$instance->product_id]['semana'][$key]['value'] = $value->carpintaria;
//    }
//}
////            print_r($SEMANAL);
//foreach ($SEMANAL as $key2 => $value2) {
//    foreach ($value2['semana'] as $key3 => $value3) {
//        if ($SEMANAL[$key2]['total_montado'] > 0) {
//            if ($SEMANAL[$key2]['total_montado'] >= $value3['value']) {
//                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//            } else if ($SEMANAL[$key2]['total_montado'] < $value3['value']) {
//                $SEMANAL[$key2]['semana'][$key3]['value'] = $value3['value'] - $SEMANAL[$key2]['total_montado'];
//                $SEMANAL[$key2]['total_montado'] = $SEMANAL[$key2]['total_montado'] - $value3['value'];
//            }
//            //print_r($SEMANAL[$key2]['semana'][$key3]);
//        }
//        if ($SEMANAL[$key2]['semana'][$key3]['value'] > 0) {
//            $week[] = $SEMANAL[$key2]['semana'][$key3]['id'];
//        }
//    }
//}
//$carpintaria_semanal = DB::table('request_week')->whereIn('id', $week)->orderBy('end_at', 'asc')->sum('carpintaria');
//$carpintaria_disponivel = RequestUser::sum('value') - WoodUser::sum('value');
//$carpintaria_saida = WoodUser::sum('value');
//
//if ($carpintaria_disponivel > 0) {
//    $carpintaria_percent_1 = round(100 * $carpintaria_semanal / ($carpintaria_disponivel + $carpintaria_semanal), 2);
//    $carpintaria_percent_1 = 100 - $carpintaria_percent_1;
//    $carpintaria_percent_2 = round(100 * $carpintaria_saida / ($carpintaria_disponivel + $carpintaria_saida), 2);
//} else {
//    $carpintaria_percent_1 = 100;
//    $carpintaria_percent_2 = 0;
//}
//$total = DB::table('requests')->sum('value');
//if ($total > 0) {
//    $maquinados = DB::table('request_users')->sum('value');
//    $por_maquinar = $total - $maquinados;
//    $percent_1 = round(100 * $por_maquinar / $total, 2);
//    $percent_2 = round(100 * $maquinados / $total, 2);
//} else {
//    $maquinados = 0;
//    $por_maquinar = 0;
//    $percent_1 = 100;
//    $percent_2 = 0;
//}
?>
<!-- Info boxes -->
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-6">

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/cnc') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-gamepad"></i></span>

                    <div class="info-box-content">
                        <h2>CNC</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/wood') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-cubes"></i></span>

                    <div class="info-box-content">
                        <h2>CARPINTARIA</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/woodfinishing') }}">

                <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="fa fa-diamond"></i></span>

                    <div class="info-box-content">
                        <h2>ACABAMENTO</h2>
                        <div style="margin-top: -6px; font-size: 16px;">DA CARPINTARIA</div>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>        

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/paint') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-paint-brush"></i></span>

                    <div class="info-box-content">
                        <h2>PINTURA</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/mount') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-sign-language"></i></span>

                    <div class="info-box-content">
                        <h2>MONTAGEM</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>        

    </div>    

    <div class="col-md-4 col-sm-4 col-xs-6">

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/prep') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="fa fa-diamond"></i></span>

                    <div class="info-box-content">
                        <h2>PREPARAÇÃO</h2>
                        <div style="margin-top: -6px; font-size: 16px;">DA CNC</div>              
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/serralharia') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-grey"><i class="fa fa-cogs"></i></span>

                    <div class="info-box-content">
                        <h2>SERRALHARIA</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/wires') }}">

                <div class="info-box">
                    <span class="info-box-icon bg-lime"><i class="fa fa-hashtag"></i></span>

                    <div class="info-box-content">
                        <h2>REDES</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>         

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/tecnica') }}">

                <div class="info-box">
                    <span class="info-box-icon bg-maroon"><i class="fa fa-tachometer"></i></span>

                    <div class="info-box-content">
                        <h2>TÉCNICA</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <!--        <div class="col-xs-12">
                    <a href="{{ URL::to('admin/blackfinishing') }}">
        
                        <div class="info-box">
                            <span class="info-box-icon bg-black"><i class="fa fa-diamond"></i></span>
        
                            <div class="info-box-content">
                                <h2>ACABAMENTO</h2>
                                <div style="margin-top: -6px; font-size: 16px;">DE PRETO</div>                       
                            </div>
                             /.info-box-content 
                        </div>
                         /.info-box 
                    </a>
                </div>         -->

    </div>    

    <div class="col-md-4 col-sm-4 col-xs-6">
        <div class="col-xs-12">
            <a href="{{ URL::to('admin/mount/paletes') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="fa fa-barcode"></i></span>

                    <div class="info-box-content">
                        <h2>PALETES</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>

        <div class="col-xs-12">
            <a href="{{ URL::to('admin/view_comsumables') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-black"><i class="fa fa-tasks"></i></span>

                    <div class="info-box-content">
                        <h2>CONSUMIVEIS</h2>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>        
        <div class="col-xs-12">
            <a href="{{ URL::to('admin/problems') }}">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-exclamation"></i></span>

                    <div class="info-box-content">
                        <h2>MANUTENÇÃO</h2>
                        <div style="margin-top: -6px; font-size: 16px;"><?=$reportados?> reportados, <?=$unsolved?> por resolver, <?=$solving?> em resolução</div>  
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </a>
        </div>               
    </div>    

</div>

<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>
<!-- /.row -->


<!-- /.row -->

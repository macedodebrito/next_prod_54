<?php
ini_set('max_execution_time', 360);

use App\Role;

$get_section = "NONE";

$role_check = 0;
$role_name = "";

$get_roles = DB::table('role_user')->where('user_id', Auth::user()->id)->first();
$role_name = Role::where('id', $get_roles->role_id)->first()->label;

// PONTOS
$data_red = "bg-red";
$data_yellow = "bg-yellow";
$data_green = "bg-green";
$hidden_points = "";

$no_obj = 0;

$COR_SEMANA = "";
$COR_DIA = "";
$COR_MES = 0;
$COR_SUPER = 0;

$SQL_POINTS_DIA = 0;
$SQL_POINTS_SEMANA = 0;
$SQL_POINTS_MES = 0;
$SQL_POINTS_SUPER = 0;

$data_min_day = 0;
$data_min_week = 0;
$data_min = 0;
$data_super = 0;

//default settings prod box
$px_top = "40px";
$px_right = "5px";
//date
$now = \Carbon\Carbon::now();

$now_d = $now->format("Y-m-d");
$tomorrow_d = $now->addDays(1)->format("Y-m-d");

$monday = $now->startOfWeek()->format("Y-m-d");

//$sundayS = $mondayS->addDays(7);
$sunday = $now->startOfWeek()->addDays(6)->format("Y-m-d");

//$st = new \Carbon\Carbon('first day of this month');
//$start_of_month = $st->format("Y-m-d");
//$end_of_month = $st->endOfMonth()->format("Y-m-d");
//$end_of_month_st = new \Carbon\Carbon('last day of this month');
//$end_of_month = $end_of_month_st->format("Y-m-d 23:59:59");

$hoje = \Carbon\Carbon::today();
$inicio = \Carbon\Carbon::now()->subMonth()->day(28); // Dia 27 do mês passado
$fim = \Carbon\Carbon::now()->day(27);

//$start_of_month = \Carbon\Carbon::now()->subMonthNoOverflow()->day(28)->startOfDay()->format("Y-m-d 00:00:00");
//$end_of_month = \Carbon\Carbon::now()->day(27)->endOfDay()->format("Y-m-d 23:59:59");

if ($hoje->between($inicio, $fim)) {
    $start_of_month = \Carbon\Carbon::now()->subMonthNoOverflow()->day(28)->startOfDay()->format("Y-m-d 00:00:00");
    $end_of_month = \Carbon\Carbon::now()->day(27)->endOfDay()->format("Y-m-d 23:59:59");
} else {
    $start_of_month = \Carbon\Carbon::now()->day(28)->startOfDay()->format("Y-m-d 00:00:00");
    $end_of_month = \Carbon\Carbon::now()->addMonthNoOverflow()->day(27)->endOfDay()->format("Y-m-d 23:59:59");
}

//dump($start_of_month);
//dump($end_of_month);

$BTN_REQUEST = "";
if (Request::path() == 'admin/blackfinishing') {
    // ACABAMENTO DE PRETO
    $points_table = "blackfinishing";
    $points_setup_w = "limits_woodfinishing_black";
    $points_setup_m = "m_limits_woodfinishing_black";
    $points_setup_s = "s_limits_woodfinishing_black";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/woodfinishing') {
    // ACABAMENTO
    $points_table = "stock_users";
    $points_setup_w = "limits_woodfinishing";
    $points_setup_m = "m_limits_woodfinishing";
    $points_setup_s = "s_limits_woodfinishing";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/wood') {
    // CARPINTARIA
    $points_table = "wood_users";
    $points_setup_w = "limits_wood";
    $points_setup_m = "m_limits_wood";
    $points_setup_s = "s_limits_wood";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/mount') {
    // MONTAGEM
    $points_table = "mount_users";
    $points_setup_w = "limits_mount";
    $points_setup_m = "m_limits_mount";
    $points_setup_s = "s_limits_mount";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/paint') {
    // PINTURA
    $points_table = "paint_users";
    $points_setup_w = "limits_paint";
    $points_setup_m = "m_limits_paint";
    $points_setup_s = "s_limits_paint";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/prep') {
    // PREPARAÇÃO
//    $points_table = "request_users";
//    $points_setup_w = "limits_prep";
//    $points_setup_m = "m_limits_prep";
//    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
//    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
//    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
    $points_table = "prep_users";
    $points_setup_w = "limits_prep";
    $points_setup_m = "m_limits_prep";
    $points_setup_s = "s_limits_prep";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();    
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();    
} else if (Request::path() == 'admin/cnc') {
    // CNC
    $points_table = "request_users";
    $points_setup_w = "limits_cnc";
    $points_setup_m = "m_limits_cnc";
    $points_setup_s = "s_limits_cnc";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->Where('published', '>=', 0)->Where('prep', '>=', 0)->get();
} else if (Request::path() == 'admin/serralharia') {
    // SERRALHARIA
    $points_table = "locksmith_requests_users";
    $points_setup_w = "limits_locksmith";
    $points_setup_m = "m_limits_locksmith";
    $points_setup_s = "s_limits_locksmith";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/tecnica') {
    // TECNICA
    $points_table = "tech_requests_users";
    $points_setup_w = "limits_tech";
    $points_setup_m = "m_limits_tech";
    $points_setup_s = "s_limits_tech";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else if (Request::path() == 'admin/wires') {
    // REDES
    $points_table = "wires";
    $points_setup_w = "limits_wires";
    $points_setup_m = "m_limits_wires";
    $points_setup_s = "s_limits_wires";
    $SQL_d = DB::table($points_table)->whereBetween('created_at', array($now_d, $tomorrow_d))->get();
    $SQL_w = DB::table($points_table)->whereBetween('created_at', array($monday, $sunday))->get();
    $SQL_m = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
    $SQL_s = DB::table($points_table)->whereBetween('created_at', array($start_of_month, $end_of_month))->get();
} else {
    $no_obj = 1;
    $hidden_points = "hidden";

    if (Request::path() == 'admin/view_paletes') {
        $get_section = "armazem";
    }
    // REQUEST (BUTTON)
    if ((Request::path() == 'admin/status_requests') || (Request::path() == 'admin/locksmith_status_requests') || (Request::path() == 'admin/tech_status_requests')) {
        $BTN_REQUEST == "";
    } else {
        $BTN_REQUEST = "hidden";
    }
}


if ($no_obj == 0) {
    $data_min_day = round(DB::table("config")->where("id", 1)->first()->$points_setup_w / 5);
    $data_min_week = DB::table("config")->where("id", 1)->first()->$points_setup_w;
    $data_min = DB::table("config")->where("id", 1)->first()->$points_setup_m;
    $data_super = DB::table("config")->where("id", 1)->first()->$points_setup_s;

// DIÁRIO
    $SQL_POINTS_DIA = 0;
    foreach ($SQL_d as $value) {
        if (($points_table != "wires") && ($points_table != "locksmith_requests_users") && ($points_table != "blackfinishing") && ($points_table != "tech_requests_users")) {
            $SQL_R = DB::table('requests')->where('id', $value->request_id)->first();
            if ($SQL_R) {
                $SQL_P = DB::table('products')->where('id', $SQL_R->product_id)->first();
            }
        } else {
            if ($points_table == "locksmith_requests_users") {
                $SQL_R = DB::table('locksmith_requests')->where('id', $value->locksmith_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('locksmith')->where('id', $SQL_R->locksmith_id)->first();
                }
            } else if ($points_table == "tech_requests_users") {
                $SQL_R = DB::table('tech_requests')->where('id', $value->tech_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('tech')->where('id', $SQL_R->tech_id)->first();
                }
            } else {
                $SQL_P = DB::table('products')->where('id', $value->product_id)->first();
            }
        }
        if (isset($SQL_P)) {
            $POINTS = $SQL_P->points;
        } else {
            $POINTS = 0;
        }
        $SQL_POINTS_DIA = $POINTS * $value->value + $SQL_POINTS_DIA;
    }
    $percentagem_actual = $SQL_POINTS_DIA * 100 / $data_min_day;
    $diff = 100 - $percentagem_actual;
    if ($percentagem_actual > 100) {
        $COR_DIA = $data_green;
    } else if ($diff > 0 && $diff < 10) {
        $COR_DIA = $data_yellow;
    } else {
        $COR_DIA = $data_red;
    }

// SEMANAL
    $SQL_POINTS_SEMANA = 0;
    foreach ($SQL_w as $value) {
        if (($points_table != "wires") && ($points_table != "locksmith_requests_users") && ($points_table != "blackfinishing") && ($points_table != "tech_requests_users")) {
            $SQL_R = DB::table('requests')->where('id', $value->request_id)->first();
            if ($SQL_R) {
                $SQL_P = DB::table('products')->where('id', $SQL_R->product_id)->first();
            }
        } else {
            if ($points_table == "locksmith_requests_users") {
                $SQL_R = DB::table('locksmith_requests')->where('id', $value->locksmith_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('locksmith')->where('id', $SQL_R->locksmith_id)->first();
                }
            } else if ($points_table == "tech_requests_users") {
                $SQL_R = DB::table('tech_requests')->where('id', $value->tech_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('tech')->where('id', $SQL_R->tech_id)->first();
                }
            } else {
                $SQL_P = DB::table('products')->where('id', $value->product_id)->first();
            }
        }
        if (isset($SQL_P)) {
            $POINTS = $SQL_P->points;
        } else {
            $POINTS = 0;
        }
        $SQL_POINTS_SEMANA = $POINTS * $value->value + $SQL_POINTS_SEMANA;
    }
    $percentagem_actual = $SQL_POINTS_SEMANA * 100 / $data_min_week;
    $diff = 100 - $percentagem_actual;
    if ($percentagem_actual > 100) {
        $COR_SEMANA = $data_green;
    } else if ($diff > 0 && $diff < 10) {
        $COR_SEMANA = $data_yellow;
    } else {
        $COR_SEMANA = $data_red;
    }

    // MENSAL
    $SQL_POINTS_MES = 0;
    foreach ($SQL_m as $value) {
        if (($points_table != "wires") && ($points_table != "locksmith_requests_users") && ($points_table != "blackfinishing") && ($points_table != "tech_requests_users")) {
            $SQL_R = DB::table('requests')->where('id', $value->request_id)->first();
            if ($SQL_R) {
                $SQL_P = DB::table('products')->where('id', $SQL_R->product_id)->first();
            }
        } else {
            if ($points_table == "locksmith_requests_users") {
                $SQL_R = DB::table('locksmith_requests')->where('id', $value->locksmith_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('locksmith')->where('id', $SQL_R->locksmith_id)->first();
                }
            } else if ($points_table == "tech_requests_users") {
                $SQL_R = DB::table('tech_requests')->where('id', $value->tech_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('tech')->where('id', $SQL_R->tech_id)->first();
                }
            } else {
                $SQL_P = DB::table('products')->where('id', $value->product_id)->first();
            }
        }
        if ($SQL_P) {
            $POINTS = $SQL_P->points;
        } else {
            $POINTS = 0;
        }
        $SQL_POINTS_MES = $POINTS * $value->value + $SQL_POINTS_MES;
    }

    $percentagem_actual = $SQL_POINTS_MES * 100 / $data_min;
    $diff = 100 - $percentagem_actual;
    if ($percentagem_actual > 100) {
        $COR_MES = $data_green;
    } else if ($diff > 0 && $diff < 10) {
        $COR_MES = $data_yellow;
    } else {
        $COR_MES = $data_red;
    }

    // SUPER
    $SQL_POINTS_SUPER = 0;
    foreach ($SQL_s as $value) {
        if (($points_table != "wires") && ($points_table != "locksmith_requests_users") && ($points_table != "blackfinishing") && ($points_table != "tech_requests_users")) {
            $SQL_R = DB::table('requests')->where('id', $value->request_id)->first();
            if ($SQL_R) {
                $SQL_P = DB::table('products')->where('id', $SQL_R->product_id)->first();
            }
        } else {
            if ($points_table == "locksmith_requests_users") {
                $SQL_R = DB::table('locksmith_requests')->where('id', $value->locksmith_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('locksmith')->where('id', $SQL_R->locksmith_id)->first();
                }
            } else if ($points_table == "tech_requests_users") {
                $SQL_R = DB::table('tech_requests')->where('id', $value->tech_requests_id)->first();
                if ($SQL_R) {
                    $SQL_P = DB::table('tech')->where('id', $SQL_R->tech_id)->first();
                }
            } else {
                $SQL_P = DB::table('products')->where('id', $value->product_id)->first();
            }
        }
        if ($SQL_P) {
            $POINTS = $SQL_P->points;
        } else {
            $POINTS = 0;
        }
        $SQL_POINTS_SUPER = $POINTS * $value->value + $SQL_POINTS_SUPER;
    }

    $percentagem_actual = $SQL_POINTS_SUPER * 100 / $data_super;
    $diff = 100 - $percentagem_actual;
    if ($percentagem_actual > 100) {
        $COR_SUPER = $data_green;
    } else if ($diff > 0 && $diff < 10) {
        $COR_SUPER = $data_yellow;
    } else {
        $COR_SUPER = $data_red;
    }

}

$get_notifications = DB::table('notifications')->where('section', $get_section)->where('status', 0)->get();
if (count($get_notifications) > 0) {
    $OPACITY = 1;
} else {
    $OPACITY = 0;
}
?>

@if ($user)
<div class="<?= $hidden_points ?>" id="prod_div" style="width: 320px; position: absolute; top: <?= $px_top ?>; right: <?= $px_right ?>; text-align: center; color: #FFFFFF; z-index: 10;">
    <div class="col-xs-3 <?= $COR_DIA ?>" style=" padding: 0 !important;">
        <div style="font-size: 12px; padding: 0 !important;">(<?= $data_min_day ?>)</div>
    </div>
    <div class="col-xs-3 <?= $COR_SEMANA ?>" style=" padding: 0 !important;">
        <div style="font-size: 12px; padding: 0 !important;">(<?= $data_min_week ?>)</div>
    </div>
    <div class="col-xs-3 <?= $COR_MES ?>" style=" padding: 0 !important;">
        <div style="font-size: 12px; padding: 0 !important;">(<?= $data_min ?>)</div>
    </div>
    <div class="col-xs-3 <?= $COR_SUPER ?>" style=" padding: 0 !important;">
        <div style="font-size: 12px; padding: 0 !important;">(<?= $data_super ?>)</div>
    </div>    
    <div>
        <a class="download_pdf" href="./pdf/all/users_pdf">
            <button class="btn form-control btn-info">PRODUTIVIDADE</button>
        </a>
    </div>    
</div>

<?php if ((Request::path() != 'admin/problems') && (Request::path() != 'admin/problems/create') && (Request::path() != 'admin')) { ?>
    <li class="dropdown user user-menu bg-black" style="margin-right: 20px;">
        <a href="{{ url('/admin/problems') }}">
            <i class="fa fa-btn fa-exclamation"></i> MANUTENÇÃO
        </a>   
    </li>
<?php } ?>

<li class="dropdown user user-menu bg-maroon" style="margin-right: 20px;">
    <a href="{{ url('/admin/cnc_bordo_v2') }}">
        <i class="fa fa-btn fa-desktop"></i> PAINEL DE BORDO
    </a>   
</li>

<li class="dropdown user user-menu bg-red" style="margin-right: 20px;">
    <a href="{{ url('/logout') }}">
        <i class="fa fa-btn fa-sign-out"></i> {{ $role_name }} / Sair
    </a>   
</li>

<li id="ocomsumables-btn" class="ocomsumables dropdown user user-menu" style="margin-right: 20px; background-color: #ddd;">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true" style="color: #000;">      
        | <i class="fa fa-tasks"></i><span class="hidden-xs">Consumíveis</span>
    </a>
    <ul class="ocomsumables-ul dropdown-menu" style="width: 900px !important;">
        <!-- User image -->
        <li id="ocomsumables-menu" class="ocomsumables-li" style="background-color: #ddd;"> 
            <div style="padding: 24px !important;">
                <div class="col-xs-12 col-sm-6"><button id="ocomsumables-btn-listview" class="btn form-control btn-warning">Listar todos os pedidos de Consumíveis</button></div>
                <div class="col-xs-12 col-sm-6"><button id="ocomsumables-btn-sendview" class="btn form-control btn-success">Efectuar novo pedido</button></div>
            </div>
            <div class="clearfix" style="padding-bottom: 24px !important;"></div>
        </li>     
        <li id="ocomsumables-send-list" class="ocomsumables-li" style="background-color: #ddd;"> 
            <form id="ocomsumables-form" name="ocomsumables-form" method="POST">
                <div style="padding-top: 12px; padding-bottom: 60px;">
                    <div class="col-xs-4">
                        <label>Digite o seu Código</label>
                        <input id="ocomsumables-code" class="form-control" type="password">
                        <input id="ocomsumables-id" name="ocomsumables-id" type="hidden" value="">
                    </div>
                    <div class="col-xs-8">  
                        <label>Secções</label>
                        <select id="ocomsumables-section" name="ocomsumables-section" class="form-control form-element-select">
                            <option value="0">Escolha uma opção</option>
                            <option value="cnc">CNC</option>
                            <option value="carpintaria">Carpintaria</option>
                            <option value="acabamento">Acabamento</option>
                            <option value="pintura">Pintura</option>
                            <option value="montagem">Montagem</option>
                            <option value="serralharia">Serralharia</option>
                            <option value="tecnica">Técnica</option>
                            <option value="redes">Redes</option>
                            <option value="serigrafia">Serigrafia</option>
                            <option value="filtros">Filtros</option>
                            <option value="armazem">Armazém de Produto Acabado</option>
                            <option value="comsumables">Armazém de Consumíveis</option>
                        </select>
                    </div>
                </div>
                <div id="ocomsumables-list-box">
                    <div class="col-xs-12 no-padding" style="font-weight: bold; padding-top: 12px !important; padding-bottom: 12px !important; background-color: #666; color: #FFF;">
                        <div class="col-xs-7">
                            <div class="col-xs-5 no-padding">Nome do Consumível</div>
                            <div class="col-xs-5 no-padding"><input class="form-control form-element-input" type="text" id="ocomsumables-search-list"></div>    
                            <div class="col-xs-2 no-padding"><button class="form-control bg-red" type="button" id="ocomsumables-search-list-reset">Limpar</button></div>    
                        </div>
                        <div class="col-xs-3">(Tipo de Unidade)</div>
                        <div class="col-xs-2">Quantidade</div>
                    </div>
                    <div id="ocomsumables-list" style="width: 100%; height: 300px; max-height: 300px; overflow-y: auto;">
                    </div>
                </div>
                <br/>
                <div class="col-xs-12 user-footer bg-green" style="padding: 4px !important;">
                    <div class="col-xs-2">
                        <span>
                            <button type="button" id="ocomsumables-btn-listview-inside" class="btn form-control btn-warning">Ver Pedidos</button>
                        </span>
                    </div>
                    <div class="col-xs-4">
                        <span id="ocomsumables-validation">
                            <span id="ocomsumables-name"></span> Confirma? <button class="btn" id="ocomsumables-check">SIM</button>
                        </span>
                    </div>
                    <div class="col-xs-3">
                        <button id="ocomsumables-btn-preview" class="btn form-control btn-info">Pré-Visualizar</button>
                    </div>
                    <div class="col-xs-3">
                        <button id="ocomsumables-btn-send" class="btn form-control">Efectuar o pedido</button>
                    </div>
                </div>
            </form>
        </li>

    </ul>
</li>


<li class="oclock dropdown user user-menu bg-yellow" style="margin-right: 20px;">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">      
        | <i class="fa fa-tachometer"></i><span class="hidden-xs">Relógio de Ponto</span>
    </a>
    <ul class="oclock-ul dropdown-menu"> 
        <!-- User image -->
        <li class="oclock-li user-header bg-yellow">            
            Digite o seu Código
            <br/>
            <input id="oclock-code" class="form-control" type="password">
            <input id="oclock-id" type="hidden" value="">
            <input id="oclock-option" type="hidden" value="">
            <br/>
            <span id="oclock-pick">                      
            </span>
            <br/>
            <span id="oclock-pick2">                
            </span>            
            <br/>
            <span id="oclock-validation">
                <span id="oclock-name"></span> Confirma? <button class="btn" id="oclock-check">SIM</button>
            </span>

        </li>
        <li class="oclock-li user-footer bg-green">
            <button id="oclock-btn-entrada" class="btn form-control">Registar ENTRADA</button>
        </li> 
        <li class="oclock-li user-footer bg-red">
            <button id="oclock-btn-saida" class="btn form-control">Registar SAÍDA</button>
        </li>
        <li class="oclock-li user-footer bg-black">
            <button id="oclock-btn-faltas" class="btn form-control">Marcar FALTA</button>
        </li> 
    </ul>
</li>
<div id="nxt-versioning" style="position: fixed; bottom: 6px; left: 6px; background: #000; padding: 5px;"><a></a><i class="fa fa-random fa-2x"></i></div>

<li class="<?= $hidden_points ?> <?= $COR_DIA ?> dropdown user user-menu text-center" style="margin-right: 0px; width: 80px;">
    <div>DIA</div>
    <div style="font-size: 18px; padding: 0 !important; margin-top: -4px;"><?= $SQL_POINTS_DIA ?></div>
</li>

<li class="<?= $hidden_points ?> <?= $COR_SEMANA ?> dropdown user user-menu text-center" style="margin-right: 0px; width: 80px;">
    <div>SEMANA</div>
    <div style="font-size: 18px; padding: 0 !important; margin-top: -4px;"><?= $SQL_POINTS_SEMANA ?></div>
</li>

<li class="<?= $hidden_points ?> <?= $COR_MES ?> dropdown user user-menu text-center" style="margin-right: 0px; width: 80px;">
    <div>MÊS</div>
    <div style="font-size: 18px; padding: 0 !important; margin-top: -4px;"><?= $SQL_POINTS_MES ?></div>
</li>

<li class="<?= $hidden_points ?> <?= $COR_SUPER ?> dropdown user user-menu text-center" style="margin-right: 20px; width: 80px;">
    <div>SUPER</div>
    <div style="font-size: 18px; padding: 0 !important; margin-top: -4px;"><?= $SQL_POINTS_MES ?></div>
</li>

<div style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; z-index: 9999;" id="overlay_clock">
    <div id = "overlay_clock_span" style = "position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        A processar o registo no Relógio de Ponto.<br/>Por favor aguarde...
    </div>
</div>

<div style = "position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; z-index: 9999;" id = "overlay_ocomsumables">
    <div id = "ocomsumables_clock_span" style = "position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        A processar o pedido de consumíveis.<br/>Por favor aguarde...
    </div>
</div>

<div style="position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat; z-index: 9999;" id="overlay_waiting">
    <div id = "waiting_overlay_span" style = "position: absolute; width: 100%; top: 40%; font-size: 20px; text-align: center;">
        A processar.<br/>Por favor aguarde...
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="WorkAbsencesModal" tabindex="-1" role="dialog" aria-labelledby="WorkAbsencesModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="WorkAbsencesModalLabel">Validar Falta</h5>
      </div>
      <div class="modal-body">
        <div class="form-group row">
            <label for="WorkAbsencesPassword" class="col-sm-5 col-form-label">Código de Utilizador</label>
            <div class="col-sm-7">
                <input type="password" class="form-control" id="WorkAbsencesPassword">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-success">Validar</button>
      </div>
    </div>
  </div>
</div>

<div style = "position: fixed; top: 0; left: 0; width: 100%; margin: 0 auto; min-height: 100%; height: 100%; display: none; background: rgba( 255, 255, 255, .75 ); z-index: 9999;" id = "overlay_ocomsumables_preview">
    <div id = "ocomsumables_preview_span" style = "position: absolute; width: 100%; top: 48px; font-size: 14px; text-align: center;">
        <div id = "overlay_ocomsumables_preview_close" style = "position: fixed; top: 12px; right: 12px; background: #ff7701; height: 48px; width: 48px; margin: 0; padding: 6px;">
            <i class = "fa fa-times"></i>
        </div>
        <div class = "col-xs-12" style = "background: #FFFFFF; min-height: 500px; margin: 0; padding: 12px;">
            <div class = "col-xs-12" style = "border-bottom: 1px solid #666;">
                <div class = "col-xs-10 text-left">Nome do Consumível</div>
                <div class = "col-xs-2">Quantidade</div>
            </div>
            <div id = "ocomsumables-preview-list" style = "text-align: left; display: inline-block; width: 100%; max-height: 500px; overflow-y: auto;"></div>
            <br/>
            <div id = "ocomsumables-preview-list-new" class = "text-left"></div>
        </div>
    </div>
</div>

<div id="all-notifications" style="position: fixed; top: 77px; right: 0; background-color: #CCCCCC; padding-top: 15px; padding-left: 15px; padding-right: 15px; opacity: <?= $OPACITY; ?>;">
    <div style="padding-bottom:32px;">
        <div style="float:left"><strong>NOTIFICAÇÕES</strong></div>
        <div style="float:right"><button id="close-notifications">X</button></div>
    </div>
    <?php
    foreach ($get_notifications as $notification) {
        if ($notification->type == 1) {
            $TYPE = "Pedido de Palete";
            $PALETE = DB::table('paletes')->where('id', $notification->object_id)->first();
            if ($PALETE->phc_id) {
                $PALETE_ID = "NSP " . $PALETE->phc_id;
            } else {
                $PALETE_ID = "NXT " . $PALETE->request_week_id;
            }
        }
        ?>
        <div class="alert alert-danger alert-dismissible fade in">
            <strong><?= $notification->open_at ?> | </strong> <?= $TYPE ?> <b>( <?= $PALETE_ID ?> )</b>
        </div>    
        <?php
    }
    ?>

</div>

<!--<div style="position: fixed; top: 90px; right: 12px; font-weight: bold; font-size: 18px;" class="<?= $BTN_REQUEST; ?>">
    <button id="status_refresh" style="background-color: #000000; padding: 12px; color: #FFFFFF; ">
        ACTUALIZAR 
    </button>
</div>-->
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>
var myURL = parseUri(window.location.href);
function parseUri(url) {

    var result = {};

    var anchor = document.createElement('a');
    anchor.href = url;

    var keys = 'protocol hostname host pathname port search hash href'.split(' ');
    for (var keyIndex in keys) {
        var currentKey = keys[keyIndex];
        result[currentKey] = anchor[currentKey];
    }

    result.toString = function () {
        return anchor.href;
    };
    result.requestUri = result.pathname + result.search;
    return result;

}
$(document).ready(function () {
    $("#overlay_waiting").show();

    $(".skin-blue .sidebar a").css("color", "#FFFFFF")

    $("#oclock-check").prop('disabled', true);
    $("#oclock-btn-entrada").prop('disabled', true);
    $("#oclock-btn-saida").prop('disabled', true);
    $("#oclock-id").val("");

    $('#ocomsumables-section').val(0);
    $("#ocomsumables-check").prop('disabled', true);
    $("#ocomsumables-btn-send").prop('disabled', true);
    $("#ocomsumables-section").prop('disabled', true);
    $("#ocomsumables-id").val("");
    $("#ocomsumables-list-box").hide();
    $("#ocomsumables-send-list").hide();
    $("#ocomsumables-view-list").hide();
    $("#ocomsumables-btn-preview").prop('disabled', true);


//    $('a.sidebar-toggle').trigger('click');
    setTimeout(function () {
        $('a.sidebar-toggle').click()
    }, 100);

    $("#close-notifications").click(function (e) {
        $("#all-notifications").remove();
    });

    $("li.oclock > ul.oclock-ul").click(function (e) {
        e.stopPropagation();
    });

    $("li.ocomsumables > ul.ocomsumables-ul").click(function (e) {
        e.stopPropagation();
    });

    $('#oclock-check').on('click', function () {
        if ($('#oclock-option').val() == "S") {
            $("#oclock-btn-saida").prop('disabled', false);
            $("#oclock-btn-entrada").prop('disabled', true);
        }
        else {
            $("#oclock-btn-entrada").prop('disabled', false);
            $("#oclock-btn-saida").prop('disabled', true);
        }
    });

    $('#ocomsumables-check').on('click', function (e) {
        e.preventDefault();
        if ($("#ocomsumables-section").val() != "0") {
            $("#ocomsumables-btn-send").prop('disabled', false);

        }
    });

    $("#oclock-btn-entrada, #oclock-btn-saida").on('click', function () {
        $("#oclock-btn-entrada").prop('disabled', true);
        $("#oclock-btn-saida").prop('disabled', true);
        var myURL = parseUri(window.location.href);
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/set_oclock/' + $("#oclock-id").val() + "/" + $("#oclock-option").val(),
            success: function (data) {
                location.reload();
            },
            beforeSend: function () {
                $('#overlay_clock').show();
            },
        });
    });

    $('#ocomsumables-btn-sendview').on('click', function () {
        $("#ocomsumables-menu").hide();
        $("#ocomsumables-send-list").show();
        $("#ocomsumables-code").focus();
    });

    (function ($) {
        $.fn.delayKeyup = function (callback, ms) {
            return this.each(function () {
                var timer = 0, elem = this;
                $(this).keyup(function () {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.call(elem);
                    }, ms);
                });
            });
        };
    })(jQuery);

    $('#oclock-code').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#oclock-code").val(),
            success: function (data) {
                if (data.id != 0) {
                    if (data.created_at != "NUNCA") {
                        var newdata = data.created_at.date;
                        var datex = newdata.split(" ");
                        var date_day = datex[0].split("-");
                        var day = date_day[2];
                        var month = date_day[1];
                        var year = date_day[0];

                        var time = datex[1].split(":");
                        var hour = time[0];
                        var minute = time[1];
                    }
                    $("#oclock-name").text(data.name + ",");
                    if (data.login_logout == "E") {
                        if (data.created_at != "NUNCA") {
                            $("#oclock-option").val('S');
                            $("#oclock-pick").html("ÚLTIMO registo:<b> " + day + "." + month + "." + year + "</b>");
                            $("#oclock-pick2").html("ENTROU às:<b> " + hour + ":" + minute + "</b>");
                        }
                        else {
                            $("#oclock-option").val('E');
                            $("#oclock-pick").html("ÚLTIMO registo:<b> Não existe!");
                            $("#oclock-pick2").html("<b>Efectue o seu primeiro registo!</b>");
                        }
                    }
                    else {
                        $("#oclock-option").val('E');
                        $("#oclock-pick").html("ÚLTIMO registo:<b> " + day + "." + month + "." + year + "</b>");
                        $("#oclock-pick2").html("SAIU às:<b> " + hour + ":" + minute + "</b>");
                    }
                    $("#oclock-id").val(data.id);
                    $("#oclock-check").prop('disabled', false);
                }
                else {
                    $("#oclock-name").text("");
                    $("#oclock-option").prop('disabled', true);
                    $("#oclock-option").val("");
                    $("#oclock-pick").text("");
                    $("#oclock-pick2").text("");
                    $("#oclock-hour").text("");
                    $("#oclock-check").prop('disabled', true);
                    $("#oclock-btn-entrada").prop('disabled', true);
                    $("#oclock-btn-saida").prop('disabled', true);

                    $("#oclock-id").val("");
                }
            }
        });
    }, 200);

    $.expr[':'].icontains = $.expr.createPseudo(function (text) {
        return function (e) {
            return $(e).text().toUpperCase().indexOf(text.toUpperCase()) >= 0;
        };
    });

    $('#ocomsumables-search-list').delayKeyup(function () {
        var searched = $('#ocomsumables-search-list').val();
        if (searched) {
            $('#ocomsumables-list > div > div.ocn-text').parent().show();
            $('#ocomsumables-list > div > div.ocn-text:not(:icontains(' + searched + '))').parent().hide();
        }
        else {
            $('#ocomsumables-list > div > div.ocn-text').parent().show();
        }
    });

    $('#ocomsumables-code').delayKeyup(function () {

        var myURL = parseUri(window.location.href);

        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#ocomsumables-code").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#ocomsumables-name").text(data.name + ",");
                    $("#ocomsumables-id").val(data.id);
                    $("#ocomsumables-check").prop('disabled', false);
                    $("#ocomsumables-section").prop('disabled', false).focus();
                }
                else {
                    $("#ocomsumables-name").text("");
                    $("#ocomsumables-check").prop('disabled', true);
                    $("#ocomsumables-btn-send").prop('disabled', true);
                    $("#ocomsumables-section").prop('disabled', true);
                    $("#ocomsumables-list-box").hide();
                    $("#ocomsumables-id").val("");
                    $("#ocomsumables-btn-preview").prop('disabled', true);
                }
            }
        });
    }, 200);


    //$("button[name='next_action']").prop('disabled', true);
    //

    $('#ocomsumables-section').on('change', function () {
        var myURL = parseUri(window.location.href);
        $("#ocomsumables-btn-preview").prop('disabled', false);
        if (this.value != 0) {
            $.ajax({
                url: myURL['pathname'].split("/admin")[0] + '/admin/get_comsumables/' + this.value,
                success: function (data) {
                    $("#ocomsumables-list-box").show();
                    $('#ocomsumables-list').html('')
                    var check = 0;
                    var info = 0;
                    while (check < 3) {
                        check = check + 1;
                        info = 1111 + check
                        $('#ocomsumables-list').prepend($('<div class="col-xs-12 no-padding" style="border-top: 1px solid #666; padding-top: 6px !important; padding-bottom: 6px !important;"><div class="col-xs-10 ocn-text"><input class="form-control form-element-input" type="text" name="ocomsumable-new-' + info + '"></div><div class="col-xs-2"><select class="form-control form-element-select" id="ocomsumable-id-new-' + info + '" name="ocn-new[][' + info + ']"><option value="0">0</option></select></div></div>'));
                        var min_max = 0;
                        while (min_max < 50) {
                            min_max = min_max + 1
                            min = 1 * min_max
                            $('<option/>', {value: min}).text(min).appendTo('#ocomsumable-id-new-' + info);
                        }
                    }

                    check = 0;
                    info = 0;
                    while (check < data.length) {
                        check = check + 1;
                        $('#ocomsumables-list').prepend($('<div class="col-xs-12 no-padding" style="border-top: 1px solid #666; padding-top: 6px !important; padding-bottom: 6px !important;"><div class="col-xs-7 ocn-text">' + data[info].nome + '</div><div class="col-xs-3">(' + data[info].unit + ')</div><div class="col-xs-2"><select class="form-control form-element-select" id="ocomsumable-id-' + data[info].comsumable_id + '" name="ocn[][' + data[info].comsumable_id + ']"><option value="0">0</option></select></div></div>'));

                        var min_max = 0;
                        while (min_max < data[info].max) {
                            min_max = min_max + 1
                            min = data[info].min * min_max
                            $('<option/>', {value: min}).text(min).appendTo('#ocomsumable-id-' + data[info].comsumable_id);
                        }

                        info = info + 1;
                    }
                }
            });
        }
        else {
            $("#ocomsumables-btn-preview").prop('disabled', true);
            $("#ocomsumables-btn-send").prop('disabled', true);
            $("#ocomsumables-list-box").hide();
            $('#ocomsumables-list').html('')
        }
    });

    $("#ocomsumables-btn-send").on('click', function (e) {
        $("#ocomsumables-btn-send").prop('disabled', true);
        var datastring = $("#ocomsumables-form").serialize();
        var myURL = parseUri(window.location.href);
        $.ajax({
            type: "POST",
            url: myURL['pathname'].split("/admin")[0] + '/admin/send_comsumables',
            data: datastring,
            dataType: "json",
            success: function (data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
                location.reload();
            },
            beforeSend: function () {
                $('#overlay_ocomsumables').show();
            }
        });
        e.preventDefault();
    });

    $('#oclock-btn-faltas').on('click', function () {
        var myURL = parseUri(window.location.href);
        var url = myURL['pathname'].split("/admin")[0] + '/admin/work_absences/create';
        window.location.href = url;
    });

    $('#ocomsumables-btn-listview').on('click', function () {
        var myURL = parseUri(window.location.href);
        var url = myURL['pathname'].split("/admin")[0] + '/admin/view_comsumables';
        window.location.href = url;
    });

    $('#ocomsumables-btn-listview-inside').on('click', function (e) {
        console.log("teste");
        e.preventDefault();
        var myURL = parseUri(window.location.href);
        var url = myURL['pathname'].split("/admin")[0] + '/admin/view_comsumables';
        window.location.href = url;
    });

    $('#ocomsumables-btn-preview').on('click', function (e) {
        e.preventDefault();
        $('#overlay_ocomsumables_preview').show();
        $("#ocomsumables-btn").trigger('click');

        $("#ocomsumables-preview-list").html("");
        $("#ocomsumables-preview-list-new").html("");
        $("#ocomsumables-list > div").each(function (e) {
            var ocn_text = '';
            var ocn_value = 0;
            var ocn_list = '';
            var ocn_color = '';
            if ($(this).find("select").val() > 0) {
                ocn_text = '';
                ocn_value = 0
                if ($(this).find("div.ocn-text").hasClass("col-xs-7")) {
                    ocn_text = $(this).find("div.ocn-text").text()
                    ocn_list = '#ocomsumables-preview-list'
                    ocn_color = '#FFFFFF'
                }
                else {
                    ocn_text = $(this).find("div.ocn-text > input").val()
                    ocn_list = '#ocomsumables-preview-list-new'
                    ocn_color = '#FFFFE0'
                }
                ocn_value = $(this).find("select").val()
                $(ocn_list).append($('<div class="col-xs-12" style="background-color: ' + ocn_color + '; padding: 10px; border-bottom: 1px solid #ccc;"><div class="col-xs-10">' + ocn_text + '</div><div class="col-xs-2 text-center">' + ocn_value + '</div></div>'));
            }

        });

    });

    $('#overlay_ocomsumables_preview_close').on('click', function (e) {
        e.stopPropagation();
        if ($('.dropdown').find('.dropdown-menu').is(":hidden")) {
            $('#ocomsumables-btn a').dropdown('toggle');
        }
        $('#overlay_ocomsumables_preview').hide();
    });

    $('#ocomsumables-search-list-reset').on('click', function (e) {
        $('#ocomsumables-search-list').focus().val("");
        $('#ocomsumables-list > div > div.ocn-text').parent().show();
    });

    // save and close
    $('button.btn.btn-success[name="next_action"][value="save_and_close"]').on('click', function (e) {
        $("#overlay_waiting").show();
    });
    // save and continue
    $('button.btn.btn-primary[name="next_action"][value="save_and_continue"]').on('click', function (e) {
        $("#overlay_waiting").show();
    });

    // save and create
    $('button.btn.btn-info[name="next_action"][value="save_and_create"]').on('click', function (e) {
        $("#overlay_waiting").show();
    });

    // first confirmation
    $('a > i.fa.fa-check.fa-2x').on('click', function (e) {
        $("#overlay_waiting").show();
    });

    // second confirmation
    $(document).on('click', 'a.second_validator > i.fa.fa-check.fa-2x', function (e) {
        $(this).parent("a.second_validator").parent("span").parent("div.tooltip-inner").parent("div.tooltip").hide();
        $("#overlay_waiting").show();
    });

    // first confirmation
    $(document).on('click', 'a > i.fa.fa-check.fa-2x', function (e) {
        $("#overlay_waiting").show();
    });

    // ask for request palete
    $('.request_palete_bell').on('click', function () {
        $("#request_palete_id_span").text($(this).attr('data-palete'));
        $("#request_palete_bell_yes").val($(this).attr('data-rel'));
        $("#request_palete_bell_loading").text("");
        $("#request_palete_bell").show();
    });

    //// NO
    $('#request_palete_bell_no').on('click', function () {
        $("#request_palete_id_span").text('');
        $("#request_palete_bell").hide();
        $("#request_palete_bell_yes").prop('disabled', true);
    });

    //// CODE
    $('#request_palete_bell_input').delayKeyup(function () {
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#request_palete_bell_input").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#request_palete_bell_yes").attr('data-user_id', data.id);
                    $("#request_palete_bell_input").prop('disabled', true);
                    $("#request_palete_bell_yes").prop('disabled', false);
                }
                else {
                    $("#request_palete_bell_input").val("");
                    $("#request_palete_bell_yes").prop('disabled', true);
                }
            }
        });
    }, 1000);

    //// YES
    $('#request_palete_bell_yes').on('click', function () {
        $("#request_palete_bell_yes").prop('disabled', true);
        $("#request_palete_bell_no").prop('disabled', true);
        $("#request_palete_bell_loading").text("Aguarde Por favor... A efectuar o pedido...");
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/request_palete_to_mount/' + $(this).val() + '/' + $(this).attr('data-user_id'),
            success: function (data) {
                document.location.reload(true);
            }
        });
    });

    // ask for palete autorization
    $('.request_palete_wait').on('click', function () {
        $("#request_palete_id_wait_span").text($(this).attr('data-palete'));
        $("#request_palete_id_wait_div").html($(this).attr('data-obs'));
        $("#request_palete_wait_yes").val($(this).attr('data-rel'));
        $("#request_palete_wait_loading").text("");
        $("#request_palete_wait").show();
    });

    //// NO
    $('#request_palete_wait_no').on('click', function () {
        $("#request_palete_id_wait_span").text('');
        $("#request_palete_id_wait_div").text('');
        $("#request_palete_wait").hide();
        $("#request_palete_wait_yes").prop('disabled', true);
    });

    //// CODE
    $('#request_palete_wait_input').delayKeyup(function () {
        $.ajax({
            url: myURL['pathname'].split("/admin")[0] + '/admin/get_code/' + $("#request_palete_wait_input").val(),
            success: function (data) {
                if (data.id != 0) {
                    $("#request_palete_wait_yes").attr('data-user_id', data.id);
                    $("#request_palete_wait_input").prop('disabled', true);
                    $("#request_palete_wait_yes").prop('disabled', false);
                }
                else {
                    $("#request_palete_wait_input").val("");
                    $("#request_palete_wait_yes").prop('disabled', true);
                }
            }
        });
    }, 1000);

    //// YES
    $('#request_palete_wait_yes').on('click', function () {
        $("#request_palete_wait_yes").prop('disabled', true);
        $("#request_palete_wait_no").prop('disabled', true);
        $("#request_palete_wait_loading").text("Aguarde Por favor... A autorizar a Palete.");
        var palete_id = $(this).val();
        var palete_user = $(this).attr('data-user_id');
        $.ajax({
            url: './paletes/' + palete_id + '/auth/' + palete_user,
            success: function (data) {
                $.ajax({
                    url: myURL['pathname'].split("/admin")[0] + '/admin/request_palete_to_mount_ok/' + palete_id + '/' + palete_user,
                    success: function (data) {
                        window.open('./pdf/' + palete_id + '/palete_pdf_view', "_blank");
                        document.location.reload(true);
                    }
                });

            }
        });
    });

    // STATUS REFRESH
    $('#status_refresh').on('click', function () {
        $("#waiting_overlay_span").parent().show();
        $.ajax({
            url: './get_requests_updated',
            success: function (data) {
                document.location.reload(true);
            }
        });
    });

});
$(window).load(function () {
    var myURL = parseUri(window.location.href);
    $("#overlay_waiting").hide();
    $(".fa.fa-bell").parentsUntil("tr").parent("tr").css("background-color", "#ffb2ae");

//    setInterval(function () {
//        $.ajax({
//            url: myURL['pathname'].split("/admin")[0] + '/admin/update_nxt',
//            success: function (data) {
//                console.log(data);
//            }
//        });
//    }, 180000);
});
</script>

@endif

<!-- Info boxes -->
<div class="row">


    <!-- /.col -->
    <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-gamepad"></i></span>

                <div class="info-box-content">
                    <h1>CNC</h1>
                </div>
                <!-- /.info-box-content -->
            </div>
    </div>
    
    <div class="col-md-2 col-sm-2 col-xs-6">
        <a href="{{ URL::to('admin/requests/cnc_open') }}">
            <div class="info-box">
                <span class="info-box-icon bg-orange"><i class="fa fa-check-square-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PEDIDOS<br/>(EM ABERTO)</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </a>
    </div> 
    <div class="col-md-offset-6 col-md-1 col-sm-1 col-xs-6">
        <a href="{{ url('/admin/requests/cnc_history_picker/create') }}">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-plus"></i></span>
            <!-- /.info-box-content -->
        </div>
        </a>
    </div>     
</div>

<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-purple"><i class="fa fa-calendar"></i></span>
            <br/>
            <div class="info-box-content">
                <span class="info-box-text"><b>ENCOMENDAS DE CLIENTES</b></span>
            </div>
            <hr>
            {!! 
            AdminSection::getmodel('App\Model\CNC_semanal_Order')->fireDisplay() 
            !!}                      
            <!-- /.info-box-content -->
        </div>        
    </div>
    <!-- /.col -->

    <div class="col-md-6 col-sm-6 col-xs-12">               
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
            <br/>
            <div class="info-box-content">
                <span class="info-box-text"><b>PEDIDOS PARA PRODUÇÃO JÁ EXECUTADOS</b></span>
            </div>
            <hr/> 
            {!! 
            AdminSection::getmodel('App\Model\CNC_history')->fireDisplay() 
            !!}             
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

</div>
<!--<script>
    window.setTimeout(function () {
        document.location.reload(true);
    }, 1800000);
</script>-->
<!-- /.row -->


<!-- /.row -->
